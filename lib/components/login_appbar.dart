import 'package:flutter/material.dart';

class LoginAppbar extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    final screenHeight = MediaQuery.of(context).size.height;
    final screenWidth = MediaQuery.of(context).size.width;
    final defaultScreenHeight = 720;
    final defaultScreenWidth = 360;
    return  Container(
      height: screenHeight*0.140,
      margin: EdgeInsets.only(left: screenWidth*0.062),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Align(
            alignment: Alignment.topLeft,
            child: Container(
              width: screenWidth*0.056,
              height: screenHeight*0.0180,
              margin: EdgeInsets.only(top: screenHeight*0.0638),
              child: FlatButton(
                onPressed: (){
                 // AppRoutes.push(context, LoginForgotPasswordWidget());
                },
                color: Colors.transparent,
                textColor: Color.fromARGB(255, 0, 0, 0),
                padding: EdgeInsets.all(0),
                child: Image.asset("assets/images/back-2.png",color: Colors.black),
              ),
            ),
          ),
          Spacer(),
          Align(
            alignment: Alignment.topLeft,
            child: Container(
              width: 282/360 * screenWidth,
              height: 81/720 * screenHeight,
              child: Image.asset(
                "assets/images/combined-shape-2.png",
                fit: BoxFit.fill,
              ),
            ),
          ),
        ],
      ),
    );
  }
}

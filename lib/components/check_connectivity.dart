import 'package:flutter/material.dart';
import 'package:connectivity/connectivity.dart';
import 'dart:async';


class CheckConnectivity extends StatefulWidget {
  bool isConnected;
  Connectivity connectivity = Connectivity();
    var connectivityResult = (Connectivity().checkConnectivity());

  @override
  _CheckConnectivityState createState() => _CheckConnectivityState();
  CheckConnectivity({
    this.isConnected
     
  });
}

class _CheckConnectivityState extends State<CheckConnectivity> {
  @override
  void initState() {
    super.initState();
    //calling api method
    print('init state called in common page');
    checkInternet();
   
  }
    // on dragging page refresh function will be activated
  Future<Null> checkInternet() async {
    if (widget.connectivityResult.toString() != 'ConnectivityResult.none') {
      setState(() {
       widget.isConnected=true;
        //Again calling api while refreshing
      });
    } else {
      widget.isConnected=false;
    }

    return null;
  }
  @override
  Widget build(BuildContext context) {
    print('something');
    return Container(
      
    );
  }
}
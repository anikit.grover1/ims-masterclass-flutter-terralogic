import 'package:flutter/material.dart';
import 'package:imsindia/utils/colors.dart';
import 'package:imsindia/utils/screen_util.dart';
import 'package:imsindia/utils/text_styles.dart';
import 'package:imsindia/utils/validator.dart';
class LoginIMSButton extends StatefulWidget {
  final GestureTapCallback onTap;
  final String text;
  final BorderRadius _borderRadius = BorderRadius.all(
    Radius.circular(Constant.sizeSmall),
  );
  bool enabled;
  bool buttonEnable =(FieldValidator.emailID && FieldValidator.loginUsingIms) ;


  LoginIMSButton({
    @required this.onTap,
    @required this.text,
    this.enabled = true,

  });
  @override
  _LoginIMSButtonState createState() => _LoginIMSButtonState();
}



class _LoginIMSButtonState extends State<LoginIMSButton> {
  @override



  @override
  Widget build(BuildContext context) {
    var screenSize= MediaQuery.of(context).size;
    var screenWidth = MediaQuery.of(context).size.width;
    var screenHeight = MediaQuery.of(context).size.height;
    return InkWell(
      borderRadius: widget._borderRadius,
      onTap:  widget.enabled ? widget.onTap : () {},
      child: Container(
        width:screenWidth * 270/360,
        height: screenHeight * 40/720,
        child: Center(
          child: Text(
            widget.text,
            style:  TextStyle(
              fontFamily: "IBMPlexSans",
              fontWeight: FontWeight.w500,
              fontSize: 14,
              color:(FieldValidator.emailID!=true && FieldValidator.loginUsingIms!=true)
                  || (FieldValidator.emailID!=true || FieldValidator.loginUsingIms!=true)
                  ? NeutralColors.blue_grey: NeutralColors.pureWhite,
              inherit: false,
            ),
          ),
        ),

        decoration: BoxDecoration(
          gradient: (FieldValidator.emailID!=true && FieldValidator.loginUsingIms!=true)
              || (FieldValidator.emailID!=true || FieldValidator.loginUsingIms!=true)
              ?LinearGradient(
              colors: [SemanticColors.iceBlue, SemanticColors.iceBlue]) : LinearGradient(
              begin: Alignment(0, 0),
              end: Alignment(1.0199999809265137, 1.0099999904632568),
              colors: [SemanticColors.light_purpely, SemanticColors.dark_purpely]),
          borderRadius: BorderRadius.all(Radius.circular(2)),
        ),
      ),
    );
  }

}

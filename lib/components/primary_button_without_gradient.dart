
import 'package:flutter/material.dart';
import 'package:imsindia/utils/colors.dart';
import 'package:imsindia/utils/screen_util.dart';
import 'package:imsindia/utils/text_styles.dart';
import 'package:imsindia/utils/validator.dart';


class PrimaryButtonWithoutGradient extends StatelessWidget {
  final GestureTapCallback onTap;
  final String text;
  final BorderRadius _borderRadius = BorderRadius.all(
    Radius.circular(Constant.sizeSmall),
  );
  bool enabled;
  bool buttonEnable =FieldValidator.buttonEnable;


  PrimaryButtonWithoutGradient({
    @required this.onTap,
    @required this.text,
    this.enabled = true,

  });



  @override
  Widget build(BuildContext context) {
    final screenHeight = MediaQuery.of(context).size.height;
    final screenWidth = MediaQuery.of(context).size.width;
    final defaultScreenHeight=720;
    final defaultScreenWidth=360;
    return InkWell(
      borderRadius: _borderRadius,
      onTap: enabled ? onTap : () {},
      child: Container(
        width:screenWidth * 270/360,
        height: screenHeight * 40/720,
        child: Center(
          child: Text(
            text,
            style: TextStyle(
              fontFamily: "IBMPlexSans",
              fontWeight: FontWeight.w500,
              fontSize: (14/defaultScreenWidth)*screenWidth,
              color: NeutralColors.blue_grey,
              inherit: false,
            ),

          ),
        ),

        decoration: BoxDecoration(
          gradient: LinearGradient(
              colors: [SemanticColors.iceBlue, SemanticColors.iceBlue]),
          borderRadius: BorderRadius.all(Radius.circular(2)),
        ),
      ),
    );
  }
}


















import 'package:flutter/material.dart';
import 'package:imsindia/utils/colors.dart';
import 'package:imsindia/utils/screen_util.dart';
import 'package:imsindia/utils/text_styles.dart';
import 'package:imsindia/utils/validator.dart';
class CreateProfileButton extends StatefulWidget {
  final GestureTapCallback onTap;
  final String text;
  final BorderRadius _borderRadius = BorderRadius.all(
    Radius.circular(Constant.sizeSmall),
  );
  bool enabled;
  bool buttonEnable =(FieldValidator.nameValue && FieldValidator.college) ;


  CreateProfileButton({
    @required this.onTap,
    @required this.text,
    this.enabled = true,

  });
  @override
  _CreateProfileButtonState createState() => _CreateProfileButtonState();
}



class _CreateProfileButtonState extends State<CreateProfileButton> {
  @override
  void initState() {
    // TODO: implement initState
    setState(() {
      widget.buttonEnable=false;
      widget.enabled=false;
    });
    super.initState();
  }


  @override
  Widget build(BuildContext context) {
    var screenSize= MediaQuery.of(context).size;
    var screenWidth = MediaQuery.of(context).size.width;
    var screenHeight = MediaQuery.of(context).size.height;
    return InkWell(
      borderRadius: widget._borderRadius,
      onTap:  widget.enabled ? widget.onTap : () {},
      child: Container(
        width:screenWidth * 270/360,
        height: screenHeight * 40/720,
        child: Center(
          child: Text(
            widget.text,
            style: TextStyle(
              fontFamily: "IBMPlexSans",
              fontWeight: FontWeight.w500,
              fontSize: 14,
              color:(FieldValidator.nameValue==true && FieldValidator.college==true)
                  ? NeutralColors.pureWhite: NeutralColors.blue_grey,
              inherit: false,
            ),
          ),
        ),

        decoration: BoxDecoration(
          gradient: (FieldValidator.nameValue==true && FieldValidator.college ==true)
              ?LinearGradient(
              begin: Alignment(0, 0),
              end: Alignment(1.0199999809265137, 1.0099999904632568),
              colors: [SemanticColors.light_purpely, SemanticColors.dark_purpely]) : LinearGradient(
              colors: [SemanticColors.iceBlue, SemanticColors.iceBlue]) ,
          borderRadius: BorderRadius.all(Radius.circular(2)),
        ),
      ),
    );
  }

}

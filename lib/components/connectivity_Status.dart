import 'dart:async';
import 'dart:convert';
import 'package:connectivity/connectivity.dart';
import 'package:http/http.dart' as http;

class ConnectionService {
  // [Arun classes like this should be singleton classes]
  //==> Praveen: From Now we will use singleton classes for this purpose
  static final ConnectionService _singleton = ConnectionService._internal();

  factory ConnectionService() {
    return _singleton;
  }

  ConnectionService._internal();
  Connectivity connectivity = Connectivity();
}

import 'package:flutter/material.dart';
import 'package:imsindia/components/custom_expansion_panel.dart';
import 'package:imsindia/utils/colors.dart';
import 'package:imsindia/resources/strings/login.dart';
import 'package:imsindia/components/Input_forms.dart';
import 'package:imsindia/utils/validator.dart';
import 'package:imsindia/views/login_page/login_create_profile.dart';
import 'package:imsindia/views/login_page/login_firstattempt_details.dart';
import 'package:shared_preferences/shared_preferences.dart';

class CatAttemptWidget extends StatefulWidget {
  final String labelText;

  CatAttemptWidget(@required this.labelText);

  @override
  _CatAttemptWidgetState createState() => _CatAttemptWidgetState();
}

class _CatAttemptWidgetState extends State<CatAttemptWidget> {
  bool expandFlag = false;
  bool firstAttemptDetails = false;
  int _activeMeterIndex;
  String title;
  String _gradeValue;
  String _grade = 'Percentage';
  String _state = '2012';
  int i =1;
  List titles = [
    'First Attempt',
    'Second',
    'Third',
    'Fourth',
    'Fifth',
  ];

  List<String> _states = <String>[
    '2012','2013', '2014', '2015'
  ];
  getFirstAttemptDetailFilled() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    firstAttemptDetails = prefs.getBool("firstDetailsFilled");
    print('Hellooooooo'+firstAttemptDetails.toString());
  }
  @override
  Widget build(BuildContext context) {
    final screenHeight = MediaQuery.of(context).size.height;
    final screenWidth = MediaQuery.of(context).size.width;
    final defaultScreenHeight = 720;
    final defaultScreenWidth = 360;
    return Container(
      margin:
      EdgeInsets.only(top: 10.0, left: 0.0, right: 0.0),
      child: CustomExpansionPanelList(
        expansionHeaderHeight: 45.0,
        iconColor: (_activeMeterIndex == 0)
            ? Colors.white
            : NeutralColors.dark_navy_blue,
        backgroundColor1: (_activeMeterIndex == 0)
            ? NeutralColors.purpleish_blue
            : Color.fromARGB(255, 51, 128, 204)
            .withOpacity(0.15),
        backgroundColor2: (_activeMeterIndex == 0)
            ? NeutralColors.purpleish_blue
            : Color.fromARGB(255, 51, 128, 204)
            .withOpacity(0.15),
        expansionCallback: (int index, bool status) {
          setState(() {
            getFirstAttemptDetailFilled();
            _activeMeterIndex =
            _activeMeterIndex == 0 ? null : 0;
          });
        },
        children: [
          new ExpansionPanel(
            canTapOnHeader: true,
            isExpanded: _activeMeterIndex == 0,
            headerBuilder: (BuildContext context,
                bool isExpanded) =>
            new Container(
              alignment: Alignment.centerLeft,
              child: new Padding(
                padding: EdgeInsets.only(
                    left: (15 / 320) * screenWidth),
                child: new Text(widget.labelText,
                    style: new TextStyle(
                        fontSize:
                        (12.5 / 360) * screenWidth,
                        fontFamily:
                        "IBMPlexSans",
                        fontWeight: FontWeight.w500,
                        color: (_activeMeterIndex == 0)
                            ? Colors.white
                            : Colors.black)),
              ),
            ),
            body:  FirstAttemptDetails(),
//            Container(
//            //  height: (70/720)*screenHeight,
//              margin: EdgeInsets.only(
//                  left: 15, top: 11, right: 15),
//              child: Row(
//                crossAxisAlignment:
//                CrossAxisAlignment.start,
//                children: [
//                  Expanded(child:
//                  Container(
//                    width: (140/360)*screenWidth,
//                   height: (67/720)*screenHeight,
//                    margin: EdgeInsets.only(
//                        top: 0,bottom:0),
//                    child: new FormField<String>(
//                      builder: (FormFieldState<String> state) {
//                        return InputDecorator(
//                          decoration: InputDecoration(
//                            labelText: CreateProfileLabels.year,
//                            labelStyle: TextStyle(
//                              color: SemanticColors.blueGrey,
//                              fontSize:   14,
//                              fontFamily: "IBMPlexSans",
//                              fontWeight: FontWeight.w400,
//                            ),
//                          ),
//                          isEmpty: _state == 'abcd',
//                          child: new DropdownButtonHideUnderline(
//                            child: new DropdownButton<String>(
//                              value: _state,
//                              isDense: true,
//                              isExpanded: false,
//                              iconEnabledColor: SemanticColors.dusky_blue,
//                              style:  TextStyle(
//                                color: NeutralColors.dark_navy_blue,
//                                fontSize:  14,
//                                fontFamily: "IBMPlexSans",
//                                fontWeight: FontWeight.w400,
//                              ),
//                              //iconSize: ,
//                              icon: const Icon(Icons.keyboard_arrow_down),
//                              onChanged: (String newValue) {
//                                setState(() {
//                                  _state = newValue;
//                                  //  state.didChange(newValue);
//                                });
//                              },
//                              items: _states.map((String value) {
//                                return new DropdownMenuItem<String>(
//                                  value: value,
//                                  child: Text(value),
//                                );
//                              }).toList(),
//                            ),
//                          ),
//                        );
//                      },
//                    ),
//                  )
//                  ),
//                  Container(
//                   width: (80/360)*screenWidth,
//                   height: (67/720)*screenHeight,
//                    margin:
//
//                    EdgeInsets.only(left:(20/360)*screenWidth,top: (2/720)*screenHeight,),
//                    child: TextInputFormField(
//                      keyboardType: TextInputType.number,
//                      lableText: CreateProfileLabels.percentile,
//                      textInputAction: TextInputAction.done,
//                      autovalidate: true,
//                     // validator: FieldValidator.validateEmail,
//                      onSaved: (value) => _gradeValue = value,
//                      //onFieldSubmitted: (_) => FocusScope.of(context).requestFocus(),
//                    ),
//                  )
//                ],
//              ),
//            ),


          ),],


      ),

    );

  }
}

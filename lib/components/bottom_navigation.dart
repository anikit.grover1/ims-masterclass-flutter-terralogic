import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:image/image.dart';
import 'package:imsindia/components/navigation_bar.dart';
import 'package:imsindia/routers/routes.dart';
import 'package:imsindia/views/Analytics/analytics_home.dart';
import 'package:imsindia/views/Arun/blogs/Blogs.dart';
import 'package:imsindia/views/Arun/settings/Settings.dart';
import 'package:imsindia/views/Emaximiser/emaximizer_home_screen.dart';
import 'package:imsindia/views/GDPI/GDPI_home_scr.dart';
import 'package:imsindia/views/GDPI/Gdpi_HomeScreen.dart';
import 'package:imsindia/views/GK_Zone/gk_home_screen.dart';
import 'package:imsindia/views/home_pages/home_widget.dart';
import 'package:imsindia/views/practice_pages/practice_home_screen.dart';
import 'package:imsindia/views/prepare_pages/prepare_home_screen.dart';

class BottomNavigation extends StatefulWidget {
  @override
  _BottomNavigationState createState() => _BottomNavigationState();
}

class _BottomNavigationState extends State<BottomNavigation> with SingleTickerProviderStateMixin {

  int _selectedIndex = 0;
  bool isVisible = false, isEdit = false, isActive = false;
  double containerHeight = 75.0;
  AnimationController _animationController;
  List<Widget> _widgetList = <Widget>[

    HomeWidget(),
    EmaximiserHomeScreen(),
    PrepareHomeScreenHtml(),
    Blogs(),
    GKZoneHomePage(),
    Gdpi_HomeScreen(),
    PracticeHomeScreen(),
    AnalyticsHomeScreen(),
    Settings(true),
  ];

  @override
  void initState() {
    // TODO: implement initState

    _animationController = AnimationController(vsync: this, duration: Duration(milliseconds: 500));

    super.initState();
  }

  @override
  Widget build(BuildContext context) {

    return AnimatedContainer(

        height: containerHeight,
        duration: Duration(milliseconds: 200),

        decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.only(topRight: Radius.circular(20), topLeft: Radius.circular(20)),
            boxShadow: [new BoxShadow(
              color: Colors.black26,
              blurRadius: 10.0,
            ),]

        ),

        child: Column(
          children: <Widget>[
            
            Align(
              alignment: Alignment.centerRight,
              child: Visibility(
                  child: Padding(
                      padding: EdgeInsets.fromLTRB(0.0, 15.0, 20.0, 0.0),
                      child: Text("Edit", style: TextStyle(color: Colors.blueAccent, fontSize: 15.0),)),
                  visible: isEdit,
              ),
            ),
            
            Visibility(

                visible: isVisible,

                child: Expanded(
                  flex: 1,
                  child: Row(
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        Expanded(
                          flex: 1,
                          child: Column(

                            children: <Widget>[

                              IconButton(
                                  iconSize: 30.0,
                                  icon: Icon(Icons.lightbulb_outline_rounded),
                                  color: Colors.blueAccent,
                                  onPressed: () {

                                    AppRoutes.push(context, GKZoneHomePage());
                                  }),

                              Text("GK ZONE", style: TextStyle(color: Colors.black),)

                            ],

                          ),
                        ),

                        Expanded(
                          flex: 1,
                          child: Column(

                            children: <Widget>[

                              IconButton(
                                iconSize: 30.0,
                                icon: Icon(Icons.group_outlined),
                                color: Colors.blueAccent,
                                onPressed: () {

                                  AppRoutes.push(context, Gdpi_HomeScreen());

                                },
                              ),

                              Text("GDPI", style: TextStyle(color: Colors.black),)

                            ],

                          ),
                        ),

                        Expanded(
                          flex: 1,
                          child: Column(

                            children: <Widget>[

                              IconButton(
                                iconSize: 30.0,
                                icon: Icon(Icons.assignment_outlined),
                                color: Colors.blueAccent,
                                onPressed: () {

                                  AppRoutes.push(context, PracticeHomeScreen());

                                },
                              ),

                              Text("PRACTICE", style: TextStyle(color: Colors.black),)

                            ],

                          ),
                        ),

                        Expanded(
                          flex: 1,
                          child: Column(

                            children: <Widget>[

                              IconButton(
                                iconSize: 30.0,
                                icon: Icon(Icons.analytics_outlined),
                                color: Colors.blueAccent,
                                onPressed: () {

                                  AppRoutes.push(context, AnalyticsHomeScreen());

                                },
                              ),

                              Text("ANALYSE", style: TextStyle(color: Colors.black),)

                            ],

                          ),
                        ),

                        Expanded(
                          flex: 1,
                          child: Column(

                            children: <Widget>[

                              IconButton(
                                iconSize: 30.0,
                                icon: Icon(Icons.settings),
                                color: Colors.blueAccent,
                                onPressed: () {

                                  AppRoutes.push(context, Settings(true));

                                },
                              ),

                              Text("SETTINGS", style: TextStyle(color: Colors.black),)

                            ],

                          ),
                        ),
                      ]),
                ),
              ),

        Expanded(
          flex: 1,
          child: Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[

                    Expanded(
                      flex: 1,
                      child: Column(

                        children: <Widget>[

                          IconButton(
                              iconSize: 30.0,
                              icon: Icon(Icons.home_outlined),
                              color: Colors.blueAccent,
                              onPressed: () {

                                AppRoutes.push(context, HomeWidget());

                              }),

                          Text("HOME", style: TextStyle(color: Colors.black),)

                        ],

                      ),
                    ),

                    Expanded(
                      flex: 1,
                      child: Column(

                        children: <Widget>[

                          IconButton(
                            iconSize: 30.0,
                            icon: Icon(Icons.apartment_outlined),
                            color: Colors.blueAccent,
                            onPressed: () {

                              AppRoutes.push(context, EmaximiserHomeScreen());

                            },
                          ),

                          Text("E-MAX", style: TextStyle(color: Colors.black),)

                        ],

                      ),
                    ),

                    Expanded(
                      flex: 1,
                      child: Column(

                        children: <Widget>[

                          IconButton(
                            iconSize: 30.0,
                            icon: Icon(Icons.playlist_play),
                            color: Colors.blueAccent,
                            onPressed: () {

                              AppRoutes.push(context, PrepareHomeScreenHtml());

                            },
                          ),

                          Text("PREPARE", style: TextStyle(color: Colors.black),)

                        ],

                      ),
                    ),

                    Expanded(
                      flex: 1,
                      child: Column(

                        children: <Widget>[

                          IconButton(
                            iconSize: 30.0,
                            icon: Icon(Icons.article_outlined),
                            color: Colors.blueAccent,
                            onPressed: () {

                              AppRoutes.push(context, Blogs());

                            },
                          ),

                          Text("BLOGS", style: TextStyle(color: Colors.black),)

                        ],

                      ),
                    ),

                    Expanded(
                      flex: 1,
                      child: Column(

                        children: <Widget>[

                          IconButton(
                            iconSize: 30.0,
                            icon: AnimatedIcon(

                              icon: AnimatedIcons.menu_close,
                              progress: _animationController,
                              color: Colors.blueAccent,

                            ),
                            color: Colors.blueAccent,
                            onPressed: () {

                              setState(() {

                                if (containerHeight == 170.0)
                                  containerHeight = 75.0;
                                else
                                  containerHeight = 170.0;

                                isActive = !isActive;

                                isActive ? _animationController.forward() : _animationController.reverse();

                                if (isActive) {

                                  Future.delayed(Duration(milliseconds: 200), () {

                                    setState(() {

                                      isVisible = !isVisible;
                                      isEdit = !isEdit;
                                    });

                                  });

                                } else {

                                  isVisible = !isVisible;
                                  isEdit = !isEdit;
                                }

                              });

                            },
                          ),

                          Text("MORE", style: TextStyle(color: Colors.black),)

                        ],

                      ),
                    ),


                  ]
              ),
        ),
          ],
        ),
//          )
      );
  }
}

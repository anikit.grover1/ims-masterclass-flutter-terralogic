
import 'package:flutter/material.dart';
import 'package:imsindia/utils/colors.dart';
import 'package:imsindia/utils/screen_util.dart';
import 'package:imsindia/utils/text_styles.dart';

//component for Google and facebook buttons
class GoogleFacebookButton extends StatelessWidget {
  final GestureTapCallback onTap;
  final String text;
  final String iconname;
  final Color color;

  final BorderRadius _borderRadius = BorderRadius.all(
    Radius.circular(Constant.sizeSmall),
  );

  final bool enabled;

  GoogleFacebookButton({
    @required this.onTap,
    @required this.text,
    @required this.iconname,
    @required this.color,


    this.enabled = true,
  });

  @override
  //widget to create google and facebook buttons
  Widget build(BuildContext context) {
    var screenSize= MediaQuery.of(context).size;
    var screenWidth = MediaQuery.of(context).size.width;
    var screenHeight = MediaQuery.of(context).size.height;
    //listener(onTap) for Container
    return InkWell(
      borderRadius: _borderRadius,
      onTap: enabled ? onTap : () {},
      //container widget used to contain a child widget
      child: Container(
        width:screenWidth/2.55,
        height: screenHeight / 16,
        //to align Row into center
        child: Center(
          child: new Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              new Center(
                //widget to insert image asset
                child: new Image.asset(
                  iconname,
                  color: Colors.white,
                  height:screenHeight/14,
                  width: screenWidth/14,
                ),
              ),
              new Padding(
                padding: new EdgeInsets.only(
                  right: screenWidth / 30,
                  left: screenWidth / 90,
                ),
                child: new Text(
                  text,
                  style:TextStyles.google_FacebookTextStyle,
                ),
              ),
            ],
          ),
        ),
        decoration: BoxDecoration(
          color: enabled ?color : Colors.grey,
          borderRadius: BorderRadius.all(Radius.circular(2)),
        ),
      ),
    );
  }
}


















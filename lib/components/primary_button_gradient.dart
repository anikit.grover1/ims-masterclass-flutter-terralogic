
import 'package:flutter/material.dart';
import 'package:imsindia/utils/colors.dart';
import 'package:imsindia/utils/screen_util.dart';
import 'package:imsindia/utils/text_styles.dart';
import 'package:imsindia/utils/validator.dart';


class PrimaryButtonGradient extends StatelessWidget {
  final GestureTapCallback onTap;
  final String text;
  bool enabled=true;
  final BorderRadius _borderRadius = BorderRadius.all(
    Radius.circular(Constant.sizeSmall),
  );



  PrimaryButtonGradient({
    @required this.onTap,
    @required this.text,

  });



  @override
  Widget build(BuildContext context) {
    final screenHeight = MediaQuery.of(context).size.height;
    final screenWidth = MediaQuery.of(context).size.width;
    final defaultScreenHeight=720;
    final defaultScreenWidth=360;
    return InkWell(
      borderRadius: _borderRadius,
      onTap:onTap,
      child: Container(
        width:screenWidth * 270/360,
        height: screenHeight * 40/720,
        child: Center(
          child: Text(
            text,
            style: TextStyle(
              fontFamily: "IBMPlexSans",
              fontWeight: FontWeight.w500,
              fontSize: (14/defaultScreenWidth)*screenWidth,
              color: NeutralColors.pureWhite,
              inherit: false,
            ),

          ),
        ),

        decoration: BoxDecoration(
          gradient: LinearGradient(
//              begin: Alignment(0, 0),
//
//              end: Alignment(1.0199999809265137, 1.0099999904632568),
              begin: Alignment(-1.031, 0.494),
              end: Alignment(1.031, 0.516),

              colors: [SemanticColors.light_purpely, SemanticColors.dark_purpely]),
              borderRadius: BorderRadius.all(Radius.circular(2)),
        ),
      ),
    );
  }
}


















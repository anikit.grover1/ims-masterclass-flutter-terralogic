import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:imsindia/utils/colors.dart';
import 'package:imsindia/utils/screen_util.dart';
import 'package:imsindia/utils/text_styles.dart';
import 'package:imsindia/resources/strings/login.dart';
class TextInputFormField extends StatelessWidget {
  final TextEditingController controller;
  final FormFieldSetter<String> onSaved;
  final FormFieldValidator<String> validator;
  final String prefixText;
  final String lableText;
  final TextStyle fontStyle;
  final TextStyle lableStyle;
    final TextStyle prefixStyle;
        final TextStyle hintStyle;


  final String initialvalue;
  final IconData icon;
  final int maxLength;
  final TextInputType keyboardType;
  final bool enabled;
  final bool obscureText;
  final TextInputAction textInputAction;
  final FocusNode focusNode;
  final bool autovalidate;
  final ValueChanged<String> onFieldSubmitted;
  final VoidCallback onEditingComplete;
  final InputBorder border;
  final String hintText;
  final RichText lable;
  final Widget suffixIcon;


  const TextInputFormField({
    Key key,
    this.hintStyle,
    this.onEditingComplete,
     this.controller ,
    this.onSaved,
    this.prefixStyle,
    this.lable,
    this.suffixIcon,
    this.validator,
    this.prefixText,
    this.lableStyle,
    this.lableText,
    this.initialvalue,
    this.maxLength,
    this.icon,
    this.keyboardType,
    this.enabled = true,
    this.obscureText = false,
    this.focusNode,
    this.onFieldSubmitted,
    this.autovalidate = false,
    this.border,
    this.fontStyle,
    this.hintText,
    this.textInputAction = TextInputAction.done,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      initialValue: initialvalue,
         controller: controller,
        onEditingComplete: onEditingComplete,
        focusNode: focusNode,
        autovalidate: autovalidate,
        decoration: InputDecoration(
          
          focusedBorder: UnderlineInputBorder(borderSide:BorderSide(color:SemanticColors.dark_purpely,width: 1 ) ),
          enabledBorder: border,
          suffixIcon: suffixIcon,
          errorStyle: TextStyles.errorTextStyle,
          prefixText: prefixText,
          labelText: lableText,
          hintText: hintText,
          hintStyle: hintStyle,
          prefixStyle: prefixStyle,
        
         // labelStyle: lableStyle,
          labelStyle: lableStyle,
          contentPadding: EdgeInsets.symmetric(vertical: 5.0)
          /*suffixIcon:IconButton(
              icon: Icon(Icons.remove),
              onPressed: () {
                debugPrint('222');
              })*/
          //border: border,
        ),
        textAlign: TextAlign.left,
        inputFormatters: (maxLength != null)
            ? obscureText
            ? [LengthLimitingTextInputFormatter(maxLength), WhitelistingTextInputFormatter(RegExp("[0-9]"))]
            : [LengthLimitingTextInputFormatter(maxLength)]
            : [],
        onSaved: onSaved,
        onFieldSubmitted: onFieldSubmitted,
        validator: validator,
        keyboardType: keyboardType,
        enabled: enabled,
        textInputAction: textInputAction,
        style: fontStyle,
        obscureText: obscureText,
    );
  }
}
// Copyright 2018 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:webview_flutter/webview_flutter.dart';
import 'package:imsindia/components/progress_hub.dart';
//void main() => runApp(
//      MaterialApp(
//        home: WebViewExample(),
//        theme: ThemeData(
//          primarySwatch: Colors.red,
//        ),
//      ),
//    );

class WebViewExample extends StatefulWidget {
  @override
  final videoId;
  WebViewExample(
      {this.videoId,
      });
  _WebViewExampleState createState() => _WebViewExampleState();
}

class _WebViewExampleState extends State<WebViewExample> {
  bool _isLoadingPage;

  @override
  void initState() {
    super.initState();
    _isLoadingPage = true;
  }

  final Completer<WebViewController> _controller =
  Completer<WebViewController>();

  String get player {
    String _player = '''
    <!DOCTYPE html>
    <html>
    <head>
        <style>
            html,
            body {
                height: 100%;
                width: 100%;
                margin: 0;
                padding: 0;
                background-color: #ffffff;
                overflow: hidden;
                position: fixed;
            }
        </style>
        <meta name='viewport' content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no'>
    </head>
    <body>
     <div  style="padding:56.25% 0 0 0;position:relative;">
     <iframe src="https://player.vimeo.com/video/${widget.videoId}" style="position:absolute;top:height/2;left:0;width:100%;height:100%;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
     </div><script src="https://player.vimeo.com/api/player.js"></script>
    <!- Your Vimeo SDK player script goes here ->
  </script>
    </body>
    </html>
    ''';
    return 'data:text/html;base64,${base64Encode(const Utf8Encoder().convert(_player))}';
  }
  bool _isLoading = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        brightness: Brightness.light,
        elevation: 0.0,
        centerTitle: false,
        titleSpacing: 0.0,
        backgroundColor: Colors.white,
        title: Text(
          'Video Caption',
          style: const TextStyle(
              color: Colors.black,
              fontWeight: FontWeight.w500,
              fontFamily: "IBMPlexSans",
              fontStyle: FontStyle.normal,
              fontSize: 16.0),
        ),
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back,
            color: Colors.black,
          ),
          onPressed: () => Navigator.pop(context, false),
        ),),

      body:  ProgressHUD(
        child: Padding(
          padding: EdgeInsets.all(10.0),
          child: Stack(
            children: <Widget>[
              WebView(
                initialUrl:player,
                initialMediaPlaybackPolicy: AutoMediaPlaybackPolicy.always_allow,
                javascriptMode: JavascriptMode.unrestricted,
    onWebViewCreated: (WebViewController webViewController) {
          _controller.complete(webViewController);

        },
                onPageFinished: pageFinishedLoading,
              ),
            ],
          ),
        ),
        inAsyncCall: _isLoading,
        opacity: 0.0,
      ));
  }
  void pageFinishedLoading(String url) {
    setState(() {
      _isLoading = false;
    });
  }
}





















//WebView(
//
//        initialUrl: player,
//        javascriptMode: JavascriptMode.unrestricted,
//        onWebViewCreated: (WebViewController webViewController) {
//          _controller.complete(webViewController);
//
//        },
//
//        initialMediaPlaybackPolicy: AutoMediaPlaybackPolicy.always_allow,
//        onPageFinished: (String url) {
//          print('Video Loaded');
//        },
//      ),
//    );
//  }
//}

import 'package:flutter/material.dart';
import 'package:imsindia/utils/colors.dart';

class CustomRadio extends StatefulWidget {
  List<String> buttonNames;
  CustomRadio(this.buttonNames);

  @override
  createState() {
    return new CustomRadioState();
  }
}

class CustomRadioState extends State<CustomRadio> {
  List<RadioModel> sampleData = new List<RadioModel>();

  addRadioModel(){
    print(widget.buttonNames);
    for(var i=0; i<widget.buttonNames.length;i++){
      sampleData.add(new RadioModel(false, widget.buttonNames[i]));
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    addRadioModel();
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body:
      SizedBox(
        height: 75,
        child: new ListView.builder(
          scrollDirection: Axis.horizontal,
          itemCount: sampleData.length,
          itemBuilder: (BuildContext context, int index) {
            return new InkWell(
              //highlightColor: Colors.red,
              splashColor: Colors.blueAccent,
              onTap: () {
                setState(() {
                  sampleData.forEach((element) => element.isSelected = false);
                  sampleData[index].isSelected = true;
                });
              },
              child: new RadioItem(sampleData[index]),
            );
          },
        ),
      ),
    );
  }
}

class RadioItem extends StatelessWidget {
  final RadioModel _item;
  RadioItem(this._item);
  @override
  Widget build(BuildContext context) {
    return new Container(
      width: 175,
      margin: new EdgeInsets.all(15.0),
      child: new Container(
        child: new Center(
          child: new Text(_item.buttonText,
              style: new TextStyle(
                  color:
                  _item.isSelected ? PrimaryColors.azure_Dark : Colors.black,
                  //fontWeight: FontWeight.bold,
                  fontSize: 18.0)),
        ),
        decoration: new BoxDecoration(
          color: _item.isSelected
              ? Colors.transparent
              : SemanticColors.light_ice_blue,
          border: new Border.all(
              width: 1.0,
              color: _item.isSelected
                  ? PrimaryColors.azure_Dark
                  : SemanticColors.light_ice_blue),
          borderRadius: const BorderRadius.all(const Radius.circular(2.0)),
        ),
      ),
    );
  }
}

class RadioModel {
  bool isSelected;
  final String buttonText;

  RadioModel(this.isSelected, this.buttonText);
}
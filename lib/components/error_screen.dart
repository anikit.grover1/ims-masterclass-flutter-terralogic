import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:imsindia/utils/svg_images/error_screen_svg_images.dart';
import 'package:imsindia/views/Arun/ims_utility.dart';


var screenWidthTotal;
var screenHeightTotal;
var _safeAreaHorizontal;
var _safeAreaVertical;
var screenHeight;
var screenWidth;

class ErrorScreen extends StatefulWidget {
  @override
  _ErrorScreenState createState() => _ErrorScreenState();
}

class _ErrorScreenState extends State<ErrorScreen> {
  @override
  Widget build(BuildContext context) {
    var _mediaQueryData = MediaQuery.of(context);
    screenWidthTotal = _mediaQueryData.size.width;
    screenHeightTotal = _mediaQueryData.size.height;
    _safeAreaHorizontal =
        _mediaQueryData.padding.left + _mediaQueryData.padding.right;
    _safeAreaVertical =
        _mediaQueryData.padding.top + _mediaQueryData.padding.bottom;

    screenHeight = screenHeightTotal - _safeAreaVertical;
    screenWidth = screenWidthTotal - _safeAreaHorizontal;
    return Container(
       child:Column(
         children: [
           Container(
             margin: EdgeInsets.only(
                 left: 18 / 360 * screenWidth,
                 top: 12.5 / 720 * screenHeight),
             child: SvgPicture.asset(
               ErrorScreenAssets.errorScreenImg,
               fit: BoxFit.fill,
             ),
           ),
           Container(
             margin: EdgeInsets.only(
                 top: 20 / 720 * screenHeight),
            child: Text("Something’s wrong!",
              style: TextStyle(color: ImsColors.dark_navy_blue,
                fontFamily: "IBMPlexSans",
                fontStyle: FontStyle.normal,
                fontSize: 16.0,
                fontWeight: FontWeight.w700,),
            ),

           ),
           Container(
             margin: EdgeInsets.only(
                 top: 20 / 720 * screenHeight,
             ),
             child: InkWell(
               onTap: (){

               },
               child: Text("Try Again",
                 style: TextStyle(color: ImsColors.azure,
                   fontFamily: "IBMPlexSans",
                   fontStyle: FontStyle.normal,
                   fontSize: 14.0,
                   fontWeight: FontWeight.w700,),
               ),
             ),

           ),
         ],
       ) ,
    );

  }
}

import 'dart:io';

import 'package:f_logs/f_logs.dart';
import 'package:flutter/material.dart';
import 'package:flutter_email_sender/flutter_email_sender.dart';
import 'package:imsindia/utils/colors.dart';
//import 'package:permission_handler/permission_handler.dart';
import 'package:sembast/sembast.dart';
import 'package:imsindia/utils/emaximiser_svg_icons.dart';

class FeedbackPage extends StatefulWidget{
  @override
  FeedbackPageState createState() => FeedbackPageState();
}

class FeedbackPageState extends State<FeedbackPage>{
  //runtime permission
  //final PermissionGroup _permissionGroup = PermissionGroup.storage;

  final Email email = Email(

    body: 'This is the feedback',
    subject: 'Feedback Log(myIMS mobile application)-Reg.',
    recipients: ['developer@imsindia.com'],
    //cc: ['cc@example.com'],
    //bcc: ['bcc@example.com'],
    //attachmentPath: '/path/to/attachment.zip',
    
    attachmentPaths: ['/storage/emulated/0/Android/data/com.india.ims.imsindia/files/Flogs/flog.txt'],
  );

  @override
  void initState() {
    super.initState();
    logExport();
    //requestPermission(_permissionGroup);
  }

  @override
  Widget build(BuildContext context) {
    final screenHeight = MediaQuery.of(context).size.height;
    final screenWidth = MediaQuery.of(context).size.width;
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
        brightness: Brightness.light,
        elevation: 0.0,
        centerTitle: false,
        titleSpacing: 0.0,
        backgroundColor: Colors.white,
        title: Text("Feedback",
        style: TextStyle(
              color: NeutralColors.dark_navy_blue,
              fontWeight: FontWeight.w500,
              fontFamily: "IBMPlexSans",
              fontStyle: FontStyle.normal,
              fontSize: 16 / 360 * screenWidth),
        ),
        leading: IconButton(
          icon: getSvgIcon.backSvgIcon,
          onPressed: () => Navigator.pop(context),
        ),),
        body: Container(
          //padding: const EdgeInsets.all(16.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
             Padding(
      padding: EdgeInsets.symmetric(horizontal: screenWidth/4),
      child: Row(
        children: <Widget>[
          _buildButton("Send Feedback", () async {
            await FlutterEmailSender.send(email);
          }),
        ],
      ),
    )
              //_buildRow4(),
            ],
          ),
        ),
      ),
    );
  }

  _build() {
    return Row(
      children: <Widget>[
        _buildButton("Send Feedback", () async {
          if(Platform.isAndroid){
          await FlutterEmailSender.send(email);
          }
        }),
      ],
    );
  }

  

  _buildRow4() {
    return Row(
      children: <Widget>[
        _buildButton("Log Event with StackTrace", () {
          FLog.error(
            text: "My log",
            dataLogType: "Zubair",
            className: "Home",
            exception: Exception("Exception and StackTrace"),
            stacktrace: StackTrace.current,
          );
        }),
        Padding(padding: EdgeInsets.symmetric(horizontal: 5.0)),
        _buildButton("Delete Logs by Filter (older then 10 seconds)", () {
          FLog.deleteAllLogsByFilter(filters: [
            Filter.lessThan(DBConstants.FIELD_TIME_IN_MILLIS,
                DateTime.now().millisecondsSinceEpoch - 1000 * 10)
          ]);
        }),
      ],
    );
  }

  _buildButton(String title, VoidCallback onPressed) {
    return Expanded(
      child: MaterialButton(
        onPressed: onPressed,
        child: Text(title),
        textColor: Colors.white,
        color: Colors.blueAccent,
      ),
    );
  }

  // general methods:-----------------------------------------------------------
  void logInfo() {
//    FLog.logThis(
//      className: "FeedbackPage",
//      methodName: "_buildRow1",
//      text: "Log text/descritption goes here",
//      type: LogLevel.INFO,
//      dataLogType: DataLogType.DEVICE.toString(),
//    );

    final LogLevel _newLogLevel = null;
    FLog.getDefaultConfigurations()..activeLogLevel = _newLogLevel;
    FLog.info(
        text:
            'LogLevel set to: ${FLog.getDefaultConfigurations().activeLogLevel}');
  }

  void logException() {
    try {
      var result = 9 ~/ 0;
      print(result);
    } on Exception catch (exception) {
      FLog.error(
        text: "Exception text/descritption goes here",
        dataLogType: "Exception (the type could be anything)",
        className: "Home",
        exception: exception,
      );
    }
  }

  void logError() {
    try {
      var string = "Zubair";
      var index = string[-1];
    } on Error catch (error) {
      FLog.error(
        text: "Error text/descritption goes here",
        dataLogType: "Error (the type could be anything)",
        className: "Home",
        //exception: error,
      );
    }
  }

  void logWarning() {
    FLog.warning(
      className: "FeedbackPage",
      methodName: "_buildRow1",
      text: "Log text/descritption goes here",
      dataLogType: "Warning (the type could be anything)",
    );
  }

  void logTrace() {
    FLog.trace(
      className: "FeedbackPage",
      methodName: "_buildRow1",
      text: "Log text/descritption goes here",
      dataLogType: "Trace (the type could be anything)",
    );
  }

  void logExport(){
    FLog.exportLogs();
  }

  //permission methods:---------------------------------------------------------
  // Future<void> requestPermission(PermissionGroup permission) async {
  //   final List<PermissionGroup> permissions = <PermissionGroup>[permission];
  //   final Map<PermissionGroup, PermissionStatus> permissionRequestResult =
  //       await PermissionHandler().requestPermissions(permissions);
  // }
}
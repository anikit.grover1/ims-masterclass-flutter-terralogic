import 'package:draggable_scrollbar/draggable_scrollbar.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';



class DraggableScrollBarDemo extends StatefulWidget {
  DraggableScrollBarDemo({Key key,this.controller,this.child,this.heightScrollThumb,this.widthScrollThumb,this.colorScrollThumb,this.marginScrollThumb}) : super(key: key);
  final ScrollController controller;
  final Widget child;
  final double heightScrollThumb;
  final double widthScrollThumb;
  final Color colorScrollThumb;
  final EdgeInsets marginScrollThumb;

  @override
  DraggableScrollBarDemoState createState() => DraggableScrollBarDemoState();
}

class DraggableScrollBarDemoState extends State<DraggableScrollBarDemo> {
  @override
  Widget build(BuildContext context) {
    return DraggableScrollbar(
      controller: widget.controller,
      child:widget.child,
      heightScrollThumb: widget.heightScrollThumb,
      backgroundColor: widget.colorScrollThumb,
      scrollThumbBuilder: (
          Color backgroundColor,
          Animation<double> thumbAnimation,
          Animation<double> labelAnimation,
          double height, {
            Text labelText,
            BoxConstraints labelConstraints,
          }) {
        return FadeTransition(
          opacity: thumbAnimation,
          child: Container(
            margin: widget.marginScrollThumb,
            height: height,
            width: widget.widthScrollThumb,
            color: backgroundColor,
          ),
        );
      },
    );
  }
}




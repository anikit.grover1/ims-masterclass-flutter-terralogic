import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
//import 'package:flutter_typeahead/flutter_typeahead.dart';
import 'package:imsindia/api/bloc/services.dart';
import 'package:imsindia/components/feedback_page.dart';
import 'package:imsindia/utils/Url.dart';
import 'package:imsindia/utils/colors.dart';
import 'package:imsindia/utils/global.dart' as global;
import 'package:imsindia/utils/home_svg_icons.dart';
import 'package:imsindia/views/Analytics/analytics_home.dart';
import 'package:imsindia/views/Arun/IMSSharedPref.dart';
// import 'package:imsindia/views/Arun/Samples/SampleApp/GoogleLoginScreen.dart';
// import 'package:imsindia/views/Arun/Samples/SampleApp/SplashScreen.dart';
import 'package:imsindia/views/Arun/aboutims/aboutims.dart';
import 'package:imsindia/views/Arun/blogs/Blogs.dart';
import 'package:imsindia/views/Arun/faq/FAQ.dart';
import 'package:imsindia/views/Arun/ims_utility.dart';
import 'package:imsindia/views/Arun/myPLAN/myPLAN_ErrorScreen.dart';
import 'package:imsindia/views/Arun/myPLAN/myPLAN_home_Screen.dart';
import 'package:imsindia/views/Arun/profile/Profile.dart';
import 'package:imsindia/views/Arun/search/search_widget.dart';
import 'package:imsindia/views/Arun/settings/Settings.dart';
import 'package:imsindia/views/Discussion/discussion_home.dart';
import 'package:imsindia/views/Emaximiser/emaximizer_home_screen.dart';
import 'package:imsindia/views/GDPI/GDPI_home_screen.dart';
import 'package:imsindia/views/GDPI/Gdpi_HomeScreen.dart';
import 'package:imsindia/views/GK_Zone/gk_home_screen.dart';
import 'package:imsindia/views/book_a_test_screens/book_test_homepage.dart';
import 'package:imsindia/views/book_a_test_screens/book_test_homepage_backup.dart';
import 'package:imsindia/views/calendar/calendar_home_screen.dart';
import 'package:imsindia/views/channel/channel_home.dart';
import 'package:imsindia/views/event_booking_pages/event_booking_widget.dart';
import 'package:imsindia/views/home_pages/home_widget.dart';
import 'package:imsindia/views/leaderboard/leaderboard_screen.dart';
import 'package:imsindia/views/practice_pages/practice_home_screen.dart';
import 'package:imsindia/views/prepare_pages/prepare_home_screen.dart';
import 'package:imsindia/views/splash_screen/splash_screen.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:imsindia/views/home_pages/nav_bar_Dropdown.dart';
import 'package:fluttertoast/fluttertoast.dart';

List<String> courses = [];
List<dynamic> data = [];
var loginId;
bool buttonClickOnlyOnce = false;
bool loadForMyPlan = false;

class CourseService {

  CourseData() async {
    //  await  ApiService().getAPI(URL.GET_USER_COURSE_NAMES, global.headersWithAuthorizationKey).then((result) {
    //     for (int i = 0; i < result[1]['data'].length; i++) {
    //       courses.add(result[1]['data'][i]['courseName']);
    //       data.add(result[1]['data'][i]['id']);
    //     }
    //     //  print(result[1]['data'][i]['courseName']);}
    //   });
    SharedPreferences prefs = await SharedPreferences.getInstance();
    courses = prefs.getStringList("usercourseName");
    data = prefs.getStringList("usercourseId");

    return courses;
  }

  static Future<List<String>> getSuggestions() async {

    //await CourseService().CourseData();
    List<String> matches = new List();
    matches.addAll(await CourseService().CourseData());
    //  matches.retainWhere((s) => s.toLowerCase().contains(query.toLowerCase()));
    return matches;
  }
}

class NavigationDrawer extends StatefulWidget {
  @override
  _NavigationDrawerState createState() => _NavigationDrawerState();
}

class _NavigationDrawerState extends State<NavigationDrawer> {

  Connectivity connectivity = Connectivity();

  ///add studen my plan  variable///
  var userpins;


  var dataaddstudent;
  var imsCourseId;
  var bookingsavailabledata;


  SharedPreferences prefs;
  bool debugmodeFlag = false;
  String courseId;
  List<String> mylist = [];
  List<String> myidlist = [];
  List<String> mydescList = [];
  List<String> staticlist = [
    "Prepare",
    //"Prepare - Web",
    //"Prepare - html",
    "E-maximiser",
    // "E-maximiser - Web",
    "Practice",
    "GK-Zone",
    "Channels",
    "Analyse",
    "Discussions",
    "Leaderboard",
    "GD-PI",
    "Blogs",
    "Event Booking",
    loadForMyPlan?"Loading":"myPLAN",
    //"Sample API Loading"
  ];
  String labelValue = "";
  String menuId = "";
  String menuDesc = "";
  String checkInternetStatusFlag;
  void onViewProfilePressed(BuildContext context) => Navigator.push(
      context, MaterialPageRoute(builder: (context) => Search()));

  final TextEditingController _courseDetails = new TextEditingController();

  void showMessage() {
    Fluttertoast.showToast(
      msg: "Please connect to internet and select a course",
      toastLength: Toast.LENGTH_SHORT,
      gravity: ToastGravity.BOTTOM,
      timeInSecForIosWeb: 1,
      backgroundColor: PrimaryColors.azure_Dark,
      textColor: Colors.white,
      fontSize: 14.0,
    );
  }

  void setLoginStatusFalse() async {
    prefs = await SharedPreferences.getInstance();
    prefs.setBool("LoginStatus", false);
    prefs.setString("courseId", null);
    prefs.setStringList('mylist', null);
    prefs.setStringList('myidlist', null);
    prefs.setStringList('mydesclist', null);
    prefs.setString("userName", '');
    prefs.setString("loginAccessToken", null);
    prefs.setString(ImsStrings.sCurrentUserName, null);
    prefs.setString(ImsStrings.sCurrentUserId, null);
    prefs.setBool('limitCellularDataUsage', null); //Settings Mobile Data Limitation Flag
    //  prefs.clear();

  }

  void getValuesSF() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    if(prefs.getString('courseName')!=null){
      String gotValue = prefs.getString('courseName');
      setState(() {
        labelValue = gotValue;
      });
    }else{
      setState(() {
        labelValue = "Select Course";
      });
    }

  }

  void sharedMethod() async{
    print('sharedMethod');
    print(_courseDetails.text);
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('courseName', _courseDetails.text);
    prefs.setString('courseId', courseId);
  }

  void getUserCourse() async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    loginId = prefs.getString(ImsStrings.sCurrentUserLoginId);
  }

  void getMenus() async {
    prefs = await SharedPreferences.getInstance();
    mylist = prefs.getStringList('mylist');
    setState(() {
      this.mylist = mylist;
    });
    print("====>" + mylist.toString());
  }

  void getMenuIds() async {
    prefs = await SharedPreferences.getInstance();
    myidlist = prefs.getStringList('myidlist');
    setState(() {
      this.myidlist = myidlist;
    });
    print("====>" + myidlist.toString());
  }

  void getMenuDescs() async {
    prefs = await SharedPreferences.getInstance();
    mydescList= prefs.getStringList('mydesclist');
    setState(() {
      this.mydescList = mydescList;
    });
    print("====>" + mydescList.toString());
  }

  void setIds(String id) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setString('moduleId', id);
  }

  void setDesc(String desc) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('moduleDesc', desc);
  }

  void finishedLoad() async {
    bool flag = false;
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setBool("flagCheck",flag);
    print(flag.toString() + "once pushed should be false");
  }

  Future checkInternet() async {
    var connectivityResult = await (Connectivity().checkConnectivity());
    checkInternetStatusFlag = connectivityResult.toString();
    return checkInternetStatusFlag;
  }

  enableLoggingStudent() async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var flag = prefs.getString(ImsStrings.sCurrentUserName);
    //print("Flag"+flag.toString());
    if(flag.toLowerCase() == "IMS0000507938".toLowerCase()||flag.toLowerCase() == "IMS0000627040".toLowerCase()){
      setState(() {
        debugmodeFlag = true;
      });
      //print("debugmodeFlag"+debugmodeFlag.toString());

    }
    else{
      setState(() {
        debugmodeFlag = false;
      });
      //print("debugmodeFlag"+debugmodeFlag.toString());
    }
  }



  /////shared preference for ims pin//
  void StudentLoginImsPin()async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    userpins = prefs.getString(ImsStrings.sCurrentUserId);
  }


  @override
  void initState() {
    getValuesSF();
    super.initState();
    StudentLoginImsPin();
    print('******************initState');
    // addstudent();
    getMenus();
    getMenuIds();
    getMenuDescs();
    getUserCourse();
    //debugmodeFlag = global.checkdebugmode; //will be true only when run debug mode
    //debugmodeFlag = true; //to enable feedback in both release and debug mode (Testing)
    enableLoggingStudent();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    print('******************didChangeDependencies');
    //getMenus();
    // setState(() {
    //    getMenus();
    // });
  }
  ///api for add student on myPlan////
  void addStudent() async{
    Map<String, dynamic> potsadd_student;
    potsadd_student = {
      "studentImsPin": userpins//studentImsPin //"99a77771"
    };
    print("userpins........................."+userpins);
    var _headers = {"Content-Type": "application/json"};
    ApiService().postAPITerr(URL.myPLAN_API_ADD_STUDENT, potsadd_student, _headers).then((result) {
      print("resulktttttttttttt");
      print(result);
      print("url");
      print(URL.myPLAN_API_ADD_STUDENT);
      print(result[0]['success']);
      if (result[0]['success']==true){
        print("here");
        dataaddstudent = result[0]['data'];
        print(result);
        print("-------------------------------print post data of students");
        print(URL.myPLAN_API_ADD_STUDENT);
        print(potsadd_student);
        print(result);
        getBookingsAvailable();
      }else{
        //  ErrorScreen();
        print("error handling");
      }
    });
  }


  //booking available//
  getBookingsAvailable() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    imsCourseId = prefs.getString("courseId");

    // var result = await ApiService().getAPITerr('https://getProjectList');
    Map<String, dynamic> postData;
    postData = {
      "studentImsPin":userpins,
      "myImsCourseId":imsCourseId,
    };
    final result = await ApiService().postAPITerr(URL.myPLAN_API_BOOKINGS_AVAILABLE, postData, global.headers);
    print("dateeeeeeeeeeeeeeeeeeeeeeee");
    print(postData.toString() + result.toString());
    print("dateeeeeeeeeeeeeeeeeeeeeeee");
    if (result[0]['success']==true){
      setState(() {
        bookingsavailabledata=result[0]['data'];
        buttonClickOnlyOnce = false;
        loadForMyPlan=false;
        if(bookingsavailabledata[0]['totalSlots']==0){
          Navigator.push(context,
              MaterialPageRoute(
                  builder: (context) =>
                      myPlanErrorScreen()));
        }else{
          Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) =>
                    myPLANHome(false,bookingsavailabledata[0]['availableSlots'],bookingsavailabledata[0]['slotsUsed']),
              ));
        }

        print(result);
        print("dataaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa");
      });
    }
    else{
      if(result[0].containsKey("message")){
        buttonClickOnlyOnce = false;
        loadForMyPlan=false;
        if(result[0]['message']=='No Record found'){
          Navigator.push(context,
              MaterialPageRoute(
                  builder: (context) =>
                      myPlanErrorScreen()));
        }else if(result[0]['message']=='available bookings are Empty'){
          Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) =>
                    myPLANHome(false,0,1),
              ));
        }else{
          print("Error Handling");
        }
      }else{
        print("Error Handling");
      }

    }
  }


  @override
  Widget build(BuildContext context) {
    final screenHeight = MediaQuery.of(context).size.height;
    final screenWidth = MediaQuery.of(context).size.width;
    setState(() => context = context);

    return Container(
      margin: EdgeInsets.only(right: 40 * screenWidth / 360),
      color: Colors.white,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [

          //User Profile Picture, Name and View profile Button
          Container(
            height: 60 / 720 * screenHeight,
            margin: EdgeInsets.only(
              top: 60 / 720 * screenHeight,
              left: 20 / 360 * screenWidth,
              bottom: 10 / 720 * screenHeight,
            ),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                Align(
                  alignment: Alignment.topLeft,
                  child: CircleAvatar(
                    backgroundImage: NetworkImage("https://secure.gravatar.com/avatar/?s=96&d=mm"),
                    radius: 60 / 720 * screenHeight / 2,
                    backgroundColor: Colors.white,
                    //width: 51 / 360 * screenWidth,
                    //  height: 51 / 720 * screenHeight,

                    child: GestureDetector(
                      onTap: () {
                        Navigator.of(context).pop();
                        // Navigator.push(
                        //     context,
                        //     MaterialPageRoute(
                        //       builder: (context) => Profile(),
                        //     ));
                      },
                      // child: Image.asset(
                      //   "assets/images/profileImage_placeholder.png",
                      //   fit: BoxFit.contain,
                      // ),
                    ),
                  ),
                ),
                Align(
                  alignment: Alignment.centerLeft,
                  child: Container(

                    margin: EdgeInsets.only(left: 13 / 360 * screenWidth),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          child: Text(
                            loginId != null ? loginId.toString().toUpperCase() : "Username",
                            style: TextStyle(
                              color: NeutralColors.black,
                              fontSize: 18 / 720 * screenHeight,
                              fontFamily: "IBMPlexSans",
                              fontWeight: FontWeight.w700,
                            ),
                            textAlign: TextAlign.left,
                          ),
                        ),
                        // Container(
                        //   child: InkWell(
                        //     onTap: () {
                        //       Navigator.of(context).pop();
                        //       // Navigator.push(context,MaterialPageRoute(
                        //       //       builder: (context) => Profile(),
                        //       //     ));
                        //     },
                        //     child: Text(
                        //       "View Profile",
                        //       style: TextStyle(
                        //         fontSize: 14 / 360 * screenWidth,
                        //         fontFamily: "IBMPlexSans",
                        //         color: PrimaryColors.azure_Dark,
                        //       ),
                        //       textAlign: TextAlign.left,
                        //     ),
                        //   ),
                        // ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),

          //Course Selection DropDown
          Container(
              margin: EdgeInsets.symmetric(horizontal: 20.0 / 360 * screenWidth),
              child:
              DropDownFormField(
                // selectedVal:_courseDetails.text ,
                getImmediateSuggestions: true,
                textFieldConfiguration: TextFieldConfiguration(
                    controller: _courseDetails,
                    label: labelValue),
                suggestionsBoxDecoration: SuggestionsBoxDecoration(
                    dropDownHeight: 150,
                    borderRadius: new BorderRadius.circular(5.0),
                    color: Colors.white),
                suggestionsCallback: (pattern) {
                  return CourseService.getSuggestions();
                },
                itemBuilder: (context, suggestion) {
                  return ListTile(
                    dense: true,
                    contentPadding: EdgeInsets.only(left: 10 / 360 * screenWidth),
                    title: Text(
                      suggestion,
                      style: TextStyle(
                        fontFamily: "IBMPlexSans",
                        fontWeight: FontWeight.w500,
                        fontSize: (13 / 720) * screenHeight,
                        color: NeutralColors.bluey_grey,
                        textBaseline: TextBaseline.alphabetic,
                        letterSpacing: 0.0,
                        inherit: false,

                      ),
                    ),
                  );
                },
                hideOnLoading: true,
                debounceDuration: Duration(milliseconds: 100),
                transitionBuilder: (context, suggestionsBox, controller) {
                  return suggestionsBox;
                },
                onSuggestionSelected: (suggestion) {
                  print("object");
                  checkInternet().then((flag){


                    if (flag == 'ConnectivityResult.none') {
                      print('connection lost');
                      showMessage();
                    } else {
                      global.getStatus.then((flag){
                        if(flag == true){
                          for (int i = 0; i < courses.length; i++) {
                            if (courses[i] == suggestion) {
                              courseId = data[i];
                            }
                          }
                          print(suggestion);
                          setState(() {
                            _courseDetails.text = suggestion;
                            labelValue = suggestion;
                            sharedMethod();
                            global.getStatus;
                            finishedLoad();
                            Navigator.of(context).pop();

                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (context) =>
                                      HomeWidget(),
                                ));
                          });
                        }

                      });
                    }


                  });



                  // setState(() {});
                },
              )
          ),

          //
          Expanded(
            child: SingleChildScrollView(
              child: Container(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
//                       Container(
//                         margin: EdgeInsets.only(
//                             top: 20 / 720 * screenHeight,
//                             left: 20 / 360 * screenWidth),
//                         child: Column(
//                           crossAxisAlignment: CrossAxisAlignment.stretch,
//                           children: [
// //                             Container(
// //                               height: 40 / 720 * screenHeight,
// //                               // margin: EdgeInsets.only(right: 30/360*screenWidth),
// //                               child: InkWell(
// //                                 onTap: () {
// // //                                  Navigator.push(context, MaterialPageRoute(
// // //                                    builder: (context)=> null(),
// // //                                  ));
// //                                 },
// //                                 child: Column(
// //                                   crossAxisAlignment:
// //                                       CrossAxisAlignment.stretch,
// //                                   children: [
// //                                     Align(
// //                                       alignment: Alignment.topLeft,
// //                                       child: GestureDetector(
// //                                         onTap: () {
// //                                           print('Arun clicked on Meet a mentor');
// //                                           Navigator.of(context).pop();
// //                                           Navigator.push(
// //                                               context,
// //                                               MaterialPageRoute(
// //                                                 builder: (context) =>
// //                                                     MyMentorHome(false),
// //                                               ));
// //                                         },
// //                                         child: Text(
// //                                           "Meet a Mentor",
// //                                           style: TextStyle(
// //                                             color:
// //                                                 NeutralColors.dark_navy_blue,
// //                                             fontSize: 14 / 720 * screenHeight,
// //                                             fontFamily: "IBMPlexSans",
// //                                           ),
// //                                           textAlign: TextAlign.left,
// //                                         ),
// //                                       ),
// //                                     ),
// //                                     Spacer(),
// //                                     Container(
// //                                       height: 1 / 720 * screenHeight,
// //                                       margin: EdgeInsets.only(
// //                                           bottom: 1 / 720 * screenHeight,
// //                                           right: 20 / 360 * screenWidth),
// //                                       decoration: BoxDecoration(
// //                                         color:
// //                                             NeutralColors.ice_blue,
// //                                       ),
// //                                       child: Container(),
// //                                     ),
// //                                   ],
// //                                 ),
// //                               ),
// //                             ),
//                             // Container(
//                             //   height: 40 / 720 * screenHeight,
//                             //   margin:
//                             //       EdgeInsets.only(top: 16 / 720 * screenHeight),
//                             //   child: InkWell(
//                             //     onTap: () {
//                             //       Navigator.of(context).pop();
//                             //       Navigator.push(
//                             //           context,
//                             //           MaterialPageRoute(
//                             //             builder: (context) =>
//                             //                 MyMentorHome(true),
//                             //           ));
//                             //     },
//                             //     child: Column(
//                             //       crossAxisAlignment:
//                             //           CrossAxisAlignment.stretch,
//                             //       children: [
//                             //         Align(
//                             //           alignment: Alignment.topLeft,
//                             //           child: Text(
//                             //             "Write to a Mentor",
//                             //             style: TextStyle(
//                             //               color: NeutralColors.dark_navy_blue,
//                             //               fontSize: 14 / 720 * screenHeight,
//                             //               fontFamily: "IBMPlexSans",
//                             //             ),
//                             //             textAlign: TextAlign.left,
//                             //           ),
//                             //         ),
//                             //         Spacer(),
//                             //         Container(
//                             //           height: 1 / 720 * screenHeight,
//                             //           margin: EdgeInsets.only(
//                             //               bottom: 1 / 720 * screenHeight,
//                             //               right: 20 / 360 * screenWidth),
//                             //           decoration: BoxDecoration(
//                             //             color:
//                             //                 NeutralColors.ice_blue,
//                             //           ),
//                             //           child: Container(),
//                             //         ),
//                             //       ],
//                             //     ),
//                             //   ),
//                             // ),
//                             // Container(
//                             //   height: 40 / 720 * screenHeight,
//                             //   margin:
//                             //       EdgeInsets.only(top: 16 / 720 * screenHeight),
//                             //   child: InkWell(
//                             //     onTap: () {
//                             //       Navigator.of(context).pop();
//                             //       Navigator.push(
//                             //           context,
//                             //           MaterialPageRoute(
//                             //             builder: (context) => AnalyticsHomeScreen(),
//                             //           ));
//                             //     },
//                             //     child: Column(
//                             //       crossAxisAlignment:
//                             //           CrossAxisAlignment.stretch,
//                             //       children: [
//                             //         Align(
//                             //           alignment: Alignment.topLeft,
//                             //           child: Text(
//                             //             "Analyse",
//                             //             style: TextStyle(
//                             //               color: NeutralColors.dark_navy_blue,
//                             //               fontSize: 14 / 720 * screenHeight,
//                             //               fontFamily: "IBMPlexSans",
//                             //             ),
//                             //             textAlign: TextAlign.left,
//                             //           ),
//                             //         ),
//                             //         Spacer(),
//                             //         Container(
//                             //           height: 1 / 720 * screenHeight,
//                             //           margin: EdgeInsets.only(
//                             //               bottom: 1 / 720 * screenHeight,
//                             //               right: 20 / 360 * screenWidth),
//                             //           decoration: BoxDecoration(
//                             //             color:
//                             //                 NeutralColors.ice_blue,
//                             //           ),
//                             //           child: Container(),
//                             //         ),
//                             //       ],
//                             //     ),
//                             //   ),
//                             // ),

//                             Container(
//                               height: 40 / 720 * screenHeight,
//                               margin:
//                                   EdgeInsets.only(top: 16 / 720 * screenHeight),
//                               child: InkWell(
//                                 onTap: () {
// //                                  Navigator.of(context).pop();
// //                                  Navigator.push(
// //                                      context,
// //                                      MaterialPageRoute(
// //                                        builder: (context) => BookHomeScreenBackup(),
// //                                      ));
//                                 },
//                                 child: Column(
//                                   crossAxisAlignment:
//                                       CrossAxisAlignment.stretch,
//                                   children: [
//                                     Align(
//                                       alignment: Alignment.topLeft,
//                                       child: Text(
//                                         "Book Test Slot",
//                                         style: TextStyle(
//                                           color: NeutralColors.dark_navy_blue,
//                                           fontSize: 14 / 720 * screenHeight,
//                                           fontFamily: "IBMPlexSans",
//                                         ),
//                                         textAlign: TextAlign.left,
//                                       ),
//                                     ),
//                                     Spacer(),
//                                     Container(
//                                       height: 1 / 720 * screenHeight,
//                                       margin: EdgeInsets.only(
//                                           bottom: 1 / 720 * screenHeight,
//                                           right: 20 / 360 * screenWidth),
//                                       decoration: BoxDecoration(
//                                         color:
//                                             NeutralColors.ice_blue,
//                                       ),
//                                       child: Container(),
//                                     ),
//                                   ],
//                                 ),
//                               ),
//                             ),
//                             Container(
//                               height: 40 / 720 * screenHeight,
//                               margin:
//                                   EdgeInsets.only(top: 16 / 720 * screenHeight),
//                               child: InkWell(
//                                 onTap: () {
// //                                  Navigator.of(context).pop();
// //                                  Navigator.push(
// //                                      context,
// //                                      MaterialPageRoute(
// //                                        builder: (context) =>
// //                                            CalendarHomeScreen(),
// //                                      ));
//                                 },
//                                 child: Column(
//                                   crossAxisAlignment:
//                                       CrossAxisAlignment.stretch,
//                                   children: [
//                                     Align(
//                                       alignment: Alignment.topLeft,
//                                       child: Text(
//                                         "Calendar",
//                                         style: TextStyle(
//                                           color: NeutralColors.dark_navy_blue,
//                                           fontSize: 14 / 720 * screenHeight,
//                                           fontFamily: "IBMPlexSans",
//                                         ),
//                                         textAlign: TextAlign.left,
//                                       ),
//                                     ),
//                                     Spacer(),
//                                     Container(
//                                       height: 1 / 720 * screenHeight,
//                                       margin: EdgeInsets.only(
//                                           bottom: 1 / 720 * screenHeight,
//                                           right: 20 / 360 * screenWidth),
//                                       decoration: BoxDecoration(
//                                         color:
//                                             Colors.transparent,
//                                       ),
//                                       child: Container(),
//                                     ),
//                                   ],
//                                 ),
//                               ),
//                             ),
//                           ],
//                         ),
//                       ),
//                       Container(
//                         margin: EdgeInsets.only(
//                             top: 5 / 720 * screenHeight,
//                             left: 20 / 360 * screenWidth),
//                         child: Opacity(
//                           opacity: 0.1,
//                           child: Container(
//                             height: 4 / 720 * screenHeight,
//                             decoration: BoxDecoration(
//                               color: PrimaryColors.azure_Dark,
//                             ),
//                             child: Container(),
//                           ),
//                         ),
//                       ),

                    //List of Modules from Home Screen
                    mylist.length == 0
                        ? new Container(
                      // margin: EdgeInsets.only(
                      //     top: (200.0 / 720) * screenHeight),
                      // child: Center(child: CircularProgressIndicator())
                    )
                        : Container(
                        height: screenHeight * 62 / 720 *
                            //mylist.length
                            mylist.length,
                        margin: EdgeInsets.only(
                            top: 20 / 720 * screenHeight,
                            left: 20 / 360 * screenWidth),
                        child: ListView.builder(
                          padding: EdgeInsets.all(0),
                          physics: NeverScrollableScrollPhysics(),
                          itemCount:
                          mylist.length,
                          //staticlist.length,
                          itemBuilder: (context, index) {
                            return Container(
                              height: screenHeight * 62 / 720,
                              child: Column(
                                children: <Widget>[
                                  Align(
                                    alignment: Alignment.centerLeft,
                                    child: InkWell(
                                      onTap: () {

                                        global.getStatus.then((flag){
                                          if(flag == true){

                                            // Navigator.push(
                                            //   context,
                                            //   MaterialPageRoute(
                                            //       builder: (context) =>
                                            //           PrepareHomeScreen()),
                                            // );

                                            //if (mylist[index] == 'Prepare')
                                            if (mylist[index] == 'Prepare')
                                            {
                                              setState(() {
                                                menuId = myidlist[index];
                                                menuDesc = mydescList[index];
                                              });
                                              print("Menu ID" + menuId);
                                              print("Menu Desc" + menuDesc);
                                              setIds(menuId);
                                              setDesc(menuDesc);
                                              Navigator.of(context).pop();
                                              Navigator.push(
                                                  context,
                                                  MaterialPageRoute(
                                                    builder: (context) =>
                                                        PrepareHomeScreenHtml(),
                                                  ));
                                            }

                                            // if (staticlist[index] == 'Prepare - Web')
                                            // {
                                            //   Navigator.of(context).pop();
                                            //   Navigator.push(
                                            //       context,
                                            //       MaterialPageRoute(
                                            //         builder: (context) =>
                                            //             PrepareHomeScreenWeb(),
                                            //       )
                                            //       );
                                            // }
                                            // if (staticlist[index] == 'Prepare - html')
                                            // {
                                            //   Navigator.of(context).pop();
                                            //   Navigator.push(
                                            //       context,
                                            //       MaterialPageRoute(
                                            //         builder: (context) =>
                                            //             PrepareHomeScreenHtml(),
                                            //       ));
                                            // }
                                            if (mylist[index] == 'E-maximiser')
                                              //if (staticlist[index] == 'E-maximiser')
                                                {
                                              setState(() {
                                                menuId = myidlist[index];
                                                menuDesc = mydescList[index];
                                              });
                                              print("Menu ID" + menuId);
                                              print("Menu Desc" + menuDesc);
                                              setIds(menuId);
                                              setDesc(menuDesc);
                                              Navigator.of(context).pop();
                                              Navigator.push(
                                                  context,
                                                  MaterialPageRoute(
                                                    builder: (context) =>
                                                        EmaximiserHomeScreen(),
                                                  ));
                                            }

                                            // if (staticlist[index] == 'E-maximiser - Web')
                                            // {
                                            //   Navigator.of(context).pop();
                                            //   Navigator.push(
                                            //       context,
                                            //       MaterialPageRoute(
                                            //         builder: (context) =>
                                            //             EmaximiserHomeScreenWeb(),
                                            //       ));
                                            // }

                                            if (mylist[index] == 'Practice')
                                              //if (staticlist[index] == 'Practice')
                                                {
                                              setState(() {
                                                menuId = myidlist[index];
                                                menuDesc = mydescList[index];
                                              });
                                              print("Menu ID" + menuId);
                                              print("Menu Desc" + menuDesc);
                                              setIds(menuId);
                                              setDesc(menuDesc);
                                              Navigator.of(context).pop();
                                              Navigator.push(
                                                  context,
                                                  MaterialPageRoute(
                                                    builder: (context) =>
                                                        PracticeHomeScreen(),
                                                  ));
                                            }
                                             if (mylist[index] == 'GK-Zone')
                                             //if (staticlist[index] == 'GK-Zone')
                                             {
                                               // setState(() {
                                               //   menuId = myidlist[index];
                                               // });
                                               // print("Menu ID" + menuId);
                                               // setIds(menuId);
                                               Navigator.of(context).pop();
                                               Navigator.push(
                                                   context,
                                                   MaterialPageRoute(
                                                     builder: (context) =>
                                                         GKZoneHomePage(),
                                                   ));
                                             }
                                            if (mylist[index] == 'Channels')
                                              //if (staticlist[index] == 'Channels')
                                                {
                                              setState(() {
                                                menuId = myidlist[index];
                                                menuDesc = mydescList[index];
                                              });
                                              print("Menu ID" + menuId);
                                              print("Menu Desc" + menuDesc);
                                              setIds(menuId);
                                              setDesc(menuDesc);
//                                                Navigator.of(context).pop();
//                                                Navigator.push(
//                                                    context,
//                                                    MaterialPageRoute(
//                                                      builder: (context) =>
//                                                          ChannelHome(),
//                                                    ));
                                            }
                                            if (mylist[index] == 'Test')
                                              //if (staticlist[index] == 'Analyse')
                                                {
                                              setState(() {
                                                menuId = myidlist[index];
                                                menuDesc = mydescList[index];
                                              });
                                              print("Menu ID" + menuId);
                                              print("Menu Desc" + menuDesc);
                                              setIds(menuId);
                                              setDesc(menuDesc);
                                              Navigator.of(context).pop();
                                              Navigator.push(
                                                  context,
                                                  MaterialPageRoute(
                                                    builder: (context) =>
                                                        AnalyticsHomeScreen(),
                                                  ));
                                            }
                                            if (mylist[index] == 'myPLAN')
                                              //if (staticlist[index] == 'Analyse')
                                                {
                                              setState(() {
                                                menuId = myidlist[index];
                                                menuDesc = mydescList[index];
                                              });
                                              print("Menu ID" + menuId);
                                              print("Menu Desc" + menuDesc);
                                              setIds(menuId);
                                              setDesc(menuDesc);
                                              if (buttonClickOnlyOnce == false) {
                                                setState(() {
                                                  buttonClickOnlyOnce = true;
                                                  loadForMyPlan=true;
                                                });

                                                addStudent();
                                              }

                                            }



                                            //if (mylist[index] == 'Discussions')
                                            // if (staticlist[index] == 'Discussions')
                                            // {
                                            //   Navigator.of(context).pop();
                                            //   Navigator.push(
                                            //       context,
                                            //       MaterialPageRoute(
                                            //         builder: (context) =>
                                            //             DiscussionHome(),
                                            //       ));
                                            // }
                                            // //if (mylist[index] == 'Leaderboard')
                                            // if (staticlist[index] == 'Leaderboard')
                                            // {
                                            //   Navigator.of(context).pop();
                                            //   Navigator.push(
                                            //       context,
                                            //       MaterialPageRoute(
                                            //         builder: (context) =>
                                            //             LeaderboardScreen(),
                                            //       ));
                                            // }
                                            if (mylist[index] == 'GD-PI')
                                          //  if (staticlist[index] == 'GDPI')
                                            {
                                              setState(() {
                                                menuId = myidlist[index];
                                                menuDesc = mydescList[index];
                                              });
                                              print("Menu ID" + menuId);
                                              print("Menu Desc" + menuDesc);
                                              setIds(menuId);
                                              setDesc(menuDesc);
                                              Navigator.of(context).pop();
                                              Navigator.push(
                                                  context,
                                                  MaterialPageRoute(
                                                    builder: (context) =>
                                                        Gdpi_HomeScreen(),
                                                  ));
                                            }
                                            // //if (mylist[index] == 'Blogs')
                                            // if (staticlist[index] == 'Blogs')
                                            // {
                                            //   Navigator.of(context).pop();
                                            //   Navigator.push(
                                            //       context,
                                            //       MaterialPageRoute(
                                            //         builder: (context) =>
                                            //             Blogs(),
                                            //       ));
                                            // }
                                            // //if (mylist[index] == 'Event Booking')
                                            // if (staticlist[index] == 'Event Booking')
                                            // {
                                            //   Navigator.of(context).pop();
                                            //   Navigator.push(
                                            //       context,
                                            //       MaterialPageRoute(
                                            //         builder: (context) =>
                                            //             EventBookingWidget(),
                                            //       ));
                                            // }
                                            //  if (staticlist[index] == 'Sample API Loading')
                                            // {
                                            //   Navigator.of(context).pop();
                                            //   Navigator.push(
                                            //       context,
                                            //       MaterialPageRoute(
                                            //         builder: (context) =>
                                            //             GoogleLoginScreen(),
                                            //       ));
                                            // }


                                          }
                                        });


                                      },


                                      child: Container(
                                        width: screenWidth * 300 / 360,
                                        height: screenHeight * 45 / 720,
                                        child: Row(
                                          crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                          children: [
                                            Container(
                                                width: screenWidth *
                                                    22 /
                                                    360,
                                                height: screenHeight *
                                                    26 /
                                                    720,
                                                margin: EdgeInsets.only(
                                                    top: 2 /
                                                        720 *
                                                        screenHeight),
                                                child:
                                                Center(
                                                  child: new SvgPicture.asset(
                                                    //       staticlist[index] == 'Prepare - Web'
                                                    // //mylist[index] == 'Prepare'
                                                    //       ? getSvgIcon.prepare

                                                    //    : staticlist[index] == 'Prepare'
                                                    mylist[index] == 'Prepare'
                                                        ? getSvgIcon.prepare

                                                    //     : staticlist[index] == 'Prepare - html'
                                                    // //mylist[index] == 'Prepare'
                                                    //     ? getSvgIcon.prepare
                                                    // : staticlist[index] == 'E-maximiser - Web'
                                                    // //mylist[index] == 'E-maximiser'
                                                    //     ? getSvgIcon
                                                    //         .maximiser

                                                        : //staticlist[index] == 'E-maximiser'
                                                    mylist[index] == 'E-maximiser'
                                                        ? getSvgIcon
                                                        .maximiser
                                                        : //taticlist[index] == 'Practice'
                                                    mylist[index] == 'Practice'
                                                        ? getSvgIcon
                                                        .practice
                                                        : //staticlist[index] == 'GK-Zone'
                                                    mylist[index] == 'GK-Zone'
                                                        ? getSvgIcon
                                                        .gkzone
                                                        : //staticlist[index] == 'Channels'
                                                    mylist[index] == 'Channels'
                                                        ? getSvgIcon.chanel
                                                        : //staticlist[index] == 'Analyse'
                                                    mylist[index] == 'Test'
                                                        ? getSvgIcon.analysis
                                                        : //staticlist[index] == 'Discussions'
                                                    mylist[index] == 'Discussions'
                                                        ? getSvgIcon
                                                        .maximiser
                                                        : mylist[index] == 'Leaderboard'
                                                    //mylist[index] == 'Leaderboard'
                                                        ? getSvgIcon
                                                        .practice
                                                        : mylist[index] == 'GD-PI'
                                                    //mylist[index] == 'GDPI'
                                                        ? getSvgIcon
                                                        .gdpi
                                                        : mylist[index] == 'Blogs'
                                                    //mylist[index] == 'Blogs'
                                                        ? getSvgIcon.blogs
                                                        : //staticlist[index] == 'Event Booking'
                                                    mylist[index] == 'Event Booking'
                                                        ? getSvgIcon.chanel
                                                        : getSvgIcon.practice,
                                                    fit: BoxFit.contain,
                                                    //color: Colors.red,
                                                  ),
                                                )),
                                            Container(
                                              margin: EdgeInsets.only(
                                                  left: 14 /
                                                      360 *
                                                      screenWidth,
                                                  top: 4 /
                                                      720 *
                                                      screenHeight),
                                              child: Text(
                                                //mylist[index]
                                                mylist[index] == "Test" ? "Analytics" : mylist[index] == "Prepare" ? "Learn" : mylist[index] == "GK-Zone" ? "GK Zone" : mylist[index] == "Channels" ? "Channel" : ((mylist[index] == "E-maximiser") && (labelValue != "PGDBA"))  ? "e-Maximiser": ((mylist[index] == "E-maximiser") && (labelValue == "PGDBA")) ? "PGDBA Workshops": ((mylist[index] == "myPLAN") && (labelValue != "GDPI")) ?loadForMyPlan==true?"Loading":"myPLAN":((mylist[index] == "myPLAN") && (labelValue == "GDPI")) ?loadForMyPlan==true?"Loading":"PI Slot Booking":mylist[index],
                                                style: TextStyle(
                                                  color: NeutralColors.dark_navy_blue,
                                                  fontSize: 14 /
                                                      720 *
                                                      screenHeight,
                                                  fontFamily:
                                                  "IBMPlexSans",
                                                ),
                                                textAlign: TextAlign.left,
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                  ),


                                  index == mylist.length - 1
                                  //index == staticlist.length - 1
                                      ? Container(
                                    height: 1 / 720 * screenHeight,
                                  )
                                      : Container(
                                      height: 1 / 720 * screenHeight,
                                      margin: EdgeInsets.only(
                                          bottom:
                                          15 / 720 * screenHeight,
                                          right:
                                          20 / 360 * screenWidth),
                                      decoration: BoxDecoration(
                                        color: NeutralColors.ice_blue,
                                      )),
                                ],
                              ),
                            );
                          },
                        )
                    ),


                    Container(
                      margin: EdgeInsets.only(
                          top: 0 / 720 * screenHeight,
                          left: 20 / 360 * screenWidth),
                      child: Opacity(
                        opacity: 0.1,
                        child: Container(
                          height: 4 / 720 * screenHeight,
                          decoration: BoxDecoration(
                            color: PrimaryColors.azure_Dark,
                          ),
                          child: Container(),
                        ),
                      ),
                    ),

                    Align(
                      alignment: Alignment.topLeft,
                      child: Container(
                        width: 320 / 360 * screenWidth,
                        //height: 316 / 720 * screenHeight, // 4 Contents
                        height: debugmodeFlag ? 316 / 720 * screenHeight : 264 / 720 * screenHeight,
                        margin: EdgeInsets.only(
                            top: 20 / 720 * screenHeight,
                            left: 20 / 360 * screenWidth),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          children: [
                            Container(
                              height: 40 / 720 * screenHeight,
                              width: 300 / 360 * screenWidth,
                              margin: EdgeInsets.only(
                                  left: 1 / 360 * screenWidth,
                                  bottom: 10 / 720 * screenHeight),
                              child: InkWell(
                                onTap: () {

                                  global.getStatus.then((flag){
                                    if(flag == true){
                                      Navigator.of(context).pop();
                                      Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                            builder: (context) => AboutIms(),
                                          ));
                                    }
                                  });

                                },
                                child: Row(
                                  crossAxisAlignment:
                                  CrossAxisAlignment.stretch,
                                  children: [
                                    Align(
                                      alignment: Alignment.center,
                                      child: Text(
                                        "Contact Us",
                                        style: TextStyle(
                                          color:
                                          NeutralColors.dark_navy_blue,
                                          fontSize: 14 / 720 * screenHeight,
                                          fontFamily: "IBMPlexSans",
                                        ),
                                        textAlign: TextAlign.left,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            Container(
                              height: 1 / 720 * screenHeight,
                              margin: EdgeInsets.only(
                                  right: 20 / 360 * screenWidth,
                                  bottom: 15 / 720 * screenHeight),
                              decoration: BoxDecoration(
                                color: NeutralColors.ice_blue,
                              ),
                              child: Container(),
                            ),
                            Container(
                              height: 40 / 720 * screenHeight,
                              width: 300 / 360 * screenWidth,
                              margin: EdgeInsets.only(
                                  left: 1 / 360 * screenWidth,
                                  bottom: 10 / 720 * screenHeight),
                              child: InkWell(
                                onTap: () {



                                  global.getStatus.then((flag){
                                    if(flag == true){
                                      bool monVal;
                                      IMSSharedPref().get_limitCellularDataUsage(
                                      ).then((monVal){
                                        Navigator.of(context).pop();
                                        Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                              builder: (context) => Settings(monVal),
                                            ));
                                      });
                                      print(monVal);
                                      print("objecttttttttttt");

                                    }
                                  });

                                },
                                child: Row(
                                  crossAxisAlignment:
                                  CrossAxisAlignment.stretch,
                                  children: [
                                    Align(
                                      alignment: Alignment.center,
                                      child: Text(
                                        "Settings",
                                        style: TextStyle(
                                          color:
                                          NeutralColors.dark_navy_blue,
                                          fontSize: 14 / 720 * screenHeight,
                                          fontFamily: "IBMPlexSans",
                                        ),
                                        textAlign: TextAlign.left,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            Container(
                              height: 1 / 720 * screenHeight,
                              margin: EdgeInsets.only(
                                  right: 20 / 360 * screenWidth,
                                  bottom: 15 / 720 * screenHeight),
                              decoration: BoxDecoration(
                                color: NeutralColors.ice_blue,
                              ),
                              child: Container(),
                            ),
                            Container(
                              height: 40 / 720 * screenHeight,
                              width: 300 / 360 * screenWidth,
                              margin: EdgeInsets.only(
                                  left: 1 / 360 * screenWidth,
                                  bottom: 10 / 720 * screenHeight),
                              child: InkWell(
                                onTap: () {
                                  global.getStatus.then((flag){
                                    if(flag == true){
                                      Navigator.of(context).pop();
                                      Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                            builder: (context) => Blogs(),
                                          ));
                                    }
                                  });
                                },
                                child: Row(
                                  crossAxisAlignment:
                                  CrossAxisAlignment.stretch,
                                  children: [
                                    Align(
                                      alignment: Alignment.center,
                                      child: Text(
                                        "Blogs",
                                        style: TextStyle(
                                          color:
                                          NeutralColors.dark_navy_blue,
                                          fontSize: 14 / 720 * screenHeight,
                                          fontFamily: "IBMPlexSans",
                                        ),
                                        textAlign: TextAlign.left,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            Container(
                              height: 1 / 720 * screenHeight,
                              margin: EdgeInsets.only(
                                  right: 20 / 360 * screenWidth,
                                  bottom: 15 / 720 * screenHeight),
                              decoration: BoxDecoration(
                                color: NeutralColors.ice_blue,
                              ),
                              child: Container(),
                            ),
                            // Container(
                            //   height: 40 / 720 * screenHeight,
                            //   width: 300 / 360 * screenWidth,
                            //   margin: EdgeInsets.only(
                            //       left: 1 / 360 * screenWidth,
                            //       bottom: 10 / 720 * screenHeight),
                            //   child: InkWell(
                            //     onTap: () {
                            //       Navigator.of(context).pop();
                            //       Navigator.push(
                            //           context,
                            //           MaterialPageRoute(
                            //             builder: (context) => FAQ(),
                            //           ));
                            //     },
                            //     child: Row(
                            //       crossAxisAlignment:
                            //           CrossAxisAlignment.stretch,
                            //       children: [
                            //         Align(
                            //           alignment: Alignment.center,
                            //           child: Text(
                            //             "FAQ",
                            //             style: TextStyle(
                            //               color:
                            //                   NeutralColors.dark_navy_blue,
                            //               fontSize: 14 / 720 * screenHeight,
                            //               fontFamily: "IBMPlexSans",
                            //             ),
                            //             textAlign: TextAlign.left,
                            //           ),
                            //         ),
                            //       ],
                            //     ),
                            //   ),
                            // ),
                            // Container(
                            //   height: 1 / 720 * screenHeight,
                            //   margin: EdgeInsets.only(
                            //       right: 20 / 360 * screenWidth,
                            //       bottom: 15 / 720 * screenHeight),
                            //   decoration: BoxDecoration(
                            //     color: NeutralColors.ice_blue,
                            //   ),
                            //   child: Container(),
                            // ),

                            debugmodeFlag ?
                            Container(
                              height: 40 / 720 * screenHeight,
                              width: 300 / 360 * screenWidth,
                              margin: EdgeInsets.only(
                                  left: 1 / 360 * screenWidth,
                                  bottom: 10 / 720 * screenHeight),
                              child: InkWell(
                                onTap: () {
                                  Navigator.of(context).pop();
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                        builder: (context) => FeedbackPage(),
                                      ));
                                },
                                child: Row(
                                  crossAxisAlignment:
                                  CrossAxisAlignment.stretch,
                                  children: [
                                    Align(
                                      alignment: Alignment.center,
                                      child: Text(
                                        "Feedback",
                                        style: TextStyle(
                                          color:
                                          NeutralColors.dark_navy_blue,
                                          fontSize: 14 / 720 * screenHeight,
                                          fontFamily: "IBMPlexSans",
                                        ),
                                        textAlign: TextAlign.left,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ):Container(),
                            debugmodeFlag ?Container(
                              height: 1 / 720 * screenHeight,
                              margin: EdgeInsets.only(
                                  right: 20 / 360 * screenWidth,
                                  bottom: 15 / 720 * screenHeight),
                              decoration: BoxDecoration(
                                color: NeutralColors.ice_blue,
                              ),
                              child: Container(),
                            ):Container(),

                            Container(
                              height: 40 / 720 * screenHeight,
                              width: 300 / 360 * screenWidth,
                              margin: EdgeInsets.only(
                                  left: 1 / 360 * screenWidth,
                                  bottom: 10 / 720 * screenHeight),
                              child: Row(
                                crossAxisAlignment:
                                CrossAxisAlignment.stretch,
                                children: [
                                  GestureDetector(
                                    onTap: () {

                                      global.getStatus.then((flag){
                                        if(flag == true){
                                          setLoginStatusFalse();
                                          Navigator.of(context).pop();
                                          Navigator.push(
                                              context,
                                              MaterialPageRoute(
                                                builder: (context) =>
                                                    SplashScreen(),
                                              ));
                                        }
                                      });

                                    },
                                    child: Align(
                                      alignment: Alignment.center,
                                      child: Text(
                                        "Signout",
                                        style: TextStyle(
                                          color:
                                          NeutralColors.dark_navy_blue,
                                          fontSize: 14 / 720 * screenHeight,
                                          fontFamily: "IBMPlexSans",
                                          fontWeight: FontWeight.w400,
                                        ),
                                        textAlign: TextAlign.left,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

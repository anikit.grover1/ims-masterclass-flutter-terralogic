import 'dart:async';
import 'dart:convert';

import 'package:connectivity/connectivity.dart';
import 'package:http/http.dart' as http;
import 'package:imsindia/utils/Url.dart';

class ApiService {
  // [Arun classes like this should be singleton classes]
  //==> Praveen: From Now we will use singleton classes for this purpose
  static final ApiService _singleton = ApiService._internal();

  factory ApiService() {
    return _singleton;
  }

  ApiService._internal();
  Future<dynamic> forgotPassword(Map dataBody) async {
    //check whether app is connected to internet or Not
    var connectivityResult = await (Connectivity().checkConnectivity());
    if (connectivityResult.toString() != 'ConnectivityResult.none') {
      var response = await http.post(Uri.parse(URL.ForgotPassword),
          headers: {"Accept": "application/json"},
          body: json.encode(dataBody),
          encoding: Encoding.getByName("utf-8"));
      if (response.body == '' || response == null) {
        return ['No Data'];
      } else {
        Map<String, dynamic> data = jsonDecode(response.body);
        if (data['mtype'] == 'success') {
          return [data['message'], data];
        } else {
          return [data['message']];
        }
      }
    } else {
      return ['No Internet Connection'];
    }
  }

  Future<dynamic> Details_Using_Email_Or_Pin(Map dataBody) async {
    //check whether app is connected to internet or Not
    var connectivityResult = await (Connectivity().checkConnectivity());
    if (connectivityResult.toString() != 'ConnectivityResult.none') {
      var response = await http.post(Uri.parse(URL.Details_Using_Email_or_Pin),
          headers: {"Accept": "application/json"},
          body: json.encode(dataBody),
          encoding: Encoding.getByName("utf-8"));
      if (response.body == '' || response == null) {
        return ['No Data'];
      } else {
        Map<String, dynamic> data = jsonDecode(response.body);
        if (data['mtype'] == 'success') {
          return [data['mtype'], data];
        } else {
          return ['User Details Not Found'];
        }
      }
    } else {
      return ['No Internet Connection'];
    }
  }

  /// ims api structure ...................
  Future<dynamic> getAPI(String params, Map headers) async {
    //[Arun this api called multiple time why ?? if same data required store in shared pref or DB]
    //Praveen: This is common method written to call get method just by passing url irrespective of api's
    //check whether app is connected to internet or Not
    var connectivityResult = await (Connectivity().checkConnectivity());
    if (connectivityResult.toString() != 'ConnectivityResult.none') {
      //var response = await http.get(params, headers: headers);
      var response;
      if(headers != null){
        response = await http.get(Uri.parse(params), headers: headers);

      }
      else{
        response = await http.get(Uri.parse(params));
      }
      //check whether response coming from server or not
      print(response.body);
      if (response.body == '' || response.body == null) {
        return '[No Data]';
      } else {
        Map<String, dynamic> data = json.decode(utf8.decode(response.bodyBytes));
        if (data['success'] == true && data['errorCode'] == null ||
            data['errorCode'] == "DYGET" && data['data'].length != 0) {
          return [data['message'], data];
        } else if (data['success'] == true && data['errorCode'] == null ||
            data['errorCode'] == "DYGET" && data['data'].length == 0) {
          return ['Empty Data'];
        } else if (data['success'] == false && data['errorCode'] == '502') {
          return [data['message'], data];
        } else if (data['success'] == false && data['errorCode'] == '501') {
          return [data['message'], data];
        }  else if (data['success'] == true && data['errorCode'] == '501') {
          return [data['message'], data];
        }else if (data['success'] == true) {
          return [data['message'], data];
        }
        else if (data['errorType'] == 'ValidationException') {
          return ['Some parameter values missing'];
        } else if (data['errorType'] == 'TypeError') {
          return ['Empty Parameter'];
        } else if (data['errorType'] == 'Error') {
          return ['Unsupported format Details'];
        } else if (response.statusCode.toString() == '400' ||
            response.statusCode.toString() == '500') {
          return ['Invalid request Params'];
        } else if (response.statusCode.toString() == '400') {
          return ['Invalid request body'];
        } else if (response.statusCode.toString() == '401') {
          return [data['message']];
        } else {
          return ['something wrong'];
        }
      }
    } else {
      return ['No Internet Connection'];
    }
  }

  Future<dynamic> postAPI(String params, Map dataJson, Map headers) async {
    //check whether app is connected to internet or Not
    var connectivityResult = await (Connectivity().checkConnectivity());
    if (connectivityResult.toString() != 'ConnectivityResult.none') {
      var response;
      if(headers != null){
         response = await http.post(Uri.parse(params),
            headers: headers,
            body: json.encode(dataJson),
            encoding: Encoding.getByName("utf-8"));
      }
      else{
        response = await http.get(Uri.parse(params));
      }

      if (response.body == '' || response.body == null) {
        return 'No Data Found';
      } else {
        Map<String, dynamic> data = jsonDecode(response.body);
        Map<String, dynamic> responseJson =
            json.decode(utf8.decode(response.bodyBytes));
        if (responseJson['success'] == true &&
                responseJson['errorCode'] == null ||
            responseJson['errorCode'] == "DYGET" &&
                responseJson['data'].length != 0) {
          return [responseJson['message'], responseJson];
        }else if(responseJson.toString().toLowerCase()== "{UnprocessedItems: {}}".toLowerCase()){
            return "Success";
        }
        else if (responseJson['success'] == true &&
                responseJson['errorCode'] == null ||
            responseJson['errorCode'] == "DYGET" &&
                responseJson['data'].length == 0) {
          return ['Empty Data'];
        } else if (responseJson['success'] == false &&
            responseJson['errorCode'] == '502') {
          return [responseJson['message'], responseJson];
        } else if (responseJson['success'] == false &&
            responseJson['errorCode'] == '501') {
          return [responseJson['message'], responseJson];
        } else if (responseJson['success'] == true &&
            responseJson['errorCode'] == '501') {
          return [responseJson['message'], responseJson];
        } else if (responseJson['errorType'] == 'ValidationException') {
          return ['Some parameter values missing'];
        } else if (responseJson['errorType'] == 'TypeError') {
          return ['Empty Parameter'];
        } else if (responseJson['errorType'] == 'Error') {
          return ['Unsupported format Details'];
        } else if (responseJson['success'] == false &&
            responseJson['errorCode'] == 'TESTSUBMITTED') {
          return [responseJson['errorCode'], responseJson];
        }
//        } else if (response.statusCode.toString() == '200') {
//          return ['200 status code'];
//        }
        else if (response.statusCode.toString() == '400') {
          return ['Invalid request body'];
        } else {
          return ['something wrong'];
        }
      }
    } else {
      return ['No Internet Connection'];
    }
  }

  Future<dynamic> putAPI(String params, Map dataJson, Map headers) async {
    //check whether app is connected to internet or Not
    var connectivityResult = await (Connectivity().checkConnectivity());
    if (connectivityResult.toString() != 'ConnectivityResult.none') {
      var response = await http.put(Uri.parse(params),
          headers: headers,
          body: json.encode(dataJson),
          encoding: Encoding.getByName("utf-8"));
      if (response.body == '' || response.body == null) {
        return 'No Data Found';
      } else {
        Map<String, dynamic> data = jsonDecode(response.body);
        Map<String, dynamic> responseJson =
            json.decode(utf8.decode(response.bodyBytes));
        if (responseJson['success'] == true &&
                responseJson['errorCode'] == null ||
            responseJson['errorCode'] == "DYGET" &&
                responseJson['data'].length != 0) {
          return [responseJson['message'], responseJson];
        } else if (responseJson['success'] == true &&
                responseJson['errorCode'] == null ||
            responseJson['errorCode'] == "DYGET" &&
                responseJson['data'].length == 0) {
          return ['Empty Data'];
        } else if (responseJson['success'] == false) {
          return [responseJson['message'], responseJson];
        } else if (responseJson['success'] == false &&
            responseJson['errorCode'] == '502') {
          return [responseJson['message'], responseJson];
        } else if (responseJson['success'] == false &&
            responseJson['errorCode'] == '501') {
          return [responseJson['message'], responseJson];
        } else if (responseJson['errorType'] == 'ValidationException') {
          return ['Some parameter values missing'];
        } else if (responseJson['errorType'] == 'TypeError') {
          return ['Empty Parameter'];
        } else if (responseJson['errorType'] == 'Error') {
          return ['Unsupported format Details'];
        } else if (response.statusCode.toString() == '400') {
          return ['Invalid request body'];
        } else {
          return ['something wrong'];
        }
      }
    } else {
      return ['No Internet Connection'];
    }
  }

  /// my plan .........................

  Future<dynamic> getAPITerr(String params) async {
    //[Arun this api called multiple time why ?? if same data required store in shared pref or DB]
    //Praveen: This is common method written to call get method just by passing url irrespective of api's
    //check whether app is connected to internet or Not
    var connectivityResult = await (Connectivity().checkConnectivity());
    if (connectivityResult.toString() != 'ConnectivityResult.none') {
      //var response = await http.get(params, headers: headers);
       var  response = await http.get(Uri.parse(params));
      //check whether response coming from server or not
      print(response.body);
      if (response.body == '' || response.body == null) {
        return '[No Data]';
      } else {
        Map<String, dynamic> data = json.decode(utf8.decode(response.bodyBytes));
          print(data);
          print("datadata");
          print(response.statusCode.toString());
        if (data['success'] == true && (response.statusCode.toString() == '200'||response.statusCode.toString() == '201')) {
          return [data];
        }
         else if (response.statusCode.toString() == '400' ||
            response.statusCode.toString() == '500') {
          return ['Invalid request Params'];
        } else if (response.statusCode.toString() == '400') {
          return ['Invalid request body'];
        } else if (response.statusCode.toString() == '401') {
          return [data];
        } else if (response.statusCode.toString() == '404') {
           return [data];
         } else {
          return ['something wrong'];
        }
      }
    } else {
      return ['No Internet Connection'];
    }
  }

  Future<dynamic> postAPITerr(String params, Map dataJson, Map headers) async {
    //check whether app is connected to internet or Not
    var connectivityResult = await (Connectivity().checkConnectivity());
    if (connectivityResult.toString() != 'ConnectivityResult.none') {
      var response = await http.post(Uri.parse(params),
            headers: headers,
            body: json.encode(dataJson),
            encoding: Encoding.getByName("utf-8"));


      if (response.body == '' || response.body == null) {
        return 'No Data Found';
      } else {
        Map<String, dynamic> data = jsonDecode(response.body);
        Map<String, dynamic> responseJson =
        json.decode(utf8.decode(response.bodyBytes));
        print("-------------------------service");
        print(responseJson);
        print(data);
        print("datadata");
        print(response.statusCode.toString());
        if (data['success'] == true && (response.statusCode.toString() == '200'||response.statusCode.toString() == '201')) {
          return [data];
        }
        else if (response.statusCode.toString() == '400' ||
            response.statusCode.toString() == '500') {
          return ['Invalid request Params'];
        } else if (response.statusCode.toString() == '400') {
          return ['Invalid request body'];
        } else if (response.statusCode.toString() == '401') {
          return [data];
        } else if (response.statusCode.toString() == '404') {
          return [data];
        }
        else if (response.statusCode.toString() == '409') {
          return [data];
        }
        else {
          return ['something wrong'];
        }
      }
    } else {
      return ['No Internet Connection'];
    }
  }

  Future<dynamic> putAPITerr(String params, Map dataJson, Map headers) async {
    //check whether app is connected to internet or Not
    var connectivityResult = await (Connectivity().checkConnectivity());
    if (connectivityResult.toString() != 'ConnectivityResult.none') {
      var response = await http.put(Uri.parse(params),
          headers: headers,
          body: json.encode(dataJson),
          encoding: Encoding.getByName("utf-8"));
      if (response.body == '' || response.body == null) {
        return 'No Data Found';
      } else {
        Map<String, dynamic> data = jsonDecode(response.body);
        Map<String, dynamic> responseJson =
        json.decode(utf8.decode(response.bodyBytes));
        print(data['success'] == true);
        print(response.statusCode.toString());
        print("dataaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa in put api ");
        if (data['success'] == true && (response.statusCode.toString() == '200'||response.statusCode.toString() == '201'||response.statusCode.toString() == '202')) {
          return [data];
        }
        else if (response.statusCode.toString() == '400' ||
            response.statusCode.toString() == '500') {
          return ['Invalid request Params'];
        } else if (response.statusCode.toString() == '400') {
          return ['Invalid request body'];
        } else if (response.statusCode.toString() == '401') {
          return [data];
        } else if (response.statusCode.toString() == '404') {
          return [data];
        } else {
          return ['something wrong'];
        }
      }
    } else {
      return ['No Internet Connection'];
    }
  }


  ///  gk zone (statusCode From response)........................

  Future<dynamic> getAPIStatusCodeFromResponse(String params) async {
    //[Arun this api called multiple time why ?? if same data required store in shared pref or DB]
    //Praveen: This is common method written to call get method just by passing url irrespective of api's
    //check whether app is connected to internet or Not
    var connectivityResult = await (Connectivity().checkConnectivity());
    if (connectivityResult.toString() != 'ConnectivityResult.none') {
      //var response = await http.get(params, headers: headers);
      var  response = await http.get(Uri.parse(params));
      //check whether response coming from server or not
      if (response.body == '' || response.body == null) {
        return '[No Data]';
      } else {
        Map<String, dynamic> data = json.decode(utf8.decode(response.bodyBytes));
        if (data['success'] == true && (data['statusCode'].toString() == '200'||data['statusCode'].toString() == '201')) {
          return [data];
        }
        else if (data['statusCode'].toString() == '400' ||
            data['statusCode'].toString() == '500') {
          return ['Invalid request Params'];
        } else if (data['statusCode'].toString() == '400') {
          return ['Invalid request body'];
        } else if (data['statusCode'].toString() == '401') {
          return [data];
        } else if (data['statusCode'].toString() == '404') {
          return [data];
        } else {
          return ['something wrong'];
        }
      }
    } else {
      return ['No Internet Connection'];
    }
  }

  Future<dynamic> postAPIStatusCodeFromResponse(String params, Map dataJson, Map headers) async {
    //check whether app is connected to internet or Not
    var connectivityResult = await (Connectivity().checkConnectivity());
    if (connectivityResult.toString() != 'ConnectivityResult.none') {
      var response = await http.post(Uri.parse(params),
          headers: headers,
          body: json.encode(dataJson),
          encoding: Encoding.getByName("utf-8"));


      if (response.body == '' || response.body == null) {
        return 'No Data Found';
      } else {
        Map<String, dynamic> data = jsonDecode(response.body);
        Map<String, dynamic> responseJson =
        json.decode(utf8.decode(response.bodyBytes));
        print("data");
        print(data);
        print(response.body);
        if (data['success'] == true && (data['statusCode'].toString() == '200'||data['statusCode'].toString() == '201'||data['statusCode'].toString() == '202')) {
          return [data];
        }
        else if (data['statusCode'].toString() == '400' ||
            data['statusCode'].toString() == '500') {
          return ['Invalid request Params'];
        } else if (data['statusCode'].toString() == '400') {
          return ['Invalid request body'];
        } else if (data['statusCode'].toString() == '401') {
          return [data];
        } else if (data['statusCode'].toString() == '404') {
          return [data];
        }
        else if (data['statusCode'].toString() == '409') {
          return [data];
        }
        else {
          return ['something wrong'];
        }
      }
    } else {
      return ['No Internet Connection'];
    }
  }

  Future<dynamic> putAPIStatusCodeFromResponse(String params, Map dataJson, Map headers) async {
    //check whether app is connected to internet or Not
    var connectivityResult = await (Connectivity().checkConnectivity());
    if (connectivityResult.toString() != 'ConnectivityResult.none') {
      var response = await http.put(Uri.parse(params),
          headers: headers,
          body: json.encode(dataJson),
          encoding: Encoding.getByName("utf-8"));
      if (response.body == '' || response.body == null) {
        return 'No Data Found';
      } else {
        Map<String, dynamic> data = jsonDecode(response.body);
        Map<String, dynamic> responseJson =
        json.decode(utf8.decode(response.bodyBytes));
        print("data");
        print(data);
        print(response.body);
        if (data['success'] == true && (data['statusCode'].toString() == '200'||data['statusCode'].toString() == '201'||data['statusCode'].toString() == '202')) {
          return [data];
        }
        else if (data['statusCode'].toString() == '400' ||
            data['statusCode'].toString() == '500') {
          return ['Invalid request Params'];
        } else if (data['statusCode'].toString() == '400') {
          return ['Invalid request body'];
        } else if (data['statusCode'].toString() == '401') {
          return [data];
        } else if (data['statusCode'].toString() == '404') {
          return [data];
        } else {
          return ['something wrong'];
        }
      }
    } else {
      return ['No Internet Connection'];
    }
  }



  Future<dynamic> postAPISettings(
      String params, Map dataJson, Map headers) async {
    //check whether app is connected to internet or Not
    var connectivityResult = await (Connectivity().checkConnectivity());
    if (connectivityResult.toString() != 'ConnectivityResult.none') {
      var response = await http.post(Uri.parse(params),
          headers: headers,
          body: json.encode(dataJson),
          encoding: Encoding.getByName("utf-8"));
      if (response.body == '' || response.body == null) {
        return 'No Data Found';
      } else {
        Map<String, dynamic> data = jsonDecode(response.body);
        Map<String, dynamic> responseJson =
            json.decode(utf8.decode(response.bodyBytes));
        return [responseJson['message'], responseJson];
//        if (responseJson['success'] == true && responseJson['errorCode'] == null || responseJson['errorCode'] == "DYGET" && responseJson['data'].lenght!=0 ) {
//          return [responseJson['message'], responseJson];
//        }else if (responseJson['success'] == true && responseJson['errorCode'] == null || responseJson['errorCode'] == "DYGET" && responseJson['data'].lenght==0 ) {
//          return ['Empty Data'];
//        } else if (responseJson['success'] == false && responseJson['errorCode'] == '502') {
//          return [responseJson['message'], responseJson];
//        } else if (responseJson['success'] == false && responseJson['errorCode'] == '501') {
//          return [responseJson['message'], responseJson];
//        } else if (responseJson['errorType'] == 'ValidationException') {
//          return ['Some parameter values missing'];
//        } else if (responseJson['errorType'] == 'TypeError') {
//          return ['Empty Parameter'];
//        } else if (responseJson['errorType'] == 'Error') {
//          return ['Unsupported format Details'];
//        } else if (response.statusCode.toString() == '400') {
//          return ['Invalid request body'];
//        }else{
//          return ['something wrong'];
//        }

      }
    } else {
      return ['No Internet Connection'];
    }
  }
}

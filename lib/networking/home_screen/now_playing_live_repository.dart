import 'package:imsindia/models/home_screen/now_playing_live_response_model.dart';
import 'package:imsindia/networking/handlers/response_channel.dart';

class NowPlayingLiveRepository {

  ResponseChannel _responseChannel = ResponseChannel();

  Future<NowPlayingLiveResponseModel> fetchNowPlayingLive() async {

    String apiEndPoint = "student-tests?parentId=d943a1ee90b33a6523301a01b77674690d772f449";

    final response = await _responseChannel.doGet(apiEndPoint);

    return NowPlayingLiveResponseModel.fromJson(response);
  }

}
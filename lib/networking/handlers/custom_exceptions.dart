class CustomExceptions implements Exception {
  final _message;
  // final _prefix;

  CustomExceptions([this._message]);

  String toString() {
    return "$_message";
  }
}

class FetchDataException extends CustomExceptions {
  FetchDataException([String message])
      : super(message);
}

class BadRequestException extends CustomExceptions {
  BadRequestException([message]) : super(message);
}

class UnprocessableException extends CustomExceptions {
  UnprocessableException([message]) : super(message);
}

class UnauthorisedException extends CustomExceptions {
  UnauthorisedException([message]) : super(message);
}

class NotFoundException extends CustomExceptions {
  NotFoundException([message]) : super(message);
}

class InvalidInputException extends CustomExceptions {
  InvalidInputException([String message]) : super(message);
}
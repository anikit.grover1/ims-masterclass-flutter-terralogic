import 'dart:convert';
import 'dart:io';

import 'package:http/http.dart' as http;
import 'package:imsindia/utils/Url.dart';

import 'custom_exceptions.dart';

class ResponseChannel {

  Future<dynamic> doPatch(String apiEndPoint, dynamic _body) async {
    String requestUrl = "${URL.BASE_URL}$apiEndPoint";
    final authToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjEzMzk3OCIsImlzU3R1ZGVudCI6MSwiY29tcGFueUNvZGUiOiJpbXMiLCJpYXQiOjE2MjAxOTcxODN9.XX-RJX6uafQwd6H3zmwVhyPsM8Q8xRrlboHgVGxJhG4";

    print('api url $requestUrl');

    // try {
    final http.Response response = await http.patch(Uri.parse(requestUrl),
        headers: <String, String>{
          'Content-Type': 'application/json',
          'Accept': 'application/json',
          'Authorization': 'Bearer $authToken',
        },
        body: _body
    );
    print('##### response.statusCode ${response.statusCode}');
    return _returnResponse(response);
  }

  Future<dynamic> doPost(String apiEndPoint, dynamic _body) async {
    String requestUrl = "${URL.BASE_URL}$apiEndPoint";
    final authToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjEzMzk3OCIsImlzU3R1ZGVudCI6MSwiY29tcGFueUNvZGUiOiJpbXMiLCJpYXQiOjE2MjAxOTcxODN9.XX-RJX6uafQwd6H3zmwVhyPsM8Q8xRrlboHgVGxJhG4";

    print('api url $requestUrl');

    // try {
    final http.Response response = await http.post(Uri.parse(requestUrl),
        headers: <String, String>{
          'Content-Type': 'application/json',
          'Accept': 'application/json',
          'Authorization': 'Bearer $authToken',
        },
        body: _body);
    print('##### response.statusCode ${response.statusCode}');
    return _returnResponse(response);
  }

  Future<dynamic> doPut(String apiEndPoint, dynamic _body) async {

    String requestUrl = "${URL.BASE_URL}$apiEndPoint";
    final authToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjEzMzk3OCIsImlzU3R1ZGVudCI6MSwiY29tcGFueUNvZGUiOiJpbXMiLCJpYXQiOjE2MjAxOTcxODN9.XX-RJX6uafQwd6H3zmwVhyPsM8Q8xRrlboHgVGxJhG4";

    print('api url $requestUrl');

    var responseJson;

    try {
      final http.Response response = await http.put(Uri.parse(requestUrl),
          headers: <String, String>{
            'Content-Type': 'application/json',
            'Accept': 'application/json',
            'Authorization': 'Bearer $authToken',
          },
          body: _body);
      responseJson = _returnResponse(response);
    } on SocketException {
      throw FetchDataException('No Internet connection');
    }

    return responseJson;
  }

  Future<dynamic> doGet(String apiEndPoint) async {

    String requestUrl = "${URL.BASE_URL}$apiEndPoint";
    final authToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjEzMzk3OCIsImlzU3R1ZGVudCI6MSwiY29tcGFueUNvZGUiOiJpbXMiLCJpYXQiOjE2MjAxOTcxODN9.XX-RJX6uafQwd6H3zmwVhyPsM8Q8xRrlboHgVGxJhG4";

    print('api url $requestUrl');

    var responseJson;

    try {
      final response = await http.get(Uri.parse(requestUrl),
        headers: <String, String>{
          'Content-Type': 'application/json',
          'Accept': 'application/json',
          'Authorization': '$authToken',
        },
      );
      responseJson = _returnResponse(response);
    } on SocketException {
      throw FetchDataException('No Internet connection');
    }
    return responseJson;
  }

  dynamic _returnResponse(http.Response response) {
    final responseCode = response.statusCode;
    if (responseCode >= 200 && responseCode < 400) {
      if (response.body.isNotEmpty) {
        var responseJson = json.decode(response.body.toString());
        print(responseJson);
        return responseJson;
      } else {
        return {};
      }
    } else if (responseCode == 400) {
      throw BadRequestException(httpExceptionMessage(response));
    } else if (responseCode == 422) {
      throw UnprocessableException(httpExceptionMessage(response));
    } else if (responseCode == 401 || responseCode == 403) {
      throw UnauthorisedException(httpExceptionMessage(response));
    } else if (responseCode == 404) {
      throw NotFoundException(httpExceptionMessage(response));
    } else {
      throw FetchDataException(
          'Error occured while Communication with Server with StatusCode : ${response.statusCode}');
    }
  }

  String httpExceptionMessage(http.Response response) {
    var responseJson = json.decode(response.body.toString());
    print('responseJson $responseJson');
    return responseJson["message"];
    //return responseJson["error"];
  }

  checkStatus200(int statusCode) {}
}

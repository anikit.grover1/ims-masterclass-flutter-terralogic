class EmaximiserHomeScreenString{
  static const String Emaximiser = "e-Maximiser";
}
class EmaximiserSubjectsScreenStrings{
  static const String PieChart = "Pie Chart";
  static const String numberOfQuestionsString = "0/50";
  static const String sloveString = "SOLVE";
  static const String learnString = "LEARN";
}
class EmaximiserParagraphScreenStrings{
  static const String header = "CAT Files 1 Set 1";
  static const String question = "Question ";
  static const String sessionPaused = "Session Paused";
  static const String resume = "Resume";
  static const String popUpForExit = "Are you sure you want to exit?";
  static const String yesForPopUpExit = "Yes";
  static const String noForPopUpExit = "No";



}

class EmaximiserCalculationScreenStrings {
  static const String Tab_Queries = "Queries";
  static const String Tab_UpNext = "Up Next";
  static const String Tab_Details = "Details";
  static const String Video_URL = "https://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4";
  static const String Text_Calculation = "Calculations";
  static const String Text_Concepts = "Concept";
  static const String Text_Accuracy = "Accuracy";
}


class Emaximiserquestions{
  static const String questions ="Prose is to Poetry, said Valéry, as walking is to dancing. That poets regularly produce prose, while prose writers rarely write poetry, is not, as Brodsky argues, evidence of poetry’s superiority. According to Brodsky, “The poet, in principle, is ‘higher’ than the prose writer because a hard-up poet can sit down and compose an article, whereas in similar straits a prose writer would hardly give thought to a poem.” But the point surely is not that writing poetry is less well paid than writing prose but that it is special — the marginalizing of poetry and its audience; that what was once considered a normal skill, like playing a musical instrument, now seems the province of the difficult and the intimidating. Not only prose writers but also cultivated people generally no longer write poetry. (As poetry is no longer, as a matter of course, something to memorize.) Modern performance in literature is partly shaped by the widespread discrediting of the idea of literary virtuosity; by a very real loss of virtuosity. It now seems utterly extraordinary that anyone can write brilliant prose in more than one language; we marvel at a Nabokov, a Beckett, a Cabrera Infante — but until two centuries ago such virtuosity would have been taken for granted. So, until recently, was the ability to write poetry as well as prose.\n";
  static const String qoToQuestion ="GO TO QUESTION";
  static const String submit= "SUBMIT";
  static const String next= "NEXT";
  static const String back= "Back to paragraph";
  static const String prev= "PREV";



}
class QueriesScreenStrings{
  static const String Text_NumberofVideo = "5 Videos";
  static const String Text_Totaltime = "2 Mins";
}




class EmaximiserStatisticsScreenStrings{
  static const String Text_BackToQuestion = "Back to question";
  static const String Text_QuestionStats = "Question Stats";
  static const String Text_AnsQuestionStats = "Status: Skipped";
  static const String Text_Timetaken = "Time Taken";
  static const String Text_AvgTimetaken= "Avg Time Taken";
  static const String Text_AttemptedBy = "Attempted By";
  static const String Text_PValue= "P-Value";
  static const String Text_ABCApproach = "ABC Approach";
  static const String Text_ValueTimetaken = "00:10:22";
  static const String Text_ValueAvgTimetaken= "00:10:22";
  static const String Text_ValueAttemptedBy = "78%";
  static const String Text_ValueAccurcy = "78%";
  static const String Text_ValuePValue= "52.8%";
  static const String Text_ValueABCApproach = "52.8%";
}

class EmaximiserShowAnswerScreenStrings{
  static const String Text_Answer = "Answer - A";
  static const String Text_Solution = "Solution";
  static const String Text_ParagraphContent = "Poetry is not superior to prose since it was once considered a normal skill expected of any cultivated individual, unlike today, when both poetry and its audience have been marginalized as being difficult and “The poet, in principle, is ‘higher’ than the prose writer because a hard-up poet can sit down and compose an article, whereas in similar straits a prose writer would hardly give thought to a poem.";
}

class SubmitGlobalButtonScreenStrings{
  static const String Button_Previous = "PREV";
  static const String Button_Next = "NEXT";
  static const String Button_Submit ="SUBMIT";
}

class SummaryScreenStrings{
  static const String Text_CalculationSummary = "Summary";
  static const String submitConfirmInSummary = "Are you sure want to submit this section?";
  static const String Total = "TOTAL";
  static const String Correct= "CORRECT";
  static const String InCorrect = "INCORRECT";
  static const String Skipped = "SKIPPED";

}
/**Sushree**/
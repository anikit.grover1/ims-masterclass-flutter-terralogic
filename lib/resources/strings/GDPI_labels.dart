class GDPIHomeString{
  static const String GDPI_Title = "GD-PI";
  static const String First_list = "Prepare for PI";
  static const String Second_list ="Prepare for GD-CASE_WAT";
  static const String Third_list = "B-School Processes and Experiences";
  static const String Sub_header = "We'd love to know your scores!";
  static const String Submit = "SUBMIT";
  static const String Score = "Overall score";
  static const String Percentile = "Overall Percentile";
  static const String Upload_Scorecard = "Upload Scorecard";
  static const String PI_Masterclass = "PI Masterclass";
  static const String SOP_Masterclass = "SOP Masterclass";
  static const String PI_WorkbookEbook = "PI Workbook E-book";
  static const String Number_ImportantConcepts_Definitions = "50 Important Concepts & Definitions";
  static const String About = "Tell us your latest exam scores!";
  static const String AboutScore ="IIFT results are out. Tell us your score!";
  static const String GD_Masterclass = "GD Master Class";
  static const String WAT_MasterClass = "WAT Master Class";
  static const String GDWAT_ContentBible = "GD-WAT Content Bible";
  static const String CaseStudy_ContentBible = "Case Study Content Bible";
  static const String Select_BSCHOOL = "Choose all institutes you recieved calls from. Upload anywhere between 1 to 5 call letters to gain access to B-School processes";
  static const String CAT_Scorecard = "CAT 2018 Scorecard";
  static const String Call_Letter = "Call Letter";
  static const String B_School_Name = "FMS - Chennai";
}
class GDPIProcessLabels{
  static const String your_Communities='Your Communities';
  static const String join_Communities='Join Communities';
  static const String uploadDocuments ='Upload Documents';

}
class LoginAppLabels {
  //Lables used in application for Login Page 
  static const String emailID = "Registered Email/IMS Pin";
  static const String email = "Registered Email";
  static const String password = "Password";
  static const String imsPassword = "IMS Password";
  static const String imsPin = "IMS PIN";
  static const String invalidID = "invalid email ID";
  static const String hintString = "stephen@gmail.com";
  static const String grade= "Grade";
  static const String nameOfCollege= "Name of College";
  static const String percentile= "Percentile";
    static const String selectCourse= "Select Course";



}

class LoginAppStrings {
  //string used in application for Login Page

  static const String appName = "IMS APP";

  static const String loginTitle = "Welcome to myIMS!";

  static const String loginSubTitle = "There is a lot to learn";

  static const String loginButton = "LOGIN";

  static const String loginGoogle = "GOOGLE";

  static const String loginFacebook = "FACEBOOK";

  static const String loginAlternativeOption = "or";

  static const String loginAlternativeMessage = "Login Using";

  static const String accountConfirmation = "Dont have an account?";

  static const String clickOnSignup = "Signup";

  static const String signupMessage = "through our website";

  static const String forgotPassword = "Forgot Password?";
  static const String forgotPasswordMessage = "Enter your email ID or IMS PIN registered with your myIMS account. A reset linkwill be sent to your email ID";

  static const String sendResetLink = "SEND RESET LINK";

  static const String nextButton = "NEXT";

  static const String createAccount = "CREATE ACCOUNT";
  static const String confirmDetails = "CONFIRM DETAILS";
  static const String getLoginPassword = "Get Login Password";




}
class CreateProfileLabels{
  static const String nextBtnText = "Next";
  static const String title = "Let's create your profile";
  static const String yourName= "Your Name";
  static const String highestQualification="Highest Qualification";
  static const String collegeName="College Name";
  static const String catExam="CAT Examination";
  static const String noOFAttempts="No. of Attempts";
  static const String notApplicable="Not Applicable";
  static const String firstAttempt="First Attempt";
  static const String secondAttempt="Second Attempt";
  static const String thirdAttempt="Third Attempt";
  static const String fourthAttempt="Fourth Attempt";
  static const String fifthAttempt="Fifth Attempt";
  static const String confirmDetails="Confirm Details";
  static const String nextBtn="Next";
  static const String changeProfilePhoto="Change Profile Photo";
  static const String uploadProfilePhoto="Upload Profile Photo";
  static const String inputText="This name will appear on your profile";
  static const String emptyText="";
  static const String nameLengh="name should be atleast 4 characters";
  static const String invalidName="invalid Name";
  static const String percentile="Percentile";
  static const String year="Year";
  static const String invalidCollege="invalid College name";
  static const String invalidCollegelength="college name should be atleast 5 characters";
//  static const String invalidName="invalid Name";
//  static const String invalidName="invalid Name";





}
class CongratulationsLabels{
  static const String btnText = "Confirm Password";
  static const String setNewPsw = "Set New Password";
  static const String continueIMS= "Continue with IMS Password";
  static const String congratulationsTitle="Congratulations!";
  static const String congratulationsSubText="Your profile has been successfully created!";
  static const String congratulationSubTitle="You can use your email next time";
  static const String invalidPassword = "invalid password";

}

class ThreeStepLoginLables{
  static const String title = "Create your free account \nin 3 easy steps!";
  static const String emailId="Your Email ID";
  static const String mobileNo="Mobile Number";
  static const String product="Product you are interested in";
  static const String btnText="Get Login Password";
  static const String invalidMobileno = "invalid mobile number";
  static const String invalidID = "invalid email ID";
  static const String invalidAddress = "invalid address";
  static const String invalidAddresslength = "address should atleast have 5 characters";
  static const String invalidPincode = "invalid pincode";


//  static const String emailId="";
//  static const String emailId="";
//  static const String emailId="";
//  static const String emailId="";

}
class LoginConfirmaddressLables{

  static const String  houseNo="House No/Building Name";
  static const String  StreetName="Street Name/Area Name";
  static const String  Landmark="Landmark";
  static const String  city="City";
  static const String  State="State";
  static const String  Pincode="Pincode";
  static const String  pincodeLength="Pincode should be of length 6";
  static const String confirmAddress="Confirm Address";
}


class  LoginWorkexperienceLables{
  static const String notApplicable="Not Applicable";
  static const String totalExperience="Total Experience - In Months";
  static const String organisation="Organisation - Any organisation you've worked at";
  static const String sector="Sector";
  static const String invalidOrganisationlength="Organisation should atleast have 5 characters";
  static const String invalidOrganisation="invalid Organisation";
  static const String invalidExperience="Experience should be numeric";


}

class  LoginResetLinkLables{
  static const String notApplicable="Not Applicable";
  static const String totalExperience="Total Experience - In Months";
  static const String organisation="Organisation - Any organisation you've worked at";
  static const String continueLable="Continue";
}
class ExpansionPanelIndexVariable{
  static  int activeMeterIndex;
}

class LoginUsingIMSPin{
  static const String invalidIMSPin="should be atleast length of 3";
  static const String invalidIMSPassword="should be atleast length of 6";
    static const String invalidIMSPasswordForLogin="IMS Pin should be atleast length of 13";

  static const String invalidLoginPassword="should be atleast length of 5";

  static const String invalidPassword="should be atleast length of 3";
  static const String invalidImsPinNo="Invalid Ims Pin";


}
class AnalyticsProgressLabels{
  static const String testVenue="Test Venue";
}

class BookTestScreenStrings{
  static const String bookaTest = "Book a Test";
  static const String bookaSlot = "BOOK SLOT";
  static const String viewAdminCard = "View Admin Card";
  static const String simcat1SlotBooking = "SimCAT 1 Slot Booking";
  static const String testVenue = "Test Venue";
  static const String availableSlots = "Available Slots";
  static const String importantInstructions = "Important Instructions";
  /// list of important instructions ///
  static const List<String> listOfInstructions=[
    "Kindly reach the venue 10 minutes before the slot timing. Students who arrive late will not be allowed to enter.",
    "Ensure that your mobile is switched off and silence is maintained.",
    "Please carry a prinout of the registration confirmation along with your IMS I-Card.",
    "Once a booking is confirmed, you can change it only after cancelling the existing booking."
  ];
  static const String link = "The link for taking the test will be made available here on the test date, 11:00 AM onwards";
  static const String cancellationHistory = "Cancellation History";
  static const String bookingCancellations= "No booking cancellations found for Last Mile to CAT Bangalore 2018";
  static const String printTicket="Print Ticket";
  static const String emailTicket="Email Ticket";
  static const String cancelBooking = "Cancel Booking";
  static const String admitCard = "Admit Card Token ID";
  static const String number =  "676916-WF6FC4";
  static const String candidateName="Candidate Name";
  static const String stephenMoses= "Stephen Moses";
  static const String stephenGmail=  "stephen@gmail.com";
  static const String date= "Date";
  static const String dates="02-07-2019";
  static const String time= "Time";
  static const String times=  "10:00 AM - 02:00 PM";
  static const String mother="Mother Tekla Auditorium, 143, General KS Thimayya Rd, Craig Park Layout, Ashok Nagar, Bengaluru, Karnataka 560025.";
  static const String are= "Are you sure you want\nto cancel booking?";
  static const String no= "No";
  static const String yes= "Yes";
}
class ChannelStrings {
  static const String appBarLabel = "Channel";
  static const String liveSessions = "Live Sessions";
  static const String recordedSessions = "Recorded Sessions";
  static const String videos = "Videos";
  static const String post_a_query = "Post a query";
  static const String rate_this_video = "Rate this video";
  static const String chief_mentor = "Chief Mentor";
  static const String experience = "Experience";
  static const String students_taught = "Students Taught";
  static const String profile = "Profile";
  static const String userName = "Stephen Grider";


}
class Quick_tipsScreenStrings {
  static const String Tab_Queries = "Queries";
  static const String Tab_UpNext = "Up Next";
  static const String Tab_Details = "Details";
  static const String Video_URL = "https://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4";
  static const String Text_title = "Quick tips for CAT preperation";
  static const String Text_Concepts = "Concept";
  static const String Text_Accuracy = "Accuracy";
  static const String Text_strategy = "Strategy";
  static const String Text_rating = "18 ratings";
}
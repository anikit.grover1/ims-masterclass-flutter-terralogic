
class MyplanHomeScreenString {

  static const String MyplanSolidPlan = "Want a solid plan of action to improve your scores? \n\n";
  static const String Bookaslottodiscuss  ="Book a slot now to discuss next stage of your preparation with a Mentor";
  static const String Noslotsavailable  = " No slots available?";
  static const String YouCan = " You can ";
  static const String WritetoaMentor="Write to a Mentor";
  static const String instead= " instead ";
  static const String BookedSlots= "Booked Slots";
  static const String Time= "Time";
  static const String Location= "Centre";
  static const String BookSlot= "BOOK SLOT";
  static const String popUpMsgForNoSlots= "You Have Availed The Eligible No. Of MYPLAN Sessions";
  static const String okayButtonForPopUpForNoSlots= "OKAY";


}



class MyplanConfirmationScreenString{
  static const String Meeting = "Meeting 1";
  static const String MentorName = "Mentor Name";
  static const String MentorLocation = "Centre";
  static const String MentorDate = "Date";
  static const String MentorTime = "Time";
  static const String MentorReasonforvisit = "Reason for visit ";
  static const String mode = "mode";
  static const String MentorRequestCancelled = "Cancelled By Mentor";
  static const String Feedback = "Feedback";
  static const String CancelBooking = "Cancel Request";
  static const String cancelling= "cancelling";
  static const String RequestCancelled="Request Cancelled";
  static const String Absent="Absent";
  static const String MentorBookimgcancelled = "Booking cancelled within 24 hours of session time will be treated as availed. ";
}




class MyplanBookMentorScreenString
{
  static const String BookaMentor= "Book a Mentor";
  static const String MentorSlotBooking= "Mentor Slot Booking";
  static const String SelectVenue= "Select Centre";
  static const String ChooseYourMentor="Choose Your Mentor";
  static const String ChooseYourMode="Choose Your Mode";
  static const String AvailableDates="Available Dates";
  static const String AvailableSlots ="Available Slots";
  static const String NodataAvailable ="No data Available";
  static const String SelectYourReason ="Select Session Name";
  static const String WhatWould  ="What would you like help with?";
  static const String BookSlot =  "BOOK SLOT";
  static const String note =  "Note: ";

  static const String noteText =  "In-centre slots will be made available as per guidelines from the local authorities. For virtual mode bookings, you will be contacted by the mentor.";
  static const String connect =  "No slots available? Connect with ";
  static const String hyperLinkForIms =  "your IMS centre";
  static const String imsSupportUrl =  "https://www.imsindia.com/locate-centre.html";
  static const String validationForMode ="Please Select Mentor";
  static const String noSlots ="The slots are not yet opened for your selection.";

}

class otpScreenString{
  static const String Otp="A text message with a 6-digit OTP has been sent to your registered Mobile Number -";
  static const String PleaseVerifyYour="Please Verify Your Request By Entering The Otp Below";
  static const String EnterOtp= "Enter the OTP";
  static const String ValidEnterOtpTitle= "Please Enter valid OTP.";
  static const String Verify="Verify";
  static const String Verifying="Verifying...";
  static const String supportMsg = "Please check if your registered mobile number is active and the phone is in a good network coverage area. Retry after an hour or write to us at";
  static const String supportMailId = "support@imsindia.com.";
  static const String ResendOtp= "Resend OTP";
  static const String ResendOtp_msg= "OTP has been sent to your mobile number.";
  static const String Success_VerifyMSG = "Your Slot booking Successfully submitted.Thank you.";
  static const String ALREADY_EXIST = "Sorry!! This slot has been taken up. Please book the session for another time-slot available.";
}

class tabHomeScreenString{
  static const String myMENTOR= "myPLAN";
  static const String piSlotBooking= "PI slot booking";

  static const String meetamentor= "Meet a Mentor";

}
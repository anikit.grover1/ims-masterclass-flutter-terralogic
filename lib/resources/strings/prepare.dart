class PrepareHomeScreenString{
  static const String Prepare = "Learn";
}
class PrepareSubjectsScreenStrings{
  static const String PieChart = "Pie Chart";
  static const String numberOfQuestionsString = "0/50";
  static const String sloveString = "SOLVE";
  static const String waitString = "WAIT";
  static const String ResumeString = "RESUME";
  static const String ReviewString = "REVIEW";
  static const String learnString = "LEARN";
  static const String productAccessForCard = "subscribe to unlock this card";

}
class PrepareParagraphScreenStrings{
  var totalQuestions;
  var totalNumberOfAttemptedQuestions;
  static const String header = "Prepositions/\nPhrasal Verbs/Idioms";
  static const String question = "Question ";
  static const String sessionPaused = "Session Paused";
  static const String resume = "Resume";
  static const String popUpForExit = "Are you sure you want to exit?";
  static const String popUpForMsgInExitButton1 = "You have not completed this section.";
  static const String popUpForMsgInExitButton2 = " questions are not yet attempted. Either solve the questions or see the answer/explanation to complete the learning experience.";
  static const String popUpForExitButton = "Do you still want to exit and solve this section later?";
  static const String yesForPopUpExit = "Yes";
  static const String noForPopUpExit = "No";

}
/**Sushree**/
class CalculationScreenStrings {
  static const String Tab_Queries = "Queries";
  static const String Tab_UpNext = "Up Next";
  static const String Tab_Details = "Details";
  static const String Video_URL = "https://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4";
  static const String Text_Calculation = "Calculations";
  static const String Text_Concepts = "Concept";
  static const String Text_Accuracy = "Accuracy";
}
class Preparequestions{
  static const String questions ="Prose is to Poetry, said Valéry, as walking is to dancing. That poets regularly produce prose, while prose writers rarely write poetry, is not, as Brodsky argues, evidence of poetry’s superiority. According to Brodsky, “The poet, in principle, is ‘higher’ than the prose writer because a hard-up poet can sit down and compose an article, whereas in similar straits a prose writer would hardly give thought to a poem.” But the point surely is not that writing poetry is less well paid than writing prose but that it is special — the marginalizing of poetry and its audience; that what was once considered a normal skill, like playing a musical instrument, now seems the province of the difficult and the intimidating. Not only prose writers but also cultivated people generally no longer write poetry. (As poetry is no longer, as a matter of course, something to memorize.) Modern performance in literature is partly shaped by the widespread discrediting of the idea of literary virtuosity; by a very real loss of virtuosity. It now seems utterly extraordinary that anyone can write brilliant prose in more than one language; we marvel at a Nabokov, a Beckett, a Cabrera Infante — but until two centuries ago such virtuosity would have been taken for granted. So, until recently, was the ability to write poetry as well as prose.\n";
  static const String qoToQuestion ="GO TO QUESTION";
  static const String submit= "SUBMIT";
  static const String submitForNextButton= "Submit";
  static const String exit= "EXIT";
  static const String emaxSubmitForLastQuestion= "SUBMIT";
  static const String next= "NEXT";
  static const String back= "Back to paragraph";
  static const String prev= "PREV";
  static const String correctStatus= "Status: Correct";
  static const String wrongStatus= "Status: Incorrect. Try Again";
  static const String wrongStatusForReview= "Status: Incorrect";
  static const String skippedStatus= "Status: skipped";
  static const String textForNeTypeQuestion= "Enter your response below.";
  static const String hintTextForNaTypeQuestions= "Enter Response here...";
  static const String messageInSolutionCheckPopup= "You will not able to attempt \n the question after seeing the solution!";
  static  String dontShowMsgInSolutionCheckPopup = "Don't show this again";
  static  String cancelInSolutionCheckPopup = "Cancel";
  static  String okayInSolutionCheckPopup = "Okay";




}
class QueriesScreenStrings{
  static const String Text_NumberofVideo = " Videos";
  static const String Text_NumberofVideo_single_videoo = " Video";

  static const String Text_Totaltime = " Mins";
  static const String Text_Totaltime_one_min = " Min";

}

class SubQueriesCommentsScreenStrings{
  static const String Text_TotalComments = "8 comments";
  static const String Text_SortBy = "Sort By";
  static const String Text_PostAquery = "Post a query";
  static const String MSG_Attach = "Attach";
  static const String ReadMoreText_Content = "How do you create a direct mail advertising campaign that gets results?  The following tips on creating a direct mail advertising campaign have been street-tested and will bring you huge returns in a short period of time.";
  static const String Text_ShowReplies = "Show Replies";
  static const String Text_UserName = "Jack Sparrow";
  static const String Text_MsgdTime = "2 Mins ago";
  static const String Text_Liked = " Upvotes";
  static const String Text_Reply = "Reply";
  static const String Text_ReadMore = "...Read more";
  static const String Text_ReadLess = " Read less";
}

class CalculationSummaryScreenStrings{
  static const String Text_CalculationSummary = "Calculation Summary";
  static const String Text_CapAccuracy = "ACCURACY";
  static const String Text_Correct = "CORRECT";
  static const String Text_Skipped = "SKIPPED";
  static const String Text_valueaccuracy = "15";
  static const String Text_valueCorrect = "12";
  static const String Text_valueSkipped = "03";
  static const String Text_Runs = "Runs";
  static const String Text_50= "50's";
  static const String Text_100 = "100's";
  static const String Text_HotStreak = "Hot Streak";
  static const String Text_BlazingStreak = "Blazing Streak";
  static const String Text_ValueRuns = "25";
  static const String Text_Value50= "25";
  static const String Text_Value100 = "25";
  static const String Text_ValueHotStreak = "25";
  static const String Text_ValueBlazingStreak = "25";
  static const String Button_Complete = "COMPLETE";
}

class StatisticsScreenStrings{
  static const String Text_BackToQuestion = "Back to question";
  static const String Text_QuestionStats = "Question Status";
  static const String Text_AnsQuestionStats = "Status: Skipped";
  static const String Text_Timetaken = "Time Taken";
  static const String Text_AvgTimetaken= "Avg Time Taken";
  static const String Text_AttemptedBy = "Attempted By";
  static const String Text_PValue= "P-Value";
  static const String Text_ABCApproach = "ABC Approach";
  static const String Text_ValueTimetaken = "00:10:22";
  static const String Text_ValueAvgTimetaken= "00:10:22";
  static const String Text_ValueAttemptedBy = "78%";
  static const String Text_ValueAccurcy = "78%";
  static const String Text_ValuePValue= "52.8%";
  static const String Text_ValueABCApproach = "52.8%";
}

class ShowAnswerScreenStrings{
  static const String Text_Answer_Emax = "Answer - ";
  static const String Text_Answer = "Answer - ";
  static const String EnterText = "Enter Text is - ";

  static const String Text_Solution = "Solution";
  static const String Text_ParagraphContent = "Poetry is not superior to prose since it was once considered a normal skill expected of any cultivated individual, unlike today, when both poetry and its audience have been marginalized as being difficult and “The poet, in principle, is ‘higher’ than the prose writer because a hard-up poet can sit down and compose an article, whereas in similar straits a prose writer would hardly give thought to a poem.";
}

class SubmitGlobalButtonScreenStrings{
  static const String Button_Previous = "PREV";
  static const String Button_Next = "NEXT";
  static const String Button_Submit ="SUBMIT";
}
/**Sushree**/


class GKZoneStrings {
  static const String appBarLabel = "GK Zone";
  static const String gk_runs = "GK runs";
  static const String weekly_rank = "weekly rank";
  static const String weekly_Stats = "Weekly Stats";
  static const String GK_Leaderboard = "GK Leaderboard";
  static const String read_label = "Read";
  static const String quiz_label = "Quiz";
  static const String select_Month = "Select Month";
  static const String select_Exam = "Select Exam";
  static const String sloveString = "START QUIZ";
  static const String waitString = "WAIT";
  static const String ResumeString = "RESUME";
  static const String ReviewString = "REVIEW";
  static const String learnString = "LEARN";
}

class GkzoneParagraphScreenStrings{
  var totalQuestions;
  var totalNumberOfAttemptedQuestions;
  static const String header = "Prepositions/\nPhrasal Verbs/Idioms";
  static const String question = "Question ";
  static const String sessionPaused = "Session Paused";
  static const String resume = "Resume";
  static const String popUpForExit = "Are you sure you want to exit?";
  static const String popUpForMsgInExitButton1 = "You have not completed this section.";
  static const String popUpForMsgInExitButton2 = " questions are not yet attempted. Either solve the questions or see the answer/explanation to complete the learning experience.";
  static const String popUpForExitButton = "Do you still want to exit and solve this section later?";
  static const String yesForPopUpExit = "Yes";
  static const String noForPopUpExit = "No";

}

class Gkzonequestions{
  static const String questions ="Prose is to Poetry, said Valéry, as walking is to dancing. That poets regularly produce prose, while prose writers rarely write poetry, is not, as Brodsky argues, evidence of poetry’s superiority. According to Brodsky, “The poet, in principle, is ‘higher’ than the prose writer because a hard-up poet can sit down and compose an article, whereas in similar straits a prose writer would hardly give thought to a poem.” But the point surely is not that writing poetry is less well paid than writing prose but that it is special — the marginalizing of poetry and its audience; that what was once considered a normal skill, like playing a musical instrument, now seems the province of the difficult and the intimidating. Not only prose writers but also cultivated people generally no longer write poetry. (As poetry is no longer, as a matter of course, something to memorize.) Modern performance in literature is partly shaped by the widespread discrediting of the idea of literary virtuosity; by a very real loss of virtuosity. It now seems utterly extraordinary that anyone can write brilliant prose in more than one language; we marvel at a Nabokov, a Beckett, a Cabrera Infante — but until two centuries ago such virtuosity would have been taken for granted. So, until recently, was the ability to write poetry as well as prose.\n";
  static const String qoToQuestion ="GO TO QUESTION";
  static const String submit= "SUBMIT";
  static const String submitForNextButton= "Submit";
  static const String exit= "EXIT";
  static const String emaxSubmitForLastQuestion= "SUBMIT";
  static const String next= "NEXT";
  static const String back= "Back to paragraph";
  static const String prev= "PREV";
  static const String correctStatus= "Status: Correct";
  static const String wrongStatus= "Status: Incorrect. Try Again";
  static const String wrongStatusForReview= "Status: Incorrect";
  static const String skippedStatus= "Status: skipped";
  static const String textForNeTypeQuestion= "Enter your response below.";
  static const String hintTextForNaTypeQuestions= "Enter Response here...";
  static const String messageInSolutionCheckPopup= "You will not able to attempt \n the question after seeing the solution!";
  static  String dontShowMsgInSolutionCheckPopup = "Don't show this again";
  static  String cancelInSolutionCheckPopup = "Cancel";
  static  String okayInSolutionCheckPopup = "Okay";




}

class GkzoneStatisticsScreenStrings{
  static const String Text_BackToQuestion = "Back to question";
  static const String Text_QuestionStats = "Question Status";
  static const String Text_AnsQuestionStats = "Status: Skipped";
  static const String Text_Timetaken = "Time Taken";
  static const String Text_AvgTimetaken= "Avg Time Taken";
  static const String Text_AttemptedBy = "Attempted By";
  static const String Text_PValue= "P-Value";
  static const String Text_ABCApproach = "ABC Approach";
  static const String Text_ValueTimetaken = "00:10:22";
  static const String Text_ValueAvgTimetaken= "00:10:22";
  static const String Text_ValueAttemptedBy = "78%";
  static const String Text_ValueAccurcy = "78%";
  static const String Text_ValuePValue= "52.8%";
  static const String Text_ValueABCApproach = "52.8%";
}

class GkzoneShowAnswerScreenStrings{
  static const String Text_Answer_Emax = "Answer - ";
  static const String Text_Answer = "Answer - ";
  static const String EnterText = "Enter Text is - ";

  static const String Text_Solution = "Solution";
  static const String Text_ParagraphContent = "Poetry is not superior to prose since it was once considered a normal skill expected of any cultivated individual, unlike today, when both poetry and its audience have been marginalized as being difficult and “The poet, in principle, is ‘higher’ than the prose writer because a hard-up poet can sit down and compose an article, whereas in similar straits a prose writer would hardly give thought to a poem.";
}


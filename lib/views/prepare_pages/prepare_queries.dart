import 'package:flutter/material.dart';
import 'package:imsindia/utils/colors.dart';
import 'package:imsindia/views/Arun/ims_utility.dart';
import 'package:imsindia/views/prepare_pages/QuestionsOptionsl.dart';
import 'package:imsindia/resources/strings/prepare.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:imsindia/utils/svg_images/prepare_svg_images.dart';
import 'package:shared_preferences/shared_preferences.dart';


var screenHeight;
var screenWidth;
List<String> VideoData = [];
List<String>  Thumbnail = [];
List<String>  titlesList = [];
List<String>  durationList = [];
List<String>  labelList=[];
List<String>  bookedmarklist=[];

class QueriesHtml extends StatefulWidget {
  final Function func;
  OverlayEntry overlayEntry;
  List<String> bookedmarkVideos;
  QueriesHtml(this.bookedmarkVideos,{Key key , this.func}): super(key: key);

  @override
  QueriesState createState() => QueriesState();


}
class QueriesState extends State<QueriesHtml> {
  bool bookmarkActive = false;
  SharedPreferences bookedmark_prefs;
  int clickedVideo_index;
  int clickedLabel_index;
  static QuestionsOptionsHtml listobj;
  String video_png ;

  _onVideoClicked(int index) {
    setState(() => clickedVideo_index = index);
  }
  void getVideoData_sharePref() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      VideoData = prefs.getStringList("VideoLists");
      Thumbnail = prefs.getStringList("ThumbnailLists");
      durationList = prefs.getStringList("DurationLists");
      titlesList = prefs.getStringList("TitlesLists");
      labelList = prefs.getStringList("LabelLists");

    });

  }

  Future getBookedmarkList_sharePref() async {
    bookedmark_prefs = await SharedPreferences.getInstance();
    setState(() {
      bookedmarklist = bookedmark_prefs.getStringList("bookmarkedVideoLists");
      print(bookedmarklist);
    });
  }




  String _printDuration(int time) {
    Duration duration = new Duration(seconds: time);
    String twoDigits(int n) {
      if (n >= 10) return "$n";
      return "$n";
    }

    String twoDigitMinutes = twoDigits(duration.inMinutes.remainder(60));
    String twoDigitSeconds = twoDigits(duration.inSeconds.remainder(60));
    String minHolder;
    String hourHolder;
    String minsMethod(String min){
      if(min=="1"){
        minHolder = "Min";
      }else{
        minHolder = "Mins";
      }
      return minHolder;
    }
    String hourMethod(int hr){
      if(hr==1){
        hourHolder = " Hr";
      }else{
        hourHolder = " Hrs";
      }
      return hourHolder;
    }
    if(duration.inHours==0){
      minsMethod(twoDigitMinutes);
      return "$twoDigitMinutes $minHolder $twoDigitSeconds Sec";
    }
    else if(twoDigitMinutes=="0"){
      hourMethod(duration.inHours);
      return "${twoDigits(duration.inHours)+ hourHolder} $twoDigitSeconds Sec";

    }
    else{
      hourMethod(duration.inHours);
      minsMethod(twoDigitMinutes);

      return "${twoDigits(duration.inHours)+ hourHolder} $twoDigitMinutes $minHolder $twoDigitSeconds Sec";
    }

  }
  @override
  void initState() {
    VideoData = [];
    Thumbnail = [];
    durationList=[];
    titlesList=[];
    labelList = [];
    getVideoData_sharePref();
    listobj = new QuestionsOptionsHtml();
    // TODO: implement initState
    setState(() {
      print("========================calling");
    });
    super.initState();
  }

  @override
  void didChangeDependencies() {
    //getVideoData_sharePref();
    super.didChangeDependencies();
  }
  @override
  Widget build(BuildContext context) {

    final screenHeight = MediaQuery.of(context).size.height;
    final screenWidth = MediaQuery.of(context).size.width;
    return VideoData.length==0?CircularProgressIndicator():Container(
      color: Colors.transparent,
      child: Container(
        color: NeutralColors.white10,
        child: Column(
          children: <Widget>[
            Expanded(
              flex: 1,
              child: LayoutBuilder(
                  builder: (BuildContext context, BoxConstraints viewportConstraints) {
                    return SingleChildScrollView(
                      child: ConstrainedBox(constraints: BoxConstraints(minHeight: viewportConstraints.maxHeight),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            /** text for number of videos **/
                            Container(
                             height: 20/764*screenHeight,
                              margin: EdgeInsets.only(left: 20/360*screenWidth,top: (25/764)*screenHeight),
                              child:  Text(
                                  VideoData.length.toString()=='1'?VideoData.length.toString()+QueriesScreenStrings.Text_NumberofVideo_single_videoo:
                                VideoData.length.toString()+QueriesScreenStrings.Text_NumberofVideo,
                                style: TextStyle (
                                  color: NeutralColors.dark_navy_blue,
                                  fontWeight: FontWeight.w600,
                                  fontFamily: "IBMPlexSans",
                                  fontStyle: FontStyle.normal,
                                  fontSize: (16/764)*screenHeight,
                                ),
                                textAlign: TextAlign.right,
                              ),
                            ),
                            /** text for Accuracy **/
                            Container(
                              height: 15/764*screenHeight,
                              width: 51/360 * screenWidth,
                              margin: EdgeInsets.only(top: 3/764*screenHeight,left: 20/360*screenWidth,bottom: (16/764)*screenHeight),
                              child:Text(
                                CalculationScreenStrings.Text_Accuracy,
                                style: TextStyle (
                                  color: ImsColors.bluey_grey,
                                  fontWeight: FontWeight.w600,
                                  fontFamily: "IBMPlexSans",
                                  fontStyle: FontStyle.normal,
                                  fontSize: (12/720)*screenHeight,
                                ),
                                textAlign: TextAlign.left,
                              ),
                            ),
                            /** number of videoss**/
                            Container(
                              margin: EdgeInsets.only(top: (16/764)*screenHeight),
                              child: getTextWidgets(screenHeight,screenWidth,widget.func),
                            ),
                            /** comments section **/
//                            Container(
//                              child: CommentsHtml(),
//                            ),
                          ],
                        ),
                        //child: getTextWidgets(list_data,screenHeight,screenWidth),
                      ),
                    );
                  }
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget getTextWidgets(final screenHeight,final screenWidth,Function func)
  {

    List<Widget> list = new List<Widget>();
    for(var i=0; i < VideoData.length; i++){
      list.add(
          new Container(
            margin: EdgeInsets.only(bottom: (34/764)*screenHeight),
            height: 80.0/720*screenHeight,
            child: Row(
              children: <Widget>[
                Container(
                  //color:Colors.white,
                  child: _getChewiePlayer(i,false,screenHeight,screenWidth,func),
//              height:71/764*screenHeight,
//              width:116/360*screenWidth ,
//              child: FittedBox(
//                child: ChewieListItem(
//                  videoPlayerController: VideoPlayerController.network(
//                    CalculationScreenStrings.Video_URL,),
//                ),
//              ),
                ),
                Container(
                  height: 80/764*screenHeight,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Container(
                        // height: 18/764*screenHeight,
                        width: 189/360 * screenWidth,
                        margin: EdgeInsets.only(left:(15/360)*screenWidth),
                        child: Text(
                          titlesList[i].toString(),
                          overflow: TextOverflow.ellipsis,
                          style: TextStyle (
                            color: NeutralColors.dark_navy_blue,
                            fontWeight: FontWeight.w600,
                            fontFamily: "IBMPlexSans",
                            fontStyle: FontStyle.normal,
                            fontSize: (14/360)*screenWidth,
                          ),
                          textAlign: TextAlign.start,
                        ),

                      ),
                      Container(
                        margin: EdgeInsets.only(bottom: 1.0),
                        height: 25/720*screenHeight,
                        child: Row(
                          children: <Widget>[
                            labelList[i]==null?Container():labelList[i]==""?Container():
                            Container(
                              height: 25/764*screenHeight,
                              width: 76/360 * screenWidth,
                              margin: EdgeInsets.only(left: (16/360)*screenWidth),
                              decoration: BoxDecoration(
                                color:NeutralColors.sun_yellow.withOpacity(0.12),
                                borderRadius: BorderRadius.all(Radius.circular(2)),
                              ),
                              child: Center(child:Text(
                                 // CalculationScreenStrings.Text_Concepts,
                                  labelList[i].toString() ,
                                  style: TextStyle (
                                    color: NeutralColors.mango,
                                    fontWeight: FontWeight.w500,
                                    fontFamily: "IBMPlexSans",
                                    fontStyle: FontStyle.normal,
                                    fontSize: (12/360)*screenWidth,
                                  ),
                                  textAlign: TextAlign.center
                              ),),
                            ),
                            Container(
                              //height: 15/764*screenHeight,
                              decoration: BoxDecoration(
                                color:Colors.transparent,
                                borderRadius: BorderRadius.all(Radius.circular(2)),
                              ),
                              margin: EdgeInsets.only(left: labelList[i]==null|| labelList[i]==""?16/360*screenWidth: 10/360*screenWidth),
                              child: Text(
                                _printDuration(int.parse(durationList[i])),
                                style: TextStyle (
                                  color: NeutralColors.blue_grey,
                                  fontWeight: FontWeight.w500,
                                  fontFamily: "IBMPlexSans",
                                  fontStyle: FontStyle.normal,
                                  fontSize: (12/360)*screenWidth,
                                ),
                                textAlign: TextAlign.center,
                              ),
                            )
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          )
      );
    }
    return new Column(children: list);
  }


  _getChewiePlayer(int index,bool offStageFlag,final screenHeight,final screenWidth,Function function){
    Offstage offstage = new Offstage(
      offstage: offStageFlag,
      child: Container(
        color: Colors.white,
         margin: EdgeInsets.only(left: (20 / 360) * screenWidth),
        height:71/720*screenHeight,
        width:116/360*screenWidth ,
        child: InkWell(
          onTap: () {
          setState(() {
            getBookedmarkList_sharePref();
            print("================_getChewiePlayer");
            print(bookedmarklist);
            _onVideoClicked(index);
            try{
              if (bookedmarklist == [] || bookedmarklist.toString() == 'null') {
                setState(() {
                  bookmarkActive = false;
                });

              }
              else {
                for (int j = 0; j < bookedmarklist.length; j++) {
                  int bookedPos = int.parse(bookedmarklist[j]);
                  print("=======prefs========"+VideoData[index]);
                  print(VideoData[bookedPos]);
                  if (VideoData[index] == VideoData[bookedPos]) {
                    setState(() {
                      bookmarkActive = true;
                    });
                    break;
                  } else {
                    setState(() {
                      bookmarkActive = false;
                    });
                  }

                }
              }
            }catch(error){
              print(error);
            }
            function(video_png = Thumbnail[clickedVideo_index],
                labelList[clickedVideo_index], clickedVideo_index,
                bookmarkActive,VideoData[clickedVideo_index]);
          });

          },
          child: Container(
            constraints: new BoxConstraints.expand(
              height:80/720*screenHeight,
            ),
            alignment: Alignment.center,
            decoration:  BoxDecoration(
              borderRadius: new BorderRadius.circular(5.0),
            ),
            child:  Center(
              child: Stack(
                  children: <Widget> [
                    new CachedNetworkImage(
                      imageUrl:Thumbnail[index],
                      imageBuilder: (context, imageProvider) => Container(
                        decoration: BoxDecoration(
                          color: Colors.white,
                          image: DecorationImage(
                              image: imageProvider,
                              fit: BoxFit.cover,
                             ),
                          borderRadius: new BorderRadius.circular(5.0),

                        ),
                      ),
                      placeholder: (context, url) =>
                          Center(
                            child: Container(
                              decoration: BoxDecoration(borderRadius: BorderRadius.circular(5),),
                              height:80/720*screenHeight,
                              child:  Center(
                                child: Image.asset(
                                  "assets/images/thumbnail.png",
                                  fit: BoxFit.contain,
                                  alignment: Alignment.center,
                                ),
                              ),),
                          ),
                      errorWidget: (context, url, error) => 
                      //Icon(Icons.error),
                          Container(
                            decoration: BoxDecoration(borderRadius: BorderRadius.circular(5),),
                            height:80/720*screenHeight,
                            child:  Center(
                              child: Image.asset(
                                "assets/images/thumbnail.png",
                                fit: BoxFit.contain,
                              ),
                            ),),
                      ),
                    Center(child:Align(
                      alignment: Alignment.center,
                      child:  Container(
                        height: (30 / 720) * screenHeight,
                        width: (30 / 360) * screenWidth,
                        child: SvgPicture.asset(
                          getPrepareSvgImages.playwhite,
                          fit: BoxFit.contain,
                        ),
                      ),
                    ),),
                  ]
              ),
            ),
          ),
        ),
      ),
    );
    return offstage;
  }


}

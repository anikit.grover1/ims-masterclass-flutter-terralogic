import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:imsindia/api/bloc/services.dart';
import 'package:imsindia/components/custom_expansion_panel.dart';
import 'package:imsindia/utils/Url.dart';
import 'package:imsindia/utils/colors.dart';
import 'package:imsindia/utils/global.dart' as global;
import 'package:imsindia/utils/svg_images/prepare_svg_images.dart';
import 'package:imsindia/views/prepare_pages/prepare_calculation.dart';
import 'package:imsindia/views/prepare_pages/prepare_home_screen.dart';
import 'package:imsindia/views/prepare_pages/prepare_questions_review_screen.dart';
import 'package:imsindia/views/prepare_pages/prepare_questions_screen.dart';
import 'package:percent_indicator/linear_percent_indicator.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../../resources/strings/prepare.dart';
import '../../routers/routes.dart';
import 'package:connectivity/connectivity.dart';

var screenWidthTotal;
var screenHeightTotal;
var _safeAreaHorizontal;
var _safeAreaVertical;
var screenHeight;
var screenWidth;
var getLearnTestTitle = [];
var toCheckVideoIsPresentOrNot = [];
var toCheckTestIsPresentOrNot = [];
var getLearnTestIds = [];
var questionsAttempt = [];
var totalQuestions = [];
var totalScore = [];
var scoreEarnByStu = [];
var accuracy = [];
var parentID = [];
var isProductAccessForCard = [];
var testStatusForCard = [];
var authorizationInvalidMsg;
var authorizationInvalidMsgForPreSubDetails;
var getIdForSubjectLevelTitle;
var getTitleForSubjectLevelTitle;
bool buttonClickOnlyOnce = false;
bool loadForReviewAndResumeButoon = false;
var authToken;
List insideList = ['Questions', 'Score', 'Accuracy'];
Connectivity connectivity = Connectivity();
final GlobalKey<ScaffoldState> _scaffoldstate = new GlobalKey<ScaffoldState>();
var refreshKey = GlobalKey<RefreshIndicatorState>();

class PrepareSubjectsScreenHtml extends StatefulWidget {
  final PrepareQuestionScreenUsingHtmlState prepareQuestionPage;

  PrepareSubjectsScreenHtml({Key key, this.prepareQuestionPage})
      : super(key: key);

  @override
  PrepareSubjectsScreenHtmlState createState() =>
      PrepareSubjectsScreenHtmlState();
}

class PrepareSubjectsScreenHtmlState extends State<PrepareSubjectsScreenHtml> {
  int _activeMeterIndex;
  ScrollController _scrollController =
      new ScrollController(initialScrollOffset: 50.0);
  double previousOffset;

  /// get subject level data ///
  void getSubjectLevelTitlesAndCardLevelData(Map t) async {
    ApiService()
        .getAPI(URL.GET_LEARN_SUBJECTS_NAMES + getIdForSubjectLevelTitle, t)
        .then((returnValue) {
      print(URL.GET_LEARN_SUBJECTS_NAMES + getIdForSubjectLevelTitle);
      setState(() {
        if (returnValue[0].toString().toLowerCase() ==
            'Fetched Successfully'.toLowerCase()) {
          var totalData = returnValue[1]['data'];
          for (var index = 0; index < totalData.length; index++) {
            if (totalData[index].containsKey('component')) {
              totalQuestions
                  .add(totalData[index]['component']['totalQuestions']);
              totalScore.add(totalData[index]['component']['totalScore']);
            } else {
              totalQuestions.add(null);
              totalScore.add(null);
            }
            getLearnTestTitle.add(totalData[index]['parentName']);
            if (totalData[index].containsKey('testId')) {
              getLearnTestIds.add(totalData[index]['testId']);
            } else if (totalData[index].containsKey('id')) {
              getLearnTestIds.add(totalData[index]['id']);
            } else {
              getLearnTestIds.add(getIdForSubjectLevelTitle);
            }
            toCheckVideoIsPresentOrNot.add(totalData[index]['IsVideo']);
            toCheckTestIsPresentOrNot.add(totalData[index]['IsTest']);
            questionsAttempt.add(totalData[index]['questionAttempt']);
            scoreEarnByStu.add(totalData[index]['score']);
            isProductAccessForCard.add(totalData[index]['isProductAccess']);
            testStatusForCard.add(totalData[index]['testStatus']);
            if (totalData[index]['accuracy'] != null &&
                totalData[index]['accuracy'].runtimeType == double) {
              accuracy.add((totalData[index]['accuracy']).toInt());
            } else {
              accuracy.add(totalData[index]['accuracy']);
            }
            parentID.add(totalData[index]['parentId']);
          }
          print("totalQuestionstotalQuestionstotalQuestions");
          print(totalQuestions);
          print(getLearnTestTitle);
          print(questionsAttempt);
          print(accuracy);
          print(scoreEarnByStu);
          print(getLearnTestIds);
        } else {
          authorizationInvalidMsg = returnValue[0];
        }
      });
    });
  }

  getIdAndTitleForSubjectLevel() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    getIdForSubjectLevelTitle = prefs.getString("idForSubjectLevelTitles");
    getTitleForSubjectLevelTitle =
        prefs.getString("titleForSubjectLevelTitles");
    print("ID FOr SUBJECT LEVEL" +
        getIdForSubjectLevelTitle +
        getTitleForSubjectLevelTitle);
  }

  void setAccessTokenForProdTestLaunch(value) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString("accessToken", value);
  }

  void setTestStatusToCallTestLaunchOrReviseApi(value) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString("testStatus", value);
    print(value + "valueeeeeeeeeee");
    print(prefs.setString("testStatus", value));
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getLearnTestTitle = [];
    toCheckTestIsPresentOrNot = [];
    toCheckVideoIsPresentOrNot = [];
    getLearnTestIds = [];
    questionsAttempt = [];
    totalQuestions = [];
    scoreEarnByStu = [];
    totalScore = [];
    accuracy = [];
    parentID = [];
    isProductAccessForCard = [];
    testStatusForCard = [];
    // TODO: implement initState
    super.initState();
    checkInternet();
    print('init state called');
    connectivity.onConnectivityChanged.listen((result) {
      if (result.toString() == 'ConnectivityResult.none') {
        print('connection lost');
        noInternetMessage('Internet Connection Lost');
      } else {
        print(_scaffoldstate.currentState);
        print("_scaffoldstate.currentState");
        _scaffoldstate.currentState.removeCurrentSnackBar();
        print('connection exist in notifier');
        getLearnTestTitle = [];
        toCheckTestIsPresentOrNot = [];
        toCheckVideoIsPresentOrNot = [];
        getLearnTestIds = [];
        questionsAttempt = [];
        totalQuestions = [];
        scoreEarnByStu = [];
        totalScore = [];
        accuracy = [];
        parentID = [];
        isProductAccessForCard = [];
        testStatusForCard = [];
        getIdAndTitleForSubjectLevel().then((returnValue) {
          global.getToken.then((t) {
            authToken = t;
            getSubjectLevelTitlesAndCardLevelData(t);
          });
        });
      }
    });
  }

  Future<Null> checkInternet() async {
    var connectivityResult = await (Connectivity().checkConnectivity());
    if (connectivityResult.toString() != 'ConnectivityResult.none') {
      setState(() {
        getLearnTestTitle = [];
        toCheckTestIsPresentOrNot = [];
        toCheckVideoIsPresentOrNot = [];
        getLearnTestIds = [];
        questionsAttempt = [];
        totalQuestions = [];
        scoreEarnByStu = [];
        totalScore = [];
        accuracy = [];
        parentID = [];
        isProductAccessForCard = [];
        testStatusForCard = [];
        getIdAndTitleForSubjectLevel().then((returnValue) {
          global.getToken.then((t) {
            authToken = t;
            getSubjectLevelTitlesAndCardLevelData(t);
          });
        });
      });
    } else {
      noInternetMessage('Internet Connection Lost');
    }

    return null;
  }

  void noInternetMessage(String value) {
    _scaffoldstate.currentState.showSnackBar(new SnackBar(
      content: new Text(value),
      action: SnackBarAction(
        textColor: Colors.blueAccent,
        label: "Dismiss",
        onPressed: () {
          // Some code to undo the change.
        },
      ),
      backgroundColor: Colors.blueGrey,
      duration: Duration(days: 365),
    ));
  }

  void showErrorMessage(String value) {
    _scaffoldstate.currentState.showSnackBar(new SnackBar(
      content: new Text(value),
      duration: new Duration(seconds: 3),
    ));
  }

  Widget build(BuildContext context) {
    var _mediaQueryData = MediaQuery.of(context);
    screenWidthTotal = _mediaQueryData.size.width;
    screenHeightTotal = _mediaQueryData.size.height;
    _safeAreaHorizontal =
        _mediaQueryData.padding.left + _mediaQueryData.padding.right;
    _safeAreaVertical =
        _mediaQueryData.padding.top + _mediaQueryData.padding.bottom;
    screenHeight = screenHeightTotal - _safeAreaVertical;
    screenWidth = screenWidthTotal - _safeAreaHorizontal;

    return Scaffold(
        key: _scaffoldstate,
        body: Container(
          color: NeutralColors.pureWhite,
          child: Column(
            children: <Widget>[
              Container(
                margin: EdgeInsets.only(
                  top: (60 / 720) * screenHeight,
                ),
                child: Row(
                  //crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    InkWell(
                      onTap: () {
                        Navigator.pop(context, PrepareHomeScreenHtml());
                      },
                      child: Container(
                        width: (58 / 360) * screenWidth,
                        height: (30.0 / 678) * screenHeight,
                        margin: EdgeInsets.only(left: 0.0),
                        child: SvgPicture.asset(
                          getPrepareSvgImages.backIcon,
                          height: (5 / 678) * screenHeight,
                          width: (14 / 360) * screenWidth,
                          fit: BoxFit.none,
                        ),
                      ),
                    ),
                    Container(
                      height: (20 / 678) * screenHeight,
                      // margin: EdgeInsets.only(left: (20 / 360) * screenWidth),
                      child: Text(
                        //"",
                        // global.titleForPrepareSubjectsScreen,
                        getTitleForSubjectLevelTitle == null
                            ? "Prepare"
                            : getTitleForSubjectLevelTitle,
                        style: TextStyle(
                          color: NeutralColors.black,
                          fontSize: (16 / 678) * screenHeight,
                          fontFamily: "IBMPlexSans",
                          fontWeight: FontWeight.w700,
                        ),
                        textAlign: TextAlign.left,
                      ),
                    ),
                  ],
                ),
              ),
              authToken == ''
                  ? new Container(
                      margin:
                          EdgeInsets.only(top: (300.0 / 720) * screenHeight),
                      child:
                          Center(child: Text('Authorization Token Expected')))
                  : authorizationInvalidMsg != null
                      ? new Container(
                          margin: EdgeInsets.only(
                              top: (300.0 / 720) * screenHeight),
                          child: Center(
                              child: Text(
                                  'Invalid Token "${authorizationInvalidMsg}" ')))
                      : getLearnTestTitle.length == 0
                          ? new Container(
                              margin: EdgeInsets.only(
                                  top: (300.0 / 720) * screenHeight),
                              child: Center(child: CircularProgressIndicator()))
                          : new Expanded(
                              child: new ListView.builder(
                                //  controller: _scrollController,
                                shrinkWrap: true,
                                padding: EdgeInsets.only(
                                    top: 10 / 720 * screenHeight),
                                itemBuilder: (BuildContext context, int i) {
                                  return getLearnTestTitle[i] == null
                                      ? Container()
                                      : Container(
                                          margin: EdgeInsets.only(
                                              top: (10.0 / 678) * screenHeight,
                                              left: (20.0 / 360) * screenWidth,
                                              right: (20.0 / 360) * screenWidth,
                                              bottom: (i ==
                                                      getLearnTestTitle.length -
                                                          1)
                                                  ? 40 / 720 * screenHeight
                                                  : 0),
                                          decoration: BoxDecoration(
                                              boxShadow: [
                                                BoxShadow(
                                                  //offset: Offset(0, 5),
                                                  blurRadius: 10,
                                                  color:
                                                      (_activeMeterIndex == i)
                                                          ? Color(0xffeaeaea)
                                                          : Colors.transparent,
                                                  //offset: Offset.lerp(Offset(10.0,-10.0), Offset(10.0,10.0), 1),
                                                  // offset: Offset(0,10.0),
                                                  //color: Colors.orange,
                                                ),
                                              ],
                                              borderRadius: new BorderRadius
                                                      .only(
                                                  topLeft:
                                                      new Radius.circular(5),
                                                  topRight:
                                                      new Radius.circular(5),
                                                  bottomLeft:
                                                      new Radius.circular(5),
                                                  bottomRight:
                                                      new Radius.circular(5)),
                                              border: new Border.all(
                                                  color: (_activeMeterIndex ==
                                                          i)
                                                      ? Colors.transparent
                                                      : NeutralColors.gunmetal
                                                          .withOpacity(0.1),
                                                  width: 1.0)),
                                          child: CustomExpansionPanelList(
                                            expansionHeaderHeight:
                                                (45 / 720) * screenHeight,
                                            iconColor: (_activeMeterIndex == i)
                                                ? NeutralColors.pureWhite
                                                : NeutralColors.black,
                                            backgroundColor1:
                                                (_activeMeterIndex == i)
                                                    ? NeutralColors.purply
                                                    : NeutralColors.pureWhite,
                                            backgroundColor2:
                                                (_activeMeterIndex == i)
                                                    ? NeutralColors.purply
                                                    : NeutralColors.pureWhite,
                                            expansionCallback: (
                                              int index,
                                              isExpanded,
                                            ) {
                                              setState(() {
                                                _activeMeterIndex =
                                                    _activeMeterIndex == i
                                                        ? null
                                                        : i;
                                              });
                                            },
                                            children: [
                                              new ExpansionPanel(
                                                  canTapOnHeader: true,
                                                  isExpanded:
                                                      (_activeMeterIndex == i),
                                                  headerBuilder: (BuildContext
                                                              context,
                                                          bool isExpanded) =>
                                                      new Container(
                                                        alignment: Alignment
                                                            .centerLeft,
                                                        child: new Padding(
                                                          padding: EdgeInsets.only(
                                                              left: (15 / 320) *
                                                                  screenWidth),
                                                          child: new Text(
                                                            getLearnTestTitle[
                                                                i],
                                                            style: new TextStyle(
                                                                fontSize: (15 /
                                                                        678) *
                                                                    screenHeight,
                                                                fontFamily:
                                                                    "IBMPlexSans",
                                                                fontWeight:
                                                                    FontWeight
                                                                        .w500,
                                                                color: (_activeMeterIndex ==
                                                                        i)
                                                                    ? NeutralColors
                                                                        .pureWhite
                                                                    : NeutralColors
                                                                        .charcoal_grey),
                                                          ),
                                                        ),
                                                      ),
                                                  body: Container(
                                                    color: Colors.white,
                                                    height: (275 / 678) *
                                                        screenHeight,
                                                    child: Column(
                                                      children: <Widget>[
                                                        /// questions,accuracy and score part..............
                                                        (toCheckTestIsPresentOrNot[
                                                                        i] ==
                                                                    true &&
                                                                totalQuestions[
                                                                        i] !=
                                                                    0 &&
                                                                totalQuestions[
                                                                        i] !=
                                                                    "0" &&
                                                                totalQuestions[
                                                                        i] !=
                                                                    null &&
                                                                totalQuestions[
                                                                        i] !=
                                                                    "null")
                                                            ? new Expanded(
                                                                flex: 3,
                                                                child: ListView(
                                                                  controller:
                                                                      _scrollController,
                                                                  physics:
                                                                      const NeverScrollableScrollPhysics(),
                                                                  children: <
                                                                      Widget>[
                                                                    new Container(
                                                                      width: (320 /
                                                                              360) *
                                                                          screenWidth,
                                                                      decoration: new BoxDecoration(
                                                                          border:
                                                                              new Border.all(color: NeutralColors.pureWhite),
                                                                          color: NeutralColors.pureWhite),
                                                                      child:
                                                                          new Column(
                                                                        mainAxisAlignment:
                                                                            MainAxisAlignment.start,
                                                                        children: <
                                                                            Widget>[
                                                                          Container(
                                                                              width: (320 / 360) * screenWidth,
                                                                              margin: EdgeInsets.only(top: (12 / 678) * screenHeight, left: (15 / 360) * screenWidth),
                                                                              child: new Text(
                                                                                insideList[0],
                                                                                style: TextStyle(
                                                                                  color: NeutralColors.blue_grey,
                                                                                  fontSize: (14 / 678) * screenHeight,
                                                                                  fontFamily: "IBMPlexSans",
                                                                                  fontWeight: FontWeight.w500,
                                                                                ),
                                                                                textAlign: TextAlign.left,
                                                                              )),
                                                                          Container(
                                                                              width: (320 / 360) * screenWidth,
                                                                              margin: EdgeInsets.only(top: (5 / 678) * screenHeight, left: (15 / 360) * screenWidth, right: (15 / 360) * screenWidth),
                                                                              child: new Row(
                                                                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                                                children: <Widget>[
                                                                                  Text(
                                                                                    (questionsAttempt[i].toString() == "null" || totalQuestions[i].toString() == "null") ? "0/" + totalQuestions[i].toString() : (questionsAttempt[i].toString() + "/" + totalQuestions[i].toString()),
                                                                                    style: TextStyle(
                                                                                      color: NeutralColors.black,
                                                                                      fontSize: (14 / 678) * screenHeight,
                                                                                      fontFamily: "IBMPlexSans",
                                                                                      fontWeight: FontWeight.w500,
                                                                                    ),
                                                                                    textAlign: TextAlign.left,
                                                                                  ),
                                                                                  Container(
                                                                                    margin: EdgeInsets.only(right: (0 / 360) * screenWidth),
                                                                                    width: (200 / 360) * screenWidth,
                                                                                    height: (2.3 / 678) * screenHeight,
                                                                                    decoration: BoxDecoration(),
                                                                                    child: LinearPercentIndicator(
                                                                                      percent: ((questionsAttempt[i] != null && totalQuestions[i] != null) && questionsAttempt[i] / totalQuestions[i] > 1.0) ? 1.0 : (questionsAttempt[i] != null && totalQuestions[i] != null) ? questionsAttempt[i] / totalQuestions[i] : 0,
                                                                                      progressColor: SemanticColors.dark_purpely,
                                                                                      backgroundColor: SemanticColors.dark_purpely.withOpacity(0.1),
                                                                                      lineHeight: 4.25 / 720 * screenHeight,
                                                                                      width: 200 / 360 * screenWidth,
                                                                                    ),
                                                                                  )
                                                                                ],
                                                                              )),
                                                                        ],
                                                                      ),
                                                                    ),
                                                                    new Container(
                                                                      width: (320 /
                                                                              360) *
                                                                          screenWidth,
                                                                      decoration: new BoxDecoration(
                                                                          border:
                                                                              new Border.all(color: NeutralColors.pureWhite),
                                                                          color: NeutralColors.pureWhite),
                                                                      child:
                                                                          new Column(
                                                                        mainAxisAlignment:
                                                                            MainAxisAlignment.start,
                                                                        children: <
                                                                            Widget>[
                                                                          Container(
                                                                              width: (320 / 360) * screenWidth,
                                                                              margin: EdgeInsets.only(top: (12 / 678) * screenHeight, left: (15 / 360) * screenWidth),
                                                                              child: new Text(
                                                                                insideList[1],
                                                                                style: TextStyle(
                                                                                  color: NeutralColors.blue_grey,
                                                                                  fontSize: (14 / 678) * screenHeight,
                                                                                  fontFamily: "IBMPlexSans",
                                                                                  fontWeight: FontWeight.w500,
                                                                                ),
                                                                                textAlign: TextAlign.left,
                                                                              )),
                                                                          Container(
                                                                              width: (320 / 360) * screenWidth,
                                                                              margin: EdgeInsets.only(top: (5 / 678) * screenHeight, left: (15 / 360) * screenWidth, right: (15 / 360) * screenWidth),
                                                                              child: new Row(
                                                                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                                                children: <Widget>[
                                                                                  Text(
                                                                                    (scoreEarnByStu[i].toString() == "null" || totalScore[i].toString() == "null") ? ("0/" + totalScore[i].toString()) : scoreEarnByStu[i].runtimeType == int ? (scoreEarnByStu[i].toString() + '/' + totalScore[i].toString()) : double.parse(scoreEarnByStu[i]).toStringAsFixed(0) + '/' + totalScore[i].toString(),
                                                                                    style: TextStyle(
                                                                                      color: NeutralColors.black,
                                                                                      fontSize: (14 / 678) * screenHeight,
                                                                                      fontFamily: "IBMPlexSans",
                                                                                      fontWeight: FontWeight.w500,
                                                                                    ),
                                                                                    textAlign: TextAlign.left,
                                                                                  ),
                                                                                  Container(
                                                                                    margin: EdgeInsets.only(right: (0 / 360) * screenWidth),
                                                                                    width: (200 / 360) * screenWidth,
                                                                                    height: (2.3 / 678) * screenHeight,
                                                                                    decoration: BoxDecoration(),
                                                                                    child: LinearPercentIndicator(
                                                                                      percent: (scoreEarnByStu[i] != null && totalScore[i] != null) ? scoreEarnByStu[i].runtimeType == int ? scoreEarnByStu[i] / totalScore[i] : double.parse(scoreEarnByStu[i]) / totalScore[i].toDouble() : 0,
                                                                                      progressColor: SemanticColors.dark_purpely,
                                                                                      backgroundColor: SemanticColors.dark_purpely.withOpacity(0.1),
                                                                                      lineHeight: 4.25 / 720 * screenHeight,
                                                                                      width: 200 / 360 * screenWidth,
                                                                                    ),
                                                                                  )
                                                                                ],
                                                                              )),
                                                                        ],
                                                                      ),
                                                                    ),
                                                                    new Container(
                                                                      width: (320 /
                                                                              360) *
                                                                          screenWidth,
                                                                      decoration: new BoxDecoration(
                                                                          border:
                                                                              new Border.all(color: NeutralColors.pureWhite),
                                                                          color: NeutralColors.pureWhite),
                                                                      child:
                                                                          new Column(
                                                                        mainAxisAlignment:
                                                                            MainAxisAlignment.start,
                                                                        children: <
                                                                            Widget>[
                                                                          Container(
                                                                              width: (320 / 360) * screenWidth,
                                                                              margin: EdgeInsets.only(top: (12 / 678) * screenHeight, left: (15 / 360) * screenWidth),
                                                                              child: new Text(
                                                                                insideList[2],
                                                                                style: TextStyle(
                                                                                  color: NeutralColors.blue_grey,
                                                                                  fontSize: (14 / 678) * screenHeight,
                                                                                  fontFamily: "IBMPlexSans",
                                                                                  fontWeight: FontWeight.w500,
                                                                                ),
                                                                                textAlign: TextAlign.left,
                                                                              )),
                                                                          Container(
                                                                              width: (320 / 360) * screenWidth,
                                                                              margin: EdgeInsets.only(top: (5 / 678) * screenHeight, left: (15 / 360) * screenWidth, right: (15 / 360) * screenWidth),
                                                                              child: new Row(
                                                                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                                                children: <Widget>[
                                                                                  Text(
                                                                                    (accuracy[i].toString() == "null") ? "0/100" : (accuracy[i].toString() + '/100'),
                                                                                    style: TextStyle(
                                                                                      color: NeutralColors.black,
                                                                                      fontSize: (14 / 678) * screenHeight,
                                                                                      fontFamily: "IBMPlexSans",
                                                                                      fontWeight: FontWeight.w500,
                                                                                    ),
                                                                                    textAlign: TextAlign.left,
                                                                                  ),
                                                                                  Container(
                                                                                    margin: EdgeInsets.only(right: (0 / 360) * screenWidth),
                                                                                    width: (200 / 360) * screenWidth,
                                                                                    height: (2.3 / 678) * screenHeight,
                                                                                    decoration: BoxDecoration(),
                                                                                    child: LinearPercentIndicator(
                                                                                      percent: accuracy[i] != null ? accuracy[i] / 100 : 0,
                                                                                      progressColor: SemanticColors.dark_purpely,
                                                                                      backgroundColor: SemanticColors.dark_purpely.withOpacity(0.1),
                                                                                      lineHeight: 4.25 / 720 * screenHeight,
                                                                                      width: 200 / 360 * screenWidth,
                                                                                    ),
                                                                                  )
                                                                                ],
                                                                              )),
                                                                        ],
                                                                      ),
                                                                    )
                                                                  ],
                                                                ),
                                                              )
                                                            : Container(),

                                                        /// learn ,solve product access part .........
                                                        isProductAccessForCard[
                                                                    i] ==
                                                                false
                                                            ?

                                                            /// product access button .............
                                                            Container(
                                                                margin: EdgeInsets.only(
                                                                    left: 15 /
                                                                        360 *
                                                                        screenWidth,
                                                                    top: 49.8 /
                                                                        678 *
                                                                        screenHeight,
                                                                    bottom: 34 /
                                                                        720 *
                                                                        screenHeight),
                                                                child: Row(
                                                                  children: <
                                                                      Widget>[
                                                                    Container(
                                                                      height: 30 /
                                                                          720 *
                                                                          screenHeight,
                                                                      width: 30 /
                                                                          360 *
                                                                          screenWidth,
                                                                      margin: EdgeInsets.only(
                                                                          right: 15 /
                                                                              360 *
                                                                              screenWidth),
                                                                      decoration: BoxDecoration(
                                                                          color: Colors
                                                                              .transparent,
                                                                          shape: BoxShape
                                                                              .circle,
                                                                          border:
                                                                              Border.all(color: SemanticColors.light_ice_blue)),
                                                                      child: Center(
                                                                          child: Icon(
                                                                        Icons
                                                                            .lock,
                                                                        color: SemanticColors
                                                                            .blueGrey,
                                                                        size:
                                                                            11,
                                                                      )),
                                                                    ),
                                                                    Text(
                                                                      PrepareSubjectsScreenStrings
                                                                          .productAccessForCard,
                                                                      style: TextStyle(
                                                                          fontSize: 13 /
                                                                              360 *
                                                                              screenWidth,
                                                                          fontFamily:
                                                                              "IBMPlexSans",
                                                                          fontWeight: FontWeight
                                                                              .w500,
                                                                          color:
                                                                              SemanticColors.blueGrey),
                                                                    )
                                                                  ],
                                                                ),
                                                              )
                                                            :

                                                            /// learn and solve part .................
                                                            Expanded(
                                                                flex: 2,
                                                                child: Row(
                                                                  crossAxisAlignment:
                                                                      CrossAxisAlignment
                                                                          .start,
                                                                  mainAxisAlignment:
                                                                      MainAxisAlignment
                                                                          .center,
                                                                  children: [
                                                                    (toCheckVideoIsPresentOrNot[i] ==
                                                                                true &&
                                                                            ((toCheckTestIsPresentOrNot[i] == true &&
                                                                                totalQuestions[i] != 0 &&
                                                                                totalQuestions[i] != "0" &&
                                                                                totalQuestions[i] != null &&
                                                                                totalQuestions[i] != "null")))
                                                                        ? Container(
                                                                            width:
                                                                                (130 / 360) * screenWidth,
                                                                            height:
                                                                                (40 / 678) * screenHeight,
                                                                            margin:
                                                                                EdgeInsets.only(left: 43 / 678 * screenWidth, top: 36.8 / 678 * screenHeight),
                                                                            decoration:
                                                                                BoxDecoration(
                                                                              gradient: LinearGradient(
                                                                                begin: Alignment(0, 0),
                                                                                end: Alignment(1.0199999809265137, 1.0099999904632568),
                                                                                colors: [
                                                                                  SemanticColors.light_purpely,
                                                                                  SemanticColors.dark_purpely,
                                                                                ],
                                                                              ),
                                                                              borderRadius: BorderRadius.all(Radius.circular(2)),
                                                                            ),
                                                                            child:
                                                                                FlatButton(
                                                                              onPressed: () {
                                                                                AppRoutes.push(context, PrepareCalculationWidgetHtml(parentID[i], getLearnTestTitle[i], global.titleForPrepareSubjectsScreen, getLearnTestIds[i]));
                                                                              },
                                                                              shape: RoundedRectangleBorder(
                                                                                borderRadius: BorderRadius.all(Radius.circular(2)),
                                                                              ),
                                                                              textColor: NeutralColors.pureWhite,
                                                                              padding: EdgeInsets.all(0),
                                                                              child: Text(
                                                                                PrepareSubjectsScreenStrings.learnString,
                                                                                style: TextStyle(
                                                                                  fontSize: 14,
                                                                                  fontFamily: "IBMPlexSans",
                                                                                  fontWeight: FontWeight.w500,
                                                                                ),
                                                                                textAlign: TextAlign.center,
                                                                              ),
                                                                            ),
                                                                          )
                                                                        : (toCheckVideoIsPresentOrNot[i] == true && (toCheckTestIsPresentOrNot[i] != true || totalQuestions[i] == 0 || totalQuestions[i] == "0" || totalQuestions[i] == null || totalQuestions[i] == "null"))
                                                                            ? Center(
                                                                                child: Container(
                                                                                  width: (270 / 360) * screenWidth,
                                                                                  height: (40 / 678) * screenHeight,
                                                                                  margin: EdgeInsets.only(
                                                                                    top: 211 / 720 * screenHeight,
                                                                                    left: 23 / 360 * screenWidth,
                                                                                    right: 23 / 360 * screenWidth,
                                                                                  ),
                                                                                  decoration: BoxDecoration(
                                                                                    gradient: LinearGradient(
                                                                                      begin: Alignment(0, 0),
                                                                                      end: Alignment(1.0199999809265137, 1.0099999904632568),
                                                                                      colors: [
                                                                                        SemanticColors.light_purpely,
                                                                                        SemanticColors.dark_purpely,
                                                                                      ],
                                                                                    ),
                                                                                    borderRadius: BorderRadius.all(Radius.circular(2)),
                                                                                  ),
                                                                                  child: FlatButton(
                                                                                    onPressed: () {
                                                                                      AppRoutes.push(context, PrepareCalculationWidgetHtml(parentID[i], getLearnTestTitle[i], global.titleForPrepareSubjectsScreen, getLearnTestIds[i]));
                                                                                    },
                                                                                    shape: RoundedRectangleBorder(
                                                                                      borderRadius: BorderRadius.all(Radius.circular(2)),
                                                                                    ),
                                                                                    textColor: NeutralColors.pureWhite,
                                                                                    padding: EdgeInsets.all(0),
                                                                                    child: Text(
                                                                                      PrepareSubjectsScreenStrings.learnString,
                                                                                      style: TextStyle(
                                                                                        fontSize: 14,
                                                                                        fontFamily: "IBMPlexSans",
                                                                                        fontWeight: FontWeight.w500,
                                                                                      ),
                                                                                      textAlign: TextAlign.center,
                                                                                    ),
                                                                                  ),
                                                                                ),
                                                                              )
                                                                            : Text(""),
                                                                    toCheckVideoIsPresentOrNot[i] ==
                                                                            true
                                                                        ? Spacer()
                                                                        : Container(),
                                                                    (toCheckTestIsPresentOrNot[i] == true &&
                                                                            totalQuestions[i] !=
                                                                                0 &&
                                                                            totalQuestions[i] !=
                                                                                "0" &&
                                                                            totalQuestions[i] !=
                                                                                null &&
                                                                            totalQuestions[i] !=
                                                                                "null")
                                                                         /// related to solve,resume and reveiw part ................
                                                                        ? Container(
                                                                            width: (toCheckTestIsPresentOrNot[i] == true && toCheckVideoIsPresentOrNot[i] == true)
                                                                                ? (130 / 360) * screenWidth
                                                                                : (270 / 360) * screenWidth,
                                                                            height:
                                                                                (40 / 678) * screenHeight,
                                                                            margin:
                                                                                EdgeInsets.only(
                                                                              right: toCheckVideoIsPresentOrNot[i] == true ? 43 / 678 * screenWidth : 0,
                                                                              top: 36.8 / 678 * screenHeight,
                                                                              bottom: 24 / 678 * screenHeight,
                                                                            ),
                                                                            decoration:
                                                                                BoxDecoration(
                                                                              gradient: LinearGradient(
                                                                                begin: Alignment(0, 0),
                                                                                end: Alignment(1.0199999809265137, 1.0099999904632568),
                                                                                colors: [
                                                                                  SemanticColors.light_purpely,
                                                                                  SemanticColors.dark_purpely,
                                                                                ],
                                                                              ),
                                                                              borderRadius: BorderRadius.all(Radius.circular(2)),
                                                                            ),
                                                                            child:
                                                                                FlatButton(
                                                                              onPressed: () {
                                                                                setTestStatusToCallTestLaunchOrReviseApi(testStatusForCard[i]);
                                                                                if (buttonClickOnlyOnce == false) {
                                                                                  setState(() {
                                                                                    buttonClickOnlyOnce = true;
                                                                                    loadForReviewAndResumeButoon = true;
                                                                                  });
                                                                                  if (global.headersWithAuthorizationKey['Authorization'] != '') {
                                                                                    Map postdata = {
                                                                                      "testId": getLearnTestIds[i].toString()
                                                                                    };
                                                                                    print("+++++++++++++++++++++++++++++++++++++++++++++++SUBJECT ID");
                                                                                    print(getLearnTestIds[i].toString());

                                                                                    ApiService().postAPI(URL.TEST_LAUNCH_FOR_YES_PREPARE, postdata, authToken).then((result) {
                                                                                      setState(() {
                                                                                        if (result[0].toString().toLowerCase() == 'successfully resume'.toLowerCase() || result[0].toString().toLowerCase() == 'successfully created'.toLowerCase()) {
                                                                                          var totalData = result[1]['data'];
                                                                                          global.tokenForTestLaunchApi = totalData['token'];
                                                                                          setAccessTokenForProdTestLaunch(totalData['token']);
                                                                                          buttonClickOnlyOnce = false;
                                                                                          if ((testStatusForCard[i] == 'Completed')) {
                                                                                            print("inside review screen");
                                                                                            print(totalQuestions[i]);
                                                                                            print(totalQuestions);
                                                                                            Navigator.of(context).push(new MaterialPageRoute(builder: (_) => PrepareQuestionReviewScreenUsingHtml())).then((value) {
                                                                                              print("valueeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee");
                                                                                              print(value);
                                                                                              if (value == true) {
                                                                                                setState(() {
                                                                                                  print("coming here");
                                                                                                  loadForReviewAndResumeButoon = false;
                                                                                                  print(loadForReviewAndResumeButoon);
                                                                                                  print("loadForReviewAndResumeButoonloadForReviewAndResumeButoon");
                                                                                                });
                                                                                              }
                                                                                            });
                                                                                          } else {
                                                                                            print("inside resume session");
                                                                                            setState(() {
                                                                                              Navigator.of(context)
                                                                                                  .push(
                                                                                                new MaterialPageRoute(
                                                                                                    builder: (_) => PrepareQuestionScreenUsingHtml()),
                                                                                              )
                                                                                                  .then((value) {
                                                                                                if (value == true) {
                                                                                                  setState(() {
                                                                                                    loadForReviewAndResumeButoon = false;
                                                                                                    print("inside set state");
                                                                                                    getLearnTestTitle = [];
                                                                                                    toCheckTestIsPresentOrNot = [];
                                                                                                    toCheckVideoIsPresentOrNot = [];
                                                                                                    getLearnTestIds = [];
                                                                                                    questionsAttempt = [];
                                                                                                    totalQuestions = [];
                                                                                                    scoreEarnByStu = [];
                                                                                                    totalScore = [];
                                                                                                    accuracy = [];
                                                                                                    parentID = [];
                                                                                                    isProductAccessForCard = [];
                                                                                                    testStatusForCard = [];
                                                                                                    getIdAndTitleForSubjectLevel().then((returnValue) {
                                                                                                      global.getToken.then((t) {
                                                                                                        authToken = t;
                                                                                                        getSubjectLevelTitlesAndCardLevelData(t);
                                                                                                      });
                                                                                                    });
                                                                                                    // refresh page 1 here, you may want to reload your SharedPreferences here according to your needs
                                                                                                  });
                                                                                                } else {}
                                                                                              });
                                                                                            });
                                                                                          }
                                                                                        } else {}
                                                                                      });
                                                                                    });
                                                                                  }
                                                                                }
                                                                              },
                                                                              shape: RoundedRectangleBorder(
                                                                                borderRadius: BorderRadius.all(Radius.circular(2)),
                                                                              ),
                                                                              textColor: NeutralColors.pureWhite,
                                                                              padding: EdgeInsets.all(0),
                                                                              child: Text(
                                                                                loadForReviewAndResumeButoon == true ?
                                                                                PrepareSubjectsScreenStrings.waitString :
                                                                                (testStatusForCard[i] == 'Start') ? PrepareSubjectsScreenStrings.sloveString :
                                                                                (testStatusForCard[i] == 'In-Progress') ? PrepareSubjectsScreenStrings.ResumeString :
                                                                                ((testStatusForCard[i] == 'Completed')) ? PrepareSubjectsScreenStrings.ReviewString : "abc",
                                                                                style: TextStyle(
                                                                                  fontSize: 14,
                                                                                  fontFamily: "IBMPlexSans",
                                                                                  fontWeight: FontWeight.w500,
                                                                                ),
                                                                                textAlign: TextAlign.center,
                                                                              ),
                                                                            ),
                                                                          )
                                                                        : Text(
                                                                            ""),
                                                                  ],
                                                                ),
                                                              ),
                                                      ],
                                                    ),
                                                  )),
                                            ],
                                          ),
                                        );
                                },
                                itemCount: getLearnTestTitle.length,
                              ),
                            ),
            ],
          ),
        ));
  }
}

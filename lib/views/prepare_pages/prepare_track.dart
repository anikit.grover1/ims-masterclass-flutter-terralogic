import 'package:f_logs/model/flog/flog.dart';
import 'package:queries/collections.dart';

class Track {
  String areaName;
  bool attempt;
  String companyCode;
  int difficultyId;
  String difficultyName;
  String enteredText;
  String id;
  bool isCorrect;
  String isSectionCompleted;
  int itemId;
  String itemType;
  bool marked;
  dynamic negativePoints;
  dynamic points;
  int questionIndex;
  int sectionId;
  int sectionIndex;
  String sectionName;
  String selectedOptionId;
  String studentId;
  int subjectId;
  String subjectName;
  String testId;
  int timeRemaining;
  String timeStamp;
  int timeTaken;
  int topicId;
  String topicName;
  int trackNumber;
  int saved;
  int areaid;
  bool submitted;
  int questionId;

  Track({this.areaName,this.attempt,this.companyCode,this.difficultyId,
    this.difficultyName,this.enteredText,this.id,this.isCorrect,
    this.isSectionCompleted,this.itemId,this.itemType,this.marked,
    this.negativePoints,this.points,this.questionIndex,this.sectionId,this.sectionIndex,
    this.sectionName,this.selectedOptionId,this.studentId,this.subjectId,
    this.subjectName,this.testId,this.timeRemaining,this.timeStamp,this.timeTaken,
    this.topicId,this.topicName,this.trackNumber,this.saved,this.areaid});
  Track.Comment(this.areaName,this.attempt,this.companyCode,this.difficultyId,
      this.difficultyName,this.enteredText,this.id,this.isCorrect,
      this.isSectionCompleted,this.itemId,this.itemType,this.marked,
      this.negativePoints,this.points,this.questionIndex,this.sectionId,this.sectionIndex,
      this.sectionName,this.selectedOptionId,this.studentId,this.subjectId,
      this.subjectName,this.testId,this.timeRemaining,this.timeStamp,this.timeTaken,
      this.topicId,this.topicName,this.trackNumber,this.saved,this.areaid);
  Track.Response(this.attempt,this.enteredText,this.marked,
      this.questionId,this.sectionId,this.selectedOptionId,this.submitted,this.timeTaken);
//  {
//    this.areaName= areaName;
//    this.attempt = attempt;
//    this.companyCode = companyCode;
//    this.difficultyId = difficultyId;
//    this.difficultyName = difficultyName;
//    this.enteredText = enteredText;
//    this.id = id;
//    this.isCorrect = isCorrect;
//    this.isSectionCompleted= isSectionCompleted;
//    this.itemId= itemId;
//    this.itemType= itemType;
//    this.marked= marked;
//    this.negativePoints=negativePoints;
//    this.points=points;
//    this.questionIndex=questionIndex;
//    this.sectionId=sectionId;
//    this.sectionIndex=sectionIndex;
//    this.sectionName=sectionName;
//    this.selectedOptionId=selectedOptionId;
//    this.studentId=studentId;
//    this.subjectId=subjectId;
//    this.subjectName=subjectName;
//    this.testId=testId;
//    this.timeRemaining=timeRemaining;
//    this.timeStamp=timeStamp;
//    this.timeTaken=timeTaken;
//    this.topicId=topicId;
//    this.topicName=topicName;
//    this.trackNumber=trackNumber;
//    this.saved=saved;
//    this.areaid=areaid;
//  }
}

class SigObject extends Track{
  static final SigObject _singleton = new SigObject._internal();
  var noOfTracks = 0;
  var list_track_prepare = new List();
  var list_response_preapare = new List();

  var list_name_sync = [];
  var result;
  AddtoSist(Track t) {
    print("track to be added"+t.areaName);
    FLog.info(
      dataLogType: "Debug",
      text: t.trackNumber.toString()+"&&&&&&&&&&&&"+(t.questionIndex).toString()+"&&&&&&&&&"+(t.subjectName).toString()+"&&&&&&&"+(t.topicName).toString()+"&&&&&&&"+t.studentId.toString()+"======================================================TRACK NUMBER && QUESTION INDEX && SUBJECT NAME && AREA/TOPIC NAME  && STUDENT USER ID IN ''''AddtoSist'''' FUNCTION",
    );
    list_track_prepare.add(t);
    FLog.info(
      dataLogType: "Debug",
      text: t.trackNumber.toString()+"&&&&&&&&&&&&"+(t.questionIndex).toString()+"&&&&&&&&&"+(t.subjectName).toString()+"&&&&&&&"+(t.topicName).toString()+"&&&&&&&"+t.studentId.toString()+"======================================================TRACK NUMBER && QUESTION INDEX && SUBJECT NAME && AREA/TOPIC NAME  && STUDENT USER ID IN ''''AddtoSist'''' FUNCTION SUCCESSFULLY ADDED TO LIST '''list_track_prepare''' ",
    );
  }

  AddtoResponse(Track t) {
    print("Response to be added");
    print(t.enteredText);
    print(t.marked.toString());
    print(t.questionId.toString());
    print(t.sectionId.toString());
    print(t.selectedOptionId);
    print(t.submitted.toString());
    print(t.timeTaken.toString());
    print(t.attempt);
    print(t.isCorrect);
    list_response_preapare.add(t);
    print("++++++++++++++RESPONSE SIZE"+list_response_preapare.length.toString());
  }


  SyncTrackToApi() {
    print('Arun list =='+list_track_prepare.length.toString());
    FLog.info(
      dataLogType: "Debug",
      text:list_track_prepare.length.toString()+"&&&&&&"+ "======================================================COMING INSIDE ''''SyncTrackToApi'''' FUNCTION",
    );
    for(int i=0;i<list_track_prepare.length;i++){
      print(list_track_prepare[i].id);
      FLog.info(
        dataLogType: "Debug",
        text:i.toString()+"======================"+list_track_prepare[i].id.toString()+"&&&&&&&&&&&&&&"+(list_track_prepare[i].saved.toString()+"&&&&&&&&&&&&&&"+ "======================================================ID AND SAVED VALUE IN '''list_track_prepare''' LIST"),
      );
      if(list_track_prepare[i].saved == 0){
        FLog.info(
          dataLogType: "Debug",
          text:i.toString()+"======================"+list_track_prepare[i].id.toString()+"&&&&&&&&&&&&&&"+(list_track_prepare[i].saved.toString()+"&&&&&&&&&&&&&&"+ "======================================================ID AND SAVED VALUE  LIST JUST BEFORE ADDING TO '''list_name_sync''' "),
        );
        list_name_sync.add(list_track_prepare[i]);
        FLog.info(
          dataLogType: "Debug",
          text:i.toString()+"======================"+list_track_prepare[i].id.toString()+"&&&&&&&&&&&&&&"+(list_track_prepare[i].saved.toString()+"&&&&&&&&&&&&&&"+ "======================================================ID AND SAVED VALUE IN  JUST AFTER ADDING TO '''list_name_sync''' "),
        );
      }
    }
    result = new Collection(list_name_sync).distinct();
    result = result.toList();
    print("sync-list"+result.toString());
    for(int i=0;i<result.length;i++){
      print(result[i].id);
      FLog.info(
        dataLogType: "Debug",
        text:i.toString()+"======================"+result[i].id.toString()+"&&&&&&&&&&&&&&"+(result[i].saved.toString()+"&&&&&&&&&&&&&&"+ "======================================================ID AND SAVED VALUE IN  RESULT LIST "),
      );
    }



  }

  update(){

  }

  IncrementTrack() {
    return ++noOfTracks;
  }

  DecrementTrack() {
    return --noOfTracks;
  }

  getTrackStatus() {
    print('Arun Track count == ${noOfTracks}');
  }

  factory SigObject() {
    return _singleton;
  }

  SigObject._internal() {}
// rest of the class
}

// consuming code
//MyClass myObj = new MyClass(); // get back the singleton
//...
//// another piece of consuming code
//MyClass myObj = new MyClass(); // still getting back the singleton

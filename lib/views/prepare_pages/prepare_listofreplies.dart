import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:imsindia/resources/strings/prepare.dart';
import 'package:imsindia/utils/colors.dart';
import 'package:flutter_document_picker/flutter_document_picker.dart';
import 'package:imsindia/views/Arun/ReadMoreText.dart';
import 'package:imsindia/views/prepare_pages/Prepare_Comments.dart';
import 'package:imsindia/utils/screen_util.dart';
import 'package:imsindia/utils/svg_images/prepare_svg_images.dart';


final List<CommentsOptionsHtml> list_Replies=[];
List<dynamic> like = [];

CommentsOptionsHtml options;
class PrepareListOfRepliesHtml extends StatefulWidget {
  @override
  PrepareListOfRepliesHtmlState createState() => PrepareListOfRepliesHtmlState();
}

class PrepareListOfRepliesHtmlState extends State<PrepareListOfRepliesHtml>{
  final GlobalKey _menuKey = new GlobalKey();
  bool uploaded = false;
  TextEditingController reply_Controller = TextEditingController();
  TextEditingController post_a_reply_Controller = TextEditingController();
  bool _pickFileInProgress = false;
  String _path = '-';
  bool _iosPublicDataUTI = true;
  bool _checkByCustomExtension = false;
  bool _checkByMimeType = false;
  bool upvoted = false;
  int liked ;
  FocusNode _focusNode = new FocusNode();
  _pickDocument() async {
    print("callinnggggggggggggggggggggggggggggg");
    String result;
    try {
      setState(() {
        _path = '-';
        _pickFileInProgress = true;
      });
      final _utiController = TextEditingController(
        text: 'com.sidlatau.example.mwfbak',
      );

      final _extensionController = TextEditingController(
        text: 'mwfbak',
      );

      final _mimeTypeController = TextEditingController(
        text: 'application/pdf image/png',
      );
      FlutterDocumentPickerParams params = FlutterDocumentPickerParams(
        allowedFileExtensions: _checkByCustomExtension
            ? _extensionController.text
            .split(' ')
            .where((x) => x.isNotEmpty)
            .toList()
            : null,
        allowedUtiTypes: _iosPublicDataUTI
            ? null
            : _utiController.text
            .split(' ')
            .where((x) => x.isNotEmpty)
            .toList(),
        allowedMimeTypes: _checkByMimeType
            ? _mimeTypeController.text
            .split(' ')
            .where((x) => x.isNotEmpty)
            .toList()
            : null,
      );

      result = await FlutterDocumentPicker.openDocument(params: params);
    } catch (e) {
      print(e);
      result = 'Error: $e';
    } finally {
      setState(() {
        _pickFileInProgress = false;
      });
    }

    setState(() {
      uploaded = true;
      _path = result;
      print(_path.length.toString());
      reply_Controller.text = _path;
    });
  }

  _onLiked(int index_like) {
    setState(() => liked = index_like);
  }
  @override
  Widget build(BuildContext context) {
    final screenHeight = MediaQuery.of(context).size.height;
    final screenWidth = MediaQuery.of(context).size.width;
    return GestureDetector(
      onTap: (){
        setState(() {

        });
        _focusNode.unfocus();
      },
      child: Scaffold(
//        appBar: AppBar(
//          brightness: Brightness.light,
//          elevation: 0.0,
//          centerTitle: false,
//          titleSpacing: 0.0,
//          backgroundColor: Colors.transparent,
//          title: Text(
//            'Replies',
//            style: const TextStyle(
//                color: Colors.black,
//                fontWeight: FontWeight.w500,
//                fontFamily: "IBMPlexSans",
//                fontStyle: FontStyle.normal,
//                fontSize: 16.0),
//          ),
//          leading: IconButton(
//            icon: Icon(
//              Icons.arrow_back,
//              color: Colors.black,
//            ),
//            onPressed: () => Navigator.pop(context, false),
//          ),
//        ),
        body: Container(
          color: _focusNode.hasFocus
              ? Colors.black.withOpacity(0.25)
              : Colors.transparent,
          margin: EdgeInsets.only(
            top: 0 / Constant.defaultScreenHeight * screenHeight,
          ),
          child: Column(
            children: <Widget>[
              Container(
                margin: EdgeInsets.only(
                  top: (42 / 720) * screenHeight,
                ),
                child: Row(
                  //crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    InkWell(
                      onTap: () {
                        Navigator.pop(context);
                      },
                      child: Container(
                        width: (58 / 360) * screenWidth,
                        height: (30.0 / 720) * screenHeight,
                        margin: EdgeInsets.only(left: 0.0),
                        child: SvgPicture.asset(
                          getPrepareSvgImages.backIcon,
                          height: (5 / 720) * screenHeight,
                          width: (14 / 360) * screenWidth,
                          fit: BoxFit.none,
                        ),
                      ),
                    ),
                    Container(
                      height: (20 / 720) * screenHeight,
                      // margin: EdgeInsets.only(left: (20 / 360) * screenWidth),
                      child: Text(
                        "Replies",
                        style: TextStyle(
                          color: NeutralColors.black,
                          fontSize: (16 / 720) * screenHeight,
                          fontFamily: "IBMPlexSans",
                          fontWeight: FontWeight.w700,
                        ),
                        textAlign: TextAlign.left,
                      ),
                    ),
                  ],
                ),
              ),
              Expanded(
                child: ListReplies(context,list_Replies,screenHeight,screenWidth),
              ),
              Align(
                alignment: Alignment.bottomCenter,
                child: Container(
                  color: _focusNode.hasFocus
                      ? Colors.white
                      : Colors.transparent,
                  child:  Row(
                    children: <Widget>[
                      Container(
                        height:30/720*screenHeight,
                        width: 30/360*screenWidth,
                        margin: EdgeInsets.only(top: (17/964)*screenHeight,left: (20/360)*screenWidth,bottom: 10/720 * screenHeight),
                        decoration: new BoxDecoration(
                            shape: BoxShape.circle,
                            image: new DecorationImage(
                                fit: BoxFit.cover,
                                image: AssetImage(
                                    'assets/images/oval-2.png'))),
                      ),
                      Container(
                        height: (40/720)*screenHeight,
                        width: (280/360) * screenWidth,
                        margin: EdgeInsets.only(left: (10/360)*screenWidth,top: (12/720)*screenHeight, bottom: 10/720 * screenHeight),
                        decoration: BoxDecoration(
                          border: Border.all(color: AccentColors.iceBlue),
                          color:Colors.white,
                          borderRadius: BorderRadius.all(Radius.circular(5)),
                        ),
                        child: Row(
                          children: <Widget>[
                            Container(
                              height: (18/720)*screenHeight,
                              width: (200/360) * screenWidth,
                              margin: EdgeInsets.only(left: (13/360)*screenWidth,top: (10/720)*screenHeight,bottom: (10/720)*screenHeight),
                              child: TextFormField(
                                focusNode: _focusNode,
                                // controller: uploaded ? new TextEditingController(text: '$_path'): post_a_query_Controller,
                                controller: uploaded ? reply_Controller : post_a_reply_Controller,
                                style: TextStyle(
                                  backgroundColor: NeutralColors.pureWhite,
                                  color: NeutralColors.black,
                                  fontWeight: FontWeight.bold,
                                  fontFamily: "IBMPlexSans",
                                  fontStyle: FontStyle.normal,
                                  fontSize: (14/720)*screenHeight,
                                ),
                                textInputAction: TextInputAction.done,
                                decoration: new InputDecoration.collapsed
                                  (
                                  hintText:  SubQueriesCommentsScreenStrings.Text_Reply,
                                  hintStyle: TextStyle (
                                    color: AccentColors.iceBlue,
                                    fontWeight: FontWeight.w400,
                                    fontFamily: "IBMPlexSans",
                                    fontStyle: FontStyle.normal,
                                    fontSize: (14/720)*screenHeight,
                                  ),
                                ),
                                onFieldSubmitted: (val){
                                  setState(() {
                                    options  = new CommentsOptionsHtml.Comment("Sushree","23",val,123);
                                    list_Replies.add(options);
                                    print(list_Replies);
                                    post_a_reply_Controller.clear();
                                  });
                                  return;
                                },
                              ),
                            ),
                            GestureDetector(
                              onTap: _pickFileInProgress ? null : _pickDocument,
                              child: Container(
                                height: (18 /720) * screenHeight,
                                width: (19 / 360) * screenWidth,
                                margin: EdgeInsets.only(left: 29/360*screenWidth,top: (8/720)*screenHeight,bottom: (10/720)*screenHeight),
                                child: Image.asset(
                                  "assets/images/clip-4.png",
                                  fit: BoxFit.fill,
                                ),
//                            child:Icon(
//                              Icons.attach_file,
//                              color: NeutralColors.purpley,
//                              size: 17/360*screenWidth,
//                            )
                              ),
                            )
                          ],
                        ),
                      ),
                    ],
                  ),
                )
              )
            ],
          ),
        ),
      ),
    );
  }
  
  Widget ListReplies(BuildContext context,List<CommentsOptionsHtml> list_replies,final ScreenHeight, final ScreenWidth){
    
    if(list_replies.isEmpty || list_replies.length == 0){
      return Container(
        child: Align(
          alignment: Alignment.center,
          child: Text(
            "No comments",
            style: TextStyle(
                color: NeutralColors.purpleish_blue
            ),
          ),
        ),
      );
    }else{
      return _focusNode.hasFocus?
        Container(
        child: ListView.separated(
            itemBuilder: (context,position){
              return Container(
                margin: EdgeInsets.only(left: (20/360)*screenWidth),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Container(
                      height: 40/720*screenHeight,
                      margin: EdgeInsets.only(top: 20/720*screenHeight),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Container(child: Row(
                            children: <Widget>[
                              Container(
                                height: (40 /720) * screenHeight,
                                width: (40 / 360) * screenWidth,
                                margin: EdgeInsets.only(top: (0 / 720) * screenHeight),
                                decoration: new BoxDecoration(
                                    shape: BoxShape.circle,
                                    image: new DecorationImage(
                                        fit: BoxFit.cover,
                                        image: AssetImage(
                                            'assets/images/oval-2.png'))),
                              ),
                              Container(
                                height: 40/720*screenHeight,
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Container(
                                      margin: EdgeInsets.only(left: (12/360)*screenWidth),
                                      child:Text(
                                        list_replies[position].nameofperson,
                                        // SubQueriesCommentsScreenStrings.Text_UserName,
                                        style: TextStyle (
                                          color: NeutralColors.dark_navy_blue,
                                          fontWeight: FontWeight.w500,
                                          fontFamily: "IBMPlexSans",
                                          fontStyle: FontStyle.normal,
                                          fontSize: (14/360)*screenWidth,
                                        ),
                                        textAlign: TextAlign.start,
                                      ),
                                    ),
                                    Container(
                                      margin: EdgeInsets.only(top: (5/720)*screenHeight,left: 12/360*screenWidth),
                                      child:Text(
                                        SubQueriesCommentsScreenStrings.Text_MsgdTime,
                                        style: TextStyle (
                                          color: NeutralColors.blue_grey,
                                          fontWeight: FontWeight.w500,
                                          fontFamily: "IBMPlexSans",
                                          fontStyle: FontStyle.normal,
                                          fontSize: (12/720)*screenHeight,
                                        ),
                                        textAlign: TextAlign.start,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),),
                          Container(
                            height: 40/720*screenHeight,
                            margin: EdgeInsets.only(top:0.0,right: 0.0,),
                            child: GestureDetector(
                              onTap: () {
                                // This is a hack because _PopupMenuButtonState is private.
                                dynamic state = _menuKey.currentState;
                                state.showButtonMenu();
                              },
                              child: Container(
                                child: PopupMenuButton(
                                    icon: Icon(Icons.more_vert,color: NeutralColors.blue_grey),
                                    padding: EdgeInsets.all(10),
                                    // key: _menuKey,
                                    itemBuilder: (_) =>
                                    <PopupMenuItem<String>>[
                                      new PopupMenuItem<String>(
                                          child: Container(
                                            alignment: Alignment.center,
                                            child: const Text(
                                              'Report',
                                              style: TextStyle(
                                                color: NeutralColors.gunmetal,
                                                fontFamily: "IBMPlexSans",
                                                fontWeight: FontWeight.w500,
                                                fontSize: 14,
                                              ),
                                              textAlign: TextAlign.center,
                                            ),
                                          ),
                                          value: 'Report'),
                                    ],
                                    onSelected: (_) {}),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    /***Comment Text***/
                    Container(
                      // height:(72/936)*screenHeight,
                      width: (320/360)*screenWidth,
                      margin: EdgeInsets.only(top: (12/720)*screenHeight,right: 20/360*screenWidth),
                      child: SingleChildScrollView(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Padding(
                              padding: const EdgeInsets.all(1.0),
                              child: ReadMoreText(
                                list_replies[position].comment_text,
                                trimLines: 3,
                                colorClickableText: Colors.blue,
                                trimMode: TrimMode.Line,
                                trimCollapsedText: SubQueriesCommentsScreenStrings.Text_ReadMore,
                                trimExpandedText: SubQueriesCommentsScreenStrings.Text_ReadLess,
                                style: TextStyle(
                                  color: NeutralColors.gunmetal,
                                  fontWeight: FontWeight.w500,
                                  fontFamily: "IBMPlexSans",
                                  fontStyle: FontStyle.normal,
                                  height: 1.5,
                                  fontSize: (11/360)*screenWidth,
                                ),

                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                    /***Upvotes and reply***/
                    Container(
                      child: Row(
                        children: <Widget>[
                          InkWell(
                            onTap: (){
                              setState(() {
                                _onLiked(position);
                                upvoted = !upvoted;
                                if(like.contains(position)){
                                  like.remove(position);
                                }else{
                                  like.add(position);
                                }
                              });
                            },
                            child: Padding(
                              padding: const EdgeInsets.all(5.0),
                              child: Row(
                                children: <Widget>[
                                  Container(
                                    height: (11/720)*screenHeight,
                                    width: (11/360)*screenWidth,
                                    margin: EdgeInsets.only(top:(18/720)*screenHeight),
                                    child: Icon(
                                      Icons.thumb_up,
                                      color: like.contains(position) ?
                                      NeutralColors.deep_sky_blue :
                                      NeutralColors.black,
                                      size: 11/360*screenWidth,
                                    ),
                                  ),
                                  Container(
                                    height: 23/720*screenHeight,
                                    width: 100/360 * screenWidth,
                                    margin: EdgeInsets.only(top: 18/720*screenHeight,left: 5/360*screenWidth),
                                    decoration: BoxDecoration(
                                      color:Colors.transparent,
                                      borderRadius: BorderRadius.all(Radius.circular(2)),
                                    ),
                                    child: Container(
                                      margin: EdgeInsets.only(top:6/720*screenHeight),
                                      child: Text(
                                        like.contains(position) ? '${(list_replies[position].numberofvotes)+1} Upvotes' : '${list_replies[position].numberofvotes} Upvotes',
                                        style: TextStyle (
                                          color: like.contains(position) ?
                                          NeutralColors.deep_sky_blue :
                                          NeutralColors.black,
                                          fontWeight: FontWeight.w500,
                                          fontFamily: "IBMPlexSans",
                                          fontStyle: FontStyle.normal,
                                          fontSize: (11/720)*screenHeight,

                                        ),
                                        textAlign: TextAlign.left,

                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                          InkWell(
                            onTap: (){
                              setState(() {
                              });
                            },
                            child: Row(
                              children: <Widget>[
                                Container(
                                  height: (9/720)*screenHeight,
                                  width: (9/360)*screenWidth,
                                  margin: EdgeInsets.only(top:(20/720)*screenHeight,left: (30/360)*screenWidth),
//                          child: Image.asset(
//                            "assets/images/reply.png",
//                            fit: BoxFit.fitHeight,
//                          ),
                                  child: Icon(
                                    Icons.chat_bubble_outline,
                                    color: NeutralColors.black,
                                    size: 9/360*screenWidth,
                                  ),
                                ),
                                Container(
                                    height: 23/720*screenHeight,
                                    width: 30/360 * screenWidth,
                                    margin: EdgeInsets.only(top: 15/720*screenHeight,left: 5/360*screenWidth),
                                    decoration: BoxDecoration(
                                      color:Colors.transparent,
                                      borderRadius: BorderRadius.all(Radius.circular(2)),
                                    ),
                                    child:Container(
                                      margin: EdgeInsets.only(top:6/720*screenHeight),
                                      child: Text(
                                        SubQueriesCommentsScreenStrings.Text_Reply,
                                        style: TextStyle (
                                          color: NeutralColors.dark_navy_blue,
                                          fontWeight: FontWeight.w500,
                                          fontFamily: "IBMPlexSans",
                                          fontStyle: FontStyle.normal,
                                          fontSize: (11/720)*screenHeight,
                                        ),
                                        textAlign: TextAlign.left,
                                      ),
                                    )
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              );
            },
            separatorBuilder: (context,position){
              return Divider();
            },
            itemCount: list_replies.length),
      ) :
        Container(
        child: ListView.separated(
            itemBuilder: (context,position){
              return Container(
                margin: EdgeInsets.only(left: (20/360)*screenWidth),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Container(
                      height: 40/720*screenHeight,
                      margin: EdgeInsets.only(top: 20/720*screenHeight),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Container(child: Row(
                            children: <Widget>[
                              Container(
                                height: (40 /720) * screenHeight,
                                width: (40 / 360) * screenWidth,
                                margin: EdgeInsets.only(top: (0 / 720) * screenHeight),
                                decoration: new BoxDecoration(
                                    shape: BoxShape.circle,
                                    image: new DecorationImage(
                                        fit: BoxFit.cover,
                                        image: AssetImage(
                                            'assets/images/oval-2.png'))),
                              ),
                              Container(
                                height: 40/720*screenHeight,
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Container(
                                      margin: EdgeInsets.only(left: (12/360)*screenWidth),
                                      child:Text(
                                        list_replies[position].nameofperson,
                                        // SubQueriesCommentsScreenStrings.Text_UserName,
                                        style: TextStyle (
                                          color: NeutralColors.dark_navy_blue,
                                          fontWeight: FontWeight.w500,
                                          fontFamily: "IBMPlexSans",
                                          fontStyle: FontStyle.normal,
                                          fontSize: (14/360)*screenWidth,
                                        ),
                                        textAlign: TextAlign.start,
                                      ),
                                    ),
                                    Container(
                                      margin: EdgeInsets.only(top: (5/720)*screenHeight,left: 12/360*screenWidth),
                                      child:Text(
                                        SubQueriesCommentsScreenStrings.Text_MsgdTime,
                                        style: TextStyle (
                                          color: NeutralColors.blue_grey,
                                          fontWeight: FontWeight.w500,
                                          fontFamily: "IBMPlexSans",
                                          fontStyle: FontStyle.normal,
                                          fontSize: (12/720)*screenHeight,
                                        ),
                                        textAlign: TextAlign.start,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),),
                          Container(
                            height: 40/720*screenHeight,
                            margin: EdgeInsets.only(top:0.0,right: 0.0,),
                            child: GestureDetector(
                              onTap: () {
                                // This is a hack because _PopupMenuButtonState is private.
                                dynamic state = _menuKey.currentState;
                                state.showButtonMenu();
                              },
                              child: Container(
                                child: PopupMenuButton(
                                    icon: Icon(Icons.more_vert,color: NeutralColors.blue_grey),
                                    padding: EdgeInsets.all(10),
                                    // key: _menuKey,
                                    itemBuilder: (_) =>
                                    <PopupMenuItem<String>>[
                                      new PopupMenuItem<String>(
                                          child: Container(
                                            alignment: Alignment.center,
                                            child: const Text(
                                              'Report',
                                              style: TextStyle(
                                                color: NeutralColors.gunmetal,
                                                fontFamily: "IBMPlexSans",
                                                fontWeight: FontWeight.w500,
                                                fontSize: 14,
                                              ),
                                              textAlign: TextAlign.center,
                                            ),
                                          ),
                                          value: 'Report'),
                                    ],
                                    onSelected: (_) {}),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    /***Comment Text***/
                    Container(
                      // height:(72/936)*screenHeight,
                      width: (320/360)*screenWidth,
                      margin: EdgeInsets.only(top: (12/720)*screenHeight,right: 20/360*screenWidth),
                      child: SingleChildScrollView(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Padding(
                              padding: const EdgeInsets.all(1.0),
                              child: ReadMoreText(
                                list_replies[position].comment_text,
                                trimLines: 3,
                                colorClickableText: Colors.blue,
                                trimMode: TrimMode.Line,
                                trimCollapsedText: SubQueriesCommentsScreenStrings.Text_ReadMore,
                                trimExpandedText: SubQueriesCommentsScreenStrings.Text_ReadLess,
                                style: TextStyle(
                                  color: NeutralColors.gunmetal,
                                  fontWeight: FontWeight.w500,
                                  fontFamily: "IBMPlexSans",
                                  fontStyle: FontStyle.normal,
                                  height: 1.5,
                                  fontSize: (11/360)*screenWidth,
                                ),

                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                    /***Upvotes and reply***/
                    Container(
                      child: Row(
                        children: <Widget>[
                          InkWell(
                            onTap: (){
                              setState(() {
                                _onLiked(position);
                                upvoted = !upvoted;
                                if(like.contains(position)){
                                  like.remove(position);
                                }else{
                                  like.add(position);
                                }
                              });
                            },
                            child: Padding(
                              padding: const EdgeInsets.all(5.0),
                              child: Row(
                                children: <Widget>[
                                  Container(
                                    height: (11/720)*screenHeight,
                                    width: (11/360)*screenWidth,
                                    margin: EdgeInsets.only(top:(18/720)*screenHeight),
                                    child: Icon(
                                      Icons.thumb_up,
                                      color: like.contains(position) ?
                                      NeutralColors.deep_sky_blue :
                                      NeutralColors.black,
                                      size: 11/360*screenWidth,
                                    ),
                                  ),
                                  Container(
                                    height: 23/720*screenHeight,
                                    width: 100/360 * screenWidth,
                                    margin: EdgeInsets.only(top: 18/720*screenHeight,left: 5/360*screenWidth),
                                    decoration: BoxDecoration(
                                      color:Colors.transparent,
                                      borderRadius: BorderRadius.all(Radius.circular(2)),
                                    ),
                                    child: Container(
                                      margin: EdgeInsets.only(top:6/720*screenHeight),
                                      child: Text(
                                        like.contains(position) ? '${(list_replies[position].numberofvotes)+1} Upvotes' : '${list_replies[position].numberofvotes} Upvotes',
                                        style: TextStyle (
                                          color: like.contains(position) ?
                                                 NeutralColors.deep_sky_blue :
                                                 NeutralColors.black,
                                          fontWeight: FontWeight.w500,
                                          fontFamily: "IBMPlexSans",
                                          fontStyle: FontStyle.normal,
                                          fontSize: (11/720)*screenHeight,

                                        ),
                                        textAlign: TextAlign.left,

                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                          InkWell(
                            onTap: (){
                              setState(() {
                              });
                            },
                            child: Row(
                              children: <Widget>[
                                Container(
                                  height: (9/720)*screenHeight,
                                  width: (9/360)*screenWidth,
                                  margin: EdgeInsets.only(top:(20/720)*screenHeight,left: (30/360)*screenWidth),
//                          child: Image.asset(
//                            "assets/images/reply.png",
//                            fit: BoxFit.fitHeight,
//                          ),
                                  child: Icon(
                                    Icons.chat_bubble_outline,
                                    color: NeutralColors.black,
                                    size: 9/360*screenWidth,
                                  ),
                                ),
                                Container(
                                    height: 23/720*screenHeight,
                                    width: 30/360 * screenWidth,
                                    margin: EdgeInsets.only(top: 15/720*screenHeight,left: 5/360*screenWidth),
                                    decoration: BoxDecoration(
                                      color:Colors.transparent,
                                      borderRadius: BorderRadius.all(Radius.circular(2)),
                                    ),
                                    child:Container(
                                      margin: EdgeInsets.only(top:6/720*screenHeight),
                                      child: Text(
                                        SubQueriesCommentsScreenStrings.Text_Reply,
                                        style: TextStyle (
                                          color: NeutralColors.dark_navy_blue,
                                          fontWeight: FontWeight.w500,
                                          fontFamily: "IBMPlexSans",
                                          fontStyle: FontStyle.normal,
                                          fontSize: (11/720)*screenHeight,
                                        ),
                                        textAlign: TextAlign.left,
                                      ),
                                    )
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              );
            },
            separatorBuilder: (context,position){
              return Divider();
            },
            itemCount: list_replies.length),
      );
    }
  }
}
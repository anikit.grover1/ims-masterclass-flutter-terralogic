import 'dart:convert';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:imsindia/routers/routes.dart';
import 'package:imsindia/views/prepare_pages/prepare_videoplayer_screen_android.dart';
import 'package:imsindia/views/prepare_pages/prepare_videoplayer_screen.dart';
import 'package:imsindia/views/prepare_pages/prepare_videoplayer_screen_android_crash_devices.dart';
import 'package:video_player/video_player.dart';
import 'package:imsindia/utils/colors.dart';
import 'package:imsindia/views/prepare_pages/prepare_queries.dart';
import 'package:imsindia/resources/strings/prepare.dart';
import 'package:imsindia/utils/svg_images/prepare_svg_images.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:cached_network_image/cached_network_image.dart';
//import 'package:chewie/chewie.dart';
import 'package:imsindia/utils/global.dart' as global;
import 'package:imsindia/api/bloc/services.dart';
import 'package:imsindia/utils/Url.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'package:connectivity/connectivity.dart';

import 'dart:async';
import 'package:flutter/services.dart';
import 'package:device_info/device_info.dart';

String userid;
var authToken;
var courseID;
bool blimitCellularDataUsage = false;
final navigatorKeyCalc = GlobalKey<NavigatorState>();
double screenWidth,screenHeight;

class PrepareCalculationWidgetHtml extends StatefulWidget{
   final parentID;
   final title;
   final subtitle;
   final refID;

   PrepareCalculationWidgetHtml(this.parentID,this.subtitle,this.title,this.refID);

  @override
  State<StatefulWidget> createState() => new PrepareCalculationWidgetHtmlState();
}
class PrepareCalculationWidgetHtmlState extends State<PrepareCalculationWidgetHtml> with TickerProviderStateMixin {
  static final DeviceInfoPlugin deviceInfoPlugin = DeviceInfoPlugin();
  Map<String, dynamic> _deviceData = <String, dynamic>{};
  String _connectionStatus = 'Unknown';
  final Connectivity _connectivity = Connectivity();
  StreamSubscription<ConnectivityResult> _connectivitySubscription;
  final queriesState = QueriesState;
  SharedPreferences prefs;
  SharedPreferences bookedmarkVideos_prefs;
  bool bookmarkActive = false;
  bool VideoID = false;
  var totalData =[];
  List<String> finalVideosList = [];
  List<String> finalThumbnailList = [];
  List<String> finalTitlesForVideos=[];
  List<String> finalDurationLists=[];
  List<String> finalLabelLists=[];
  List<String> finalobjectType=[];
  List<String> finalreferenceType=[];
  List<String> bookedmarkVideos=[];
  List<String> bookedmark_API = [];
  ///List from learn/videos API///
  List<bool> isBookmarkedList = [];
  List<String> objectTypeList = [];
  List<String> companyCodeList = [];
  List<String> userIdList = [];
  List<String> InProgressList = [];
  List<String> courseIdList = [];
  List<String> parentIdList = [];
  List<String> timeStampList = [];
  List<String> idList = [];
  List<String> durationList = [];
  List<String> referenceTypeList = [];
  List<String> referenceIdList = [];
  List<String> userRatingCountList = [];
  ///List of devices removed of fullscreen option/// 
  // List<String> crashingDeviceProductNames = ['whyred','violet','lavender', 'j7xelte','ginkgo',
  //                                     'HWDUK','HWLLD-H2','pine','HWBND-H','tulip','HWRNE',
  //                                      'RMX1921','cactus','onc','riva','RMX1801','RMX1805','LND-AL30',
  //                                    ];
  ///Empty list so that fullscreen video is available for all devices///
  List<String> crashingDeviceProductNames = []; 

  ///end**//
  OverlayEntry overlayEntry;
  VideoPlayerController _videoPlayerController1;
  VideoPlayerController _videoPlayerController2;
  //ChewieController _chewieController;
  List<Tab> tabList = List();
  TabController _tabController;
  bool clicked = false;
  bool isSelected = false;
  bool isVideo_clicked = false;
  String video_png;
  String labeltitle;
  String videoid;
  int checkedPos = 0;
  bool dropdown_active = false;
  FocusNode _focusNode = new FocusNode();
  bool isClicked = false;
  GlobalKey<ScaffoldState> _keyCalc;
  var authorizationInvalidMsg;
  bool VIDEODTS = false;
  var systemtime;
  bool cellularDataFlag;
  void getCourseId() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    courseID = prefs.getString("courseId");
    print("oooooooooooooooooooooooooooooooo");
    print(courseID);
  }


  LoadAPI_PrepareCalculation(String parentID,Map t) async{
    print("**************************LoadAPI_PrepareCalculation");
    print("**************************"+URL.GET_VIDEOS + parentID);
    ApiService().getAPI(URL.GET_VIDEOS + parentID, t)
        .then((returnValue) {
      //setState(() {
      print("**************************"+URL.GET_VIDEOS + parentID);
      print(t);
      if (returnValue[0].toString().toLowerCase() == 'Fetched Successfully'.toLowerCase()) {
        totalData=returnValue[1]['data'];
        for(int pos=0;pos < totalData.length;pos++){
          print("+++++++++++++insideforloop");
          if(totalData.length != 0){
            print("+++++++++++++insideIFloop");
            setState(() {
              VideoID = true;
              if(totalData[pos]['isActive']==1){
              finalVideosList.add(totalData[pos]['videoId']);
              if(totalData[pos]['component']['thumbnail'] == null){
                finalThumbnailList.add("");
              }else{
                finalThumbnailList.add(totalData[pos]['component']['thumbnail']);
              }
              finalTitlesForVideos.add(totalData[pos]['component']['title']);
              finalDurationLists.add(totalData[pos]['component']['duration']);
              if(totalData[pos]['component']['label'] == null){
                finalLabelLists.add("");
              }else{
                finalLabelLists.add(totalData[pos]['component']['label']);
              }
              finalobjectType.add(totalData[pos]['component']['objectType']);
              finalreferenceType.add(totalData[pos]['component']['referenceType']);
              print("Final thumbnail list"+finalThumbnailList.toString());
              print("Final video list"+finalVideosList.toString());
              print("Final tilte for videos list"+finalTitlesForVideos.toString());
              print("Final duration  list"+finalDurationLists.toString());
              print("Final label list"+finalLabelLists.toString());
              print("Final object type list"+finalobjectType.toString());
              if(totalData[pos]['videoDts'] == null){
                VIDEODTS = false;
                print("==========no data");
                isBookmarkedList.add(null);
              }
              else{
                VIDEODTS = true;
                print(totalData[pos]['videoDts']);
                bool data = totalData[pos]['videoDts']['isBookmarked'];
                print(data);
                print("dtaaaa bookmark");
                bookedmark_API.add(data.toString());
                isBookmarkedList.add(data);
                objectTypeList.add(totalData[pos]['videoDts']['objectType']);
                companyCodeList.add(totalData[pos]['videoDts']['companyCode']);
                userIdList.add(totalData[pos]['videoDts']['userId']);
                InProgressList.add(totalData[pos]['videoDts']['status']);
                courseIdList.add(totalData[pos]['videoDts']['courseId']);
                parentIdList.add(totalData[pos]['videoDts']['parentId']);
                timeStampList.add(totalData[pos]['videoDts']['timeStamp']);
                idList.add(totalData[pos]['videoDts']['id']);
//                durationList.add(totalData[pos]['videoDts']['duration']);
                referenceTypeList.add(totalData[pos]['videoDts']['referenceType']);
                referenceIdList.add(totalData[pos]['videoDts']['referenceId']);
                userRatingCountList.add(totalData[pos]['videoDts']['userRatingCount'].toString());
                print(isBookmarkedList);
                print(finalVideosList);
                print(finalLabelLists);
                print(finalLabelLists[0]);
                print("isBookmarkedList value");
              }
            }
            });
            
          }else{
            setState(() {
              VideoID = false;
            });
            setData_sharePref();
          }
        }

        setData_sharePref();
      } else {
        authorizationInvalidMsg = returnValue[0];
      }
      //});
    });
    print("==========================after loading");
    print(isBookmarkedList);
    print(bookedmark_API);
  }

  Savebookmark_UseractionAPI(String timestamp){
    print("=================timestamp==================================="+timestamp);
    print("+++++++++++++++++++++++++++++++=AFTER CLICK BOOKMARKED"+checkedPos.toString());
    //print(courseID);
    print(bookmarkActive);
    print(finalobjectType);
    print( widget.parentID);
    print(widget.refID);
    print(finalVideosList);
    print(finalreferenceType);
    /// "parentId": "12900",
    ///          "id": "12911",///
    Map<String,dynamic> postdata;
    postdata = {
      "courseId": courseID,
      "duration": "0",
      "isBookmarked": bookmarkActive==true?true:false,
      "isChannel": false,
      "objectType": finalobjectType[checkedPos],
      "parentId": widget.parentID,
      "referParentId": widget.refID,
      "referenceId": finalVideosList[checkedPos],
      "referenceType": finalreferenceType[checkedPos],
      "status":"InProgress",
      "timeStamp": timestamp,
      "videoRating": 0
    };
    print(postdata);
    print(jsonEncode(postdata));
      ApiService().postAPI(
          URL.USERACTION_API_FOR_PREPARE, postdata, authToken)
          .then((result) {
        print("=======================COMING");
        print(authToken);
        print(result);
      });

  }

  Future setData_sharePref() async {
    prefs = await SharedPreferences.getInstance();
      prefs.setStringList("VideoLists", finalVideosList);
      prefs.setStringList("ThumbnailLists", finalThumbnailList);
      prefs.setStringList("DurationLists", finalDurationLists);
      prefs.setStringList("TitlesLists", finalTitlesForVideos);
      prefs.setStringList("LabelLists", finalLabelLists);

  }

  Future storeList_pref() async {
    if(bookedmarkVideos.length!=0){
      bookedmarkVideos_prefs = await SharedPreferences.getInstance();
      bookedmarkVideos_prefs.setStringList("bookmarkedVideoLists", bookedmarkVideos);
    }
  }

  void getTimeStamp() {
    var moonLanding = DateTime.now();
    print("Current UTC Timing");
    print(moonLanding.toUtc());
    String systemDate = moonLanding.toUtc().toString().split(" ")[0];
    String systemTime = moonLanding.toUtc().toString().split(" ")[1].toString().split(".")[0];
    String lastZstring = moonLanding.toUtc().toString().split(" ")[1].toString().split(".")[1];
    String thridDigit = lastZstring.substring(0,3)+"Z";
    print("=============="+systemDate+"================"+systemTime+"==========="+lastZstring+"=========="+thridDigit);
    systemtime = systemDate+"T"+systemTime+"."+thridDigit;
    print("***********************************123");
    print(systemtime);
  }
  Future<void> initConnectivity() async {
    ConnectivityResult result;
    // Platform messages may fail, so we use a try/catch PlatformException.
    try {
      result = await _connectivity.checkConnectivity();
    } on PlatformException catch (e) {
      print(e.toString());
    }

    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) {
      return Future.value(null);
    }

    return _updateConnectionStatus(result);
  }
  @override
  void initState() {
    GetMobileDataStatus_pref();
    labelList=[];
    finalVideosList = [];
    finalThumbnailList = [];
    finalDurationLists=[];
    finalTitlesForVideos=[];
    bookedmarkVideos = [];
    finalLabelLists=[];
    print("&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&TITLE");
    getCourseId();
    print(widget.title);
    global.getToken.then((t){
      authToken=t;
      LoadAPI_PrepareCalculation(widget.parentID,authToken);
    });

    getUserid();
    _videoPlayerController1 = VideoPlayerController.network(
        'https://dfhvzpvwnedye.cloudfront.net/ims-ugvideos/myIMS - For Channel/CMAT_Analysis_2019_HQ/output.mdpmaster.m3u8');
    _keyCalc = GlobalKey<ScaffoldState>();
    // KeyboardVisibilityNotification().addNewListener(
    //   onHide: () {
    //     _focusNode.unfocus();
    //   },
    // );

//    tabList.add(new Tab(
//      child: Text(
//        //CalculationScreenStrings.Tab_Queries,
//        "",
//        style: TextStyle (
//          fontWeight: FontWeight.w100,
//          fontFamily: "IBMPlexSans",
//          fontStyle: FontStyle.normal,
//          fontSize: 14,
//        ),
//        textAlign: TextAlign.left,
//      ),
//    ));
    tabList.add(new Tab(
      child: Text(
        CalculationScreenStrings.Tab_UpNext,
        style: TextStyle (
          fontWeight: FontWeight.w100,
          fontFamily: "IBMPlexSans",
          fontStyle: FontStyle.normal,
          fontSize: 14,
        ),
        textAlign: TextAlign.left,
      ),
    ));
//    tabList.add(new Tab(
//      child: Text(
//        //CalculationScreenStrings.Tab_Details,
//        "",
//        style: TextStyle (
//          fontWeight: FontWeight.w100,
//          fontFamily: "IBMPlexSans",
//          fontStyle: FontStyle.normal,
//          fontSize: 14,
//        ),
//        textAlign: TextAlign.left,
//      ),
//    ));
    _tabController = new TabController(vsync: this, length:
    tabList.length);

    //get cellular data flag
    getCellularDataFlag();
    initPlatformState();
    super.initState();
  }


  Future<void> initPlatformState() async {
    Map<String, dynamic> deviceData;

    try {
      if (Platform.isAndroid) {
        deviceData = _readAndroidBuildData(await deviceInfoPlugin.androidInfo);
      } else if (Platform.isIOS) {
        deviceData = _readIosDeviceInfo(await deviceInfoPlugin.iosInfo);
      }
    } on PlatformException {
      deviceData = <String, dynamic>{
        'Error:': 'Failed to get platform version.'
      };
    }

    if (!mounted) return;

    setState(() {
      _deviceData = deviceData;
    });
  }

  Map<String, dynamic> _readAndroidBuildData(AndroidDeviceInfo build) {
    return <String, dynamic>{
      'version.securityPatch': build.version.securityPatch,
      'version.sdkInt': build.version.sdkInt,
      'version.release': build.version.release,
      'version.previewSdkInt': build.version.previewSdkInt,
      'version.incremental': build.version.incremental,
      'version.codename': build.version.codename,
      'version.baseOS': build.version.baseOS,
      'board': build.board,
      'bootloader': build.bootloader,
      'brand': build.brand,
      'device': build.device,
      'display': build.display,
      'fingerprint': build.fingerprint,
      'hardware': build.hardware,
      'host': build.host,
      'id': build.id,
      'manufacturer': build.manufacturer,
      'model': build.model,
      'product': build.product,
      'supported32BitAbis': build.supported32BitAbis,
      'supported64BitAbis': build.supported64BitAbis,
      'supportedAbis': build.supportedAbis,
      'tags': build.tags,
      'type': build.type,
      'isPhysicalDevice': build.isPhysicalDevice,
      'androidId': build.androidId,
      'systemFeatures': build.systemFeatures,
    };
  }

  Map<String, dynamic> _readIosDeviceInfo(IosDeviceInfo data) {
    return <String, dynamic>{
      'name': data.name,
      'systemName': data.systemName,
      'systemVersion': data.systemVersion,
      'model': data.model,
      'localizedModel': data.localizedModel,
      'identifierForVendor': data.identifierForVendor,
      'isPhysicalDevice': data.isPhysicalDevice,
      'utsname.sysname:': data.utsname.sysname,
      'utsname.nodename:': data.utsname.nodename,
      'utsname.release:': data.utsname.release,
      'utsname.version:': data.utsname.version,
      'utsname.machine:': data.utsname.machine,
    };
  }

 void getCellularDataFlag() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    //Return bool
    bool boolValue = prefs.getBool('limitCellularDataUsage');
    setState(() {
      cellularDataFlag = boolValue;
    });
    print("**************************** limitCellularDataUsage"+boolValue.toString());
  }
  @override
  void dispose() {
    if(_connectivitySubscription!=null){
      _connectivitySubscription.cancel();
    }
    if(_tabController!=null){
      _tabController.dispose();
    }
    if(_videoPlayerController1!=null){
      _videoPlayerController1.dispose();
    }
    // if(_chewieController!=null){
    //   _chewieController.dispose();
    // }
//    if(overlayEntry!=null){
//      overlayEntry.remove();
//    }
    super.dispose();
  }
  @override
  void didChangeDependencies(){
  //  videos = new List();
  //  thumbnail = new List();
 //   LoadAPI_PrepareCalculation(widget.parentID);
    super.didChangeDependencies();
  }
  @override
  Widget build(BuildContext context) {
//
     screenHeight = MediaQuery.of(context).size.height;
     screenWidth = MediaQuery.of(context).size.width;
    TextStyle label = new TextStyle(
        fontWeight: FontWeight.w700,
        color: NeutralColors.purpleish_blue
    );
    TextStyle unselectedLabel = new TextStyle(
        fontWeight: FontWeight.w200,
        color: NeutralColors.blue_grey
    );
    return Scaffold(
      key: _keyCalc,
      
      resizeToAvoidBottomInset: true,
      appBar: AppBar(
        brightness: Brightness.light,
        elevation: 0.0,
        centerTitle: false,
        titleSpacing: 0.0,
        backgroundColor: Colors.transparent,
        title: Text(
          '',
          style: const TextStyle(
              color: Colors.black,
              fontWeight: FontWeight.w500,
              fontFamily: "IBMPlexSans",
              fontStyle: FontStyle.normal,
              fontSize: 16.0),
        ),
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back,
            color: Colors.black,
          ),
          onPressed: () {
            Navigator.of(context).pop();
          }
        ),
      ),
      body:finalThumbnailList.length==0?
      Center(child:CircularProgressIndicator()):
      Container(
        color: Colors.white,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: <Widget>[
                  /**For video container**/
                  Container(
                    color: Colors.white,
                     // height: 180.1/764 * screenHeight,
//                        width: 320/360 * screenWidth,
                    margin: EdgeInsets.only(left: 20/360 * screenWidth,right: 20/360 * screenWidth),
//                      child: isVideo_clicked ?
//                      Chewie(
//                        controller: ChewieController(
//                          videoPlayerController: _videoPlayerController1,
//                          aspectRatio: 16 / 9,
//                          autoPlay: true,
//                          looping: true,
//                        ),
//                      )
//                          : _getChewiePlayer(false,screenHeight,screenWidth) ,
                  child: _getChewiePlayer(false,screenHeight,screenWidth) ,
//                          child: ChewieListItem(
//                            videoPlayerController: VideoPlayerController.network(
//                                CalculationScreenStrings.Video_URL,),
//                          ),
                  ),
                  /**For Calculation text container**/
                  Container(
                    color: Colors.white,
                    margin: EdgeInsets.only(right: 20/360*screenWidth),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        /** calculation text**/
                        Container(
                          margin: EdgeInsets.only(top: 24.9/764 * screenHeight,left: 20/360*screenWidth),
                          height: 27/764 * screenHeight,
                          //width: 106/360*screenWidth,
                          child: Text(
                           // CalculationScreenStrings.Text_Calculation,
                            widget.title.toString(),
                            style: TextStyle (
                              fontWeight: FontWeight.bold,
                              fontFamily: "IBMPlexSans",
                              fontStyle: FontStyle.normal,
                              fontSize: (18/360)*screenWidth,
                            ),
                            textAlign: TextAlign.left,
                          ),
                        ),
                        Container(child: Row(
                          children: <Widget>[
                            /** bookmark imageeee**/
                            InkWell(
                              onTap: (){
                                getTimeStamp();
                                setState(() {
                                  print("SSSSSSSSSSSSSSSSSSSSSSSSSSSSSS");
                                  bookmarkActive==true?bookmarkActive=false:bookmarkActive=true;
                                  print(bookmarkActive);
                                  print(checkedPos);
                                  print(isBookmarkedList);
                                  isBookmarkedList[checkedPos] = (bookmarkActive==true?true:false);
                                  print(isBookmarkedList[checkedPos] );
                                  print(isBookmarkedList);

                                  //bookmarkActive = !bookmarkActive;
//                                if(bookmarkActive == true){
//                                  print("===coming if");
//                                  if(bookedmarkVideos.contains(checkedPos.toString())){
//                                    bookedmarkVideos.remove(checkedPos.toString());
//                                  }else{
//                                    bookedmarkVideos.add(checkedPos.toString());
//                                  }
//                                  //bookmarkActive = !bookmarkActive;
//                                }else{
//                                  print("=======coming else");
//                                  print(bookedmarkVideos);
//                                  if(bookedmarkVideos.contains(checkedPos.toString())){
//                                    bookedmarkVideos.remove(checkedPos.toString());
//                                  }
//                                }
                                  Savebookmark_UseractionAPI(systemtime);
//                                print("====================calculation=============");
//                                print(bookedmarkVideos);
//                                storeList_pref();
                                });
                              },
                              child: Container(
                                margin: EdgeInsets.only(top: 25.9/764 *screenHeight),
                                child: SvgPicture.asset(
                                  isBookmarkedList[checkedPos] == true ? getPrepareSvgImages.bookmarkActive :getPrepareSvgImages.bookmarkInactive,
                                  //fit: BoxFit.scaleDown,
                                ),
                              ),
                            ),
                            /** concept tezt**/
                            labelList.length == 0?
                            Container(
                              margin: EdgeInsets.only(top: 25.9/764 * screenHeight,left: 19/360*screenWidth),
                              height: 25/764 * screenHeight,
                              width: 76/360*screenWidth,
                              decoration: finalLabelLists[0]==null?BoxDecoration():
                              BoxDecoration(
                                  borderRadius: BorderRadius.all(Radius.circular(2)),
                                  color: NeutralColors.sun_yellow.withOpacity(0.12)

                              ),
                              child:finalLabelLists[0]==null?Text(""):
                              Center(child:Text(
                                //CalculationScreenStrings.Text_Concepts,
                                finalLabelLists[0].toString(),
                                style: TextStyle (
                                  color: NeutralColors.mango,
                                  fontWeight: FontWeight.w400,
                                  fontFamily: "IBMPlexSans",
                                  fontStyle: FontStyle.normal,
                                  fontSize: (14/360)*screenWidth,
                                ),
                                textAlign: TextAlign.left,
                              ),),
                            ):
                            Container(
                              margin: EdgeInsets.only(top: 25.9/764 * screenHeight,left: 19/360*screenWidth),
                              height: 25/764 * screenHeight,
                              width: 76/360*screenWidth,
                              decoration:labelList[checkedPos]==null?BoxDecoration():
                              BoxDecoration(
                                  borderRadius: BorderRadius.all(Radius.circular(2)),
                                  color: NeutralColors.sun_yellow.withOpacity(0.12)

                              ),
                              child:labelList[checkedPos]==null?Text(""):
                              Center(child:Text(
                                //CalculationScreenStrings.Text_Concepts,
                               labelList[checkedPos].toString(),
                                style: TextStyle (
                                  color: NeutralColors.mango,
                                  fontWeight: FontWeight.w400,
                                  fontFamily: "IBMPlexSans",
                                  fontStyle: FontStyle.normal,
                                  fontSize: (14/360)*screenWidth,
                                ),
                                textAlign: TextAlign.left,
                              ),),
                            ),
                          ],
                        ),
                        ),
                      ],
                    ),
                  ),
                  /**For Accuracy text container**/
                  Container(
                    color: Colors.white,
                    margin: EdgeInsets.only(left: 20/360*screenWidth),
                    child: Text(
                      //CalculationScreenStrings.Text_Accuracy,
                      widget.subtitle.toString(),
                      style: TextStyle (
                          fontWeight: FontWeight.w500,
                          fontFamily: "IBMPlexSans",
                          fontStyle: FontStyle.normal,
                          fontSize: (12/360)*screenWidth,
                          color: SemanticColors.blueGrey
                      ),
                      textAlign: TextAlign.left,
                    ),
                  ),
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.only(top:(20/360)*screenWidth ,left: (20/360)*screenWidth),
//                child: Column(
//                  children: <Widget>[
//                    Container(
//                      child: Text(
//                        CalculationScreenStrings.Tab_UpNext,
//                        style: TextStyle (
//                          color: NeutralColors.purpleish_blue,
//                          fontWeight: FontWeight.w500,
//                          fontFamily: "IBMPlexSans",
//                          fontStyle: FontStyle.normal,
//                          fontSize: (14/360)*screenWidth,
//                        ),
//                        textAlign: TextAlign.left,
//                      ),
//                    ),
//                    new Container(height: 1, width: 70, color: NeutralColors.purpleish_blue,
//                      margin: const EdgeInsets.only(top: 10.0),),
//                  ],
//                ),
            width: 80,
              padding: EdgeInsets.only(
                bottom: 3, // space between underline and text
              ),
              decoration: BoxDecoration(
                  border: Border(bottom: BorderSide(
                    color: NeutralColors.purpleish_blue,  // Text colour here
                    width: 2.0, // Underline width
                  ))
              ),
               child: Align(
                 alignment: Alignment.center,
                 child: Text(
                   CalculationScreenStrings.Tab_UpNext,
                   style: TextStyle (
                     color: NeutralColors.purpleish_blue,
                     fontWeight: FontWeight.w500,
                     fontFamily: "IBMPlexSans",
                     fontStyle: FontStyle.normal,
                     fontSize: (14/360)*screenWidth,
                   ),
                   textAlign: TextAlign.left,
                 ),
               ),
            ),
             /**Up Next Section**/
             Expanded(
               child: QueriesHtml(bookedmarkVideos,func: function),
             ),
             /**Tab bar section**/
//              Expanded(
//                child: new Column(
//                  children: <Widget>[
//                    new Container(
//                      color: Colors.white,
//                      width: 150,
//                      // decoration: new BoxDecoration(color: Theme.of(context).primaryColor),
//                      child: new TabBar(
//                        controller: _tabController,
//                        indicatorColor: NeutralColors.purpleish_blue,
//                        labelColor: NeutralColors.purpleish_blue,
//                        unselectedLabelColor: NeutralColors.blue_grey,
//                        indicatorSize: TabBarIndicatorSize.tab,
//                        tabs: tabList,
//                        indicator:  UnderlineTabIndicator(
//                          borderSide: BorderSide(width: 2.0,color: NeutralColors.purpleish_blue ),
//                          insets: EdgeInsets.symmetric(horizontal:20.0),
//
//                        ),
//                      ),
//                    ),
//                    new Expanded(
//                      //height: 574/720 * screenHeight,
//                      child: new TabBarView(
//                        controller: _tabController,
//                        children: <Widget>[
//                          QueriesHtml(func: function),
////                          QueriesHtml(func: function),
////                          QueriesHtml(func: function),
//                        ],
//                      ),
//                    )
//                  ],
//                ),
//              ),
          ],
        ),
      ),
    );
  }


  function(value,label,pos,videoClicked,videoID) {
    setState(() {
      print(label);
      print("label");
      video_png = value;
      labeltitle = label;
      checkedPos = pos;
      bookmarkActive = videoClicked;
      videoid = videoID;
    });
  }
  function_dropdown(value) => setState(() => dropdown_active = value);

  _getChewiePlayer(bool offStageFlag,final screenHeight,final screenWidth){
    print("**********************************************8VIDEOID");
    print(isBookmarkedList);
    print(videoid);
    print(checkedPos);
    Offstage offstage = new Offstage(
      offstage: offStageFlag,
      child: GestureDetector(
        onTap: (){
          initConnectivity();
//          _connectivitySubscription =
//              _connectivity.onConnectivityChanged.listen(_updateConnectionStatus);
          //AppRoutes.push(context,videoid!=null ? WebViewExample1(videoid,userid) : WebViewExample1(finalVideosList[0],userid));
        },
        child: Center(
          child: Container(
              constraints: new BoxConstraints.expand(
                height:181/720*screenHeight,
              ),
              alignment: Alignment.center,
              decoration:  BoxDecoration(
                borderRadius: new BorderRadius.circular(5.0),
              ),
              child:  Stack(
                  children: <Widget> [
                    new CachedNetworkImage(
                      imageUrl:video_png == null ? finalThumbnailList.length==0 ? null : finalThumbnailList[0] : video_png,
                      imageBuilder: (context, imageProvider) => Center(
                        child: Container(
                          decoration: BoxDecoration(
                            color: Colors.white,
                            image: DecorationImage(
                                image: imageProvider,
                                fit: BoxFit.fill,
                               ),
                            borderRadius: new BorderRadius.circular(5.0),

                          ),
                        ),
                      ),
                      placeholder: (context, url) =>
                          Center(
                            child: Container(
                              decoration: BoxDecoration(borderRadius: BorderRadius.circular(5),),
                              //height:80/720*screenHeight,
                              height:181/720*screenHeight,
                              child:  Image.asset(
                                "assets/images/thumbnail.png",
                                fit: BoxFit.fill,
                              ),),
                          ),
                      errorWidget: (context, url, error) => 
                      //Icon(Icons.error),
                          Center(
                            child: Container(
                              decoration: BoxDecoration(borderRadius: BorderRadius.circular(5),),
                              //height:80/720*screenHeight,
                              height:181/720*screenHeight,
                              child:  Image.asset(
                                "assets/images/thumbnail.png",
                                fit: BoxFit.fill,
                              ),),
                          ),
                      ),
                    Center(child:Align(
                      alignment: Alignment.center,
                      child:  Container(
                        height: (40 / 720) * screenHeight,
                        width: (40 / 360) * screenWidth,
                        child: SvgPicture.asset(
                          getPrepareSvgImages.playwhite,
                          fit: BoxFit.contain,
                        ),
                      ),
                    ),),
                  ]
              ),
            ),
        ),
    ));
    return offstage;
  }

  Future<void> getUserid() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    userid = prefs.getString("userId");
  }

  void GetMobileDataStatus_pref() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    if(prefs.getBool("limitCellularDataUsage") != null){
      blimitCellularDataUsage = prefs.getBool("limitCellularDataUsage");
    }
    print("+++++++++++++++++");
    print(blimitCellularDataUsage);
  }

  void noInternetMessage(String value) {
    _keyCalc.currentState.showSnackBar(new SnackBar(
      content: new Text(value),
      action: SnackBarAction(
        textColor: Colors.blueAccent,
        label: "Dismiss",
        onPressed: () {
          // Some code to undo the change.
        },
      ),
      backgroundColor: Colors.blueGrey,
      duration: Duration(days: 365),
    ));
  }

  void LoadDialogifmibiledataactive() {
    final dialog =  AlertDialog(
      title: Text(
        'Please confirm',
        style: TextStyle(
          color: NeutralColors.dark_navy_blue,
          fontWeight: FontWeight.w500,
          fontFamily: "IBMPlexSans",
          fontStyle: FontStyle.normal,
          fontSize: (14/360)*screenWidth,
        ),
      ),
      content: const Text(
          'You are about to use mobile data to see the Video',
        style: TextStyle(
          color: NeutralColors.dark_navy_blue,
          fontWeight: FontWeight.w400,
          fontFamily: "IBMPlexSans",
          fontStyle: FontStyle.normal,
          fontSize: 14,
        ),),
      actions: <Widget>[
        FlatButton(
          child: const Text('GO BACK',
            style: TextStyle(
              color: PrimaryColors.azure_Dark,
              fontWeight: FontWeight.w500,
              fontFamily: "IBMPlexSans",
              fontStyle: FontStyle.normal,
              fontSize: 13,
            ),),
          onPressed: () {
            //Navigator.pop(context);
            Navigator.of(context).pop();
          },
        ),
        FlatButton(
          child: const Text('ALLOW',
            style: TextStyle(
              color: PrimaryColors.azure_Dark,
              fontWeight: FontWeight.w500,
              fontFamily: "IBMPlexSans",
              fontStyle: FontStyle.normal,
              fontSize: 13,
            ),),
          onPressed: () {
            //Navigator.pop(context);
            print("======================================================================================");
            print(Platform.isIOS);
            if(Platform.isIOS){
              Navigator.of(context).pop();
            AppRoutes.push(context,videoid!=null ? WebViewExample1(videoid,userid) : WebViewExample1(finalVideosList[0],userid));
            }
            if(Platform.isAndroid){
                if(crashingDeviceProductNames.contains(_deviceData['product'])){
                  Navigator.of(context).pop();
                  AppRoutes.push(context,videoid!=null ? WebViewExample111(videoid,userid) : WebViewExample111(finalVideosList[0],userid));
                }
              else{
              Navigator.of(context).pop();
              AppRoutes.push(context,videoid!=null ? WebViewExample11(videoid,userid) : WebViewExample11(finalVideosList[0],userid));
              }
            }
            
            //Navigator.of(context).pop();
          },
        )
      ],
    );
    showDialog(context: context, builder: (x) => dialog);
  }

  Future<void> _updateConnectionStatus(ConnectivityResult result) async {
    switch (result) {
      case ConnectivityResult.none:
        print("no net");
        return noInternetMessage('Internet Connection Lost');
      case ConnectivityResult.mobile:
        print("++++++++++++++MOBILE DATA"+cellularDataFlag.toString());
        // if(cellularDataFlag==null || cellularDataFlag){
        //   return  LoadDialogifmibiledataactive();
        // }
        if(cellularDataFlag == true){
          print("object1");
          LoadDialogifmibiledataactive();
        }else if(Platform.isIOS){
          print("object2");
          AppRoutes.push(context,videoid!=null ? WebViewExample1(videoid,userid) : WebViewExample1(finalVideosList[0],userid));
        }else if(Platform.isAndroid){
          print("object3");
            if(crashingDeviceProductNames.contains(_deviceData['product'])){
              AppRoutes.push(context,videoid!=null ? WebViewExample111(videoid,userid) : WebViewExample111(finalVideosList[0],userid));
            }
            else{
              AppRoutes.push(context,videoid!=null ? WebViewExample11(videoid,userid) : WebViewExample11(finalVideosList[0],userid));
            }
          }
        break;
        //return (cellularDataFlag == true) ?  LoadDialogifmibiledataactive() : Platform.isIOS ? AppRoutes.push(context,videoid!=null ? WebViewExample1(videoid,userid) : WebViewExample1(finalVideosList[0],userid)) ? Platform.isAndroid : AppRoutes.push(context,videoid!=null ? WebViewExample11(videoid,userid) : WebViewExample11(finalVideosList[0],userid)) : null;
      case ConnectivityResult.wifi:
        print("++++++++++++++WIFI DATA");
        if(Platform.isIOS){
              //Navigator.of(context).pop();
            AppRoutes.push(context,videoid!=null ? WebViewExample1(videoid,userid) : WebViewExample1(finalVideosList[0],userid));
            }
            if(Platform.isAndroid){
              if(crashingDeviceProductNames.contains(_deviceData['product'])){
                AppRoutes.push(context,videoid!=null ? WebViewExample111(videoid,userid) : WebViewExample111(finalVideosList[0],userid));
              }
              else{
                AppRoutes.push(context,videoid!=null ? WebViewExample11(videoid,userid) : WebViewExample11(finalVideosList[0],userid));
              }            }
            
        break;
      default:
        return Center(child: Text("No Internet Connection!"));
    }
  }
}



import 'dart:async';
import 'package:imsindia/views/prepare_pages/prepare_model_for_track.dart';
import 'package:imsindia/utils/Url.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

class PrepareHelperClass {
  String dbName = '';
  Future<Database> database;

  PrepareHelperClass(String dbName) {
    this.dbName = dbName;
  }
  createDB() async {
    // debugPrint(
    //   'prepare table definition ' +
    //       "CREATE TABLE ${URL.TBLForPrepareTrackApi}(areaName TEXT,attempt BOOLEAN,companyCode TEXT,difficultyId INT,difficultyName TEXT,enteredText TEXT,id VARCHAR,isCorrect BOOLEAN,isSectionCompleted TEXT,itemId TEXT,itemType TEXT,marked BOOLEAN,negativePoints TEXT,points TEXT, questionIndex TEXT,sectionId TEXT,sectionIndex TEXT, sectionName TEXT,selectedOptionId TEXT,studentId VARCHAR,subjectId TEXT,subjectName TEXT,testId VARCHAR,timeRemaining TEXT,timeStamp VARCHAR,timeTaken TEXT, topicId TEXT,topicName TEXT,trackNumber TEXT)",
    // );
    database = openDatabase(
      // Set the path to the database. Note: Using the `join` function from the
      // `path` package is best practice to ensure the path is correctly
      // constructed for each platform.
      join(await getDatabasesPath(), dbName),
      // When the database is first created, create a table to store dogs.
      onCreate: (db, version) {
        return db.execute(
          "CREATE TABLE ${URL.TBLForPrepareTrackApi}(areaName TEXT,attempt BOOLEAN,companyCode TEXT,difficultyId TEXT,difficultyName TEXT,enteredText TEXT,id TEXT,isCorrect TEXT,isSectionCompleted TEXT,itemId TEXT,itemType TEXT,marked TEXT,negativePoints TEXT,points TEXT, questionIndex TEXT,saved BOOLEAN,sectionId TEXT,sectionIndex TEXT, sectionName TEXT,selectedOptionId TEXT,studentId TEXT,subjectId TEXT,subjectName TEXT,testId TEXT,timeRemaining TEXT,timeStamp TEXT,timeTaken TEXT, topicId TEXT,topicName TEXT,trackNumber INT autoincrement)",
        );
      },


      // Set the version. This executes the onCreate function and provides a
      // path to perform database upgrades and downgrades.
      version: 1,
    );

    //debugPrint('Prepare db created successfully');
  }
  Future<void> check(PrepareModelClass data) async {
    // Get a reference to the database.
    Database db = await database;

    // Insert the BlogContent into the correct table. Also specify the
    // `conflictAlgorithm`. In this case, if the same BlogContent is inserted
    // multiple times, it replaces the previous data.
    if (db == null) {
      db = await openDatabase(
        // Set the path to the database. Note: Using the `join` function from the
        // `path` package is best practice to ensure the path is correctly
        // constructed for each platform.
        join(await getDatabasesPath(), 'preparedbfortrackapi.db'),
      );
    }
//    debugPrint('Emax map of data : ${data.toMap()}');
//    debugPrint('Emax String of data : ${data.toString()}');
    await db.insert(
      '${URL.TBLForPrepareTrackApi}',
      data.toMap(),
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
  }

  Future<void> insert(PrepareModelClass data) async {
    // Get a reference to the database.
    Database db = await database;

    // Insert the BlogContent into the correct table. Also specify the
    // `conflictAlgorithm`. In this case, if the same BlogContent is inserted
    // multiple times, it replaces the previous data.
    if (db == null) {
      db = await openDatabase(
        // Set the path to the database. Note: Using the `join` function from the
        // `path` package is best practice to ensure the path is correctly
        // constructed for each platform.
        join(await getDatabasesPath(), 'preparedbfortrackapi.db'),
      );
    }
   // debugPrint('Emax map of data : ${data.toMap()}');
    //debugPrint('Emax String of data : ${data.toString()}');
    await db.insert(
      '${URL.TBLForPrepareTrackApi}',
      data.toMap(),
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
  }

  Future<List<PrepareModelClass>> getData() async {
    // Get a reference to the database.
    Database db = await database;

    if (db == null) {
      db = await openDatabase(
        // Set the path to the database. Note: Using the `join` function from the
        // `path` package is best practice to ensure the path is correctly
        // constructed for each platform.
        join(await getDatabasesPath(), dbName),
      );
    }
    // Query the table for all The BlogContent.
    final List<Map<String, dynamic>> maps = await db.query('TableForPrepareTrackApiDetails');

    // Convert the List<Map<String, dynamic> into a List<Dog>.
   // print("MAPS ${maps}");
    return List.generate(maps.length, (i) {
      return PrepareModelClass(
        areaName: maps[i]['areaName'],
        attempt: (maps[i]['attempt']==1)?true:false,
        companyCode: maps[i]['companyCode'],
        difficultyId: int.parse(maps[i]['difficultyId']),
        difficultyName: maps[i]['difficultyName'],
        enteredText: maps[i]['enteredText'],
        id: maps[i]['id'],
        isCorrect: (maps[i]['isCorrect']==1)?true:false,
        isSectionCompleted: maps[i]['isSectionCompleted'],
        itemId: int.parse(maps[i]['itemId']),
        itemType: maps[i]['itemType'],
        marked: (maps[i]['marked']==1)?true:false,
        negativePoints: int.parse(maps[i]['negativePoints']),
        points: int.parse(maps[i]['points']),
        questionIndex: int.parse(maps[i]['questionIndex']),
        saved: (maps[i]['saved']==1)?true:false,
        sectionId: int.parse(maps[i]['sectionId']),
        sectionIndex: int.parse(maps[i]['sectionIndex']),
        sectionName: maps[i]['sectionName'],
        selectedOptionId: int.parse(maps[i]['selectedOptionId']),
        studentId: maps[i]['studentId'],
        subjectId: int.parse(maps[i]['subjectId']),
        subjectName: maps[i]['subjectName'],
        testId: maps[i]['testId'],
        timeRemaining: int.parse(maps[i]['timeRemaining']),
        timeStamp: maps[i]['timeStamp'],
        timeTaken: int.parse(maps[i]['timeTaken']),
        topicId: int.parse(maps[i]['topicId']),
        topicName: maps[i]['topicName'],
        trackNumber: int.parse(maps[i]['trackNumber']),

      );


//      return Dog(
//        id: maps[i]['id'],
//        name: maps[i]['name'],
//        age: maps[i]['age'],
//      );
    });
  }

  Future<void> update(PrepareModelClass data) async {
    // Get a reference to the database.
    Database db = await database;
    if (db == null) {
      db = await openDatabase(
        // Set the path to the database. Note: Using the `join` function from the
        // `path` package is best practice to ensure the path is correctly
        // constructed for each platform.
        join(await getDatabasesPath(), dbName),
      );
    }
    // Update the given BlogContent.
    await db.update(
      '${URL.TBLForPrepareTrackApi}',
      data.toMap(),

      where: "content = ?",
      // Pass the BlogContent's id as a whereArg to prevent SQL injection.
      whereArgs: [data.timeTaken],
    );
  }

  Future<void> delete() async {
    // Get a reference to the database.
    Database db = await database;

    if (db == null) {
      db = await openDatabase(
        // Set the path to the database. Note: Using the `join` function from the
        // `path` package is best practice to ensure the path is correctly
        // constructed for each platform.
        join(await getDatabasesPath(), dbName),
      );
    }
    // Remove the BlogContent from the database.
    await db.delete(
      '${URL.TBLForPrepareTrackApi}',
      // Use a `where` clause to delete a specific BlogContent.
      where: "content = ?",
      // Pass the BlogContent's id as a whereArg to prevent SQL injection.
      whereArgs: [],
    );
  }

  Future<void> deleteAll() async {
    // Get a reference to the database.
    Database db = await database;
   // debugPrint('In Emaximiser deleteAll delete from ${URL.TBLForPrepareTrackApi}');
    if (db == null) {
      db = await openDatabase(
        // Set the path to the database. Note: Using the `join` function from the
        // `path` package is best practice to ensure the path is correctly
        // constructed for each platform.
        join(await getDatabasesPath(), dbName),
      );
    }
    // Remove the BlogContent from the database.
    await db.rawDelete(
      'delete from ${URL.TBLForPrepareTrackApi}',
      // Use a `where` clause to delete a specific BlogContent.
    );
    //debugPrint('Emaximiser db deleted successfully');

  }

  Future<void> deleteTable() async {
    // Get a reference to the database.
    Database db = await database;
   // debugPrint('In Emaximiser deleteAll drop table ${URL.TBLForPrepareTrackApi}');
    if (db == null) {
      db = await openDatabase(
        // Set the path to the database. Note: Using the `join` function from the
        // `path` package is best practice to ensure the path is correctly
        // constructed for each platform.
        join(await getDatabasesPath(), dbName),
      );
    }
    // Remove the BlogContent from the database.
    await db.rawDelete(
      'drop table ${URL.TBLForPrepareTrackApi}',
      // Use a `where` clause to delete a specific BlogContent.
    );
    //debugPrint('Emaximiser table deleted successfully');

  }
}


class CommentsOptionsHtml{
  String nameofperson;
  String durationofcomment;
  String comment_text;
  int numberofvotes;
  String replyofcomment;
  String replies;
  var imageURL;
  CommentsOptionsHtml({
    this.nameofperson,
    this.durationofcomment,
    this.comment_text,
    this.numberofvotes,
    this.replyofcomment,
    this.replies,
    this.imageURL
  });

  CommentsOptionsHtml.Comment(this.nameofperson,this.durationofcomment,this.comment_text,this.numberofvotes);
  CommentsOptionsHtml.replies(this.replies);
}

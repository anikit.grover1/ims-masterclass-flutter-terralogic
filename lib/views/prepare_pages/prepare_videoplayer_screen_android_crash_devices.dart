

import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:imsindia/utils/colors.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:webview_flutter/webview_flutter.dart';
import 'package:imsindia/utils/emaximiser_svg_icons.dart';
import 'package:http/http.dart' as http;
//import 'package:flutter_inappwebview/flutter_inappwebview.dart';

//void main() => runApp(
//      MaterialApp(
//        home: WebViewExample(),
//        theme: ThemeData(
//          primarySwatch: Colors.red,
//        ),
//      ),
//    );
String userID;
class WebViewExample111 extends StatefulWidget {
  String videoid;
  String userid;
  WebViewExample111(this.videoid,this.userid);

  @override
  _WebViewExampleState createState() => _WebViewExampleState();
}

class _WebViewExampleState extends State<WebViewExample111> with WidgetsBindingObserver{
  final Completer<WebViewController> _controller =
  Completer<WebViewController>();
  WebViewController webViewController;
  String id;


  
  @override
  void initState() {
    //To enable landscape in this particular screen alone(Android and iOS)
    SystemChrome.setPreferredOrientations([
    DeviceOrientation.landscapeRight,
    DeviceOrientation.landscapeLeft,
    //DeviceOrientation.portraitUp,
  ]);
    // TODO: implement initState
    print("***************************************123");
    print(widget.videoid);
    print(widget.userid);
    super.initState();
    WidgetsBinding.instance.addObserver(this);
  }

  @override
  void dispose() {
    //To reset the screen orientation 
    SystemChrome.setPreferredOrientations([
    DeviceOrientation.portraitUp,
    ]);
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
    _controller.isCompleted;
  }

  @override
  void deactivate() {
    //To reset the screen orientation 
    SystemChrome.setPreferredOrientations([
    DeviceOrientation.portraitUp,
    ]);
    super.deactivate();
    webViewController?.reload();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    if (state == AppLifecycleState.paused) {
      webViewController?.reload();
    }
  }

  @override
  Widget build(BuildContext context) {
    final screenHeight = MediaQuery.of(context).size.height;
    final screenWidth = MediaQuery.of(context).size.width;

    return Scaffold(
      
      body: SafeArea(
              child: Center(
                child: Container(
                  height: screenHeight ,
                  width: screenWidth * .9,
                  child: Center(
                    child: WebView(
          
  initialUrl: widget.videoid != null ? "https://ims.quizky.com/vimeo-player/"+widget.videoid+"/ims/"+widget.userid:"https://ims.quizky.com/vimeo-player/d6562e61116a55a298105a73c50ec967/ims/342934 ",
  
  javascriptMode: JavascriptMode.unrestricted,
  // initialHeaders: {

  // },
  // initialOptions: InAppWebViewWidgetOptions(
  //   androidInAppWebViewOptions: AndroidInAppWebViewOptions(
  //     hardwareAcceleration: true
  //   ),

  //     inAppWebViewOptions: InAppWebViewOptions(
  //       debuggingEnabled: true,
  //       preferredContentMode: InAppWebViewUserPreferredContentMode.MOBILE,


  //     )
  // ),
  onWebViewCreated: (controller) {
    //webView = controller;
    webViewController = controller;
     _controller.complete(webViewController);
  },
                         
  
   ),
                  ),
                ),
              ),
      ),

    );

  }
  

}

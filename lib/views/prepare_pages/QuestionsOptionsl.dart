import 'package:flutter/material.dart';

class QuestionsOptionsHtml{
  String qid;
  String question;
  List<String> options;
  PageController controller;
  int pos;
  var imageURL;
  QuestionsOptionsHtml({
    this.qid,
    this.question,
    this.options,
    this.controller,
    this.imageURL
  });
  QuestionsOptionsHtml.pos(this.pos);

}

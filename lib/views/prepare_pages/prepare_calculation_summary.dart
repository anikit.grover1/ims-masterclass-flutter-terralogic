import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:percent_indicator/circular_percent_indicator.dart';
import 'package:imsindia/resources/strings/prepare.dart';
import 'package:imsindia/utils/colors.dart';


class PrepareCalculationSummaryHtml extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final screenHeight = MediaQuery.of(context).size.height;
    final screenWidth = MediaQuery.of(context).size.width;
    String Score;
    String ScoreText;
    Future<bool> _onWillPop() {
      int count = 0;
      Navigator.popUntil(context, (route) {
        return count++ == 2;
      });    }
    return WillPopScope(
      onWillPop: _onWillPop,
      child: Scaffold(
        body: Container(
          constraints: BoxConstraints.expand(),
          decoration: BoxDecoration(
            color:NeutralColors.pureWhite,
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              Align(
                alignment: Alignment.topCenter,
                child: Container(
                  width: 182 / 360 * screenWidth,
                  margin: EdgeInsets.only(top: 64 /720 * screenHeight),
                  child: Text(
                      CalculationSummaryScreenStrings.Text_CalculationSummary,
                    style: TextStyle(
                      fontSize: 18,
                      color:NeutralColors.dark_navy_blue,
                      fontFamily: "IBMPlexSans",
                      fontWeight: FontWeight.w700,
                    ),
                    textAlign: TextAlign.center,
                  ),
                ),
              ),
              Container(
                height: 147/720*screenHeight,
                margin: EdgeInsets.only(
                    left: 70 / 360 * screenWidth,
                    top: 16 / 720 * screenHeight,
                    right: 59 / 360 * screenWidth),
                  decoration: BoxDecoration(
                    image: DecorationImage(
                      image: AssetImage( "assets/images/elements.png"),
                        fit: BoxFit.scaleDown,
                    ),
                  ),
                  child:Center(
                    child: Padding(
                      padding: EdgeInsets.only(top: 20.0/720*screenHeight),
                      child: CircularPercentIndicator(
                      linearGradient: LinearGradient(
                          colors: [SemanticColors.dark_purpely, SemanticColors.light_purpely],

                      ) ,
                      circularStrokeCap: CircularStrokeCap.butt,
                      backgroundColor:Color.fromARGB(220,220,220,220),
                     // progressColor: Color.fromARGB(250, 250, 110, 216),
                      radius: 110.0/720*screenHeight,
                      lineWidth: 6,
                      animation: true,
                      percent: 0.7,
                      center: Container(
//                                      height:45,
//                                      width:48,
                       // margin:EdgeInsets.only(left:25.8/360*screenWidth,right:26.2/360*screenWidth,bottom: 0.0,top:28.0/720*screenHeight),
                        child: Center(
                          child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              mainAxisSize: MainAxisSize.min,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: <Widget>[
                                Text('74',
                                  style: TextStyle(
                                    color: Color(0xff7a71d5),
                                    fontSize: 40/720*screenHeight,
                                    fontFamily: "IBMPlexSans",
                                    fontWeight: FontWeight.w500,
                                  ),),
                                Text('SCORE',style: TextStyle(
                                  color: Color(0xff7a71d5),
                                  fontSize: 12/720*screenHeight,
                                  fontFamily: "IBMPlexSans",
                                  fontWeight: FontWeight.w500,
                                ),)
                              ]
                          ),
                        ),
                      ),
                  ),
                    ), ),
              ),
              Align(
                alignment: Alignment.topCenter,
                child: Container(
                  width: 300/360*screenWidth,
                 height: 80/720*screenHeight,
                  margin: EdgeInsets.only(top: 13/720*screenHeight),
                  decoration: BoxDecoration(
                    border: Border.all(
                      color: Color.fromARGB(255, 242, 244, 244),
                      width: 1,
                    ),
                    borderRadius: BorderRadius.all(Radius.circular(5)),
                  ),
                  child: Stack(
                    alignment: Alignment.center,
                    children: [
                      Positioned(
                        left: 15/360*screenWidth,
                        right: 25/360*screenWidth,

                        child: Container(
                          height: 48/720*screenHeight,
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.stretch,
                            children: [
                              Align(
                                alignment: Alignment.centerLeft,
                                child: Container(
                                  width: 72/360*screenWidth,
                                  height: 44/720*screenHeight,
                                  child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.stretch,
                                    children: [
                                      Container(

                                        margin: EdgeInsets.only(left: 18/360*screenWidth),
                                        child: Text(
                                          CalculationSummaryScreenStrings.Text_valueaccuracy,
                                          style: TextStyle(
                                            color: Color.fromARGB(255, 0, 3, 44),
                                            fontSize: 20/720*screenHeight,
                                            fontFamily: "IBMPlexSans",
                                            fontWeight: FontWeight.w700,
                                          ),
                                          textAlign: TextAlign.left,
                                        ),
                                      ),
                                      Spacer(),
                                      Container(
                                        //margin: EdgeInsets.only(right: 1/360*screenWidth),
                                        child: Text(
                                          CalculationSummaryScreenStrings.Text_CapAccuracy,
                                          style: TextStyle(
                                            color: Color.fromARGB(255, 87, 93, 96),
                                            fontSize: 12/720*screenHeight,
                                            fontFamily: "IBMPlexSans",
                                            fontWeight: FontWeight.bold,
                                          ),
                                          textAlign: TextAlign.left,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                              Align(
                                alignment: Alignment.centerLeft,
                                child: Container(
                                  width: 1/360*screenWidth,
                                  height: 48/720*screenHeight,
                                  margin: EdgeInsets.only(left: 19/360*screenWidth),
                                  decoration: BoxDecoration(
                                    color: Color.fromARGB(255, 242, 244, 244),
                                  ),
                                  child: Container(),
                                ),
                              ),
                              Spacer(),
                              Align(
                                alignment: Alignment.centerLeft,
                                child: Container(
                                  width: 1/360*screenWidth,
                                  height: 48/720*screenHeight,
                                  margin: EdgeInsets.only(right: 24/360*screenWidth),
                                  decoration: BoxDecoration(
                                    color: Color.fromARGB(255, 242, 244, 244),
                                  ),
                                  child: Container(),
                                ),
                              ),
                              Align(
                                alignment: Alignment.centerLeft,
                                child: Container(
                                  width: 63/360*screenWidth,
                                  height: 44/720*screenHeight,
                                  child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.stretch,
                                    children: [
                                      Container(
                                        margin: EdgeInsets.only(left: 14/360*screenWidth, right: 13/360*screenWidth),
                                        child: Text(
                                          CalculationSummaryScreenStrings.Text_valueSkipped,
                                          style: TextStyle(
                                            color: Color.fromARGB(255, 0, 3, 44),
                                            fontSize: 20/720*screenHeight,
                                            fontFamily: "IBMPlexSans",
                                            fontWeight: FontWeight.w700,
                                          ),
                                          textAlign: TextAlign.left,
                                        ),
                                      ),
                                      Spacer(),
                                      Text(
                                        CalculationSummaryScreenStrings.Text_Skipped,
                                        style: TextStyle(
                                          color: Color.fromARGB(255, 87, 93, 96),
                                          fontSize: 12/720*screenHeight,
                                          fontFamily: "IBMPlexSans",
                                          fontWeight: FontWeight.bold,
                                        ),
                                        textAlign: TextAlign.left,
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                      Positioned(
                        child: Container(
                          width: 65/360*screenWidth,
                          height: 44/720*screenHeight,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.stretch,
                            children: [
                              Container(
                                margin: EdgeInsets.symmetric(horizontal: 15),
                                child: Text(
                                  CalculationSummaryScreenStrings.Text_valueCorrect,
                                  style: TextStyle(
                                    color: Color.fromARGB(255, 0, 3, 44),
                                    fontSize: 20/720*screenHeight,
                                    fontFamily: "IBMPlexSans",
                                    fontWeight: FontWeight.w700,
                                  ),
                                  textAlign: TextAlign.left,
                                ),
                              ),
                              Spacer(),
                              Text(
                                CalculationSummaryScreenStrings.Text_Correct,
                                style: TextStyle(
                                  color: Color.fromARGB(255, 87, 93, 96),
                                  fontSize: 12/720*screenHeight,
                                  fontFamily: "IBMPlexSans",
                                  fontWeight: FontWeight.bold,
                                ),
                                textAlign: TextAlign.left,
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              Align(
                alignment: Alignment.topCenter,
                child: Container(
                  width: 300/360*screenWidth,
//                height: 257,
                  margin: EdgeInsets.only(top: 0.02*screenHeight),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      Container(
                        height: 45/720*screenHeight,
                        child: Stack(
                          alignment: Alignment.center,
                          children: [
                            Positioned(
                              left: 0,
                              top: 0,
                              right: 0,
                              child: Opacity(
                                opacity: 0.05,
                                child: Container(
                                  height: 45/720*screenHeight,
                                  decoration: BoxDecoration(
                                    gradient: LinearGradient(
                                      begin: Alignment(-0.011, 0.507),
                                      end: Alignment(1.031, 0.483),
                                      stops: [
                                        0,
                                        1,
                                      ],
                                      colors: [
                                        Color.fromARGB(255, 168, 174, 35),
                                        Color.fromARGB(255, 201, 110, 216),
                                      ],
                                    ),
                                    borderRadius: BorderRadius.all(Radius.circular(5)),
                                  ),
                                  child: Container(),
                                ),
                              ),
                            ),
                            Positioned(
                              left: 25/360*screenWidth,
                              top: 6/720*screenHeight,
                              right: 28/360*screenWidth,
                              child: Container(
                                height: 27/720*screenHeight,
                                child: Row(
                                  crossAxisAlignment: CrossAxisAlignment.stretch,
                                  children: [
                                    Align(
                                      alignment: Alignment.topLeft,
                                      child: Container(
                                        margin: EdgeInsets.only(top: 8/720*screenHeight),
                                        child: Text(
                                          CalculationSummaryScreenStrings.Text_Runs,
                                          style: TextStyle(
                                            color: Color.fromARGB(255, 0, 3, 44),
                                            fontSize: 14/678*screenHeight,
                                            fontFamily: "IBMPlexSans",
                                          ),
                                          textAlign: TextAlign.left,
                                        ),
                                      ),
                                    ),
                                    Spacer(),
                                    Align(
                                      alignment: Alignment.topLeft,
                                      child: Text(
                                        CalculationSummaryScreenStrings.Text_ValueRuns,
                                        style: TextStyle(
                                          color: Color.fromARGB(255, 0, 3, 44),
                                          fontSize: 14/678*screenHeight,
                                          fontFamily: "IBMPlexSans",
                                          fontWeight: FontWeight.w500,
                                        ),
                                        textAlign: TextAlign.left,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      Container(
                        height: 45/720*screenHeight,
                        margin: EdgeInsets.only(top: 8/720*screenHeight),
                        child: Stack(
                          alignment: Alignment.center,
                          children: [
                            Positioned(
                              left: 0,
                              top: 0,
                              right: 0,
                              child: Opacity(
                                opacity: 0.05,
                                child: Container(
                                  height: 45/720*screenHeight,
                                  decoration: BoxDecoration(
                                    gradient: LinearGradient(
                                      begin: Alignment(-0.011, 0.507),
                                      end: Alignment(1.031, 0.483),
                                      stops: [
                                        0,
                                        1,
                                      ],
                                      colors: [
                                        Color.fromARGB(255, 168, 174, 35),
                                        Color.fromARGB(255, 201, 110, 216),
                                      ],
                                    ),
                                    borderRadius: BorderRadius.all(Radius.circular(5)),
                                  ),
                                  child: Container(),
                                ),
                              ),
                            ),
                            Positioned(
                              left: 25/360*screenWidth,
                              top: 6/720*screenHeight,
                              right: 28/360*screenWidth,
                              child: Container(
                                height: 27/720*screenHeight,
                                child: Row(
                                  crossAxisAlignment: CrossAxisAlignment.stretch,
                                  children: [
                                    Align(
                                      alignment: Alignment.topLeft,
                                      child: Container(
                                        margin: EdgeInsets.only(top: 8/720*screenHeight),
                                        child: Text(
                                          CalculationSummaryScreenStrings.Text_50,
                                          style: TextStyle(
                                            color: Color.fromARGB(255, 0, 3, 44),
                                            fontSize: 14/678*screenHeight,
                                            fontFamily: "IBMPlexSans",
                                          ),
                                          textAlign: TextAlign.left,
                                        ),
                                      ),
                                    ),
                                    Spacer(),
                                    Align(
                                      alignment: Alignment.topLeft,
                                      child: Text(
                                        CalculationSummaryScreenStrings.Text_Value50,
                                        style: TextStyle(
                                          color: Color.fromARGB(255, 0, 3, 44),
                                          fontSize: 14/678*screenHeight,
                                          fontFamily: "IBMPlexSans",
                                          fontWeight: FontWeight.w500,
                                        ),
                                        textAlign: TextAlign.left,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      Container(
                        height: 45/720*screenHeight,
                        margin: EdgeInsets.only(top: 8/720*screenHeight),
                        child: Stack(
                          alignment: Alignment.center,
                          children: [
                            Positioned(
                              left: 0,
                              top: 0,
                              right: 0,
                              child: Opacity(
                                opacity: 0.05,
                                child: Container(
                                  height: 45/720*screenHeight,
                                  decoration: BoxDecoration(
                                    gradient: LinearGradient(
                                      begin: Alignment(-0.011, 0.507),
                                      end: Alignment(1.031, 0.483),
                                      stops: [
                                        0,
                                        1,
                                      ],
                                      colors: [
                                        Color.fromARGB(255, 168, 174, 35),
                                        Color.fromARGB(255, 201, 110, 216),
                                      ],
                                    ),
                                    borderRadius: BorderRadius.all(Radius.circular(5)),
                                  ),
                                  child: Container(),
                                ),
                              ),
                            ),
                            Positioned(
                              left: 25/360*screenWidth,
                              top: 6/720*screenHeight,
                              right: 28/360*screenWidth,
                              child: Container(
                                height: 27/720*screenHeight,
                                child: Row(
                                  crossAxisAlignment: CrossAxisAlignment.stretch,
                                  children: [
                                    Align(
                                      alignment: Alignment.topLeft,
                                      child: Container(
                                        margin: EdgeInsets.only(top: 8/720*screenHeight),
                                        child: Text(
                                          CalculationSummaryScreenStrings.Text_100,
                                          style: TextStyle(
                                            color: Color.fromARGB(255, 0, 3, 44),
                                            fontSize: 14/678*screenHeight,
                                            fontFamily: "IBMPlexSans",
                                          ),
                                          textAlign: TextAlign.left,
                                        ),
                                      ),
                                    ),
                                    Spacer(),
                                    Align(
                                      alignment: Alignment.topLeft,
                                      child: Text(
                                        CalculationSummaryScreenStrings.Text_Value100,
                                        style: TextStyle(
                                          color: Color.fromARGB(255, 0, 3, 44),
                                          fontSize: 14/678*screenHeight,
                                          fontFamily: "IBMPlexSans",
                                          fontWeight: FontWeight.w500,
                                        ),
                                        textAlign: TextAlign.left,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      Container(
                        height: 45/720*screenHeight,
                        margin: EdgeInsets.only(top: 8/720*screenHeight),
                        child: Stack(
                          alignment: Alignment.center,
                          children: [
                            Positioned(
                              left: 0,
                              top: 0,
                              right: 0,
                              child: Opacity(
                                opacity: 0.05,
                                child: Container(
                                  height: 45/720*screenHeight,
                                  decoration: BoxDecoration(
                                    gradient: LinearGradient(
                                      begin: Alignment(-0.011, 0.507),
                                      end: Alignment(1.031, 0.483),
                                      stops: [
                                        0,
                                        1,
                                      ],
                                      colors: [
                                        Color.fromARGB(255, 168, 174, 35),
                                        Color.fromARGB(255, 201, 110, 216),
                                      ],
                                    ),
                                    borderRadius: BorderRadius.all(Radius.circular(5)),
                                  ),
                                  child: Container(),
                                ),
                              ),
                            ),
                            Positioned(
                              left: 25/360*screenWidth,
                              top: 6/720*screenHeight,
                              right: 28/360*screenWidth,
                              child: Container(
                                height: 27/720*screenHeight,
                                child: Row(
                                  crossAxisAlignment: CrossAxisAlignment.stretch,
                                  children: [
                                    Align(
                                      alignment: Alignment.topLeft,
                                      child: Container(
                                        margin: EdgeInsets.only(top: 8/720*screenHeight),
                                        child: Text(
                                          CalculationSummaryScreenStrings.Text_HotStreak,
                                          style: TextStyle(
                                            color: Color.fromARGB(255, 0, 3, 44),
                                            fontSize: 14/678*screenHeight,
                                            fontFamily: "IBMPlexSans",
                                          ),
                                          textAlign: TextAlign.left,
                                        ),
                                      ),
                                    ),
                                    Spacer(),
                                    Align(
                                      alignment: Alignment.topLeft,
                                      child: Text(
                                        CalculationSummaryScreenStrings.Text_ValueHotStreak,
                                        style: TextStyle(
                                          color: Color.fromARGB(255, 0, 3, 44),
                                          fontSize: 14/720*screenHeight,
                                          fontFamily: "IBMPlexSans",
                                          fontWeight: FontWeight.w500,
                                        ),
                                        textAlign: TextAlign.left,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      Container(
                        height: 45/720*screenHeight,
                        margin: EdgeInsets.only(top: 8/720*screenHeight),
                        child: Stack(
                          alignment: Alignment.center,
                          children: [
                            Positioned(
                              left: 0,
                              top: 0,
                              right: 0,
                              child: Opacity(
                                opacity: 0.05,
                                child: Container(
                                  height: 45/720*screenHeight,
                                  decoration: BoxDecoration(
                                    gradient: LinearGradient(
                                      begin: Alignment(-0.011, 0.507),
                                      end: Alignment(1.031, 0.483),
                                      stops: [
                                        0,
                                        1,
                                      ],
                                      colors: [
                                        Color.fromARGB(255, 168, 174, 35),
                                        Color.fromARGB(255, 201, 110, 216),
                                      ],
                                    ),
                                    borderRadius: BorderRadius.all(Radius.circular(5)),
                                  ),
                                  child: Container(),
                                ),
                              ),
                            ),
                            Positioned(
                              left: 25/360*screenWidth,
                              top: 6/720*screenHeight,
                              right: 28/360*screenWidth,
                              child: Container(
                                height: 29/720*screenHeight,
                                child: Row(
                                  crossAxisAlignment: CrossAxisAlignment.stretch,
                                  children: [
                                    Align(
                                      alignment: Alignment.topLeft,
                                      child: Container(
                                        margin: EdgeInsets.only(top: 9/720*screenHeight),
                                        child: Text(
                                          CalculationSummaryScreenStrings.Text_BlazingStreak,
                                          style: TextStyle(
                                            color: Color.fromARGB(255, 0, 3, 44),
                                            fontSize: 14/678*screenHeight,
                                            fontFamily: "IBMPlexSans",
                                          ),
                                          textAlign: TextAlign.left,
                                        ),
                                      ),
                                    ),
                                    Spacer(),
                                    Align(
                                      alignment: Alignment.topLeft,
                                      child: Container(
                                        height: 27/720*screenHeight,
                                        child: Text(
                                            CalculationSummaryScreenStrings.Text_ValueBlazingStreak,
                                          style: TextStyle(
                                            color: Color.fromARGB(255, 0, 3, 44),
                                            fontSize: 14/678*screenHeight,
                                            fontFamily: "IBMPlexSans",
                                            fontWeight: FontWeight.w500,
                                          ),
                                          textAlign: TextAlign.left,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              InkWell(
                child: Container(
                  height: 40/720*screenHeight,
                  margin: EdgeInsets.only(left: 45/360*screenWidth, top: 40/720*screenHeight, right: 45/360*screenWidth, ),
                  decoration: BoxDecoration(
                    gradient: LinearGradient(
                      begin: Alignment(-0.011, 0.494),
                      end: Alignment(1.031, 0.516),
                      stops: [
                        0,
                        1,
                      ],
                      colors: [
                        Color.fromARGB(255, 51, 128, 204),
                        Color.fromARGB(255, 137, 110, 216),
                      ],
                    ),
                    borderRadius: BorderRadius.all(Radius.circular(2)),
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        CalculationSummaryScreenStrings.Button_Complete,
                        style: TextStyle(
                          color: Color.fromARGB(255, 255, 255, 255),
                          fontSize: 14,
                          fontFamily: "IBMPlexSans",
                          fontWeight: FontWeight.w500,
                        ),
                        textAlign: TextAlign.center,
                      ),
                    ],
                  ),
                ),
                onTap: (){
                  int count = 0;
                  Navigator.popUntil(context, (route) {
                    return count++ == 2;
                  });
                  },
              ),
            ],
          ),




        ),

      ),
    );
  }
}
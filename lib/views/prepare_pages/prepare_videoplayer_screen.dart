//import 'package:chewie/chewie.dart';
//import 'package:flutter/material.dart';
//import 'package:video_player/video_player.dart';
//
///**Chewie Video Player**/
//
//class ChewieListItemHtml extends StatefulWidget {
//  // This will contain the URL/asset path which we want to play
//  final VideoPlayerController videoPlayerController;
//  final bool looping;
//
//  ChewieListItemHtml({
//    @required this.videoPlayerController,
//    this.looping,
//    Key key,
//  }) : super(key: key);
//
//  @override
//  _ChewieListItemHtmlState createState() => _ChewieListItemHtmlState();
//}
//
//class _ChewieListItemHtmlState extends State<ChewieListItemHtml> {
//  ChewieController _chewieController;
//
//  @override
//  void initState() {
//    super.initState();
//    // Wrapper on top of the videoPlayerController
//    _chewieController = ChewieController(
//      videoPlayerController: widget.videoPlayerController,
//      aspectRatio: 14 / 9,
//      // Prepare the video to be played and display the first frame
//      autoInitialize: true,
//      looping: widget.looping,
//      // Errors can occur for example when trying to play a video
//      // from a non-existent URL
//      errorBuilder: (context, errorMessage) {
//        return Center(
//          child: Text(
//            errorMessage,
//            style: TextStyle(color: Colors.white),
//          ),
//        );
//      },
//    );
//  }
//
//  @override
//  Widget build(BuildContext context) {
//    return Container(
//      child: Chewie(
//        controller: _chewieController,
//      ),
//    );
//  }
//
//  @override
//  void dispose() {
//    super.dispose();
//    // IMPORTANT to dispose of all the used resources
//    widget.videoPlayerController.dispose();
//    _chewieController.dispose();
//  }
//}
//
//class VideoPlayerApp extends StatelessWidget {
//  @override
//  Widget build(BuildContext context) {
//    return Scaffold(
//        appBar: AppBar(
//          brightness: Brightness.light,
//          elevation: 0.0,
//          centerTitle: false,
//          titleSpacing: 0.0,
//          backgroundColor: Colors.transparent,
//          title: Text(
//            '',
//            style: const TextStyle(
//                color: Colors.black,
//                fontWeight: FontWeight.w500,
//                fontFamily: "IBMPlexSans",
//                fontStyle: FontStyle.normal,
//                fontSize: 16.0),
//          ),
//          leading: IconButton(
//            icon: Icon(
//              Icons.arrow_back,
//              color: Colors.black,
//            ),
//            onPressed: () => Navigator.pop(context, false),
//          ),
//        ),
//        body: Container(
//            child:  Center(
//              child:  ChewieListItemHtml(
//                videoPlayerController: VideoPlayerController.network(
//                  'https://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4',
//                ),
//              ),
//            ))
//    );
//  }
//}

/**Chewie Video Player**/



//import 'package:flutter/material.dart';
////import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';
//import 'package:neeko/neeko.dart';
//import 'package:flutter/services.dart';
//
//class VideoPlayerScreen extends StatefulWidget {
//  @override
//  VideoPlayerScreenState createState() => VideoPlayerScreenState();
//}
//
//class VideoPlayerScreenState extends State<VideoPlayerScreen> {
//  //final flutterWebviewPlugin = new FlutterWebviewPlugin();
//  static const String beeUri =
//      'https://dfhvzpvwnedye.cloudfront.net/ims-ugvideos/myIMS - For Channel/CMAT_Analysis_2019_HQ/output.mdpmaster.m3u8';
//
//  final VideoControllerWrapper videoControllerWrapper = VideoControllerWrapper(
//      DataSource.network(
//          'https://dfhvzpvwnedye.cloudfront.net/ims-ugvideos/myIMS - For Channel/CMAT_Analysis_2019_HQ/output.mdpmaster.m3u8'));
//  @override
//  void initState() {
//    super.initState();
//    SystemChrome.setEnabledSystemUIOverlays([SystemUiOverlay.top]);
//  }
//
//  @override
//  void dispose() {
//    SystemChrome.restoreSystemUIOverlays();
//    super.dispose();
//  }
//  @override
//  Widget build(BuildContext context) {
//    final screenHeight = MediaQuery.of(context).size.height;
//    final screenWidth = MediaQuery.of(context).size.width;
//    return Scaffold(
//      backgroundColor: Colors.black,
//      appBar: AppBar(title: const Text(''),
//        backgroundColor: Colors.black,
//      ),
//      body: Center(
//        child: NeekoPlayerWidget(
//          onSkipPrevious: () {
//            print("skip");
//            videoControllerWrapper.prepareDataSource(DataSource.network(
//                "https://dfhvzpvwnedye.cloudfront.net/ims-ugvideos/myIMS - For Channel/CMAT_Analysis_2019_HQ/output.mdpmaster.m3u8",
//                displayName: "This house is not for sale"));
//          },
//          onSkipNext: () {
//            videoControllerWrapper.prepareDataSource(DataSource.network(
//                'https://dfhvzpvwnedye.cloudfront.net/ims-ugvideos/myIMS - For Channel/CMAT_Analysis_2019_HQ/output.mdpmaster.m3u8',
//                displayName: "displayName"));
//          },
//          videoControllerWrapper: videoControllerWrapper,
//          actions: <Widget>[
//            IconButton(
//                icon: Icon(
//                  Icons.share,
//                  color: Colors.white,
//                ),
//                onPressed: () {
//                  print("share");
//                })
//          ],
//        ),
//      ),
//    );
//  }
//}


/**WEBVIEW PLAYER**/

// Copyright 2018 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:imsindia/utils/colors.dart';
import 'package:webview_flutter/webview_flutter.dart';
import 'package:imsindia/utils/emaximiser_svg_icons.dart';

//void main() => runApp(
//      MaterialApp(
//        home: WebViewExample(),
//        theme: ThemeData(
//          primarySwatch: Colors.red,
//        ),
//      ),
//    );
String userID;
class WebViewExample1 extends StatefulWidget {
  String videoid;
  String userid;
  WebViewExample1(this.videoid,this.userid);

  @override
  _WebViewExampleState createState() => _WebViewExampleState();
}

class _WebViewExampleState extends State<WebViewExample1> with WidgetsBindingObserver{
  final Completer<WebViewController> _controller =
  Completer<WebViewController>();
  WebViewController webViewController;
  String id;
  var urll;

  String get player {
    String _player = '''
    <!DOCTYPE html>
    <html>
    <head>
        <style>
            html,
            body {
                height: 100%;
                width: 100%;
                margin: 0;
                padding: 0;
                background-color: #ffffff;
                overflow: hidden;
                position: fixed;
            }
        </style>
        <meta name='viewport' content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no'>
    </head>
    <body>
     <div style="padding:56.25% 0 0 0;position:relative;">
     <iframe src="https://player.vimeo.com/video/366417284" style="position:absolute;top:height / 2;left:0;width:100%;height:100%;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
     </div><script src="https://player.vimeo.com/api/player.js"></script>
    <!- Your Vimeo SDK player script goes here ->
  </script>
    </body>
    </html>
    ''';
    return 'data:text/html;base64,${base64Encode(const Utf8Encoder().convert(_player))}';
  }

  
  @override
  void initState() {
    //To enable landscape in this particular screen alone(Android and iOS)
    SystemChrome.setPreferredOrientations([
    DeviceOrientation.landscapeRight,
    DeviceOrientation.landscapeLeft,
    DeviceOrientation.portraitUp,
  ]);
    // TODO: implement initState
    print("***************************************123");
    print(widget.videoid);
    print(widget.userid);
    super.initState();
    WidgetsBinding.instance.addObserver(this);
  }

  @override
  void dispose() {
    //To reset the screen orientation 
    SystemChrome.setPreferredOrientations([
    DeviceOrientation.portraitUp,
    ]);
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
    _controller.isCompleted;
  }

  @override
  void deactivate() {
    //To reset the screen orientation 
    SystemChrome.setPreferredOrientations([
    DeviceOrientation.portraitUp,
    ]);
    super.deactivate();
    webViewController?.reload();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    if (state == AppLifecycleState.paused) {
      webViewController?.reload();
    }
  }

  @override
  Widget build(BuildContext context) {
    final screenHeight = MediaQuery.of(context).size.height;
    final screenWidth = MediaQuery.of(context).size.width;

    return Scaffold(
      appBar: AppBar(
        brightness: Brightness.light,
        elevation: 0.0,
        centerTitle: false,
        titleSpacing: 0.0,
        backgroundColor: Colors.white,
        title: Text(
         '',
          style: TextStyle(
              color: NeutralColors.dark_navy_blue,
              fontWeight: FontWeight.w500,
              fontFamily: "IBMPlexSans",
              fontStyle: FontStyle.normal,
              fontSize: 16 / 360 * screenWidth),
        ),
        leading: IconButton(
          icon: getSvgIcon.backSvgIcon,
          onPressed: () => Navigator.pop(context, false),
        ),
      ),
      body: WebView(
        
  initialUrl: widget.videoid != null ? "https://ims.quizky.com/vimeo-player/"+widget.videoid+"/ims/"+widget.userid:"https://ims.quizky.com/vimeo-player/d6562e61116a55a298105a73c50ec967/ims/342934 ",
  
  onWebViewCreated: (controller) {
    //webView = controller;
    webViewController = controller;
     _controller.complete(webViewController);
  },
  javascriptMode: JavascriptMode.unrestricted,

   ),

    );

  }
  

}

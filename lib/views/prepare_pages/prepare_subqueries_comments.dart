import 'dart:async';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:imsindia/components/custom_dropdown_channel.dart';
import 'package:imsindia/routers/routes.dart';
import 'package:imsindia/utils/colors.dart';
import 'package:imsindia/views/Arun/ReadMoreText.dart';
import 'package:imsindia/resources/strings/prepare.dart';
import 'package:imsindia/views/prepare_pages/Prepare_Comments.dart';
import 'package:imsindia/views/prepare_pages/prepare_listofreplies.dart';
import 'package:flutter_document_picker/flutter_document_picker.dart';
import 'package:imsindia/utils/screen_util.dart';



bool value = false;
GlobalKey _keyRed = GlobalKey();
GlobalKey _keyRed1 = GlobalKey();
final GlobalKey _menuKey = new GlobalKey();
List<dynamic> like = [];
List titles = [
  'Date',
  'Upvotes',
];

List<String> list = ["123 Votes", "124 Votes"];
final List listOfComments = ['1','2'];
var time_ago;

class CommentsHtml extends StatefulWidget {
  FocusNode focusNode = new FocusNode();
  CommentsHtml();

  @override
  CommentsHtmlState createState() => CommentsHtmlState();
}
class CommentsHtmlState extends State<CommentsHtml> {
  bool sortbydate = false;
  bool sortbyvote=false;
  bool sortby = false;
  int i=123;
  List<CommentsOptionsHtml> list_comments=[];
  List<CommentsOptionsHtml> sortedlist_comments=[];
  List<CommentsOptionsHtml> list_Reply=[];
  bool timeago = false;
  String timeAgo;
  String timeAgoSinceDate(String dateString, {bool numericDates = true}) {
    print("********************************************Coming" +dateString.toString());

    DateTime date = DateTime.parse(dateString);

    final date2 = DateTime.now();

    final difference = date2.difference(date);

    if ((difference.inDays / 365).floor() >= 2) {
      return '${(difference.inDays / 365).floor()} years ago';
    } else if ((difference.inDays / 365).floor() >= 1) {
      return (numericDates) ? '1 year ago' : 'Last year';
    } else if ((difference.inDays / 30).floor() >= 2) {
      print("********************************************monthsmonthsmonthsmonthsmonths" +dateString.toString());

      return '${(difference.inDays / 365).floor()} months ago';
    } else if ((difference.inDays / 30).floor() >= 1) {
      return (numericDates) ? '1 month ago' : 'Last month';
    } else if ((difference.inDays / 7).floor() >= 2) {
      return '${(difference.inDays / 7).floor()} weeks ago';
    } else if ((difference.inDays / 7).floor() >= 1) {
      return (numericDates) ? '1 week ago' : 'Last week';
    } else if (difference.inDays >= 2) {
      return '${difference.inDays} days ago';
    } else if (difference.inDays >= 1) {
      return (numericDates) ? '1 day ago' : 'Yesterday';
    } else if (difference.inHours >= 2) {
      return '${difference.inHours} hours ago';
    } else if (difference.inHours >= 1) {
      return (numericDates) ? '1 hour ago' : 'An hour ago';
    } else if (difference.inMinutes >= 2) {
      return '${difference.inMinutes} minutes ago';
    } else if (difference.inMinutes >= 1) {
      setState(() {
        timeAgo ='Just now';
        print('timeAgotimeAgotimeAgotimeAgo'+timeAgo);

      });
      return (numericDates) ? '1 minute ago' : 'A minute ago';
    } else if (difference.inSeconds >= 3) {
      setState(() {
        timeAgo ='${difference.inSeconds} seconds ago';
        print('timeAgotimeAgotimeAgotimeAgo'+timeAgo);
      });
      return '${difference.inSeconds} seconds ago';
    } else {
      setState(() {
        timeAgo ='Just now';
        print('timeAgotimeAgotimeAgotimeAgo'+timeAgo);

      });
      return 'Just now';
    }
  }



  FocusNode _focusNode = new FocusNode();
  bool flag_sortby = false;
  String post_value = "";
  bool upvoted = false;
  bool _pickFileInProgress = false;
  bool isReply = false;
  String _path = '-';
  bool uploaded = false;
  final TextEditingController _sample = new TextEditingController();

  bool _pickFileInProgress_reply = false;
  String _path_reply = '-';
  bool uploaded_reply = false;


  bool _iosPublicDataUTI = true;
  bool _checkByCustomExtension = false;
  bool _checkByMimeType = false;
  TextEditingController post_a_query_Controller = TextEditingController();
  TextEditingController reply_Controller = TextEditingController();
  TextEditingController path_Controller = TextEditingController();
  TextEditingController pathreply_Controller = TextEditingController();
  OverlayEntry _overlayEntry;
  int selectedValue;
  int liked = 0;
  String selectedString = "Sort By";
  bool isClicked = false;

//  static var option1 =
//  new CommentsOptionsHtml.Comment("Arpita","2min ago",
//      "How do you create a direct mail advertising campaign that gets results? "
//          " The following tips on creating a direct mail advertising campaign "
//          "have been street-tested and will bring you huge returns in a short "
//          "period of time.",111);
//  static var option2 =
//  new CommentsOptionsHtml.Comment("Anjali","10min ago",
//      "How do you create a direct mail advertising campaign that gets results? "
//          " The following tips on creating a direct mail advertising campaign "
//          "have been street-tested and will bring you huge returns in a short "
//          "period of time.",111);
//  List<CommentsOptionsHtml> list_comments = [option1,option2];



  OverlayEntry _createOverlayEntry() {
    final screenHeight = MediaQuery.of(context).size.height;
    final screenWidth = MediaQuery.of(context).size.width;
    RenderBox renderBox = context.findRenderObject();
    setState(() {
      isClicked = true;
    });
    var size = _getSizes();

    var offset = _getPositions();

    return OverlayEntry(
        builder: (context) => Positioned(
          top: offset.dy + size,
          left:
          offset.dx - (10 / 360) * screenWidth,
          child: Container(
            // padding: EdgeInsets.only(top: 5),
            width: (95 / 360) * screenWidth,
            height: (81 / 720) * screenHeight,
            child: Scaffold(
              body: Container(
                decoration: BoxDecoration(
                    color: Colors.white,
                    boxShadow: [
                      BoxShadow(
                        blurRadius: 10,
                        color: Color(0xffeaeaea),
                        //offset: Offset.lerp(Offset(10.0,-10.0), Offset(10.0,10.0), 1),
                        // offset: Offset(0,10.0),
                        // color: Colors.orange,
                      ),
                    ],
                    border: new Border.all(
                        color: Colors.white.withOpacity(0.1), width: 1.0)),
                margin: EdgeInsets.only(top: 0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    new Expanded(
                        child: ListView.builder(
                          padding: EdgeInsets.only(top: 0, bottom: 0),
                          itemBuilder: (BuildContext context, int index) {
                            return InkWell(
                              onTap: () {},
                              child: Container(
                                //color: Colors.white,
                                child: Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: InkWell(
                                    onTap: () {
                                      _overlayEntry.remove();
                                      setState(() {
                                        isClicked = false;
                                        selectedValue = index;
                                        selectedString = titles[index];
                                        print(selectedValue);
                                        print(selectedString.length);
                                      });
                                    },
                                    child: new Text(titles[index],
                                        // textAlign: TextAlign.left,
                                        style: new TextStyle(
                                          fontSize: (14 / 360) * screenWidth,
                                          fontFamily: "IBMPlexSansMedium",
                                          fontWeight: FontWeight.w500,
                                          color: NeutralColors.gunmetal,
                                        )),
                                  ),
                                ),
                              ),
                            );
                          },
                          itemCount: titles.length,
                        )),
                  ],
                ),
              ),
            ),
          ),
        ));
  }
  bool clicked = false;
  bool isSelected = false;

  final GlobalKey _menuKey = new GlobalKey();
  bool showReplies_Active = false;
  _pickDocument() async {
    print("callinnggggggggggggggggggggggggggggg");
    String result;
    try {
      setState(() {
        _path = '-';
        _pickFileInProgress = true;
      });
      final _utiController = TextEditingController(
        text: 'com.sidlatau.example.mwfbak',
      );

      final _extensionController = TextEditingController(
        text: 'mwfbak',
      );

      final _mimeTypeController = TextEditingController(
        text: 'application/pdf image/png',
      );
      FlutterDocumentPickerParams params = FlutterDocumentPickerParams(
        allowedFileExtensions: _checkByCustomExtension
            ? _extensionController.text
            .split(' ')
            .where((x) => x.isNotEmpty)
            .toList()
            : null,
        allowedUtiTypes: _iosPublicDataUTI
            ? null
            : _utiController.text
            .split(' ')
            .where((x) => x.isNotEmpty)
            .toList(),
        allowedMimeTypes: _checkByMimeType
            ? _mimeTypeController.text
            .split(' ')
            .where((x) => x.isNotEmpty)
            .toList()
            : null,
      );

      result = await FlutterDocumentPicker.openDocument(params: params);
    } catch (e) {
      print(e);
      result = 'Error: $e';
    } finally {
      setState(() {
        _pickFileInProgress = false;
      });
    }

    setState(() {
      uploaded = true;
      _path = result;
      print(_path.length.toString());
      path_Controller.text = _path;
    });
  }
  _pickDocument_reply() async {
    print("callinnggggggggggggggggggggggggggggg");
    String result;
    try {
      setState(() {
        _path_reply = '-';
        _pickFileInProgress_reply = true;
      });
      final _utiController = TextEditingController(
        text: 'com.sidlatau.example.mwfbak',
      );

      final _extensionController = TextEditingController(
        text: 'mwfbak',
      );

      final _mimeTypeController = TextEditingController(
        text: 'application/pdf image/png',
      );
      FlutterDocumentPickerParams params = FlutterDocumentPickerParams(
        allowedFileExtensions: _checkByCustomExtension
            ? _extensionController.text
            .split(' ')
            .where((x) => x.isNotEmpty)
            .toList()
            : null,
        allowedUtiTypes: _iosPublicDataUTI
            ? null
            : _utiController.text
            .split(' ')
            .where((x) => x.isNotEmpty)
            .toList(),
        allowedMimeTypes: _checkByMimeType
            ? _mimeTypeController.text
            .split(' ')
            .where((x) => x.isNotEmpty)
            .toList()
            : null,
      );

      result = await FlutterDocumentPicker.openDocument(params: params);
    } catch (e) {
      print(e);
      result = 'Error: $e';
    } finally {
      setState(() {
        _pickFileInProgress_reply = false;
      });
    }

    setState(() {
      uploaded_reply = true;
      _path_reply = result;
      print(_path_reply.length.toString());
      pathreply_Controller.text = _path_reply;
    });
  }
  int _selectedIndexReply;
  int _selectedIndexShowReply;


  _onSelected_reply(int indexReply) {
    setState(() => _selectedIndexReply = indexReply);
  }
  _onSelected_showreply(indexShowreplies){
    setState(() {
      _selectedIndexShowReply = indexShowreplies;
    });
  }

  @override
  void dispose() {
    // TODO: implement dispose
    _overlayEntry.remove();
    super.dispose();
  }
  @override
  void initState() {
    // KeyboardVisibilityNotification().addNewListener(
    //   onHide: () {
    //     print(_focusNode.hasFocus);
    //     _focusNode.unfocus();
    //   },
    // );
    print("=comment"+list_comments.toString());
    const oneSecond = const Duration(seconds: 25);
    new Timer.periodic(oneSecond, (Timer t) => setState((){
      print("========================updating");
      timeago = true;
    }));
    // TODO: implement initState
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    final screenHeight = MediaQuery.of(context).size.height;
    final screenWidth = MediaQuery.of(context).size.width;
    return InkWell(
      onTap: (){
        _overlayEntry.remove();
      },
      child: Container(
        color: Colors.transparent,
        margin: EdgeInsets.only(bottom: 20/720*screenHeight),
        child: Column(
          children: <Widget>[
            /**1 colum**/
            Container(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  GestureDetector(
                    child: Container(
                      height: (23 / 720) * screenHeight,
                      margin: EdgeInsets.only(
                          top: (19 / 720) * screenHeight,
                          left: (17 / 360) * screenWidth),
                      child: Text(
                        SubQueriesCommentsScreenStrings.Text_TotalComments,
                        style: TextStyle(
                          color: NeutralColors.dark_navy_blue,
                          fontWeight: FontWeight.w500,
                          fontFamily: "IBMPlexSans",
                          fontStyle: FontStyle.normal,
                          fontSize: (14 / 360) * screenWidth,
                        ),
                        textAlign: TextAlign.left,
                      ),
                    ),
                    onTap: (){_overlayEntry.remove();},
                  ),
//                  Container(
//                    padding: EdgeInsets.all(10),
//                    child: Column(
//                      children: [
//                        InkWell(
//                          child: Container(
//                            margin: EdgeInsets.only(right: (10 / 360) * screenWidth, top: (19 /720) * screenHeight,
//                            ),
//                            height: 23,
//                            child: Row(
//                              mainAxisAlignment: MainAxisAlignment.end,
//                              crossAxisAlignment: CrossAxisAlignment.end,
//                              children: <Widget>[
//                                Center(
//                                  child: Container(
//                                    child: Padding(
//                                      padding: EdgeInsets.only(
//                                          top: (0 /
//                                              720) *
//                                              screenHeight,
//                                          bottom: (0 /
//                                              720) *
//                                              screenHeight,
//                                          left: (0 /
//                                              360) *
//                                              screenWidth,
//                                          right: (0 /
//                                              360) *
//                                              screenWidth),
//                                      child: Text(
//                                        selectedString,
//                                        key: _keyRed,
//                                        style: TextStyle(
//                                            fontSize: (14/720) * screenHeight,
//                                            fontFamily: "IBMPlexSans",
//                                            fontWeight: FontWeight.w500,
//                                            color: SemanticColors.blueGrey),
//                                      ),
//                                    ),
//                                  ),
//                                ),
//                                Container(
//                                  margin: EdgeInsets.only(
//                                      top: (1/720) *screenHeight),
//                                  child: isClicked ? Icon(Icons.keyboard_arrow_up, color: SemanticColors.blueGrey, size: 25.0,)
//                                      : Icon(Icons.keyboard_arrow_down, color: SemanticColors.blueGrey, size: 25.0,),
//                                ),
//                              ],
//                            ),
//
//                          ),
//                          onTap: (){
//                            print("======click");
//                            this._overlayEntry = this._createOverlayEntry();
//                            Overlay.of(context).insert(this._overlayEntry);
//                          },
//                        ),
//                      ],
//                    ),
//                  ),
                  Container(
                    padding: EdgeInsets.all(0.0),
                    height: 40/720*screenHeight,
                    margin: EdgeInsets.only(right: (20/screenWidth)*Constant.defaultScreenWidth,top: (19/720)*screenHeight),
                    child: DropDownFormField(
                      getImmediateSuggestions: true,
                      // autoFlipDirection: true,
                      direction: AxisDirection.down,
                      textFieldConfiguration: TextFieldConfiguration(
                        controller: _sample,
                        label: selectedString,
                      ),
                      // suggestionsBoxController:sugg,
                      suggestionsBoxDecoration: SuggestionsBoxDecoration(
                          borderRadius: new BorderRadius.circular(5.0),
                          color: Colors.white),
                      suggestionsCallback: (pattern) {
                        return SampleService.getSuggestions(pattern);
                      },
                      itemBuilder: (context, suggestion) {

                        return Container(
                          color:NeutralColors.pureWhite,
                          child: Padding(
                            padding:  EdgeInsets.only(top: (10 / Constant.defaultScreenHeight) * screenHeight,
                                bottom: (10 / Constant.defaultScreenHeight) * screenHeight,
                                left:(15 / Constant.defaultScreenWidth) * screenWidth ),
                            child: Text(
                              suggestion,
                              style: TextStyle(
                                fontFamily: "IBMPlexSans",
                                fontWeight: FontWeight.w500,
                                fontSize: (14 / Constant.defaultScreenWidth) * screenWidth,
                                color: NeutralColors.gunmetal,
                                textBaseline: TextBaseline.alphabetic,
                                letterSpacing: 0.0,
                                inherit: false,
                              ),
                            //  textAlign: TextAlign.center,
                            ),
                          ),
                        );
                      },
                      hideOnLoading: true,
                      debounceDuration: Duration(milliseconds: 100),
                      transitionBuilder: (context, suggestionsBox, controller) {
                        return suggestionsBox;
                      },
                      onSuggestionSelected: (suggestion) {
                        sortby = true;
                        _sample.text = suggestion;
                        if(suggestion == "Date"){
                          sortbydate = true;
                          sortbyvote =false;
                        }else{
                          sortbyvote=true;
                          sortbydate = false;
                        }
                        print("==================status"+sortby.toString()+sortbyvote.toString()+sortbydate.toString());
                      },
                      onSaved: (value) => {print("something")},
                    ),

                  ),
                ],
              ),
            ),
            /**2 colum**/
            Container(
              child: Row(
                children: <Widget>[
                  /**Circular Avatar**/
                    Container(
                      height:30/720*screenHeight,
                      width: 30/360*screenWidth,
                      margin: EdgeInsets.only(top: (17/964)*screenHeight,left: (20/360)*screenWidth),
                      decoration: new BoxDecoration(
                          shape: BoxShape.circle,
                          image: new DecorationImage(
                              fit: BoxFit.cover,
                              image: AssetImage(
                                  'assets/images/oval-2.png'))),
                    ),
                  /**Post a Query**/
                  Container(
                    height: (40/720)*screenHeight,
                    width: (280/360) * screenWidth,
                    margin: EdgeInsets.only(left: (10/360)*screenWidth,top: (12/720)*screenHeight),
                    decoration: BoxDecoration(
                      border: Border.all(color: AccentColors.iceBlue),
                      color:Colors.transparent,
                      borderRadius: BorderRadius.all(Radius.circular(5)),
                    ),
                    child: Row(
                        mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Container(
                          height: (18/720)*screenHeight,
                          width: (200/360) * screenWidth,
                          margin: EdgeInsets.only(left: (13/360)*screenWidth,top: (10/720)*screenHeight,bottom: (10/720)*screenHeight),
                          child: GestureDetector(
                            onTap: (){_overlayEntry.remove();},
                            child: TextFormField(
                              // controller: uploaded ? new TextEditingController(text: '$_path'): post_a_query_Controller,
                              controller: uploaded ? path_Controller : post_a_query_Controller,
                              style: TextStyle(
                                color: NeutralColors.gunmetal,
                                fontWeight: FontWeight.normal,
                                fontFamily: "IBMPlexSans",
                                fontStyle: FontStyle.normal,
                                fontSize: (14/720)*screenHeight,
                              ),
                              textInputAction: TextInputAction.done,
                              decoration: new InputDecoration.collapsed
                                (
                                hintText:  SubQueriesCommentsScreenStrings.Text_PostAquery,
                                hintStyle: TextStyle (
                                  color: AccentColors.blueGrey,
                                  fontWeight: FontWeight.w400,
                                  fontFamily: "IBMPlexSans",
                                  fontStyle: FontStyle.normal,
                                  fontSize: (14/720)*screenHeight,
                                ),
                              ),
                              onFieldSubmitted: (val){
                                setState(() {
                                  var timeofreply = DateTime.now();
                                  CommentsOptionsHtml option =
                                  new CommentsOptionsHtml.Comment("Sushree",timeofreply.toString(),val,i++);
                                  list_comments.add(option);
                                  print(list_comments);
                                  post_a_query_Controller.clear();
                                });
                                return;
                              },
                            ),
                          ),
                        ),
                        GestureDetector(
                          onTap: _pickFileInProgress ? null : _pickDocument,
                          child: Container(
                            height: (18 /720) * screenHeight,
                            width: (19 / 360) * screenWidth,
                            margin: EdgeInsets.only(left: 29/360*screenWidth,top: (8/720)*screenHeight,bottom: (10/720)*screenHeight),
                            child: Image.asset(
                              "assets/images/clip-4.png",
                              fit: BoxFit.fill,
                            ),
//                            child:Icon(
//                              Icons.attach_file,
//                              color: NeutralColors.purpley,
//                              size: 17/360*screenWidth,
//                            )
                          ),
                        )
                      ],
                    ),
                  ),
                ],
              ),
            ),
            /**3rd colum**/
            Container(
              margin: EdgeInsets.only(top: (16/764)*screenHeight),
              child: getCommentsSection(list_comments,screenHeight,screenWidth),
            ),
          ],
        ),

      ),
    );

  }

  Widget getCommentsSection(List<CommentsOptionsHtml> list_comments,screenHeight,screenWidth)
  {
    List<Widget> list = new List<Widget>();
    setState(() {
      print("==================status"+sortby.toString()+sortbyvote.toString()+sortbydate.toString());
      if(sortby == true){
        if(sortbyvote == true){
          list_comments.sort((b,a)=>a.numberofvotes.compareTo(b.numberofvotes));
        }else{
          list_comments.sort((a,b)=>a.durationofcomment.compareTo(b.durationofcomment));
        }
        sortby = false;
      }
    });
      if (list_comments.isEmpty || list_comments.length == null) {
        list.add(
            Container(
              height: 40 / 720 * screenHeight,
              margin: EdgeInsets.only(top: 24.5 / 720 * screenHeight),
              child: Text("No Comments", style: TextStyle(color: Colors.blue),),
            ));
      } else {
        for (var i = 0; i < list_comments.length; i++) {
          list.add(
            Container(
              margin: EdgeInsets.only(left: (20 / 360) * screenWidth),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  /**profile image and name colum**/
                  Container(
                    height: 40 / 720 * screenHeight,
                    margin: EdgeInsets.only(top: 24.5 / 720 * screenHeight),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        /**Profile pic**/
                        Container(child: Row(
                          children: <Widget>[
                            Container(
                              height: (40 / 720) *
                                  screenHeight,
                              width:
                              (40 / 360) *
                                  screenWidth,
                              margin: EdgeInsets.only(
                                  top: (0 / 720) * screenHeight),
                              decoration: new BoxDecoration(
                                  shape: BoxShape.circle,
                                  image: new DecorationImage(
                                      fit: BoxFit.cover,
                                      image: AssetImage(
                                          'assets/images/oval-2.png'))),
                            ),
                            Container(
                              height: 40 / 720 * screenHeight,
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment
                                    .spaceBetween,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Container(
                                    margin: EdgeInsets.only(
                                        left: (12 / 360) * screenWidth),
                                    child: Text(
                                      list_comments[i].nameofperson,
                                      // SubQueriesCommentsScreenStrings.Text_UserName,
                                      style: TextStyle(
                                        color: NeutralColors.dark_navy_blue,
                                        fontWeight: FontWeight.w500,
                                        fontFamily: "IBMPlexSans",
                                        fontStyle: FontStyle.normal,
                                        fontSize: (14 / 720) * screenHeight,
                                      ),
                                      textAlign: TextAlign.start,
                                    ),
                                  ),
                                  Container(
                                    margin: EdgeInsets.only(
                                        top: (5 / 720) * screenHeight,
                                        left: 12 / 360 * screenWidth),
                                    child: Text(
                                      timeago ? timeAgoSinceDate(
                                          list_comments[i].durationofcomment)
                                          .toString() : timeAgoSinceDate(
                                          list_comments[i].durationofcomment)
                                          .toString(),
                                      // SubQueriesCommentsScreenStrings.Text_MsgdTime,
                                      style: TextStyle(
                                        color: NeutralColors.blue_grey,
                                        fontWeight: FontWeight.w500,
                                        fontFamily: "IBMPlexSans",
                                        fontStyle: FontStyle.normal,
                                        fontSize: (12 / 720) * screenHeight,
                                      ),
                                      textAlign: TextAlign.start,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),),
                        Container(
                          height: 40 / 720 * screenHeight,
                          margin: EdgeInsets.only(top: 0.0, right: 0.0,),
                          child: GestureDetector(
                            onTap: () {
                              // This is a hack because _PopupMenuButtonState is private.
                              dynamic state = _menuKey.currentState;
                              state.showButtonMenu();
                            },
                            child: Container(
                              child: PopupMenuButton(
                                  icon: Icon(Icons.more_vert,
                                      color: NeutralColors.blue_grey),
                                  padding: EdgeInsets.all(10),
                                  // key: _menuKey,
                                  itemBuilder: (_) =>
                                  <PopupMenuItem<String>>[
                                    new PopupMenuItem<String>(
                                        child: Container(
                                          alignment: Alignment.center,
                                          child: const Text(
                                            'Report',
                                            style: TextStyle(
                                              color: NeutralColors.gunmetal,
                                              fontFamily: "IBMPlexSans",
                                              fontWeight: FontWeight.w500,
                                              fontSize: 14,
                                            ),
                                            textAlign: TextAlign.center,
                                          ),
                                        ),
                                        value: 'Report'),
                                  ],
                                  onSelected: (_) {}),
                            ),
                          ),
                        ),


                      ],

                    ),
                  ),
                  /**Extended Paragraph colum**/

                  Container(
                    // height:(72/936)*screenHeight,
                    width: (320 / 360) * screenWidth,
                    margin: EdgeInsets.only(top: (12 / 720) * screenHeight,
                        right: 20 / 360 * screenWidth),
                    child: SingleChildScrollView(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.all(1.0),
                            child: ReadMoreText(
                              list_comments[i].comment_text,
                              //  SubQueriesCommentsScreenStrings.ReadMoreText_Content,
                              trimLines: 3,
                              colorClickableText: Colors.blue,
                              trimMode: TrimMode.Line,
                              trimCollapsedText: SubQueriesCommentsScreenStrings
                                  .Text_ReadMore,
                              trimExpandedText: SubQueriesCommentsScreenStrings
                                  .Text_ReadLess,
                              style: TextStyle(
                                color: NeutralColors.gunmetal,
                                fontWeight: FontWeight.w500,
                                fontFamily: "IBMPlexSans",
                                fontStyle: FontStyle.normal,
                                height: 1.5,
                                fontSize: (13 / 720) * screenHeight,
                              ),

                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                  /**upvotes and reply colum**/
                  Container(
                    child: Row(
                      children: <Widget>[
                        InkWell(
                          onTap: () {
                            setState(() {
                              upvoted = !upvoted;
                              if (like.contains(i)) {
                                like.remove(i);
                              } else {
                                like.add(i);
                              }
                            });
                          },
                          child: Padding(
                            padding: const EdgeInsets.all(5.0),
                            child: Row(
                              children: <Widget>[
                                Container(
                                  height: (11 / 720) * screenHeight,
                                  width: (11 / 360) * screenWidth,
                                  margin: EdgeInsets.only(
                                      top: (18 / 720) * screenHeight),
                                  child: Icon(
                                    Icons.thumb_up,
                                    color: like.contains(i) ? NeutralColors
                                        .deep_sky_blue : NeutralColors.black,
                                    size: 11 / 360 * screenWidth,
                                  ),
                                ),
                                Container(
                                  height: 23 / 720 * screenHeight,
                                  width: 64 / 360 * screenWidth,
                                  margin: EdgeInsets.only(
                                      top: 18 / 720 * screenHeight,
                                      left: 5 / 360 * screenWidth),
                                  decoration: BoxDecoration(
                                    color: Colors.transparent,
                                    borderRadius: BorderRadius.all(
                                        Radius.circular(2)),
                                  ),
                                  child: Container(
                                    margin: EdgeInsets.only(
                                        top: 6 / 720 * screenHeight),
                                    child: Text(
                                      like.contains(i) ? "${(list_comments[i]
                                          .numberofvotes) + 1}" +
                                          SubQueriesCommentsScreenStrings
                                              .Text_Liked : "${list_comments[i]
                                          .numberofvotes}" +
                                          SubQueriesCommentsScreenStrings
                                              .Text_Liked,
                                      style: TextStyle(
                                        color: like.contains(i) ? NeutralColors
                                            .deep_sky_blue : NeutralColors
                                            .dark_navy_blue,
                                        fontWeight: FontWeight.w500,
                                        fontFamily: "IBMPlexSans",
                                        fontStyle: FontStyle.normal,
                                        fontSize: (11 / 720) * screenHeight,

                                      ),
                                      textAlign: TextAlign.left,

                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                        InkWell(
                          onTap: () {
//                          setState(() {
//                            isReply = !isReply;
//                            _onSelected_reply(i);
//                            print("isreplies================00"+i.toString()+"=========="+list_comments[i].toString());
//                          });
                            AppRoutes.push(context, PrepareListOfRepliesHtml());
                          },
                          child: Row(
                            children: <Widget>[
                              Container(
                                height: (9 / 720) * screenHeight,
                                width: (9 / 360) * screenWidth,
                                margin: EdgeInsets.only(
                                    top: (20 / 720) * screenHeight,
                                    left: (30 / 360) * screenWidth),
//                          child: Image.asset(
//                            "assets/images/reply.png",
//                            fit: BoxFit.fitHeight,
//                          ),
                                child: Icon(
                                  Icons.chat_bubble_outline,
                                  color: NeutralColors.black,
                                  size: 9 / 360 * screenWidth,
                                ),
                              ),
                              Container(
                                  height: 23 / 720 * screenHeight,
                                  width: 30 / 360 * screenWidth,
                                  margin: EdgeInsets.only(
                                      top: 15 / 720 * screenHeight,
                                      left: 5 / 360 * screenWidth),
                                  decoration: BoxDecoration(
                                    color: Colors.transparent,
                                    borderRadius: BorderRadius.all(
                                        Radius.circular(2)),
                                  ),
                                  child: Container(
                                    margin: EdgeInsets.only(
                                        top: 6 / 720 * screenHeight),
                                    child: Text(
                                      SubQueriesCommentsScreenStrings
                                          .Text_Reply,
                                      style: TextStyle(
                                        color: NeutralColors.dark_navy_blue,
                                        fontWeight: FontWeight.w500,
                                        fontFamily: "IBMPlexSans",
                                        fontStyle: FontStyle.normal,
                                        fontSize: (11 / 720) * screenHeight,
                                      ),
                                      textAlign: TextAlign.left,
                                    ),
                                  )
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
//                /**replybox**/
//               _selectedIndexReply == i ? (isReply ? Container(
//                 child: Row(
//                   children: <Widget>[
//                     Container(
//                       height:30/720*screenHeight,
//                       width: 30/360*screenWidth,
//                       margin: EdgeInsets.only(top: (17/964)*screenHeight),
//                       decoration: new BoxDecoration(
//                           shape: BoxShape.circle,
//                           image: new DecorationImage(
//                               fit: BoxFit.cover,
//                               image: AssetImage(
//                                   'assets/images/oval-2.png'))),
//                     ),
//                     Container(
//                       height: (40/720)*screenHeight,
//                       width: (280/360) * screenWidth,
//                       margin: EdgeInsets.only(left: (10/360)*screenWidth,top: (12/720)*screenHeight),
//                       decoration: BoxDecoration(
//                         border: Border.all(color: AccentColors.iceBlue),
//                         color:Colors.white,
//                         borderRadius: BorderRadius.all(Radius.circular(5)),
//                       ),
//                       child: Row(
//                         children: <Widget>[
//                           Container(
//                             height: (18/720)*screenHeight,
//                             width: (200/360) * screenWidth,
//                             margin: EdgeInsets.only(left: (13/360)*screenWidth,top: (10/720)*screenHeight,bottom: (10/720)*screenHeight),
//                             child: TextFormField(
//                               controller: uploaded_reply ? pathreply_Controller : reply_Controller,
//                               style: TextStyle(
//                                 backgroundColor: NeutralColors.pureWhite,
//                                 color: NeutralColors.black,
//                                 fontWeight: FontWeight.bold,
//                                 fontFamily: "IBMPlexSans",
//                                 fontStyle: FontStyle.normal,
//                                 fontSize: (14/720)*screenHeight,
//                               ),
//                               decoration: new InputDecoration.collapsed
//                                 (
//                                 hintText:  SubQueriesCommentsScreenStrings.Text_Reply + "...",
//                                 hintStyle: TextStyle (
//                                   color: AccentColors.iceBlue,
//                                   fontWeight: FontWeight.w400,
//                                   fontFamily: "IBMPlexSans",
//                                   fontStyle: FontStyle.normal,
//                                   fontSize: (14/720)*screenHeight,
//                                 ),
//                               ),
//                               onFieldSubmitted: (replytext){
//                                 print(replytext);
//                                 reply_Controller.clear();
//                                 list_Reply.add(CommentsOptions.replies(replytext));
//                                  setState(() {
//                                    isReply = !isReply;
//                                  });
//                               },
//                             ),
//                           ),
//                           GestureDetector(
//                             onTap: _pickFileInProgress_reply ? null : _pickDocument_reply,
//                             child: Container(
//                               height: (18 /720) * screenHeight,
//                               width: (19 / 360) * screenWidth,
//                               margin: EdgeInsets.only(left: 29/360*screenWidth,top: (8/720)*screenHeight,bottom: (10/720)*screenHeight),
//                               child: Image.asset(
//                                 "assets/images/clip-4.png",
//                                 fit: BoxFit.fill,
//                               ),
//                             ),
//                           )
//                         ],
//                       ),
//                     ),
//                   ],
//                 ),
//               ) : Container()) : Container(),
//                /**showreplies colum**/
//                InkWell(
//                  onTap: (){
//                    print("clicked reply");
//                    setState(() {
//                      if(list_Reply.isEmpty && list_Reply.length == null){
//                        Container(
//                          height: 20/720*screenHeight,
//                          margin: EdgeInsets.only(top: 24.5/720*screenHeight),
//                          child: Text("No Comments",style: TextStyle(color: Colors.blue),),
//                        );
//                      }else{
//                        _onSelected_showreply(i);
//                        if(i != null){showReplies_Active = !showReplies_Active;}
//                      }
//
//
//                    });
//                  },
//                  child: Container(
//                      height: 23/720*screenHeight,
//                      width: 73/360 * screenWidth,
//                      margin: EdgeInsets.only(top: 5/720*screenHeight),
//                      decoration: BoxDecoration(
//                        color:Colors.transparent,
//                        borderRadius: BorderRadius.all(Radius.circular(2)),
//                      ),
//                      child:Container(
//                        margin: EdgeInsets.only(top:6/720*screenHeight),
//                        child: Text(
//                          SubQueriesCommentsScreenStrings.Text_ShowReplies,
//                          style: TextStyle (
//                            color: NeutralColors.blue_grey,
//                            fontWeight: FontWeight.w500,
//                            fontFamily: "IBMPlexSans",
//                            fontStyle: FontStyle.normal,
//                            fontSize: (12/720)*screenHeight,
//                          ),
//                          textAlign: TextAlign.left,
//                        ),
//                      )
//                  ),
//                ),
//                _selectedIndexShowReply == i ?
//                _showReply(screenHeight, screenWidth) :
//                Container(),
                  Container(margin: EdgeInsets.only(
                      top: 24.5 / 720 * screenHeight,
                      right: 20 / 360 * screenWidth),
                      child: Divider(
                        height: 1 / 720 * screenHeight,
                        color: Color(0xffe4e8e8),
                      ))
                ],
              ),
            ),
          );
        }
      }
      return new Column(children: list);

  }
   Widget _showReply(final screenHeight,final screenWidth){
     List<Widget> list = new List<Widget>();
     if(list_Reply.isEmpty || list_Reply.length == null){
       list.add(
           Container(
             height: 20/720*screenHeight,
             margin: EdgeInsets.only(top: 24.5/720*screenHeight),
             child: Text("No Comments",style: TextStyle(color: Colors.blue),),
           ));
     }else{
       for(var i=0; i < list_Reply.length; i++){
         list.add(
             Container(
               height: 20,
               child: Text(list_Reply[i].replies,
                 style: TextStyle(
                   color: NeutralColors.black,
                   fontWeight: FontWeight.w200,
                   fontFamily: "IBMPlexSans",
                   fontStyle: FontStyle.normal,
                   fontSize: (10/720)*screenHeight,
                 ),
               ),
             )
         );
       }
     }
     return new Column(children: list);
   }
}


Widget _ReadMoreReadLess(){
  final String description =
      SubQueriesCommentsScreenStrings.ReadMoreText_Content;
  return Container(
    child: new DescriptionTextWidget(text: description),
  );
}

class DescriptionTextWidget extends StatefulWidget {
  final String text;

  DescriptionTextWidget({@required this.text});

  @override
  _DescriptionTextWidgetState createState() => new _DescriptionTextWidgetState();
}

class _DescriptionTextWidgetState extends State<DescriptionTextWidget> {
  String firstHalf;
  String secondHalf;

  bool flag = true;

  @override
  void initState() {

    super.initState();

    if (widget.text.length > 50) {
      firstHalf = widget.text.substring(0, 70);
      secondHalf = widget.text.substring(50, widget.text.length);
    } else {
      firstHalf = widget.text;
      secondHalf = "";
    }
  }

  @override
  Widget build(BuildContext context) {
    final screenHeight = MediaQuery.of(context).size.height;
    final screenWidth = MediaQuery.of(context).size.width;
    return new Container(
      padding: new EdgeInsets.symmetric(horizontal: 10.0, vertical: 10.0),
      child: secondHalf.isEmpty
          ? new Text(firstHalf)
          : new Column(
        children: <Widget>[
          new Text(flag ? (firstHalf + "...") : (firstHalf + secondHalf)),
          new InkWell(
            child: new Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                new Text(
                  flag ? "show more" : "show less",
                  style: new TextStyle(color: Colors.blue,),
                ),
              ],
            ),
            onTap: () {
              setState(() {
                flag = !flag;
              });
            },
          ),
        ],
      ),
    );
  }

}
_getPositions() {
  final RenderBox renderBoxRed = _keyRed.currentContext.findRenderObject();
  final positionRed = renderBoxRed.localToGlobal(Offset.zero);
  print("POSITION of Red: $positionRed ");
  return positionRed;
}

_getSizes() {
  final RenderBox renderBoxRed = _keyRed.currentContext.findRenderObject();
  final sizeRed = renderBoxRed.size;

  print("SIZE of Red: $sizeRed");
  return sizeRed.height;
}
class SampleService {
  static final List<String> degrees = [
    '',
    'Anusha',
    'Vetri',
    'Kavya',
  ];

  static List<String> getSuggestions(String query) {
    List<String> matches = List();
    matches.add("Date");
    matches.add("Upvotes");

    //  matches.retainWhere((s) => s.toLowerCase().contains(query.toLowerCase()));
    return matches;
  }
}











import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:imsindia/utils/svg_images/leaderboard_svg_images.dart';
import 'package:imsindia/views/leaderboard/leaderboard_screen.dart';
import '../../utils/colors.dart';

class HOFData {
  String name;
  String batch;
  String score;
  HOFData({
    this.name,
    this.batch,
    this.score,
  });
}

class UnicornOutlineButton extends StatelessWidget {
  final _GradientPainter _painter;
  final Widget _child;
  // final VoidCallback _callback;
  final double _radius;

  UnicornOutlineButton({
    @required double strokeWidth,
    @required double radius,
    @required Gradient gradient,
    @required Widget child,
    //@required VoidCallback onPressed,
  })  : this._painter = _GradientPainter(strokeWidth: strokeWidth, radius: radius, gradient: gradient),
        this._child = child,
        //this._callback = onPressed,
        this._radius = radius;

  @override
  Widget build(BuildContext context) {
    return CustomPaint(
      painter: _painter,
      child: GestureDetector(
        behavior: HitTestBehavior.translucent,
        //onTap: _callback,
        child: InkWell(
          borderRadius: BorderRadius.circular(_radius),
          //onTap: _callback,
          child: Container(
            constraints: BoxConstraints(minWidth: 88, minHeight: 48),
            child: _child,
          ),
        ),
      ),
    );
  }
}

class _GradientPainter extends CustomPainter {
  final Paint _paint = Paint();
  final double radius;
  final double strokeWidth;
  final Gradient gradient;

  _GradientPainter({@required double strokeWidth, @required double radius, @required Gradient gradient})
      : this.strokeWidth = strokeWidth,
        this.radius = radius,
        this.gradient = gradient;

  @override
  void paint(Canvas canvas, Size size) {
    // create outer rectangle equals size
    Rect outerRect = Offset.zero & size;
    var outerRRect = RRect.fromRectAndRadius(outerRect, Radius.circular(radius));

    // create inner rectangle smaller by strokeWidth
    Rect innerRect = Rect.fromLTWH(strokeWidth, strokeWidth, size.width - strokeWidth * 2, size.height - strokeWidth * 2);
    var innerRRect = RRect.fromRectAndRadius(innerRect, Radius.circular(radius - strokeWidth));

    // apply gradient shader
    _paint.shader = gradient.createShader(outerRect);

    // create difference between outer and inner paths and draw it
    Path path1 = Path()..addRRect(outerRRect);
    Path path2 = Path()..addRRect(innerRRect);
    var path = Path.combine(PathOperation.difference, path1, path2);
    canvas.drawPath(path, _paint);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) => oldDelegate != this;
}




class HalloffameScreen extends StatefulWidget {
  @override
  _HalloffameScreenState createState() => _HalloffameScreenState();
}

class _HalloffameScreenState extends State<HalloffameScreen> {

  @override
  Widget build(BuildContext context) {
    final screenHeight = MediaQuery.of(context).size.height;
    final screenWidth = MediaQuery.of(context).size.width;

    final member = HOFData(
      name: "Steven Allison",
      batch: "Batch of 2019",
      score: '10,129 runs',
    );

    final List<HOFData> members = [
      member,
      member,
      member,
      member,
      member,
      member,
      member,
      member,
      member,
      member,
      member,
    ];

    return Scaffold(
      appBar: new AppBar(
        brightness: Brightness.light,
        elevation: 0.0,
        centerTitle: false,
        iconTheme: IconThemeData(
          color: Colors.black, //change your color here
        ),
        backgroundColor: Color.fromARGB(255, 255, 255, 255),
        titleSpacing: 0.0,
        title: Text(
          "Hall of fame",
          style: TextStyle(
            color: Color.fromARGB(255, 0, 0, 0),
            fontSize: screenWidth * 16 / 360,
            fontFamily: "IBMPlexSans",
            fontWeight: FontWeight.w500,
          ),
          textAlign: TextAlign.left,
        ),
        leading:  GestureDetector(
                  onTap: (){
                    Navigator.pop(context);
                  },
                  child: Container(
                          color: Colors.transparent,
                          width: 28 / 360 * screenWidth,
                          height: 24 / 720 * screenHeight,
                          child: SvgPicture.asset(
                            LeaderboardAssets.back_icon,
                            fit: BoxFit.scaleDown,
                          ),
                        ),
        ),
      ),
      body: Container(
        constraints: BoxConstraints.expand(),
        decoration: BoxDecoration(
          color: Color.fromARGB(255, 255, 255, 255),
        ),
        child: Column(
          children: <Widget>[
            Expanded(
                child: Container(
                margin: EdgeInsets.only(
                    left: 18 / 360 * screenWidth,
                    right: 18 / 360 * screenWidth),
                child: ListView.builder(
                  itemCount: members.length,
                  scrollDirection: Axis.vertical,
                  shrinkWrap: true,
                  itemBuilder: (context, index) {
                    return Container(
                      color: Colors.transparent,
                      margin: EdgeInsets.only(top: 10 / 720 * screenHeight,
                      bottom: index == members.length-1 ?  10 / 720 * screenHeight: 0),
                      child: UnicornOutlineButton(
                         strokeWidth: 1,
                         radius: 5,
                         gradient: LinearGradient(
                           colors: [
                            //  Color(0x33a8ae23), 
                            //  Color(0x33c86dd7)
                              Color(0x33a8ae23),
                                Color(0x33c86dd7)
                             ]
                            ),
                          child: Container(
                         // height: 100 / 720 * screenHeight,
                          decoration: BoxDecoration(
                            gradient: LinearGradient(
                              colors: [
                            
                                //  Color(0x33a8ae23),
                                //  Color(0x33c86dd7),
                                Color(0xffa8ae23).withOpacity(.1),
                                Color(0xffc86dd7).withOpacity(0.1)
                              ],
                              // begin: Alignment(0, 0),
                              // end: Alignment(
                              //     1.0199999809265137, 1.0099999904632568),
                            ),
                            borderRadius: BorderRadius.all(Radius.circular(5)),
                          ),
                          child: Container(
                           // contentPadding: EdgeInsets.all(0),
                           // dense: true,
                            margin: EdgeInsets.symmetric(horizontal: 15 * screenWidth / 360,vertical: 15 / 720 * screenHeight),
                            child: Row(
                              children: <Widget>[
                                Container(
                                  height: 60 / 720 * screenHeight,
                                  width: 60 / 360 * screenWidth,
                                  child:
                                      Image.asset('assets/images/avatar-2.png'
                                      ,fit: BoxFit.contain,
                                      ),
                                ),
                                Expanded(
                                   child: Container(
                                     margin: EdgeInsets.only(left: 10 / 720 * screenHeight),
                                     child: Row(
                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                      children: <Widget>[
                                        Container(                                          
                                          //padding: EdgeInsets.only(top: 20 / 720 * screenHeight,bottom: 20 / 720 * screenHeight),
                                          child: Column(
                                            crossAxisAlignment: CrossAxisAlignment.start,
                                            children: <Widget>[
                                              Container(
                                                child: Text(
                                                   members[index].name,
                                                   softWrap: false,
                                                  style: TextStyle(
                                                fontStyle: FontStyle.normal,
                                                fontSize: 14 / 360 * screenWidth,
                                                fontFamily: "IBMPlexSans",
                                                color: NeutralColors.dark_navy_blue,
                                                fontWeight: FontWeight.w500,
                                                  ),
                                                ),
                                              ),
                                              Container(
                                                child: Text(
                                                  members[index].batch,
                                                  style: TextStyle(
                                                    fontSize: 12 / 360 * screenWidth,
                                                    fontFamily: "IBMPlexSans",
                                                    color: NeutralColors.bluey_grey,
                                                    fontWeight: FontWeight.w500,
                                                  ),
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                          Container(
                                          child: Text(
                                            members[index].score,
                                            overflow: TextOverflow.clip,
                                            style: TextStyle(
                                              fontSize: 16 / 360 * screenWidth,
                                              fontFamily: "IBMPlexSans",
                                              color: Color(0xff1f252b),
                                              fontWeight: FontWeight.w700,
                                            ),
                                          ),
                                        ),
                                      ],
                                  ),
                                   ),
                                ),
                              ],
                            ),
                            // title: Text(
                            //   members[index].name,
                            //   softWrap: false,
                            //   style: TextStyle(
                            //     fontStyle: FontStyle.normal,
                            //     fontSize: 14 / 360 * screenWidth,
                            //     fontFamily: "IBMPlexSans",
                            //     color: NeutralColors.dark_navy_blue,
                            //     fontWeight: FontWeight.w500,
                            //   ),
                            // ),
                            // subtitle: Text(
                            //   members[index].batch,
                            //   style: TextStyle(
                            //     fontSize: 12 / 360 * screenWidth,
                            //     fontFamily: "IBMPlexSans",
                            //     color: NeutralColors.bluey_grey,
                            //     fontWeight: FontWeight.w500,
                            //   ),
                            // ),
                            // trailing: Text(
                            //   members[index].score,
                            //   style: TextStyle(
                            //     fontSize: 16 / 360 * screenWidth,
                            //     fontFamily: "IBMPlexSans",
                            //     color: Color(0xff1f252b),
                            //     fontWeight: FontWeight.w700,
                            //   ),
                            // ),
                          ),
                        ),
                      ),
                    );
                  },
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

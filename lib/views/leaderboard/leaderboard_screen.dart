import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:imsindia/components/Dropdown_Leaderboard.dart';
import 'package:imsindia/components/bottom_navigation.dart';
import 'package:sliding_up_panel/sliding_up_panel.dart';
import 'package:imsindia/utils/colors.dart';
import 'package:imsindia/components/navigation_bar.dart';
import 'package:imsindia/utils/svg_images/leaderboard_svg_images.dart';
import 'package:imsindia/views/leaderboard/halloffame_screen.dart';
import 'package:imsindia/utils/emaximiser_svg_icons.dart';

var screenWidthTotal;
var screenHeightTotal;
var _safeAreaHorizontal;
var _safeAreaVertical;
var screenHeight;
var screenWidth;

class LeaderData {
  String name;
  List<String> marks;
  int rank;
  String imageLink;
  String city;
  String badges;
  List<String> badge;
  LeaderData(
      {this.name,
        this.marks,
        this.rank,
        this.imageLink,
        this.city,
        this.badges,
        this.badge});
}


class StatesService {
  static final List<String> subjects = [
    'Overall',
    'Blue Cap',
    'Purple Cap Keeper',
    'VA-RC Ameteur',
    'Quant Wizard',
  ];

  static List<String> getSuggestions() {
    List<String> matches = List();
    matches.addAll(subjects);

    //matches.retainWhere((s) => s.toLowerCase().contains(query.toLowerCase()));
    return matches;

  }

//  static List<String> getSuggestions() {
//    CourseService().CourseData();
//    List<String> matches = List();
//    matches.addAll(courses);
//    //  matches.retainWhere((s) => s.toLowerCase().contains(query.toLowerCase()));
//    return courses;
//  }
}




class LeaderboardScreen extends StatefulWidget {
  @override
  LeaderboardState createState() => LeaderboardState();

}

class LeaderboardState extends State<LeaderboardScreen> {

  final TextEditingController _stateOfsubjects = new TextEditingController();
  String courseId;


  var value = '';
  String _selectedItem = '';
  final _scaffoldKey = new GlobalKey<ScaffoldState>();
  VoidCallback _showPersBottomSheetCallBack;

  // String _selectedItem = '';

  var first = '';
  bool isSelected = true;



  final List<Map> badgesList = [
    {
      // "image":"assets/images/lock.svg",
      "image" :"assets/images/Maestro_DI.svg",
      "name": "Blue Cap",
      "type": "State VA-RC Leaderboard Topper",

    },
    {
      "image":"assets/images/Amateur_DI.svg",
      "name": "Purple Cap Keeper",
      "type": "National VA-RC Leaderboard Topper",
      // "logoText": "https://cdn.pixabay.com/photo/2017/01/31/13/14/animal-2023924_960_720.png"
    },
    {
      "image":"assets/images/Maestro_LR.svg",
      "name": "VA-RC Ameteur",
      "type": "State VA-RC Leaderboard Topper",
      //"logoText": "https://cdn.pixabay.com/photo/2016/06/09/18/36/logo-1446293_960_720.png"
    },
    {
      "image":"assets/images/Prodigy_GK.svg",
      "name": "DI-LR Maestro",
      "type": "State DI-LR Leaderboard Topper",
      //"logoText": "https://cdn.pixabay.com/photo/2017/03/16/21/18/logo-2150297_960_720.png"
    },
    {
      "image":"assets/images/Prodigy_GK.svg",
      "name": "Quant Wizard",
      "type": "State Quant Leaderboard Topper",
      //"logoText": "https://cdn.pixabay.com/photo/2017/03/16/21/18/logo-2150297_960_720.png"
    },
    {
      "image":"assets/images/Maestro_DI.svg",
      "name": "GK Maestro",
      "type": "State DI-LR Leaderboard Topper",
      //"logoText": "https://cdn.pixabay.com/photo/2017/01/31/13/14/animal-2023924_960_720.png"
    },
    {
      "image":"assets/images/Amateur_DI.svg",
      "name": "GK Maestro",
      "type": "State DI-LR Leaderboard Topper",
      //"logoText": "https://cdn.pixabay.com/photo/2017/01/31/13/14/animal-2023924_960_720.png"
    },
    {
      "image":"assets/images/Maestro_LR.svg",
      "name": "Blue Cap",
      "type": "State VA-RC Leaderboard Topper",
      //"logoText": "https://cdn.pixabay.com/photo/2017/01/13/01/22/rocket-1976107_960_720.png"
    },
  ];


  @override
  void initState() {
    super.initState();
    _showPersBottomSheetCallBack = _showBottomSheet;
  }

  void _showBottomSheet() {
    final screenWidth = MediaQuery.of(context).size.width;
    final screenHeight = MediaQuery.of(context).size.height;
    setState(() {
      _showPersBottomSheetCallBack = null;
    });

    _scaffoldKey.currentState
        .showBottomSheet((context) {
      return Container(
        decoration: BoxDecoration(
          color: NeutralColors.pureWhite,
          borderRadius:BorderRadius.only(
            topLeft:Radius.circular(20),
            topRight: Radius.circular(20),
          ),
          boxShadow: [
            BoxShadow(
              blurRadius: 16.0,
              color: Colors.black.withOpacity(.5),
              offset: Offset(0.0,8.0),
            ),
          ],
        ),


        child: Stack(
          children: <Widget>[

            InkWell(
              onTap: () => _selectItem('Juan'),
              child: Container(
                height: 76/720*screenHeight,
                width: 360*screenWidth / 360,
                margin: EdgeInsets.only(top: 30/720*screenHeight,left:25/360*screenWidth,bottom: 25/720*screenHeight),
                child: Text("Juan Gregory's badges",
                  style:TextStyle(
                      fontSize: 18/360*screenWidth,
                      fontFamily: "IBMPlexSans",
                      fontWeight:FontWeight.bold,
                      color: NeutralColors.black),),
              ),
            ),
            Container(
              margin: EdgeInsets.only(top:76/720*screenHeight),
              height: 2,
              color:NeutralColors.ice_blue,
              width: double.infinity,
            ),

            InkWell(
              onTap: () => _selectItem(''),
              child: Container(
                padding: EdgeInsets.only(top: 102/720*screenHeight),
                height: MediaQuery.of(context).size.height,
                width: double.infinity,
                child: ListView.builder(
                    itemCount: badgesList.length,
                    itemBuilder: (BuildContext context, int index) {
                      return buildList(context, index);
                    }),
              ),
            ),

          ],
        ),
      );
    })
        .closed
        .whenComplete(() {
      if (mounted) {
        setState(() {
          _showPersBottomSheetCallBack = _showBottomSheet;
        });
      }
    });
  }



  @override
  Widget build(BuildContext context) {
    //final screenHeight = MediaQuery.of(context).size.height;
    // final screenWidth = MediaQuery.of(context).size.width;

    var _mediaQueryData = MediaQuery.of(context);
    screenWidthTotal = _mediaQueryData.size.width;
    screenHeightTotal = _mediaQueryData.size.height;
    _safeAreaHorizontal =
        _mediaQueryData.padding.left + _mediaQueryData.padding.right;
    _safeAreaVertical =
        _mediaQueryData.padding.top + _mediaQueryData.padding.bottom;
    screenHeight = screenHeightTotal - _safeAreaVertical;
    screenWidth = screenWidthTotal - _safeAreaHorizontal;

    

    final leader = LeaderData(
      name: "Rena Tyler",
      rank: 11,
      city: "Mumbai",
      imageLink: 'assets/images/avatar-2.png',
      badge: ['assets/images/Amateur.png'],

      marks: [
        "99",
        "10",
      ],
    );

    final heading = LeaderData(name: "STANDINGS", marks: [
      "RUNS",
      "S/R",
      "ACHIEVEMENTS",
    ]);

    final List<LeaderData> leaders = [
      heading,
      leader,
      leader,
      leader,
      leader,
      leader,
      leader,
      leader,
      leader,
      leader,
      leader,
      leader,

    ];

    return Scaffold(
      key: _scaffoldKey,
      appBar: new AppBar(
        elevation: 0.0,
        centerTitle: false,
        brightness: Brightness.light,
        iconTheme: IconThemeData(
          color: Colors.black, //change your color here
        ),
        backgroundColor: Color.fromARGB(255, 255, 255, 255),
        titleSpacing: 0.0,
        title: Text(
          "Leaderboard",
          style: TextStyle(
            color: Color.fromARGB(255, 0, 0, 0),
            fontSize: screenHeight * 16 / 720,
            fontFamily: "IBMPlexSans",
            fontWeight: FontWeight.w500,
          ),
          textAlign: TextAlign.left,
        ),
        actions: <Widget>[
          GestureDetector(
            onTap: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => HalloffameScreen()));
            },
            child: Container(
              height: 20 / 720 * screenHeight,
              width: 25 / 360 * screenWidth,
              margin: EdgeInsets.only(right: 20 / 360 * screenWidth),
              child: new SvgPicture.asset(
                LeaderboardAssets.olive_leaves_icon,
                fit: BoxFit.contain,
              ),
            ),
          ),
        ],
      ),
      drawer: NavigationDrawer(),
//      bottomNavigationBar: BottomNavigation(),

      body: Container(
        color:NeutralColors.pureWhite,
        child: Column(
          children: <Widget>[

            Container(
              decoration: BoxDecoration(
                color: NeutralColors.pureWhite,
                boxShadow: [
                            BoxShadow(
                              color: Color(0x0d676767),
                              //color: Colors.red,
                              offset: Offset(8, 0),
                              spreadRadius: 2,
                              blurRadius: 16,
                            ),
                          ],
              ),
              
              child: Column(
                children: <Widget>[
                  Container(
                      decoration: BoxDecoration(
                        color: NeutralColors.pureWhite,
                        
                      ),

                      margin: EdgeInsets.symmetric(horizontal: 20.0),
                      // color: Colors.orange,
                      child: DropDownFormField(
                        getImmediateSuggestions: true,

                        // autoFlipDirection: true,
                        textFieldConfiguration: TextFieldConfiguration(
                          //  controller: _courseDetails,
                            controller: _stateOfsubjects,
                            label: value == '' ? "Overall" : value 
                        ),
                        suggestionsBoxDecoration: SuggestionsBoxDecoration(
                            dropDownHeight: 150,
                            borderRadius: new BorderRadius.circular(5.0),
                            color: Colors.white),
                        suggestionsCallback: (pattern) {
                          return  StatesService.getSuggestions();
                        },
                        itemBuilder: (context, suggestion) {
                          return ListTile(
                            dense: true,
                            contentPadding: EdgeInsets.only(left: 10),
                            title: Text(
                              suggestion,
                              style: TextStyle(
                                fontFamily: "IBMPlexSans",
                                fontWeight: FontWeight.w400,
                                fontSize: (13 / 720) * screenHeight,
                                color: NeutralColors.bluey_grey,
                                //   textBaseline: TextBaseline.alphabetic,
                                letterSpacing: 0.0,
                                inherit: false,

                              ),
                            ),
                          );
                        },
                        hideOnLoading: true,
                        debounceDuration: Duration(milliseconds: 100),
                        transitionBuilder: (context, suggestionsBox, controller) {
                          return suggestionsBox;
                        },
                        onSuggestionSelected: (suggestion) {
                          for (int i = 0; i <StatesService.subjects.length; i++) {
                            if (StatesService.subjects[i] == suggestion) {
                              // courseId = CourseService.data[i];
                            }
                          }
                          print(suggestion);
                         
                           setState(() {
                              _stateOfsubjects.text=suggestion;
                              value = _stateOfsubjects.text;
                           });
                        },
                        onSaved: (value) => {print("something")},
                      )
                  ),
                  Container(
                    margin:EdgeInsets.only(right: 186/360*screenWidth,top: 18/720*screenHeight,left: 19/360*screenWidth,bottom: 18/720*screenHeight),
                    height: 40/720*screenHeight,
                    width:155/360*screenWidth,

                    child: Stack(
                      children: <Widget>[
                        GestureDetector(
                          onTap: (){
                            setState(() {
                              isSelected = !isSelected;
                            });
                          },
                          child: ClipPath(
                            clipper: ShapeBorderClipper(
                                shape: StadiumBorder(side: BorderSide(width: 1,style: BorderStyle.solid,color: NeutralColors.purpleish_blue))
                            ),

                            child: Container(
                              width:155/360*screenWidth,
                              height: 40/720*screenHeight,
                              color: isSelected ? NeutralColors.ice_blue : NeutralColors.purpleish_blue,
                              child: Padding(
                                padding: EdgeInsets.only(left: 85/360*screenWidth,top: 10/720*screenHeight,right: 12/360*screenWidth),
                                child: Text('Mumbai',style: TextStyle(
                                  color: isSelected ? NeutralColors.dark_navy_blue : Colors.white,
                                  fontSize: 14/360*screenWidth,
                                  fontFamily: "IBMPlexSans",
                                  fontWeight: FontWeight.w500,
                                ),
                                ),
                              ),

                            ),

                          ),
                        ),

                        GestureDetector(
                          onTap: (){
                            setState(() {
                              isSelected = !isSelected;
                            });
                          },
                          child: ClipPath(
                            clipper: ShapeBorderClipper(
                                shape: StadiumBorder(side: BorderSide(width: 1,style: BorderStyle.solid,color: NeutralColors.purpleish_blue))
                            ),
                            child: Container(
                              padding: EdgeInsets.only(left:22/360*screenWidth,top: 10/720*screenHeight,right: 10/360*screenWidth ),
                              width: 78/360*screenWidth,
                              height: 40/720*screenHeight,
                              color: isSelected ? NeutralColors.purpleish_blue : NeutralColors.ice_blue,
                              child: Text('India',style: TextStyle(
                                color: isSelected ?Colors.white : NeutralColors.dark_navy_blue,
                                fontSize: 14/360*screenWidth,
                                fontFamily: "IBMPlexSans",
                                fontWeight:FontWeight.w500,
                              ),
                              ),
                            ),

                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),



            Expanded(
              //height: 590 / 720 * screenHeight,
              child: SingleChildScrollView(
                child: Container(
                  decoration: BoxDecoration(
                    //color: Colors.white
                          ),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Expanded(
                          child: Container(
                          decoration: BoxDecoration(
                            color: Colors.white70,

                            boxShadow: [
                              BoxShadow(
                                color: Color(0x0d676767),
                                //color:Colors.red,
                                offset: Offset(8, 0),
                                spreadRadius: 2,
                                blurRadius: 16,
                              ),
                            ],
                            
                          ),
                          // margin: EdgeInsets.only(right: screenWidth/2, top:2),
                          // height:screenHeight ,
                          child: ListView.builder(
                              physics: NeverScrollableScrollPhysics(),
                              //controller: _scrollController,
                              shrinkWrap: true,
                              itemCount: leaders.length,
                              itemBuilder: (context, index) {
                                final item = leaders[index];
                                return Container(
                                  //color: index == 0 ? Colors.transparent : Colors.white,
                                  child: Column(
                                    children: <Widget>[
                                      (index == 0)
                                          ? Container(
                                        height: 60 / 720 * screenHeight,
                                        //width: 100,
                                        margin: EdgeInsets.only(left: 20 / 360 * screenWidth),
                                        alignment: Alignment.centerLeft,
                                        child: Text(
                                          item.name,
                                          style: TextStyle(
                                            color: NeutralColors.gunmetal,
                                            fontSize: 10 / 360 * screenWidth,
                                            fontFamily:
                                            "IBMPlexSans",
                                            fontWeight: FontWeight.w500,
                                          ),
                                          textAlign: TextAlign.left,
                                        ),
                                      )
                                          : Container(
                                        height: 60 / 720 * screenHeight,
                                        //width: 100,
                                        margin: EdgeInsets.only(left: 20 / 360 * screenWidth),
                                        child: Row(
                                          mainAxisAlignment:
                                          MainAxisAlignment.start,
                                          children: <Widget>[
                                            Center(
                                              child: Text(
                                                item.rank.toString(),
                                                style: TextStyle(
                                                  color: NeutralColors.gunmetal,
                                                  fontSize:
                                                  12 / 360 * screenWidth,
                                                  fontFamily:
                                                  "IBMPlexSans",
                                                  fontWeight: FontWeight.w500,
                                                ),
                                                textAlign: TextAlign.left,
                                              ),
                                            ),
                                            Container(
                                              margin: EdgeInsets.only(left: 11 / 360 * screenWidth),
                                              child: Row(
                                                mainAxisAlignment:MainAxisAlignment.spaceBetween,
                                                children: <Widget>[
//                                                Container(
//                                                  height:30 / 720 * screenHeight,
//                                                  width:30 / 360 * screenWidth,
//                                                  child: Image.asset(
//                                                    item.imageLink,
//                                                    fit: BoxFit.contain,
//                                                  ),
//                                                ),

                                                  CircleAvatar(
                                                    backgroundColor: NeutralColors.purpleish_blue,
                                                    radius: 14/360*screenWidth,
                                                    child: Image.asset(
                                                      item.imageLink,
                                                      fit: BoxFit.contain,
                                                      width: 30/ 720 * screenHeight,
                                                      height: 30/ 360 * screenWidth,
//                                                    'assets/photo-camera.png',
//                                                    width: 20.9,
//                                                    height: 19.9,
                                                    ),
                                                  ),
//                                                Padding(
//                                                  padding: const EdgeInsets.all(20.0),
//                                                  child: Image.asset(
//                                                    'assets/photo-camera.png',
//                                                    width: 20.9,
//                                                    height: 19.9,
//                                                  ),
//                                                ),
                                                  Container(
                                                    margin: EdgeInsets.only(left: 8 / 360 * screenWidth),
                                                    child: Column(
                                                      mainAxisAlignment:
                                                      MainAxisAlignment
                                                          .center,
                                                      crossAxisAlignment:
                                                      CrossAxisAlignment
                                                          .start,
                                                      children: <Widget>[
                                                        Text(
                                                          item.name,
                                                          style: TextStyle(
                                                            color: NeutralColors
                                                                .dark_navy_blue,
                                                            fontSize: 14 /
                                                                360 *
                                                                screenWidth,
                                                            fontFamily:
                                                            "IBMPlexSans",
                                                            fontWeight:
                                                            FontWeight.w500,
                                                          ),
                                                          textAlign:
                                                          TextAlign.left,
                                                        ),
                                                        Text(
                                                          item.city,
                                                          style: TextStyle(
                                                            color:
                                                            SemanticColors
                                                                .blueGrey,
                                                            fontSize: 10 /
                                                                360 *
                                                                screenWidth,
                                                            fontFamily:
                                                            "IBMPlexSans",
                                                            fontWeight:
                                                            FontWeight.w500,
                                                          ),
                                                          textAlign:
                                                          TextAlign.left,
                                                        ),
                                                      ],
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                      Container(
                                        height: 1 / 720 * screenHeight,
                                        margin: EdgeInsets.only(
                                            bottom: 1 / 720 * screenHeight),
                                        decoration: BoxDecoration(
                                          color: Color.fromARGB(255, 244, 244, 244),
                                        ),
                                        child: Container(),
                                      ),
                                    ],
                                  ),
                                );
                              }),
                        ),
                      ),
                      Expanded(
                          child: Container(
                           // color: Colors.white24,
                          //height: 590 / 720 * screenHeight,
                          child: Scrollbar(
                            //ListView
                            child: SingleChildScrollView(
                              scrollDirection: Axis.horizontal,
                              child: Container(
                                width: 265/ 360 * screenWidth,
                                child: ListView.builder(
                                  physics: NeverScrollableScrollPhysics(),
                                  //controller: _scrollController,
                                  shrinkWrap: true,
                                  scrollDirection: Axis.vertical,
                                  itemCount: leaders.length,
                                  itemBuilder: (context, index) {
                                    final item = leaders[index];
                                    return Container(
                                      child: Column(
                                        children: <Widget>[
                                          Container(
                                            height: 60 / 720 * screenHeight,
                                            //width: 100,
                                            //margin: EdgeInsets.only(top: 15 / 720 * screenHeight),
                                            child: Row(
                                              crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                              mainAxisAlignment:
                                              MainAxisAlignment.center,
                                              children: <Widget>[

                                                // RUNS
                                                Container(
                                                  width: 50 / 360 * screenWidth,
                                                  child: Center(
                                                    child: Text(
                                                      item.marks[0],
                                                      style: TextStyle(
                                                        color: NeutralColors.gunmetal,
                                                        fontSize: index == 0
                                                            ? 10 / 360 * screenWidth
                                                            : 12 / 360 * screenWidth,
                                                        fontFamily: "IBMPlexSans",
                                                        fontWeight: FontWeight.w500,
                                                      ),
                                                      textAlign: TextAlign.left,
                                                    ),
                                                  ),
                                                ),

                                                // S/R
                                                Container(
                                                  width: 25 / 360 * screenWidth,
                                                  margin: EdgeInsets.only(left: 28/ 360 * screenWidth),
                                                  child: Center(
                                                    child: Text(
                                                      item.marks[1],
                                                      style: TextStyle(
                                                        color: NeutralColors.gunmetal,
                                                        fontSize: index == 0
                                                            ? 10 / 360 * screenWidth
                                                            : 12 / 360 * screenWidth,
                                                        fontFamily: "IBMPlexSans",
                                                        fontWeight: FontWeight.w500,
                                                      ),
                                                      textAlign: TextAlign.left,
                                                    ),
                                                  ),
                                                ),

                                                // ACHIEVEMENTS
                                                Container(
                                                  width: 150/ 360 * screenWidth,
                                                  child: Center(
                                                    child: InkWell(
                                                      onTap: _showPersBottomSheetCallBack,
                                                      child: Container(
//                                                    padding: EdgeInsets.symmetric(
//                                                        horizontal:35/ 360 * screenWidth,
//                                                        vertical: 15 /720 * screenHeight),
                                                          margin: EdgeInsets.only(left: 40 / 360 * screenWidth),
                                                          child: index == 0
                                                              ? Container(
                                                            height: 60 /720 * screenHeight,
                                                            width: 150 / 360 * screenWidth,
                                                            alignment: Alignment.centerLeft,
                                                            // margin: EdgeInsets.only(left: 15 / 360 * screenWidth),
                                                            child: Text(
                                                              item.marks[2],
                                                              style: TextStyle(
                                                                color:NeutralColors.gunmetal,
                                                                fontSize: 10 /360 * screenWidth,
                                                                fontFamily:"IBMPlexSans",
                                                                fontWeight:FontWeight.w500,
                                                              ),
                                                              textAlign:
                                                              TextAlign.left,
                                                            ),
                                                          )
                                                              : Container(
                                                                alignment: Alignment.centerLeft,
                                                            child: Row(
                                                              mainAxisAlignment: MainAxisAlignment.start,
                                                              children: <Widget>[

                                                                Container(
                                                                  height: 60 / 720 * screenHeight,
                                                                  width: 25 / 360 * screenWidth,
                                                                  margin: EdgeInsets.only(right: 5 /360*screenWidth ),
                                                                  child: new SvgPicture.asset(
                                                                    //  LeaderboardAssets.Maestro_DI_icon,
                                                                    badgesList[1]['image'],
                                                                    fit: BoxFit.contain,
                                                                  ),
                                                                ),

                                                                Container(
                                                                  height: 60 / 720 * screenHeight,
                                                                  width: 25 / 360 * screenWidth,
                                                                  margin: EdgeInsets.only(right: 5 /360*screenWidth ),
                                                                  child: new SvgPicture.asset(
                                                                    //  LeaderboardAssets.Maestro_DI_icon,
                                                                    badgesList[4]['image'],
                                                                    fit: BoxFit.contain,
                                                                  ),
                                                                ),

                                                                Container(
                                                                  height: 60 / 720 * screenHeight,
                                                                  width: 25 / 360 * screenWidth,
                                                                  margin: EdgeInsets.only(right: 5 /360*screenWidth ),
                                                                  child: new SvgPicture.asset(
                                                                    //  LeaderboardAssets.Maestro_DI_icon,
                                                                    badgesList[2]['image'],
                                                                    fit: BoxFit.contain,
                                                                  ),
                                                                ),
                                                              ],
                                                            ),
                                                          )

                                                      ),
                                                    ),

                                                  ),
                                                ),

                                              ],
                                            ),
                                          ),
                                          Container(
                                            height: 1 / 720 * screenHeight,
                                            margin: EdgeInsets.only(
                                                bottom: 1 / 720 * screenHeight),
                                            decoration: BoxDecoration(
                                              color:
                                              Color.fromARGB(255, 244, 244, 244),
                                            ),
                                            child: Container(),
                                          ),
                                        ],
                                      ),
                                    );
                                  },
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
            Container(
              height: 60 / 720 * screenHeight,
              //width: 100,
              color: NeutralColors.purpleish_blue,
              //margin: EdgeInsets.only(top: 15 / 720 * screenHeight),
              child: Row(
                children: <Widget>[
                  Expanded(
                      child: Container(
                      decoration: BoxDecoration(
                        color: NeutralColors.purpleish_blue,
                        //       boxShadow: [
                        //    BoxShadow(
                        //     //color: Colors.white,
                        //     color :Color(0x0d676767),
                        //     offset: Offset(8, 0),
                        //     spreadRadius: 2,
                        //     blurRadius: 16,
                        //   ),
                        // ],
                      ),
                      child: Container(
                        height: 60 / 720 * screenHeight,
                        //width: 100,
                        decoration: BoxDecoration(
                        color: NeutralColors.purpleish_blue,
                              boxShadow: [
                           BoxShadow(
                            //color: Colors.white,
                            color :Color(0x0d979797),
                            offset: Offset(8, 0),
                            spreadRadius: 2,
                            blurRadius: 16,
                          ),
                        ],
                      ),
                        margin: EdgeInsets.only(left: 20 / 360 * screenWidth),
                        child: Row(
                          mainAxisAlignment:
                          MainAxisAlignment.start,
                          children: <Widget>[
                            Center(
                              child: Text(
                                "23",
                                style: TextStyle(
                                  color: Colors.white,
                                  fontSize:
                                  12 / 360 * screenWidth,
                                  fontFamily:
                                  "IBMPlexSans",
                                  fontWeight: FontWeight.w500,
                                ),
                                textAlign: TextAlign.left,
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.only(left: 11 / 360 * screenWidth),
                              child: Row(
                                mainAxisAlignment:MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  Container(
                                    height:30 / 720 * screenHeight,
                                    width:30 / 360 * screenWidth,
                                    child: Image.asset(
                                      'assets/images/avatar-2.png',
                                      fit: BoxFit.contain,
                                    ),
                                  ),
                                  Container(
                                    margin: EdgeInsets.only(left: 8 / 360 * screenWidth),
                                    child: Column(
                                      mainAxisAlignment:
                                      MainAxisAlignment
                                          .center,
                                      crossAxisAlignment:
                                      CrossAxisAlignment
                                          .start,
                                      children: <Widget>[
                                        Text(
                                          "Harsh Thacker",
                                          style: TextStyle(
                                            color: Colors.white,
                                            fontSize: 14 /
                                                360 *
                                                screenWidth,
                                            fontFamily:
                                            "IBMPlexSans",
                                            fontWeight:
                                            FontWeight.w500,
                                          ),
                                          textAlign:
                                          TextAlign.left,
                                        ),
                                        Text(
                                          "Mumbai",
                                          style: TextStyle(
                                            color:
                                            SemanticColors
                                                .iceBlue,
                                            fontSize: 10 /
                                                360 *
                                                screenWidth,
                                            fontFamily:
                                            "IBMPlexSans",
                                            fontWeight:
                                            FontWeight.w500,
                                          ),
                                          textAlign:
                                          TextAlign.left,
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                  Expanded(
                  child: Container(
                      decoration: BoxDecoration(
                       
                      ),
                      child: SingleChildScrollView(
                        scrollDirection: Axis.horizontal,
                        child: Container(
                          height: 60 / 720 * screenHeight,
                          //margin: EdgeInsets.only(top: 15 / 720 * screenHeight),
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Container(
                                width: 15/ 360 * screenWidth,
                                margin: EdgeInsets.only(left: 42/360*screenWidth),
                                child: Center(
                                  child: Text(
                                    "99",
                                    style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 12 / 360 * screenWidth,
                                      fontFamily: "IBMPlexSans",
                                      fontWeight: FontWeight.w500,
                                    ),
                                    textAlign: TextAlign.left,
                                  ),
                                ),
                              ),
                              Container(
                                width: 15/ 360 * screenWidth,
                                margin: EdgeInsets.only(left: 42/360*screenWidth),
                                child: Center(
                                  child: Text(
                                    "10",
                                    style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 12 / 360 * screenWidth,
                                      fontFamily: "IBMPlexSans",
                                      fontWeight: FontWeight.w500,
                                    ),
                                    textAlign: TextAlign.left,
                                  ),
                                ),
                              ),
                              Container(
                                height: 90/720*screenHeight,
                                width: 90 / 360 * screenWidth,
                                margin: EdgeInsets.only(left: 30/360*screenWidth),
                                child: Center(
                                  child: Container(
                                      padding: EdgeInsets.symmetric(
                                          horizontal: 10 / 360 * screenWidth,
                                          vertical: 15 / 720 * screenHeight),
                                      child: Row(
                                        children: <Widget>[
                                          InkWell(
                                            onTap: _showPersBottomSheetCallBack,
                                            child: Container(
                                              height:30 / 720 * screenHeight,
                                              width:30 / 360 * screenWidth,
                                              margin: EdgeInsets.only(top: 2/720*screenHeight),
                                              child: new SvgPicture.asset(
                                                LeaderboardAssets.Amateur_DI_icon,
                                                fit: BoxFit.contain,
                                              ),
                                            ),
                                          ),
                                          Text(
                                            _selectedItem,
                                            style: TextStyle(fontSize: 30),
                                          ),
                                        ],
                                      )),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),



          ],
        ),
      ),

    );
  }


  void _selectItem(String name) {
    Navigator.pop(context);
    setState(() {
      _selectedItem = "";

    });
  }
  Widget buildList(BuildContext context, int index) {
    return Container(
      decoration: BoxDecoration(
        //  borderRadius: BorderRadius.circular(25),
        color: Colors.white,
      ),
      width: double.infinity,
      height: 50 / 720 * screenHeight,
      margin: EdgeInsets.only(bottom: 25 / 720 * screenHeight, top : 10 / 720 * screenHeight),
      //  padding: EdgeInsets.symmetric(vertical: 10, horizontal: 20),
      child: Row(
        children: <Widget>[
          Container(
            height: 50 / 720 * screenHeight,
            width: 50 / 360 * screenWidth,
            margin: EdgeInsets.only(left: 25/ 360 * screenWidth,),
            child: new SvgPicture.asset(
              //  LeaderboardAssets.Maestro_DI_icon,
              badgesList[index]['image'],
              fit: BoxFit.contain,
            ),
          ),
          SizedBox(
            width: 5 / 360 * screenWidth,
          ),
          Container(
            // margin: EdgeInsets.only(left: 20),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Container(
                  child: Text(badgesList[index]['name'],
                      style: TextStyle(
                          color: NeutralColors.dark_navy_blue,
                          fontSize: 14 / 360 * screenWidth,
                          fontFamily:"IBMPlexSans",
                          fontWeight: FontWeight.w500,
                          letterSpacing: .3)
                  ),
                ),
                Container(
                  child: Text(badgesList[index]['type'],
                      style: TextStyle(
                        color:NeutralColors.bluey_grey,
                        fontSize: 14 / 360 * screenWidth,
                        fontFamily: "IBMPlexSans",
                        fontWeight: FontWeight.w500,
                      )),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}

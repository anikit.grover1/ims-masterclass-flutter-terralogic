import 'package:flutter/material.dart';
import 'package:imsindia/api/bloc/services.dart';
import 'package:imsindia/components/custom_DropDown_Analytics.dart' as firstDropDown;
import 'package:imsindia/components/custom_DropDown_Analytics_Second.dart' as secondDropDown;
import 'package:imsindia/components/primary_button_gradient.dart';
import 'package:imsindia/utils/Url.dart';
import 'package:imsindia/utils/colors.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:imsindia/utils/global.dart' as global;
import 'package:imsindia/utils/svg_icons.dart';

var screenWidthTotal;
var screenHeightTotal;

var _safeAreaHorizontal;

var _safeAreaVertical;

var screenHeight;

var screenWidth;
var getLearningModules = [];
var getLearningObjects = [];
var getLearningModulesId = [];
var getLearningObjectsId = [];
var finalListForLearningModules = [];
var listForLearningObjectsId = [];
var selectedLearningModule;
var selectedLearningModuleName;
var selectedLearningObject;
var totalData1;
var notSelectedLearningModule = ["Select Test"];
var lastTestId;
var selectedLearningObjectId;
var menuId;
var courseID;

class StatesService {
  static final List<String> venues = [
    'simCAT Proctored',
    'simCAT Proctored1',
    'simCAT Proctored2',
    'simCAT Proctored3',

  ];
  static final List<String> venues1 = [
    'simCAT 01',
    'simCAT 02',
    'simCAT 03',
  ];
  static  getSuggestions(String query) {
    //List<String> matches = List();
    //matches.addAll(venues);

    // matches.retainWhere((s) => s.toLowerCase().contains(query.toLowerCase()));
    return finalListForLearningModules;
  }
  static  getSuggestions1(String query) {
    //List<String> matches = List();
    //finalListForLearningObjects.addAll(getLearningObjects);

    // matches.retainWhere((s) => s.toLowerCase().contains(query.toLowerCase()));
    return getLearningObjects;
  }
}


class Data {
  String questions;
  String overall;
  String dilr;
  String qa;
  String va;
  Data({
    this.questions,
    this.overall,
    this.dilr,
    this.qa,
    this.va,
  });
}

class AnalyticsScoreImprovement extends StatefulWidget {
  @override
  AnalyticsScoreImprovementState createState() =>
      AnalyticsScoreImprovementState();
}

class AnalyticsScoreImprovementState extends State<AnalyticsScoreImprovement> {

  var totalData;
  var totalDataSection;
  var totalDataScorecard;
  var totalDataScorecardSection;
  var totalDataScorecardStudentTestDetails;
  var overallCutoff;
  var getScoreDataSectionName = [];
  var getScoreDataSectionScore = [];
  var getScoreDataSectionId = [];
  var getScoreDataSectionCutOff = [];
  String course;
  var testIdImprove;
  var section;
  var getSectionName = ["OVERALL"];
  var getSectionId = [];
  var getAnalysisData = [];
  var questionsList = ["QUESTIONS","Current Score","Potential Score","Net Guessed Score","Misread","Misjudged","Miscalculated"];
  var getPotentialScore = [];
  var getQA = [];
  var getVARC = [];
  var getDILR = [];
  var getGuessed = [];
  var getMisread = [];
  var getMisjudged = [];
  var getMiscalculated = [];
  var getOverall = [0.0];
  var authorizationInvalidMsg;
  var retureValue;
  bool isLoading = true;
  bool noData = false;
  bool onClickChange = false;
  String dropdownValue = "simCAT Proctored";
  String dropdownValueSimCAT = "simCAT 01";
  final TextEditingController _stateOfVenues = new TextEditingController();

  final TextEditingController _stateOfVenues1 = new TextEditingController();
  //AnalyticsScorecardState scoreCardObj= new AnalyticsScorecardState();
  //var authorizationInvalidMsg;
  bool isLoadingDropDown = true;
  void getLists(Map t) async {
    if (global.headersWithAuthorizationKey['Authorization'] != '') {
      ApiService().getAPI(URL.ANALYTICS_API_LEARN_MODULE_LIST + courseID + "&id=" + menuId, t)
          .then((returnValue) {
        setState(() {
          print(returnValue);
          getLearningModules = [];
          getLearningModulesId = [];
          listForLearningObjectsId = [];
          finalListForLearningModules = [];
          if (returnValue[0] == 'Data fetched successfully') {
            isLoadingDropDown = true;
            totalData1=returnValue[1]['data'];
            lastTestId = returnValue[1]['lastTestId'];
            testIdImprove = lastTestId;
            //saveTestIdImprove(testId);
            print("&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&"+testIdImprove.toString());
            for (var index = 0; index < totalData1.length; index++) {
              if (totalData1[index]['componentType'] == "learningModule"){
                getLearningModules.add(totalData1[index]['component']['title']);
                getLearningModulesId.add(totalData1[index]['id']);
              }
            }
            for(var index = 0; index<totalData1.length;index++){
              if (totalData1[index]['componentType'] == "learningObject"){
                //finalListForLearningModules.add(totalData[index]['component']['title']);
                if(!listForLearningObjectsId.contains(totalData1[index]['parentId']))
                  listForLearningObjectsId.add(totalData1[index]['parentId']);
              }
            }
            for(var i = 0;i<totalData1.length;i++) {
              for (var index = 0; index <
                  listForLearningObjectsId.length; index++) {
                if (listForLearningObjectsId[index] ==
                    totalData1[i]['id']) {
                  finalListForLearningModules.add(
                      totalData1[i]['component']['title']);
                }
              }
            }
            if (lastTestId != null){
              getScoreCardData(t);
              for(var j = 0;j<totalData1.length;j++){
                if(lastTestId == totalData1[j]['id']){
                  selectedLearningObject = totalData1[j]['component']['title'];
                  selectedLearningModule = totalData1[j]['parentId'];
                  getSecondDropDownData(selectedLearningModule);
                  //print("Selected Learning Object"+ selectedLearningObject);
                  //print("Selected Learning Module"+ selectedLearningModule.toString());
                }
              }
              for(var index =0;index<totalData1.length;index++){
                if(selectedLearningModule == totalData1[index]['id']){
                  selectedLearningModuleName = totalData1[index]['component']['title'];
                  //print("Selected Learning Module"+ selectedLearningModuleName);
                }
              }
              //print("Selected Learning Module"+ selectedLearningModuleName);
              //print("Selected Learning Object"+ selectedLearningObject);
            }
            isLoadingDropDown = false;
          }else {
            authorizationInvalidMsg = returnValue[0];
          }
        });
      });
    }
  }

  void getSecondDropDownData(String selectedLearningModule){
    getLearningObjects = [];
    getLearningObjectsId = [];
    for (var i = 0; i < getLearningModulesId.length; i++) {
      for (var j = 0; j < totalData1.length; j++) {
        if (totalData1[j]['parentId'] == selectedLearningModule && totalData1[j]['componentType'] == "learningObject") {
          if(!getLearningObjects.contains(totalData1[j]['component']['title'])){
            getLearningObjects.add(totalData1[j]['component']['title']);
          }
          if(!getLearningObjectsId.contains(totalData1[j]['id'])){
            getLearningObjectsId.add(totalData1[j]['id']);
          }
        }
      }
    }
    //print("Hiiiiiiiiiiiiii"+ getLearningObjects.toString());
  }

  void getScoreCardData(Map t) async {
    if (global.headersWithAuthorizationKey['Authorization'] != '') {
      print("Course iddddddD"+courseID.toString());
      ApiService().getAPI(URL.ANALYTICS_API_SCORECARD_DATA + courseID + "&testId=" + testIdImprove, t)
          .then((returnValue) {
        setState(() {
          isLoading = true;
          retureValue = returnValue[0];
          print("*******************************************returnValue"+returnValue.toString());
          if(returnValue[0]!= 'something wrong'){
            if (returnValue[0] == 'Success') {
              totalDataScorecard=returnValue[1]['data'];
              //print("*******************************************returnValue"+totalDataScorecard.toString());
              getScoreDataSectionName = [];
              getScoreDataSectionScore = [];
              getScoreDataSectionCutOff = [];
              getScoreDataSectionId = [];
              totalDataScorecardSection = totalDataScorecard['sectionWise'];
              totalDataScorecardStudentTestDetails = totalDataScorecard['studentTestDetails'];
              getIsProtected = totalDataScorecard['testDetails']['component']['IsProctored'];
              getIsPerReport = totalDataScorecard['testDetails']['isPerReport'];
              print(totalDataScorecard['testDetails']['testTemplate']);
              if(totalDataScorecard['testDetails']['testTemplate']=="xat"){
                testTemplateForXat = true;
              }else{
                testTemplateForXat = false;
              }
              if(totalDataScorecard['testDetails']['testTemplate']=="nmat-adaptive"){
                  testTemplate = true;
              }else{
                testTemplate = false;
              }
              if(totalDataScorecard['testDetails']['testTemplate']=="nmat"){
                testTemplateForNonAdaptive = true;
              }else{
                testTemplateForNonAdaptive = false;
              }
              if(totalDataScorecard['testDetails']['testTemplate']=="tcat2new"){
                testTemplateForMICAT = true;
              }else{
                testTemplateForMICAT = false;
              }
              if(totalDataScorecard['testDetails']['testTemplate']=="cat2layout"){
                testTemplateForTISSMAT = true;
              }else{
                testTemplateForTISSMAT = false;
              }

              print("Section Protected "+getIsProtected.toString());
              print("Section  Students Report"+getIsPerReport.toString());
              //setSectionData_sharePref();
              if(totalDataScorecardSection != null){
                print("Total section length"+totalDataScorecardSection.length.toString());
                for (var index = 0; index < totalDataScorecardSection.length; index++) {
                  getScoreDataSectionName.add(totalDataScorecard['sectionWise'][index]['sectionName']);
                  getScoreDataSectionId.add(totalDataScorecard['sectionWise'][index]['sectionId']);
                  if(totalDataScorecard['sectionWise'][index]['score'] is int){
                    getScoreDataSectionScore.add(totalDataScorecard['sectionWise'][index]['score']);
                  }else if (totalDataScorecard['sectionWise'][index]['score'] is double) {
                    getScoreDataSectionScore.add(totalDataScorecard['sectionWise'][index]['score'].toInt());
                  }else if(testTemplateForXat == true){}else{
                    getScoreDataSectionScore.add(int.parse(totalDataScorecard['sectionWise'][index]['score']));
                  }
                  getScoreDataSectionCutOff.add(totalDataScorecard['sectionWise'][index]['cutoff']);
                }
                print("Section Name"+getScoreDataSectionName.toString());
                print("Section score"+getScoreDataSectionScore.toString());
                print("Section id"+getScoreDataSectionId.toString());
                print("Section cut off"+getScoreDataSectionCutOff.toString());
                global.getToken.then((t){
                  getScoreImprovementData(t);
                });
              }
              if(totalDataScorecardStudentTestDetails!=null){
                overallCutoff = totalDataScorecard['studentTestDetails']['cutoff'];
              }

              isLoading = false;
            } else {
              authorizationInvalidMsg = returnValue[0];

            }
          }

        });
      });
    }
  }
 var getIsProtected;
  var getIsPerReport;
  var testTemplate;
  var testTemplateForMICAT;
  var testTemplateForTISSMAT;
  var testTemplateForNonAdaptive;
  var testTemplateForXat;
  var getAnalysedCount;
  void getScoreImprovementData(Map t) async {
    if (global.headersWithAuthorizationKey['Authorization'] != '') {
      print("test iddddddD"+ testIdImprove.toString());
      print("Autorizationnnnnn"+t.toString());
      ApiService().getAPI(URL.ANALYTICS_API_SCORECARDIMPROVEMENT + testIdImprove, t)
          .then((returnValue) {
        if (this.mounted) {
          setState(() {
            isLoading = true;
            //print(returnValue);
            if (returnValue[0] == 'Success') {
              totalData = returnValue[1]['data'];
              print("Score" + totalData['sections'].toString());
              totalDataSection = totalData['sections'];
              getSectionName = [];
              getSectionId = [];
              getAnalysisData = [];
              getPotentialScore = [];
              getGuessed = [];
              getMisread = [];
              getMisjudged = [];
              getMiscalculated = [];
              getVARC = [];
              getDILR = [];
              getQA = [];
              getOverall = [];
              //print("Length"+totalData['sections'].length.toString());
              if (totalData['sections'] != null) {
                getIsProtected = totalData['test']['component']['IsProctored'];
                getIsPerReport = totalData['test']['isPerReport'];
              //   if(totalData['testDetails']['testTemplate']=="nmat-adaptive"){
              //     testTemplate = true;
              // }else{
              //   totalData = false;
              // }

                getAnalysedCount = totalData['overallData']['analysedCount'];
                for (var index = 0; index <
                    totalData['sections'].length; index++) {
                  getSectionName.add(
                      totalData['sections'][index]['sectionName']);
                  getSectionId.add(totalData['sections'][index]['sectionId']);
                  getAnalysisData.add(totalData['sections'][index]['analyis']);
                  getPotentialScore.add(
                      totalData['sections'][index]['analyis']['potentialScore']);
                  getGuessed.add(
                      totalData['sections'][index]['analyis']['guessed']);
                  getMisread.add(
                      totalData['sections'][index]['analyis']['misread']);
                  getMisjudged.add(
                      totalData['sections'][index]['analyis']['misjudged']);
                  getMiscalculated.add(
                      totalData['sections'][index]['analyis']['miscalculated']);
                  print("Section name" + getSectionName.toString());
                }
//                for (var index = 0; index < getSectionName.length; index++) {
//                  if (getSectionName.contains(
//                      "Verbal Ability & Reading Comprehension") ||
//                      getSectionName.contains(
//                          "Data Interpretation & Logical Reasoning") ||
//                      getSectionName.contains("Quantitative Ability")) {
//                    getSectionName[0] =
//                    "Verbal Ability & Reading Comprehension";
//                  }
//                  if (getSectionName.contains(
//                      "Data Interpretation & Logical Reasoning") ||
//                      getSectionName.contains("Quantitative Ability") ||
//                      getSectionName.contains(
//                          "Verbal Ability & Reading Comprehension")) {
//                    getSectionName[1] =
//                    "Data Interpretation & Logical Reasoning";
//                  }
//                  if (getSectionName.contains("Quantitative Ability") ||
//                      getSectionName.contains(
//                          "Verbal Ability & Reading Comprehension") ||
//                      getSectionName.contains(
//                          "Data Interpretation & Logical Reasoning")) {
//                    getSectionName[2] = "Quantitative Ability";
//                  }
//                }
                print("Section name again" + getSectionName.toString());
                if(testTemplateForXat == true){}else {
                  for (var index = 0; index < getSectionName.length; index++) {
                    if (getSectionName[index] ==
                        getSectionName[0]) {
                      getQA.add(totalData['sections'][index]['sectionName']);
                      getQA.add(getScoreDataSectionScore[index]);
                      getQA.add(
                          totalData['sections'][index]['analyis']['potentialScore'] +
                              getScoreDataSectionScore[index]);
                      getQA.add(
                          totalData['sections'][index]['analyis']['guessed']);
                      getQA.add(
                          totalData['sections'][index]['analyis']['misread']);
                      getQA.add(
                          totalData['sections'][index]['analyis']['misjudged']);
                      getQA.add(
                          totalData['sections'][index]['analyis']['miscalculated']);
                    }
                    if (getSectionName[index] == getSectionName[1]) {
                      getVARC.add(totalData['sections'][index]['sectionName']);
                      getVARC.add(getScoreDataSectionScore[index]);
                      getVARC.add(
                          totalData['sections'][index]['analyis']['potentialScore'] +
                              getScoreDataSectionScore[index]);
                      getVARC.add(
                          totalData['sections'][index]['analyis']['guessed']);
                      getVARC.add(
                          totalData['sections'][index]['analyis']['misread']);
                      getVARC.add(
                          totalData['sections'][index]['analyis']['misjudged']);
                      getVARC.add(
                          totalData['sections'][index]['analyis']['miscalculated']);
                    }
                    if (getSectionName[index] == getSectionName[2]) {
                      getDILR.add(totalData['sections'][index]['sectionName']);
                      getDILR.add(getScoreDataSectionScore[index]);
                      getDILR.add(
                          totalData['sections'][index]['analyis']['potentialScore'] +
                              getScoreDataSectionScore[index]);
                      getDILR.add(
                          totalData['sections'][index]['analyis']['guessed']);
                      getDILR.add(
                          totalData['sections'][index]['analyis']['misread']);
                      getDILR.add(
                          totalData['sections'][index]['analyis']['misjudged']);
                      getDILR.add(
                          totalData['sections'][index]['analyis']['miscalculated']);
                    }
                  }
                }
                print("Quality" + getQA.toString());
                print("Verbal" + getVARC.toString());
                print("Data" + getDILR.toString());
                getOverall.add(getScoreDataSectionScore.fold(0, (p, c) => p + c));
                getOverall.add(getPotentialScore.fold(0, (p, c) => p + c) + getOverall[0]);
                getOverall.add(getGuessed.fold(0, (p, c) => p + c));
                getOverall.add(getMisread.fold(0, (p, c) => p + c));
                getOverall.add(getMisjudged.fold(0, (p, c) => p + c));
                getOverall.add(getMiscalculated.fold(0, (p, c) => p + c));
//            print("getOverall"+ getOverall.toString());
//            print("section Name"+ getSectionName.toString());
//            print("section analysis"+ getAnalysisData.toString());
//            print("section analysis Potential score"+ getPotentialScore.toString());
                section = totalData['sections'];
//            print("Section"+section.toString());
                isLoading = false;
                noData = false;
              } else {
                noData = true;
                isLoading = false;
              }
            } else {
              authorizationInvalidMsg = returnValue[0];
            }
          });
        }
      });
    }
  }

  @override
  void initState() {
    getPotentialScore = [];
    getQA = [];
    getVARC = [];
    getDILR = [];
    getGuessed = [];
    getMisread = [];
    getMisjudged = [];
    getMiscalculated = [];
    getOverall = [];
    super.initState();
    setState(() {
      getCourseId();
    });
  }

  void getCourseId() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    courseID = prefs.getString("courseId");
    menuId = prefs.getString("moduleId");
    global.getToken.then((t){
      getLists(t);
    });

    //getCourseIdData();
  }
    void getCourseIdScorecard() async {
    print("&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&Coming getCourseId");
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      courseID = prefs.getString("courseId");
      //menuId = prefs.getString("moduleId");
      global.getToken.then((t){
        getScoreCardData(t);
      });
    });
  }
  saveTestIdImprove(String id) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('testIdImprove',id);
  }



  @override
  Widget build(BuildContext context) {
    var _mediaQueryData = MediaQuery.of(context);
    screenWidthTotal = _mediaQueryData.size.width;
    screenHeightTotal = _mediaQueryData.size.height;
    _safeAreaHorizontal =
        _mediaQueryData.padding.left + _mediaQueryData.padding.right;
    _safeAreaVertical =
        _mediaQueryData.padding.top + _mediaQueryData.padding.bottom;

    screenHeight = screenHeightTotal - _safeAreaVertical;
    screenWidth = screenWidthTotal - _safeAreaHorizontal;

    TrackingScrollController _scrollController;

    final data1 = Data(
      questions: "",
      overall: "",
      dilr: "",
      qa: "",
      va: "",
    );
    final data2 = Data(
      questions: "Pie-chart",
      overall: "12",
      dilr: "-1",
      qa: "10",
      va: "12",
    );

    final data3 = Data(
      questions: "Guessed",
      overall: "12",
      dilr: "-2",
      qa: "10",
      va: "12",
    );

    final data4 = Data(
      questions: "Misread",
      overall: "12",
      dilr: "-14",
      qa: "10",
      va: "12",
    );

    final data5 = Data(
      questions: "Misjudged",
      overall: "12",
      dilr: "10",
      qa: "10",
      va: "12",
    );
    final data6 = Data(
      questions: "Miscalculated",
      overall: "12",
      dilr: "10",
      qa: "10",
      va: "12",
    );

    final List<Data> datas = [
      data1,
      data2,
      data3,
      data4,
      data5,
      data6
    ];

    return Scaffold(
      
      body: authorizationInvalidMsg != null
          ? new Container(
//          margin: EdgeInsets.only(
//              top: (200.0 / 720) * screenHeight),
          child: Center(
              child: Text(
                authorizationInvalidMsg.toString(),
              )
          )
      )
          :isLoadingDropDown
          ? new Container(
          child:
          Center(child: CircularProgressIndicator()))
          :isLoadingDropDown == false   && lastTestId == null?
      Container(
//          margin: EdgeInsets.only(
//              top: (200.0 / 720) * screenHeight),
          child: Center(
              child: Text(
                "No Data",
              )
          )
      ):
          Container(
        //     height: screenHeight,
        color: Colors.white,
        child: Column(
          children: <Widget>[
            Container(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Container(
              //margin: EdgeInsets.only(top: 30/720*screenHeight),
              decoration: BoxDecoration(
                gradient: LinearGradient(
                  // Where the linear gradient begins and ends
                  begin: Alignment.topCenter,
                  end: Alignment.bottomCenter,
                  // Add one stop for each color. Stops should increase from 0 to 1
                  stops: [0.1, 0.5],
                  colors: [
                    // Colors are easy thanks to Flutter's Colors class.
                    NeutralColors.ice_blue,
                    Colors.white,
                  ],
                ),
                //     color: Color.fromARGB(255, 255, 255, 255),
                boxShadow: [
                  BoxShadow(
                    color: NeutralColors.ice_blue,
                    offset: Offset(0, 10),
                    blurRadius: 18,
                  ),
                ],
              ),

              //color: Colors.red,
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Container(
                    margin: EdgeInsets.only(top: 30/720*screenHeight,left: 20/360*screenWidth),
                    width: 220/360 * screenWidth,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Container(
                          child: Text(
                            selectedLearningModuleName != null?selectedLearningModuleName:"Last Taken Test",
                            overflow: TextOverflow.ellipsis,
                            style: TextStyle(
                              color: NeutralColors.dark_navy_blue,
                              fontSize: 18/720 * screenHeight,
                              fontFamily: "IBMPlexSans",
                              fontWeight: FontWeight.w700,
                            ),
                            textAlign: TextAlign.left,
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(bottom: 20/720 * screenHeight),
                          child: Text(
                            selectedLearningObject!=null?selectedLearningObject:"",
                            overflow: TextOverflow.ellipsis,
                            style: TextStyle(
                              color: NeutralColors.blue_grey,
                              fontSize: 14/720 * screenHeight,
                              fontFamily: "IBMPlexSans",
                              fontWeight: FontWeight.w500,
                            ),
                            textAlign: TextAlign.left,
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 30/720*screenHeight,right: 20/360*screenWidth,bottom: 20/720 * screenHeight),
                    //height: 40/720 * screenHeight,
                    width: 100/360*screenWidth,
                    child: PrimaryButtonGradient(

                      onTap: () {
                        setState(() {
                          (onClickChange == true)?
                          onClickChange = false:
                          onClickChange = true;
                        });
                        if(!onClickChange){
                          totalData = null;
                          //isLoading = false;
                          getQA = [];
                          getLearningModules = [];
                          if(testIdImprove != null){
                            global.getToken.then((t){
                              getScoreCardData(t);
                            });
                          }

                        }
                        //AppRoutes.push(context, AnalyticsScorecard());
                      },
                      text: (onClickChange)?'DONE':'CHANGE',
                    ),
                  ),
                ],
              ),
            ),
            (onClickChange)?
            Container(
              alignment: Alignment.topCenter,

              //  color:Colors.white,
              //height: 70/720*screenHeight,
              // width: screenWidth,
              decoration: BoxDecoration(
                boxShadow: [
                  BoxShadow(
                    blurRadius: 13.0,
                    color: Colors.black.withOpacity(.03),
                    offset: Offset(6.0, 7.0),
                    // spreadRadius: 500.0,
                  ),
                ],
                //shape: BoxShape.rectangle,
                //border: Border.all(),
                color: Colors.white,
              ),
              //    margin: EdgeInsets.only(right: screenWidth/19,left: screenWidth/19),
              child:Container(
                //  color: Colors.green,
                margin: EdgeInsets.only(right: 19/360*screenWidth,left: 19/360*screenWidth,top: 10/720*screenHeight, bottom: 26/720 * screenHeight),

                child: Row(

                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: <Widget>[
                    Expanded(child: Padding(
                      padding: EdgeInsets.only(right:10/360*screenWidth),
                      child:

                      firstDropDown.DropDownFormField(
                        getImmediateSuggestions: true,
                        textFieldConfiguration: firstDropDown.TextFieldConfiguration(
                          controller: _stateOfVenues,
                          label: selectedLearningModuleName != null?selectedLearningModuleName:"Select Value",

                        ),
                        suggestionsBoxDecoration: firstDropDown.SuggestionsBoxDecoration(
                            dropDownHeight: 150,
                            borderRadius: new BorderRadius.circular(5.0),
                            color: Colors.white),
                        suggestionsCallback: (pattern) {
                          return finalListForLearningModules;
                        },
                        itemBuilder: (context, suggestion) {
                          return ListTile(
                            dense: true,
                            contentPadding: EdgeInsets.only(left: 10/360*screenWidth),
                            title: Text(
                              suggestion,
                              style: TextStyle(
                                fontFamily: "IBMPlexSans",
                                fontWeight: FontWeight.w400,
                                fontSize: (14 / 720) * screenHeight,
                                color: NeutralColors.bluey_grey,
                                textBaseline: TextBaseline.alphabetic,
                                letterSpacing: 0.0,
                                inherit: false,
                              ),
                            ),
                          );
                        },
                        hideOnLoading: true,
                        debounceDuration: Duration(milliseconds: 100),
                        transitionBuilder: (context, suggestionsBox, controller) {
                          return suggestionsBox;
                        },
                        onSuggestionSelected: (suggestion) {
                          //print('Sangeethaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa'+suggestion);
                          _stateOfVenues.text = suggestion;

                          //print("=======>>>>"+_stateOfVenues.text);
                          setState(() {
                            //selectedLearningModule = suggestion;
                            for (var j = 0; j < totalData1.length; j++) {
                              if (totalData1[j]['component']['title'] == suggestion) {
                                selectedLearningModule = totalData1[j]['id'];
                              }
                            }
                            selectedLearningModuleName = suggestion;
                            selectedLearningObject = "Select Test";
                            _stateOfVenues1.text = "Select Test";
                            testIdImprove = null;
//                            if(selectedLearningObject!=null){
//                              selectedLearningObject = "Select test";
//                            }
                          });
                          getSecondDropDownData(selectedLearningModule);
                        },
                        onSaved: (value) => {print("something")},
                      ),
                    ),
                    ),
                    Expanded(
                      child: Padding(
                        padding: EdgeInsets.only(left:10/360*screenWidth),
                        child: secondDropDown.DropDownFormField(
                          hideOnEmpty: true,
                          getImmediateSuggestions: true,
                          textFieldConfiguration: secondDropDown.TextFieldConfiguration(
                            controller: _stateOfVenues1,
                            label: selectedLearningObject != null?selectedLearningObject:"Select Test",
                          ),
                          suggestionsBoxDecoration: secondDropDown.SuggestionsBoxDecoration(
                              dropDownHeight: getLearningObjects.length == 1?50:150,
                              borderRadius: new BorderRadius.circular(5.0),
                              color: Colors.white),
                          suggestionsCallback: (pattern) {
                            return getLearningObjects;
                          },
                          itemBuilder: (context, suggestion) {
                            return ListTile(
                              dense: true,
                              contentPadding: EdgeInsets.only(left: 10/360*screenWidth),
                              title: Text(
                                suggestion,
                                style: TextStyle(
                                  fontFamily: "IBMPlexSans",
                                  fontWeight: FontWeight.w400,
                                  fontSize: (14 / 720) * screenHeight,
                                  color: NeutralColors.bluey_grey,
                                  textBaseline: TextBaseline.alphabetic,
                                  letterSpacing: 0.0,
                                  inherit: false,
                                ),
                              ),
                            );
                          },
                          hideOnLoading: true,
                          debounceDuration: Duration(milliseconds: 100),
                          transitionBuilder: (context, suggestionsBox, controller) {
                            return suggestionsBox;
                          },
                          onSuggestionSelected: (suggestion) {
                            _stateOfVenues1.text = suggestion;
                            setState(() {
                              for (var j = 0; j < totalData1.length; j++) {
                                if (totalData1[j]['component']['title'] == suggestion) {
                                  selectedLearningObjectId = totalData1[j]['id'];
                                  testIdImprove = selectedLearningObjectId;
                                  //saveTestIdImprove(selectedLearningObjectId);
                                }
                              }
                              selectedLearningObject = suggestion;
                            });
                          },
                          onSaved: (value) => {print("something" + selectedLearningObjectId.toString())},
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ):Container(),
          ],
        )
      ),
            //simCATWidget(),
            Expanded(
                child: SingleChildScrollView(
                    child:  Column(
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: [
//                          Container(
//
//
//                            margin: EdgeInsets.only(right:20/360*screenWidth,left:20/360*screenWidth,top:25/720*screenHeight,bottom: 10/720*screenHeight ),
//                            decoration: BoxDecoration(
//
//                              gradient: LinearGradient(
//                                // Where the linear gradient begins and ends
//                                begin: Alignment.topCenter,
//                                end: Alignment.bottomCenter,
//                                // Add one stop for each color. Stops should increase from 0 to 1
//                                stops: [0.1, 0.5],
//                                colors: [
//                                  // Colors are easy thanks to Flutter's Colors class.
//                                  NeutralColors.ice_blue,
//                                  Colors.white,
//                                ],
//                              ),
//                              //     color: Color.fromARGB(255, 255, 255, 255),
//                              boxShadow: [
//                                BoxShadow(
//                                  color: NeutralColors.ice_blue,
//                                  offset: Offset(0, 10),
//                                  blurRadius: 18,
//                                ),
//                              ],
//                            ),
//                            // child: _getStackpage(false,context),
//                            child: _getChewiePlayer(false,context),
//                          ),
                          Container(
                            height: 1,
                            //margin: EdgeInsets.only(top: 20/720 * screenHeight),
                            decoration: BoxDecoration(
                              color: NeutralColors.dark_navy_blue,
                            ),
                            //  child: Container(),
                          ),
                         getQA.length == 0 && totalData == null && getLearningModules.length == 0 && retureValue != "something wrong"
                              ? new Container(
                              margin: EdgeInsets.only(
                                  top: (200.0 / 720) * screenHeight),
                              child:
                              Center(child: CircularProgressIndicator()))
                              :
                         retureValue == "something wrong" || totalDataSection == null || lastTestId == null || getAnalysedCount == 0 || (getIsProtected == true && getIsPerReport == true) ||  testTemplateForNonAdaptive == true?
                             Container(
                                 margin: EdgeInsets.only(top: 38/720 * screenHeight),
                                 child: Column(
                                   children: <Widget>[
                                     Center(
                                       child: getSvgIcon.analyticsSvgIcon,
                                     ),
                                     getIsProtected == true && getIsPerReport == true?
                                     Container(
                                       margin: EdgeInsets.only(top: 20/720 * screenHeight, left: 65/360 * screenWidth, right: 65/360 * screenWidth,bottom: 10/720 * screenHeight),
                                       child: Text(
                                         "Sorry",
                                         style: TextStyle(
                                           fontSize: 14/720 * screenHeight,
                                           fontFamily: "IBMPlexSans",
                                           fontWeight: FontWeight.w400,
                                           color: NeutralColors.gunmetal,
                                         ),
                                         textAlign: TextAlign.center,
                                       ),
                                     ):Container(),
                                     Container(
                                       margin: EdgeInsets.only(top: 20/720 * screenHeight, left: 65/360 * screenWidth, right: 65/360 * screenWidth,bottom: 10/720 * screenHeight),
                                       child: Text(
                                         getIsProtected == true && getIsPerReport == true?"This test is available at Proctored test centres only":testTemplate == true ? "Not applicable as the selected test is adaptive with no negative marking and all questions should be attempted." : testTemplateForNonAdaptive == true?"Not applicable as the selected test does not have negative marking and also all the questions should be attempted.": testTemplateForMICAT == true?"Not Available for the selected test.":testTemplateForXat == true?"Not applicable for the selected test":testTemplateForTISSMAT == true?"Not applicable for the selected test" :"You have not yet attempted the selected test",
                                         style: TextStyle(
                                           fontSize: 14/720 * screenHeight,
                                           fontFamily: "IBMPlexSans",
                                           fontWeight: FontWeight.w400,
                                           color: NeutralColors.gunmetal,
                                         ),
                                         textAlign: TextAlign.center,
                                       ),
                                     )
                                   ],
                                 )
                             ):
                             Container(
                               //width: 360/360*screenWidth,
                               child: Row(
                                 children: <Widget>[
                                   Container(
                                     decoration: BoxDecoration(
                                       color: Color.fromARGB(255, 255, 255, 255),
                                       boxShadow: [
                                         BoxShadow(
                                           color: Color.fromARGB(61, 193, 188, 164),
                                           offset: Offset(10, 0),
                                           blurRadius: 22,
                                         ),
                                       ],
                                     ),
                                     width: screenWidth / 3,
                                     child:Column(
                                       children: <Widget>[
                                         Container(
                                           child: ListView.builder(
                                               addRepaintBoundaries: true,
                                               addAutomaticKeepAlives: true,
                                               controller: _scrollController,
                                               shrinkWrap:true,
                                               physics: NeverScrollableScrollPhysics(),
                                               itemCount: questionsList.length,
                                               itemBuilder: (context, index) {
                                                 //final item = datas[index];
                                                 Color color;
                                                 if (index % 2 == 0) {
                                                   color = null;
                                                 } else if (index % 2 == 1)   {
                                                   color = NeutralColors.sun_yellow.withOpacity(0.1);
                                                 }
                                                 if (index == 0) {
                                                   return Container(
                                                     color: Colors.white,
                                                     child: Row(
                                                       mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                                       mainAxisSize: MainAxisSize.max,
                                                       children: <Widget>[
                                                         Container(
                                                           width: 85/360 * screenWidth,
                                                           margin: EdgeInsets.only(left: 10/360 * screenWidth,top: 18/720 * screenHeight, bottom: 24/720 * screenHeight),
                                                           child: Text(
                                                             questionsList[index],
                                                             overflow: TextOverflow.ellipsis,
                                                             style: TextStyle(
                                                               fontSize: 10/720 * screenHeight,
                                                               fontFamily: "IBMPlexSans-Medium",
                                                               fontWeight: FontWeight.w500,
                                                               color: NeutralColors.blue_grey,
                                                             ),
                                                             textAlign: TextAlign.left,
                                                           ),
                                                         ),
                                                       ],
                                                     ),
                                                     padding: EdgeInsets.all(10.0),
                                                   );
                                                 }
                                                 return Container(
                                                   color: color,
                                                   child: Row(
                                                     mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                                     mainAxisSize: MainAxisSize.max,
                                                     children: <Widget>[
                                                       Container(
                                                         width: 85/360 * screenWidth,
                                                         margin: EdgeInsets.only(left: 10/360 * screenWidth,top: 15/720 * screenHeight, bottom: 15/720 * screenHeight),
                                                         child: Text(
                                                           questionsList[index],
                                                           overflow: TextOverflow.ellipsis,
                                                           style: TextStyle(
                                                             fontSize: 12/720 * screenHeight,
                                                             fontFamily: "IBMPlexSans",
                                                             color: NeutralColors.dark_navy_blue,
                                                           ),
                                                           textAlign: TextAlign.left,
                                                         ),
                                                       ),
                                                     ],
                                                   ),
                                                   padding: EdgeInsets.all(10.0),
                                                 );
                                               }
                                           ),
                                         ),
                                       ],
                                     ),
                                   ),
                                   Container(
                                     width: 240/360*screenWidth,
                                     child:Column(
                                       children: <Widget>[
                                         Container(
                                           child: Scrollbar(
                                             //ListView
                                             child: SingleChildScrollView(
                                               scrollDirection: Axis.horizontal,
                                               child: Container(
                                                 width: 500 / 360 * screenWidth,
                                                 child: ListView.builder(
                                                   shrinkWrap: true,
                                                   controller: _scrollController,
                                                   scrollDirection: Axis.vertical,
                                                   physics: NeverScrollableScrollPhysics(),
                                                   itemCount: getQA == null ? 1 : getQA.length,
                                                   itemBuilder: (context, index) {
                                                    // final item = datas[index];
                                                     //print("Second Index"+ index.toString());
                                                     Color color;
                                                     Color cutoffColor;
                                                     if (index % 2 == 0) {
                                                       color = null;
                                                     } else if (index % 2 == 1)   {
                                                       color = NeutralColors.sun_yellow.withOpacity(0.1);
                                                     }
                                                     if(index == 1 || index == 2){
                                                       cutoffColor = const Color(0xFF48c100);
                                                     }else{
                                                       cutoffColor = NeutralColors.gunmetal;
                                                     }
                                                     if (index == 0){
                                                       return Container(
                                                         height: 80/720 * screenHeight,
                                                         child: Row(
                                                           mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                                           mainAxisSize: MainAxisSize.max,
                                                           children: <Widget>[
                                                             Container(
                                                               width: 60/360 * screenWidth,
                                                               margin: EdgeInsets.only(left: 25/360 * screenWidth,top: 10/720 * screenHeight,bottom: 10/720 * screenHeight),
                                                               child: Row(
                                                                 crossAxisAlignment: CrossAxisAlignment.start,
                                                                 children: [
                                                                   Text(
                                                                     "OVERALL",
                                                                     style: TextStyle(
                                                                       fontSize: 10/720 * screenHeight,
                                                                       fontFamily: "IBMPlexSans-Medium",
                                                                       fontWeight: FontWeight.w500,
                                                                       color: NeutralColors.blue_grey,
                                                                     ),
                                                                     textAlign: TextAlign.center,
                                                                   ),
//                                                                Container(
//                                                                  width: 8/360 * screenWidth,
//                                                                  height: 5/720 * screenHeight,
//                                                                  margin: EdgeInsets.only(left: 5/360 * screenWidth, top: 4/720 * screenHeight),
//                                                                  child: Image.asset(
//                                                                    "assets/images/path1.png",
//                                                                    fit: BoxFit.none,
//                                                                  ),
//                                                                ),
                                                                 ],
                                                               ),
                                                             ),
                                                             Container(
                                                               width: 100/360 * screenWidth,
                                                               margin: EdgeInsets.only(left: 10/360 * screenWidth,top: 10/720 * screenHeight,bottom: 10/720 * screenHeight),
                                                               child:Row(
                                                                   crossAxisAlignment: CrossAxisAlignment.start,
                                                                   children: [
                                                                     Container(
                                                                       width: 85/360 * screenWidth,
                                                                       child: Text(
                                                                         getSectionName[0].toString(),
                                                                         style: TextStyle(
                                                                           fontSize: 10/720 * screenHeight,
                                                                           fontFamily: "IBMPlexSans-Medium",
                                                                           fontWeight: FontWeight.w500,
                                                                           color: NeutralColors.blue_grey,
                                                                         ),
                                                                         textAlign: TextAlign.center,
                                                                       ),
                                                                     ),
//                                                                  Container(
//                                                                    width: 8/360 * screenWidth,
//                                                                    height: 5/720 * screenHeight,
//                                                                    margin: EdgeInsets.only(left: 5/360 * screenWidth, top: 4/720 * screenHeight),
//                                                                    child: Image.asset(
//                                                                      "assets/images/path1.png",
//                                                                      fit: BoxFit.none,
//                                                                    ),
//                                                                  ),
                                                                   ]
                                                               ),
                                                             ),
                                                             Container(
                                                               width: 100/360 * screenWidth,
                                                               margin: EdgeInsets.only(left: 10/360 * screenWidth,top: 10/720 * screenHeight,bottom: 10/720 * screenHeight),
                                                               child:Row(
                                                                 crossAxisAlignment: CrossAxisAlignment.start,
                                                                 children: <Widget>[
                                                                   Container(
                                                                     width: 85/360 * screenWidth,
                                                                     child: Text(
                                                                       getSectionName[1].toString(),
                                                                       style: TextStyle(
                                                                         fontSize: 10/720 * screenHeight,
                                                                         fontFamily: "IBMPlexSans-Medium",
                                                                         fontWeight: FontWeight.w500,
                                                                         color: NeutralColors.blue_grey,
                                                                       ),
                                                                       textAlign: TextAlign.center,
                                                                     ),
                                                                   ),
//                                                                Container(
//                                                                  width: 8/360 * screenWidth,
//                                                                  height: 5/720 * screenHeight,
//                                                                  margin: EdgeInsets.only(left: 5/360 * screenWidth, top: 4/720 * screenHeight),
//                                                                  child: Image.asset(
//                                                                    "assets/images/path1.png",
//                                                                    fit: BoxFit.none,
//                                                                  ),
//                                                                ),
                                                                 ],
                                                               ),
                                                             ),
                                                             Container(
                                                               width: 100/360 * screenWidth,
                                                               margin: EdgeInsets.only(left: 10/360 * screenWidth,top: 10/720 * screenHeight,bottom: 10/720 * screenHeight),
                                                               child:Row(
                                                                 crossAxisAlignment: CrossAxisAlignment.start,
                                                                 children: <Widget>[
                                                                   Container(
                                                                     width: 85/360 * screenWidth,
                                                                     child: Text(
                                                                       getSectionName[2].toString(),
                                                                       style: TextStyle(
                                                                         fontSize: 10/720 * screenHeight,
                                                                         fontFamily: "IBMPlexSans-Medium",
                                                                         fontWeight: FontWeight.w500,
                                                                         color: NeutralColors.blue_grey,
                                                                       ),
                                                                       textAlign: TextAlign.center,
                                                                     ),
                                                                   ),
//                                                                Container(
//                                                                  width: 8/360 * screenWidth,
//                                                                  height: 5/720 * screenHeight,
//                                                                  margin: EdgeInsets.only(left: 5/360 * screenWidth, top: 4/720 * screenHeight),
//                                                                  child: Image.asset(
//                                                                    "assets/images/path1.png",
//                                                                    fit: BoxFit.none,
//                                                                  ),
//                                                                ),
                                                                 ],
                                                               ),
                                                             ),
                                                           ],
                                                         ),
                                                         padding: EdgeInsets.all(10.0),
                                                       );
                                                     }
                                                     return Container(
                                                       color: color,
                                                       child: Column(
                                                         mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                                         mainAxisSize: MainAxisSize.max,
                                                         children: <Widget>[
                                                           Container(
                                                             child: Row(
                                                               mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                                               children: <Widget>[
                                                                 Container(
                                                                   width: 50/360 * screenWidth,
                                                                   margin: EdgeInsets.only(left: 10/360 * screenWidth,top: 15/720 * screenHeight,bottom: 15/720 * screenHeight),
                                                                   child: Text(
                                                                     getOverall[index-1].round().toString(),
                                                                     style: TextStyle(
                                                                       fontSize: 12/720 * screenHeight,
                                                                       fontFamily: "IBMPlexSans",
                                                                       fontWeight: FontWeight.w500,
                                                                       color: overallCutoff == 0 ? NeutralColors.gunmetal : getOverall[index-1] >= overallCutoff ? cutoffColor : NeutralColors.gunmetal,
                                                                     ),
                                                                     textAlign: TextAlign.center,
                                                                   ),
                                                                 ),
                                                                 Container(
                                                                   width: 90/360 * screenWidth,
                                                                   margin: EdgeInsets.only(left: 10/360 * screenWidth,top: 15/720 * screenHeight,bottom: 15/720 * screenHeight),
                                                                   child: Text(
                                                                     getQA[index].round().toString(),
                                                                     style: TextStyle(
                                                                       fontSize: 12/720 * screenHeight,
                                                                       fontFamily: "IBMPlexSans",
                                                                       fontWeight: FontWeight.w500,
                                                                       color: getScoreDataSectionCutOff[0] == 0 ? NeutralColors.gunmetal :getQA[index]>= getScoreDataSectionCutOff[0]?cutoffColor:NeutralColors.gunmetal,
                                                                     ),
                                                                     textAlign: TextAlign.center,
                                                                   ),
                                                                 ),
                                                                 Container(
                                                                   width: 90/360 * screenWidth,
                                                                   margin: EdgeInsets.only(left: 10/360 * screenWidth,top: 15/720 * screenHeight,bottom: 15/720 * screenHeight),
                                                                   child: Text(
                                                                     getVARC[index].round().toString(),
                                                                     style: TextStyle(
                                                                       fontSize: 12/720 * screenHeight,
                                                                       fontFamily: "IBMPlexSans",
                                                                       fontWeight: FontWeight.w500,
                                                                       color: getScoreDataSectionCutOff[0] == 0 ? NeutralColors.gunmetal :getVARC[index]>= getScoreDataSectionCutOff[0]?cutoffColor:NeutralColors.gunmetal,
                                                                     ),
                                                                     textAlign: TextAlign.center,
                                                                   ),
                                                                 ),
                                                                 Container(
                                                                   width: 90/360 * screenWidth,
                                                                   margin: EdgeInsets.only(left: 10/360 * screenWidth,top: 15/720 * screenHeight,bottom: 15/720 * screenHeight),
                                                                   child: Text(
                                                                     getDILR[index].round().toString(),
                                                                     style: TextStyle(
                                                                       fontSize: 12/720 * screenHeight,
                                                                       fontFamily: "IBMPlexSans",
                                                                       fontWeight: FontWeight.w500,
                                                                       color: getScoreDataSectionCutOff[0] == 0 ? NeutralColors.gunmetal :getDILR[index]>= getScoreDataSectionCutOff[0]?cutoffColor:NeutralColors.gunmetal,
                                                                     ),
                                                                     textAlign: TextAlign.center,
                                                                   ),
                                                                 ),
                                                               ],
                                                             ),
                                                           ),
                                                         ],
                                                       ),
                                                       padding: EdgeInsets.all(10.0),
                                                     );
                                                   },
                                                 ),
                                               ),
                                             ),
                                           ),
                                         ),
                                       ],
                                     ),
                                   ),
                                 ],
                               ),
                             ),

                        ]
                    )
                )
            ),
          ],
        ),
      ),
    );
  }
}

//_getStackpage(bool offStageFlag,BuildContext context){
//  Offstage offstage = new Offstage(
//      offstage: offStageFlag,
//      child:  ChewieListItem(
//        videoPlayerController: VideoPlayerController.network(
//          'https://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4',),
//      )
//  );
//  return offstage;
//}

//_getChewiePlayer(bool offStageFlag, BuildContext context){
//  Offstage offstage = new Offstage(
//    offstage: offStageFlag,
//    child: Container(
//      decoration:  BoxDecoration(
//          borderRadius: new BorderRadius.circular(8.0/360*screenWidth)),
////      margin: EdgeInsets.only(
////          left: (20 / 360) * screenWidth, right: (20 / 360) * screenWidth),
//      height: 181 / 720 * screenHeight,
//      child: InkWell(
//        onTap: () {
//          AppRoutes.push(context,VideoPlayerApp());
//        },
//        child: Container(
//
//          constraints: new BoxConstraints.expand(
//            height: (181 / 720) * screenHeight,
//
//          ),
//          alignment: Alignment.bottomLeft,
//          padding: new EdgeInsets.only(bottom: 10.0/720*screenHeight),
//          decoration:  BoxDecoration(
//            borderRadius: new BorderRadius.circular(8.0/360*screenWidth),
//            image: DecorationImage(
//                alignment: Alignment(-.2, 0),
//                image: AssetImage("./assets/images/videoplaceholder.png"),
////                  image: NetworkImage(
////                      'http://d1cn01kw45hc6h.cloudfront.net/wp-content/uploads/2019/08/22161210/Quadtratic.png'
////                  ),
//                fit: BoxFit.cover),
//          ),
//          child: Center(
//            child: Container(
//              height: (50 / 720) * screenHeight,
//              width: (50 / 360) * screenWidth,
//              child: SvgPicture.asset(
//                getPrepareSvgImages.playwhite,
//                fit: BoxFit.fitHeight,
//              ),
//            ),
//          ),
//        ),
//      ),
//    ),
//  );
//  return offstage;
//}

import 'package:flutter/material.dart';
import 'package:imsindia/views/Analytics/analytic_simcatWidget.dart';
import 'package:imsindia/utils/colors.dart';
import '../../routers/routes.dart';

var screenWidthTotal;
var screenHeightTotal;

var _safeAreaHorizontal;

var _safeAreaVertical;

var screenHeight;

var screenWidth;
//class StatesService {
//  static final List<String> venues = [
//    'simCAT Proctored',
//    'simCAT Proctored1',
//    'simCAT Proctored2',
//    'simCAT Proctored3',
//
//  ];
//  static final List<String> venues1 = [
//    'simCAT 01',
//    'simCAT 02',
//    'simCAT 03',
//  ];
//  static List<String> getSuggestions(String query) {
//    List<String> matches = List();
//    matches.addAll(venues);
//
//    // matches.retainWhere((s) => s.toLowerCase().contains(query.toLowerCase()));
//    return matches;
//  }
//  static List<String> getSuggestions1(String query) {
//    List<String> matches = List();
//    matches.addAll(venues1);
//
//    // matches.retainWhere((s) => s.toLowerCase().contains(query.toLowerCase()));
//    return matches;
//  }
//}
class analyis {
  String sectionName;
  String overall;
  String dilr;
  String qa;
  String va;
  analyis ({
    this.sectionName,
    this.overall,
    this.dilr,
    this.qa,
    this.va,
  });
}
class Section {
  String sectionName;
  Section({
    this.sectionName,
});
}

class Analyticdemo extends StatefulWidget {
  @override
  AnalyticdemoState createState()=> AnalyticdemoState();
}

class AnalyticdemoState extends State<Analyticdemo> {
  bool onClickChange = false;
  String dropdownValue = "simCAT Proctored";
  String dropdownValueSimCAT = "simCAT 01";

  final TextEditingController _stateOfVenues =
  new TextEditingController();

  final TextEditingController _stateOfVenues1 =
  new TextEditingController();
  @override
  Future<bool> _onWillPop() {
    AppRoutes.pop((context));
    setState(() {
    });

  }

  @override
  Widget build(BuildContext context) {
    var _mediaQueryData = MediaQuery.of(context);
    screenWidthTotal = _mediaQueryData.size.width;
    screenHeightTotal = _mediaQueryData.size.height;
    _safeAreaHorizontal =
        _mediaQueryData.padding.left + _mediaQueryData.padding.right;
    _safeAreaVertical =
        _mediaQueryData.padding.top + _mediaQueryData.padding.bottom;

    screenHeight = screenHeightTotal - _safeAreaVertical;
    screenWidth = screenWidthTotal - _safeAreaHorizontal;

    TrackingScrollController _scrollController;

    final data1 = analyis(
      overall: "",
      dilr: "",
      qa: "",
      va: "",
    );
    final data2 = analyis(
      sectionName: "Pie-chart",
      overall: "",
      dilr: "-1",
      qa: "10",
      va: "12",
    );

    final data3 = analyis(
      sectionName: "Guessed",
      overall: "",
      dilr: "-2",
      qa: "10",
      va: "12",
    );

    final data4 = analyis(
      sectionName: "Misread",
      overall: "",
      dilr: "-14",
      qa: "10",
      va: "12",
    );

    final data5 =analyis(
      sectionName: "Misjudged",
      overall: "",
      dilr: "10",
      qa: "10",
      va: "12",
    );
    final data6 =analyis(
      overall: "",
      dilr: "10",
      qa: "10",
      va: "12",
    );

    final List<analyis> datas = [
      data1,
      data2,
      data3,
      data4,
      data5,
      data6
    ];
    final datasection1= Section(
      sectionName: "",
    );
    final datasection2= Section(
      sectionName: "OVERALL",
    );
    final datasection3= Section(
      sectionName: "Quantitative Ability",
    );
    final datasection4= Section(
      sectionName: "Verbal Ability and Reading Comprehension",
    );
    final datasection5= Section(
      sectionName: "Data Interpretation & Logical Resoning",
    );

    final List<Section> scetions =[
      datasection1,
      datasection2,
      datasection3,
      datasection4,
      datasection5

    ];
    return Scaffold(
      
      body: Container(
        //     height: screenHeight,
        color: Colors.white,
        child: Column(
          children: <Widget>[
            //simCAT(context),
            simCATWidget(),
            Expanded(
                child: SingleChildScrollView(
                    child:  Column(
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: [
                          Container(


                            margin: EdgeInsets.only(right:20/360*screenWidth,left:20/360*screenWidth,top:25/720*screenHeight,bottom: 10/720*screenHeight ),
                            decoration: BoxDecoration(

                              gradient: LinearGradient(
                                // Where the linear gradient begins and ends
                                begin: Alignment.topCenter,
                                end: Alignment.bottomCenter,
                                // Add one stop for each color. Stops should increase from 0 to 1
                                stops: [0.1, 0.5],
                                colors: [
                                  // Colors are easy thanks to Flutter's Colors class.
                                  NeutralColors.ice_blue,
                                  Colors.white,
                                ],
                              ),
                              //     color: Color.fromARGB(255, 255, 255, 255),
                              boxShadow: [
                                BoxShadow(
                                  color: NeutralColors.ice_blue,
                                  offset: Offset(0, 10),
                                  blurRadius: 18,
                                ),
                              ],
                            ),
                            // child: _getStackpage(false,context),
                           // child: _getChewiePlayer(false,context),
                          ),
                          Container(
                            height: 1,
                            margin: EdgeInsets.only(top: 20/720 * screenHeight),
                            decoration: BoxDecoration(
                              color: Color.fromARGB(255, 0, 3, 44),
                            ),
                            //  child: Container(),
                          ),
                          Container(
                            // height: 320 / 720 * screenHeight,
                           // width: 360/360*screenWidth,
                            // color:Colors.red,
                            child: Row(
                              children: <Widget>[
                                Container(
                                  //  color:Colors.red,
                                  decoration: BoxDecoration(
                                    color: Color.fromARGB(255, 255, 255, 255),
                                    boxShadow: [
                                      BoxShadow(
                                        color: Color.fromARGB(61, 193, 188, 164),
                                        offset: Offset(10, 0),
                                        blurRadius: 22,
                                      ),
                                    ],
                                  ),
                                  // // margin: EdgeInsets.only(right: screenWidth/2, top:2),
                                  // height:screenHeight ,
                                  width: screenWidth / 3,
                                  child:Column(
                                    children: <Widget>[
//                        Container(
//                          height: 1 / 720 * screenHeight,
//                          //margin: EdgeInsets.only(bottom: 1 / 720 * screenHeight),
//                          decoration: BoxDecoration(
//                            color: Color.fromARGB(255, 244, 244, 244),
//                          ),
//                          child: Container(),
//                        ),

                                      Container(
                                        // height: 320/720*screenHeight,
                                        color: Colors.white,
                                        child: ListView.builder(
                                            addRepaintBoundaries: true,
                                            addAutomaticKeepAlives: true,
                                            controller: _scrollController,
                                            shrinkWrap:true,
                                            physics: NeverScrollableScrollPhysics(),
                                            itemCount: datas.length,
                                            itemBuilder: (context, index) {
                                              final item = datas[index];
                                              // ignore: unused_local_variable
                                              Color color;
                                              if (index % 2 == 0) {
                                                color = null;
                                              } else if (index % 2 == 1)   {
                                                color = NeutralColors.sun_yellow.withOpacity(0.1);
                                              }
                                              if (index == 0) {
                                                return Container(
                                                  //height: 50/720 * screenHeight,
                                                  color: Colors.white,
                                                  child: Row(
                                                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                                    mainAxisSize: MainAxisSize.max,
                                                    children: <Widget>[
                                                      Container(
                                                        //width: 70/360 * screenWidth,
                                                        margin: EdgeInsets.only(left: 10/360 * screenWidth),
                                                        child: Text(
                                                          "QUESTIONS",
                                                          style: TextStyle(
                                                            fontSize: 10/720 * screenHeight,
                                                            fontFamily: "IBMPlexSans-Medium",
                                                            color: const Color(0xFF7e919a),
                                                          ),
                                                          textAlign: TextAlign.left,
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                  padding: EdgeInsets.all(10.0),
                                                );
                                              }
                                              return  Container(
                                                //height: 50/720 * screenHeight,
                                                color: color,
                                                child: Row(
                                                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                                  mainAxisSize: MainAxisSize.max,
                                                  children: <Widget>[
                                                    Container(
                                                      width: 70/360 * screenWidth,
                                                      //margin: EdgeInsets.only(left: 10/360 * screenWidth),
                                                      child: Text(
                                                        item.sectionName,
                                                        style: TextStyle(
                                                          fontSize: 11.8/720 * screenHeight,
                                                          fontFamily: "IBMPlexSans",
                                                          color: const Color(0xFF00022c),
                                                        ),
                                                        textAlign: TextAlign.center,
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                                padding: EdgeInsets.all(10.0),
                                              );
                                            }),
                                      ),
                                    ],
                                  ),
                                ),
                                Container(

                                  // height: 350 / 720 * screenHeight,
                                  width: 240/360*screenWidth,
                                  //margin:EdgeInsets.only(left: 70/360*screenWidth),
                                  child:Column(
                                    children: <Widget>[

                                      Container(
                                        //   //   gradient: LinearGradient(
                                        //     begin: FractionalOffset.topCenter,
                                        //   //  end: FractionalOffset.topLeft,
                                        //     colors: [
                                        //       Colors.grey,
                                        //     Colors.white,
                                        //     ],
                                        //     stops: [0.95, 0],
                                        //   ),
                                        // ), height: 310/720*screenHeight,
                                        // decoration: BoxDecoration(


                                        child: Scrollbar(
                                          //ListView
                                          child: SingleChildScrollView(
                                            scrollDirection: Axis.horizontal,
                                            child: Container(
                                              width: 300 / 360 * screenWidth,
                                              //margin: EdgeInsets.only(left:70/360*screenWidth),


                                              child: ListView.builder(
                                                shrinkWrap: true,
                                                controller: _scrollController,
                                                scrollDirection: Axis.vertical,
                                                physics: NeverScrollableScrollPhysics(),
                                                itemCount: datas == null ? 1 : datas.length,
                                                itemBuilder: (context, index) {
                                                  final item = datas[index];
                                                  Color color;
                                                  if (index % 2 == 0) {
                                                    color = null;
                                                  } else if (index % 2 == 1)   {
                                                    color = NeutralColors.sun_yellow.withOpacity(0.1);
                                                  }
                                                  if (index == 0){
                                                    return Container(
                                                      //height: 50/720 * screenHeight,
                                                      color: null,
                                                      child: Row(
                                                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                                        mainAxisSize: MainAxisSize.max,
                                                        children: <Widget>[
                                                          Container(
                                                            width: 60/360 * screenWidth,
                                                            margin: EdgeInsets.only(left: 10/360 * screenWidth),
                                                            child: Row(
                                                              crossAxisAlignment: CrossAxisAlignment.start,
                                                              children: [
                                                                Text(
                                                                  "OVERALL",
                                                                  style: TextStyle(
                                                                    fontSize: 10/720 * screenHeight,
                                                                    fontFamily: "IBMPlexSans-Medium",
                                                                    color: const Color(0xFF7e919a),
                                                                  ),
                                                                  textAlign: TextAlign.center,
                                                                ),
                                                                Container(
                                                                  width: 8/360 * screenWidth,
                                                                  height: 5/720 * screenHeight,
                                                                  margin: EdgeInsets.only(left: 5/360 * screenWidth, top: 4/720 * screenHeight),
                                                                  child: Image.asset(
                                                                    "assets/images/path1.png",
                                                                    fit: BoxFit.none,
                                                                  ),
                                                                ),
                                                              ],
                                                            ),
                                                          ),
                                                          Container(
                                                            width: 50/360 * screenWidth,
                                                            margin: EdgeInsets.only(left: 10/360 * screenWidth),
                                                            child:Row(
                                                                crossAxisAlignment: CrossAxisAlignment.start,
                                                                children: [
                                                                  Text(
                                                                    "DI-LR",
                                                                    style: TextStyle(
                                                                      fontSize: 10/720 * screenHeight,
                                                                      fontFamily: "IBMPlexSans-Medium",
                                                                      color: const Color(0xFF7e919a),
                                                                    ),
                                                                    textAlign: TextAlign.center,
                                                                  ),
                                                                  Container(
                                                                    width: 8/360 * screenWidth,
                                                                    height: 5/720 * screenHeight,
                                                                    margin: EdgeInsets.only(left: 5/360 * screenWidth, top: 4/720 * screenHeight),
                                                                    child: Image.asset(
                                                                      "assets/images/path1.png",
                                                                      fit: BoxFit.none,
                                                                    ),
                                                                  ),
                                                                ]
                                                            ),
                                                          ),
                                                          Container(
                                                            width: 30/360 * screenWidth,
                                                            margin: EdgeInsets.only(left: 10/360 * screenWidth),
                                                            child:Row(
                                                              crossAxisAlignment: CrossAxisAlignment.start,
                                                              children: <Widget>[
                                                                Text(
                                                                  "QA",
                                                                  style: TextStyle(
                                                                    fontSize: 10/720 * screenHeight,
                                                                    fontFamily: "IBMPlexSans-Medium",
                                                                    color: const Color(0xFF7e919a),
                                                                  ),
                                                                  textAlign: TextAlign.center,
                                                                ),
                                                                Container(
                                                                  width: 8/360 * screenWidth,
                                                                  height: 5/720 * screenHeight,
                                                                  margin: EdgeInsets.only(left: 5/360 * screenWidth, top: 4/720 * screenHeight),
                                                                  child: Image.asset(
                                                                    "assets/images/path1.png",
                                                                    fit: BoxFit.none,
                                                                  ),
                                                                ),
                                                              ],
                                                            ),
                                                          ),
                                                          Container(
                                                            width: 30/360 * screenWidth,
                                                            margin: EdgeInsets.only(left: 10/360 * screenWidth),
                                                            child:Row(
                                                              crossAxisAlignment: CrossAxisAlignment.start,
                                                              children: <Widget>[
                                                                Text(
                                                                  "VA",
                                                                  style: TextStyle(
                                                                    fontSize: 10/720 * screenHeight,
                                                                    fontFamily: "IBMPlexSans-Medium",
                                                                    color: const Color(0xFF7e919a),
                                                                  ),
                                                                  textAlign: TextAlign.center,
                                                                ),
                                                                Container(
                                                                  width: 8/360 * screenWidth,
                                                                  height: 5/720 * screenHeight,
                                                                  margin: EdgeInsets.only(left: 5/360 * screenWidth, top: 4/720 * screenHeight),
                                                                  child: Image.asset(
                                                                    "assets/images/path1.png",
                                                                    fit: BoxFit.none,
                                                                  ),
                                                                ),
                                                              ],
                                                            ),
                                                          ),
                                                        ],
                                                      ),
                                                      padding: EdgeInsets.all(10.0),
                                                    );
                                                  }
                                                  return Container(
//                                                    decoration: BoxDecoration(
//                                                      gradient: LinearGradient(
//                                                        begin: FractionalOffset.topCenter,
//                                                        //  end: FractionalOffset.topLeft,
//                                                        colors: [
//                                                          Colors.grey.withOpacity(0.0),
//                                                          Colors.white,
//                                                        ],
//                                                        stops: [0.95, 0],
//                                                      ),
//                                                    ),
                                                    //height: 50/720 * screenHeight,
                                                    color: color,
                                                    child: Row(
                                                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                                      mainAxisSize: MainAxisSize.max,
                                                      children: <Widget>[
                                                        Container(
                                                          width: 50/360 * screenWidth,
                                                          margin: EdgeInsets.only(left: 10/360 * screenWidth),
                                                          child: Text(
                                                            item.overall,
                                                            style: TextStyle(
                                                              fontSize: 12/720 * screenHeight,
                                                              fontFamily: "IBMPlexSans-Medium",
                                                              color: const Color(0xFF00022c),
                                                            ),
                                                            textAlign: TextAlign.center,
                                                          ),
                                                        ),
                                                        Container(
                                                          width: 50/360 * screenWidth,
                                                          margin: EdgeInsets.only(left: 10/360 * screenWidth),
                                                          child: Text(
                                                            item.dilr,
                                                            style: TextStyle(
                                                              fontSize: 12/720 * screenHeight,
                                                              fontFamily: "IBMPlexSans-Medium",
                                                              color: const Color(0xFF00022c),
                                                            ),
                                                            textAlign: TextAlign.center,
                                                          ),
                                                        ),
                                                        Container(
                                                          width: 30/360 * screenWidth,
                                                          margin: EdgeInsets.only(left: 10/360 * screenWidth),
                                                          child: Text(
                                                            item.qa,
                                                            style: TextStyle(
                                                              fontSize: 12/720 * screenHeight,
                                                              fontFamily: "IBMPlexSans-Medium",
                                                              color: const Color(0xFF00022c),
                                                            ),
                                                            textAlign: TextAlign.center,
                                                          ),
                                                        ),
                                                        Container(
                                                          width: 30/360 * screenWidth,
                                                          margin: EdgeInsets.only(left: 10/360 * screenWidth),
                                                          child: Text(
                                                            item.va,
                                                            style: TextStyle(
                                                              fontSize: 12/720 * screenHeight,
                                                              fontFamily: "IBMPlexSans-Medium",
                                                              color: const Color(0xFF00022c),
                                                            ),
                                                            textAlign: TextAlign.center,
                                                          ),
                                                        ),
                                                      ],
                                                    ),
                                                    padding: EdgeInsets.all(10.0),
                                                  );
                                                },
                                              ),
                                            ),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ]))),









//                Container(
//                  height: 300/720 * screenHeight,
//                  child: Row(
//                    crossAxisAlignment: CrossAxisAlignment.stretch,
//                    children: [
//                      Expanded(
//                        child: ListView.builder(
//                          addRepaintBoundaries: true,
//                          addAutomaticKeepAlives: true,
//                          //physics: NeverScrollableScrollPhysics(),
//                          //scrollDirection: Axis.horizontal,
//                          itemCount: datas == null ? 1 : datas.length,
//                          itemBuilder: (context, index) {
//                            final item = datas[index];
//                            Color color;
//                            if (index % 2 == 0) {
//                              color = Colors.white;
//                            } else if (index % 2 == 1)   {
//                              color = NeutralColors.sun_yellow.withOpacity(0.1);
//                            }
//                            if (index == 0) {
//                              return Container(
//                                height: 50/720 * screenHeight,
//                                color: Colors.white,
//                                child: Row(
//                                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
//                                  mainAxisSize: MainAxisSize.max,
//                                  children: <Widget>[
//                                    Container(
//                                      width: 70/360 * screenWidth,
//                                      margin: EdgeInsets.only(left: 10/360 * screenWidth),
//                                      child: Text(
//                                        "QUESTIONS",
//                                        style: TextStyle(
//                                          fontSize: 10/720 * screenHeight,
//                                          fontFamily: "IBMPlexSans-Medium",
//                                          color: const Color(0xFF7e919a),
//                                        ),
//                                        textAlign: TextAlign.left,
//                                      ),
//                                    ),
//                                    Container(
//                                      width: 50/360 * screenWidth,
//                                      margin: EdgeInsets.only(left: 10/360 * screenWidth),
//                                      child: Text(
//                                        "OVERALL",
//                                        style: TextStyle(
//                                          fontSize: 10/720 * screenHeight,
//                                          fontFamily: "IBMPlexSans-Medium",
//                                          color: const Color(0xFF7e919a),
//                                        ),
//                                        textAlign: TextAlign.center,
//                                      ),
//                                    ),
//                                    Container(
//                                      width: 50/360 * screenWidth,
//                                      margin: EdgeInsets.only(left: 10/360 * screenWidth),
//                                      child: Text(
//                                        "DI-LR",
//                                        style: TextStyle(
//                                          fontSize: 10/720 * screenHeight,
//                                          fontFamily: "IBMPlexSans-Medium",
//                                          color: const Color(0xFF7e919a),
//                                        ),
//                                        textAlign: TextAlign.center,
//                                      ),
//                                    ),
//                                    Container(
//                                      width: 50/360 * screenWidth,
//                                      margin: EdgeInsets.only(left: 10/360 * screenWidth),
//                                      child: Text(
//                                        "QA",
//                                        style: TextStyle(
//                                          fontSize: 10/720 * screenHeight,
//                                          fontFamily: "IBMPlexSans-Medium",
//                                          color: const Color(0xFF7e919a),
//                                        ),
//                                        textAlign: TextAlign.center,
//                                      ),
//                                    ),
//                                    Container(
//                                      width: 50/360 * screenWidth,
//                                      margin: EdgeInsets.only(left: 10/360 * screenWidth),
//                                      child: Text(
//                                        "VA",
//                                        style: TextStyle(
//                                          fontSize: 10/720 * screenHeight,
//                                          fontFamily: "IBMPlexSans-Medium",
//                                          color: const Color(0xFF7e919a),
//                                        ),
//                                        textAlign: TextAlign.center,
//                                      ),
//                                    ),
//                                  ],
//                                ),
//                                padding: EdgeInsets.all(10.0),
//                              );
//                            }
//                            return Container(
//                              height: 50/720 * screenHeight,
//                              color: color,
//                              child: Row(
//                                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
//                                mainAxisSize: MainAxisSize.max,
//                                children: <Widget>[
//                                  Container(
//                                    width: 70/360 * screenWidth,
//                                    margin: EdgeInsets.only(left: 10/360 * screenWidth),
//                                    child: Text(
//                                      item.questions,
//                                      style: TextStyle(
//                                        fontSize: 12/720 * screenHeight,
//                                        fontFamily: "IBMPlexSans",
//                                        color: const Color(0xFF00022c),
//                                      ),
//                                      textAlign: TextAlign.left,
//                                    ),
//                                  ),
//                                  Container(
//                                    width: 50/360 * screenWidth,
//                                    margin: EdgeInsets.only(left: 10/360 * screenWidth),
//                                    child: Text(
//                                      item.overall,
//                                      style: TextStyle(
//                                        fontSize: 12/720 * screenHeight,
//                                        fontFamily: "IBMPlexSans-Medium",
//                                        color: const Color(0xFF00022c),
//                                      ),
//                                      textAlign: TextAlign.center,
//                                    ),
//                                  ),
//                                  Container(
//                                    width: 50/360 * screenWidth,
//                                    margin: EdgeInsets.only(left: 10/360 * screenWidth),
//                                    child: Text(
//                                      item.dilr,
//                                      style: TextStyle(
//                                        fontSize: 12/720 * screenHeight,
//                                        fontFamily: "IBMPlexSans-Medium",
//                                        color: const Color(0xFF00022c),
//                                      ),
//                                      textAlign: TextAlign.center,
//                                    ),
//                                  ),
//                                  Container(
//                                    width: 50/360 * screenWidth,
//                                    margin: EdgeInsets.only(left: 10/360 * screenWidth),
//                                    child: Text(
//                                      item.qa,
//                                      style: TextStyle(
//                                        fontSize: 12/720 * screenHeight,
//                                        fontFamily: "IBMPlexSans-Medium",
//                                        color: const Color(0xFF00022c),
//                                      ),
//                                      textAlign: TextAlign.center,
//                                    ),
//                                  ),
//                                  Container(
//                                    width: 50/360 * screenWidth,
//                                    margin: EdgeInsets.only(left: 10/360 * screenWidth),
//                                    child: Text(
//                                      item.va,
//                                      style: TextStyle(
//                                        fontSize: 12/720 * screenHeight,
//                                        fontFamily: "IBMPlexSans-Medium",
//                                        color: const Color(0xFF00022c),
//                                      ),
//                                      textAlign: TextAlign.center,
//                                    ),
//                                  ),
//                                ],
//                              ),
//                              padding: EdgeInsets.all(10.0),
//                            );
//                          },
//                        ),
//                      ),
//                    ],
//                  ),
//                ),
          ],
        ),
      ),

    );
  }
}

//_getStackpage(bool offStageFlag,BuildContext context){
//  Offstage offstage = new Offstage(
//      offstage: offStageFlag,
//      child:  ChewieListItem(
//        videoPlayerController: VideoPlayerController.network(
//          'https://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4',),
//      )
//  );
//  return offstage;
//}



//_getChewiePlayer(bool offStageFlag, BuildContext context){
//  Offstage offstage = new Offstage(
//    offstage: offStageFlag,
//    child: Container(
//      decoration:  BoxDecoration(
//          borderRadius: new BorderRadius.circular(8.0/360*screenWidth)),
////      margin: EdgeInsets.only(
////          left: (20 / 360) * screenWidth, right: (20 / 360) * screenWidth),
//      height: 181 / 720 * screenHeight,
//      child: InkWell(
//        onTap: () {
//          AppRoutes.push(context,VideoPlayerApp());
//        },
//        child: Container(
//
//          constraints: new BoxConstraints.expand(
//            height: (181 / 720) * screenHeight,
//
//          ),
//          alignment: Alignment.bottomLeft,
//          padding: new EdgeInsets.only(bottom: 10.0/720*screenHeight),
//          decoration:  BoxDecoration(
//            borderRadius: new BorderRadius.circular(8.0/360*screenWidth),
//            image: DecorationImage(
//                alignment: Alignment(-.2, 0),
//                image: AssetImage("./assets/images/videoplaceholder.png"),
////                  image: NetworkImage(
////                      'http://d1cn01kw45hc6h.cloudfront.net/wp-content/uploads/2019/08/22161210/Quadtratic.png'
////                  ),
//                fit: BoxFit.cover),
//          ),
//          child: Center(
//            child: Container(
//              height: (50 / 720) * screenHeight,
//              width: (50 / 360) * screenWidth,
//              child: SvgPicture.asset(
//                getPrepareSvgImages.playwhite,
//                fit: BoxFit.fitHeight,
//              ),
//            ),
//          ),
//        ),
//      ),
//    ),
//  );
//  return offstage;
//}
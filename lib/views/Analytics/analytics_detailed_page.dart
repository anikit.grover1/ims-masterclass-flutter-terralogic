import 'dart:async';
import 'dart:convert';
import 'dart:math';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:imsindia/api/bloc/services.dart';
import 'package:imsindia/resources/strings/analytics_labels.dart';
import 'package:imsindia/utils/colors.dart';
//import 'package:indexed_list_view/indexed_list_view.dart';
import 'package:imsindia/components/custom_DropDown_Performance.dart';
import 'package:imsindia/utils/global.dart' as global;
import 'package:imsindia/utils/Url.dart';
import 'package:shared_preferences/shared_preferences.dart';


class AnalyticsDetails extends StatefulWidget {
  @override
  _AnalyticsDetailsState createState() => _AnalyticsDetailsState();
}

List<String> list = [
  'Leni Reifenstahl was: A filmmaker that reduced production of poetry, when compared to prose, stems not from the former’s superiority but because what was once considered a normal skill the composition of verse is now considered a niche and intimidating skill.',
  'Sony Laptops Are Still Part Of Leni Reifenstahl was: A filmmaker that reduced production of poetry, when compared to prose, stems not from the former’s superiority but because what was once considered a normal skill the composition of verse is now considered a niche and intimidating skill.',
  'Stu Unger Rise And Fall Of A Poker Geniu Leni Reifenstahl was: A filmmaker that reduced production of poetry, when compared to prose, stems not from the former’s superiority but because what was once considered a normal skill the composition of verse is now considered a niche and intimidating skill.',
  'Microsoft Patch Management For Hom Leni Reifenstahl was: A filmmaker that reduced production of poetry, when compared to prose, stems not from the former’s superiority but because what was once considered a normal skill the composition of verse is now considered a niche and intimidating skill.',
  'Microsoft Patch Management For HomeLeni Reifenstahl was: A filmmaker that reduced production of poetry, when compared to prose, stems not from the former’s superiority but because what was once considered a normal skill the composition of verse is now considered a niche and intimidating skill.',
  'Microsoft Patch Management For HomeLeni Reifenstahl was: A filmmaker that reduced production of poetry, when compared to prose, stems not from the former’s superiority but because what was once considered a normal skill the composition of verse is now considered a niche and intimidating skill.',
  'Microsoft Patch Management For HomeLeni Reifenstahl was: A filmmaker that reduced production of poetry, when compared to prose, stems not from the former’s superiority but because what was once considered a normal skill the composition of verse is now considered a niche and intimidating skill.',
  'Leni Reifenstahl was: A filmmaker that reduced production of poetry, when compared to prose, stems not from the former’s superiority but because what was once considered a normal skill the composition of verse is now considered a niche and intimidating skill.',
  'Sony Laptops Are Still Part Of Leni Reifenstahl was: A filmmaker that reduced production of poetry, when compared to prose, stems not from the former’s superiority but because what was once considered a normal skill the composition of verse is now considered a niche and intimidating skill.',
  'Stu Unger Rise And Fall Of A Poker Geniu Leni Reifenstahl was: A filmmaker that reduced production of poetry, when compared to prose, stems not from the former’s superiority but because what was once considered a normal skill the composition of verse is now considered a niche and intimidating skill.',
  'Microsoft Patch Management For Hom Leni Reifenstahl was: A filmmaker that reduced production of poetry, when compared to prose, stems not from the former’s superiority but because what was once considered a normal skill the composition of verse is now considered a niche and intimidating skill.',
  'Microsoft Patch Management For HomeLeni Reifenstahl was: A filmmaker that reduced production of poetry, when compared to prose, stems not from the former’s superiority but because what was once considered a normal skill the composition of verse is now considered a niche and intimidating skill.',
  'Microsoft Patch Management For HomeLeni Reifenstahl was: A filmmaker that reduced production of poetry, when compared to prose, stems not from the former’s superiority but because what was once considered a normal skill the composition of verse is now considered a niche and intimidating skill.',
  'Microsoft Patch Management For HomeLeni Reifenstahl was: A filmmaker that reduced production of poetry, when compared to prose, stems not from the former’s superiority but because what was once considered a normal skill the composition of verse is now considered a niche and intimidating skill.',
];

class _AnalyticsDetailsState extends State<AnalyticsDetails> {
  static List<bool> flagValue=List.filled(list.length, false);
  int currentSelectedIndex;

  String selectedLabel1;
  String selectedLabel2;
  String selectedLabel3;

  String _labelValue1 = 'ALL AREAS';
  String _labelValue2 = 'ALL TOPICS';
  String _labelValue3 = 'MCQ/TITA';

  String course;


  final TextEditingController _sample1 = new TextEditingController();
  final TextEditingController _sample2 = new TextEditingController();
  final TextEditingController _sample3 = new TextEditingController();
  //var controller = IndexedScrollController();
  var authorizationInvalidMsg;

   ScrollController _scrollController = new ScrollController();
  final dataKey = new GlobalKey();
    int i;
   double _ITEM_HEIGHT;
   int indexValue=0;
  @override
  void initState() {
    super.initState();
    setState(() {
      getCourseId();
    });
  }
  void getCourseId() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    course = prefs.getString("courseId");
    global.getToken.then((t){
      getScoreCardData(t);
    });
  }
  void _goToElement(int index){
    _scrollController.animateTo((_ITEM_HEIGHT * index), // 100 is the height of container and index of 6th element is 5
        duration: const Duration(milliseconds: 300),
        curve: Curves.easeOut);
  }

  var getAreaDropDownItems = ["All Areas"];
  var getTopicsDropDownItems = ["All Topics"];
  var getMcqDropDownItems = ["MCQ/TITA"];
  var apiData;
 // List<dynamic> apiData = List();

  void getScoreCardData(Map t) async {
    if (global.headersWithAuthorizationKey['Authorization'] != '') {
      ApiService().getAPI(URL.ANALYTICS_API_PERFORMANCE_ANALYTICS  + "5002683073", t)
          .then((returnValue) {
        setState(() {
          //print(returnValue);
          if (returnValue[0] == 'Success') {
          apiData = returnValue[1]['data'];
           //print("---------"+apiData.toString());
            for (var index = 0; index <apiData['studentTestItems'].length; index++) {
              var itemName = apiData['studentTestItems'][index]['areaName'];
              if(!getAreaDropDownItems.contains(itemName)){
                getAreaDropDownItems.add(itemName);
              }
            }

          } else {
            authorizationInvalidMsg = returnValue[0];
          }
        });
      });
    }

  }

  void loadDropdownItemsForTopics(String selectedLabel1) {
    getTopicsDropDownItems = ["All Topics"];
    for (var index = 0; index < apiData['studentTestItems'].length; index++) {
      var itemName = apiData['studentTestItems'][index]['topicName'];
      if(apiData['studentTestItems'][index]['areaName'] == selectedLabel1){
        if(!getTopicsDropDownItems.contains(itemName)){
          getTopicsDropDownItems.add(itemName);
        }
      }
    }
  }

  void loadDropdownItemsForMcq(String selectedLabel2) {
    getMcqDropDownItems = ["MCQ/TITA"];
    var itemName;
    for (var index = 0; index < apiData['studentTestItems'].length; index++) {
  //    if(apiData['studentTestItems'][index]['itemType']!=null){
         itemName = apiData['studentTestItems'][index]['itemType'];

   //   }
      if(apiData['studentTestItems'][index]['topicName'] == selectedLabel2){
        //print("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%called" + apiData['studentTestItems'][index]['itemType'] +selectedLabel2);
          if(!getMcqDropDownItems.contains(apiData['studentTestItems'][index]['itemType'])){
            getMcqDropDownItems.add(apiData['studentTestItems'][index]['itemType']);
        }

      }
    }
  }


  @override
  Widget build(BuildContext context) {
    //Timer(Duration(milliseconds: 1000), () => _scrollController.jumpTo(_scrollController.position.maxScrollExtent));
     double defaultScreenWidth = 360.0;
     double defaultScreenHeight = 720.0;
    var _mediaQueryData = MediaQuery.of(context);
    double screenWidthTotal = _mediaQueryData.size.width;
    double screenHeightTotal = _mediaQueryData.size.height;

    return Scaffold(
//      appBar: AppBar(
//        title: Text("Demo"),
//      ),
      body: Container(
          child: Column(
            children: <Widget>[
              Container(
                margin: EdgeInsets.only(top: 40),
                child: Column(
                  mainAxisAlignment:
                  MainAxisAlignment
                      .spaceBetween,
                  children: <Widget>[
                     Container(
                       margin: EdgeInsets.only(left: 20,right: 20),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Expanded(
                            child: Container(
                              height: 40/720*screenHeightTotal,
                              child: DropDownFormField(
                                getImmediateSuggestions: true,
                                textFieldConfiguration: TextFieldConfiguration(
                                  controller: _sample1,
                                  label: selectedLabel1!=null ? selectedLabel1 :_labelValue1,

                                ),
                                //  suggestionsBoxController:sugg,
                                suggestionsBoxDecoration: SuggestionsBoxDecoration(
                                    color: Colors.white),
                                suggestionsCallback: (pattern) {
                                  return getAreaDropDownItems;
                                },
                                itemBuilder: (context, suggestion) {
                                  //print("suggestinmon"+suggestion.toString());
                                  return Container(
                                    color:suggestion =="All Areas"
                                        ? NeutralColors.ice_blue
                                        : NeutralColors.pureWhite,
                                    child: Padding(
                                      padding:  EdgeInsets.only(top: (10 / defaultScreenHeight) * screenHeightTotal,
                                          bottom: (11 / defaultScreenHeight) * screenHeightTotal,
                                          left:(15 / defaultScreenWidth) * screenWidthTotal ),
                                      child: Text(
                                        suggestion,
                                        style: TextStyle(
                                          fontFamily: "IBMPlexSans",
                                          fontWeight: FontWeight.w500,
                                          fontSize:
                                          (12 / defaultScreenWidth) *
                                              screenWidthTotal,
                                          color:  suggestion =="All Areas"
                                              ? NeutralColors.purpley
                                              : NeutralColors.black,
                                          textBaseline: TextBaseline.alphabetic,
                                          letterSpacing: 0.0,
                                          inherit: false,
                                        ),
                                      ),
                                    ),
                                  );
                                },
                                hideOnLoading: true,
                                debounceDuration: Duration(milliseconds: 100),
                                transitionBuilder: (context, suggestionsBox, controller) {
                                  return suggestionsBox;
                                },
                                onSuggestionSelected: (suggestion) {
                                //  print('Sangeethaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa'+suggestion);
                                  _sample1.text = suggestion;
                                  //print("=======>>>>"+_sample1.text);
                                  setState(() {
                                    selectedLabel1 = suggestion;
                                    if(selectedLabel2!=null){
                                      selectedLabel2 = "All Topics";
                                    }
                                  });
                                  loadDropdownItemsForTopics(selectedLabel1);
                                },
                                onSaved: (value) => {print("something")},
                              ),

                            ),
                          ),
                          Expanded(
                            child: Container(
                              height: 40/720*screenHeightTotal,
                              child: DropDownFormField(
                                //  selected_month: selectedMonth,
                                getImmediateSuggestions: true,
                                // autoFlipDirection: true,
                                textFieldConfiguration: TextFieldConfiguration(
                                  controller: _sample2,
                                  label: selectedLabel2!=null ? selectedLabel2.toUpperCase() :_labelValue2,

                                ),
                                //  suggestionsBoxController:sugg,
                                suggestionsBoxDecoration: SuggestionsBoxDecoration(
                                    dropDownHeight: 130/720*screenHeightTotal,
                                    color: Colors.white),
                                suggestionsCallback: (pattern) {
                                  return getTopicsDropDownItems;
                                },
                                itemBuilder: (context, suggestion) {
                                //  print("suggestinmon"+suggestion.toString());
                                  return Container(
                                    color:suggestion =="All Topics"
                                        ? NeutralColors.ice_blue
                                        : NeutralColors.pureWhite,
                                    child: Padding(
                                      padding:  EdgeInsets.only(top: (10 / defaultScreenHeight) * screenHeightTotal,
                                          bottom: (11 / defaultScreenHeight) * screenHeightTotal,
                                          left:(15 / defaultScreenWidth) * screenWidthTotal ),
                                      child: Text(
                                        suggestion,
                                        style: TextStyle(
                                          fontFamily: "IBMPlexSans",
                                          fontWeight: FontWeight.w500,
                                          fontSize:
                                          (12 / defaultScreenWidth) *
                                              screenWidthTotal,
                                          color: suggestion =="All Topics"
                                              ? NeutralColors.purpley
                                              : NeutralColors.black,
                                          textBaseline: TextBaseline.alphabetic,
                                          letterSpacing: 0.0,
                                          inherit: false,
                                        ),
                                      ),
                                    ),
                                  );
                                },
                                hideOnLoading: true,
                                debounceDuration: Duration(milliseconds: 100),
                                transitionBuilder: (context, suggestionsBox, controller) {
                                  return suggestionsBox;
                                },
                                onSuggestionSelected: (suggestion) {
                                  _sample2.text = suggestion;
                                  setState(() {
                                    selectedLabel2 = suggestion;
                                    if(selectedLabel3!=null){
                                      selectedLabel3 = "MCQ/TITA";
                                    }
                                  });
                                  loadDropdownItemsForMcq(selectedLabel2);
                                },
                              ),

                            ),
                          ),
                          Expanded(
                            child: Container(
                              height: 40/720*screenHeightTotal,

                              child: DropDownFormField(
                                //  selected_month: selectedMonth,
                                getImmediateSuggestions: true,
                                // autoFlipDirection: true,
                                textFieldConfiguration: TextFieldConfiguration(
                                  controller: _sample3,
                                  label: selectedLabel3!=null ? selectedLabel3.toUpperCase() :_labelValue3,

                                ),
                                //  suggestionsBoxController:sugg,
                                suggestionsBoxDecoration: SuggestionsBoxDecoration(
                                    dropDownHeight: 130/720*screenHeightTotal,
                                    color: Colors.white),
                                suggestionsCallback: (pattern) {
                                  return getMcqDropDownItems;
                                },
                                itemBuilder: (context, suggestion) {
                                  //print("suggestinmon"+suggestion.toString());
                                  return Container(
                                    color:
                                    suggestion =="MCQ/TITA"
                                        ? NeutralColors.ice_blue
                                        :
                                    NeutralColors.pureWhite,
                                    child: Padding(
                                      padding:  EdgeInsets.only(top: (10 / defaultScreenHeight) * screenHeightTotal,
                                          bottom: (11 / defaultScreenHeight) * screenHeightTotal,
                                          left:(15 / defaultScreenWidth) * screenWidthTotal ),
                                      child: Text(
                                        suggestion,
                                        style: TextStyle(
                                          fontFamily: "IBMPlexSans",
                                          fontWeight: FontWeight.w500,
                                          fontSize:
                                          (12 / defaultScreenWidth) *
                                              screenWidthTotal,
                                          color:
                                          suggestion =="MCQ/TITA"
                                              ? NeutralColors.purpley
                                              :
                                          NeutralColors.black,
                                          textBaseline: TextBaseline.alphabetic,
                                          letterSpacing: 0.0,
                                          inherit: false,
                                        ),
                                      ),
                                    ),
                                  );
                                },
                                hideOnLoading: true,
                                debounceDuration: Duration(milliseconds: 100),
                                transitionBuilder: (context, suggestionsBox, controller) {
                                  return suggestionsBox;
                                },
                                onSuggestionSelected: (suggestion) {
                                  //print('Sangeethaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa'+suggestion
                                  //);
                                  _sample3.text = suggestion.toString().toUpperCase();
                                  //print("=======>>>>"+_sample3.text);
                                  setState(() {
                                    selectedLabel3 = suggestion;
                                  });

                                },
                              ),

                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              Container(
               // height:(20/defaultScreenHeight) * screenHeightTotal,
                margin: EdgeInsets.only(top:(15/defaultScreenHeight)*screenHeightTotal,
                          bottom: (6/defaultScreenHeight)*screenHeightTotal,
                    left: (20 / defaultScreenWidth) * screenWidthTotal,
                    right: (20 / defaultScreenWidth) * screenWidthTotal),
                width: double.infinity,
                child: Text(AnalyticsLabels.questionsLabel,
                  style: TextStyle(
                    fontSize: (14/defaultScreenWidth)*screenWidthTotal,
                    color: NeutralColors.dark_navy_blue,
                    fontFamily: "IBMPlexSansBold",
                    fontWeight: FontWeight.bold,
                  ),),
              ),
              Divider(),
              Expanded(
                child: ListView.separated(
                  controller: _scrollController,
                  scrollDirection: Axis.vertical,
                  itemCount: list.length,
                  itemBuilder: (context, index) {
                    return GestureDetector(
                    onTap: () {
                      setState(() {
                        currentSelectedIndex = index;
                        if(currentSelectedIndex == index){
                          flagValue[index] =
                          flagValue.elementAt(index) == true
                              ? false
                              : true;
                          _ITEM_HEIGHT = 50.0;

                        }
                       else{
                         flagValue.removeLast();
                        }
//

                      });
                      _goToElement(index);
                    },
                      child: Container(
                       // height: flagValue.elementAt(index) ? _ITEM_HEIGHT = 300 : _ITEM_HEIGHT = 50,
                        child: Container(
                          margin: EdgeInsets.only(
                            top: (15 / defaultScreenHeight) * screenHeightTotal,
                            bottom:
                                (16 / defaultScreenHeight) * screenHeightTotal,
                              left: (20 / defaultScreenWidth) * screenWidthTotal,
                              right: (20 / defaultScreenWidth) * screenWidthTotal
                          ),
                          child: Column(
                            children: <Widget>[
                                 Container(
                                  //height: _ITEM_HEIGHT,
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: <Widget>[
                                      Container(
                                        alignment: Alignment.topRight,
                                        child: Text(
                                          '${index+1}.',
                                          style: TextStyle(
                                            fontSize: (14 / defaultScreenWidth) *
                                                screenWidthTotal,
                                            color: NeutralColors.dark_navy_blue,
                                            fontFamily: "IBMPlexSans",
                                            fontWeight: FontWeight.w400,
                                            //  height:  (18/defaultScreenWidth)*screenWidthTotal,
                                          ),
                                        ),
                                      ),
                                      Expanded(
                                          child: Text(
                                            list[index],
                                            overflow:  currentSelectedIndex == index
                                                ? TextOverflow.visible
                                                : TextOverflow.ellipsis,
                                            style: TextStyle(
                                              fontSize:
                                                  (14 / defaultScreenWidth) *
                                                      screenWidthTotal,
                                              color: NeutralColors.dark_navy_blue,
                                              fontFamily: "IBMPlexSans",
                                              fontWeight: FontWeight.w400,
                                              //  height:  (18/defaultScreenWidth)*screenWidthTotal,
                                            ),
                                          )),
                                      Center(
                                        child: Container(
                                          width: 15 / 360 * screenWidthTotal,
                                          child: Icon( currentSelectedIndex == index
                                              ? Icons.keyboard_arrow_up
                                              : Icons.keyboard_arrow_down),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              currentSelectedIndex == index
                                  ? Container(
                                decoration: BoxDecoration(
                                  color: NeutralColors.ice_blue,
                                  borderRadius:
                                  BorderRadius.all(Radius.circular(5)),
                                ),
                                margin: EdgeInsets.only(
                                    top: (15 / defaultScreenHeight) *
                                        screenHeightTotal),
                                child: Padding(
                                  padding: EdgeInsets.only(
                                      top: (10 / defaultScreenHeight) *
                                          screenHeightTotal,
                                      bottom: (10 / defaultScreenHeight) *
                                          screenHeightTotal,
                                      left: (10 / defaultScreenWidth) *
                                          screenWidthTotal,
                                      right: (10 / defaultScreenWidth) *
                                          screenWidthTotal),
                                  child: Row(
                                    mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: <Widget>[
                                      Column(
                                        children: <Widget>[
                                          Text(
                                            "Status",
                                            style: TextStyle(
                                              color: Color.fromARGB(
                                                  255, 153, 154, 171),
                                              fontSize: 12,
                                              fontFamily: "IBMPlexSans",
                                              fontWeight: FontWeight.w500,
                                            ),
                                            textAlign: TextAlign.center,
                                          ),
                                          Text(
                                            "Correct",
                                            style: TextStyle(
                                              color: Color.fromARGB(255, 0, 3, 44),
                                              fontSize: 14,
                                              fontFamily: "IBMPlexSans",
                                              fontWeight: FontWeight.w500,
                                            ),
                                            textAlign: TextAlign.center,
                                          ),
                                        ],
                                      ),
                                      Column(
                                        children: <Widget>[
                                          Text(
                                            "Time Taken",
                                            style: TextStyle(
                                              color: Color.fromARGB(
                                                  255, 153, 154, 171),
                                              fontSize: 12,
                                              fontFamily: "IBMPlexSans",
                                              fontWeight: FontWeight.w500,
                                            ),
                                            textAlign: TextAlign.center,
                                          ),
                                          Text(
                                            "00:02:25",
                                            style: TextStyle(
                                              color: Color.fromARGB(255, 0, 3, 44),
                                              fontSize: 14,
                                              fontFamily: "IBMPlexSans",
                                              fontWeight: FontWeight.w500,
                                            ),
                                            textAlign: TextAlign.center,
                                          ),
                                        ],
                                      ),
                                      Column(
                                        children: <Widget>[
                                          Text(
                                            "Average Time",
                                            style: TextStyle(
                                              color: Color.fromARGB(
                                                  255, 153, 154, 171),
                                              fontSize: 12,
                                              fontFamily: "IBMPlexSans",
                                              fontWeight: FontWeight.w500,
                                            ),
                                            textAlign: TextAlign.center,
                                          ),
                                          Text(
                                            "00:02:25",
                                            style: TextStyle(
                                              color: Color.fromARGB(255, 0, 3, 44),
                                              fontSize: 14,
                                              fontFamily: "IBMPlexSans",
                                              fontWeight: FontWeight.w500,
                                            ),
                                            textAlign: TextAlign.center,
                                          ),
                                        ],
                                      ),
                                      Column(
                                        children: <Widget>[
                                          Text(
                                            "P-Value",
                                            style: TextStyle(
                                              color: Color.fromARGB(
                                                  255, 153, 154, 171),
                                              fontSize: 12,
                                              fontFamily: "IBMPlexSans",
                                              fontWeight: FontWeight.w500,
                                            ),
                                            textAlign: TextAlign.center,
                                          ),
                                          Text(
                                            "54.23%",
                                            style: TextStyle(
                                              color: Color.fromARGB(255, 0, 3, 44),
                                              fontSize: 14,
                                              fontFamily: "IBMPlexSans",
                                              fontWeight: FontWeight.w500,
                                            ),
                                            textAlign: TextAlign.center,
                                          ),
                                        ],
                                      )
                                    ],
                                  ),
                                ),
                              )
                                  : Container(),
                            ],
                          ),
                        ),
                      ),
                    );
                  },
                  separatorBuilder: (context, index) {
                    return Divider();
                  },
                ),
              ),
            ],
          ),
      ),
    );
  }


}
class Test {
  List<String> textList;

  Test({List<String> this.textList});
}

class SampleService3 {
  static List<String> getSuggestions(String query) {
    List<String> matches = List();
    matches.add("MCQ/TITA");
    matches.add("MCQ");
    matches.add("TITA");

    //  matches.retainWhere((s) => s.toLowerCase().contains(query.toLowerCase()));
    return matches;
  }
}

class SampleService1 {
  static List<String> getSuggestions(String query) {
    List<String> matches = List();
    matches.add("All Areas");
    matches.add("Pie-Chart");
    matches.add("Table");
    matches.add("Multiple\nDiagrams");
    matches.add("Puzzles");
    matches.add("Table");

    //  matches.retainWhere((s) => s.toLowerCase().contains(query.toLowerCase()));
    return matches;
  }
}

class SampleService2 {
  static List<String> getSuggestions(String query) {
    List<String> matches = List();
    matches.add("All Topics");
    matches.add("Observation");
    matches.add("Reasoning");
    matches.add("Calculation");
    matches.add("Numerical\nPuzzles");
    matches.add("Arrangements");
    //  matches.retainWhere((s) => s.toLowerCase().contains(query.toLowerCase()));
    return matches;
  }
}


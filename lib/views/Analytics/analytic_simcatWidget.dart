import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:imsindia/components/custom_DropDown_Analytics.dart';
import 'package:imsindia/components/primary_button_gradient.dart';
import 'package:imsindia/utils/colors.dart';
import 'package:imsindia/api/bloc/services.dart';
import 'package:imsindia/utils/Url.dart';
import 'package:imsindia/views/Analytics/analytics_scorecard.dart' ;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:imsindia/utils/global.dart' as global;


var screenWidthTotal;
var screenHeightTotal;
var screenHeight;
var courseID;
var menuId;
var screenWidth;

var getLearningModules = [];
var getLearningObjects = [];
var getLearningModulesId = [];
var getLearningObjectsId = [];
var finalListForLearningModules = [];
var listForLearningObjectsId = [];
var selectedLearningModule;
var selectedLearningModuleName;
var selectedLearningObject;
var totalData;
var notSelectedLearningModule = ["Select Test"];
var lastTestId;
var selectedLearningObjectId;

class StatesService {
  static final List<String> venues = [
    'simCAT Proctored',
    'simCAT Proctored1',
    'simCAT Proctored2',
    'simCAT Proctored3',

  ];
  static final List<String> venues1 = [
    'simCAT 01',
    'simCAT 02',
    'simCAT 03',
  ];
  static  getSuggestions(String query) {
    //List<String> matches = List();
    //matches.addAll(venues);

    // matches.retainWhere((s) => s.toLowerCase().contains(query.toLowerCase()));
    return finalListForLearningModules;
  }
  static  getSuggestions1(String query) {
    //List<String> matches = List();
    //finalListForLearningObjects.addAll(getLearningObjects);

    // matches.retainWhere((s) => s.toLowerCase().contains(query.toLowerCase()));
    return getLearningObjects;
  }
}


class simCATWidget extends StatefulWidget{
  @override
  simCATWidgetState createState() => simCATWidgetState();

}
class simCATWidgetState extends State < simCATWidget>{

  bool onClickChange = false;
  String dropdownValue = "simCAT Proctored";
  String dropdownValueSimCAT = "simCAT 01";
  final TextEditingController _stateOfVenues =
  new TextEditingController();

  final TextEditingController _stateOfVenues1 =
  new TextEditingController();
  AnalyticsScorecardState scoreCardObj= new AnalyticsScorecardState();
  var authorizationInvalidMsg;

  void getLists(Map t) async {
    if (global.headersWithAuthorizationKey['Authorization'] != '') {
      ApiService().getAPI(URL.ANALYTICS_API_LEARN_MODULE_LIST + courseID + "&id=" + menuId, t)
          .then((returnValue) {
        setState(() {
          print(returnValue);
          getLearningModules = [];
          getLearningModulesId = [];
          listForLearningObjectsId = [];
          finalListForLearningModules = [];
          if (returnValue[0] == 'Data fetched successfully') {
            totalData=returnValue[1]['data'];
            lastTestId = returnValue[1]['lastTestId'];
            saveTestId(lastTestId);
            print("&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&"+returnValue[1]['lastTestId']);
            for (var index = 0; index < totalData.length; index++) {
              if (totalData[index]['componentType'] == "learningModule"){
                getLearningModules.add(totalData[index]['component']['title']);
                getLearningModulesId.add(totalData[index]['id']);
              }
            }
            for(var index = 0; index<totalData.length;index++){
              if (totalData[index]['componentType'] == "learningObject"){
                //finalListForLearningModules.add(totalData[index]['component']['title']);
                if(!listForLearningObjectsId.contains(totalData[index]['parentId']))
                  listForLearningObjectsId.add(totalData[index]['parentId']);
              }
            }
            for(var i = 0;i<totalData.length;i++) {
              for (var index = 0; index <
                  listForLearningObjectsId.length; index++) {
                if (listForLearningObjectsId[index] ==
                    totalData[i]['id']) {
                  finalListForLearningModules.add(
                      totalData[i]['component']['title']);
                }
              }
            }
            if (lastTestId != null){
              for(var j = 0;j<totalData.length;j++){
                if(lastTestId == totalData[j]['id']){
                  selectedLearningObject = totalData[j]['component']['title'];
                  selectedLearningModule = totalData[j]['parentId'];
                  //print("Selected Learning Object"+ selectedLearningObject);
                  //print("Selected Learning Module"+ selectedLearningModule.toString());
                }
              }
              for(var index =0;index<totalData.length;index++){
                if(selectedLearningModule == totalData[index]['id']){
                  selectedLearningModuleName = totalData[index]['component']['title'];
                  //print("Selected Learning Module"+ selectedLearningModuleName);
                }
              }
              //print("Selected Learning Module"+ selectedLearningModuleName);
              //print("Selected Learning Object"+ selectedLearningObject);
            }
          }else {
            authorizationInvalidMsg = returnValue[0];
          }
        });
      });
    }
  }

  void getSecondDropDownData(String selectedLearningModule){
    getLearningObjects = [];
    getLearningObjectsId = [];
    print("&&&&&&&&&&&Before&&&&&&&&&&&getLearningObjects"+getLearningObjects.toString());
    //print("gdffdhffgdgdgf"+selectedLearningModule);
    for (var i = 0; i < getLearningModulesId.length; i++) {
      for (var j = 0; j < totalData.length; j++) {
        if (totalData[j]['parentId'] == selectedLearningModule && totalData[j]['componentType'] == "learningObject") {
          if(!getLearningObjects.contains(totalData[j]['component']['title'])){
            getLearningObjects.add(totalData[j]['component']['title']);
          }
          if(!getLearningObjectsId.contains(totalData[j]['id'])){
            getLearningObjectsId.add(totalData[j]['id']);
          }
        }
      }
    }
    print("&&&&&&&&&&&After&&&&&&&&&&&getLearningObjects"+getLearningObjects.toString());
    //print("Hiiiiiiiiiiiiii"+ getLearningObjects.toString());
  }

  void getCourseId() async {
    print("#########################################################################");
    SharedPreferences prefs = await SharedPreferences.getInstance();
    courseID = prefs.getString("courseId");
    menuId = prefs.getString("moduleId");
    print("**********************course id "+courseID);
    print("**********************menuId"+menuId);

    global.getToken.then((t){
      getLists(t);
    });
    //getScoreCardData();
  }

  saveTestId(String id) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('testId',id);
  }

  void initState() {
    getLearningModules = [];
    getLearningModulesId = [];
    listForLearningObjectsId = [];
    finalListForLearningModules = [];
    super.initState();
    setState(() {
      getCourseId();
    });
  }
  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {

    var _mediaQueryData = MediaQuery.of(context);
    screenWidth = _mediaQueryData.size.width;
    screenHeight = _mediaQueryData.size.height;


    return Container(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Container(
              //margin: EdgeInsets.only(top: 30/720*screenHeight),
              decoration: BoxDecoration(
                gradient: LinearGradient(
                  // Where the linear gradient begins and ends
                  begin: Alignment.topCenter,
                  end: Alignment.bottomCenter,
                  // Add one stop for each color. Stops should increase from 0 to 1
                  stops: [0.1, 0.5],
                  colors: [
                    // Colors are easy thanks to Flutter's Colors class.
                    NeutralColors.ice_blue,
                    Colors.white,
                  ],
                ),
                //     color: Color.fromARGB(255, 255, 255, 255),
                boxShadow: [
                  BoxShadow(
                    color: NeutralColors.ice_blue,
                    offset: Offset(0, 10),
                    blurRadius: 18,
                  ),
                ],
              ),

              //color: Colors.red,
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Container(
                    margin: EdgeInsets.only(top: 30/720*screenHeight,left: 20/360*screenWidth),
                    width: 220/360 * screenWidth,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Container(
                          child: Text(
                            selectedLearningModuleName != null?selectedLearningModuleName:"Last Taken Test",
                            overflow: TextOverflow.ellipsis,
                            style: TextStyle(
                              color: NeutralColors.dark_navy_blue,
                              fontSize: 18/720 * screenHeight,
                              fontFamily: "IBMPlexSans",
                              fontWeight: FontWeight.w700,
                            ),
                            textAlign: TextAlign.left,
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(bottom: 20/720 * screenHeight),
                          child: Text(
                            selectedLearningObject!=null?selectedLearningObject:"",
                            overflow: TextOverflow.ellipsis,
                            style: TextStyle(
                              color: NeutralColors.blue_grey,
                              fontSize: 14/720 * screenHeight,
                              fontFamily: "IBMPlexSans",
                              fontWeight: FontWeight.w500,
                            ),
                            textAlign: TextAlign.left,
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 30/720*screenHeight,right: 20/360*screenWidth,bottom: 20/720 * screenHeight),
                    //height: 40/720 * screenHeight,
                    width: 100/360*screenWidth,
                    child: PrimaryButtonGradient(

                      onTap: () {
                        setState(() {
                          (onClickChange == true)?
                          onClickChange = false:
                          onClickChange = true;
                        });
                        if(!onClickChange){
                          AnalyticsScorecard();
                        }
                        //AppRoutes.push(context, AnalyticsScorecard());
                      },
                      text: (onClickChange)?'DONE':'CHANGE',
                    ),
                  ),
                ],
              ),
            ),
            (onClickChange)?
            Container(
              alignment: Alignment.topCenter,

              //  color:Colors.white,
              //height: 70/720*screenHeight,
              // width: screenWidth,
              decoration: BoxDecoration(
                boxShadow: [
                  BoxShadow(
                    blurRadius: 13.0,
                    color: Colors.black.withOpacity(.03),
                    offset: Offset(6.0, 7.0),
                    // spreadRadius: 500.0,
                  ),
                ],
                //shape: BoxShape.rectangle,
                //border: Border.all(),
                color: Colors.white,
              ),
              //    margin: EdgeInsets.only(right: screenWidth/19,left: screenWidth/19),
              child:Container(
                //  color: Colors.green,
                margin: EdgeInsets.only(right: 19/360*screenWidth,left: 19/360*screenWidth,top: 10/720*screenHeight, bottom: 26/720 * screenHeight),

                child: Row(

                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: <Widget>[
                    Expanded(child: Padding(
                      padding: EdgeInsets.only(right:10/360*screenWidth),
                      child:

                      DropDownFormField(
                        getImmediateSuggestions: true,
                        textFieldConfiguration: TextFieldConfiguration(
                            controller: _stateOfVenues,
                            label: selectedLearningModuleName != null?selectedLearningModuleName:"Select Value",

                        ),
                        suggestionsBoxDecoration: SuggestionsBoxDecoration(
                            dropDownHeight: 150,
                            borderRadius: new BorderRadius.circular(5.0),
                            color: Colors.white),
                        suggestionsCallback: (pattern) {
                          return finalListForLearningModules;
                        },
                        itemBuilder: (context, suggestion) {
                          return ListTile(
                            dense: true,
                            contentPadding: EdgeInsets.only(left: 10/360*screenWidth),
                            title: Text(
                              suggestion,
                              style: TextStyle(
                                fontFamily: "IBMPlexSans",
                                fontWeight: FontWeight.w400,
                                fontSize: (14 / 720) * screenHeight,
                                color: NeutralColors.bluey_grey,
                                textBaseline: TextBaseline.alphabetic,
                                letterSpacing: 0.0,
                                inherit: false,
                              ),
                            ),
                          );
                        },
                        hideOnLoading: true,
                        debounceDuration: Duration(milliseconds: 100),
                        transitionBuilder: (context, suggestionsBox, controller) {
                          return suggestionsBox;
                        },
                        onSuggestionSelected: (suggestion) {
                          //print('Sangeethaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa'+suggestion);
                          _stateOfVenues.text = suggestion;

                          //print("=======>>>>"+_stateOfVenues.text);
                          setState(() {
                            //selectedLearningModule = suggestion;
                            for (var j = 0; j < totalData.length; j++) {
                              if (totalData[j]['component']['title'] == suggestion) {
                                selectedLearningModule = totalData[j]['id'];
                              }
                            }
                            selectedLearningModuleName = suggestion;
                            selectedLearningObject = "Select Test";
                            _stateOfVenues1.text = "Select Test";
//                            if(selectedLearningObject!=null){
//                              selectedLearningObject = "Select test";
//                            }
                          });
                          getSecondDropDownData(selectedLearningModule);
                        },
                        onSaved: (value) => {print("something")},
                      ),
                    ),
                    ),
                    Expanded(
                      child: Padding(
                        padding: EdgeInsets.only(left:10/360*screenWidth),
                          child: DropDownFormField(
                            hideOnEmpty: true,
                              getImmediateSuggestions: true,
                              textFieldConfiguration: TextFieldConfiguration(
                                  controller: _stateOfVenues1,
                                  label: selectedLearningObject != null?selectedLearningObject:"Select Test",
                              ),
                              suggestionsBoxDecoration: SuggestionsBoxDecoration(
                                  dropDownHeight: getLearningObjects.length == 1?50:150,
                                  borderRadius: new BorderRadius.circular(5.0),
                                  color: Colors.white),
                              suggestionsCallback: (pattern) {
                                return getLearningObjects;
                              },
                              itemBuilder: (context, suggestion) {
                                return ListTile(
                                  dense: true,
                                  contentPadding: EdgeInsets.only(left: 10/360*screenWidth),
                                  title: Text(
                                    suggestion,
                                    style: TextStyle(
                                      fontFamily: "IBMPlexSans",
                                      fontWeight: FontWeight.w400,
                                      fontSize: (14 / 720) * screenHeight,
                                      color: NeutralColors.bluey_grey,
                                      textBaseline: TextBaseline.alphabetic,
                                      letterSpacing: 0.0,
                                      inherit: false,
                                    ),
                                  ),
                                );
                              },
                              hideOnLoading: true,
                              debounceDuration: Duration(milliseconds: 100),
                              transitionBuilder: (context, suggestionsBox, controller) {
                                return suggestionsBox;
                              },
                              onSuggestionSelected: (suggestion) {
                                _stateOfVenues1.text = suggestion;
                                setState(() {
                                  for (var j = 0; j < totalData.length; j++) {
                                    if (totalData[j]['component']['title'] == suggestion) {
                                      selectedLearningObjectId = totalData[j]['id'];
                                      saveTestId(selectedLearningObjectId);
                                    }
                                  }
                                  selectedLearningObject = suggestion;
                                });
                              },
                              onSaved: (value) => {print("something" + selectedLearningObjectId.toString())},
                            ),
                      ),
                    ),
                  ],
                ),
              ),
            ):Container(),
          ],
        )
    );
  }

}
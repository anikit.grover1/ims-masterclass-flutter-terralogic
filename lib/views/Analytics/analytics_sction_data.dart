import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:imsindia/api/bloc/services.dart';
import 'package:imsindia/utils/Url.dart';
import 'package:imsindia/utils/colors.dart';
import 'package:imsindia/utils/text_styles.dart';
import 'package:imsindia/views/Analytics/analytic_simcatWidget.dart';
import 'package:imsindia/views/Analytics/analytics_efficiency_tracker.dart';
import 'package:imsindia/views/Analytics/analytics_performance_analytics.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:imsindia/utils/global.dart' as global;

var screenWidthTotal;
var screenHeightTotal;

var _safeAreaHorizontal;

var _safeAreaVertical;

var screenHeight;

var screenWidth;
var name ;

List<String> sectionName = [];
List<String> sectionScore = [];
List<String> sectionPercentile = [];
class Data {
  String title;
  String subtitle;
  String score;
  String percentile;

  Data({
    this.title,
    this.subtitle,
    this.score,
    this.percentile,
  });
}

final data1 = Data(
  title: "Data Interpretation",
  subtitle: "Your score is 23 marks below cut off",
  score: "56",
  percentile: "56th",
);

final data2 = Data(
  title: "VA-RC",
  subtitle: "Your score is 23 marks above cut off",
  score: "56",
  percentile: "13th",
);

final data3 = Data(
  title: "Quantitative Ability",
  subtitle: "Your score is 23 marks above cut off",
  score: "56",
  percentile: "13th",
);

final List<Data> datas = [
  data1,
  data2,
  data3,
];



class AnalyticsSectionData extends StatefulWidget {
//  final Function func;
//  OverlayEntry overlayEntry;
  List<String> sectionName = [];
  List<String> sectionScore = [];
  List<String> sectionPercentile = [];
  bool showPercentile;
  AnalyticsSectionData(this.sectionName,this.sectionScore,this.sectionPercentile,this.showPercentile);

  //AnalyticsSectionData({Key key, this.func}) : super(key: key);

  @override
  AnalyticsSectionDataState createState() => AnalyticsSectionDataState();
}

class AnalyticsSectionDataState extends State<AnalyticsSectionData>
    with TickerProviderStateMixin{


  void getSectionData_sharePref() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      sectionName = prefs.getStringList("SectionName");
      sectionScore = prefs.getStringList("SectionScore");
      sectionPercentile = prefs.getStringList("SectionPercentile");

    });
    //print("=======getvideo"+sectionName.toString());
    //print("jhjhjhkjhhkjhj"+sectionScore.toString());
    //print("jhjhjhkjhhkjhj"+sectionPercentile.toString());
  }
  @override
  void initState() {
    sectionName = [];
    sectionScore = [];
    sectionPercentile=[];
    //getSectionData_sharePref();
    // TODO: implement initState
    super.initState();
  }

  @override
  void didChangeDependencies() {
    sectionName = [];
    sectionScore = [];
    sectionPercentile=[];
    getSectionData_sharePref();
    print("Section Data");
    //getVideoData_sharePref();
    super.didChangeDependencies();
  }
  @override
  Widget build(BuildContext context) {
    var _mediaQueryData = MediaQuery.of(context);
    screenWidth = _mediaQueryData.size.width;
    screenHeight = _mediaQueryData.size.height;


    return Container(
      child: Column(
        children: <Widget>[
          Container(
            child: ListView.builder(
                addRepaintBoundaries: true,
                addAutomaticKeepAlives: true,
                //controller: _scrollController,
                shrinkWrap:true,
                physics: NeverScrollableScrollPhysics(),
                itemCount: widget.sectionName.length,
                itemBuilder: (context, i) {
                  //final item = datas[i];
                  final name = widget.sectionName[i];
                  final score  = widget.sectionScore[i];
                  final percentile = widget.sectionPercentile[i];
                  Color color;
                  if (i == 0) {
                    color = const Color(0xFFff5757);
                  } else {
                    color = const Color(0xFF48c100);
                  }
                  return name == "Pyschometric" || name == "Descriptive" || name == "Descriptive Writing"?Container():Container(
                    decoration: BoxDecoration(
                      border: Border.all(width: 2.0, color: NeutralColors.ice_blue),
                      borderRadius: BorderRadius.all(
                        Radius.circular(5.0),
                        //         <--- border radius here
                      ),
                    ),
                    margin: EdgeInsets.only(top: 10 / 720 * screenHeight),
                    child: Container(
                      child: IntrinsicHeight(
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          children: <Widget>[
                            Container(
                              margin: EdgeInsets.only(left: 0),
                              width: 2 / 360 * screenWidth,
                              //height: 10,
                              decoration: BoxDecoration(
                                color: color,
                                borderRadius: BorderRadius.all(Radius.circular(1.5)),
                              ),
                              //color: color,
                            ),
                            Container(
                              width: 150 / 360 * screenWidth,
                              margin: EdgeInsets.only(
                                  left: 13 / 360 * screenWidth,
                                  top: 15 / 720 * screenHeight),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Align(
                                    alignment: Alignment.topLeft,
                                    child: Text(
                                      name,
                                      style: TextStyle(
                                        color: NeutralColors.dark_navy_blue,
                                        fontSize: 14 / 720 * screenHeight,
                                        fontFamily: "IBMPlexSans",
                                        fontWeight: FontWeight.w500,
                                      ),
                                    ),
                                  ),
                                  Container(
                                    margin: EdgeInsets.only(
                                        top: 2 / 720 * screenHeight,
                                        bottom: 15 / 720 * screenHeight),
                                    child: Text(
                                      "",
                                      style: TextStyle(
                                        color: NeutralColors.blue_grey,
                                        fontSize: 12 / 720 * screenHeight,
                                        fontFamily: "IBMPlexSans",
                                        fontWeight: FontWeight.w500,
                                      ),
                                    ),
                                  )
                                ],
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.only(
                                  left: 23 / 360 * screenWidth,
                                  top: 15 / 720 * screenHeight),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Container(
                                    child: Text(
                                      score,
                                      textAlign: TextAlign.left,
                                      style: TextStyle(
                                        color: color,
                                        fontSize: 16 / 720 * screenHeight,
                                        fontFamily: "IBMPlexSans",
                                        fontWeight: FontWeight.w500,
                                      ),
                                    ),
                                  ),
                                  Container(
                                    margin: EdgeInsets.only(top: 2 / 720 * screenHeight),
                                    child: Text(
                                      "Score",
                                      style: TextStyle(
                                        color: NeutralColors.gunmetal,
                                        fontSize: 12 / 720 * screenHeight,
                                        fontFamily: "IBMPlexSans",
                                        fontWeight: FontWeight.w500,
                                      ),
                                    ),
                                  )
                                ],
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.only(
                                  left: 20 / 360 * screenWidth,
                                  top: 15 / 720 * screenHeight,
                                  right: 10 / 360 * screenWidth),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Container(
                                    child: Text(
                                      widget.showPercentile == true?percentile:"",
                                      textAlign: TextAlign.left,
                                      style: TextStyle(
                                          color: color,
                                          fontSize: 16 / 720 * screenHeight,
                                          fontFamily: "IBMPlexSans",
                                          fontWeight: FontWeight.w500),
                                    ),
                                  ),
                                  Container(
                                    margin: EdgeInsets.only(top: 2 / 720 * screenHeight),
                                    child: Text(
                                      widget.showPercentile == true?"Percentile":"",
                                      style: TextStyle(
                                          color: NeutralColors.gunmetal,
                                          fontSize: 12 / 720 * screenHeight,
                                          fontFamily: "IBMPlexSans",
                                          fontWeight: FontWeight.w500),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  );
                }
            ),
          ),
          //getTextWidgets(screenHeight, screenWidth, widget.func),
        ],
      ),

    );
  }
}

Widget getTextWidgets(final screenHeight, final screenWidth, Function func) {
  ////print(datas.length);
  //print("lengthhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhh");
  List<Widget> list = new List<Widget>();
  for (var i = 0; i < sectionName.length; i++) {
    final item = datas[i];
    final name = sectionName[i];
    final score  = sectionScore[i];
    final percentile = sectionPercentile[i];
    Color color;
    if (i == 0) {
      color = const Color(0xFFff5757);
    } else {
      color = const Color(0xFF48c100);
    }
    list.add(
      new Container(
        decoration: BoxDecoration(
          border: Border.all(width: 2.0, color: NeutralColors.ice_blue),
          borderRadius: BorderRadius.all(
            Radius.circular(5.0),
            //         <--- border radius here
          ),
        ),
        margin: EdgeInsets.only(top: 10 / 720 * screenHeight),
        child: Container(
          child: IntrinsicHeight(
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                Container(
                  margin: EdgeInsets.only(left: 0),
                  width: 2 / 360 * screenWidth,
                  //height: 10,
                  decoration: BoxDecoration(
                    color: color,
                    borderRadius: BorderRadius.all(Radius.circular(1.5)),
                  ),
                  //color: color,
                ),
                Container(
                  width: 150 / 360 * screenWidth,
                  margin: EdgeInsets.only(
                      left: 13 / 360 * screenWidth,
                      top: 15 / 720 * screenHeight),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Align(
                        alignment: Alignment.topLeft,
                        child: Text(
                          name,
                          style: TextStyle(
                            color: NeutralColors.dark_navy_blue,
                            fontSize: 14 / 720 * screenHeight,
                            fontFamily: "IBMPlexSans",
                            fontWeight: FontWeight.w500,
                          ),
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(
                            top: 2 / 720 * screenHeight,
                            bottom: 15 / 720 * screenHeight),
                        child: Text(
                          item.subtitle,
                          style: TextStyle(
                            color: NeutralColors.blue_grey,
                            fontSize: 12 / 720 * screenHeight,
                            fontFamily: "IBMPlexSans",
                            fontWeight: FontWeight.w500,
                          ),
                        ),
                      )
                    ],
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(
                      left: 23 / 360 * screenWidth,
                      top: 15 / 720 * screenHeight),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Container(
                        child: Text(
                          score,
                          textAlign: TextAlign.left,
                          style: TextStyle(
                            color: color,
                            fontSize: 16 / 720 * screenHeight,
                            fontFamily: "IBMPlexSans",
                            fontWeight: FontWeight.w500,
                          ),
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(top: 2 / 720 * screenHeight),
                        child: Text(
                          "Score",
                          style: TextStyle(
                            color: NeutralColors.gunmetal,
                            fontSize: 12 / 720 * screenHeight,
                            fontFamily: "IBMPlexSans",
                            fontWeight: FontWeight.w500,
                          ),
                        ),
                      )
                    ],
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(
                      left: 20 / 360 * screenWidth,
                      top: 15 / 720 * screenHeight,
                      right: 10 / 360 * screenWidth),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Container(
                        child: Text(
                          percentile,
                          textAlign: TextAlign.left,
                          style: TextStyle(
                              color: color,
                              fontSize: 16 / 720 * screenHeight,
                              fontFamily: "IBMPlexSans",
                              fontWeight: FontWeight.w500),
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(top: 2 / 720 * screenHeight),
                        child: Text(
                          "Percentile",
                          style: TextStyle(
                              color: NeutralColors.gunmetal,
                              fontSize: 12 / 720 * screenHeight,
                              fontFamily: "IBMPlexSans",
                              fontWeight: FontWeight.w500),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
  return new Column(children: list);
}

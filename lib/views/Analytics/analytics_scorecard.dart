import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
//import 'package:flutter_circular_chart/flutter_circular_chart.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:imsindia/api/bloc/services.dart';
import 'package:imsindia/components/custom_DropDown_Analytics.dart' as firstDropDown;
import 'package:imsindia/components/custom_DropDown_Analytics_Second.dart' as secondDropDown;
import 'package:imsindia/components/primary_button_gradient.dart';
import 'package:imsindia/utils/Url.dart';
import 'package:imsindia/utils/colors.dart';
import 'package:imsindia/utils/text_styles.dart';
import 'package:imsindia/views/Analytics/analytic_simcatWidget.dart';
import 'package:imsindia/views/Analytics/analytics_efficiency_tracker.dart';
import 'package:imsindia/views/Analytics/analytics_performance_analytics.dart';
import 'package:imsindia/views/Analytics/analytics_scorecard_performance_summary.dart';
import 'package:imsindia/views/Analytics/analytics_sction_data.dart';
import 'package:intl/intl.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:imsindia/utils/global.dart' as global;
import 'package:imsindia/utils/svg_icons.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:shared_preferences/shared_preferences.dart';

var screenWidthTotal;
var screenHeightTotal;

var _safeAreaHorizontal;

var _safeAreaVertical;

var screenHeight;

var screenWidth;

class Data {
  String title;
  String subtitle;
  String score;
  String percentile;

  Data({
    this.title,
    this.subtitle,
    this.score,
    this.percentile,
  });
}

final data1 = Data(
  title: "Data Interpretation",
  subtitle: "Your score is 23 marks below cut off",
  score: "56",
  percentile: "56th",
);

final data2 = Data(
  title: "VA-RC",
  subtitle: "Your score is 23 marks above cut off",
  score: "56",
  percentile: "13th",
);

final data3 = Data(
  title: "Quantitative Ability",
  subtitle: "Your score is 23 marks above cut off",
  score: "56",
  percentile: "13th",
);

final List<Data> datas = [
  data1,
  data2,
  data3,
];

List<String> getScoreDataSectionName = [];
var getScoreDataSectionScore = [];
List<String> getFinalScoreDataSectionScore = [];
var getScoreDataSectionPercentile = [];
List<String> getFinalScoreDataSectionPercentile = [];
bool showPercentile;
bool showRank;
var percentile;
int rank;
var yourScore;
var diff;
var remaing;
var mCutOff;
var mYourScore;
var cutOff;
var totalScore;
var yourScorePercent;
var remainingValue;
//var sectionName = [];
//var sectionScore = [];
//var sectionPerctile = [];
var authorizationInvalidMsg;
var totalDataScorecard;
var getLearningModules = [];
var getLearningObjects = [];
var getLearningModulesId = [];
var getLearningObjectsId = [];
var finalListForLearningModules = [];
var listForLearningObjectsId = [];
var selectedLearningModule;
var selectedLearningModuleName;
var selectedLearningObject;
var totalDataDropDown;
var notSelectedLearningModule = ["Select Test"];
var lastTestId;
var selectedLearningObjectId;
var menuId;
var totalDataScorecardSection;
var totalDataScorecardStudentTestDetails;

class StatesService {
  static final List<String> venues = [
    'simCAT Proctored',
    'simCAT Proctored1',
    'simCAT Proctored2',
    'simCAT Proctored3',

  ];
  static final List<String> venues1 = [
    'simCAT 01',
    'simCAT 02',
    'simCAT 03',
  ];
  static  getSuggestions(String query) {
    //List<String> matches = List();
    //matches.addAll(venues);

    // matches.retainWhere((s) => s.toLowerCase().contains(query.toLowerCase()));
    return finalListForLearningModules;
  }
  static  getSuggestions1(String query) {
    //List<String> matches = List();
    //finalListForLearningObjects.addAll(getLearningObjects);

    // matches.retainWhere((s) => s.toLowerCase().contains(query.toLowerCase()));
    return getLearningObjects;
  }
}

class AnalyticsScorecard extends StatefulWidget {
  @override
  AnalyticsScorecardState createState() => AnalyticsScorecardState();
}

class AnalyticsScorecardState extends State<AnalyticsScorecard>
    with TickerProviderStateMixin {
  SharedPreferences prefs;
  List<Tab> tabList = List();
  List<bool> _isDisabled = [false, true];
  int _index;
  var testId;
  bool isLoading = true;
  var retureValue;
  int _currentIndex;

  TabController _tabController;
  ScrollController _scroll;
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  _handleTabSelection() {
    if (_isDisabled[_tabController.index]) {
      int index = _tabController.previousIndex;
      setState(() {
        _tabController.index = index;
      });
    }
//    setState(() {
//      _currentIndex = _tabController.index;
//      //print("index" + _currentIndex.toString());
//    });
    // entry.overlayEntry.remove();
  }

  // final GlobalKey<AnimatedCircularChartState> _chartKey =
  // new GlobalKey<AnimatedCircularChartState>();
  // List<CircularStackEntry> dataChart =[];
  bool onClickChange = false;
  String dropdownValue = "simCAT Proctored";
  String dropdownValueSimCAT = "simCAT 01";
  final TextEditingController _stateOfVenues = new TextEditingController();

  final TextEditingController _stateOfVenues1 = new TextEditingController();
  //AnalyticsScorecardState scoreCardObj= new AnalyticsScorecardState();
  var authorizationInvalidMsg;
  bool isLoadingDropDown = true;
  void getLists(Map t) async {
    if (global.headersWithAuthorizationKey['Authorization'] != '') {
      print("Course iddddddD"+courseID.toString());
      ApiService().getAPI(URL.ANALYTICS_API_LEARN_MODULE_LIST + courseID + "&id=" + menuId, t)
          .then((returnValue) {
        setState(() {
          //print(returnValue);
          getLearningModules = [];
          getLearningModulesId = [];
          listForLearningObjectsId = [];
          finalListForLearningModules = [];
          if (returnValue[0] == 'Data fetched successfully') {
            isLoadingDropDown = true;
            totalDataDropDown=returnValue[1]['data'];
            lastTestId = returnValue[1]['lastTestId'];
            testId = lastTestId;
            saveTestId(lastTestId);
            print("&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&"+lastTestId.toString());
            for (var index = 0; index < totalDataDropDown.length; index++) {
              if (totalDataDropDown[index]['componentType'] == "learningModule"){
                getLearningModules.add(totalDataDropDown[index]['component']['title']);
                getLearningModulesId.add(totalDataDropDown[index]['id']);
              }
            }
            for(var index = 0; index<totalDataDropDown.length;index++){
              if (totalDataDropDown[index]['componentType'] == "learningObject"){
                //finalListForLearningModules.add(totalData[index]['component']['title']);
                if(!listForLearningObjectsId.contains(totalDataDropDown[index]['parentId']))
                  listForLearningObjectsId.add(totalDataDropDown[index]['parentId']);
              }
            }
            for(var i = 0;i<totalDataDropDown.length;i++) {
              for (var index = 0; index <
                  listForLearningObjectsId.length; index++) {
                if (listForLearningObjectsId[index] ==
                    totalDataDropDown[i]['id']) {
                  finalListForLearningModules.add(
                      totalDataDropDown[i]['component']['title']);
                }
              }
            }
            if (lastTestId != null){
              getScoreCardData(t);
              for(var j = 0;j<totalDataDropDown.length;j++){
                if(lastTestId == totalDataDropDown[j]['id']){
                  selectedLearningObject = totalDataDropDown[j]['component']['title'];
                  selectedLearningModule = totalDataDropDown[j]['parentId'];
                  getSecondDropDownData(selectedLearningModule);
                  //print("Selected Learning Object"+ selectedLearningObject);
                  //print("Selected Learning Module"+ selectedLearningModule.toString());
                }
              }
              for(var index =0;index<totalDataDropDown.length;index++){
                if(selectedLearningModule == totalDataDropDown[index]['id']){
                  selectedLearningModuleName = totalDataDropDown[index]['component']['title'];
                  //print("Selected Learning Module"+ selectedLearningModuleName);
                }
              }
              //print("Selected Learning Module"+ selectedLearningModuleName);
              //print("Selected Learning Object"+ selectedLearningObject);
            }
            isLoadingDropDown = false;
          }else {
            authorizationInvalidMsg = returnValue[0];
            print("Invalid Message"+authorizationInvalidMsg.toString());
          }
        });
      });
    }
  }

  void getSecondDropDownData(String selectedLearningModule){

    setState(() {
      getLearningObjects = [];
      getLearningObjectsId = [];
    });
    //print("&&&&&&&&&&&Before&&&&&&&&&&&getLearningObjects"+getLearningObjects.toString());
    for (var i = 0; i < getLearningModulesId.length; i++) {
      for (var j = 0; j < totalDataDropDown.length; j++) {
        //print("selectedLearningModule"+selectedLearningModuleName.toString());
        //print("selectedLearningModule"+selectedLearningModule.toString());
        if (totalDataDropDown[j]['parentId'] == selectedLearningModule && totalDataDropDown[j]['componentType'] == "learningObject") {
          if(!getLearningObjects.contains(totalDataDropDown[j]['component']['title'])){
            setState(() {
              getLearningObjects.add(totalDataDropDown[j]['component']['title']);

            });
          }
          if(!getLearningObjectsId.contains(totalDataDropDown[j]['id'])){
            setState(() {
              getLearningObjectsId.add(totalDataDropDown[j]['id']);

            });
          }
        }
      }
    }
    //print("&&&&&&&&&&&AFter&&&&&&&&&&&getLearningObjects"+getLearningObjects.toString());
  }

  var getIsProtected;
  var getIsPerReport;
  var testTemplate;//added this lines for NMAT Tests
  var testTemplateForXat;
  var testTemplateForMICAT; //added this lines for MICAT Tests
  var testTemplateForTISSMAT; //added for TISSMAT tests
  void getScoreCardData(Map t) async {
    if (global.headersWithAuthorizationKey['Authorization'] != '') {
      print("Course iddddddD"+courseID.toString());
      print("Testttttt iddddddD"+testId.toString());
      ApiService().getAPI(URL.ANALYTICS_API_SCORECARD_DATA + courseID + "&testId=" + testId, t)
          .then((returnValue) {
        setState(() {
          isLoading = true;
          retureValue = returnValue[0];
          print("*******************************************returnValue"+returnValue.toString());
          if(returnValue[0]!= 'something wrong'){
            if (returnValue[0] == 'Success') {
              totalDataScorecard=returnValue[1]['data'];
              //print("*******************************************returnValue"+totalDataScorecard.toString());
              getScoreDataSectionName = [];
              getScoreDataSectionScore = [];
              getScoreDataSectionPercentile = [];
              getFinalScoreDataSectionScore = [];
              getFinalScoreDataSectionPercentile = [];
              totalDataScorecardSection = totalDataScorecard['sectionWise'];
              totalDataScorecardStudentTestDetails = totalDataScorecard['studentTestDetails'];
              getIsProtected = totalDataScorecard['testDetails']['component']['IsProctored'];
              getIsPerReport = totalDataScorecard['testDetails']['isPerReport'];
              print("Section Protected "+getIsProtected.toString());
              print("Section  Students Report"+getIsPerReport.toString());
              //added this lines for NMAT Tests(Start)
          
              if(totalDataScorecard['testDetails']['testTemplate']=="nmat-adaptive"){
                  testTemplate = true;
              }else{
                testTemplate = false;
              }
              //added this lines for NMAT Tests(End)
              if(totalDataScorecard['testDetails']['testTemplate']=="xat"){
                testTemplateForXat = true;
              }else{
                testTemplateForXat = false;
              }
              //added this lines for MICAT Tests(Start)
              if(totalDataScorecard['testDetails']['testTemplate']=="tcat2new"){
                testTemplateForMICAT = true;
              }else{
                testTemplateForMICAT = false;
              }
              //added this lines for MICAT Tests(End)

              //added this lines for TISSMAT Tests(Start)
              if(totalDataScorecard['testDetails']['testTemplate']=="cat2layout"){
                testTemplateForTISSMAT = true;
              }else{
                testTemplateForTISSMAT = false;
              }
              //added this lines for TISSMAT Tests(End)

              //setSectionData_sharePref();
              if(totalDataScorecardSection != null){
                for (var index = 0; index < totalDataScorecardSection.length; index++) {
                  getScoreDataSectionName.add(totalDataScorecard['sectionWise'][index]['sectionName']);
                  getScoreDataSectionScore.add(totalDataScorecard['sectionWise'][index]['score']);
                  getScoreDataSectionPercentile.add(totalDataScorecard['sectionWise'][index]['percentile']);
                  getFinalScoreDataSectionScore.add(getScoreDataSectionScore[index].toString());
                  getFinalScoreDataSectionPercentile.add(getScoreDataSectionPercentile[index].toString());
                }
                //print("Section Name"+getScoreDataSectionName.toString());
                //print("Section score"+getScoreDataSectionScore.toString());
                //print("Section percentile"+getScoreDataSectionPercentile.toString());
              }
              showPercentile = totalDataScorecard['testDetails']['showPercentile'];
              showRank = totalDataScorecard['testDetails']['showRank'];
              totalScore = totalDataScorecard['testDetails']['component']['totalScore'];
              if(totalDataScorecardStudentTestDetails!=null){
                percentile = totalDataScorecard['studentTestDetails']['percentile'];
                rank = totalDataScorecard['studentTestDetails']['rank'];
                yourScore = totalDataScorecard['studentTestDetails']['score'];
                cutOff = totalDataScorecard['studentTestDetails']['cutoff'];
              }
//            print("show percentile"+showPercentile.toString());
//            print("show rank"+showRank.toString());
//            print("show percentile"+percentile.toString());
//            print("show rank"+rank.toString());
//            print("djfhjdsjhfhjdjh"+getScoreDataSectionName.toString());
//            print("djfhjdsjhfhjdjh"+getFinalScoreDataSectionScore.toString());
//            print("djfhjdsjhfhjdjh"+getFinalScoreDataSectionPercentile.toString());
              //print("%%%%%%%%%%%%cutoff"+cutOff.toString());
              //print("%%%%%%%%%%%%yourScore"+yourScore.toString());
              mCutOff = double.parse(cutOff.toString());
              mYourScore = double.parse(yourScore.toString());
              diff = ( mCutOff - mYourScore )/totalScore * 100;
              yourScorePercent = (mYourScore/totalScore) * 100;
              remaing = mYourScore-mCutOff;
              remainingValue = 100 - (yourScorePercent + diff);
              // dataChart = <CircularStackEntry>[
              //   new CircularStackEntry(
              //     <CircularSegmentEntry>[
              //       new CircularSegmentEntry(remainingValue.toDouble(), Color(0xfff2f4f4), rankKey: 'Q1'),
              //       new CircularSegmentEntry(yourScorePercent.toDouble(), Color(0xff886dd7), rankKey: 'Q3'),
              //       new CircularSegmentEntry(diff.toDouble(), mCutOff>mYourScore?Colors.red:Colors.green, rankKey: 'Q2'),
              //       //    new CircularSegmentEntry(50.0, Colors.yellow, rankKey: 'Q4'),
              //     ],
              //     rankKey: 'Partes',
              //   ),
              // ];
              //_chartKey.currentState.updateData(dataChart);
              //print("data chart"+dataChart.toString());
              isLoading = false;
            } else {
              authorizationInvalidMsg = returnValue[0];

            }
          }

        });
      });
    }
  }

  void getCourseId() async {
    //print("&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&Coming getCourseId");
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      courseID = prefs.getString("courseId");

      menuId = prefs.getString("moduleId");

      global.getToken.then((t){
        //print("************************token"+t.toString());
        getLists(t);
        //testId = prefs.getString("testId");
        //print("************************testId" +testId);
        //getScoreCardData(t);

      });


    });
  }

  void getCourseIdScorecard() async {
    //print("&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&Coming getCourseId");
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      courseID = prefs.getString("courseId");
      menuId = prefs.getString("moduleId");
      global.getToken.then((t){
        //print("************************token"+t.toString());
        //getScoreCardData(t);

      });
    });
  }

  saveTestId(String id) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('testId',id);
  }

  @override
  void initState() {
    getScoreDataSectionName = [];
    getScoreDataSectionScore = [];
    getScoreDataSectionScore=[];
    getFinalScoreDataSectionScore =[];
    super.initState();
    getCourseId();
    //getScoreCardData();
    _scroll = new ScrollController();
    _tabController = new TabController(vsync: this, length: tabList.length);
    _index = 0;
    tabList.add(new Tab(
      child: Text(
        "Performance Analytics",
      ),
    ));
    setState(() {
      _tabController = new TabController(vsync: this, length: tabList.length);
      _tabController.addListener(_handleTabSelection);
      this._index = _index;
    });
  }
  @override
  void didChangeDependencies() {
    // TODO: implement didChangeDependencies
    super.didChangeDependencies();
    //print("hiiiiiiiiiiiiiiiii");
    //getCourseIdScorecard();
  }



  @override
  Widget build(BuildContext context) {
    var _mediaQueryData = MediaQuery.of(context);
    screenWidthTotal = _mediaQueryData.size.width;
    screenHeightTotal = _mediaQueryData.size.height;
    _safeAreaHorizontal =
        _mediaQueryData.padding.left + _mediaQueryData.padding.right;
    _safeAreaVertical =
        _mediaQueryData.padding.top + _mediaQueryData.padding.bottom;

    screenHeight = screenHeightTotal - _safeAreaVertical;
    screenWidth = screenWidthTotal - _safeAreaHorizontal;

    return Scaffold(
      body: authorizationInvalidMsg != null
          ? new Container(
//          margin: EdgeInsets.only(
//              top: (200.0 / 720) * screenHeight),
          child: Center(
              child: Text(
                   authorizationInvalidMsg.toString(),
              )
          )
      )
      :isLoadingDropDown
          ? new Container(
//          margin: EdgeInsets.only(
//              top: (200.0 / 720) * screenHeight),
          child:
          Center(child: CircularProgressIndicator()))
          :isLoadingDropDown == false   && lastTestId == null?
      Container(
//          margin: EdgeInsets.only(
//              top: (200.0 / 720) * screenHeight),
          child: Center(
            child: Text(
              "No Data",
            ),
          )
      ):
      Container(
        child: Column(
          //crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
        Container(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Container(
              //margin: EdgeInsets.only(top: 30/720*screenHeight),
              decoration: BoxDecoration(
                gradient: LinearGradient(
                  // Where the linear gradient begins and ends
                  begin: Alignment.topCenter,
                  end: Alignment.bottomCenter,
                  // Add one stop for each color. Stops should increase from 0 to 1
                  stops: [0.1, 0.5],
                  colors: [
                    // Colors are easy thanks to Flutter's Colors class.
                    NeutralColors.ice_blue,
                    Colors.white,
                  ],
                ),
                //     color: Color.fromARGB(255, 255, 255, 255),
                boxShadow: [
                  BoxShadow(
                    color: NeutralColors.ice_blue,
                    offset: Offset(0, 10),
                    blurRadius: 18,
                  ),
                ],
              ),

              //color: Colors.red,
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Container(
                    margin: EdgeInsets.only(top: 30/720*screenHeight,left: 20/360*screenWidth),
                    width: 220/360 * screenWidth,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Container(
                          child: Text(
                            selectedLearningModuleName != null?selectedLearningModuleName:"Last Taken Test",
                            overflow: TextOverflow.ellipsis,
                            style: TextStyle(
                              color: NeutralColors.dark_navy_blue,
                              fontSize: 18/720 * screenHeight,
                              fontFamily: "IBMPlexSans",
                              fontWeight: FontWeight.w700,
                            ),
                            textAlign: TextAlign.left,
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(bottom: 20/720 * screenHeight),
                          child: Text(
                            selectedLearningObject!=null?selectedLearningObject:"",
                            overflow: TextOverflow.ellipsis,
                            style: TextStyle(
                              color: NeutralColors.blue_grey,
                              fontSize: 14/720 * screenHeight,
                              fontFamily: "IBMPlexSans",
                              fontWeight: FontWeight.w500,
                            ),
                            textAlign: TextAlign.left,
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 30/720*screenHeight,right: 20/360*screenWidth,bottom: 20/720 * screenHeight),
                    //height: 40/720 * screenHeight,
                    width: 100/360*screenWidth,
                    child: PrimaryButtonGradient(

                      onTap: () {
                        setState(() {
                          (onClickChange == true)?
                          onClickChange = false:
                          onClickChange = true;
                        });
                        if(!onClickChange){
                          setState(() {
                            if(testId == null){
                              isLoading = false;
                            }else{
                              isLoading = true;
                            }

                            totalDataScorecard = null;
                            totalDataScorecardSection = null;
                            //print("On done toatalData"+totalData.toString());
                            getScoreDataSectionName = [];
                            getLearningModules = [];
                            print("**************** Selected Test"+testId.toString());
                           // print("On done getScoreDataSectionName"+getScoreDataSectionName.toString());
                            if(testId != null){
                              global.getToken.then((t){
                                //print("************************token"+t.toString());
                                getScoreCardData(t);
                                 print("On done After getScoreDataSectionName"+getScoreDataSectionName.toString());

                              });
                            }else{}


                          });
                        }
                        //AppRoutes.push(context, AnalyticsScorecard());
                      },
                      text: (onClickChange)?'DONE':'CHANGE',
                    ),
                  ),
                ],
              ),
            ),
            (onClickChange)?
            Container(
              alignment: Alignment.topCenter,

              //  color:Colors.white,
              //height: 70/720*screenHeight,
              // width: screenWidth,
              decoration: BoxDecoration(
                boxShadow: [
                  BoxShadow(
                    blurRadius: 13.0,
                    color: Colors.black.withOpacity(.03),
                    offset: Offset(6.0, 7.0),
                    // spreadRadius: 500.0,
                  ),
                ],
                //shape: BoxShape.rectangle,
                //border: Border.all(),
                color: Colors.white,
              ),
              //    margin: EdgeInsets.only(right: screenWidth/19,left: screenWidth/19),
              child:Container(
                //  color: Colors.green,
                margin: EdgeInsets.only(right: 19/360*screenWidth,left: 19/360*screenWidth,top: 10/720*screenHeight, bottom: 26/720 * screenHeight),

                child: Row(

                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: <Widget>[
                    Expanded(child: Padding(
                      padding: EdgeInsets.only(right:10/360*screenWidth),
                      child:

                      firstDropDown.DropDownFormField(
                        getImmediateSuggestions: true,
                        textFieldConfiguration: firstDropDown.TextFieldConfiguration(
                          controller: _stateOfVenues,
                          label: selectedLearningModuleName != null?selectedLearningModuleName:"Select Value",

                        ),
                        suggestionsBoxDecoration: firstDropDown.SuggestionsBoxDecoration(
                            dropDownHeight: 150,
                            borderRadius: new BorderRadius.circular(5.0),
                            color: Colors.white),
                        suggestionsCallback: (pattern) {
                          return finalListForLearningModules;
                        },
                        itemBuilder: (context, suggestion) {
                          return ListTile(
                            dense: true,
                            contentPadding: EdgeInsets.only(left: 10/360*screenWidth),
                            title: Text(
                              suggestion,
                              style: TextStyle(
                                fontFamily: "IBMPlexSans",
                                fontWeight: FontWeight.w400,
                                fontSize: (14 / 720) * screenHeight,
                                color: NeutralColors.bluey_grey,
                                textBaseline: TextBaseline.alphabetic,
                                letterSpacing: 0.0,
                                inherit: false,
                              ),
                            ),
                          );
                        },
                        hideOnLoading: true,
                        debounceDuration: Duration(milliseconds: 100),
                        transitionBuilder: (context, suggestionsBox, controller) {
                          return suggestionsBox;
                        },
                        onSuggestionSelected: (suggestion) {
                          //print('Sangeethaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa'+suggestion);
                          _stateOfVenues.text = suggestion;

                          //print("=======>>>>"+_stateOfVenues.text);
                          setState(() {
                            //selectedLearningModule = suggestion;
                            for (var j = 0; j < totalDataDropDown.length; j++) {
                              if (totalDataDropDown[j]['component']['title'] == suggestion) {
                                selectedLearningModule = totalDataDropDown[j]['id'];
                              }
                            }
                            selectedLearningModuleName = suggestion;
                            selectedLearningObject = "Select Test";
                            _stateOfVenues1.text = "Select Test";
                            testId = null;
//                            if(selectedLearningObject!=null){
//                              selectedLearningObject = "Select test";
//                            }
                          });
                          getSecondDropDownData(selectedLearningModule);
                        },
                        onSaved: (value) => {print("something")},
                      ),
                    ),
                    ),
                    Expanded(
                      child: Padding(
                        padding: EdgeInsets.only(left:10/360*screenWidth),
                        child: secondDropDown.DropDownFormField(
                          hideOnEmpty: true,
                          getImmediateSuggestions: true,
                          textFieldConfiguration: secondDropDown.TextFieldConfiguration(
                            controller: _stateOfVenues1,
                            label: selectedLearningObject != null?selectedLearningObject:"Select Test",
                          ),
                          suggestionsBoxDecoration: secondDropDown.SuggestionsBoxDecoration(
                              dropDownHeight: getLearningObjects.length == 1?50:150,
                              borderRadius: new BorderRadius.circular(5.0),
                              color: Colors.white),
                          suggestionsCallback: (pattern) {
                            return getLearningObjects;
                          },
                          itemBuilder: (context, suggestion) {
                            return ListTile(
                              dense: true,
                              contentPadding: EdgeInsets.only(left: 10/360*screenWidth),
                              title: Text(
                                suggestion,
                                style: TextStyle(
                                  fontFamily: "IBMPlexSans",
                                  fontWeight: FontWeight.w400,
                                  fontSize: (14 / 720) * screenHeight,
                                  color: NeutralColors.bluey_grey,
                                  textBaseline: TextBaseline.alphabetic,
                                  letterSpacing: 0.0,
                                  inherit: false,
                                ),
                              ),
                            );
                          },
                          hideOnLoading: true,
                          debounceDuration: Duration(milliseconds: 100),
                          transitionBuilder: (context, suggestionsBox, controller) {
                            return suggestionsBox;
                          },
                          onSuggestionSelected: (suggestion) {
                            _stateOfVenues1.text = suggestion;
                            setState(() {
                              for (var j = 0; j < totalDataDropDown.length; j++) {
                                if (totalDataDropDown[j]['component']['title'] == suggestion) {
                                  selectedLearningObjectId = totalDataDropDown[j]['id'];
                                  testId = selectedLearningObjectId;
                                  saveTestId(testId);
                                }
                              }
                              selectedLearningObject = suggestion;
                            });
                          },
                          onSaved: (value) => {//print("something" + selectedLearningObjectId.toString())
                             },
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ):Container(),
          ],
        )
      ),
            //simCATWidget(),
            isLoading && retureValue != "something wrong"
                ? new Container(
                margin: EdgeInsets.only(
                    top: (200.0 / 720) * screenHeight),
                child:
                Center(child: CircularProgressIndicator()))
                :
            totalDataScorecardSection == null || retureValue == "something wrong" || lastTestId == null ?
            Container(
                margin: EdgeInsets.only(top: 38/720 * screenHeight),
                child: Column(
                  children: <Widget>[
                    Center(
                      child: getSvgIcon.analyticsSvgIcon,
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 20/720 * screenHeight, left: 65/360 * screenWidth, right: 65/360 * screenWidth,bottom: 10/720 * screenHeight),
                      child: Text(
                        "You have not yet attempted the selected test",
                        style: TextStyle(
                          fontSize: 14/720 * screenHeight,
                          fontFamily: "IBMPlexSans",
                          fontWeight: FontWeight.w400,
                          color: NeutralColors.gunmetal,
                        ),
                        textAlign: TextAlign.center,
                      ),
                    )
                  ],
                )
            ):
            Expanded(
              child: SingleChildScrollView(
                child: Container(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: <Widget>[
//                        Container(
//                          margin: EdgeInsets.only(top: 20/720 * screenHeight),
//                          //color: Colors.red,
//                          child: Stack(
//                            alignment: Alignment.center,
//                             children: <Widget>[
//                               Container(
//                                 margin: EdgeInsets.only(top: 30 / 720 * screenHeight),
//                                 //    color:Colors.red,
//                                 width: 132 / 360 * screenWidth,
//                                 height: 133 / 720 * screenHeight,
//                                 child: AnimatedCircularChart(
//                                   key: _chartKey,
//                                   duration: Duration(milliseconds: 1000),
//                                   size: const Size(150.0, 150.0),
//                                   initialChartData: <CircularStackEntry>[
//                                     new CircularStackEntry(
//                                       <CircularSegmentEntry>[
//                                         new CircularSegmentEntry(remainingValue.toDouble(), Color(0xfff2f4f4), rankKey: 'Q1'),
//                                         new CircularSegmentEntry(yourScorePercent.toDouble(), Color(0xff886dd7), rankKey: 'Q3'),
//                                         new CircularSegmentEntry(diff.toDouble(), mCutOff>mYourScore?Colors.red:Colors.green, rankKey: 'Q2'),
//                                         //    new CircularSegmentEntry(50.0, Colors.yellow, rankKey: 'Q4'),
//                                       ],
//                                       rankKey: 'Partes',
//                                     ),
//                                   ],
//                                   chartType: CircularChartType.Radial,
//                                   labelStyle: TextStyle(
//                                     color: NeutralColors.purply,
//                                     //fontSize: 17,
//                                     fontSize: 43 / 720 * screenHeight,
//                                     fontFamily: "IBMPlexSans",
//                                     fontWeight: FontWeight.w500,
//                                   ),
//                                   holeLabel: yourScore.toString(),
//                                   holeRadius: 40,
//                                   startAngle: 108,
//                                   //       holeRadius: screenHeight/15,
//                                 ),
//                               ),
//                               Positioned(
//                                 left: 250 / 360 * screenWidth,
//                                 top: 50 / 720 * screenHeight,
//                                 right: 5 / 360 * screenWidth,
//                                 child: Container(
//                                   child: Text(
//                                     "Cut-off Score:",
//                                     style: TextStyle(
//                                       fontSize: 12,
//                                       fontWeight: FontWeight.w500,
//                                       fontFamily: "IBMPlexSans",
//                                       color: Colors.blueGrey,
//                                     ),
//                                   ),
//                                 ),
//                               ),
//                               Positioned(
//                                 left: 280 / 360 * screenWidth,
//                                 top: 67 / 720 * screenHeight,
//                                 right: 5 / 360 * screenWidth,
//                                 child: Container(
//                                   child: Text(
//                                     mCutOff.toString(),
//                                     style: TextStyle(
//                                       fontSize: 14 / 360 * screenWidth,
//                                       fontWeight: FontWeight.w500,
//                                       fontFamily: "IBMPlexSans",
//                                       fontStyle: FontStyle.normal,
//                                       color: NeutralColors.grapefruit,
//                                     ),
//                                   ),
//                                 ),
//                               ),
//                             ],
//                          ),
//                        ),
//                        Container(
//                          margin: EdgeInsets.only(
//                              left: 20 / 360 * screenWidth,
//                              right: 20 / 360 * screenWidth,
//                              top: 16 / 720 * screenHeight),
//                          child: Text(
//                            mCutOff>mYourScore?"You score is below cut-off by ${remaing.toString()} marks!":"You score is above cut-off by ${remaing.toString()} marks!",
//                            style: mCutOff>mYourScore?
//                            TextStyle(
//                              color: NeutralColors.grapefruit,
//                              fontSize: 14 / 720 * screenHeight,
//                              fontFamily: "IBMPlexSans",
//                              fontWeight: FontWeight.w500,
//                            ):
//                            TextStyle(
//                              color: Colors.green,
//                              fontSize: 14 / 720 * screenHeight,
//                              fontFamily: "IBMPlexSans",
//                              fontWeight: FontWeight.w500,
//                            ),
//                            textAlign: TextAlign.center,
//                          ),
//                        ),
                      showPercentile == true && showRank == true?Container(
                        height: 80 / 720 * screenHeight,
                        margin: EdgeInsets.only(
                            left: 20 / 360 * screenWidth,
                            top: 15 / 720 * screenHeight,
                            right: 20 / 360 * screenWidth),
                        decoration: BoxDecoration(
                          gradient: LinearGradient(
                            begin: Alignment(0, 0),
                            end: Alignment(1.049, 1.008),
                            stops: [
                              0,
                              1,
                            ],
                            colors: [
                              Color.fromARGB(255, 168, 174, 35).withOpacity(0.1),
                              Color.fromARGB(255, 201, 110, 216).withOpacity(0.1),
                            ],
                          ),
                          borderRadius: BorderRadius.all(Radius.circular(5)),
                        ),
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Container(
                              //width: 39/360 * screenWidth,
                              //height: 44/720 * screenHeight,
                              margin: EdgeInsets.only(
                                  left: 20 / 360 * screenHeight,
                                  top: 17 / 720 * screenHeight),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Container(
                                    child: Text(
                                      percentile.toString(),
                                      style: TextStyle(
                                        color: NeutralColors.dark_navy_blue,
                                        fontSize: 20 / 720 * screenHeight,
                                        fontFamily: "IBMPlexSans",
                                        fontWeight: FontWeight.w700,
                                      ),
                                      textAlign: TextAlign.left,
                                    ),
                                  ),
                                  Align(
                                    alignment: Alignment.topLeft,
                                    child: Container(
                                      margin: EdgeInsets.only(
                                          top: 6 / 720 * screenHeight,
                                          ),
                                      child: Text(
                                        "PERCENTILE",
                                        style: TextStyle(
                                          color: NeutralColors.gunmetal,
                                          fontSize: 12 / 720 * screenHeight,
                                          fontFamily: "IBMPlexSans",
                                          fontWeight: FontWeight.w500,
                                        ),
                                        textAlign: TextAlign.left,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.only(
                                  left: 28 / 360 * screenHeight,
                                  top: 16 / 720 * screenHeight),
                              child: Opacity(
                                opacity: 0.15,
                                child: Container(
                                  width: 1,
                                  height: 48 / 720 * screenHeight,
                                  decoration: BoxDecoration(
                                    color: Color.fromARGB(255, 122, 113, 213),
                                  ),
                                  child: Container(),
                                ),
                              ),
                            ),
                            Container(
                              //width: 66/360 * screenWidth,
                              //height: 56/720 * screenHeight,
                              margin: EdgeInsets.only(
                                  left: 28 / 360 * screenWidth,
                                  top: 17 / 720 * screenHeight,right: 10/360 * screenWidth),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    rank.toString(),
                                    style: TextStyle(
                                      color: NeutralColors.dark_navy_blue,
                                      fontSize: 20 / 720 * screenHeight,
                                      fontFamily: "IBMPlexSans",
                                      fontWeight: FontWeight.w700,
                                    ),
                                    textAlign: TextAlign.left,
                                  ),
                                  Align(
                                    alignment: Alignment.topLeft,
                                    child: Container(
                                      margin: EdgeInsets.only(
                                          top: 6 / 720 * screenHeight,
                                          ),
                                      child: Text(
                                        "ALL INDIA RANK",
                                        style: TextStyle(
                                          color: NeutralColors.gunmetal,
                                          fontSize: 12 / 720 * screenHeight,
                                          fontFamily: "IBMPlexSans",
                                          fontWeight: FontWeight.w500,
                                        ),
                                        textAlign: TextAlign.center,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ):Container(),
                  testTemplateForXat==true?Container(
                    margin: EdgeInsets.only(top: 20/720 * screenHeight, left: 20/360 * screenWidth, right: 20/360 * screenWidth),
                    child: Text(
                      "In line with the official exam criteria, GK scores have been excluded from the overall score and 0.10 marks are deducted for more than 8 unattempted questions across the remaining sections.",
                      style: TextStyle(
                        fontSize: 14/720 * screenHeight,
                        fontFamily: "IBMPlexSans",
                        fontWeight: FontWeight.w400,
                        color: NeutralColors.gunmetal,
                      ),
                      textAlign: TextAlign.left,
                    ),
                  ):Container(),
                  Container(
                    decoration: BoxDecoration(
                      border: Border.all(width: 2.0, color: NeutralColors.ice_blue),
                      borderRadius: BorderRadius.all(
                        Radius.circular(5.0),
                        //         <--- border radius here
                      ),
                    ),
                    margin: EdgeInsets.only(top: 20 / 720 * screenHeight,left: 20/360 * screenWidth,right: 20/360 * screenWidth),
                    child: Container(
                      child: IntrinsicHeight(
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          children: <Widget>[
                            Container(
                              margin: EdgeInsets.only(left: 0),
                              width: 2 / 360 * screenWidth,
                              //height: 10,
                              decoration: BoxDecoration(
                                color: Color(0xFFff5757),
                                borderRadius: BorderRadius.all(Radius.circular(1.5)),
                              ),
                              //color: color,
                            ),
                            Container(
                              width: 150 / 360 * screenWidth,
                              margin: EdgeInsets.only(
                                  left: 13 / 360 * screenWidth,
                                  top: 15 / 720 * screenHeight),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Align(
                                    alignment: Alignment.topLeft,
                                    child: Text(
                                      "Overall",
                                      style: TextStyle(
                                        color: NeutralColors.dark_navy_blue,
                                        fontSize: 14 / 720 * screenHeight,
                                        fontFamily: "IBMPlexSans",
                                        fontWeight: FontWeight.w500,
                                      ),
                                    ),
                                  ),
                                  Container(
                                    margin: EdgeInsets.only(
                                        top: 2 / 720 * screenHeight,
                                        bottom: 15 / 720 * screenHeight),
                                    child: Text(
                                      "",
                                      style: TextStyle(
                                        color: NeutralColors.blue_grey,
                                        fontSize: 12 / 720 * screenHeight,
                                        fontFamily: "IBMPlexSans",
                                        fontWeight: FontWeight.w500,
                                      ),
                                    ),
                                  )
                                ],
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.only(
                                  left: 23 / 360 * screenWidth,
                                  top: 15 / 720 * screenHeight),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Container(
                                    child: Text(
                                      yourScore.toString(),
                                      textAlign: TextAlign.left,
                                      style: TextStyle(
                                        color: Color(0xFFff5757),
                                        fontSize: 16 / 720 * screenHeight,
                                        fontFamily: "IBMPlexSans",
                                        fontWeight: FontWeight.w500,
                                      ),
                                    ),
                                  ),
                                  Container(
                                    margin: EdgeInsets.only(top: 2 / 720 * screenHeight),
                                    child: Text(
                                      "Score",
                                      style: TextStyle(
                                        color: NeutralColors.gunmetal,
                                        fontSize: 12 / 720 * screenHeight,
                                        fontFamily: "IBMPlexSans",
                                        fontWeight: FontWeight.w500,
                                      ),
                                    ),
                                  )
                                ],
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.only(
                                  left: 20 / 360 * screenWidth,
                                  top: 15 / 720 * screenHeight,
                                  right: 10 / 360 * screenWidth),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Container(
                                    child: Text(
                                      showPercentile == true?percentile.toString():"",
                                      textAlign: TextAlign.left,
                                      style: TextStyle(
                                          color: Color(0xFFff5757),
                                          fontSize: 16 / 720 * screenHeight,
                                          fontFamily: "IBMPlexSans",
                                          fontWeight: FontWeight.w500),
                                    ),
                                  ),
                                  Container(
                                    margin: EdgeInsets.only(top: 2 / 720 * screenHeight),
                                    child: Text(
                                      showPercentile == true?"Percentile":"",
                                      style: TextStyle(
                                          color: NeutralColors.gunmetal,
                                          fontSize: 12 / 720 * screenHeight,
                                          fontFamily: "IBMPlexSans",
                                          fontWeight: FontWeight.w500),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                      Container(
                        margin: EdgeInsets.only(
                            left: 20 / 360 * screenWidth,
                            right: 20 / 360 * screenWidth,
                            top: 10 / 720 * screenHeight),
                        child: AnalyticsSectionData(getScoreDataSectionName,getFinalScoreDataSectionScore,getFinalScoreDataSectionPercentile,showPercentile),
                      ),
//                        Container(
//                          height: 40 / 720 * screenHeight,
//                          width: 320 / 360 * screenWidth,
//                          decoration: BoxDecoration(
//                              color: NeutralColors.ice_blue,
//                              borderRadius:
//                                  BorderRadius.all(Radius.circular(5))),
//                          margin: EdgeInsets.only(top: 40 / 720 * screenHeight,left: 20/360 * screenWidth,right: 20/360 * screenWidth),
//                          child: ClipRRect(
//                            borderRadius: BorderRadius.all(Radius.circular(5)),
//                            child: new TabBar(
//                              controller: _tabController,
//                              indicatorColor: Colors.transparent,
//                              indicator: BoxDecoration(
//                                color: NeutralColors.purpleish_blue,
//                              ),
//                              labelColor: Colors.white,
//                              unselectedLabelColor: NeutralColors.blue_grey,
//                              indicatorSize: TabBarIndicatorSize.tab,
//                              tabs: tabList,
//                              isScrollable: true,
//                              unselectedLabelStyle: TextStyle(
//                                fontSize: 14 / 360 * screenWidth,
//                                fontFamily: "IBMPlexSans",
//                                fontWeight: FontWeight.w400,
//                              ),
//                              labelStyle: TextStyle(
//                                //color: Colors.white,
//                                fontSize: 14 / 360 * screenWidth,
//                                fontFamily: "IBMPlexSans",
//                                fontWeight: FontWeight.w500,
//                              ),
//                            ),
//                          ),
//                        ),
                      
                      (getIsProtected == true && getIsPerReport == true) ?
                      Container():
                      Center(
                        child:  Container(
                          height: 40/720 * screenHeight,
                          //width: 120/720 * screenWidth,
                          decoration: BoxDecoration(
                              color:  NeutralColors.purpleish_blue,
                              borderRadius:
                              BorderRadius.all(Radius.circular(5))),
                          margin: EdgeInsets.only(top: 40/720 * screenHeight,left: 60/360 * screenWidth,right: 60/360 * screenWidth),
                          child: Center(
                            child: Text(
                              "Performance Analytics",
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 14 / 360 * screenWidth,
                                fontFamily: "IBMPlexSans",
                                fontWeight: FontWeight.w500,
                              ),
                            ),
                          ),
                        ),
                      ) ,

//                      testTemplate == true ?
//                       Container(
//                                 margin: EdgeInsets.only(top: 38/720 * screenHeight),
//                                 child: Column(
//                                   children: <Widget>[
//                                     Center(
//                                       child: getSvgIcon.analyticsSvgIcon,
//                                     ),
//                                     Container(
//                                       margin: EdgeInsets.only(top: 20/720 * screenHeight, left: 65/360 * screenWidth, right: 65/360 * screenWidth,bottom: 10/720 * screenHeight),
//                                       child: Text(
//                                         "Not applicable as the selected test is adaptive with no negative marking and all questions should be attempted.",
//                                         style: TextStyle(
//                                           fontSize: 14/720 * screenHeight,
//                                           fontFamily: "IBMPlexSans",
//                                           fontWeight: FontWeight.w400,
//                                           color: NeutralColors.gunmetal,
//                                         ),
//                                         textAlign: TextAlign.center,
//                                       ),
//                                     )
//                                   ],
//                                 )
//                             )://added this lines for NMAT Tests
                      (getIsProtected == true && getIsPerReport == true) ?
                      Container():
                        Container(
                        height: 400/720 * screenHeight,
                        child:AnalyticsPerformanceTracker(selectedLearningObject,testTemplate,testTemplateForXat) ,
//                          child: new TabBarView(
//                            controller: _tabController,
//                            children: <Widget>[
//                              AnalyticsPerformanceTracker(),
//                              AnalyticsEfficiencyTracker(),
//                            ],
//                          ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
//            Container(
//              margin: EdgeInsets.only(top: 38/720 * screenHeight),
//              child: Column(
//                children: <Widget>[
//                  Center(
//                    child: getSvgIcon.analyticsSvgIcon,
//                  ),
//                  Container(
//                    margin: EdgeInsets.only(top: 20/720 * screenHeight, left: 65/360 * screenWidth, right: 65/360 * screenWidth,bottom: 10/720 * screenHeight),
//                    child: Text(
//                      "You have not attended any Full-Length Tests yet…",
//                      style: TextStyle(
//                        fontSize: 14/720 * screenHeight,
//                        fontFamily: "IBMPlexSans",
//                        fontWeight: FontWeight.w400,
//                        color: NeutralColors.gunmetal,
//                      ),
//                      textAlign: TextAlign.center,
//                    ),
//                  )
//                ],
//              )
//            ),
          ],
        ),
      ),
    );
  }
}

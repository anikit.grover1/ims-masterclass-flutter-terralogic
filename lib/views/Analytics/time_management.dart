import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:imsindia/components/custom_expansion_panel.dart';
import 'package:imsindia/components/custom_expansion_panel_analytics.dart';
import 'package:imsindia/utils/colors.dart';

var screenWidthTotal;
var screenHeightTotal;

var _safeAreaHorizontal;

var _safeAreaVertical;

var screenHeight;

var screenWidth;
var i;




class TimeManagemt extends StatefulWidget {
  var overalltimetakenforcorrectanswer;
  var overalltimetakenforincorrectanswer;
  var overalltimetakenforskippedanswer;
  var overalltotalnumberofquestionsforcorrectanswer;
  var overalltotalnumberofquestionsforincorrectanswer;
  var overalltotalnumberofquestionsforskippedanswer;
  var listofsectionname = [];
  var listofareanames = [];
  var listoftopicnames = [];
  var listoftotaltopictotalnumberofquestionsforcorrectanswer = [];
  var listoftotalareatotalnumberofquestionsforcorrectanswer = [];
  var listofsectiontotalnumberofquestionsforcorrectanswer = [];
  var listoftotaltopictotalnumberofquestionsforincorrectanswer = [];
  var listoftotalareatotalnumberofquestionsforincorrectanswer = [];
  var listofsectiontotalnumberofquestionsforincorrectanswer = [];
  var listoftotaltopictotalnumberofquestionsforskippedanswer = [];
  var listoftotalareatotalnumberofquestionsforskippedanswer = [];
  var listofsectiontotalnumberofquestionsforskippedanswer = [];

  var listoftotaltopictimetakenforcorrectanswer = [];
  var listoftotalareatimetakenforcorrectanswer = [];
  var listofsectiontimetakenforcorrectanswer = [];
  var listoftotaltopictimetakenforincorrectanswer = [];
  var listoftotalareatimetakenforincorrectanswer = [];
  var listofsectiontimetakenforincorrectanswer = [];
  var listoftotaltopictimetakenforskippedanswer = [];
  var listoftotalareatimetakenforskippedanswer = [];
  var listofsectiontimetakenforskippedanswer = [];
  TimeManagemt(
      this.overalltimetakenforcorrectanswer,
      this.overalltimetakenforincorrectanswer,
      this.overalltimetakenforskippedanswer,
      this.overalltotalnumberofquestionsforcorrectanswer,
      this.overalltotalnumberofquestionsforincorrectanswer,
      this.overalltotalnumberofquestionsforskippedanswer,
      this.listofsectionname,
      this.listofareanames,
      this.listoftopicnames,
      this.listoftotaltopictotalnumberofquestionsforcorrectanswer,
      this.listoftotalareatotalnumberofquestionsforcorrectanswer,
      this.listofsectiontotalnumberofquestionsforcorrectanswer,
      this.listoftotaltopictotalnumberofquestionsforincorrectanswer,
      this.listoftotalareatotalnumberofquestionsforincorrectanswer,
      this.listofsectiontotalnumberofquestionsforincorrectanswer,
      this.listoftotaltopictotalnumberofquestionsforskippedanswer,
      this.listoftotalareatotalnumberofquestionsforskippedanswer,
      this.listofsectiontotalnumberofquestionsforskippedanswer,
      this.listoftotaltopictimetakenforcorrectanswer,
      this.listoftotalareatimetakenforcorrectanswer,
      this.listofsectiontimetakenforcorrectanswer,
      this.listoftotaltopictimetakenforincorrectanswer,
      this.listoftotalareatimetakenforincorrectanswer,
      this.listofsectiontimetakenforincorrectanswer,
      this.listoftotaltopictimetakenforskippedanswer,
      this.listoftotalareatimetakenforskippedanswer,
      this.listofsectiontimetakenforskippedanswer,
      );
  @override
  TimeManagemtState createState() => TimeManagemtState();
}

class TimeManagemtState extends State<TimeManagemt> {
  bool connected;
  String testId;
  final GlobalKey<ScaffoldState> _scaffoldstate =
  new GlobalKey<ScaffoldState>();
  var refreshKey = GlobalKey<RefreshIndicatorState>();
  var connectivityResult;
//  var test = new Set();
  var section=new Map();
  var area= new Map();
  var areaName = new Map();
  var topicNames = new Map();
  var topic = new Map();
  var sectionName = ["All section"];
  String course;

  @override
  void initState() {
    super.initState();
    //calling api method
    //print('init state called');
  }
  String _printDuration(Duration duration) {
    if(duration!=null){
      String twoDigits(int n) {
        if (n >= 10) return "$n";
        return "0$n";
      }

      String twoDigitMinutes = twoDigits(duration.inMinutes.remainder(60));
      String twoDigitSeconds = twoDigits(duration.inSeconds.remainder(60));
      print("hours");
      print(twoDigits(duration.inHours));
      print(twoDigits(duration.inHours).runtimeType);

      if(twoDigits(duration.inHours).toString()=="00"){
        return "$twoDigitMinutes:$twoDigitSeconds";
      }else{
        return "${twoDigits(duration.inHours)}:$twoDigitMinutes:$twoDigitSeconds";
      }
    }else{
      return "";
    }

  }

  // on dragging page refresh function will be activated

  int activeMeterIndex;
  int activeMeterIndexForSecondExpansion;
  var value;

  @override
  Widget build(BuildContext context) {
    var _mediaQueryData = MediaQuery.of(context);
    screenWidthTotal = _mediaQueryData.size.width;
    screenHeightTotal = _mediaQueryData.size.height;
    _safeAreaHorizontal =
        _mediaQueryData.padding.left + _mediaQueryData.padding.right;
    _safeAreaVertical =
        _mediaQueryData.padding.top + _mediaQueryData.padding.bottom;

    screenHeight = screenHeightTotal - _safeAreaVertical;
    screenWidth = screenWidthTotal - _safeAreaHorizontal;
    TrackingScrollController _scrollController;

    return Scaffold(
      body: Column(
        children: <Widget>[
          Expanded(
            //height: 590 / 720 * screenHeight,
            child: SingleChildScrollView(
              child: Container(
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    /// section and section related filters
                    Container(
                      decoration: BoxDecoration(
                        color: Color.fromARGB(255, 255, 255, 255),
                        boxShadow: [
                          BoxShadow(
                            color: Color.fromARGB(61, 193, 188, 164),
                            offset: Offset(10, 0),
                            blurRadius: 22,
                          ),
                        ],
                      ),
                      width: screenWidthTotal / 3,
                      child: Column(
                        children: <Widget>[
                          Container(
                            child: ListView.builder(
                                physics: NeverScrollableScrollPhysics(),
                                //controller: _scrollController,
                                shrinkWrap: true,
                                itemCount: widget.listofsectionname.length+2,
                                itemBuilder: (context, k) {
                                  Color color;
                                  if (k  == 0) {
                                    color = Colors.white;
                                  } else {
                                    color = NeutralColors.sun_yellow
                                        .withOpacity(0.1);
                                  }
                                  if (k == 0) {
                                    /// section ......
                                    return Container(
                                      width: 85 / 360 * screenWidthTotal,
                                      height: 85/720*screenHeightTotal,
                                      margin: EdgeInsets.only(
                                          left: 10 / 360 * screenWidthTotal,
                                      ),
                                          /// section .....
                                      child: new Row(
                                      crossAxisAlignment: CrossAxisAlignment.center,
                                      mainAxisAlignment: MainAxisAlignment.start,
                                      children: <Widget>[
                                             Text(
                                              "Section",
                                              style: TextStyle(
                                                fontSize:
                                                10 / 720 * screenHeightTotal,
                                                fontFamily:
                                                "IBMPlexSans-Medium",
                                                fontWeight: FontWeight.w500,
                                                color: NeutralColors.blue_grey,
                                              ),
                                              textAlign: TextAlign.left,
                                              overflow: TextOverflow.ellipsis,
                                          ),
                                        ])
//                                          /// overall ......
//                                          Center(
//                                            child: Text(
//                                              "OVERALL",
//                                              style: TextStyle(
//                                                fontSize:
//                                                10 / 720 * screenHeightTotal,
//                                                fontFamily:
//                                                "IBMPlexSans-Medium",
//                                                fontWeight: FontWeight.w500,
//                                                color: NeutralColors.blue_grey,
//                                              ),
//                                              textAlign: TextAlign.left,
//                                              overflow: TextOverflow.ellipsis,
//                                            ),
//                                          )
                                    );
                                  }
                                  if(k==1){
                                    /// over all .....
                                   return Container(
                                      color : NeutralColors.sun_yellow
                                          .withOpacity(0.1),
                                      width: 185 / 360 * screenWidthTotal,
                                      height: 40/720*screenHeightTotal,

                                      padding: EdgeInsets.only(
                                        left: 10 / 360 * screenWidthTotal,
                                      ),
                                      /// overall .....
                                      child: new Row(
                                        crossAxisAlignment: CrossAxisAlignment.center,
                                        mainAxisAlignment: MainAxisAlignment.start,
                                        children: <Widget>[
                                          Text(
                                            "OVERALL",
                                            style: new TextStyle(
                                                fontSize: (12 /
                                                    720) *
                                                    screenHeightTotal,
                                                fontFamily:
                                                "IBMPlexSans",
                                                fontWeight:
                                                FontWeight
                                                    .w400,
                                                color: NeutralColors
                                                    .dark_navy_blue),
                                            overflow: TextOverflow.ellipsis,
                                          ),
                                        ],),

//                                          /// overall ......
//                                          Center(
//                                            child: Text(
//                                              "OVERALL",
//                                              style: TextStyle(
//                                                fontSize:
//                                                10 / 720 * screenHeightTotal,
//                                                fontFamily:
//                                                "IBMPlexSans-Medium",
//                                                fontWeight: FontWeight.w500,
//                                                color: NeutralColors.blue_grey,
//                                              ),
//                                              textAlign: TextAlign.left,
//                                              overflow: TextOverflow.ellipsis,
//                                            ),
//                                          )
                                    );
                                  } if(widget.listofsectionname[k-2] == "Pyschometric" || widget.listofsectionname[k-2] == "Descriptive" || widget.listofsectionname[k-2] == "Descriptive Writing"){
                                    return Container();
                                  }else {
                                    return
                                      Container(
                                        //color: color,
                                        child: Column(
                                          mainAxisAlignment: MainAxisAlignment.start,
                                          crossAxisAlignment: CrossAxisAlignment.start,
                                          children: <Widget>[
                                            Container(
                                              child: Row(
                                                mainAxisAlignment:
                                                MainAxisAlignment.start,
                                                children: <Widget>[
                                                  Expanded(
                                                    child: Container(
                                                      //  height: 50/720 * screenHeight,
                                                      //color: color,
                                                      child:
                                                      CustomExpansionPanelList(
                                                        expansionHeaderHeight:
                                                        50 / 720 * screenHeightTotal,
                                                        iconColor:
                                                        (activeMeterIndex == k)
                                                            ? NeutralColors
                                                            .blue_grey
                                                            : NeutralColors
                                                            .blue_grey,
                                                        backgroundColor1:
                                                        (activeMeterIndex == k)
                                                            ? color
                                                            : color,
                                                        backgroundColor2:
                                                        (activeMeterIndex == k)
                                                            ? color
                                                            : color,
                                                        expansionCallback:
                                                            (int index,
                                                            bool status) {
                                                          setState(() {
                                                            activeMeterIndex =
                                                            activeMeterIndex == k
                                                                ? null
                                                                : k;
                                                          });
                                                        },
                                                        children: [
                                                          new ExpansionPanel(


                                                            canTapOnHeader: true,
                                                            isExpanded:activeMeterIndex == k,
                                                            headerBuilder: (BuildContext
                                                            context,
                                                                bool
                                                                isExpanded) =>
                                                            new Container(
                                                              margin: EdgeInsets.only(
                                                                  left: 10 /
                                                                      360 *
                                                                      screenWidthTotal,
                                                                  top:15/720*screenHeightTotal,
                                                                  bottom: 15/720*screenHeightTotal),
                                                              child: new Text(
                                                                widget.listofsectionname[(k-2)].toString(),
                                                                style: new TextStyle(
                                                                    fontSize: (12 /
                                                                        720) *
                                                                        screenHeightTotal,
                                                                    fontFamily:
                                                                    "IBMPlexSans",
                                                                    fontWeight:
                                                                    FontWeight
                                                                        .w400,
                                                                    color: NeutralColors
                                                                        .dark_navy_blue),
                                                                overflow: TextOverflow.ellipsis,
                                                              ),
                                                              //padding: EdgeInsets.all(10.0),
                                                            ),
                                                            body: ListView.builder(
                                                                physics: NeverScrollableScrollPhysics(),
                                                                //controller: _scrollController,
                                                                shrinkWrap: true,
                                                                itemCount: widget.listofareanames[(k-2)].length,
                                                                itemBuilder: (context, j) {
//                                                              Color color;
//                                                              if (j % 2 == 0) {
//                                                                color = Colors.white;
//                                                              } else if (j % 2 == 1) {
//                                                                color = NeutralColors.sun_yellow
//                                                                    .withOpacity(0.1);
//                                                              }
                                                                  return Container(
                                                                    color: color,
                                                                    child: Column(
                                                                      children: <Widget>[
                                                                        Container(
                                                                          child: Row(
                                                                            mainAxisAlignment:
                                                                            MainAxisAlignment.start,
                                                                            children: <Widget>[
                                                                              Expanded(
                                                                                child: Container(
                                                                                  //height: 50/720 * screenHeight,
                                                                                  //color: color,
                                                                                  child:
                                                                                  CustomExpansionPanelList(
                                                                                    expansionHeaderHeight:
                                                                                    50 / 720 * screenHeightTotal,
                                                                                    iconColor:
                                                                                    (activeMeterIndexForSecondExpansion == j)
                                                                                        ? NeutralColors
                                                                                        .blue_grey
                                                                                        : NeutralColors
                                                                                        .blue_grey,
                                                                                    backgroundColor1:
                                                                                    (activeMeterIndexForSecondExpansion == j)
                                                                                        ? color
                                                                                        : color,
                                                                                    backgroundColor2:
                                                                                    (activeMeterIndexForSecondExpansion == j)
                                                                                        ? color
                                                                                        : color,
                                                                                    expansionCallback:
                                                                                        (int index,
                                                                                        bool status) {
                                                                                      setState(() {
                                                                                        activeMeterIndexForSecondExpansion =
                                                                                        activeMeterIndexForSecondExpansion == j
                                                                                            ? null
                                                                                            : j;
                                                                                      });
                                                                                    },
                                                                                    children: [
                                                                                      new ExpansionPanel(


                                                                                        canTapOnHeader: true,
                                                                                        isExpanded:activeMeterIndexForSecondExpansion == j,
                                                                                        headerBuilder: (BuildContext
                                                                                        context,
                                                                                            bool
                                                                                            isExpanded) =>
                                                                                        new Container(
                                                                                          margin: EdgeInsets.only(
                                                                                              left: 10 /
                                                                                                  360 *
                                                                                                  screenWidthTotal,
                                                                                              top:15/720*screenHeightTotal,
                                                                                              bottom: 15/720*screenHeightTotal),
                                                                                          child: new Text(
                                                                                            widget.listofareanames[(k-2)][j].toString(),
                                                                                            style: new TextStyle(
                                                                                                fontSize: (12 /
                                                                                                    720) *
                                                                                                    screenHeightTotal,
                                                                                                fontFamily:
                                                                                                "IBMPlexSans",
                                                                                                fontWeight:
                                                                                                FontWeight
                                                                                                    .w400,
                                                                                                color: NeutralColors
                                                                                                    .dark_navy_blue),
                                                                                            overflow: TextOverflow.ellipsis,
                                                                                          ),
                                                                                          //padding: EdgeInsets.all(10.0),
                                                                                        ),
                                                                                        body: new Container(
                                                                                          //color: color,
                                                                                          child: new ListView.builder(
                                                                                            shrinkWrap: true,
                                                                                            physics:
                                                                                            NeverScrollableScrollPhysics(),
//                                                                    itemExtent: (50.0 /
//                                                                        720) *
//                                                                        screenHeightTotal,
                                                                                            itemBuilder:
                                                                                                (BuildContext
                                                                                            context,
                                                                                                int i) {
                                                                                              return  Container(
                                                                                                height: 40/720*screenHeightTotal,
//                                                                                            margin: EdgeInsets.only(
//                                                                                                left: 10 / 360 * screenWidthTotal,
//                                                                                                ),

                                                                                                child: Text(
                                                                                                  widget.listoftopicnames[(k-2)][j][i].toString(),
                                                                                                  textAlign:
                                                                                                  TextAlign
                                                                                                      .left,
                                                                                                  style: new TextStyle(
                                                                                                      fontSize:
                                                                                                      (12 / 720) *
                                                                                                          screenHeightTotal,
                                                                                                      fontFamily:
                                                                                                      "IBMPlexSans",
                                                                                                      fontWeight:
                                                                                                      FontWeight
                                                                                                          .w400,
                                                                                                      color: NeutralColors
                                                                                                          .blue_grey),
                                                                                                  overflow: TextOverflow.ellipsis,
                                                                                                ),
                                                                                                padding: EdgeInsets.all(10.0),
                                                                                              );
                                                                                            },
                                                                                            itemCount: widget.listoftopicnames[(k-2)][j].length,
                                                                                          ),
                                                                                          // padding: EdgeInsets.all(10.0),
                                                                                        ),

                                                                                      ),
                                                                                    ],

                                                                                  ),
                                                                                  //padding: EdgeInsets.all(10.0),
                                                                                ),
                                                                              ),
                                                                            ],
                                                                          ),
                                                                        ),
                                                                      ],
                                                                    ),
                                                                  );
                                                                  //return Text(section.values.elementAt(index).keys.toString());
                                                                }),

                                                          ),
                                                        ],

                                                      ),
                                                      //padding: EdgeInsets.all(10.0),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ],
                                        ),
                                      );
                                  }

                                  //return Text(section.values.elementAt(index).keys.toString());
                                }),
                          ),
                        ],
                      ),
                    ),
                    /// data related to skipped correct and incorrect questions
                    Container(
                      decoration: BoxDecoration(
                        color: Color.fromARGB(255, 255, 255, 255),
                        boxShadow: [
                          BoxShadow(
                            color: Color.fromARGB(61, 193, 188, 164),
                            offset: Offset(10, 0),
                            blurRadius: 22,
                          ),
                        ],
                      ),
                      width: 240 / 360 * screenWidthTotal,
                      child: Column(
                        children: <Widget>[
                          Container(
                            child: Scrollbar(
                              child:SingleChildScrollView (
                                scrollDirection: Axis.horizontal,
                                physics: ScrollPhysics(),
                                child:
                                Container(
                                  width: 670/360*screenWidthTotal,
                                  decoration: BoxDecoration(
                                    color: Color.fromARGB(255, 255, 255, 255),
                                   // color: Colors.red,
                                    boxShadow: [
                                      BoxShadow(
                                        color: Color.fromARGB(61, 193, 188, 164),
                                        offset: Offset(10, 0),
                                        blurRadius: 22,
                                      ),
                                    ],
                                  ),
                                  //color: Colors.white,
                                  child: Column(
                                    children: <Widget>[
                                      ListView.builder(
                                          physics: NeverScrollableScrollPhysics(),
                                          //controller: _scrollController,
                                          shrinkWrap: true,
                                          itemCount: widget.listofsectiontimetakenforcorrectanswer.length+2,
                                          itemBuilder: (context, k) {
                                            //final item = datas[index];
                                            Color color;
                                            if (k == 0) {
                                              color = Colors.white;
                                            } else  {
                                              color = NeutralColors.sun_yellow
                                                  .withOpacity(0.1);
                                            }
                                            if (k == 0) {
                                              /// ********************HEADER FOR  SECTION AREA AND TOPIC LEVEL DATA*****************///
                                              return Container(
                                                height: 85/720*screenHeightTotal,
                                                child:
                                                    /// headers ..........
                                                    Row(
                                                      crossAxisAlignment: CrossAxisAlignment.center,
                                                      children: <Widget>[
                                                        /// for correct answer ...........
                                                        Column(
                                                          mainAxisAlignment: MainAxisAlignment.center,
                                                          children: <Widget>[
                                                            /// correct text ..............
                                                            Container(
                                                              margin: EdgeInsets.only(
                                                                  right: 10/screenWidth*screenWidthTotal,
                                                                  bottom: 15 /
                                                                      720 *
                                                                      screenHeightTotal),
                                                              child: Text(
                                                                "Correct",
                                                                style: TextStyle(
                                                                  fontSize: 10 /
                                                                      720 *
                                                                      screenHeightTotal,
                                                                  fontFamily:
                                                                  "IBMPlexSans-Medium",
                                                                  fontWeight:
                                                                  FontWeight.w500,
                                                                  color: NeutralColors
                                                                      .blue_grey,
                                                                ),
                                                                textAlign:
                                                                TextAlign.center,
                                                              ),
                                                            ),
                                                            Row(
                                                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                                              children: <Widget>[
                                                                Container(
                                                                  width: 70 / 360 * screenWidthTotal,
                                                                  child: Text(
                                                                    "Number",
                                                                    style: TextStyle(
                                                                      fontSize: 10 /
                                                                          720 *
                                                                          screenHeightTotal,
                                                                      fontFamily:
                                                                      "IBMPlexSans-Medium",
                                                                      fontWeight:
                                                                      FontWeight.w500,
                                                                      color: NeutralColors
                                                                          .blue_grey,
                                                                    ),
                                                                    textAlign:
                                                                    TextAlign.center,
                                                                  ),
                                                                ),
                                                                Container(
                                                                  width: 70 / 360 * screenWidthTotal,
                                                                  child: Text(
                                                                    "Total Time",
                                                                    style: TextStyle(
                                                                      fontSize: 10 /
                                                                          720 *
                                                                          screenHeightTotal,
                                                                      fontFamily:
                                                                      "IBMPlexSans-Medium",
                                                                      fontWeight:
                                                                      FontWeight.w500,
                                                                      color: NeutralColors
                                                                          .blue_grey,
                                                                    ),
                                                                    textAlign:
                                                                    TextAlign.center,
                                                                  ),
                                                                ),
                                                                Container(
                                                                  margin: EdgeInsets.only(right: 10/screenWidth*screenWidthTotal),
                                                                  width: 70 / 360 * screenWidthTotal,

                                                                  child: Text(
                                                                    "Average Time",
                                                                    style: TextStyle(
                                                                      fontSize: 10 /
                                                                          720 *
                                                                          screenHeightTotal,
                                                                      fontFamily:
                                                                      "IBMPlexSans-Medium",
                                                                      fontWeight:
                                                                      FontWeight.w500,
                                                                      color: NeutralColors
                                                                          .blue_grey,
                                                                    ),
                                                                    textAlign:
                                                                    TextAlign.center,
                                                                  ),
                                                                ),
                                                              ],
                                                            ),
                                                          ],
                                                        ),

                                                        /// divider between correct and incorrect layout
                                                        Container(
                                                          color:Colors.black,
                                                            height: 85/720*screenHeightTotal,
                                                            width: 1,
//                                                      margin: EdgeInsets.only(
//                                                          right: 15/360*screenWidthTotal,
//                                                          ),
                                                            )   ,

                                                        /// for incorrect answer .........
                                                        Column(
                                                          mainAxisAlignment: MainAxisAlignment.center,
                                                          children: <Widget>[
                                                            /// incorrect text ............
                                                            Container(
                                                              margin: EdgeInsets.only(
                                                                  right: 10/screenWidth*screenWidthTotal,
                                                                  bottom: 15 /
                                                                      720 *
                                                                      screenHeightTotal),
                                                              child: Text(
                                                                "InCorrect",
                                                                style: TextStyle(
                                                                  fontSize: 10 /
                                                                      720 *
                                                                      screenHeightTotal,
                                                                  fontFamily:
                                                                  "IBMPlexSans-Medium",
                                                                  fontWeight:
                                                                  FontWeight.w500,
                                                                  color: NeutralColors
                                                                      .blue_grey,
                                                                ),
                                                                textAlign:
                                                                TextAlign.center,
                                                              ),
                                                            ),
                                                            Row(
                                                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                                              children: <Widget>[
                                                                Container(
                                                                  width: 70 / 360 * screenWidthTotal,
                                                                  child: Text(
                                                                    "Number",
                                                                    style: TextStyle(
                                                                      fontSize: 10 /
                                                                          720 *
                                                                          screenHeightTotal,
                                                                      fontFamily:
                                                                      "IBMPlexSans-Medium",
                                                                      fontWeight:
                                                                      FontWeight.w500,
                                                                      color: NeutralColors
                                                                          .blue_grey,
                                                                    ),
                                                                    textAlign:
                                                                    TextAlign.center,
                                                                  ),
                                                                ),
                                                                Container(
                                                                  width: 70 / 360 * screenWidthTotal,
                                                                  child: Text(
                                                                    "Total Time",
                                                                    style: TextStyle(
                                                                      fontSize: 10 /
                                                                          720 *
                                                                          screenHeightTotal,
                                                                      fontFamily:
                                                                      "IBMPlexSans-Medium",
                                                                      fontWeight:
                                                                      FontWeight.w500,
                                                                      color: NeutralColors
                                                                          .blue_grey,
                                                                    ),
                                                                    textAlign:
                                                                    TextAlign.center,
                                                                  ),
                                                                ),
                                                                Container(
                                                                  margin: EdgeInsets.only(right: 10/screenWidth*screenWidthTotal),
                                                                  width: 70 / 360 * screenWidthTotal,

                                                                  child: Text(
                                                                    "Average Time",
                                                                    style: TextStyle(
                                                                      fontSize: 10 /
                                                                          720 *
                                                                          screenHeightTotal,
                                                                      fontFamily:
                                                                      "IBMPlexSans-Medium",
                                                                      fontWeight:
                                                                      FontWeight.w500,
                                                                      color: NeutralColors
                                                                          .blue_grey,
                                                                    ),
                                                                    textAlign:
                                                                    TextAlign.center,
                                                                  ),
                                                                ),                                                        ],
                                                            ),
                                                          ],
                                                        ),

                                                        /// divider between incorrect and skipped layout
                                                        Container(
                                                          color:Colors.black,
                                                          height: 85/720*screenHeightTotal,
                                                          width: 1,
//                                                      margin: EdgeInsets.only(
//                                                          right: 15/360*screenWidthTotal,
//                                                          ),
                                                        ),

                                                        /// for skipped question ----------
                                                        Column(
                                                          mainAxisAlignment: MainAxisAlignment.center,
                                                          children: <Widget>[

                                                            /// skipped text ............
                                                            Container(
                                                              margin: EdgeInsets.only(
                                                                  right: 10/screenWidth*screenWidthTotal,
                                                                  bottom: 15 /
                                                                      720 *
                                                                      screenHeightTotal),
                                                              child: Text(
                                                                "Skipped",
                                                                style: TextStyle(
                                                                  fontSize: 10 /
                                                                      720 *
                                                                      screenHeightTotal,
                                                                  fontFamily:
                                                                  "IBMPlexSans-Medium",
                                                                  fontWeight:
                                                                  FontWeight.w500,
                                                                  color: NeutralColors
                                                                      .blue_grey,
                                                                ),
                                                                textAlign:
                                                                TextAlign.center,
                                                              ),
                                                            ),
                                                            Row(
                                                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                                              children: <Widget>[
                                                                Container(
                                                                  width: 70 / 360 * screenWidthTotal,
                                                                  child: Text(
                                                                    "Number",
                                                                    style: TextStyle(
                                                                      fontSize: 10 /
                                                                          720 *
                                                                          screenHeightTotal,
                                                                      fontFamily:
                                                                      "IBMPlexSans-Medium",
                                                                      fontWeight:
                                                                      FontWeight.w500,
                                                                      color: NeutralColors
                                                                          .blue_grey,
                                                                    ),
                                                                    textAlign:
                                                                    TextAlign.center,
                                                                  ),
                                                                ),
                                                                Container(
                                                                  width: 70 / 360 * screenWidthTotal,
                                                                  child: Text(
                                                                    "Total Time",
                                                                    style: TextStyle(
                                                                      fontSize: 10 /
                                                                          720 *
                                                                          screenHeightTotal,
                                                                      fontFamily:
                                                                      "IBMPlexSans-Medium",
                                                                      fontWeight:
                                                                      FontWeight.w500,
                                                                      color: NeutralColors
                                                                          .blue_grey,
                                                                    ),
                                                                    textAlign:
                                                                    TextAlign.center,
                                                                  ),
                                                                ),
                                                                Container(
                                                                  margin: EdgeInsets.only(right: 10/screenWidth*screenWidthTotal),
                                                                  width: 70 / 360 * screenWidthTotal,

                                                                  child: Text(
                                                                    "Average Time",
                                                                    style: TextStyle(
                                                                      fontSize: 10 /
                                                                          720 *
                                                                          screenHeightTotal,
                                                                      fontFamily:
                                                                      "IBMPlexSans-Medium",
                                                                      fontWeight:
                                                                      FontWeight.w500,
                                                                      color: NeutralColors
                                                                          .blue_grey,
                                                                    ),
                                                                    textAlign:
                                                                    TextAlign.center,
                                                                  ),
                                                                ),                                                        ],
                                                            ),
                                                          ],
                                                        ),

                                                      ],
                                                    ),
//                                              /// overall level values .....
//                                              Row(
//                                                crossAxisAlignment: CrossAxisAlignment.center,
//                                                children: <Widget>[
//                                                  /// for correct answer ...........
//                                                  Row(
//                                                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
//                                                    children: <Widget>[
//                                                      /// total number of questions  for correct answer
//                                                      Container(
//                                                        width: 70 / 360 * screenWidthTotal,
//                                                        child: Text(
//                                                         widget.overalltotalnumberofquestionsforcorrectanswer.toString(),
//                                                          style: TextStyle(
//                                                            fontSize: 10 /
//                                                                720 *
//                                                                screenHeightTotal,
//                                                            fontFamily:
//                                                            "IBMPlexSans-Medium",
//                                                            fontWeight:
//                                                            FontWeight.w500,
//                                                            color: NeutralColors
//                                                                .blue_grey,
//                                                          ),
//                                                          textAlign:
//                                                          TextAlign.center,
//                                                        ),
//                                                      ),
//                                                      /// time taken for correct answer
//                                                      Container(
//                                                        width: 70 / 360 * screenWidthTotal,
//                                                        child: Text(
//                                                          widget.overalltimetakenforcorrectanswer==0?"00:00":_printDuration(Duration(seconds: (widget.overalltimetakenforcorrectanswer).round().toInt())),
//                                                          style: TextStyle(
//                                                            fontSize: 10 /
//                                                                720 *
//                                                                screenHeightTotal,
//                                                            fontFamily:
//                                                            "IBMPlexSans-Medium",
//                                                            fontWeight:
//                                                            FontWeight.w500,
//                                                            color: NeutralColors
//                                                                .blue_grey,
//                                                          ),
//                                                          textAlign:
//                                                          TextAlign.center,
//                                                        ),
//                                                      ),
//                                                      /// avg time taken for correct answer
//                                                      Container(
//                                                        margin: EdgeInsets.only(right: 10/screenWidth*screenWidthTotal),
//                                                        width: 70 / 360 * screenWidthTotal,
//
//                                                        child: Text(
//                                                          widget.overalltimetakenforcorrectanswer==0?"00:00":_printDuration(Duration(seconds: (widget.overalltimetakenforcorrectanswer/widget.overalltotalnumberofquestionsforcorrectanswer).round().round().toInt())),
//                                                          style: TextStyle(
//                                                            fontSize: 10 /
//                                                                720 *
//                                                                screenHeightTotal,
//                                                            fontFamily:
//                                                            "IBMPlexSans-Medium",
//                                                            fontWeight:
//                                                            FontWeight.w500,
//                                                            color: NeutralColors
//                                                                .blue_grey,
//                                                          ),
//                                                          textAlign:
//                                                          TextAlign.center,
//                                                        ),
//                                                      ),
//                                                    ],
//                                                  ),
//
//                                                  /// divider between correct and incorrect layout
//                                                  Container(
//                                                    color:Colors.black,
//                                                    height: 85/720*screenHeightTotal,
//                                                    width: 1,
////                                                      margin: EdgeInsets.only(
////                                                          right: 15/360*screenWidthTotal,
////                                                          ),
//                                                  )   ,
//
//                                                  /// for incorrect answer .........
//                                                  Row(
//                                                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
//                                                    children: <Widget>[
//                                                      /// total number of questions  for incorrect answer
//                                                      Container(
//                                                        width: 70 / 360 * screenWidthTotal,
//                                                        child: Text(
//                                                          widget.overalltotalnumberofquestionsforincorrectanswer.toString(),
//                                                          style: TextStyle(
//                                                            fontSize: 10 /
//                                                                720 *
//                                                                screenHeightTotal,
//                                                            fontFamily:
//                                                            "IBMPlexSans-Medium",
//                                                            fontWeight:
//                                                            FontWeight.w500,
//                                                            color: NeutralColors
//                                                                .blue_grey,
//                                                          ),
//                                                          textAlign:
//                                                          TextAlign.center,
//                                                        ),
//                                                      ),
//                                                      /// time taken for incorrect answer
//                                                      Container(
//                                                        width: 70 / 360 * screenWidthTotal,
//                                                        child: Text(
//                                                          widget.overalltimetakenforincorrectanswer==0?"00:00":_printDuration(Duration(seconds: (widget.overalltimetakenforincorrectanswer).round().toInt())),
//                                                          style: TextStyle(
//                                                            fontSize: 10 /
//                                                                720 *
//                                                                screenHeightTotal,
//                                                            fontFamily:
//                                                            "IBMPlexSans-Medium",
//                                                            fontWeight:
//                                                            FontWeight.w500,
//                                                            color: NeutralColors
//                                                                .blue_grey,
//                                                          ),
//                                                          textAlign:
//                                                          TextAlign.center,
//                                                        ),
//                                                      ),
//                                                      /// avg time taken for incorrect answer
//                                                      Container(
//                                                        margin: EdgeInsets.only(right: 10/screenWidth*screenWidthTotal),
//                                                        width: 70 / 360 * screenWidthTotal,
//
//                                                        child: Text(
//                                                          widget.overalltimetakenforincorrectanswer==0?"00:00":_printDuration(Duration(seconds: (widget.overalltimetakenforincorrectanswer/widget.overalltotalnumberofquestionsforincorrectanswer).round().round().toInt())),
//                                                          style: TextStyle(
//                                                            fontSize: 10 /
//                                                                720 *
//                                                                screenHeightTotal,
//                                                            fontFamily:
//                                                            "IBMPlexSans-Medium",
//                                                            fontWeight:
//                                                            FontWeight.w500,
//                                                            color: NeutralColors
//                                                                .blue_grey,
//                                                          ),
//                                                          textAlign:
//                                                          TextAlign.center,
//                                                        ),
//                                                      ),
//                                                    ],
//                                                  ),
//
//                                                  /// divider between incorrect and skipped layout
//                                                  Container(
//                                                    color:Colors.black,
//                                                    height: 85/720*screenHeightTotal,
//                                                    width: 1,
////                                                      margin: EdgeInsets.only(
////                                                          right: 15/360*screenWidthTotal,
////                                                          ),
//                                                  ),
//
//                                                  /// for skipped question ----------
//                                                  Row(
//                                                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
//                                                    children: <Widget>[
//                                                      /// total number of questions  for skipped answer
//                                                      Container(
//                                                        width: 70 / 360 * screenWidthTotal,
//                                                        child: Text(
//                                                          widget.overalltotalnumberofquestionsforskippedanswer.toString(),
//                                                          style: TextStyle(
//                                                            fontSize: 10 /
//                                                                720 *
//                                                                screenHeightTotal,
//                                                            fontFamily:
//                                                            "IBMPlexSans-Medium",
//                                                            fontWeight:
//                                                            FontWeight.w500,
//                                                            color: NeutralColors
//                                                                .blue_grey,
//                                                          ),
//                                                          textAlign:
//                                                          TextAlign.center,
//                                                        ),
//                                                      ),
//                                                      /// time taken for skipped answer
//                                                      Container(
//                                                        width: 70 / 360 * screenWidthTotal,
//                                                        child: Text(
//                                                          widget.overalltimetakenforskippedanswer==0?"00:00":_printDuration(Duration(seconds: (widget.overalltimetakenforskippedanswer).round().toInt())),
//                                                          style: TextStyle(
//                                                            fontSize: 10 /
//                                                                720 *
//                                                                screenHeightTotal,
//                                                            fontFamily:
//                                                            "IBMPlexSans-Medium",
//                                                            fontWeight:
//                                                            FontWeight.w500,
//                                                            color: NeutralColors
//                                                                .blue_grey,
//                                                          ),
//                                                          textAlign:
//                                                          TextAlign.center,
//                                                        ),
//                                                      ),
//                                                      /// avg time taken for skipped answer
//                                                      Container(
//                                                        margin: EdgeInsets.only(right: 10/screenWidth*screenWidthTotal),
//                                                        width: 70 / 360 * screenWidthTotal,
//
//                                                        child: Text(
//                                                          widget.overalltimetakenforskippedanswer==0?"00:00":_printDuration(Duration(seconds: (widget.overalltimetakenforskippedanswer/widget.overalltotalnumberofquestionsforskippedanswer).round().round().toInt())),
//                                                          style: TextStyle(
//                                                            fontSize: 10 /
//                                                                720 *
//                                                                screenHeightTotal,
//                                                            fontFamily:
//                                                            "IBMPlexSans-Medium",
//                                                            fontWeight:
//                                                            FontWeight.w500,
//                                                            color: NeutralColors
//                                                                .blue_grey,
//                                                          ),
//                                                          textAlign:
//                                                          TextAlign.center,
//                                                        ),
//                                                      ),
//                                                    ],
//                                                  ),
//
//                                                ],
//                                              ),
                                              );
                                            }
                                            if  (k == 1){
                                             /// OVER ALL LEVEL VALUES *************************
                                             return Container(
                                                  height: 40/720*screenHeightTotal,
                                                  color: NeutralColors.sun_yellow.withOpacity(0.1),
                                                  child:Row(
                                                    crossAxisAlignment: CrossAxisAlignment.center,
                                                    children: <Widget>[
                                                      /// for correct answer ...........
                                                      Row(
                                                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                                        children: <Widget>[
                                                          /// total number of questions  for correct answer
                                                          Container(
                                                            width: 70 / 360 * screenWidthTotal,
                                                            child: Text(
                                                              widget.overalltotalnumberofquestionsforcorrectanswer.toString(),
                                                              style: TextStyle(
                                                                  fontSize: 12 /
                                                                      720 *
                                                                      screenHeightTotal,
                                                                  fontFamily:
                                                                  "IBMPlexSans",
                                                                  color:
                                                                  NeutralColors.gunmetal,
                                                                  fontWeight: FontWeight.w500),
                                                              textAlign:
                                                              TextAlign.center,
                                                            ),
                                                          ),
                                                          /// time taken for correct answer
                                                          Container(
                                                            width: 70 / 360 * screenWidthTotal,
                                                            child: Text(
                                                              widget.overalltimetakenforcorrectanswer==0?"00:00":_printDuration(Duration(seconds: (widget.overalltimetakenforcorrectanswer).round().toInt())),
                                                              style: TextStyle(
                                                                  fontSize: 12 /
                                                                      720 *
                                                                      screenHeightTotal,
                                                                  fontFamily:
                                                                  "IBMPlexSans",
                                                                  color:
                                                                  NeutralColors.gunmetal,
                                                                  fontWeight: FontWeight.w500),
                                                              textAlign:
                                                              TextAlign.center,
                                                            ),
                                                          ),
                                                          /// avg time taken for correct answer
                                                          Container(
                                                            margin: EdgeInsets.only(right: 10/screenWidth*screenWidthTotal),
                                                            width: 70 / 360 * screenWidthTotal,
                                                            child: Text(
                                                              widget.overalltimetakenforcorrectanswer==0?"00:00":_printDuration(Duration(seconds: (widget.overalltimetakenforcorrectanswer/widget.overalltotalnumberofquestionsforcorrectanswer).round().round().toInt())),
                                                              style: TextStyle(
                                                                  fontSize: 12 /
                                                                      720 *
                                                                      screenHeightTotal,
                                                                  fontFamily:
                                                                  "IBMPlexSans",
                                                                  color:
                                                                  NeutralColors.gunmetal,
                                                                  fontWeight: FontWeight.w500),
                                                              textAlign:
                                                              TextAlign.center,
                                                            ),
                                                          ),
                                                        ],
                                                      ),

                                                      /// divider between correct and incorrect layout
                                                      Container(
                                                        color:Colors.black,
                                                        height: 85/720*screenHeightTotal,
                                                        width: 1,
//                                                      margin: EdgeInsets.only(
//                                                          right: 15/360*screenWidthTotal,
//                                                          ),
                                                      )   ,

                                                      /// for incorrect answer .........
                                                      Row(
                                                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                                        children: <Widget>[
                                                          /// total number of questions  for incorrect answer
                                                          Container(
                                                            width: 70 / 360 * screenWidthTotal,
                                                            child: Text(
                                                              widget.overalltotalnumberofquestionsforincorrectanswer.toString(),
                                                              style: TextStyle(
                                                                  fontSize: 12 /
                                                                      720 *
                                                                      screenHeightTotal,
                                                                  fontFamily:
                                                                  "IBMPlexSans",
                                                                  color:
                                                                  NeutralColors.gunmetal,
                                                                  fontWeight: FontWeight.w500),
                                                              textAlign:
                                                              TextAlign.center,
                                                            ),
                                                          ),
                                                          /// time taken for incorrect answer
                                                          Container(
                                                            width: 70 / 360 * screenWidthTotal,
                                                            child: Text(
                                                              widget.overalltimetakenforincorrectanswer==0?"00:00":_printDuration(Duration(seconds: (widget.overalltimetakenforincorrectanswer).round().toInt())),
                                                              style: TextStyle(
                                                                  fontSize: 12 /
                                                                      720 *
                                                                      screenHeightTotal,
                                                                  fontFamily:
                                                                  "IBMPlexSans",
                                                                  color:
                                                                  NeutralColors.gunmetal,
                                                                  fontWeight: FontWeight.w500),
                                                              textAlign:
                                                              TextAlign.center,
                                                            ),
                                                          ),
                                                          /// avg time taken for incorrect answer
                                                          Container(
                                                            margin: EdgeInsets.only(right: 10/screenWidth*screenWidthTotal),
                                                            width: 70 / 360 * screenWidthTotal,
                                                            child: Text(
                                                              widget.overalltimetakenforincorrectanswer==0?"00:00":_printDuration(Duration(seconds: (widget.overalltimetakenforincorrectanswer/widget.overalltotalnumberofquestionsforincorrectanswer).round().round().toInt())),
                                                              style: TextStyle(
                                                                  fontSize: 12 /
                                                                      720 *
                                                                      screenHeightTotal,
                                                                  fontFamily:
                                                                  "IBMPlexSans",
                                                                  color:
                                                                  NeutralColors.gunmetal,
                                                                  fontWeight: FontWeight.w500),
                                                              textAlign:
                                                              TextAlign.center,
                                                            ),
                                                          ),
                                                        ],
                                                      ),

                                                      /// divider between incorrect and skipped layout
                                                      Container(
                                                        color:Colors.black,
                                                        height: 85/720*screenHeightTotal,
                                                        width: 1,
//                                                      margin: EdgeInsets.only(
//                                                          right: 15/360*screenWidthTotal,
//                                                          ),
                                                      ),

                                                      /// for skipped question ----------
                                                      Row(
                                                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                                        children: <Widget>[
                                                          /// total number of questions  for skipped answer
                                                          Container(
                                                            width: 70 / 360 * screenWidthTotal,
                                                            child: Text(
                                                              widget.overalltotalnumberofquestionsforskippedanswer.toString(),
                                                              style: TextStyle(
                                                                  fontSize: 12 /
                                                                      720 *
                                                                      screenHeightTotal,
                                                                  fontFamily:
                                                                  "IBMPlexSans",
                                                                  color:
                                                                  NeutralColors.gunmetal,
                                                                  fontWeight: FontWeight.w500),
                                                              textAlign:
                                                              TextAlign.center,
                                                            ),
                                                          ),
                                                          /// time taken for skipped answer
                                                          Container(
                                                            width: 70 / 360 * screenWidthTotal,
                                                            child: Text(
                                                              widget.overalltimetakenforskippedanswer==0?"00:00":_printDuration(Duration(seconds: (widget.overalltimetakenforskippedanswer).round().toInt())),
                                                              style: TextStyle(
                                                                  fontSize: 12 /
                                                                      720 *
                                                                      screenHeightTotal,
                                                                  fontFamily:
                                                                  "IBMPlexSans",
                                                                  color:
                                                                  NeutralColors.gunmetal,
                                                                  fontWeight: FontWeight.w500),
                                                              textAlign:
                                                              TextAlign.center,
                                                            ),
                                                          ),
                                                          /// avg time taken for skipped answer
                                                          Container(
                                                            margin: EdgeInsets.only(right: 10/screenWidth*screenWidthTotal),
                                                            width: 70 / 360 * screenWidthTotal,
                                                            child: Text(
                                                              widget.overalltimetakenforskippedanswer==0?"00:00":_printDuration(Duration(seconds: (widget.overalltimetakenforskippedanswer/widget.overalltotalnumberofquestionsforskippedanswer).round().round().toInt())),
                                                              style: TextStyle(
                                                                  fontSize: 12 /
                                                                      720 *
                                                                      screenHeightTotal,
                                                                  fontFamily:
                                                                  "IBMPlexSans",
                                                                  color:
                                                                  NeutralColors.gunmetal,
                                                                  fontWeight: FontWeight.w500),
                                                              textAlign:
                                                              TextAlign.center,
                                                            ),
                                                          ),
                                                        ],
                                                      ),

                                                    ],
                                                  ));
                                            }
                                            /// ********************DATA ACCORDING TO SECTION AREA AND TOPIC LEVEL *****************///
                                            if(widget.listofsectionname[k-2] == "Pyschometric" || widget.listofsectionname[k-2] == "Descriptive" || widget.listofsectionname[k-2] == "Descriptive Writing"){
                                              return Container();
                                            }else {
                                              return Container(
                                                child: Column(
                                                  children: <Widget>[
                                                    Container(
                                                      //color: color,
                                                      child:
                                                      Row(
                                                        mainAxisAlignment: MainAxisAlignment.start,
                                                        children: <Widget>[
                                                          Expanded(
                                                            child: Container(
                                                              //margin: EdgeInsets.only(left: 11 / 360 * screenWidth),
                                                              child: CustomExpansionPanelLists(
                                                                expansionHeaderHeight: (50 / 720) * screenHeightTotal,
                                                                iconColor:  NeutralColors.blue_grey,
                                                                backgroundColor1:  color,
                                                                backgroundColor2:  color,
                                                                expansionCallback: (int index, bool status) {
                                                                  setState(() {
                                                                    activeMeterIndex = activeMeterIndex == k ? null : k;
                                                                  });
                                                                },
                                                                children: [
                                                                  new ExpansionPanel(
                                                                    canTapOnHeader: true,
                                                                    isExpanded: activeMeterIndex == k,
                                                                    headerBuilder: (BuildContext context, bool isExpanded) =>
                                                                    /// ********************SECTION LEVEL DATA *****************///
                                                                    new Container(
                                                                      //color: color,
                                                                      height: 50 / 720 * screenHeightTotal,
                                                                      child: Row(
                                                                        children: <
                                                                            Widget>[
                                                                          /// for correct answer  ................
                                                                          Row(
                                                                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                                                            children: <Widget>[
                                                                              Container(
                                                                                width: 70 /
                                                                                    360 *
                                                                                    screenWidthTotal,
                                                                                child:
                                                                                Text(
                                                                                  widget.listofsectiontotalnumberofquestionsforcorrectanswer[(k-2)].toString(),
                                                                                  style: TextStyle(
                                                                                      fontSize: 12 /
                                                                                          720 *
                                                                                          screenHeightTotal,
                                                                                      fontFamily:
                                                                                      "IBMPlexSans",
                                                                                      color:
                                                                                      NeutralColors.gunmetal,
                                                                                      fontWeight: FontWeight.w500),
                                                                                  textAlign:
                                                                                  TextAlign.center,
                                                                                ),
                                                                              ),
                                                                              Container(
                                                                                width: 70 /
                                                                                    360 *
                                                                                    screenWidthTotal,
                                                                                child:
                                                                                Text(
                                                                                  widget.listofsectiontimetakenforcorrectanswer[(k-2)] == 0?"00.00":_printDuration(Duration(seconds: (widget.listofsectiontimetakenforcorrectanswer[(k-2)]).round().toInt())),
                                                                                  style:
                                                                                  TextStyle(
                                                                                    fontSize: 12 /
                                                                                        720 *
                                                                                        screenHeightTotal,
                                                                                    fontFamily:
                                                                                    "IBMPlexSans",
                                                                                    fontWeight:
                                                                                    FontWeight.w500,
                                                                                    color:
                                                                                    NeutralColors.gunmetal,
                                                                                  ),
                                                                                  textAlign:
                                                                                  TextAlign.center,
                                                                                ),
                                                                              ),
                                                                              Container(
                                                                                width: 70 /
                                                                                    360 *
                                                                                    screenWidthTotal,
                                                                                margin: EdgeInsets.only(right: 10/screenWidth*screenWidthTotal),
                                                                                child:
                                                                                Text(
                                                                                  widget.listofsectiontimetakenforcorrectanswer[(k-2)]==0?"00:00":_printDuration(Duration(seconds: (widget.listofsectiontimetakenforcorrectanswer[(k-2)]/widget.listofsectiontotalnumberofquestionsforcorrectanswer[(k-2)]).round().round().toInt())),
                                                                                  style:
                                                                                  TextStyle(
                                                                                    fontSize: 12 /
                                                                                        720 *
                                                                                        screenHeightTotal,
                                                                                    fontFamily:
                                                                                    "IBMPlexSans",
                                                                                    fontWeight:
                                                                                    FontWeight.w500,
                                                                                    color:
                                                                                    NeutralColors.gunmetal,
                                                                                  ),
                                                                                  textAlign:
                                                                                  TextAlign.center,
                                                                                ),
                                                                              ),
                                                                            ],
                                                                          ),
                                                                          /// divider between correct and incorrect layout
                                                                          Container(
                                                                            color:Colors.black,
                                                                            height: 85/720*screenHeightTotal,
                                                                            width: 1,
//                                                      margin: EdgeInsets.only(
//                                                          right: 15/360*screenWidthTotal,
//                                                          ),
                                                                          )   ,
                                                                          /// for incorrect answer ...............
                                                                          Row(
                                                                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                                                            children: <Widget>[
                                                                              Container(
                                                                                width: 70 /
                                                                                    360 *
                                                                                    screenWidthTotal,
                                                                                child:
                                                                                Text(
                                                                                  widget.listofsectiontotalnumberofquestionsforincorrectanswer[(k-2)].toString(),
                                                                                  style: TextStyle(
                                                                                      fontSize: 12 /
                                                                                          720 *
                                                                                          screenHeightTotal,
                                                                                      fontFamily:
                                                                                      "IBMPlexSans",
                                                                                      color:
                                                                                      NeutralColors.gunmetal,
                                                                                      fontWeight: FontWeight.w500),
                                                                                  textAlign:
                                                                                  TextAlign.center,
                                                                                ),
                                                                              ),
                                                                              Container(
                                                                                width: 70 /
                                                                                    360 *
                                                                                    screenWidthTotal,
                                                                                child:
                                                                                Text(
                                                                                  widget.listofsectiontimetakenforincorrectanswer[(k-2)] == 0?"00.00":_printDuration(Duration(seconds: (widget.listofsectiontimetakenforincorrectanswer[(k-2)]).round().toInt())),
                                                                                  style:
                                                                                  TextStyle(
                                                                                    fontSize: 12 /
                                                                                        720 *
                                                                                        screenHeightTotal,
                                                                                    fontFamily:
                                                                                    "IBMPlexSans",
                                                                                    fontWeight:
                                                                                    FontWeight.w500,
                                                                                    color:
                                                                                    NeutralColors.gunmetal,
                                                                                  ),
                                                                                  textAlign:
                                                                                  TextAlign.center,
                                                                                ),
                                                                              ),
                                                                              Container(
                                                                                width: 70 /
                                                                                    360 *
                                                                                    screenWidthTotal,
                                                                                margin: EdgeInsets.only(right: 10/screenWidth*screenWidthTotal),
                                                                                child:
                                                                                Text(
                                                                                  widget.listofsectiontimetakenforincorrectanswer[(k-2)]==0?"00:00":_printDuration(Duration(seconds: (widget.listofsectiontimetakenforincorrectanswer[(k-2)]/widget.listofsectiontotalnumberofquestionsforincorrectanswer[(k-2)]).round().toInt())),
                                                                                  style:
                                                                                  TextStyle(
                                                                                    fontSize: 12 /
                                                                                        720 *
                                                                                        screenHeightTotal,
                                                                                    fontFamily:
                                                                                    "IBMPlexSans",
                                                                                    fontWeight:
                                                                                    FontWeight.w500,
                                                                                    color:
                                                                                    NeutralColors.gunmetal,
                                                                                  ),
                                                                                  textAlign:
                                                                                  TextAlign.center,
                                                                                ),
                                                                              ),                                                                   ],
                                                                          ),
                                                                          /// divider between incorrect and skipped layout
                                                                          Container(
                                                                            color:Colors.black,
                                                                            height: 85/720*screenHeightTotal,
                                                                            width: 1,
//                                                      margin: EdgeInsets.only(
//                                                          right: 15/360*screenWidthTotal,
//                                                          ),
                                                                          )   ,
                                                                          /// for skipped answer .................
                                                                          Row(
                                                                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                                                            children: <Widget>[
                                                                              Container(
                                                                                width: 70 /
                                                                                    360 *
                                                                                    screenWidthTotal,
                                                                                child:
                                                                                Text(
                                                                                  widget.listofsectiontotalnumberofquestionsforskippedanswer[(k-2)].toString(),
                                                                                  style: TextStyle(
                                                                                      fontSize: 12 /
                                                                                          720 *
                                                                                          screenHeightTotal,
                                                                                      fontFamily:
                                                                                      "IBMPlexSans",
                                                                                      color:
                                                                                      NeutralColors.gunmetal,
                                                                                      fontWeight: FontWeight.w500),
                                                                                  textAlign:
                                                                                  TextAlign.center,
                                                                                ),
                                                                              ),
                                                                              Container(
                                                                                width: 70 /
                                                                                    360 *
                                                                                    screenWidthTotal,
                                                                                child:
                                                                                Text(
                                                                                  widget.listofsectiontimetakenforskippedanswer[(k-2)] == 0?"00.00":_printDuration(Duration(seconds: (widget.listofsectiontimetakenforskippedanswer[(k-2)]).round().toInt())),
                                                                                  style:
                                                                                  TextStyle(
                                                                                    fontSize: 12 /
                                                                                        720 *
                                                                                        screenHeightTotal,
                                                                                    fontFamily:
                                                                                    "IBMPlexSans",
                                                                                    fontWeight:
                                                                                    FontWeight.w500,
                                                                                    color:
                                                                                    NeutralColors.gunmetal,
                                                                                  ),
                                                                                  textAlign:
                                                                                  TextAlign.center,
                                                                                ),
                                                                              ),
                                                                              Container(
                                                                                width: 70 /
                                                                                    360 *
                                                                                    screenWidthTotal,
                                                                                margin: EdgeInsets.only(right: 10/screenWidth*screenWidthTotal),
                                                                                child:
                                                                                Text(
                                                                                  widget.listofsectiontimetakenforskippedanswer[(k-2)]==0?"00:00":_printDuration(Duration(seconds: (widget.listofsectiontimetakenforskippedanswer[(k-2)]/widget.listofsectiontotalnumberofquestionsforskippedanswer[(k-2)]).round().toInt())),
                                                                                  style:
                                                                                  TextStyle(
                                                                                    fontSize: 12 /
                                                                                        720 *
                                                                                        screenHeightTotal,
                                                                                    fontFamily:
                                                                                    "IBMPlexSans",
                                                                                    fontWeight:
                                                                                    FontWeight.w500,
                                                                                    color:
                                                                                    NeutralColors.gunmetal,
                                                                                  ),
                                                                                  textAlign:
                                                                                  TextAlign.center,
                                                                                ),
                                                                              ),                                                                        ],
                                                                          ),

                                                                        ],
                                                                      ),
                                                                    ),
                                                                    body: ListView.builder(
                                                                        physics: NeverScrollableScrollPhysics(),
                                                                        //controller: _scrollController,
                                                                        shrinkWrap: true,
                                                                        itemCount: widget.listoftotalareatimetakenforcorrectanswer[(k-2)].length,
                                                                        itemBuilder: (context, j) {
                                                                          //final item = datas[index];
//                                                                  Color color;
//                                                                  if (j % 2 == 0) {
//                                                                    color = Colors.white;
//                                                                  } else if (j % 2 == 1) {
//                                                                    color = NeutralColors.sun_yellow
//                                                                        .withOpacity(0.1);
//                                                                  }
                                                                          return Container(
                                                                            child: Column(
                                                                              children: <Widget>[
                                                                                Container(
                                                                                  color: color,
                                                                                  child: Row(
                                                                                    mainAxisAlignment:
                                                                                    MainAxisAlignment.start,
                                                                                    children: <Widget>[
                                                                                      Expanded(
                                                                                        child: Container(
                                                                                          //margin: EdgeInsets.only(left: 11 / 360 * screenWidth),
                                                                                          child:
                                                                                          CustomExpansionPanelLists(
                                                                                            expansionHeaderHeight: (50 / 720) * screenHeightTotal,
                                                                                            iconColor:  NeutralColors.blue_grey,
                                                                                            backgroundColor1:  color,
                                                                                            backgroundColor2:  color,
                                                                                            expansionCallback: (int index, bool status) {
                                                                                              setState(() {
                                                                                                activeMeterIndexForSecondExpansion = activeMeterIndexForSecondExpansion == j ? null : j;
                                                                                              });
                                                                                            },
                                                                                            children: [
                                                                                              new ExpansionPanel(
                                                                                                canTapOnHeader: true,
                                                                                                isExpanded: activeMeterIndexForSecondExpansion == j,
                                                                                                headerBuilder: (BuildContext context, bool isExpanded) =>

                                                                                                /// ********************AREA LEVEL DATA *****************///
                                                                                                new Container(
                                                                                                  //color: color,
                                                                                                  height: 50 /
                                                                                                      720 *
                                                                                                      screenHeightTotal,
                                                                                                  child: Row(
                                                                                                    mainAxisAlignment:
                                                                                                    MainAxisAlignment
                                                                                                        .start,
                                                                                                    mainAxisSize:
                                                                                                    MainAxisSize
                                                                                                        .max,
                                                                                                    children: <
                                                                                                        Widget>[
                                                                                                      /// for correct answer  ................
                                                                                                      Row(
                                                                                                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                                                                                        children: <Widget>[
                                                                                                          Container(
                                                                                                            width: 70 /
                                                                                                                360 *
                                                                                                                screenWidthTotal,
                                                                                                            child:
                                                                                                            Text(
                                                                                                              widget.listoftotalareatotalnumberofquestionsforcorrectanswer[(k-2)][j].toString(),
                                                                                                              style: TextStyle(
                                                                                                                  fontSize: 12 /
                                                                                                                      720 *
                                                                                                                      screenHeightTotal,
                                                                                                                  fontFamily:
                                                                                                                  "IBMPlexSans",
                                                                                                                  color:
                                                                                                                  NeutralColors.gunmetal,
                                                                                                                  fontWeight: FontWeight.w500),
                                                                                                              textAlign:
                                                                                                              TextAlign.center,
                                                                                                            ),
                                                                                                          ),
                                                                                                          Container(
                                                                                                            width: 70 /
                                                                                                                360 *
                                                                                                                screenWidthTotal,
                                                                                                            child:
                                                                                                            Text(
                                                                                                              widget.listoftotalareatimetakenforcorrectanswer[(k-2)][j]==0?"00:00":_printDuration(Duration(seconds: (widget.listoftotalareatimetakenforcorrectanswer[(k-2)][j]).round().toInt())),
                                                                                                              style:
                                                                                                              TextStyle(
                                                                                                                fontSize: 12 /
                                                                                                                    720 *
                                                                                                                    screenHeightTotal,
                                                                                                                fontFamily:
                                                                                                                "IBMPlexSans",
                                                                                                                fontWeight:
                                                                                                                FontWeight.w500,
                                                                                                                color:
                                                                                                                NeutralColors.gunmetal,
                                                                                                              ),
                                                                                                              textAlign:
                                                                                                              TextAlign.center,
                                                                                                            ),
                                                                                                          ),
                                                                                                          Container(
                                                                                                            width: 70 /
                                                                                                                360 *
                                                                                                                screenWidthTotal,
                                                                                                            margin: EdgeInsets.only(right: 10/screenWidth*screenWidthTotal),
                                                                                                            child:
                                                                                                            Text(
                                                                                                              widget.listoftotalareatimetakenforcorrectanswer[(k-2)][j]==0?"00:00":_printDuration(Duration(seconds: (widget.listoftotalareatimetakenforcorrectanswer[(k-2)][j]/widget.listoftotalareatotalnumberofquestionsforcorrectanswer[(k-2)][j]).round().toInt())),
                                                                                                              style:
                                                                                                              TextStyle(
                                                                                                                fontSize: 12 /
                                                                                                                    720 *
                                                                                                                    screenHeightTotal,
                                                                                                                fontFamily:
                                                                                                                "IBMPlexSans",
                                                                                                                fontWeight:
                                                                                                                FontWeight.w500,
                                                                                                                color:
                                                                                                                NeutralColors.gunmetal,
                                                                                                              ),
                                                                                                              textAlign:
                                                                                                              TextAlign.center,
                                                                                                            ),
                                                                                                          ),
                                                                                                        ],
                                                                                                      ),
                                                                                                      /// divider between correct and incorrect layout
                                                                                                      Container(
                                                                                                        color:Colors.black,
                                                                                                        height: 85/720*screenHeightTotal,
                                                                                                        width: 1,
//                                                      margin: EdgeInsets.only(
//                                                          right: 15/360*screenWidthTotal,
//                                                          ),
                                                                                                      )   ,
                                                                                                      /// for incorrect answer ...............
                                                                                                      Row(
                                                                                                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                                                                                        children: <Widget>[
                                                                                                          Container(
                                                                                                            width: 70 /
                                                                                                                360 *
                                                                                                                screenWidthTotal,
                                                                                                            child:
                                                                                                            Text(
                                                                                                              widget.listoftotalareatotalnumberofquestionsforincorrectanswer[(k-2)][j].toString(),
                                                                                                              style: TextStyle(
                                                                                                                  fontSize: 12 /
                                                                                                                      720 *
                                                                                                                      screenHeightTotal,
                                                                                                                  fontFamily:
                                                                                                                  "IBMPlexSans",
                                                                                                                  color:
                                                                                                                  NeutralColors.gunmetal,
                                                                                                                  fontWeight: FontWeight.w500),
                                                                                                              textAlign:
                                                                                                              TextAlign.center,
                                                                                                            ),
                                                                                                          ),
                                                                                                          Container(
                                                                                                            width: 70 /
                                                                                                                360 *
                                                                                                                screenWidthTotal,
                                                                                                            child:
                                                                                                            Text(
                                                                                                              widget.listoftotalareatimetakenforincorrectanswer[(k-2)][j]==0?"00:00":_printDuration(Duration(seconds: (widget.listoftotalareatimetakenforincorrectanswer[(k-2)][j]).round().toInt())),
                                                                                                              style:
                                                                                                              TextStyle(
                                                                                                                fontSize: 12 /
                                                                                                                    720 *
                                                                                                                    screenHeightTotal,
                                                                                                                fontFamily:
                                                                                                                "IBMPlexSans",
                                                                                                                fontWeight:
                                                                                                                FontWeight.w500,
                                                                                                                color:
                                                                                                                NeutralColors.gunmetal,
                                                                                                              ),
                                                                                                              textAlign:
                                                                                                              TextAlign.center,
                                                                                                            ),
                                                                                                          ),
                                                                                                          Container(
                                                                                                            width: 70 /
                                                                                                                360 *
                                                                                                                screenWidthTotal,
                                                                                                            margin: EdgeInsets.only(right: 10/screenWidth*screenWidthTotal),
                                                                                                            child:
                                                                                                            Text(
                                                                                                              widget.listoftotalareatimetakenforincorrectanswer[(k-2)][j]==0?"00:00":_printDuration(Duration(seconds: (widget.listoftotalareatimetakenforincorrectanswer[(k-2)][j]/widget.listoftotalareatotalnumberofquestionsforincorrectanswer[(k-2)][j]).round().toInt())),
                                                                                                              style:
                                                                                                              TextStyle(
                                                                                                                fontSize: 12 /
                                                                                                                    720 *
                                                                                                                    screenHeightTotal,
                                                                                                                fontFamily:
                                                                                                                "IBMPlexSans",
                                                                                                                fontWeight:
                                                                                                                FontWeight.w500,
                                                                                                                color:
                                                                                                                NeutralColors.gunmetal,
                                                                                                              ),
                                                                                                              textAlign:
                                                                                                              TextAlign.center,
                                                                                                            ),
                                                                                                          ),                                                                    ],
                                                                                                      ),
                                                                                                      /// divider between incorrect and skipped layout
                                                                                                      Container(
                                                                                                        color:Colors.black,
                                                                                                        height: 85/720*screenHeightTotal,
                                                                                                        width: 1,
//                                                      margin: EdgeInsets.only(
//                                                          right: 15/360*screenWidthTotal,
//                                                          ),
                                                                                                      )   ,
                                                                                                      /// for skipped answer .................
                                                                                                      Row(
                                                                                                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                                                                                        children: <Widget>[
                                                                                                          Container(
                                                                                                            width: 70 /
                                                                                                                360 *
                                                                                                                screenWidthTotal,
                                                                                                            child:
                                                                                                            Text(
                                                                                                              widget.listoftotalareatotalnumberofquestionsforskippedanswer[(k-2)][j].toString(),
                                                                                                              style: TextStyle(
                                                                                                                  fontSize: 12 /
                                                                                                                      720 *
                                                                                                                      screenHeightTotal,
                                                                                                                  fontFamily:
                                                                                                                  "IBMPlexSans",
                                                                                                                  color:
                                                                                                                  NeutralColors.gunmetal,
                                                                                                                  fontWeight: FontWeight.w500),
                                                                                                              textAlign:
                                                                                                              TextAlign.center,
                                                                                                            ),
                                                                                                          ),
                                                                                                          Container(
                                                                                                            width: 70 /
                                                                                                                360 *
                                                                                                                screenWidthTotal,
                                                                                                            child:
                                                                                                            Text(
                                                                                                              widget.listoftotalareatimetakenforskippedanswer[(k-2)][j]==0?"00:00":_printDuration(Duration(seconds: (widget.listoftotalareatimetakenforskippedanswer[(k-2)][j]).round().toInt())),
                                                                                                              style:
                                                                                                              TextStyle(
                                                                                                                fontSize: 12 /
                                                                                                                    720 *
                                                                                                                    screenHeightTotal,
                                                                                                                fontFamily:
                                                                                                                "IBMPlexSans",
                                                                                                                fontWeight:
                                                                                                                FontWeight.w500,
                                                                                                                color:
                                                                                                                NeutralColors.gunmetal,
                                                                                                              ),
                                                                                                              textAlign:
                                                                                                              TextAlign.center,
                                                                                                            ),
                                                                                                          ),
                                                                                                          Container(
                                                                                                            width: 70 /
                                                                                                                360 *
                                                                                                                screenWidthTotal,
                                                                                                            margin: EdgeInsets.only(right: 10/screenWidth*screenWidthTotal),
                                                                                                            child:
                                                                                                            Text(
                                                                                                              widget.listoftotalareatimetakenforskippedanswer[(k-2)][j]==0?"00:00":_printDuration(Duration(seconds: (widget.listoftotalareatimetakenforskippedanswer[(k-2)][j]/widget.listoftotalareatotalnumberofquestionsforskippedanswer[(k-2)][j]).round().toInt())),
                                                                                                              style:
                                                                                                              TextStyle(
                                                                                                                fontSize: 12 /
                                                                                                                    720 *
                                                                                                                    screenHeightTotal,
                                                                                                                fontFamily:
                                                                                                                "IBMPlexSans",
                                                                                                                fontWeight:
                                                                                                                FontWeight.w500,
                                                                                                                color:
                                                                                                                NeutralColors.gunmetal,
                                                                                                              ),
                                                                                                              textAlign:
                                                                                                              TextAlign.center,
                                                                                                            ),
                                                                                                          ),                                                                      ],
                                                                                                      ),

                                                                                                    ],
                                                                                                  ),
                                                                                                ),
                                                                                                body:
                                                                                                new Container(
                                                                                                  child: new ListView.builder(
                                                                                                    shrinkWrap: true,
                                                                                                    physics: NeverScrollableScrollPhysics(),
//                                                                          itemExtent: (50.0 /
//                                                                              720) *
//                                                                              screenHeightTotal,
                                                                                                    itemBuilder: (BuildContext context, int i) {
                                                                                                      /// ********************TOPIC LEVEL DATA *****************///
                                                                                                      return new Container(
                                                                                                        // color: Colors.red,
                                                                                                        child:
                                                                                                        Container(
                                                                                                          //color: Colors.yellow,
                                                                                                          height: 40/720*screenHeightTotal,
                                                                                                          child: Row(
                                                                                                            children: <
                                                                                                                Widget>[

                                                                                                              /// for correct answer  ................
                                                                                                              Row(
                                                                                                                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                                                                                                children: <Widget>[
                                                                                                                  Container(
                                                                                                                    width: 70 /
                                                                                                                        360 *
                                                                                                                        screenWidthTotal,
                                                                                                                    child:
                                                                                                                    Text(
                                                                                                                      widget.listoftotaltopictotalnumberofquestionsforcorrectanswer[(k-2)][j][i].toString(),
                                                                                                                      style: TextStyle(
                                                                                                                          fontSize: 12 /
                                                                                                                              720 *
                                                                                                                              screenHeightTotal,
                                                                                                                          fontFamily:
                                                                                                                          "IBMPlexSans",
                                                                                                                          color:
                                                                                                                          NeutralColors.gunmetal,
                                                                                                                          fontWeight: FontWeight.w500),
                                                                                                                      textAlign:
                                                                                                                      TextAlign.center,
                                                                                                                    ),
                                                                                                                  ),
                                                                                                                  Container(
                                                                                                                    width: 70 /
                                                                                                                        360 *
                                                                                                                        screenWidthTotal,
                                                                                                                    child:
                                                                                                                    Text(
                                                                                                                      widget.listoftotaltopictimetakenforcorrectanswer[(k-2)][j][i]==0?"00:00":_printDuration(Duration(seconds: (widget.listoftotaltopictimetakenforcorrectanswer[(k-2)][j][i]).round().toInt())),
                                                                                                                      style:
                                                                                                                      TextStyle(
                                                                                                                        fontSize: 12 /
                                                                                                                            720 *
                                                                                                                            screenHeightTotal,
                                                                                                                        fontFamily:
                                                                                                                        "IBMPlexSans",
                                                                                                                        fontWeight:
                                                                                                                        FontWeight.w500,
                                                                                                                        color:
                                                                                                                        NeutralColors.gunmetal,
                                                                                                                      ),
                                                                                                                      textAlign:
                                                                                                                      TextAlign.center,
                                                                                                                    ),
                                                                                                                  ),
                                                                                                                  Container(
                                                                                                                    width: 70 /
                                                                                                                        360 *
                                                                                                                        screenWidthTotal,
                                                                                                                    margin: EdgeInsets.only(right: 10/screenWidth*screenWidthTotal),
                                                                                                                    child:
                                                                                                                    Text(
                                                                                                                      widget.listoftotaltopictimetakenforcorrectanswer[(k-2)][j][i]==0?"00:00":_printDuration(Duration(seconds: (widget.listoftotaltopictimetakenforcorrectanswer[(k-2)][j][i]/widget.listoftotaltopictotalnumberofquestionsforcorrectanswer[(k-2)][j][i]).round().toInt())),
                                                                                                                      style:
                                                                                                                      TextStyle(
                                                                                                                        fontSize: 12 /
                                                                                                                            720 *
                                                                                                                            screenHeightTotal,
                                                                                                                        fontFamily:
                                                                                                                        "IBMPlexSans",
                                                                                                                        fontWeight:
                                                                                                                        FontWeight.w500,
                                                                                                                        color:
                                                                                                                        NeutralColors.gunmetal,
                                                                                                                      ),
                                                                                                                      textAlign:
                                                                                                                      TextAlign.center,
                                                                                                                    ),
                                                                                                                  ),
                                                                                                                ],
                                                                                                              ),
                                                                                                              /// divider between correct and incorrect layout
                                                                                                              Container(
                                                                                                                color:Colors.black,
                                                                                                                height: 40/720*screenHeightTotal,
                                                                                                                width: 1,
//                                                      margin: EdgeInsets.only(
//                                                          right: 15/360*screenWidthTotal,
//                                                          ),
                                                                                                              )   ,
                                                                                                              /// for incorrect answer ...............
                                                                                                              Row(
                                                                                                                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                                                                                                children: <Widget>[
                                                                                                                  Container(
                                                                                                                    width: 70 /
                                                                                                                        360 *
                                                                                                                        screenWidthTotal,
                                                                                                                    child:
                                                                                                                    Text(
                                                                                                                      widget.listoftotaltopictotalnumberofquestionsforincorrectanswer[(k-2)][j][i].toString(),
                                                                                                                      style: TextStyle(
                                                                                                                          fontSize: 12 /
                                                                                                                              720 *
                                                                                                                              screenHeightTotal,
                                                                                                                          fontFamily:
                                                                                                                          "IBMPlexSans",
                                                                                                                          color:
                                                                                                                          NeutralColors.gunmetal,
                                                                                                                          fontWeight: FontWeight.w500),
                                                                                                                      textAlign:
                                                                                                                      TextAlign.center,
                                                                                                                    ),
                                                                                                                  ),
                                                                                                                  Container(
                                                                                                                    width: 70 /
                                                                                                                        360 *
                                                                                                                        screenWidthTotal,
                                                                                                                    child:
                                                                                                                    Text(
                                                                                                                      widget.listoftotaltopictimetakenforincorrectanswer[(k-2)][j][i]==0?"00:00":_printDuration(Duration(seconds: (widget.listoftotaltopictimetakenforincorrectanswer[(k-2)][j][i]).round().toInt())),
                                                                                                                      style:
                                                                                                                      TextStyle(
                                                                                                                        fontSize: 12 /
                                                                                                                            720 *
                                                                                                                            screenHeightTotal,
                                                                                                                        fontFamily:
                                                                                                                        "IBMPlexSans",
                                                                                                                        fontWeight:
                                                                                                                        FontWeight.w500,
                                                                                                                        color:
                                                                                                                        NeutralColors.gunmetal,
                                                                                                                      ),
                                                                                                                      textAlign:
                                                                                                                      TextAlign.center,
                                                                                                                    ),
                                                                                                                  ),
                                                                                                                  Container(
                                                                                                                    width: 70 /
                                                                                                                        360 *
                                                                                                                        screenWidthTotal,
                                                                                                                    margin: EdgeInsets.only(right: 10/screenWidth*screenWidthTotal),
                                                                                                                    child:
                                                                                                                    Text(
                                                                                                                      widget.listoftotaltopictimetakenforincorrectanswer[(k-2)][j][i]==0?"00:00":_printDuration(Duration(seconds: (widget.listoftotaltopictimetakenforincorrectanswer[(k-2)][j][i]/widget.listoftotaltopictotalnumberofquestionsforincorrectanswer[(k-2)][j][i]).round().toInt())),
                                                                                                                      style:
                                                                                                                      TextStyle(
                                                                                                                        fontSize: 12 /
                                                                                                                            720 *
                                                                                                                            screenHeightTotal,
                                                                                                                        fontFamily:
                                                                                                                        "IBMPlexSans",
                                                                                                                        fontWeight:
                                                                                                                        FontWeight.w500,
                                                                                                                        color:
                                                                                                                        NeutralColors.gunmetal,
                                                                                                                      ),
                                                                                                                      textAlign:
                                                                                                                      TextAlign.center,
                                                                                                                    ),
                                                                                                                  ),                                                                   ],
                                                                                                              ),
                                                                                                              /// divider between incorrect and skipped layout
                                                                                                              Container(
                                                                                                                color:Colors.black,
                                                                                                                height: 85/720*screenHeightTotal,
                                                                                                                width: 1,
//                                                      margin: EdgeInsets.only(
//                                                          right: 15/360*screenWidthTotal,
//                                                          ),
                                                                                                              )   ,
                                                                                                              /// for skipped answer .................
                                                                                                              Row(
                                                                                                                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                                                                                                children: <Widget>[
                                                                                                                  Container(
                                                                                                                    width: 70 /
                                                                                                                        360 *
                                                                                                                        screenWidthTotal,
                                                                                                                    child:
                                                                                                                    Text(
                                                                                                                      widget.listoftotaltopictotalnumberofquestionsforskippedanswer[(k-2)][j][i].toString(),
                                                                                                                      style: TextStyle(
                                                                                                                          fontSize: 12 /
                                                                                                                              720 *
                                                                                                                              screenHeightTotal,
                                                                                                                          fontFamily:
                                                                                                                          "IBMPlexSans",
                                                                                                                          color:
                                                                                                                          NeutralColors.gunmetal,
                                                                                                                          fontWeight: FontWeight.w500),
                                                                                                                      textAlign:
                                                                                                                      TextAlign.center,
                                                                                                                    ),
                                                                                                                  ),
                                                                                                                  Container(
                                                                                                                    width: 70 /
                                                                                                                        360 *
                                                                                                                        screenWidthTotal,
                                                                                                                    child:
                                                                                                                    Text(
                                                                                                                      widget.listoftotaltopictimetakenforskippedanswer[(k-2)][j][i]==0?"00:00":_printDuration(Duration(seconds: (widget.listoftotaltopictimetakenforskippedanswer[(k-2)][j][i]).round().toInt())),
                                                                                                                      style:
                                                                                                                      TextStyle(
                                                                                                                        fontSize: 12 /
                                                                                                                            720 *
                                                                                                                            screenHeightTotal,
                                                                                                                        fontFamily:
                                                                                                                        "IBMPlexSans",
                                                                                                                        fontWeight:
                                                                                                                        FontWeight.w500,
                                                                                                                        color:
                                                                                                                        NeutralColors.gunmetal,
                                                                                                                      ),
                                                                                                                      textAlign:
                                                                                                                      TextAlign.center,
                                                                                                                    ),
                                                                                                                  ),
                                                                                                                  Container(
                                                                                                                    width: 70 /
                                                                                                                        360 *
                                                                                                                        screenWidthTotal,
                                                                                                                    margin: EdgeInsets.only(right: 10/screenWidth*screenWidthTotal),
                                                                                                                    child:
                                                                                                                    Text(
                                                                                                                      widget.listoftotaltopictimetakenforskippedanswer[(k-2)][j][i]==0?"00:00":_printDuration(Duration(seconds: (widget.listoftotaltopictimetakenforskippedanswer[(k-2)][j][i]/widget.listoftotaltopictotalnumberofquestionsforskippedanswer[(k-2)][j][i]).round().toInt())),
                                                                                                                      style:
                                                                                                                      TextStyle(
                                                                                                                        fontSize: 12 /
                                                                                                                            720 *
                                                                                                                            screenHeightTotal,
                                                                                                                        fontFamily:
                                                                                                                        "IBMPlexSans",
                                                                                                                        fontWeight:
                                                                                                                        FontWeight.w500,
                                                                                                                        color:
                                                                                                                        NeutralColors.gunmetal,
                                                                                                                      ),
                                                                                                                      textAlign:
                                                                                                                      TextAlign.center,
                                                                                                                    ),
                                                                                                                  ),                                                                   ],
                                                                                                              ),

                                                                                                            ],

                                                                                                          ),


                                                                                                        ),

                                                                                                      );
                                                                                                    },
                                                                                                    itemCount: widget.listoftotaltopictimetakenforcorrectanswer[(k-2)][j].length,
                                                                                                  ),
                                                                                                  //  padding: EdgeInsets.all(10.0),
                                                                                                ),
                                                                                              ),
                                                                                            ],
                                                                                          ),
                                                                                        ),
                                                                                      ),
                                                                                    ],
                                                                                  ),
                                                                                ),
                                                                              ],
                                                                            ),
                                                                          );
                                                                        }),
                                                                  ),
                                                                ],
                                                              ),
                                                            ),
                                                          ),
                                                        ],
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              );
                                            }

                                          }),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

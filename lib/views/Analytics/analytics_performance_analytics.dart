import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:flutter_html/image_properties.dart';
import 'package:html/parser.dart';
import 'package:imsindia/api/bloc/services.dart';
import 'package:imsindia/components/custom_DropDown_Performance.dart';
import 'package:imsindia/components/custom_DropDown_Performance_Item.dart'
    as item;
import 'package:imsindia/components/custom_DropDown_Performance_topics.dart'
    as topics;
import 'package:imsindia/resources/strings/analytics_labels.dart';
import 'package:imsindia/utils/Url.dart';
import 'package:imsindia/utils/colors.dart';
import 'package:imsindia/utils/global.dart' as global;
import 'package:imsindia/utils/svg_images/channel_svg_icons.dart';
import 'package:imsindia/utils/text_styles.dart';
import 'package:imsindia/views/Analytics/analytics_scorecard_performance_summary.dart';
import 'package:imsindia/views/Analytics/time_management.dart';
import 'package:imsindia/views/home_pages/nav_bar_Dropdown.dart' as drop;
import 'package:shared_preferences/shared_preferences.dart';

class StatesService {
  static final List<String> venues = [
    'simCAT Proctored',
    'simCAT Proctored1',
    'simCAT Proctored2',
    'simCAT Proctored3',
  ];
  static final List<String> venues1 = [
    'simCAT 01',
    'simCAT 02',
    'simCAT 03',
  ];

  static getSuggestions(String query) {
    List<String> matches = List();
    matches.addAll(venues);

    matches.retainWhere((s) => s.toLowerCase().contains(query.toLowerCase()));
    return matches;
  }

  static getSuggestions1(String query) {
    List<String> matches = List();
    matches.addAll(venues1);

    matches.retainWhere((s) => s.toLowerCase().contains(query.toLowerCase()));
    return matches;
  }
}

enum ItemTypeTITA {
  BN,
  NE,
  AN,
}
enum ItemType {
  MCQ,
  MCA,
  TITA,
}
List<String> list = [
  'Leni Reifenstahl was: A filmmaker that reduced production of poetry, when compared to prose, stems not from the former’s superiority but because what was once considered a normal skill the composition of verse is now considered a niche and intimidating skill.',
  'Sony Laptops Are Still Part Of Leni Reifenstahl was: A filmmaker that reduced production of poetry, when compared to prose, stems not from the former’s superiority but because what was once considered a normal skill the composition of verse is now considered a niche and intimidating skill.',
  'Stu Unger Rise And Fall Of A Poker Geniu Leni Reifenstahl was: A filmmaker that reduced production of poetry, when compared to prose, stems not from the former’s superiority but because what was once considered a normal skill the composition of verse is now considered a niche and intimidating skill.',
  'Microsoft Patch Management For Hom Leni Reifenstahl was: A filmmaker that reduced production of poetry, when compared to prose, stems not from the former’s superiority but because what was once considered a normal skill the composition of verse is now considered a niche and intimidating skill.',
  'Microsoft Patch Management For HomeLeni Reifenstahl was: A filmmaker that reduced production of poetry, when compared to prose, stems not from the former’s superiority but because what was once considered a normal skill the composition of verse is now considered a niche and intimidating skill.',
  'Microsoft Patch Management For HomeLeni Reifenstahl was: A filmmaker that reduced production of poetry, when compared to prose, stems not from the former’s superiority but because what was once considered a normal skill the composition of verse is now considered a niche and intimidating skill.',
  'Microsoft Patch Management For HomeLeni Reifenstahl was: A filmmaker that reduced production of poetry, when compared to prose, stems not from the former’s superiority but because what was once considered a normal skill the composition of verse is now considered a niche and intimidating skill.',
  'Leni Reifenstahl was: A filmmaker that reduced production of poetry, when compared to prose, stems not from the former’s superiority but because what was once considered a normal skill the composition of verse is now considered a niche and intimidating skill.',
  'Sony Laptops Are Still Part Of Leni Reifenstahl was: A filmmaker that reduced production of poetry, when compared to prose, stems not from the former’s superiority but because what was once considered a normal skill the composition of verse is now considered a niche and intimidating skill.',
  'Stu Unger Rise And Fall Of A Poker Geniu Leni Reifenstahl was: A filmmaker that reduced production of poetry, when compared to prose, stems not from the former’s superiority but because what was once considered a normal skill the composition of verse is now considered a niche and intimidating skill.',
  'Microsoft Patch Management For Hom Leni Reifenstahl was: A filmmaker that reduced production of poetry, when compared to prose, stems not from the former’s superiority but because what was once considered a normal skill the composition of verse is now considered a niche and intimidating skill.',
  'Microsoft Patch Management For HomeLeni Reifenstahl was: A filmmaker that reduced production of poetry, when compared to prose, stems not from the former’s superiority but because what was once considered a normal skill the composition of verse is now considered a niche and intimidating skill.',
  'Microsoft Patch Management For HomeLeni Reifenstahl was: A filmmaker that reduced production of poetry, when compared to prose, stems not from the former’s superiority but because what was once considered a normal skill the composition of verse is now considered a niche and intimidating skill.',
  'Microsoft Patch Management For HomeLeni Reifenstahl was: A filmmaker that reduced production of poetry, when compared to prose, stems not from the former’s superiority but because what was once considered a normal skill the composition of verse is now considered a niche and intimidating skill.',
];

class Data {
  String area;
  List areaChildren;
  var marks;
  String attempts;
  var negativeMarks;
  int isCorrect;
  int isNotNull;
  int timeTaken;
  int timeTakenNotNull;
  int totalMarks = 0;
  String accuracy;
  List marksChildren;
  List attemptsChildren;
  List accuracyChildren;
  int questionIndex;
  /// data related to Time Management Tab ................
  int timeTakenForCorrectAnswer;
  int totalNumberOfQuestionsForCorrectAnswer;
  int timeTakenFonINCorrectAnswer;
  int totalNumberOfQuestionsForInCorrectAnswer;
  int timeTakenForSkippedAnswer;
  int totalNumberOfQuestionsForSkippedAnswer;
  /// end data related to Time Management Tab ................
  //String toString() => "negative:$negativeMarks" + "," + "marks:$marks";


  Data({
    this.area,
    this.marks,
    this.attempts,
    this.accuracy,
    this.isCorrect,
    this.isNotNull,
    this.timeTaken,
    this.timeTakenNotNull,
    this.totalMarks,
    this.areaChildren,
    this.marksChildren,
    this.attemptsChildren,
    this.accuracyChildren,
    this.questionIndex,
    this.negativeMarks,
    this.timeTakenForCorrectAnswer,
    this.totalNumberOfQuestionsForCorrectAnswer,
    this.timeTakenFonINCorrectAnswer,
    this.totalNumberOfQuestionsForInCorrectAnswer,
    this.timeTakenForSkippedAnswer,
    this.totalNumberOfQuestionsForSkippedAnswer,
  });
}

var screenWidthTotal;
var screenHeightTotal;
/// variable related to summary tab
var listofsectionname = [];
var listofarea = [];
var listofareanames = [];
var listoftopicnames = [];
var listoftopics = [];
var listoftopic = [];
var listofmarks = [];
var listoftotalmarks = [];
var listofnagativemarks = [];
var listoftotalnegativemarks = [];
var listoffinalmarks = [];
var listofiscorrectnotnull = [];
var listoftimespent = [];
var listoftimespentNotNull = [];
var listoflength = [];
var listoftotallength = [];
var listofareamarks = [];
var listofareamark = [];
var listoftopicmarks = [];
var listoftopicmark = [];
var listofarealength = [];
var listoftopiclength = [];
var listofattempts = [];
var listoftopicattempts = [];
var listoftopicattempt = [];
var listofareaattempts = [];
var listofareaattempt = [];
var listoftotaltimespent = [];
var listoftotaltimespentNotNull = [];
var listoftopictimespent = [];
var listoftopictimespentNotNull = [];
var listofareatimespent = [];
var listofareatimespentNotNull = [];
var listoftopicmarksperminute = [];
var listofaareamarksperminute = [];
var listofiscorrect = [];
var listofaccuracy = [];
var listoftopicaccuracy = [];
var listofareaaccuracy = [];
var listoftopicaccuracys = [];
var listoftopictimespents = [];
var listoftopictimespentNotNulls = [];
var listofareaaccuracys = [];
var listofareatimespents = [];
var listofareatimespentNotNulls = [];
var listOfMarksDenominator = [];
var listofsectionmarks = [];
var listofsectionattempts = [];
var listofsectionaccuracy = [];
var listofsectiontimetaken = [];
var listofsectiontimetakenNotNull = [];
var overallmarks;
var overallattempts;
var overallaccuracy;
var overalltimetaken;
var overalltimetakenNotNull;
///***************** VARIABLES FOR TIME MANAGEMENT ********************///

/// ..........time taken for correct answer list ...........
var listoftimetakenforcorrectanswer = [];
var listoftotaltimetakenforcorrectanswer = [];
var listofareatimetakenforcorrectanswer=[];
var listoftopictimetakenforcorrectanswer=[];
/// overall value for total time taken for correct answer
var overalltimetakenforcorrectanswer;
/// section level list for total time taken for correct answer
var listofsectiontimetakenforcorrectanswer=[];
/// area level list for total time taken for correct answer
var listoftotalareatimetakenforcorrectanswer = [];
/// topic level list for total time taken for correct answer
var listoftotaltopictimetakenforcorrectanswer = [];


/// ............ time taken for incorrect answer list ..........

var listoftimetakenforincorrectanswer = [];
var listoftotaltimetakenforincorrectanswer = [];
var listofareatimetakenforincorrectanswer=[];
var listoftopictimetakenforincorrectanswer=[];
/// overall value for total time taken for in correct answer
var overalltimetakenforincorrectanswer;
/// section level list for total time taken for incorrect answer
var listofsectiontimetakenforincorrectanswer=[];
/// area level list for total time taken for incorrect answer
var listoftotalareatimetakenforincorrectanswer = [];
/// topic level list for total time taken for incorrect answer
var listoftotaltopictimetakenforincorrectanswer = [];

/// ............ time taken for incorrect skipped list ..........

var listoftimetakenforskippedanswer = [];
var listoftotaltimetakenforskippedanswer = [];
var listofareatimetakenforskippedanswer=[];
var listoftopictimetakenforskippedanswer=[];
/// overall value for total time taken for skipped answer
var overalltimetakenforskippedanswer;
/// section level list for total time taken for skipped answer
var listofsectiontimetakenforskippedanswer=[];
/// area level list for total time taken for skipped answer
var listoftotalareatimetakenforskippedanswer = [];
/// topic level list for total time taken for skipped answer
var listoftotaltopictimetakenforskippedanswer = [];


/// ..........total number of questions  for correct answer list ...........
var listoftotalnumberofquestionsforcorrectanswer = [];
var listoftotaltotalnumberofquestionsforcorrectanswer = [];
var listofareatotalnumberofquestionsforcorrectanswer=[];
var listoftopictotalnumberofquestionsforcorrectanswer=[];
/// overall value for total time taken for correct answer
var overalltotalnumberfquestionsforcorrectanswer;
/// section level list for total number of questions for correct answer
var listofsectiontotalnumberofquestionsforcorrectanswer=[];
/// area level list for total number of questions for correct answer
var listoftotalareatotalnumberofquestionsforcorrectanswer = [];
/// topic level list for total number of questions for correct answer
var listoftotaltopictotalnumberofquestionsforcorrectanswer = [];

/// ..........total number of questions  for incorrect answer list ...........
var listoftotalnumberofquestionsforincorrectanswer = [];
var listoftotaltotalnumberofquestionsforincorrectanswer = [];
var listofareatotalnumberofquestionsforincorrectanswer=[];
var listoftopictotalnumberofquestionsforincorrectanswer=[];
/// overall value for total time taken for incorrect answer
var overalltotalnumberofquestionsforincorrectanswer;
/// section level list for total number of questions for incorrect answer
var listofsectiontotalnumberofquestionsforincorrectanswer=[];
/// area level list for total number of questions for incorrect answer
var listoftotalareatotalnumberofquestionsforincorrectanswer = [];
/// topic level list for total number of questions for incorrect answer
var listoftotaltopictotalnumberofquestionsforincorrectanswer = [];

/// ..........total number of questions  for skipped list ...........
var listoftotalnumberofquestionsforskippedanswer = [];
var listoftotaltotalnumberofquestionsforskippedanswer = [];
var listofareatotalnumberofquestionsforskippedanswer=[];
var listoftopictotalnumberofquestionsforskippedanswer=[];
/// overall value for total time taken for skipped answer
var overalltotalnumberofquestionsforskippedanswer;
/// section level list for total number of questions for skipped answer
var listofsectiontotalnumberofquestionsforskippedanswer=[];
/// area level list for total number of questions for incorrect answer
var listoftotalareatotalnumberofquestionsforskippedanswer = [];
/// topic level list for total number of questions for incorrect answer
var listoftotaltopictotalnumberofquestionsforskippedanswer = [];


/// end variables for time management ...........///


List<int> _isExpanded;
void expandList(int length) {
  if (_isExpanded == null) {
    _isExpanded = new List(length);
    _isExpanded.fillRange(0, length, null);
  }
}


class AnalyticsPerformanceTracker extends StatefulWidget {
  var selectedLearningObject;
  var testTemplate;
  var testTemplateForXat;
  AnalyticsPerformanceTracker(this.selectedLearningObject, this.testTemplate, this.testTemplateForXat);
  @override
  AnalyticsPerformanceTrackerState createState() =>
      AnalyticsPerformanceTrackerState();
}

class AnalyticsPerformanceTrackerState
    extends State<AnalyticsPerformanceTracker> with TickerProviderStateMixin {
  final TextEditingController _stateOfVenues = new TextEditingController();
  int currentSelectedIndex;
  bool isLoading = true;
  double _animatedHeight;
  //SummaryTableState summaryObj=new SummaryTableState();
  String selectedAreaValue ;
  String selectedTopicValue ;
  String selectedItemTypeValue;
  String status;
  var testId;
  String _areaLabelValue = 'ALL AREAS';
  String _topicLabelValue = 'ALL TOPICS';
  String _itemTypeLabelValue = 'All Items';
  bool iconStatus = false;
  String savedVal;

  final TextEditingController _sample1 = new TextEditingController();
  final TextEditingController _sample2 = new TextEditingController();
  final TextEditingController _sample3 = new TextEditingController();
  //var controller = IndexedScrollController();
  var authorizationInvalidMsg;
  static const double _kPanelHeaderCollapsedHeight = kMinInteractiveDimension;
  static const double _kPanelHeaderExpandedHeight = 50.0;
  ScrollController _scrollController = new ScrollController();
  final dataKey = new GlobalKey();
  bool isExpanded = false;
  int i;
  String course;
  double _ITEM_HEIGHT;
  bool isFilterClicked = false;
  int indexValue = 0;

  int filterFlag;
  bool flagForItemType = false;
  @override
  void initState() {
    super.initState();
    _tabController = new TabController(vsync: this, length: tabList.length);
    setState(() {
      getCourseId();
    });
  }

  void getCourseId() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    course = prefs.getString("courseId");
    testId = prefs.getString("testId");
    if(widget.testTemplate != true){
      print("NonAdaptive");
      global.getToken.then((t) {
        getScoreCardData(t);
        getSummaryData(t);
      });
    }else{
      print("Adaptive");
      getScoreCardDataAdaptive();
      getSummaryDataForAdaptive();

    }
  }

  void _goToElement(int index) {
    _scrollController.animateTo(
        (_ITEM_HEIGHT *
            index), // 100 is the height of container and index of 6th element is 5
        duration: const Duration(milliseconds: 300),
        curve: Curves.easeOut);
  }

  var getAreaDropDownItems = ["All Areas"];
  var getTopicsDropDownItems = ["All Topics"];
  var getItemTypeDropdownItems = ["All Items"];
  var apiData;
  var jsonLinkResponse;
  var questionsList = [];
  TabController _tabController;

  List<Tab> tabList = [
    new Tab(
      child: Text(
        "Summary",
      ),
    ),
    new Tab(
      child: Text(
        "Detailed",
      ),
    ),
    new Tab(
      child: Text(
        "Time Management",
      ),
    ),
  ];

  var getSectionDropDownItems = ["All Sections"];
  static var getQuestions = [];
  static List<bool> flagValue = List.filled(getQuestions.length, false);

  var getSectionTitles = [];
  final GlobalKey<ScaffoldState> _scaffoldstate =
      new GlobalKey<ScaffoldState>();
  String selectedSection = "";
  var getItemId = [];
  String selectedlabel = "All Sections";
  var section = new Map();
  var area = new Map();
  var areaName = new Map();
  var topicNames = new Map();
  var topic = new Map();
  var sectionName = ["All section"];
  int activeMeterIndex;
  var value;
  @override
  String _printDuration(Duration duration) {
    if (duration != null) {
      String twoDigits(int n) {
        if (n >= 10) return "$n";
        return "0$n";
      }

      String twoDigitMinutes = twoDigits(duration.inMinutes.remainder(60));
      String twoDigitSeconds = twoDigits(duration.inSeconds.remainder(60));
      return "${twoDigits(duration.inHours)}:$twoDigitMinutes:$twoDigitSeconds";
    } else {
      return "";
    }
  }

  String _parseHtmlString(String htmlString) {
    var document = parse(htmlString);

    String parsedString = parse(document.body.text).documentElement.text;

    return parsedString;
  }

  void getScoreCardData(Map t) async {
    if (global.headersWithAuthorizationKey['Authorization'] != '') {
      ApiService()
          .getAPI(URL.ANALYTICS_API_PERFORMANCE_ANALYTICS + testId, t)
          .then((returnValue) {
        print("**********Detaieled " + testId + t.toString());
        setState(() {
          if (returnValue[0] == 'Success') {
            apiData = returnValue[1]['data'];
            for (var index = 0;
                index < apiData['studentTestItems'].length;
                index++) {
              var itemName;
              var count = 0;
              if(apiData['studentTestItems'][index]['sectionName'] == "Pyschometric" || apiData['studentTestItems'][index]['sectionName'] == "Descriptive" || apiData['studentTestItems'][index]['sectionName'] == "Descriptive Writing") {
                count = count + 1;
              }
              else{
                itemName = apiData['studentTestItems'][index]['sectionName'];
                if (!getSectionDropDownItems.contains(itemName)) {
                  getSectionDropDownItems.add(itemName);
                }
              }

            }
            jsonLinkResponse = apiData['testJson'];
            loadAllQuestions();
            loadAllDropdownItemsForAreas();
            // if(selectedSection!=null ||selectedSection!=""){
            //   getSectionWiseQuestions("All Sections",false);
            // }
            setState(() {
              isLoading = false;
            });
            loadDropDownItemsForItemType();
          } else {
            authorizationInvalidMsg = returnValue[0];
          }
        });
      });
    }
  }
  ///Function to call api for Detailed tab when selected test is adaptive type
  void getScoreCardDataAdaptive() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    authorization = prefs.getString("loginAccessToken");
    courseID = prefs.getString("courseId");
    Map postdata = {
      "testId": testId,
      "Authorization": authorization,
      "courseId":courseID
    };
    print("Post Data"+postdata.toString());
    ApiService().postAPITerr(URL.ANALYTICS_API_PERFORMANCE_ANALYTICS_WRAPPER, postdata, global.headers).then((returnValue){
        setState(() {
          if (returnValue[0]['success']==true) {
            apiData = returnValue[0]['data'];
            for (var index = 0;
            index < apiData['studentTestItems'].length;
            index++) {
              var itemName = apiData['studentTestItems'][index]['sectionName'];
              if (!getSectionDropDownItems.contains(itemName)) {
                getSectionDropDownItems.add(itemName);
              }
            }
            jsonLinkResponse = apiData['testJson'];
            loadAllQuestions();
            loadAllDropdownItemsForAreas();
            // if(selectedSection!=null ||selectedSection!=""){
            //   getSectionWiseQuestions("All Sections",false);
            // }
            setState(() {
              isLoading = false;
            });
            loadDropDownItemsForItemType();
          } else {

          }
        });
      });

  }
///Adding drop down values for areas
  void loadAllDropdownItemsForAreas() {
    for (var i = 0; i < jsonLinkResponse['SectionResponse'].length; i++) {
      if (jsonLinkResponse['SectionResponse'][i]['SectionTitle'].contains("Pyschometric")
          ||
          jsonLinkResponse['SectionResponse'][i]['SectionTitle'].contains("Descriptive")
          ||
          jsonLinkResponse['SectionResponse'][i]['SectionTitle'].contains("Descriptive Writing")
      ) {
      }
      else{
        for (var j = 0;
        j < jsonLinkResponse['SectionResponse'][i]['ItemResponse'].length;
        j++) {
          setState(() {
            if (!getAreaDropDownItems.contains(
                jsonLinkResponse['SectionResponse']
                [i]['ItemResponse'][j]['AreaName'])) {
              if (jsonLinkResponse['SectionResponse'][i]['ItemResponse'][j]['AreaName'] !=
                  null) {
                getAreaDropDownItems.add(
                    jsonLinkResponse['SectionResponse'][i]
                    ['ItemResponse'][j]['AreaName']);
              }
            }
          });
        }
    }
  }
    print("Area Name"+getAreaDropDownItems.toString());
  }
/// It will Load the Dropdown Items for Area Dropdown
  void loadDropdownItemsForAreas(String selectedSection) {
    setState(() {
      // we need the first value as All Areas so we are adding manually
      getAreaDropDownItems = ["All Areas"];
      // we will empty the list
      getQuestions = [];
      getItemId = [];
    });
    // checking whether the Section Drop down is selected
    if (selectedSection == "All Sections" || selectedSection == "") {
      loadAllQuestions();
      loadAllDropdownItemsForAreas();
    }
    else {
      for (var i = 0; i < jsonLinkResponse['SectionResponse'].length; i++) {
        if (selectedSection ==
            jsonLinkResponse['SectionResponse'][i]['SectionTitle']) {
          for (var j = 0;
              j < jsonLinkResponse['SectionResponse'][i]['ItemResponse'].length;
              j++) {
            setState(() {
              if (!getAreaDropDownItems.contains(
                  jsonLinkResponse['SectionResponse'][i]['ItemResponse'][j]
                      ['AreaName'])) {
                getAreaDropDownItems.add(jsonLinkResponse['SectionResponse'][i]
                    ['ItemResponse'][j]['AreaName']);
              }
              // getQuestions.add(jsonLinkResponse['SectionResponse'][i]
              //     ['ItemResponse'][j]['Items']);
            });
          }
        }
      }
    }
  }

///Loading Dropdown Items for ItemType
  loadDropDownItemsForItemType() {
    for (var i = 0; i < jsonLinkResponse['SectionResponse'].length; i++) {
      for (var j = 0; j < jsonLinkResponse['SectionResponse'][i]['ItemResponse'].length; j++) {
        setState(() {
          //As getting the values sometimes Upper and Lower
          var itemType = jsonLinkResponse['SectionResponse'][i]['ItemResponse'][j]['ItemType'].toString().toUpperCase();
          //checking whether the value is in the drop down list or not
          if (!getItemTypeDropdownItems.contains(itemType)) {
            if (itemType == "MCA") {
                getItemTypeDropdownItems.add("MCA");
            } else if (itemType == "MCQ") {
                getItemTypeDropdownItems.add("MCQ");
            } else if (itemType == "AN" || itemType == "BN" || itemType == "NE") {
              if (!getItemTypeDropdownItems.contains("TITA")) {
                getItemTypeDropdownItems.add("TITA");
              }
              //adding values other than these item types
            }else if(itemType != "AN" && itemType != "BN" && itemType != "NE" && itemType != "MCA" && itemType != "MCQ"){
              if (!getItemTypeDropdownItems.contains("OTHERS")) {
                getItemTypeDropdownItems.add("OTHERS");
              }
            }else{

            }
          }
        });
      }
    }
  }

  /// Load Dropdown Items For Topics
  void loadDropdownItemsForTopics(String selectedLabel1) {
    // We have two possibilities, the user may check ALL AREAS or ANY VALUE
    //If Selects ALL AREAS
    if (selectedLabel1 == "All Areas") {
      //if user selected All Sections
      if (selectedSection != "" || selectedSection != null) {
        getSectionWiseItemQuestions(selectedSection);
      } else {
        //for the first time it will be All Sections(without the user selection)
        getSectionWiseItemQuestions(selectedSection);
      }
    } else {
      setState(() {
        getTopicsDropDownItems = ["All Topics"];
      });
      for (var index = 0; index < apiData['studentTestItems'].length; index++) {
        var itemName = apiData['studentTestItems'][index]['topicName'];
        if (apiData['studentTestItems'][index]['areaName'] == selectedLabel1) {
          if (!getTopicsDropDownItems.contains(itemName)) {
            getTopicsDropDownItems.add(itemName);
          }
        }
      }
    //  loadAreaWiseQuestions(selectedLabel1);
    }
  }

  List loadAreaWiseQuestions(String selectedArea) {
    print("***************************loadAreaWiseQuestions"+selectedArea);
    if (selectedArea == "All Areas") {
      if (selectedSection != "") {
        print("***************************loadAreaWiseQuestions"+selectedSection);

        getSectionWiseItemQuestions(selectedSection);
      } else {
        //getSectionWiseItemQuestions("All Sections");
        loadAllQuestions();
      }
    } else {
      print("**********ELSE*****************loadAreaWiseQuestions"+selectedArea);

      setState(() {
        getQuestions = [];
        getItemId = [];
      });
        for (var i = 0; i < jsonLinkResponse['SectionResponse'].length; i++) {
          for (var j = 0; j < jsonLinkResponse['SectionResponse'][i]['ItemResponse'].length; j++) {
            for (var index = 0; index < apiData['studentTestItems'].length; index++) {
            if (jsonLinkResponse['SectionResponse'][i]['ItemResponse'][j]
                    ['ItemID'] ==
                int.parse(apiData['studentTestItems'][index]['itemId'])) {
              //Checking whether the ITEM TYPE is Selected
              if (selectedItemTypeValue != null) {
                //Whether the SELECTED ITEM TYPE is other than ALL ITEMS
                if (selectedItemTypeValue != "All Items") {
                  if (selectedItemTypeValue == "MCQ" ||
                      selectedItemTypeValue == "MCA") {
                    if (selectedItemTypeValue ==
                        (jsonLinkResponse['SectionResponse'][i]['ItemResponse']
                            [j]['ItemType'].toString().toUpperCase())) {
                      if (jsonLinkResponse['SectionResponse'][i]['ItemResponse'][j]
                      ['AreaName'] != null){
                      if (jsonLinkResponse['SectionResponse'][i]['ItemResponse']
                      [j]['AreaName']
                          .contains(selectedArea)) {
                        setState(() {
                          getQuestions.add(jsonLinkResponse['SectionResponse']
                          [i]['ItemResponse'][j]['Items']);
                          getItemId.add(apiData['studentTestItems'][index]);
                        });
                      }
                    }
                    }
                  } else if (selectedItemTypeValue == "TITA") {
                    if (jsonLinkResponse['SectionResponse'][i]['ItemResponse'][j]
                    ['AreaName'] != null){
                    if (jsonLinkResponse['SectionResponse'][i]['ItemResponse'][j]['AreaName']
                        .contains(selectedArea)) {
                      if ((jsonLinkResponse['SectionResponse'][i]
                      ['ItemResponse'][j]['ItemType']
                          .toString()
                          .toUpperCase()) ==
                          "AN" ||
                          (jsonLinkResponse['SectionResponse'][i]
                          ['ItemResponse'][j]['ItemType']
                              .toString()
                              .toUpperCase()) ==
                              "BN" ||
                          (jsonLinkResponse['SectionResponse'][i]
                          ['ItemResponse'][j]['ItemType']
                              .toString()
                              .toUpperCase()) ==
                              "NE") {
                        setState(() {
                          getQuestions.add(jsonLinkResponse['SectionResponse']
                          [i]['ItemResponse'][j]['Items']);
                          getItemId.add(apiData['studentTestItems'][index]);
                        });
                      }
                    }
                  }
                  } else if (selectedItemTypeValue == "OTHERS") {
                    if ((jsonLinkResponse['SectionResponse'][i]['ItemResponse']
                                [j]['ItemType'].toString().toUpperCase()) !=
                            "AN" &&
                        (jsonLinkResponse['SectionResponse'][i]['ItemResponse']
                                [j]['ItemType'].toString().toUpperCase()) !=
                            "BN" &&
                        (jsonLinkResponse['SectionResponse'][i]['ItemResponse']
                                [j]['ItemType'].toString().toUpperCase()) !=
                            "NE" &&
                        (jsonLinkResponse['SectionResponse'][i]['ItemResponse']
                                [j]['ItemType'].toString().toUpperCase()) !=
                            "MCA" &&
                        (jsonLinkResponse['SectionResponse'][i]['ItemResponse']
                                [j]['ItemType'].toString().toUpperCase()) !=
                            "MCQ") {
                      setState(() {
                        getQuestions.add(jsonLinkResponse['SectionResponse'][i]
                            ['ItemResponse'][j]['Items']);
                        getItemId.add(apiData['studentTestItems'][index]);
                      });
                    }
                  }
                }
                //Whether the SELECTED ITEM TYPE is ALL ITEMS
                else {
                  if (jsonLinkResponse['SectionResponse'][i]['ItemResponse'][j]
                  ['AreaName'] != null){
                  if (jsonLinkResponse['SectionResponse'][i]['ItemResponse'][j]
                  ['AreaName']
                      .contains(selectedArea)) {
                    setState(() {
                      getQuestions.add(jsonLinkResponse['SectionResponse'][i]
                      ['ItemResponse'][j]['Items']);
                      getItemId.add(apiData['studentTestItems'][index]);
                    });
                  }
                }
                }
              }
             // Checking whether the ITEM TYPE is not Selected
              else {
                if (jsonLinkResponse['SectionResponse'][i]['ItemResponse'][j]
                ['AreaName'] != null){
                  if (jsonLinkResponse['SectionResponse'][i]['ItemResponse'][j]
                  ['AreaName']
                      .contains(selectedArea)) {
                    setState(() {
                      getQuestions.add(jsonLinkResponse['SectionResponse'][i]
                      ['ItemResponse'][j]['Items']);
                      getItemId.add(apiData['studentTestItems'][index]);
                    });
                  }
              }
              }
            }
          }
        }
      }
    }

    return getQuestions;
  }

  void getSectionWiseItemQuestions(String selectedSection) {
    print("************************getSectionWiseItemQuestions" +selectedSection +selectedItemTypeValue);
    //if Selected section is All Sections
    if(selectedSection=="All Sections"){
      print("************IF************getSectionWiseItemQuestions" +selectedItemTypeValue);

      setState(() {
        getQuestions =[];
        getItemId = [];
        getTopicsDropDownItems = ["All Topics"];
        selectedAreaValue = "All Areas";
        selectedTopicValue = "All Topics";
      });
      if(selectedItemTypeValue!=null){
      //  print("*******IF*****IF************getSectionWiseItemQuestions" +selectedItemTypeValue);

        if(selectedItemTypeValue!="All Items") {
          print("************IF************selectedItemTypeValue" +
              selectedItemTypeValue);

          for (var i = 0; i < jsonLinkResponse['SectionResponse'].length; i++) {
            if (jsonLinkResponse['SectionResponse'][i]['SectionTitle'].contains("Pyschometric")
                ||
                jsonLinkResponse['SectionResponse'][i]['SectionTitle'].contains("Descriptive")
                ||
                jsonLinkResponse['SectionResponse'][i]['SectionTitle'].contains("Descriptive Writing")
            ) {
            }
            else{
            for (var j = 0; j <
                jsonLinkResponse['SectionResponse'][i]['ItemResponse']
                    .length; j++) {
              for (var index = 0; index <
                  apiData['studentTestItems'].length; index++) {
                if (jsonLinkResponse['SectionResponse'][i]['ItemResponse'][j]['ItemID'] ==
                    int.parse(apiData['studentTestItems'][index]['itemId'])) {
                  if (selectedItemTypeValue == "MCQ" ||
                      selectedItemTypeValue == "MCA") {
                    if (selectedItemTypeValue ==
                        (jsonLinkResponse['SectionResponse'][i]['ItemResponse'][j]['ItemType']
                            .toString()
                            .toUpperCase())) {
                      print(
                          "************Entered************selectedItemTypeValue" +
                              selectedItemTypeValue);

                      setState(() {
                        getQuestions.add(
                            jsonLinkResponse['SectionResponse'][i]['ItemResponse'][j]['Items']);
                        getItemId.add(apiData['studentTestItems'][index]);
                      });
                    }
                  }
                  else if (selectedItemTypeValue == "TITA") {
                    if ((jsonLinkResponse['SectionResponse'][i]['ItemResponse'][j]['ItemType']
                        .toString()
                        .toUpperCase()) ==
                        "AN" ||
                        (jsonLinkResponse['SectionResponse'][i]['ItemResponse'][j]['ItemType']
                            .toString()
                            .toUpperCase()) ==
                            "BN"
                        ||
                        (jsonLinkResponse['SectionResponse'][i]['ItemResponse'][j]['ItemType']
                            .toString()
                            .toUpperCase()) ==
                            "NE") {
                      setState(() {
                        getQuestions.add(
                            jsonLinkResponse['SectionResponse'][i]['ItemResponse'][j]['Items']);
                        getItemId.add(apiData['studentTestItems'][index]);
                      });
                    }
                  } else if (selectedItemTypeValue == "OTHERS") {
                    if ((jsonLinkResponse['SectionResponse'][i]['ItemResponse'][j]['ItemType']
                        .toString()
                        .toUpperCase()) !=
                        "AN" &&
                        (jsonLinkResponse['SectionResponse'][i]['ItemResponse'][j]['ItemType']
                            .toString()
                            .toUpperCase()) !=
                            "BN"
                        &&
                        (jsonLinkResponse['SectionResponse'][i]['ItemResponse'][j]['ItemType']
                            .toString()
                            .toUpperCase()) !=
                            "NE" &&
                        (jsonLinkResponse['SectionResponse'][i]['ItemResponse'][j]['ItemType']
                            .toString()
                            .toUpperCase()) !=
                            "MCA"
                        &&
                        (jsonLinkResponse['SectionResponse'][i]['ItemResponse'][j]['ItemType']
                            .toString()
                            .toUpperCase()) !=
                            "MCQ") {
                      setState(() {
                        getQuestions.add(
                            jsonLinkResponse['SectionResponse'][i]['ItemResponse'][j]['Items']);
                        getItemId.add(apiData['studentTestItems'][index]);
                      });
                    }
                  }
                }
              }
            }
          }
        }
        }else {
          for (var i = 0; i < jsonLinkResponse['SectionResponse'].length; i++) {
            if (jsonLinkResponse['SectionResponse'][i]['SectionTitle'].contains("Pyschometric")
                ||
                jsonLinkResponse['SectionResponse'][i]['SectionTitle'].contains("Descriptive")
                ||
                jsonLinkResponse['SectionResponse'][i]['SectionTitle'].contains("Descriptive Writing")
            ) {
            }
            else {
              for (var j = 0; j <
                  jsonLinkResponse['SectionResponse'][i]['ItemResponse']
                      .length; j++) {
                if (jsonLinkResponse['SectionResponse'][i]['SectionTitle'].contains("Pyschometric")
                    ||
                    jsonLinkResponse['SectionResponse'][i]['SectionTitle'].contains("Descriptive")
                    ||
                    jsonLinkResponse['SectionResponse'][i]['SectionTitle'].contains("Descriptive Writing")
                ) {
                }
                else{
                for (var index = 0; index <
                    apiData['studentTestItems'].length; index++) {
                  if (jsonLinkResponse['SectionResponse'][i]['ItemResponse'][j]['ItemID'] ==
                      int.parse(apiData['studentTestItems'][index]['itemId'])) {
                    setState(() {
                      getQuestions.add(
                          jsonLinkResponse['SectionResponse'][i]['ItemResponse'][j]['Items']);
                      getItemId.add(apiData['studentTestItems'][index]);
                    });
                  }
                }
              }
            }
          }
        }
        }
      }else {
        for (var i = 0; i < jsonLinkResponse['SectionResponse'].length; i++) {
          if (jsonLinkResponse['SectionResponse'][i]['SectionTitle'].contains("Pyschometric")
              ||
              jsonLinkResponse['SectionResponse'][i]['SectionTitle'].contains("Descriptive")
              ||
              jsonLinkResponse['SectionResponse'][i]['SectionTitle'].contains("Descriptive Writing")
          ) {
          }
          else{
          for (var j = 0; j <
              jsonLinkResponse['SectionResponse'][i]['ItemResponse']
                  .length; j++) {
            for (var index = 0; index <
                apiData['studentTestItems'].length; index++) {
              if (jsonLinkResponse['SectionResponse'][i]['ItemResponse'][j]['ItemID'] ==
                  int.parse(apiData['studentTestItems'][index]['itemId'])) {
                setState(() {
                  getQuestions.add(
                      jsonLinkResponse['SectionResponse'][i]['ItemResponse'][j]['Items']);
                  getItemId.add(apiData['studentTestItems'][index]);
                });
              }
            }
          }
        }
      }
      }
      loadDropdownItemsForAreas("All Sections");
    }
    else{
      loadDropdownItemsForAreas(selectedSection);
     // print("************ELSE************getSectionWiseItemQuestions" +selectedSection);

      setState(() {
        getQuestions =[];
        getItemId = [];
        getTopicsDropDownItems = ["All Topics"];
      });
        for(var i=0 ; i<jsonLinkResponse['SectionResponse'].length;i++) {
          for (var j = 0; j <
              jsonLinkResponse['SectionResponse'][i]['ItemResponse']
                  .length; j++) {
            for (var index = 0; index <apiData['studentTestItems'].length; index++) {
              if (jsonLinkResponse['SectionResponse'][i]['ItemResponse'][j]['ItemID'] ==
                int.parse(apiData['studentTestItems'][index]['itemId'])) {
              if (selectedSection ==
                  jsonLinkResponse['SectionResponse'][i]['SectionTitle']) {
                if (selectedItemTypeValue != null) {
                  if (selectedItemTypeValue != "All Items") {
                    if (selectedItemTypeValue == "MCQ" ||
                        selectedItemTypeValue == "MCA") {
                      if (selectedItemTypeValue ==
                          (jsonLinkResponse['SectionResponse'][i]['ItemResponse'][j]['ItemType'].toString().toUpperCase())) {
                        setState(() {
                          getQuestions.add(
                              jsonLinkResponse['SectionResponse'][i]['ItemResponse'][j]['Items']);
                          getItemId.add(apiData['studentTestItems'][index]);
                        });
                      }
                    } else if (selectedItemTypeValue == "TITA") {
                      if ((jsonLinkResponse['SectionResponse'][i]['ItemResponse'][j]['ItemType'].toString().toUpperCase()) ==
                          "AN" ||
                          (jsonLinkResponse['SectionResponse'][i]['ItemResponse'][j]['ItemType'].toString().toUpperCase()) ==
                              "BN"
                          ||
                          (jsonLinkResponse['SectionResponse'][i]['ItemResponse'][j]['ItemType'].toString().toUpperCase()) ==
                              "NE") {
                        setState(() {
                          getQuestions.add(
                              jsonLinkResponse['SectionResponse'][i]['ItemResponse'][j]['Items']);
                          getItemId.add(apiData['studentTestItems'][index]);
                        });
                      }
                    } else if (selectedItemTypeValue == "OTHERS") {
                      if ((jsonLinkResponse['SectionResponse'][i]['ItemResponse'][j]['ItemType'].toString().toUpperCase()) !=
                          "AN" &&
                          (jsonLinkResponse['SectionResponse'][i]['ItemResponse'][j]['ItemType'].toString().toUpperCase()) !=
                              "BN"
                          &&
                          (jsonLinkResponse['SectionResponse'][i]['ItemResponse'][j]['ItemType'].toString().toUpperCase()) !=
                              "NE" &&
                          (jsonLinkResponse['SectionResponse'][i]['ItemResponse'][j]['ItemType'].toString().toUpperCase()) !=
                              "MCA"
                          &&
                          (jsonLinkResponse['SectionResponse'][i]['ItemResponse'][j]['ItemType'].toString().toUpperCase()) !=
                              "MCQ") {
                        setState(() {
                          getQuestions.add(
                              jsonLinkResponse['SectionResponse'][i]['ItemResponse'][j]['Items']);
                          getItemId.add(apiData['studentTestItems'][index]);
                        });
                      }
                    }
                  } else {
                    setState(() {
                      getQuestions.add(
                          jsonLinkResponse['SectionResponse'][i]['ItemResponse'][j]['Items']);
                      getItemId.add(apiData['studentTestItems'][index]);
                    });
                  }
                } else {
                  setState(() {
                    getQuestions.add(
                        jsonLinkResponse['SectionResponse'][i]['ItemResponse'][j]['Items']);
                    getItemId.add(apiData['studentTestItems'][index]);
                  });
                }
              }
            }
          }
        }
      }
    }
  }

  void getSectionWiseQuestions(String selectedSection) {

    if(selectedSection=="All Sections"){
      setState(() {
        getQuestions =[];
        getItemId = [];
        getTopicsDropDownItems = ["All Topics"];
        selectedAreaValue = "All Areas";
        selectedTopicValue = "All Topics";
        selectedItemTypeValue = "All Items";
      });
      if(selectedItemTypeValue!=null){
        if(selectedItemTypeValue!="All Items"){
            for(var i=0 ; i<jsonLinkResponse['SectionResponse'].length;i++) {
              for (var j = 0; j <
                  jsonLinkResponse['SectionResponse'][i]['ItemResponse']
                      .length; j++) {
                for (var index = 0; index <apiData['studentTestItems'].length; index++) {
                  if (jsonLinkResponse['SectionResponse'][i]['ItemResponse'][j]['ItemID'] ==
                    int.parse(apiData['studentTestItems'][index]['itemId'])) {
                  if (selectedItemTypeValue == "MCQ" ||
                      selectedItemTypeValue == "MCA") {
                    if (selectedItemTypeValue ==
                        (jsonLinkResponse['SectionResponse'][i]['ItemResponse'][j]['ItemType'].toString().toUpperCase())) {
                      setState(() {
                        getQuestions.add(
                            jsonLinkResponse['SectionResponse'][i]['ItemResponse'][j]['Items'].toString().toUpperCase());
                        getItemId.add(apiData['studentTestItems'][index]);
                      });
                    }
                  }
                  else if (selectedItemTypeValue == "TITA") {
                    if ((jsonLinkResponse['SectionResponse'][i]['ItemResponse'][j]['ItemType'].toString().toUpperCase()) ==
                        "AN" ||
                        (jsonLinkResponse['SectionResponse'][i]['ItemResponse'][j]['ItemType'].toString().toUpperCase()) ==
                            "BN"
                        ||
                        (jsonLinkResponse['SectionResponse'][i]['ItemResponse'][j]['ItemType'].toString().toUpperCase()) ==
                            "NE") {
                      setState(() {
                        getQuestions.add(
                            jsonLinkResponse['SectionResponse'][i]['ItemResponse'][j]['Items']);
                        getItemId.add(apiData['studentTestItems'][index]);

                      });
                    }
                  } else if (selectedItemTypeValue == "OTHERS") {
                    if ((jsonLinkResponse['SectionResponse'][i]['ItemResponse'][j]['ItemType'].toString().toUpperCase()) !=
                        "AN" &&
                        (jsonLinkResponse['SectionResponse'][i]['ItemResponse'][j]['ItemType'].toString().toUpperCase()) !=
                            "BN"
                        &&
                        (jsonLinkResponse['SectionResponse'][i]['ItemResponse'][j]['ItemType'].toString().toUpperCase()) !=
                            "NE" &&
                        (jsonLinkResponse['SectionResponse'][i]['ItemResponse'][j]['ItemType'].toString().toUpperCase()) !=
                            "MCA"
                        &&
                        (jsonLinkResponse['SectionResponse'][i]['ItemResponse'][j]['ItemType'].toString().toUpperCase()) !=
                            "MCQ") {

                      setState(() {
                        getQuestions.add(
                            jsonLinkResponse['SectionResponse'][i]['ItemResponse'][j]['Items']);
                        getItemId.add(apiData['studentTestItems'][index]);

                      });
                    }
                  }
                }
              }
            }
          }
        }else {
          for (var index = 0; index <apiData['studentTestItems'].length; index++) {

            for (var i = 0; i < jsonLinkResponse['SectionResponse'].length; i++) {
              for (var j = 0; j <
                  jsonLinkResponse['SectionResponse'][i]['ItemResponse']
                      .length; j++) {
                if (jsonLinkResponse['SectionResponse'][i]['ItemResponse'][j]['ItemID'] ==
                    int.parse(apiData['studentTestItems'][index]['itemId'])) {
                  setState(() {
                    getQuestions.add(
                        jsonLinkResponse['SectionResponse'][i]['ItemResponse'][j]['Items']);
                    getItemId.add(apiData['studentTestItems'][index]);

                  });
                }
              }
            }
          }
        }
      }else{
        for (var index = 0; index <apiData['studentTestItems'].length; index++) {
          for(var i=0 ; i<jsonLinkResponse['SectionResponse'].length;i++) {
            for (var j = 0; j <
                jsonLinkResponse['SectionResponse'][i]['ItemResponse']
                    .length; j++) {
              if (jsonLinkResponse['SectionResponse'][i]['ItemResponse'][j]['ItemID'] ==
                  int.parse(apiData['studentTestItems'][index]['itemId'])) {
                setState(() {
                  getQuestions.add(
                      jsonLinkResponse['SectionResponse'][i]['ItemResponse'][j]['Items']);
                  getItemId.add(apiData['studentTestItems'][index]);

                });
              }
            }
          }
        }
      }


      loadDropdownItemsForAreas("All Sections");

    }
    else{
      loadDropdownItemsForAreas(selectedSection);
      setState(() {
        getQuestions =[];
        getItemId = [];
        getTopicsDropDownItems = ["All Topics"];
        selectedAreaValue = "All Areas";
        selectedTopicValue = "All Topics";
        selectedItemTypeValue = "All Items";
      });
        for(var i=0 ; i<jsonLinkResponse['SectionResponse'].length;i++) {
          for (var j = 0; j <
              jsonLinkResponse['SectionResponse'][i]['ItemResponse']
                  .length; j++) {
            for (var index = 0; index <apiData['studentTestItems'].length; index++) {
              if (jsonLinkResponse['SectionResponse'][i]['ItemResponse'][j]['ItemID'] ==
                int.parse(apiData['studentTestItems'][index]['itemId'])) {
              if (selectedSection ==
                  jsonLinkResponse['SectionResponse'][i]['SectionTitle']) {
                if (selectedItemTypeValue != null) {
                  if (selectedItemTypeValue != "All Items") {
                    if (selectedItemTypeValue == "MCQ" ||
                        selectedItemTypeValue == "MCA") {
                      if (selectedItemTypeValue ==
                          (jsonLinkResponse['SectionResponse'][i]['ItemResponse'][j]['ItemType'].toString().toUpperCase())) {
                        setState(() {
                          getQuestions.add(
                              jsonLinkResponse['SectionResponse'][i]['ItemResponse'][j]['Items']);
                          getItemId.add(apiData['studentTestItems'][index]);
                        });
                      }
                    } else if (selectedItemTypeValue == "TITA") {
                      if ((jsonLinkResponse['SectionResponse'][i]['ItemResponse'][j]['ItemType'].toString().toUpperCase()) ==
                          "AN" ||
                          (jsonLinkResponse['SectionResponse'][i]['ItemResponse'][j]['ItemType'].toString().toUpperCase()) ==
                              "BN"
                          ||
                          (jsonLinkResponse['SectionResponse'][i]['ItemResponse'][j]['ItemType'].toString().toUpperCase()) ==
                              "NE") {
                        setState(() {
                          getQuestions.add(
                              jsonLinkResponse['SectionResponse'][i]['ItemResponse'][j]['Items']);
                          getItemId.add(apiData['studentTestItems'][index]);
                        });
                      }
                    } else if (selectedItemTypeValue == "OTHERS") {
                      if ((jsonLinkResponse['SectionResponse'][i]['ItemResponse'][j]['ItemType'].toString().toUpperCase()) !=
                          "AN" &&
                          (jsonLinkResponse['SectionResponse'][i]['ItemResponse'][j]['ItemType'].toString().toUpperCase()) !=
                              "BN"
                          &&
                          (jsonLinkResponse['SectionResponse'][i]['ItemResponse'][j]['ItemType'].toString().toUpperCase()) !=
                              "NE" &&
                          (jsonLinkResponse['SectionResponse'][i]['ItemResponse'][j]['ItemType'].toString().toUpperCase()) !=
                              "MCA"
                          &&
                          (jsonLinkResponse['SectionResponse'][i]['ItemResponse'][j]['ItemType'].toString().toUpperCase()) !=
                              "MCQ") {
                        setState(() {
                          getQuestions.add(
                              jsonLinkResponse['SectionResponse'][i]['ItemResponse'][j]['Items']);
                          getItemId.add(apiData['studentTestItems'][index]);
                        });
                      }
                    }
                  } else {
                    setState(() {
                      getQuestions.add(
                          jsonLinkResponse['SectionResponse'][i]['ItemResponse'][j]['Items']);
                      getItemId.add(apiData['studentTestItems'][index]);
                    });
                  }
                } else {
                  setState(() {
                    getQuestions.add(
                        jsonLinkResponse['SectionResponse'][i]['ItemResponse'][j]['Items']);
                    getItemId.add(apiData['studentTestItems'][index]);
                  });
                }
              }
            }
          }
        }
      }
    }
  }

  void loadAllQuestions(){
    setState(() {
      getQuestions = [];
      getItemId = [];
      });
    if(selectedItemTypeValue!=null){
      if(selectedItemTypeValue!="All Items") {
        for (var i = 0; i < jsonLinkResponse['SectionResponse'].length; i++) {
          if (jsonLinkResponse['SectionResponse'][i]['SectionTitle'].contains("Pyschometric")
              ||
              jsonLinkResponse['SectionResponse'][i]['SectionTitle'].contains("Descriptive")
              ||
              jsonLinkResponse['SectionResponse'][i]['SectionTitle'].contains("Descriptive Writing")
          ) {
          }
          else{
          for (var j = 0; j <
              jsonLinkResponse['SectionResponse'][i]['ItemResponse']
                  .length; j++) {
            for (var index = 0; index <
                apiData['studentTestItems'].length; index++) {
              if (jsonLinkResponse['SectionResponse'][i]['ItemResponse'][j]['ItemID'] ==
                  int.parse(apiData['studentTestItems'][index]['itemId'])) {
                if (selectedItemTypeValue == "MCQ") {
                  if (selectedItemTypeValue ==
                      (jsonLinkResponse['SectionResponse'][i]['ItemResponse'][j]['ItemType']
                          .toString()
                          .toUpperCase())) {
                    setState(() {
                      getQuestions.add(
                          jsonLinkResponse['SectionResponse'][i]['ItemResponse'][j]['Items']);
                      getItemId.add(apiData['studentTestItems'][index]);
                    });
                  }
                } else if (selectedItemTypeValue == "TITA") {
                  if ((jsonLinkResponse['SectionResponse'][i]['ItemResponse'][j]['ItemType']
                      .toString()
                      .toUpperCase()) ==
                      "AN" ||
                      (jsonLinkResponse['SectionResponse'][i]['ItemResponse'][j]['ItemType']
                          .toString()
                          .toUpperCase()) ==
                          "BN"
                      ||
                      (jsonLinkResponse['SectionResponse'][i]['ItemResponse'][j]['ItemType']
                          .toString()
                          .toUpperCase()) ==
                          "NE") {
                    setState(() {
                      getQuestions.add(
                          jsonLinkResponse['SectionResponse'][i]['ItemResponse'][j]['Items']);
                      getItemId.add(apiData['studentTestItems'][index]);
                    });
                  }
                } else {
                  if ((jsonLinkResponse['SectionResponse'][i]['ItemResponse'][j]['ItemType']
                      .toString()
                      .toUpperCase()) !=
                      "AN" &&
                      (jsonLinkResponse['SectionResponse'][i]['ItemResponse'][j]['ItemType']
                          .toString()
                          .toUpperCase()) !=
                          "BN"
                      &&
                      (jsonLinkResponse['SectionResponse'][i]['ItemResponse'][j]['ItemType']
                          .toString()
                          .toUpperCase()) !=
                          "NE" &&
                      (jsonLinkResponse['SectionResponse'][i]['ItemResponse'][j]['ItemType']
                          .toString()
                          .toUpperCase()) !=
                          "MCA"
                      &&
                      (jsonLinkResponse['SectionResponse'][i]['ItemResponse'][j]['ItemType']
                          .toString()
                          .toUpperCase()) !=
                          "MCQ") {
                    setState(() {
                      getQuestions.add(
                          jsonLinkResponse['SectionResponse'][i]['ItemResponse'][j]['Items']);
                      getItemId.add(apiData['studentTestItems'][index]);
                    });
                  }
                }
              }
            }
          }
        }
      }

      }else {
        for (var i = 0; i < jsonLinkResponse['SectionResponse'].length; i++) {
          if (jsonLinkResponse['SectionResponse'][i]['SectionTitle'].contains("Pyschometric")
              ||
              jsonLinkResponse['SectionResponse'][i]['SectionTitle'].contains("Descriptive")
              ||
              jsonLinkResponse['SectionResponse'][i]['SectionTitle'].contains("Descriptive Writing")
          ) {
          }
          else{
          for (var j = 0; j <
              jsonLinkResponse['SectionResponse'][i]['ItemResponse']
                  .length; j++) {
            for (var index = 0; index <
                apiData['studentTestItems'].length; index++) {
              if (jsonLinkResponse['SectionResponse'][i]['ItemResponse'][j]['ItemID'] ==
                  int.parse(apiData['studentTestItems'][index]['itemId'])) {
                setState(() {
                  getQuestions.add(
                      jsonLinkResponse['SectionResponse'][i]['ItemResponse'][j]['Items']);
                  getItemId.add(apiData['studentTestItems'][index]);
                });
              }
            }
          }
        }
      }
      }
    }else {
      for (var i = 0; i < jsonLinkResponse['SectionResponse'].length; i++) {
        if (jsonLinkResponse['SectionResponse'][i]['SectionTitle'].contains("Pyschometric")
           ||
            jsonLinkResponse['SectionResponse'][i]['SectionTitle'].contains("Descriptive")
            ||
            jsonLinkResponse['SectionResponse'][i]['SectionTitle'].contains("Descriptive Writing")
                ) {
        }
        else {
          for (var j = 0; j <
              jsonLinkResponse['SectionResponse'][i]['ItemResponse']
                  .length; j++) {
            if (jsonLinkResponse['SectionResponse'][i]['SectionTitle'].contains("Pyschometric")
                ||
                jsonLinkResponse['SectionResponse'][i]['SectionTitle'].contains("Descriptive")
                ||
                jsonLinkResponse['SectionResponse'][i]['SectionTitle'].contains("Descriptive Writing")
            ) {
            }
            else{
            for (var index = 0; index <
                apiData['studentTestItems'].length; index++) {
              if (jsonLinkResponse['SectionResponse'][i]['ItemResponse'][j]['ItemID'] ==
                  int.parse(apiData['studentTestItems'][index]['itemId'])) {
                setState(() {
                  getQuestions.add(
                      jsonLinkResponse['SectionResponse'][i]['ItemResponse'][j]['Items']);
                  getItemId.add(apiData['studentTestItems'][index]);
                });
              }
            }
          }
        }
      }
    }
    }
  }


  void loadItemWiseQuestions(String selectedLabel3) {

    setState(() {
      getQuestions = [];
      getItemId = [];
    });

    if((selectedSection != "" && selectedSection != "All Sections")){
      print("*****IF********selectedSection"+selectedSection);
      getSectionWiseItemQuestions(selectedSection);
      if(selectedAreaValue!=null){
        if(( selectedAreaValue != "All Areas") ){
          loadAreaWiseQuestions(selectedAreaValue);
          print("*****IF********selectedAreaValue"+selectedAreaValue);
          if(selectedTopicValue!=null){
            if(selectedTopicValue != "All Topics"){
              loadTopicwiseQuestions(selectedTopicValue);
              print("*******IF******selectedTopicValue"+selectedTopicValue);
            }else{
              loadTopicwiseQuestions("All Topics");
              print("*****ELSE********selectedTopicValue"+selectedTopicValue);
            }
          }else{
            print("@@@@@@@@@@@@@@@@@@ Topic");
          }
        }else{
          print("*****ELSE********selectedAreaValue"+selectedAreaValue);
          loadAreaWiseQuestions("All Areas");
        }
      }else{
        print("@@@@@@@@@@@@@@@@@@ Area");
      }
    }else{
      print("******ELSE*******selectedSection"+selectedSection);
      if(selectedAreaValue!=null){
        if(( selectedAreaValue != "All Areas") ){
          loadAreaWiseQuestions(selectedAreaValue);
          print("*****IF********selectedAreaValue"+selectedAreaValue);
          if(selectedTopicValue!=null){
            if(selectedTopicValue != "All Topics"){
              loadTopicwiseQuestions(selectedTopicValue);
              print("*******IF******selectedTopicValue"+selectedTopicValue);
            }else{
              loadAreaWiseQuestions(selectedAreaValue);
              print("*****ELSE********selectedTopicValue"+selectedTopicValue);
            }
          }else{
            print("@@@@@@@@@@@@@@@@@@ Topic");
          }
        }else{
          print("*****ELSE********selectedAreaValue"+selectedAreaValue +selectedItemTypeValue);
          loadAllQuestions();
        }
      }
      else{
        print("*****Else*** Null *****selectedAreaValue");

        for (var i = 0; i < jsonLinkResponse['SectionResponse'].length; i++) {
          for (var j = 0;
          j < jsonLinkResponse['SectionResponse'][i]['ItemResponse'].length;
          j++) {
            for (var index = 0; index <
                apiData['studentTestItems'].length; index++) {
              if (jsonLinkResponse['SectionResponse'][i]['ItemResponse'][j]
              ['ItemID'] ==
                  int.parse(apiData['studentTestItems'][index]['itemId'])) {
                if (selectedLabel3 != "All Items") {
                  if (selectedItemTypeValue == "MCQ" ||
                      selectedItemTypeValue == "MCA") {
                    if (selectedItemTypeValue ==
                        (jsonLinkResponse['SectionResponse'][i]['ItemResponse'][j]
                        ['ItemType'].toString().toUpperCase())) {
                      setState(() {
                        getQuestions.add(jsonLinkResponse['SectionResponse'][i]
                        ['ItemResponse'][j]['Items']);
                        getItemId.add(apiData['studentTestItems'][index]);
                      });
                    }
                  } else if (selectedItemTypeValue == "TITA") {
                    if ((jsonLinkResponse['SectionResponse'][i]['ItemResponse'][j]
                    ['ItemType'].toString().toUpperCase()) ==
                        "AN" ||
                        (jsonLinkResponse['SectionResponse'][i]['ItemResponse'][j]
                        ['ItemType'].toString().toUpperCase()) ==
                            "BN" ||
                        (jsonLinkResponse['SectionResponse'][i]['ItemResponse'][j]
                        ['ItemType'].toString().toUpperCase()) ==
                            "NE") {
                      setState(() {
                        getQuestions.add(jsonLinkResponse['SectionResponse'][i]
                        ['ItemResponse'][j]['Items']);
                        getItemId.add(apiData['studentTestItems'][index]);
                      });
                    }
                  } else if ((jsonLinkResponse['SectionResponse'][i]
                  ['ItemResponse'][j]['ItemType'].toString().toUpperCase()) !=
                      "AN" &&
                      (jsonLinkResponse['SectionResponse'][i]['ItemResponse'][j]
                      ['ItemType'].toString().toUpperCase()) !=
                          "BN" &&
                      (jsonLinkResponse['SectionResponse'][i]['ItemResponse'][j]
                      ['ItemType'].toString().toUpperCase()) !=
                          "NE" &&
                      (jsonLinkResponse['SectionResponse'][i]['ItemResponse'][j]
                      ['ItemType'].toString().toUpperCase()) !=
                          "MCA" &&
                      (jsonLinkResponse['SectionResponse'][i]['ItemResponse'][j]
                      ['ItemType'].toString().toUpperCase()) !=
                          "MCQ") {
                    setState(() {
                      getQuestions.add(jsonLinkResponse['SectionResponse'][i]
                      ['ItemResponse'][j]['Items']);
                      getItemId.add(apiData['studentTestItems'][index]);
                    });
                  }
                } else {
                  setState(() {
                    getQuestions.add(jsonLinkResponse['SectionResponse'][i]
                    ['ItemResponse'][j]['Items']);
                    getItemId.add(apiData['studentTestItems'][index]);
                  });
                }
              }
            }
          }
        }
      }

    }




      // if ((selectedSection != "" && selectedSection != "All Sections") ||
      //     ( selectedAreaValue != "All Areas") ||
      //     (selectedTopicValue != "All Topics")) {
      //
      //   //Section is Selected and Area is Selected and Topics is not Selected
      //   if ((selectedSection != "" && selectedSection != "All Sections") &&
      //       ( selectedAreaValue != "All Areas") &&
      //       selectedTopicValue == "All Topics") {
      //     print("&&&&&&&&&&&&&&&&Coming loadItemWiseQuestions 1");
      //
      //     loadAreaWiseQuestions(selectedAreaValue);
      //     //Only Area is selected
      //   } else if ( ( selectedAreaValue != "All Areas") &&
      //       (selectedSection == null || selectedSection == "All Sections") &&
      //       selectedTopicValue == "All Topics") {
      //     print("&&&&&&&&&&&&&&&&Coming loadItemWiseQuestions 2");
      //
      //     loadAreaWiseQuestions(selectedAreaValue);
      //     //Area and Topic is Selected
      //   } else if ( ( selectedAreaValue != "All Areas")&&
      //       selectedTopicValue != "All Topics" &&
      //       (selectedSection == null || selectedSection == "All Sections")) {
      //     print("&&&&&&&&&&&&&&&&Coming loadItemWiseQuestions 3");
      //
      //     loadTopicwiseQuestions(selectedTopicValue);
      //     //Only Section is Selected
      //   } else if (selectedSection != "" &&
      //       selectedSection != "All Sections" &&
      //       ( selectedAreaValue != "All Areas")&&
      //       selectedTopicValue == "All Topics") {
      //     print("&&&&&&&&&&&&&&&&Coming loadItemWiseQuestions 4");
      //
      //     getSectionWiseItemQuestions(selectedSection);
      //     //All the Dropdown is selected
      //   } else if ((selectedSection != "" && selectedSection != "All Sections") &&
      //       ( selectedAreaValue != "All Areas") &&
      //       selectedTopicValue != "All Topics") {
      //     print("&&&&&&&&&&&&&&&&Coming loadItemWiseQuestions 5");
      //
      //     loadTopicwiseQuestions(selectedTopicValue);
      //   } else {
      //     print("&&&&&&&&&&&&&&&&Coming loadItemWiseQuestions 6");
      //     loadAreaWiseQuestions(selectedAreaValue);
      //
      //   }
      // }
      // else {
      //   print("&&&&&&&&&&&&&&&&Coming loadItemWiseQuestions");
      //   print("********************loadItemWiseQuestions Else");
      //   for (var i = 0; i < jsonLinkResponse['SectionResponse'].length; i++) {
      //     for (var j = 0;
      //     j < jsonLinkResponse['SectionResponse'][i]['ItemResponse'].length;
      //     j++) {
      //       for (var index = 0; index <
      //           apiData['studentTestItems'].length; index++) {
      //         if (jsonLinkResponse['SectionResponse'][i]['ItemResponse'][j]
      //         ['ItemID'] ==
      //             int.parse(apiData['studentTestItems'][index]['itemId'])) {
      //           if (selectedLabel3 != "All Items") {
      //             if (selectedItemTypeValue == "MCQ" ||
      //                 selectedItemTypeValue == "MCA") {
      //               if (selectedItemTypeValue ==
      //                   (jsonLinkResponse['SectionResponse'][i]['ItemResponse'][j]
      //                   ['ItemType'].toString().toUpperCase())) {
      //                 setState(() {
      //                   getQuestions.add(jsonLinkResponse['SectionResponse'][i]
      //                   ['ItemResponse'][j]['Items']);
      //                   getItemId.add(apiData['studentTestItems'][index]);
      //                 });
      //               }
      //             } else if (selectedItemTypeValue == "TITA") {
      //               if ((jsonLinkResponse['SectionResponse'][i]['ItemResponse'][j]
      //               ['ItemType'].toString().toUpperCase()) ==
      //                   "AN" ||
      //                   (jsonLinkResponse['SectionResponse'][i]['ItemResponse'][j]
      //                   ['ItemType'].toString().toUpperCase()) ==
      //                       "BN" ||
      //                   (jsonLinkResponse['SectionResponse'][i]['ItemResponse'][j]
      //                   ['ItemType'].toString().toUpperCase()) ==
      //                       "NE") {
      //                 setState(() {
      //                   getQuestions.add(jsonLinkResponse['SectionResponse'][i]
      //                   ['ItemResponse'][j]['Items']);
      //                   getItemId.add(apiData['studentTestItems'][index]);
      //                 });
      //               }
      //             } else if ((jsonLinkResponse['SectionResponse'][i]
      //             ['ItemResponse'][j]['ItemType'].toString().toUpperCase()) !=
      //                 "AN" &&
      //                 (jsonLinkResponse['SectionResponse'][i]['ItemResponse'][j]
      //                 ['ItemType'].toString().toUpperCase()) !=
      //                     "BN" &&
      //                 (jsonLinkResponse['SectionResponse'][i]['ItemResponse'][j]
      //                 ['ItemType'].toString().toUpperCase()) !=
      //                     "NE" &&
      //                 (jsonLinkResponse['SectionResponse'][i]['ItemResponse'][j]
      //                 ['ItemType'].toString().toUpperCase()) !=
      //                     "MCA" &&
      //                 (jsonLinkResponse['SectionResponse'][i]['ItemResponse'][j]
      //                 ['ItemType'].toString().toUpperCase()) !=
      //                     "MCQ") {
      //               setState(() {
      //                 getQuestions.add(jsonLinkResponse['SectionResponse'][i]
      //                 ['ItemResponse'][j]['Items']);
      //                 getItemId.add(apiData['studentTestItems'][index]);
      //               });
      //             }
      //           } else {
      //             setState(() {
      //               getQuestions.add(jsonLinkResponse['SectionResponse'][i]
      //               ['ItemResponse'][j]['Items']);
      //               getItemId.add(apiData['studentTestItems'][index]);
      //             });
      //           }
      //         }
      //       }
      //     }
      //   }
      // }


  }

  saveSection(String val) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('sectionVal', val);
  }

  void loadTopicwiseQuestions(String selectedLabel2) {
    if (selectedLabel2 == "All Topics") {
      if ( ( selectedAreaValue != "All Areas")) {
        loadAreaWiseQuestions(selectedAreaValue);
      }else{
        loadAreaWiseQuestions("All Areas");
      }
    } else {
      print("Coming loadTopicwiseQuestions else");
      setState(() {
        getQuestions = [];
        getItemId = [];
      });
        for (var i = 0; i < jsonLinkResponse['SectionResponse'].length; i++) {
          print("Entering first for");
          for (var j = 0;
              j < jsonLinkResponse['SectionResponse'][i]['ItemResponse'].length;
              j++) {
            print("Entering second for");
            if (selectedAreaValue ==
                jsonLinkResponse['SectionResponse'][i]['ItemResponse']
                [j]['AreaName']) {
              if (selectedLabel2 ==
                  (jsonLinkResponse['SectionResponse'][i]['ItemResponse'][j]
                  ['TopicName'])) {
                print("***********topic"+selectedLabel2);
            for (var index = 0; index < apiData['studentTestItems'].length; index++) {
                if (jsonLinkResponse['SectionResponse'][i]['ItemResponse'][j]
                ['ItemID'] ==
                    int.parse(apiData['studentTestItems'][index]['itemId'])) {
                  print("****Entered*******topic"+selectedLabel2);

                  if (selectedItemTypeValue != null) {
                      print("######################## Load Topic wise"+selectedAreaValue + selectedTopicValue +selectedItemTypeValue);

                      if (selectedItemTypeValue != "All Items") {
                        if (selectedItemTypeValue == "MCQ" ||
                            selectedItemTypeValue == "MCA") {
                          if (selectedItemTypeValue ==
                              (jsonLinkResponse['SectionResponse'][i]
                              ['ItemResponse'][j]['ItemType']
                                  .toString()
                                  .toUpperCase())) {
                            setState(() {
                              getQuestions.add(
                                  jsonLinkResponse['SectionResponse']
                                  [i]['ItemResponse'][j]['Items']);
                              getItemId.add(apiData['studentTestItems'][index]);
                            });
                          }
                        } else if (selectedItemTypeValue == "TITA") {
                          if ((jsonLinkResponse['SectionResponse'][i]
                          ['ItemResponse'][j]['ItemType']
                              .toString()
                              .toUpperCase()) ==
                              "AN" ||
                              (jsonLinkResponse['SectionResponse'][i]
                              ['ItemResponse'][j]['ItemType']
                                  .toString()
                                  .toUpperCase()) ==
                                  "BN" ||
                              (jsonLinkResponse['SectionResponse'][i]
                              ['ItemResponse'][j]['ItemType']
                                  .toString()
                                  .toUpperCase()) ==
                                  "NE") {
                            setState(() {
                              getQuestions.add(
                                  jsonLinkResponse['SectionResponse']
                                  [i]['ItemResponse'][j]['Items']);
                              getItemId.add(apiData['studentTestItems'][index]);
                            });
                          }
                        } else if ((jsonLinkResponse['SectionResponse'][i]
                        ['ItemResponse'][j]['ItemType']
                            .toString()
                            .toUpperCase()) !=
                            "AN" &&
                            (jsonLinkResponse['SectionResponse'][i]['ItemResponse']
                            [j]['ItemType'].toString().toUpperCase()) !=
                                "BN" &&
                            (jsonLinkResponse['SectionResponse'][i]['ItemResponse']
                            [j]['ItemType'].toString().toUpperCase()) !=
                                "NE" &&
                            (jsonLinkResponse['SectionResponse'][i]['ItemResponse']
                            [j]['ItemType'].toString().toUpperCase()) !=
                                "MCA" &&
                            (jsonLinkResponse['SectionResponse'][i]['ItemResponse']
                            [j]['ItemType'].toString().toUpperCase()) !=
                                "MCQ") {
                          setState(() {
                            getQuestions.add(
                                jsonLinkResponse['SectionResponse'][i]
                                ['ItemResponse'][j]['Items']);
                            getItemId.add(apiData['studentTestItems'][index]);
                          });
                        }
                      } else {
                        setState(() {
                          getQuestions.add(
                              jsonLinkResponse['SectionResponse'][i]
                              ['ItemResponse'][j]['Items']);
                          getItemId.add(apiData['studentTestItems'][index]);
                        });
                      }
                    } else {
                      print("######################### ELSE");
                      setState(() {
                        getQuestions.add(jsonLinkResponse['SectionResponse'][i]
                        ['ItemResponse'][j]['Items']);
                        getItemId.add(apiData['studentTestItems'][index]);
                      });
                    }
                  }
                }

            }
              }
        }
      }
    }
  }

  String statusValidation(bool Lstatus) {
    if (Lstatus != null) {
      if (Lstatus) {
        //status = "Correct";
        //setState(() {
          status = "assets/images/status_tick.png";
        //});
      } else {
        //status = "Incorrect";
        //setState(() {
          status = "assets/images/status_cross.png";
        //});
      }
    } else {
      //status = "Skipped";
      //setState(() {
          status = "assets/images/status_hyphen.png";
       // });
    }

    return status;
  }
  // void getSectionWiseQuestions(String selectedSection,bool flag) {
  //   if (selectedSection == "All Sections") {
  //     setState(() {
  //       getQuestions = [];
  //       getItemId = [];
  //       getTopicsDropDownItems = ["All Topics"];
  //       selectedAreaValue = "All Areas";
  //       selectedTopicValue = "All Topics";
  //      // selectedItemTypeValue = "All Items";
  //       if(!flag){
  //         selectedItemTypeValue = "All Items";
  //       }else{
  //       }
  //     });
  //     loadAllQuestions();
  //     loadAllDropdownItemsForAreas();
  //
  //   } else {
  //     loadDropdownItemsForAreas(selectedSection);
  //     setState(() {
  //       getQuestions = [];
  //       getItemId = [];
  //       getTopicsDropDownItems = ["All Topics"];
  //       selectedAreaValue = "All Areas";
  //       selectedTopicValue = "All Topics";
  //      // selectedItemTypeValue = "All Items";
  //       if(flag){
  //       }else{
  //         selectedItemTypeValue = "All Items";
  //       }
  //     });
  //     for (var i = 0; i < jsonLinkResponse['SectionResponse'].length; i++) {
  //       for (var j = 0;
  //       j < jsonLinkResponse['SectionResponse'][i]['ItemResponse'].length;
  //       j++) {
  //         for (var index = 0; index < apiData['studentTestItems'].length; index++) {
  //
  //           if (jsonLinkResponse['SectionResponse'][i]['ItemResponse'][j]
  //           ['ItemID'] ==
  //               int.parse(apiData['studentTestItems'][index]['itemId'])) {
  //             if (selectedSection ==
  //                 jsonLinkResponse['SectionResponse'][i]['SectionTitle']) {
  //               setState(() {
  //                 getQuestions.add(jsonLinkResponse['SectionResponse'][i]
  //                 ['ItemResponse'][j]['Items']);
  //                 getItemId.add(apiData['studentTestItems'][index]);
  //               });
  //
  //             }
  //           }
  //         }
  //       }
  //     }
  //   }
  // }

  void showErrorMessage(String value) {
    _scaffoldstate.currentState.showSnackBar(new SnackBar(
      content: new Text(
        value,
        textAlign: TextAlign.center,
        style: TextStyles.buttonTextStyle,
      ),
      duration: new Duration(seconds: 3),
      backgroundColor: NeutralColors.black,
    ));
  }

  var pointsForMarks;
  var authorization;
  var courseID;
  void getSummaryData(Map t) async {
    if (global.headersWithOtherAuthorizationKey['Authorization'] != '') {
      print("Testttttttttttttttttt IDDDDDDDDDDDDDDDDDD" + testId.toString());
      ApiService()
          .getAPI(URL.ANALYTICS_API_PERFORMANCE_ANALYTICS + testId, t)
          .then((result) {
        setState(() {
          if (result[0] == 'Success') {
            for (var i = 0; i < result[1]['data']['studentTestItems'].length; i++) {

              /// section map contains key sectionName ................
              if (section.containsKey(result[1]['data']['studentTestItems'][i]['sectionName'])) {
                areaName = section[result[1]['data']['studentTestItems'][i]['sectionName']];

                /// area map contains key areaName ..............
                if (areaName.containsKey(result[1]['data']['studentTestItems'][i]['areaName'])) {
                  topicNames = areaName[result[1]['data']['studentTestItems'][i]['areaName']];

                  /// topic map contains key topicName ..............
                  if (topicNames.containsKey(result[1]['data']['studentTestItems'][i]['topicName'])) {
                    List<Data> marksDataList = topicNames[result[1]['data']['studentTestItems'][i]['topicName']];
                    var marksData = new Data();
                    /// when isCorrect is true .........................................

                    if (result[1]['data']['studentTestItems'][i]['isCorrect'] == true) {
                      marksData.marks = result[1]['data']['studentTestItems'][i]['points'];
                      pointsForMarks = result[1]['data']['studentTestItems'][i]['points'];
                      marksData.negativeMarks = 0;
                      marksData.attempts = '1';
                      marksData.isCorrect = 1;
                      marksData.timeTakenForCorrectAnswer= result[1]['data']['studentTestItems'][i]['timeTaken'];
                      if(marksData.totalNumberOfQuestionsForCorrectAnswer==null){
                        marksData.totalNumberOfQuestionsForCorrectAnswer=0;
                      }
                      marksData.totalNumberOfQuestionsForCorrectAnswer=marksData.totalNumberOfQuestionsForCorrectAnswer+1;
                      marksData.timeTakenFonINCorrectAnswer=0;
                      marksData.totalNumberOfQuestionsForInCorrectAnswer=0;
                      marksData.timeTakenForSkippedAnswer=0;
                      marksData.totalNumberOfQuestionsForSkippedAnswer=0;
                    }

                    /// when isCorrect is false .........................................

                    else if (result[1]['data']['studentTestItems'][i]['isCorrect'] ==false) {
                      marksData.marks = 0;
                      marksData.negativeMarks = result[1]['data']['studentTestItems'][i]['negativePoints'];
                      marksData.attempts = '1';
                      marksData.isCorrect = 0;
                      marksData.timeTakenForCorrectAnswer= 0;
                      marksData.totalNumberOfQuestionsForCorrectAnswer=0;
                      marksData.timeTakenFonINCorrectAnswer=result[1]['data']['studentTestItems'][i]['timeTaken'];
                      if( marksData.totalNumberOfQuestionsForInCorrectAnswer==null){
                        marksData.totalNumberOfQuestionsForInCorrectAnswer=0;
                      }
                      marksData.totalNumberOfQuestionsForInCorrectAnswer=marksData.totalNumberOfQuestionsForInCorrectAnswer+1;
                      marksData.timeTakenForSkippedAnswer=0;
                      marksData.totalNumberOfQuestionsForSkippedAnswer=0;
                    }

                    /// when isCorrect is null .........................................

                    else if (result[1]['data']['studentTestItems'][i]['isCorrect'] == null) {
                      //marksData.attempts = '0';
                      marksData.marks = 0;
                      marksData.negativeMarks = 0;
                      marksData.isCorrect = 0;
                      marksData.isNotNull = 0;
                      marksData.timeTakenNotNull = 0;
                      marksData.timeTakenForCorrectAnswer= 0;
                      marksData.totalNumberOfQuestionsForCorrectAnswer=0;
                      marksData.timeTakenFonINCorrectAnswer=0;
                      marksData.totalNumberOfQuestionsForInCorrectAnswer=0;
                      marksData.timeTakenForSkippedAnswer=result[1]['data']['studentTestItems'][i]['timeTaken'];
                      if(marksData.totalNumberOfQuestionsForSkippedAnswer==null){
                        marksData.totalNumberOfQuestionsForSkippedAnswer=0;
                      }
                      marksData.totalNumberOfQuestionsForSkippedAnswer=marksData.totalNumberOfQuestionsForSkippedAnswer+1;
                    }

                    /// when isCorrect is not equal to null .........................................
                    if (result[1]['data']['studentTestItems'][i]['isCorrect'] != null) {
                      marksData.isNotNull = 1;
                      marksData.timeTakenNotNull = result[1]['data']['studentTestItems'][i]['timeTaken'];
                    }

                    marksData.timeTaken = result[1]['data']['studentTestItems'][i]['timeTaken'];
                    marksData.questionIndex = result[1]['data']['studentTestItems'][i]['questionIndex'];
                    marksDataList.add(marksData);
                    topicNames[result[1]['data']['studentTestItems'][i]['topicName']] = marksDataList;
                    areaName[result[1]['data']['studentTestItems'][i]['areaName']] = topicNames;
                    section[result[1]['data']['studentTestItems'][i]['sectionName']] = areaName;
                  }

                  /// topic map doesn't contains key topicName ..............
                  else {
                    List<Data> data = new List();
                    var marksData = new Data();

                    /// when isCorrect is true .........................................

                    if (result[1]['data']['studentTestItems'][i]['isCorrect'] == true) {
                      marksData.marks = result[1]['data']['studentTestItems'][i]['points'];
                      marksData.negativeMarks = 0;
                      marksData.attempts = '1';
                      marksData.isCorrect = 1;
                      marksData.timeTakenForCorrectAnswer= result[1]['data']['studentTestItems'][i]['timeTaken'];
                      if(marksData.totalNumberOfQuestionsForCorrectAnswer==null){
                        marksData.totalNumberOfQuestionsForCorrectAnswer=0;
                      }
                      marksData.totalNumberOfQuestionsForCorrectAnswer=marksData.totalNumberOfQuestionsForCorrectAnswer+1;
                      marksData.timeTakenFonINCorrectAnswer=0;
                      marksData.totalNumberOfQuestionsForInCorrectAnswer=0;
                      marksData.timeTakenForSkippedAnswer=0;
                      marksData.totalNumberOfQuestionsForSkippedAnswer=0;
                    }

                    /// when isCorrect is false .........................................

                    else if (result[1]['data']['studentTestItems'][i]['isCorrect'] == false) {
                      marksData.marks = 0;
                      marksData.negativeMarks = result[1]['data']['studentTestItems'][i]['negativePoints'];
                      marksData.attempts = '1';
                      marksData.isCorrect = 0;
                      marksData.timeTakenForCorrectAnswer= 0;
                      marksData.totalNumberOfQuestionsForCorrectAnswer=0;
                      marksData.timeTakenFonINCorrectAnswer=result[1]['data']['studentTestItems'][i]['timeTaken'];
                      if( marksData.totalNumberOfQuestionsForInCorrectAnswer==null){
                        marksData.totalNumberOfQuestionsForInCorrectAnswer=0;
                      }
                      marksData.totalNumberOfQuestionsForInCorrectAnswer=marksData.totalNumberOfQuestionsForInCorrectAnswer+1;
                      marksData.timeTakenForSkippedAnswer=0;
                      marksData.totalNumberOfQuestionsForSkippedAnswer=0;
                    }

                    /// when isCorrect is null .........................................

                    else if (result[1]['data']['studentTestItems'][i]['isCorrect'] == null) {
                      //marksData.attempts = '0';
                      marksData.marks = 0;
                      marksData.negativeMarks = 0;
                      marksData.isCorrect = 0;
                      marksData.isNotNull = 0;
                      marksData.timeTakenNotNull = 0;
                      marksData.timeTakenForCorrectAnswer= 0;
                      marksData.totalNumberOfQuestionsForCorrectAnswer=0;
                      marksData.timeTakenFonINCorrectAnswer=0;
                      marksData.totalNumberOfQuestionsForInCorrectAnswer=0;
                      marksData.timeTakenForSkippedAnswer=result[1]['data']['studentTestItems'][i]['timeTaken'];
                      if(marksData.totalNumberOfQuestionsForSkippedAnswer==null){
                        marksData.totalNumberOfQuestionsForSkippedAnswer=0;
                      }
                      marksData.totalNumberOfQuestionsForSkippedAnswer=marksData.totalNumberOfQuestionsForSkippedAnswer+1;
                    }
                    if (result[1]['data']['studentTestItems'][i]['isCorrect'] != null) {
                      marksData.isNotNull = 1;
                      marksData.timeTakenNotNull = result[1]['data']['studentTestItems'][i]['timeTaken'];
                    }
                    marksData.timeTaken = result[1]['data']['studentTestItems'][i]['timeTaken'];
                    marksData.questionIndex = result[1]['data']['studentTestItems'][i]['questionIndex'];
                    data.add(marksData);

                    topicNames[result[1]['data']['studentTestItems'][i]['topicName']] = data;
                    areaName[result[1]['data']['studentTestItems'][i]['areaName']] = topicNames;
                    section[result[1]['data']['studentTestItems'][i]['sectionName']] = areaName;
                  }
                }

                /// area map doesn't contains key areaName ..............
                else {
                  List<Data> data = new List();
                  var marksData = new Data();

                  /// when isCorrect is true .........................................

                  if (result[1]['data']['studentTestItems'][i]['isCorrect'] == true) {
                    marksData.marks = result[1]['data']['studentTestItems'][i]['points'];
                    marksData.negativeMarks = 0;
                    marksData.attempts = '1';
                    marksData.isCorrect = 1;
                    marksData.timeTakenForCorrectAnswer= result[1]['data']['studentTestItems'][i]['timeTaken'];
                    if(marksData.totalNumberOfQuestionsForCorrectAnswer==null){
                      marksData.totalNumberOfQuestionsForCorrectAnswer=0;
                    }
                    marksData.totalNumberOfQuestionsForCorrectAnswer=marksData.totalNumberOfQuestionsForCorrectAnswer+1;
                    marksData.timeTakenFonINCorrectAnswer=0;
                    marksData.totalNumberOfQuestionsForInCorrectAnswer=0;
                    marksData.timeTakenForSkippedAnswer=0;
                    marksData.totalNumberOfQuestionsForSkippedAnswer=0;
                  }

                  /// when isCorrect is false .........................................

                  else if (result[1]['data']['studentTestItems'][i]['isCorrect'] == false) {
                    marksData.marks = 0;
                    marksData.negativeMarks = result[1]['data']['studentTestItems'][i]['negativePoints'];
                    marksData.attempts = '1';
                    marksData.isCorrect = 0;
                    marksData.timeTakenForCorrectAnswer= 0;
                    marksData.totalNumberOfQuestionsForCorrectAnswer=0;
                    marksData.timeTakenFonINCorrectAnswer=result[1]['data']['studentTestItems'][i]['timeTaken'];
                    if( marksData.totalNumberOfQuestionsForInCorrectAnswer==null){
                      marksData.totalNumberOfQuestionsForInCorrectAnswer=0;
                    }
                    marksData.totalNumberOfQuestionsForInCorrectAnswer=marksData.totalNumberOfQuestionsForInCorrectAnswer+1;
                    marksData.timeTakenForSkippedAnswer=0;
                    marksData.totalNumberOfQuestionsForSkippedAnswer=0;
                  }

                  /// when isCorrect is null .........................................

                  else if (result[1]['data']['studentTestItems'][i]['isCorrect'] == null) {
                    //marksData.attempts = '0';
                    marksData.marks = 0;
                    marksData.negativeMarks = 0;
                    marksData.isCorrect = 0;
                    marksData.isNotNull = 0;
                    marksData.timeTakenNotNull = 0;
                    marksData.timeTakenForCorrectAnswer= 0;
                    marksData.totalNumberOfQuestionsForCorrectAnswer=0;
                    marksData.timeTakenFonINCorrectAnswer=0;
                    marksData.totalNumberOfQuestionsForInCorrectAnswer=0;
                    marksData.timeTakenForSkippedAnswer=result[1]['data']['studentTestItems'][i]['timeTaken'];
                    if(marksData.totalNumberOfQuestionsForSkippedAnswer==null){
                      marksData.totalNumberOfQuestionsForSkippedAnswer=0;
                    }
                    marksData.totalNumberOfQuestionsForSkippedAnswer=marksData.totalNumberOfQuestionsForSkippedAnswer+1;
                  }

                  if (result[1]['data']['studentTestItems'][i]['isCorrect'] != null) {
                    marksData.isNotNull = 1;
                    marksData.timeTakenNotNull =
                        result[1]['data']['studentTestItems'][i]['timeTaken'];
                  }
                  marksData.timeTaken = result[1]['data']['studentTestItems'][i]['timeTaken'];
                  marksData.questionIndex = result[1]['data']['studentTestItems'][i]['questionIndex'];
                  data.add(marksData);
                  topic = new Map<String, List<Data>>();
                  topic = {
                    result[1]['data']['studentTestItems'][i]['topicName']: data
                  };
                  areaName[result[1]['data']['studentTestItems'][i]['areaName']] = topic;
                  section[result[1]['data']['studentTestItems'][i]['sectionName']] = areaName;
                }

              }

              /// section map doesn't contains key sectionName ................
              else {
                List<Data> data = new List();
                var marksData = new Data();

                /// when isCorrect is true .........................................

                if (result[1]['data']['studentTestItems'][i]['isCorrect'] == true) {
                  marksData.marks = result[1]['data']['studentTestItems'][i]['points'];
                  marksData.negativeMarks = 0;
                  marksData.attempts = '1';
                  marksData.isCorrect = 1;
                  marksData.timeTakenForCorrectAnswer= result[1]['data']['studentTestItems'][i]['timeTaken'];
                  if(marksData.totalNumberOfQuestionsForCorrectAnswer==null){
                    marksData.totalNumberOfQuestionsForCorrectAnswer=0;
                  }
                  marksData.totalNumberOfQuestionsForCorrectAnswer=marksData.totalNumberOfQuestionsForCorrectAnswer+1;
                  marksData.timeTakenFonINCorrectAnswer=0;
                  marksData.totalNumberOfQuestionsForInCorrectAnswer=0;
                  marksData.timeTakenForSkippedAnswer=0;
                  marksData.totalNumberOfQuestionsForSkippedAnswer=0;
                }

                /// when isCorrect is false .........................................

                else if (result[1]['data']['studentTestItems'][i]['isCorrect'] == false) {
                  marksData.marks = 0;
                  marksData.negativeMarks = result[1]['data']['studentTestItems'][i]['negativePoints'];
                  marksData.attempts = '1';
                  marksData.isCorrect = 0;
                  marksData.timeTakenForCorrectAnswer= 0;
                  marksData.totalNumberOfQuestionsForCorrectAnswer=0;
                  marksData.timeTakenFonINCorrectAnswer=result[1]['data']['studentTestItems'][i]['timeTaken'];
                  if( marksData.totalNumberOfQuestionsForInCorrectAnswer==null){
                    marksData.totalNumberOfQuestionsForInCorrectAnswer=0;
                  }
                  marksData.totalNumberOfQuestionsForInCorrectAnswer=marksData.totalNumberOfQuestionsForInCorrectAnswer+1;
                  marksData.timeTakenForSkippedAnswer=0;
                  marksData.totalNumberOfQuestionsForSkippedAnswer=0;
                }

                /// when isCorrect is null .........................................

                else if (result[1]['data']['studentTestItems'][i]['isCorrect'] == null) {
                  //marksData.attempts = '0';
                  marksData.marks = 0;
                  marksData.negativeMarks = 0;
                  marksData.isCorrect = 0;
                  marksData.isNotNull = 0;
                  marksData.timeTakenNotNull = 0;
                  marksData.timeTakenForCorrectAnswer= 0;
                  marksData.totalNumberOfQuestionsForCorrectAnswer=0;
                  marksData.timeTakenFonINCorrectAnswer=0;
                  marksData.totalNumberOfQuestionsForInCorrectAnswer=0;
                  marksData.timeTakenForSkippedAnswer=result[1]['data']['studentTestItems'][i]['timeTaken'];
                  if(marksData.totalNumberOfQuestionsForSkippedAnswer==null){
                    marksData.totalNumberOfQuestionsForSkippedAnswer=0;
                  }
                  marksData.totalNumberOfQuestionsForSkippedAnswer=marksData.totalNumberOfQuestionsForSkippedAnswer+1;
                }

                if (result[1]['data']['studentTestItems'][i]['isCorrect'] != null) {
                  marksData.isNotNull = 1;
                  marksData.timeTakenNotNull = result[1]['data']['studentTestItems'][i]['timeTaken'];
                }

                marksData.timeTaken = result[1]['data']['studentTestItems'][i]['timeTaken'];
                marksData.questionIndex = result[1]['data']['studentTestItems'][i]['questionIndex'];
                data.add(marksData);
                topic = new Map<String, List<Data>>();
                topic = {
                  result[1]['data']['studentTestItems'][i]['topicName']: data
                };
                area = {
                  result[1]['data']['studentTestItems'][i]['areaName']: topic
                };
                section[result[1]['data']['studentTestItems'][i]
                    ['sectionName']] = area;
                //section.p
              }


            }



            print("**************** Section Data" + section.toString());

            var totalmarks = 0.0;
            var negative = 0.0;
            List negativeScore = [];
            for (var i = 0; i < section.length; i++) {
              sectionName.add(section.keys.elementAt(i));
              for (var j = 0; j < section.values.elementAt(i).length; j++) {
                for (var k = 0;
                    k < section.values.elementAt(i).values.elementAt(j).length;
                    k++) {
                  for (int x = 0;
                      x <
                          section.values
                              .elementAt(i)
                              .values
                              .elementAt(j)
                              .values
                              .elementAt(k)
                              .length;
                      x++) {
                    Data dataObject = section.values
                        .elementAt(i)
                        .values
                        .elementAt(j)
                        .values
                        .elementAt(k)[x];
                    totalmarks = totalmarks + dataObject.marks;
                    negative = negative + dataObject.negativeMarks;
                  }
                  negativeScore.add(negative);
                  negative = 0;
                }
              }
            }
            getListofAreas("");
            getTimeManagementData();
          }

          else {
            if (result[0] != 'No Internet Connection') {
              showErrorMessage(result[0]);
            }
          }
        });
      });
    }
  }
  /// Function for Api calling when selected test is Adaptive type
  void getSummaryDataForAdaptive() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    authorization = prefs.getString("loginAccessToken");
    courseID = prefs.getString("courseId");
    print("&&&&&&&&&&&courseID"+courseID);
    Map postdata = {
      "testId": testId,
      "Authorization": authorization,
      "courseId":courseID
    };
    print("Post Data"+postdata.toString());
    ApiService().postAPITerr(URL.ANALYTICS_API_PERFORMANCE_ANALYTICS_WRAPPER, postdata, global.headers).then((result){
      print("Result Adaptive"+result.toString());
      setState(() {
        if (result[0]['success']==true) {
          for (var i = 0; i < result[0]['data']['studentTestItems'].length; i++) {

            /// section map contains key sectionName ................
            if (section.containsKey(result[0]['data']['studentTestItems'][i]['sectionName'])) {
              areaName = section[result[0]['data']['studentTestItems'][i]['sectionName']];

              /// area map contains key areaName ..............
              if (areaName.containsKey(result[0]['data']['studentTestItems'][i]['areaName'])) {
                topicNames = areaName[result[0]['data']['studentTestItems'][i]['areaName']];

                /// topic map contains key topicName ..............
                if (topicNames.containsKey(result[0]['data']['studentTestItems'][i]['topicName'])) {
                  List<Data> marksDataList = topicNames[result[0]['data']['studentTestItems'][i]['topicName']];
                  var marksData = new Data();
                  /// when isCorrect is true .........................................

                  if (result[0]['data']['studentTestItems'][i]['isCorrect'] == true) {
                    marksData.marks = result[0]['data']['studentTestItems'][i]['points'];
                    pointsForMarks = result[0]['data']['studentTestItems'][i]['points'];
                    marksData.negativeMarks = 0;
                    marksData.attempts = '1';
                    marksData.isCorrect = 1;
                    marksData.timeTakenForCorrectAnswer= result[0]['data']['studentTestItems'][i]['timeTaken'];
                    if(marksData.totalNumberOfQuestionsForCorrectAnswer==null){
                      marksData.totalNumberOfQuestionsForCorrectAnswer=0;
                    }
                    marksData.totalNumberOfQuestionsForCorrectAnswer=marksData.totalNumberOfQuestionsForCorrectAnswer+1;
                    marksData.timeTakenFonINCorrectAnswer=0;
                    marksData.totalNumberOfQuestionsForInCorrectAnswer=0;
                    marksData.timeTakenForSkippedAnswer=0;
                    marksData.totalNumberOfQuestionsForSkippedAnswer=0;
                  }

                  /// when isCorrect is false .........................................

                  else if (result[0]['data']['studentTestItems'][i]['isCorrect'] ==false) {
                    marksData.marks = 0;
                    marksData.negativeMarks = result[0]['data']['studentTestItems'][i]['negativePoints'];
                    marksData.attempts = '1';
                    marksData.isCorrect = 0;
                    marksData.timeTakenForCorrectAnswer= 0;
                    marksData.totalNumberOfQuestionsForCorrectAnswer=0;
                    marksData.timeTakenFonINCorrectAnswer=result[0]['data']['studentTestItems'][i]['timeTaken'];
                    if( marksData.totalNumberOfQuestionsForInCorrectAnswer==null){
                      marksData.totalNumberOfQuestionsForInCorrectAnswer=0;
                    }
                    marksData.totalNumberOfQuestionsForInCorrectAnswer=marksData.totalNumberOfQuestionsForInCorrectAnswer+1;
                    marksData.timeTakenForSkippedAnswer=0;
                    marksData.totalNumberOfQuestionsForSkippedAnswer=0;
                  }

                  /// when isCorrect is null .........................................

                  else if (result[0]['data']['studentTestItems'][i]['isCorrect'] == null) {
                    //marksData.attempts = '0';
                    marksData.marks = 0;
                    marksData.negativeMarks = 0;
                    marksData.isCorrect = 0;
                    marksData.isNotNull = 0;
                    marksData.timeTakenNotNull = 0;
                    marksData.timeTakenForCorrectAnswer= 0;
                    marksData.totalNumberOfQuestionsForCorrectAnswer=0;
                    marksData.timeTakenFonINCorrectAnswer=0;
                    marksData.totalNumberOfQuestionsForInCorrectAnswer=0;
                    marksData.timeTakenForSkippedAnswer=result[1]['data']['studentTestItems'][i]['timeTaken'];
                    if(marksData.totalNumberOfQuestionsForSkippedAnswer==null){
                      marksData.totalNumberOfQuestionsForSkippedAnswer=0;
                    }
                    marksData.totalNumberOfQuestionsForSkippedAnswer=marksData.totalNumberOfQuestionsForSkippedAnswer+1;
                  }

                  /// when isCorrect is not equal to null .........................................
                  if (result[0]['data']['studentTestItems'][i]['isCorrect'] != null) {
                    marksData.isNotNull = 1;
                    marksData.timeTakenNotNull = result[0]['data']['studentTestItems'][i]['timeTaken'];
                  }

                  marksData.timeTaken = result[0]['data']['studentTestItems'][i]['timeTaken'];
                  marksData.questionIndex = result[0]['data']['studentTestItems'][i]['questionIndex'];
                  marksDataList.add(marksData);
                  topicNames[result[0]['data']['studentTestItems'][i]['topicName']] = marksDataList;
                  areaName[result[0]['data']['studentTestItems'][i]['areaName']] = topicNames;
                  section[result[0]['data']['studentTestItems'][i]['sectionName']] = areaName;
                }

                /// topic map doesn't contains key topicName ..............
                else {
                  List<Data> data = new List();
                  var marksData = new Data();

                  /// when isCorrect is true .........................................

                  if (result[0]['data']['studentTestItems'][i]['isCorrect'] == true) {
                    marksData.marks = result[0]['data']['studentTestItems'][i]['points'];
                    marksData.negativeMarks = 0;
                    marksData.attempts = '1';
                    marksData.isCorrect = 1;
                    marksData.timeTakenForCorrectAnswer= result[0]['data']['studentTestItems'][i]['timeTaken'];
                    if(marksData.totalNumberOfQuestionsForCorrectAnswer==null){
                      marksData.totalNumberOfQuestionsForCorrectAnswer=0;
                    }
                    marksData.totalNumberOfQuestionsForCorrectAnswer=marksData.totalNumberOfQuestionsForCorrectAnswer+1;
                    marksData.timeTakenFonINCorrectAnswer=0;
                    marksData.totalNumberOfQuestionsForInCorrectAnswer=0;
                    marksData.timeTakenForSkippedAnswer=0;
                    marksData.totalNumberOfQuestionsForSkippedAnswer=0;
                  }

                  /// when isCorrect is false .........................................

                  else if (result[0]['data']['studentTestItems'][i]['isCorrect'] == false) {
                    marksData.marks = 0;
                    marksData.negativeMarks = result[0]['data']['studentTestItems'][i]['negativePoints'];
                    marksData.attempts = '1';
                    marksData.isCorrect = 0;
                    marksData.timeTakenForCorrectAnswer= 0;
                    marksData.totalNumberOfQuestionsForCorrectAnswer=0;
                    marksData.timeTakenFonINCorrectAnswer=result[0]['data']['studentTestItems'][i]['timeTaken'];
                    if( marksData.totalNumberOfQuestionsForInCorrectAnswer==null){
                      marksData.totalNumberOfQuestionsForInCorrectAnswer=0;
                    }
                    marksData.totalNumberOfQuestionsForInCorrectAnswer=marksData.totalNumberOfQuestionsForInCorrectAnswer+1;
                    marksData.timeTakenForSkippedAnswer=0;
                    marksData.totalNumberOfQuestionsForSkippedAnswer=0;
                  }

                  /// when isCorrect is null .........................................

                  else if (result[0]['data']['studentTestItems'][i]['isCorrect'] == null) {
                    //marksData.attempts = '0';
                    marksData.marks = 0;
                    marksData.negativeMarks = 0;
                    marksData.isCorrect = 0;
                    marksData.isNotNull = 0;
                    marksData.timeTakenNotNull = 0;
                    marksData.timeTakenForCorrectAnswer= 0;
                    marksData.totalNumberOfQuestionsForCorrectAnswer=0;
                    marksData.timeTakenFonINCorrectAnswer=0;
                    marksData.totalNumberOfQuestionsForInCorrectAnswer=0;
                    marksData.timeTakenForSkippedAnswer=result[0]['data']['studentTestItems'][i]['timeTaken'];
                    if(marksData.totalNumberOfQuestionsForSkippedAnswer==null){
                      marksData.totalNumberOfQuestionsForSkippedAnswer=0;
                    }
                    marksData.totalNumberOfQuestionsForSkippedAnswer=marksData.totalNumberOfQuestionsForSkippedAnswer+1;
                  }
                  if (result[0]['data']['studentTestItems'][i]['isCorrect'] != null) {
                    marksData.isNotNull = 1;
                    marksData.timeTakenNotNull = result[0]['data']['studentTestItems'][i]['timeTaken'];
                  }
                  marksData.timeTaken = result[0]['data']['studentTestItems'][i]['timeTaken'];
                  marksData.questionIndex = result[0]['data']['studentTestItems'][i]['questionIndex'];
                  data.add(marksData);

                  topicNames[result[0]['data']['studentTestItems'][i]['topicName']] = data;
                  areaName[result[0]['data']['studentTestItems'][i]['areaName']] = topicNames;
                  section[result[0]['data']['studentTestItems'][i]['sectionName']] = areaName;
                }
              }

              /// area map doesn't contains key areaName ..............
              else {
                List<Data> data = new List();
                var marksData = new Data();

                /// when isCorrect is true .........................................

                if (result[0]['data']['studentTestItems'][i]['isCorrect'] == true) {
                  marksData.marks = result[0]['data']['studentTestItems'][i]['points'];
                  marksData.negativeMarks = 0;
                  marksData.attempts = '1';
                  marksData.isCorrect = 1;
                  marksData.timeTakenForCorrectAnswer= result[0]['data']['studentTestItems'][i]['timeTaken'];
                  if(marksData.totalNumberOfQuestionsForCorrectAnswer==null){
                    marksData.totalNumberOfQuestionsForCorrectAnswer=0;
                  }
                  marksData.totalNumberOfQuestionsForCorrectAnswer=marksData.totalNumberOfQuestionsForCorrectAnswer+1;
                  marksData.timeTakenFonINCorrectAnswer=0;
                  marksData.totalNumberOfQuestionsForInCorrectAnswer=0;
                  marksData.timeTakenForSkippedAnswer=0;
                  marksData.totalNumberOfQuestionsForSkippedAnswer=0;
                }

                /// when isCorrect is false .........................................

                else if (result[0]['data']['studentTestItems'][i]['isCorrect'] == false) {
                  marksData.marks = 0;
                  marksData.negativeMarks = result[0]['data']['studentTestItems'][i]['negativePoints'];
                  marksData.attempts = '1';
                  marksData.isCorrect = 0;
                  marksData.timeTakenForCorrectAnswer= 0;
                  marksData.totalNumberOfQuestionsForCorrectAnswer=0;
                  marksData.timeTakenFonINCorrectAnswer=result[0]['data']['studentTestItems'][i]['timeTaken'];
                  if( marksData.totalNumberOfQuestionsForInCorrectAnswer==null){
                    marksData.totalNumberOfQuestionsForInCorrectAnswer=0;
                  }
                  marksData.totalNumberOfQuestionsForInCorrectAnswer=marksData.totalNumberOfQuestionsForInCorrectAnswer+1;
                  marksData.timeTakenForSkippedAnswer=0;
                  marksData.totalNumberOfQuestionsForSkippedAnswer=0;
                }

                /// when isCorrect is null .........................................

                else if (result[0]['data']['studentTestItems'][i]['isCorrect'] == null) {
                  //marksData.attempts = '0';
                  marksData.marks = 0;
                  marksData.negativeMarks = 0;
                  marksData.isCorrect = 0;
                  marksData.isNotNull = 0;
                  marksData.timeTakenNotNull = 0;
                  marksData.timeTakenForCorrectAnswer= 0;
                  marksData.totalNumberOfQuestionsForCorrectAnswer=0;
                  marksData.timeTakenFonINCorrectAnswer=0;
                  marksData.totalNumberOfQuestionsForInCorrectAnswer=0;
                  marksData.timeTakenForSkippedAnswer=result[0]['data']['studentTestItems'][i]['timeTaken'];
                  if(marksData.totalNumberOfQuestionsForSkippedAnswer==null){
                    marksData.totalNumberOfQuestionsForSkippedAnswer=0;
                  }
                  marksData.totalNumberOfQuestionsForSkippedAnswer=marksData.totalNumberOfQuestionsForSkippedAnswer+1;
                }

                if (result[0]['data']['studentTestItems'][i]['isCorrect'] != null) {
                  marksData.isNotNull = 1;
                  marksData.timeTakenNotNull =
                  result[0]['data']['studentTestItems'][i]['timeTaken'];
                }
                marksData.timeTaken = result[0]['data']['studentTestItems'][i]['timeTaken'];
                marksData.questionIndex = result[0]['data']['studentTestItems'][i]['questionIndex'];
                data.add(marksData);
                topic = new Map<String, List<Data>>();
                topic = {
                  result[0]['data']['studentTestItems'][i]['topicName']: data
                };
                areaName[result[0]['data']['studentTestItems'][i]['areaName']] = topic;
                section[result[0]['data']['studentTestItems'][i]['sectionName']] = areaName;
              }

            }

            /// section map doesn't contains key sectionName ................
            else {
              List<Data> data = new List();
              var marksData = new Data();

              /// when isCorrect is true .........................................

              if (result[0]['data']['studentTestItems'][i]['isCorrect'] == true) {
                marksData.marks = result[0]['data']['studentTestItems'][i]['points'];
                marksData.negativeMarks = 0;
                marksData.attempts = '1';
                marksData.isCorrect = 1;
                marksData.timeTakenForCorrectAnswer= result[0]['data']['studentTestItems'][i]['timeTaken'];
                if(marksData.totalNumberOfQuestionsForCorrectAnswer==null){
                  marksData.totalNumberOfQuestionsForCorrectAnswer=0;
                }
                marksData.totalNumberOfQuestionsForCorrectAnswer=marksData.totalNumberOfQuestionsForCorrectAnswer+1;
                marksData.timeTakenFonINCorrectAnswer=0;
                marksData.totalNumberOfQuestionsForInCorrectAnswer=0;
                marksData.timeTakenForSkippedAnswer=0;
                marksData.totalNumberOfQuestionsForSkippedAnswer=0;
              }

              /// when isCorrect is false .........................................

              else if (result[0]['data']['studentTestItems'][i]['isCorrect'] == false) {
                marksData.marks = 0;
                marksData.negativeMarks = result[0]['data']['studentTestItems'][i]['negativePoints'];
                marksData.attempts = '1';
                marksData.isCorrect = 0;
                marksData.timeTakenForCorrectAnswer= 0;
                marksData.totalNumberOfQuestionsForCorrectAnswer=0;
                marksData.timeTakenFonINCorrectAnswer=result[0]['data']['studentTestItems'][i]['timeTaken'];
                if( marksData.totalNumberOfQuestionsForInCorrectAnswer==null){
                  marksData.totalNumberOfQuestionsForInCorrectAnswer=0;
                }
                marksData.totalNumberOfQuestionsForInCorrectAnswer=marksData.totalNumberOfQuestionsForInCorrectAnswer+1;
                marksData.timeTakenForSkippedAnswer=0;
                marksData.totalNumberOfQuestionsForSkippedAnswer=0;
              }

              /// when isCorrect is null .........................................

              else if (result[0]['data']['studentTestItems'][i]['isCorrect'] == null) {
                //marksData.attempts = '0';
                marksData.marks = 0;
                marksData.negativeMarks = 0;
                marksData.isCorrect = 0;
                marksData.isNotNull = 0;
                marksData.timeTakenNotNull = 0;
                marksData.timeTakenForCorrectAnswer= 0;
                marksData.totalNumberOfQuestionsForCorrectAnswer=0;
                marksData.timeTakenFonINCorrectAnswer=0;
                marksData.totalNumberOfQuestionsForInCorrectAnswer=0;
                marksData.timeTakenForSkippedAnswer=result[0]['data']['studentTestItems'][i]['timeTaken'];
                if(marksData.totalNumberOfQuestionsForSkippedAnswer==null){
                  marksData.totalNumberOfQuestionsForSkippedAnswer=0;
                }
                marksData.totalNumberOfQuestionsForSkippedAnswer=marksData.totalNumberOfQuestionsForSkippedAnswer+1;
              }

              if (result[0]['data']['studentTestItems'][i]['isCorrect'] != null) {
                marksData.isNotNull = 1;
                marksData.timeTakenNotNull = result[0]['data']['studentTestItems'][i]['timeTaken'];
              }

              marksData.timeTaken = result[0]['data']['studentTestItems'][i]['timeTaken'];
              marksData.questionIndex = result[0]['data']['studentTestItems'][i]['questionIndex'];
              data.add(marksData);
              topic = new Map<String, List<Data>>();
              topic = {
                result[0]['data']['studentTestItems'][i]['topicName']: data
              };
              area = {
                result[0]['data']['studentTestItems'][i]['areaName']: topic
              };
              section[result[0]['data']['studentTestItems'][i]
              ['sectionName']] = area;
              //section.p
            }


          }



          print("**************** Section Data" + section.toString());

          var totalmarks = 0.0;
          var negative = 0.0;
          List negativeScore = [];
          for (var i = 0; i < section.length; i++) {
            sectionName.add(section.keys.elementAt(i));
            for (var j = 0; j < section.values.elementAt(i).length; j++) {
              for (var k = 0;
              k < section.values.elementAt(i).values.elementAt(j).length;
              k++) {
                for (int x = 0;
                x <
                    section.values
                        .elementAt(i)
                        .values
                        .elementAt(j)
                        .values
                        .elementAt(k)
                        .length;
                x++) {
                  Data dataObject = section.values
                      .elementAt(i)
                      .values
                      .elementAt(j)
                      .values
                      .elementAt(k)[x];
                  totalmarks = totalmarks + dataObject.marks;
                  negative = negative + dataObject.negativeMarks;
                }
                negativeScore.add(negative);
                negative = 0;
              }
            }
          }
          getListofAreas("");
          getTimeManagementData();
        }

        else {
//          if (result[0] != 'No Internet Connection') {
//            showErrorMessage(result[0]);
//          }
        }
      });
    });
  }
  ///Function to get summary tab values
  void getListofAreas(String val) {
    setState(() {
      _isExpanded = null;

      listofmarks = [];
      listoffinalmarks = [];

      listoflength = [];
      listofattempts = [];

      listofaccuracy = [];
      listoftotaltimespent = [];
      listoftotaltimespentNotNull = [];
      listofnagativemarks = [];
      listofiscorrectnotnull = [];
      listoftimespent = [];
      listoftimespentNotNull = [];
      listofiscorrect = [];
      listoftopicattempts = [];
      listofareaattempts = [];
      listoftopicaccuracy = [];
      listofareaaccuracy = [];
      listoftopicmarks = [];
      listoftopiclength = [];
      listoftotallength = [];
      listoftopictimespent = [];
      listoftopictimespentNotNull = [];
      listofareatimespent = [];
      listofareatimespentNotNull = [];
      listofareamarks = [];
      listoftotalmarks = [];
      listoftotalnegativemarks = [];
      listOfMarksDenominator = [];
      listofsectionmarks = [];
      listofsectionattempts = [];
      listofsectionaccuracy = [];
      listofsectiontimetaken = [];
      listofsectiontimetakenNotNull = [];
      for (var i = 0; i < section.length; i++) {
        var count = 0;
        if (section.keys.elementAt(i) == "Pyschometric" ||
            section.keys.elementAt(i) == "Descriptive" ||
            section.keys.elementAt(i) == "Descriptive Writing") {
          count = count + 1;
        } else {
          listofareamark = new List();
          listofareaattempt = new List();
          listofareaaccuracys = new List();
          listofareatimespents = new List();
          listofareatimespentNotNulls = new List();
          listoftopicmark = new List();
          listoftopicattempt = new List();
          listoftopicaccuracys = new List();
          listoftopictimespents = new List();
          listoftopictimespentNotNulls = new List();
          for (var j = 0; j < section.values
              .elementAt(i)
              .length; j++) {
            listoffinalmarks = new List();
            listoflength = new List();
            listofattempts = new List();
            listofaccuracy = new List();
            listoftotaltimespent = new List();
            listoftotaltimespentNotNull = new List();
            listoftotalmarks = new List();
            listoftotalnegativemarks = new List();

            for (var k = 0; k < section.values
                .elementAt(i)
                .values
                .elementAt(j)
                .length; k++) {
              listofmarks = new List();
              listofnagativemarks = new List();
              listofiscorrectnotnull = new List();
              listoftimespent = new List();
              listoftimespentNotNull = new List();
              listofiscorrect = new List();


              for (int x = 0; x < section.values
                  .elementAt(i)
                  .values
                  .elementAt(j)
                  .values
                  .elementAt(k)
                  .length; x++) {
                Data dataObject = section.values
                    .elementAt(i)
                    .values
                    .elementAt(j)
                    .values
                    .elementAt(k)[x];
                listofmarks.add(dataObject.marks);
                listofnagativemarks.add(dataObject.negativeMarks);
                listofiscorrectnotnull.add(dataObject.isNotNull);
                listoftimespent.add(dataObject.timeTaken);
                listoftimespentNotNull.add(dataObject.timeTakenNotNull);
                listofiscorrect.add(dataObject.isCorrect);
              }
              print("List of time taken is not null" +
                  listoftimespentNotNull.toString());
              //print("topic"+listoftopictimetakenforcorrectanswer.toString());
              listoflength.add(section.values
                  .elementAt(i)
                  .values
                  .elementAt(j)
                  .values
                  .elementAt(k)
                  .length);
              listoftotalmarks.add(listofmarks.fold(0, (p, c) => p + c));
              listoftotalnegativemarks.add(
                  listofnagativemarks.fold(0, (p, c) => p + c));
              listoffinalmarks.add(
                  listoftotalmarks[k] - listoftotalnegativemarks[k]);
              listoftotaltimespent.add(
                  listoftimespent.fold(0, (p, c) => p + c));
              listoftotaltimespentNotNull.add(
                  listoftimespentNotNull.fold(0, (p, c) => p + c));
              listofattempts.add(
                  listofiscorrectnotnull.fold(0, (p, c) => p + c));
              listofaccuracy.add(listofiscorrect.fold(0, (p, c) => p + c));
            }
            print("List of total time taken is not null" +
                listoftotaltimespentNotNull.toString());
            listoftopicattempt.add(listofattempts);
            listofareaattempt.add(listofattempts.fold(0, (p, c) => p + c));
            listoftopicaccuracys.add(listofaccuracy);
            listofareaaccuracys.add(listofaccuracy.fold(0, (p, c) => p + c));
            listoftopicmark.add(listoffinalmarks);
            //print("Listoftopicmark"+listoftopicmark.toString());
            listofareamark.add(listoffinalmarks.fold(0, (p, c) => p + c));
            listoftopiclength.add(listoflength);
            listoftotallength.add(listoflength.fold(0, (p, c) => p + c));
            listoftopictimespents.add(listoftotaltimespent);
            listoftopictimespentNotNulls.add(listoftotaltimespentNotNull);
            listofareatimespents.add(
                listoftotaltimespent.fold(0, (p, c) => p + c));
            listofareatimespentNotNulls.add(
                listoftotaltimespentNotNull.fold(0, (p, c) => p + c));
          }

          listoftopicmarks.add(listoftopicmark);
          listoftopicattempts.add(listoftopicattempt);
          listoftopicaccuracy.add(listoftopicaccuracys);
          listoftopictimespent.add(listoftopictimespents);
          listoftopictimespentNotNull.add(listoftopictimespentNotNulls);
          listofareamarks.add(listofareamark);
          listofareaattempts.add(listofareaattempt);
          listofareaaccuracy.add(listofareaaccuracys);
          listofareatimespent.add(listofareatimespents);
          listofareatimespentNotNull.add(listofareatimespentNotNulls);
          listofsectionmarks.add(listofareamark.fold(0, (p, c) => p + c));
          listofsectionattempts.add(listofareaattempt.fold(0, (p, c) => p + c));
          listofsectionaccuracy.add(
              listofareaaccuracys.fold(0, (p, c) => p + c));
          listofsectiontimetaken.add(
              listofareatimespents.fold(0, (p, c) => p + c));
          listofsectiontimetakenNotNull.add(
              listofareatimespentNotNulls.fold(0, (p, c) => p + c));
          print("List of area marks" + listofareamarks.toString());
          print("List of topic time taken is not null" +
              listoftopictimespentNotNull.toString());
//        print("section wise timetaken for correct answ"+listofsectiontimetakenforcorrectanswer.toString());
//        print("area wise timetaken for correct answ"+listofareatimetakenforcorrectanswer.toString());
//        print("topic wise timetaken for correct answ"+listoftopictimetakenforcorrectanswer.toString());

        }
      }
      overallmarks = listofsectionmarks.fold(0, (p, c) => p + c);
      overallattempts = listofsectionattempts.fold(0, (p, c) => p + c);
      overallaccuracy = listofsectionaccuracy.fold(0, (p, c) => p + c);
      overalltimetaken = listofsectiontimetaken.fold(0, (p, c) => p + c);
      overalltimetakenNotNull = listofsectiontimetakenNotNull.fold(0, (p, c) => p + c);
      print("Overall attempts"+overallattempts.toString());
      print("Overall accuracy"+overallaccuracy.toString());



      listofareanames = [];
      listoftopicnames = [];
      listofsectionname = [];

      for (var i = 0; i < section.values.length; i++) {
        //print("Sectionnnnnnnnnnnnn " + section.keys.elementAt(i).toString());
        var count = 0;
        if (section.keys.elementAt(i) == "Pyschometric" || section.keys.elementAt(i)=="Descriptive" || section.keys.elementAt(i) == "Descriptive Writing") {
          count = count + 1;
        }
        else{
          listofsectionname.add(section.keys.elementAt(i));
          listofarea = new List();
          listoftopics = new List();
          for (var j = 0; j < section.values.toList()[i].length; j++) {
            listofarea.add(section.values.elementAt(i).keys.elementAt(j));
            listoftopic = new List();
            for (var k = 0; k < section.values.elementAt(i).values.elementAt(j).keys.toList().length; k++) {
              listoftopic.add(section.values.elementAt(i).values.elementAt(j).keys.toList()[k].toString());
            }
            listoftopics.add(listoftopic);
          }
          listoftopicnames.add(listoftopics);
          listofareanames.add(listofarea);
//        print("listofsectionnammmmmmmmmmmmmmm" + listofsectionname.toString());
//        print("listofareanamesssssssssssssss" + listofareanames.toString());
//        print("listoftopicnameeeeeeeeeeee" + listoftopicnames.toString());
        }
      }
      expandList(listofareanames.length + 1);
//      print("listof         index" + _isExpanded.toString());
//      print("listofareanames" + listofareanames.toString());
      print("listoftopicnames" + listoftopicnames.toString());
      print("Section Names" + listofsectionname.toString());
    });
  }
  ///Function to get Time management values
  void getTimeManagementData(){
    setState(() {
      listoftimetakenforcorrectanswer = [];
      listoftotaltimetakenforcorrectanswer = [];
      listoftotaltopictimetakenforcorrectanswer = [];
      listoftotalareatimetakenforcorrectanswer = [];
      listofsectiontimetakenforcorrectanswer = [];
      listoftimetakenforincorrectanswer = [];
      listoftotaltimetakenforincorrectanswer = [];
      listoftotaltopictimetakenforincorrectanswer = [];
      listoftotalareatimetakenforincorrectanswer = [];
      listofsectiontimetakenforincorrectanswer = [];
      listoftimetakenforskippedanswer = [];
      listoftotaltimetakenforskippedanswer = [];
      listoftotaltopictimetakenforskippedanswer = [];
      listoftotalareatimetakenforskippedanswer = [];
      listofsectiontimetakenforskippedanswer = [];

      listoftotalnumberofquestionsforcorrectanswer = [];
      listoftotaltotalnumberofquestionsforcorrectanswer = [];
      listoftotaltopictotalnumberofquestionsforcorrectanswer = [];
      listoftotalareatotalnumberofquestionsforcorrectanswer = [];
      listofsectiontotalnumberofquestionsforcorrectanswer = [];
      listoftotalnumberofquestionsforincorrectanswer = [];
      listoftotaltotalnumberofquestionsforincorrectanswer = [];
      listoftotaltopictotalnumberofquestionsforincorrectanswer = [];
      listoftotalareatotalnumberofquestionsforincorrectanswer = [];
      listofsectiontotalnumberofquestionsforincorrectanswer = [];
      listoftotalnumberofquestionsforskippedanswer = [];
      listoftotaltotalnumberofquestionsforskippedanswer = [];
      listoftotaltopictotalnumberofquestionsforskippedanswer = [];
      listoftotalareatotalnumberofquestionsforskippedanswer = [];
      listofsectiontotalnumberofquestionsforskippedanswer = [];

      for (var i = 0; i < section.length; i++) {
        var count = 0;
        if (section.keys.elementAt(i) == "Pyschometric" ||
            section.keys.elementAt(i) == "Descriptive" ||
            section.keys.elementAt(i) == "Descriptive Writing") {
          count = count + 1;
        } else {
          listoftopictimetakenforcorrectanswer = new List();
          listofareatimetakenforcorrectanswer = new List();
          listoftopictimetakenforincorrectanswer = new List();
          listofareatimetakenforincorrectanswer = new List();
          listoftopictimetakenforskippedanswer = new List();
          listofareatimetakenforskippedanswer = new List();

          listoftopictotalnumberofquestionsforcorrectanswer = new List();
          listofareatotalnumberofquestionsforcorrectanswer = new List();
          listoftopictotalnumberofquestionsforincorrectanswer = new List();
          listofareatotalnumberofquestionsforincorrectanswer = new List();
          listoftopictotalnumberofquestionsforskippedanswer = new List();
          listofareatotalnumberofquestionsforskippedanswer = new List();
          for (var j = 0; j < section.values
              .elementAt(i)
              .length; j++) {
            listoftotaltimetakenforcorrectanswer = new List();
            listoftotaltimetakenforincorrectanswer = new List();
            listoftotaltimetakenforskippedanswer = new List();
            listoftotaltotalnumberofquestionsforcorrectanswer = new List();
            listoftotaltotalnumberofquestionsforincorrectanswer = new List();
            listoftotaltotalnumberofquestionsforskippedanswer = new List();
            for (var k = 0; k < section.values
                .elementAt(i)
                .values
                .elementAt(j)
                .length; k++) {
              listoftimetakenforcorrectanswer = new List();
              listoftimetakenforincorrectanswer = new List();
              listoftimetakenforskippedanswer = new List();
              listoftotalnumberofquestionsforcorrectanswer = new List();
              listoftotalnumberofquestionsforincorrectanswer = new List();
              listoftotalnumberofquestionsforskippedanswer = new List();
              for (int x = 0; x < section.values
                  .elementAt(i)
                  .values
                  .elementAt(j)
                  .values
                  .elementAt(k)
                  .length; x++) {
                Data dataObject = section.values
                    .elementAt(i)
                    .values
                    .elementAt(j)
                    .values
                    .elementAt(k)[x];
                listoftimetakenforcorrectanswer.add(
                    dataObject.timeTakenForCorrectAnswer);
                listoftimetakenforincorrectanswer.add(
                    dataObject.timeTakenFonINCorrectAnswer);
                listoftimetakenforskippedanswer.add(
                    dataObject.timeTakenForSkippedAnswer);
                listoftotalnumberofquestionsforcorrectanswer.add(
                    dataObject.totalNumberOfQuestionsForCorrectAnswer);
                listoftotalnumberofquestionsforincorrectanswer.add(
                    dataObject.totalNumberOfQuestionsForInCorrectAnswer);
                listoftotalnumberofquestionsforskippedanswer.add(
                    dataObject.totalNumberOfQuestionsForSkippedAnswer);
              }
              listoftotaltimetakenforcorrectanswer.add(
                  listoftimetakenforcorrectanswer.fold(0, (p, c) => p + c));
              listoftotaltimetakenforincorrectanswer.add(
                  listoftimetakenforincorrectanswer.fold(0, (p, c) => p + c));
              listoftotaltimetakenforskippedanswer.add(
                  listoftimetakenforskippedanswer.fold(0, (p, c) => p + c));
              listoftotaltotalnumberofquestionsforcorrectanswer.add(
                  listoftotalnumberofquestionsforcorrectanswer.fold(
                      0, (p, c) => p + c));
              listoftotaltotalnumberofquestionsforincorrectanswer.add(
                  listoftotalnumberofquestionsforincorrectanswer.fold(
                      0, (p, c) => p + c));
              listoftotaltotalnumberofquestionsforskippedanswer.add(
                  listoftotalnumberofquestionsforskippedanswer.fold(
                      0, (p, c) => p + c));
            }
            listoftopictimetakenforcorrectanswer.add(
                listoftotaltimetakenforcorrectanswer);
            listofareatimetakenforcorrectanswer.add(
                listoftotaltimetakenforcorrectanswer.fold(0, (p, c) => p + c));
            listoftopictimetakenforincorrectanswer.add(
                listoftotaltimetakenforincorrectanswer);
            listofareatimetakenforincorrectanswer.add(
                listoftotaltimetakenforincorrectanswer.fold(
                    0, (p, c) => p + c));
            listoftopictimetakenforskippedanswer.add(
                listoftotaltimetakenforskippedanswer);
            listofareatimetakenforskippedanswer.add(
                listoftotaltimetakenforskippedanswer.fold(0, (p, c) => p + c));

            listoftopictotalnumberofquestionsforcorrectanswer.add(
                listoftotaltotalnumberofquestionsforcorrectanswer);
            listofareatotalnumberofquestionsforcorrectanswer.add(
                listoftotaltotalnumberofquestionsforcorrectanswer.fold(
                    0, (p, c) => p + c));
            listoftopictotalnumberofquestionsforincorrectanswer.add(
                listoftotaltotalnumberofquestionsforincorrectanswer);
            listofareatotalnumberofquestionsforincorrectanswer.add(
                listoftotaltotalnumberofquestionsforincorrectanswer.fold(
                    0, (p, c) => p + c));
            listoftopictotalnumberofquestionsforskippedanswer.add(
                listoftotaltotalnumberofquestionsforskippedanswer);
            listofareatotalnumberofquestionsforskippedanswer.add(
                listoftotaltotalnumberofquestionsforskippedanswer.fold(
                    0, (p, c) => p + c));
          }
          listoftotaltopictimetakenforcorrectanswer.add(
              listoftopictimetakenforcorrectanswer);
          listoftotalareatimetakenforcorrectanswer.add(
              listofareatimetakenforcorrectanswer);
          listofsectiontimetakenforcorrectanswer.add(
              listofareatimetakenforcorrectanswer.fold(0, (p, c) => p + c));
          listoftotaltopictimetakenforincorrectanswer.add(
              listoftopictimetakenforincorrectanswer);
          listoftotalareatimetakenforincorrectanswer.add(
              listofareatimetakenforincorrectanswer);
          listofsectiontimetakenforincorrectanswer.add(
              listofareatimetakenforincorrectanswer.fold(0, (p, c) => p + c));
          listoftotaltopictimetakenforskippedanswer.add(
              listoftopictimetakenforskippedanswer);
          listoftotalareatimetakenforskippedanswer.add(
              listofareatimetakenforskippedanswer);
          listofsectiontimetakenforskippedanswer.add(
              listofareatimetakenforskippedanswer.fold(0, (p, c) => p + c));

          listoftotaltopictotalnumberofquestionsforcorrectanswer.add(
              listoftopictotalnumberofquestionsforcorrectanswer);
          listoftotalareatotalnumberofquestionsforcorrectanswer.add(
              listofareatotalnumberofquestionsforcorrectanswer);
          listofsectiontotalnumberofquestionsforcorrectanswer.add(
              listofareatotalnumberofquestionsforcorrectanswer.fold(
                  0, (p, c) => p + c));
          listoftotaltopictotalnumberofquestionsforincorrectanswer.add(
              listoftopictotalnumberofquestionsforincorrectanswer);
          listoftotalareatotalnumberofquestionsforincorrectanswer.add(
              listofareatotalnumberofquestionsforincorrectanswer);
          listofsectiontotalnumberofquestionsforincorrectanswer.add(
              listofareatotalnumberofquestionsforincorrectanswer.fold(
                  0, (p, c) => p + c));
          listoftotaltopictotalnumberofquestionsforskippedanswer.add(
              listoftopictotalnumberofquestionsforskippedanswer);
          listoftotalareatotalnumberofquestionsforskippedanswer.add(
              listofareatotalnumberofquestionsforskippedanswer);
          listofsectiontotalnumberofquestionsforskippedanswer.add(
              listofareatotalnumberofquestionsforskippedanswer.fold(
                  0, (p, c) => p + c));
        }
      }
    });
    print("topic time taken for correct answer" + listoftotaltopictimetakenforcorrectanswer.toString());
    print("area time taken for correct answer" + listoftotalareatimetakenforcorrectanswer.toString());
    print("section time taken for correct answer" + listofsectiontimetakenforcorrectanswer.toString());
    print("topic time taken for incorrect answer" + listoftotaltopictimetakenforincorrectanswer.toString());
    print("area time taken for incorrect answer" + listoftotalareatimetakenforincorrectanswer.toString());
    print("section time taken for incorrect answer" + listofsectiontimetakenforincorrectanswer.toString());
    print("topic time taken for skipped answer" + listoftotaltopictimetakenforskippedanswer.toString());
    print("area time taken for skipped answer" + listoftotalareatimetakenforskippedanswer.toString());
    print("section time taken for skipped answer" + listofsectiontimetakenforskippedanswer.toString());

    print("topic totalnumberofquestions for correct answer" + listoftotaltopictotalnumberofquestionsforcorrectanswer.toString());
    print("area totalnumberofquestions for correct answer" + listoftotalareatotalnumberofquestionsforcorrectanswer.toString());
    print("section totalnumberofquestions for correct answer" + listofsectiontotalnumberofquestionsforcorrectanswer.toString());
    print("topic totalnumberofquestions for incorrect answer" + listoftotaltopictotalnumberofquestionsforincorrectanswer.toString());
    print("area totalnumberofquestions for incorrect answer" + listoftotalareatotalnumberofquestionsforincorrectanswer.toString());
    print("section totalnumberofquestions for incorrect answer" + listofsectiontotalnumberofquestionsforincorrectanswer.toString());
    print("topic totalnumberofquestions for skipped answer" + listoftotaltopictotalnumberofquestionsforskippedanswer.toString());
    print("area totalnumberofquestions for skipped answer" + listoftotalareatotalnumberofquestionsforskippedanswer.toString());
    print("section totalnumberofquestions for skipped answer" + listofsectiontotalnumberofquestionsforskippedanswer.toString());
    overalltimetakenforcorrectanswer = listofsectiontimetakenforcorrectanswer.reduce((a, b) => a + b);
    overalltimetakenforincorrectanswer = listofsectiontimetakenforincorrectanswer.reduce((a, b) => a + b);
    overalltimetakenforskippedanswer = listofsectiontimetakenforskippedanswer.reduce((a, b) => a + b);
    overalltotalnumberfquestionsforcorrectanswer = listofsectiontotalnumberofquestionsforcorrectanswer.reduce((a, b) => a + b);
    overalltotalnumberofquestionsforincorrectanswer = listofsectiontotalnumberofquestionsforincorrectanswer.reduce((a, b) => a + b);
    overalltotalnumberofquestionsforskippedanswer = listofsectiontotalnumberofquestionsforskippedanswer.reduce((a, b) => a + b);
  }

  @override
  Widget build(BuildContext context) {
    double defaultScreenWidth = 360.0;
    double defaultScreenHeight = 720.0;
    var _mediaQueryData = MediaQuery.of(context);
    screenWidthTotal = _mediaQueryData.size.width;
    screenHeightTotal = _mediaQueryData.size.height;
    const EdgeInsets kExpandedEdgeInsets = EdgeInsets.symmetric(
        vertical: _kPanelHeaderExpandedHeight - _kPanelHeaderCollapsedHeight);
    return Scaffold(
      body: Container(
        child: Column(
          children: <Widget>[
            /// section dropdown filter .........
            filterFlag == 1?
            Container(
              //  height: 20 / defaultScreenHeight * screenHeightTotal,
              //   width: 320/defaultScreenWidth*screenWidthTotal,
              margin: EdgeInsets.only(
                  left: 20 / defaultScreenWidth * screenWidthTotal,
                  top: 30 / defaultScreenHeight * screenHeightTotal,
                  right: 20 / defaultScreenWidth * (screenWidthTotal)),
//              decoration: BoxDecoration(
//                //color: Color.fromARGB(255, 255, 255, 255),
//                border: Border.all(
//                  color: NeutralColors.ice_blue,
//                  width: 1,
//                ),
//                borderRadius: BorderRadius.all(Radius.circular(5)),
//              ),
              child: Container(
                child: drop.DropDownField(
                  getImmediateSuggestions: true,
                  textFieldConfiguration: drop.TextFieldConfiguration(
                    controller: _stateOfVenues,
                    label:
                        selectedSection != "" ? selectedSection : selectedlabel,
                  ),
                  suggestionsBoxDecoration: drop.SuggestionsBoxDecoration(
                      //dropDownHeight: 140,
                      borderRadius: new BorderRadius.circular(5.0),
                      color: Colors.white),
                  suggestionsCallback: (pattern) {
                    // print("%%%%%suggestionsCallback%%%%%%%%%%%%%%%%%%%%%%%getSectionDropDownItems"+getSectionDropDownItems.toString());
                    return getSectionDropDownItems;
                  },
                  itemBuilder: (context, suggestion) {
                    return ListTile(
                      dense: true,
                      contentPadding:
                          EdgeInsets.only(left: 10 / 360 * screenHeightTotal),
                      title: Text(
                        suggestion,
                        style: TextStyle(
                          fontFamily: "IBMPlexSans",
                          fontWeight: FontWeight.w400,
                          fontSize:
                              (14 / defaultScreenHeight) * screenHeightTotal,
                          color: NeutralColors.bluey_grey,
                          textBaseline: TextBaseline.alphabetic,
                          letterSpacing: 0.0,
                          inherit: false,
                        ),
                        overflow: TextOverflow.ellipsis,
                      ),
                    );
                  },
                  hideOnLoading: true,
                  debounceDuration: Duration(milliseconds: 100),
                  transitionBuilder: (context, suggestionsBox, controller) {
                    return suggestionsBox;
                  },
                  onSuggestionSelected: (suggestion) {
                    _stateOfVenues.text = suggestion;
                    setState(() {
                      selectedSection = suggestion;
                    });
                    //summaryObj.getListofAreas(selectedSection);
                    saveSection(selectedSection);
                    getListofAreas(selectedSection);
                    getSectionWiseQuestions(selectedSection);
                  },
                  //onSaved: (value) => {print("something")},
                ),
              ),
            ):Container(),
            Container(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Expanded(
                    flex: 10,
                    child: Container(
                      height: 36 / defaultScreenHeight * screenHeightTotal,
                      margin: EdgeInsets.only(
                          top: 15 / defaultScreenHeight * screenHeightTotal,
                          left: 28 / defaultScreenHeight * screenHeightTotal),
                      child: new TabBar(
                        controller: _tabController,
                        onTap: (value) {
                          print("\\\\" + value.toString());
                          setState(() {
                            filterFlag = value;
                          });
                        },
                        indicatorColor: NeutralColors.dark_navy_blue,
                        labelColor: NeutralColors.dark_navy_blue,
                        unselectedLabelColor: NeutralColors.blue_grey,
                        indicatorSize: TabBarIndicatorSize.tab,
                        tabs: tabList,
                        indicatorPadding: EdgeInsets.only(
                            left: 5.0 / defaultScreenWidth * screenWidthTotal,
                            right: 5.0 / defaultScreenWidth * screenWidthTotal),
                        isScrollable: true,
                        unselectedLabelStyle: TextStyle(
                          fontSize: 14 / defaultScreenWidth * screenWidthTotal,
                          fontFamily: "IBMPlexSans",
                          fontWeight: FontWeight.w400,
                        ),
                        labelStyle: TextStyle(
                          fontSize: 14 / defaultScreenWidth * screenWidthTotal,
                          fontFamily: "IBMPlexSans",
                          fontWeight: FontWeight.w500,
                        ),
                      ),
                    ),
                  ),
                  filterFlag == 1
                      ? Expanded(
                          flex: 1,
                          child: Container(
                            margin: EdgeInsets.only(
                              left: 0 / defaultScreenWidth * screenWidthTotal,
                              top: 15 / defaultScreenHeight * screenHeightTotal,
                            ),
                            child: GestureDetector(
                              onTap: () {
                                _tabController.index == 1
                                    ? setState(() {
                                        isFilterClicked = !isFilterClicked;
                                        // AppRoutes.push(context, ExpandableTextText(getQuestions));
                                      })
                                    : setState(() {});
                              },
                              child: ChannelAssets.filterIconAnalytics,
                            ),
                          ),
                        )
                      : Container(),
                ],
              ),
            ),
            /// tab bar for summary,time management and detailed part
            listofsectionname.length == 0?
            new Container(
                margin: EdgeInsets.only(
                    top: (100 / 720) * screenHeightTotal),
                child:
                Center(child: CircularProgressIndicator())):
            Expanded(
              child: new TabBarView(
                physics: NeverScrollableScrollPhysics(),
                controller: _tabController,
                children: <Widget>[
                  SummaryTable(
                    widget.testTemplate,
                    widget.selectedLearningObject,
                      widget.testTemplateForXat,
                      listofsectionname,
                      listofareanames,
                      listoftopicnames,
                      overallmarks,
                      overallattempts,
                      overallaccuracy,
                      overalltimetaken,
                      overalltimetakenNotNull,
                      listofsectionmarks,
                      listofsectionattempts,
                      listofsectionaccuracy,
                      listofsectiontimetaken,
                      listofsectiontimetakenNotNull,
                      listofareamarks,
                      listoftotallength,
                      listofareaattempts,
                      listofareaaccuracy,
                      listofareatimespent,
                      listofareatimespentNotNull,
                      listoftopicmarks,
                      listoftopiclength,
                      listoftopicattempts,
                      listoftopicaccuracy,
                      listoftopictimespent,
                      listoftopictimespentNotNull),
                  
                  Container(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Container(
                          margin: EdgeInsets.only(top: 20),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Container(
                                margin: EdgeInsets.only(left: 20, right: 10),
                                child: isFilterClicked
                                    ? Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: <Widget>[
                                          Expanded(
                                            child: Container(
                                              height: 40 /
                                                  defaultScreenHeight *
                                                  screenHeightTotal,
                                              child: DropDownFormField(
                                                getImmediateSuggestions: true,
                                                textFieldConfiguration:
                                                    TextFieldConfiguration(
                                                  controller: _sample1,
                                                  label:
                                                      selectedAreaValue != null
                                                          ? selectedAreaValue
                                                          : _areaLabelValue,
                                                ),
                                                //  suggestionsBoxController:sugg,
                                                suggestionsBoxDecoration:
                                                    SuggestionsBoxDecoration(
                                                        color: Colors.white),
                                                suggestionsCallback: (pattern) {
                                                  return getAreaDropDownItems;
                                                },
                                                itemBuilder:
                                                    (context, suggestion) {
                                                  //print("suggestinmon"+suggestion.toString());
                                                  return Container(
                                                    //margin: EdgeInsets.only(left: 05/360*screenWidthTotal),
                                                    color: suggestion ==
                                                            "All Areas"
                                                        ? NeutralColors.ice_blue
                                                        : NeutralColors
                                                            .pureWhite,
                                                    child: Padding(
                                                      padding: EdgeInsets.only(
                                                          top: (10 /
                                                                  defaultScreenHeight) *
                                                              screenHeightTotal,
                                                          bottom: (11 /
                                                                  defaultScreenHeight) *
                                                              screenHeightTotal,
                                                          left: (05 /
                                                                  defaultScreenWidth) *
                                                              screenWidthTotal),
                                                      child: Text(
                                                        suggestion,
                                                        style: TextStyle(
                                                          fontFamily:
                                                              "IBMPlexSans",
                                                          fontWeight:
                                                              FontWeight.w500,
                                                          fontSize: (12 /
                                                                  defaultScreenWidth) *
                                                              screenWidthTotal,
                                                          color: suggestion ==
                                                                  "All Areas"
                                                              ? NeutralColors
                                                                  .purpley
                                                              : NeutralColors
                                                                  .black,
                                                          textBaseline:
                                                              TextBaseline
                                                                  .alphabetic,
                                                          letterSpacing: 0.0,
                                                          inherit: false,
                                                        ),
                                                      ),
                                                    ),
                                                  );
                                                },
                                                hideOnLoading: true,
                                                debounceDuration:
                                                    Duration(milliseconds: 100),
                                                transitionBuilder: (context,
                                                    suggestionsBox,
                                                    controller) {
                                                  return suggestionsBox;
                                                },
                                                onSuggestionSelected:
                                                    (suggestion) {
                                                  //  print('Sangeethaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa'+suggestion);
                                                  _sample1.text = suggestion;
                                                  //print("=======>>>>"+_sample1.text);
                                                  setState(() {
                                                    selectedAreaValue =
                                                        suggestion;
                                                    if (selectedTopicValue !=
                                                        null) {
                                                      selectedTopicValue =
                                                          "All Topics";
                                                    }
                                                  });
                                                  loadDropdownItemsForTopics(
                                                      selectedAreaValue);
                                                  loadAreaWiseQuestions(
                                                      selectedAreaValue);
                                                },
                                                onSaved: (value) =>
                                                    {print("something")},
                                              ),
                                            ),
                                          ),
                                          Expanded(
                                            child: Container(
                                              height: 40 /
                                                  defaultScreenHeight *
                                                  screenHeightTotal,
                                              margin: EdgeInsets.only(
                                                  left: 5 /
                                                      360 *
                                                      screenWidthTotal),
                                              child: topics.DropDownFormField(
                                                //  selected_month: selectedMonth,
                                                getImmediateSuggestions: true,
                                                // autoFlipDirection: true,
                                                textFieldConfiguration: topics
                                                    .TextFieldConfiguration(
                                                  controller: _sample2,
                                                  label:
                                                      selectedTopicValue != null
                                                          ? selectedTopicValue
                                                              .toUpperCase()
                                                          : _topicLabelValue,
                                                ),
                                                //  suggestionsBoxController:sugg,
                                                suggestionsBoxDecoration: topics
                                                    .SuggestionsBoxDecoration(
                                                        dropDownHeight: 130 /
                                                            defaultScreenHeight *
                                                            screenHeightTotal,
                                                        color: Colors.white),
                                                suggestionsCallback: (pattern) {
                                                  return getTopicsDropDownItems;
                                                },
                                                itemBuilder:
                                                    (context, suggestion) {
                                                  //  print("suggestinmon"+suggestion.toString());
                                                  return Container(
                                                    color: suggestion ==
                                                            "All Topics"
                                                        ? NeutralColors.ice_blue
                                                        : NeutralColors
                                                            .pureWhite,
                                                    child: Padding(
                                                      padding: EdgeInsets.only(
                                                          top: (10 /
                                                                  defaultScreenHeight) *
                                                              screenHeightTotal,
                                                          bottom: (11 /
                                                                  defaultScreenHeight) *
                                                              screenHeightTotal,
                                                          left: (05 /
                                                                  defaultScreenWidth) *
                                                              screenWidthTotal),
                                                      child: Text(
                                                        suggestion,
                                                        style: TextStyle(
                                                          fontFamily:
                                                              "IBMPlexSans",
                                                          fontWeight:
                                                              FontWeight.w500,
                                                          fontSize: (12 /
                                                                  defaultScreenWidth) *
                                                              screenWidthTotal,
                                                          color: suggestion ==
                                                                  "All Topics"
                                                              ? NeutralColors
                                                                  .purpley
                                                              : NeutralColors
                                                                  .black,
                                                          textBaseline:
                                                              TextBaseline
                                                                  .alphabetic,
                                                          letterSpacing: 0.0,
                                                          inherit: false,
                                                        ),
                                                      ),
                                                    ),
                                                  );
                                                },
                                                hideOnLoading: true,
                                                debounceDuration:
                                                    Duration(milliseconds: 100),
                                                transitionBuilder: (context,
                                                    suggestionsBox,
                                                    controller) {
                                                  return suggestionsBox;
                                                },
                                                onSuggestionSelected:
                                                    (suggestion) {
                                                  _sample2.text = suggestion;
                                                  setState(() {
                                                    selectedTopicValue =
                                                        suggestion;
                                                  });
                                                  loadTopicwiseQuestions(
                                                      selectedTopicValue);
                                                },
                                              ),
                                            ),
                                          ),
                                          Expanded(
                                            child: Container(
                                              height: 40 /
                                                  defaultScreenHeight *
                                                  screenHeightTotal,
                                              margin: EdgeInsets.only(
                                                  left: 5 /
                                                      360 *
                                                      screenWidthTotal),
                                              child: item.DropDownFormField(
                                                getImmediateSuggestions: true,
                                                textFieldConfiguration:
                                                    item.TextFieldConfiguration(
                                                  controller: _sample3,
                                                  label: selectedItemTypeValue !=
                                                          null
                                                      ? selectedItemTypeValue
                                                      : _itemTypeLabelValue,
                                                ),
                                                //  suggestionsBoxController:sugg,
                                                suggestionsBoxDecoration: item
                                                    .SuggestionsBoxDecoration(
                                                        dropDownHeight: 130 /
                                                            defaultScreenHeight *
                                                            screenHeightTotal,
                                                        color: Colors.white),
                                                suggestionsCallback: (pattern) {
                                                  return getItemTypeDropdownItems;
                                                },
                                                itemBuilder:
                                                    (context, suggestion) {
                                                  //print("suggestinmon"+suggestion.toString());
                                                  return Container(
                                                    color: suggestion ==
                                                            "All Items"
                                                        ? NeutralColors.ice_blue
                                                        : NeutralColors
                                                            .pureWhite,
                                                    child: Padding(
                                                      padding: EdgeInsets.only(
                                                          top: (10 /
                                                                  defaultScreenHeight) *
                                                              screenHeightTotal,
                                                          bottom: (11 /
                                                                  defaultScreenHeight) *
                                                              screenHeightTotal,
                                                          left: (05 /
                                                                  defaultScreenWidth) *
                                                              screenWidthTotal),
                                                      child: Text(
                                                        suggestion,
                                                        style: TextStyle(
                                                          fontFamily:
                                                              "IBMPlexSans",
                                                          fontWeight:
                                                              FontWeight.w500,
                                                          fontSize: (12 /
                                                                  defaultScreenWidth) *
                                                              screenWidthTotal,
                                                          color: suggestion ==
                                                                  "All Items"
                                                              ? NeutralColors
                                                                  .purpley
                                                              : NeutralColors
                                                                  .black,
                                                          textBaseline:
                                                              TextBaseline
                                                                  .alphabetic,
                                                          letterSpacing: 0.0,
                                                          inherit: false,
                                                        ),
                                                      ),
                                                    ),
                                                  );
                                                },
                                                hideOnLoading: true,
                                                debounceDuration:
                                                    Duration(milliseconds: 100),
                                                transitionBuilder: (context,
                                                    suggestionsBox,
                                                    controller) {
                                                  return suggestionsBox;
                                                },
                                                onSuggestionSelected:
                                                    (suggestion) {
                                                  _sample3.text = suggestion
                                                      .toString()
                                                      .toUpperCase();
                                                  //print("=======>>>>"+_sample3.text);
                                                  setState(() {
                                                    selectedItemTypeValue =
                                                        suggestion;
                                                  });
                                                  loadItemWiseQuestions(
                                                      selectedItemTypeValue);
                                                  print('Sangeethaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa'+selectedItemTypeValue);

                                                      },
                                              ),
                                            ),
                                          ),
                                        ],
                                      )
                                    : Container(),
                              ),
                              isFilterClicked ? Divider() : Container(),
                            ],
                          ),
                        ),
                        Container(
                          // height:(20/defaultScreenHeight) * screenHeightTotal,
                          margin: EdgeInsets.only(
                              top:
                                  (0 / defaultScreenHeight) * screenHeightTotal,
                              bottom:
                                  (6 / defaultScreenHeight) * screenHeightTotal,
                              left:
                                  (20 / defaultScreenWidth) * screenWidthTotal,
                              right:
                                  (20 / defaultScreenWidth) * screenWidthTotal),
                          width: double.infinity,
                          child: Text(
                            AnalyticsLabels.questionsLabel,
                            style: TextStyle(
                              fontSize:
                                  (14 / defaultScreenWidth) * screenWidthTotal,
                              color: NeutralColors.dark_navy_blue,
                              fontFamily: "IBMPlexSansBold",
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                        Divider(),
                        Expanded(
                          child: Center(
                            child: isLoading
                                ? Center(
                                    child: CircularProgressIndicator(),
                                  )
                                : Container(
                                    child: getQuestions.length > 0
                                        ? ListView.separated(
                                          shrinkWrap: true,
                                            itemCount: getQuestions.length,
                                            itemBuilder: (context, index) {
                                              return GestureDetector(
                                                onTap: () {
                                                  setState(() {
                                                    currentSelectedIndex =
                                                        index;
                                                    isExpanded = !isExpanded;
                                                    if (currentSelectedIndex ==
                                                        index) {
                                                      if (isExpanded) {
                                                        iconStatus = true;
                                                      } else {
                                                        iconStatus = false;
                                                      }
                                                    } else {
                                                      iconStatus = false;
                                                    }
                                                    if (currentSelectedIndex ==
                                                        index) {
                                                      flagValue[index] =
                                                          flagValue.elementAt(
                                                                      index) ==
                                                                  true
                                                              ? false
                                                              : true;
                                                      _ITEM_HEIGHT = 50.0;
                                                    } else {
                                                      flagValue.removeLast();
                                                    }
                                                  });
                                                },
                                                child: Column(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment.start,
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.start,
                                                  children: <Widget>[
                                                    Container(
                                                      child: AnimatedContainer(
                                                        margin: flagValue
                                                                .elementAt(
                                                                    index)
                                                            ? kExpandedEdgeInsets
                                                            : EdgeInsets.zero,
                                                        duration:
                                                            const Duration(
                                                                milliseconds:
                                                                    120),
                                                        child: ConstrainedBox(
                                                          constraints:
                                                              const BoxConstraints(
                                                                  minHeight:
                                                                      _kPanelHeaderCollapsedHeight),
                                                          child: Container(
                                                            margin: EdgeInsets.only(
                                                                top: (15 /
                                                                        defaultScreenHeight) *
                                                                    screenHeightTotal,
                                                                bottom: (5 /
                                                                        defaultScreenHeight) *
                                                                    screenHeightTotal,
                                                                left: (20 /
                                                                        defaultScreenWidth) *
                                                                    screenWidthTotal,
                                                                right: (20 /
                                                                        defaultScreenWidth) *
                                                                    screenWidthTotal),
                                                            child: Column(
                                                              children: <
                                                                  Widget>[
                                                                Container(
                                                                  //height: _ITEM_HEIGHT,
                                                                  child: Row(
                                                                    mainAxisAlignment:
                                                                        MainAxisAlignment
                                                                            .start,
                                                                    crossAxisAlignment:
                                                                        CrossAxisAlignment
                                                                            .start,
                                                                    children: <
                                                                        Widget>[
                                                                      // Container(
                                                                      //   //  color: Colors.blue,
                                                                      //   alignment:
                                                                      //       Alignment.topRight,
                                                                      //   child:
                                                                      //       Text(
                                                                      //     '${index + 1}.',
                                                                      //     style:
                                                                      //         TextStyle(
                                                                      //       fontSize:
                                                                      //           (14 / defaultScreenWidth) * screenWidthTotal,
                                                                      //       color:
                                                                      //           NeutralColors.dark_navy_blue,
                                                                      //       fontFamily:
                                                                      //           "IBMPlexSans",
                                                                      //       fontWeight:
                                                                      //           FontWeight.w400,
                                                                      //     ),
                                                                      //   ),
                                                                      // ),
                                                                      Expanded(
                                                                          child: getQuestions[index].contains("src") && flagValue.elementAt(index)
                                                                              ? Html(
                                                                                  customTextAlign: (elem) {
                                                                                    return TextAlign.start;
                                                                                  },
                                                                                  data: getQuestions[index],
                                                                                  defaultTextStyle: TextStyle(
                                                                                    color: Color.fromARGB(255, 0, 3, 44),
                                                                                    fontSize: 14 / 360 * screenWidthTotal,
                                                                                    fontFamily: "IBMPlexSans",
                                                                                    fontWeight: FontWeight.w500,
                                                                                  ),
                                                                                  customTextStyle: (node, textStyle) {
                                                                                    return TextStyle(
                                                                                      color: Color.fromARGB(255, 0, 3, 44),
                                                                                      fontSize: 14 / 360 * screenWidthTotal,
                                                                                      fontFamily: "IBMPlexSans",
                                                                                      fontWeight: FontWeight.w500,
                                                                                    );
                                                                                  },
                                                                                  imageProperties: ImageProperties(
                                                                                    fit: BoxFit.contain,
                                                                                    matchTextDirection: false,
                                                                                  ))
                                                                              : getQuestions[index].contains("src")
                                                                                  ? Text(
                                                                                      "...",
                                                                                      overflow: flagValue.elementAt(index) ? TextOverflow.visible : TextOverflow.ellipsis,
                                                                                      style: TextStyle(
                                                                                        fontSize: (14 / defaultScreenWidth) * screenWidthTotal,
                                                                                        color: NeutralColors.dark_navy_blue,
                                                                                        fontFamily: "IBMPlexSans",
                                                                                        fontWeight: FontWeight.w400,
                                                                                      ),
                                                                                    )
                                                                                  : Container(
                                                                                      child: Text(
                                                                                      _parseHtmlString(getQuestions[index]),
                                                                                      // html2md.convert(getQuestions[index]),
                                                                                      overflow: flagValue.elementAt(index) ? TextOverflow.visible : TextOverflow.ellipsis,
                                                                                      style: TextStyle(
                                                                                        fontSize: (14 / defaultScreenWidth) * screenWidthTotal,
                                                                                        color: NeutralColors.dark_navy_blue,
                                                                                        fontFamily: "IBMPlexSans",
                                                                                        fontWeight: FontWeight.w400,
                                                                                      ),
                                                                                    ))),
                                                                      Center(
                                                                        child:
                                                                            Container(
                                                                          //  color: Colors.yellow,
                                                                          width: 15 /
                                                                              defaultScreenWidth *
                                                                              screenWidthTotal,
                                                                          child: Icon(flagValue.elementAt(index)
                                                                              ? Icons.keyboard_arrow_up
                                                                              : Icons.keyboard_arrow_down),
                                                                        ),
                                                                      ),
                                                                    ],
                                                                  ),
                                                                ),
                                                                flagValue.elementAt(
                                                                        index)
                                                                    ? Column(
                                                                        children: <
                                                                            Widget>[
                                                                          Container(
                                                                            decoration:
                                                                                BoxDecoration(
                                                                              color: NeutralColors.ice_blue,
                                                                              borderRadius: BorderRadius.all(Radius.circular(5)),
                                                                            ),
                                                                            margin:
                                                                                EdgeInsets.only(top: (15 / defaultScreenHeight) * screenHeightTotal),
                                                                            child:
                                                                                Padding(
                                                                              padding: EdgeInsets.only(top: (10 / defaultScreenHeight) * screenHeightTotal, bottom: (10 / defaultScreenHeight) * screenHeightTotal, left: (10 / defaultScreenWidth) * screenWidthTotal, right: (10 / defaultScreenWidth) * screenWidthTotal),
                                                                              child: Row(
                                                                                mainAxisAlignment: MainAxisAlignment.spaceAround,
                                                                                crossAxisAlignment: CrossAxisAlignment.start,
                                                                                children: <Widget>[
                                                                                  Column(
                                                                                    children: <Widget>[
                                                                                      Text(
                                                                                        "Status",
                                                                                        style: TextStyle(
                                                                                          color: Color.fromARGB(255, 153, 154, 171),
                                                                                          fontSize: 12,
                                                                                          fontFamily: "IBMPlexSans",
                                                                                          fontWeight: FontWeight.w500,
                                                                                        ),
                                                                                        textAlign: TextAlign.center,
                                                                                      ),
                                                                                      Container(
                                                                                        padding: EdgeInsets.only(top: (10 / defaultScreenHeight) * screenHeightTotal, bottom: (10 / defaultScreenHeight) * screenHeightTotal),
                                                                                        child: Image.asset(statusValidation(getItemId[index]['isCorrect']),),
                                                                                      ),
                                                                                      // Text(
                                                                                      //   statusValidation(getItemId[index]['isCorrect']),
                                                                                      //   style: TextStyle(
                                                                                      //     color: Color.fromARGB(255, 0, 3, 44),
                                                                                      //     fontSize: 14,
                                                                                      //     fontFamily: "IBMPlexSans",
                                                                                      //     fontWeight: FontWeight.w500,
                                                                                      //   ),
                                                                                      //   textAlign: TextAlign.center,
                                                                                      // ),
                                                                                    ],
                                                                                  ),
                                                                                  Column(
                                                                                    children: <Widget>[
                                                                                      Text(
                                                                                        "Time",
                                                                                        style: TextStyle(
                                                                                          color: Color.fromARGB(255, 153, 154, 171),
                                                                                          fontSize: 12,
                                                                                          fontFamily: "IBMPlexSans",
                                                                                          fontWeight: FontWeight.w500,
                                                                                        ),
                                                                                        textAlign: TextAlign.center,
                                                                                      ),
                                                                                      getItemId[index]['timeTaken'] == null ? 
                                                                                      Container(
                                                                                        padding: EdgeInsets.only(top: (10 / defaultScreenHeight) * screenHeightTotal, bottom: (10 / defaultScreenHeight) * screenHeightTotal),
                                                                                        child: Image.asset("assets/images/status_hyphen.png"),
                                                                                      ):
                                                                                      Text(
                                                                                        _printDuration(Duration(seconds: getItemId[index]['timeTaken'])),
                                                                                        style: TextStyle(
                                                                                          color: Color.fromARGB(255, 0, 3, 44),
                                                                                          fontSize: 14,
                                                                                          fontFamily: "IBMPlexSans",
                                                                                          fontWeight: FontWeight.w500,
                                                                                        ),
                                                                                        textAlign: TextAlign.center,
                                                                                      ),
                                                                                    ],
                                                                                  ),
                                                                                ],
                                                                              ),
                                                                            ),
                                                                          ),
                                                                          (getItemId[index]['averageTimeTaken'] == null && getItemId[index]['attemptPercentage'] == null && getItemId[index]['correctPercentage'] == getItemId[index]['pvalue'])
                                                                              ? Container()
                                                                              : Container(
                                                                            decoration:
                                                                                BoxDecoration(
                                                                              color: NeutralColors.ice_blue,
                                                                              borderRadius: BorderRadius.all(Radius.circular(5)),
                                                                            ),
                                                                            margin:
                                                                                EdgeInsets.only(top: (15 / defaultScreenHeight) * screenHeightTotal),
                                                                            child:
                                                                                Padding(
                                                                              padding: EdgeInsets.only(top: (10 / defaultScreenHeight) * screenHeightTotal, bottom: (10 / defaultScreenHeight) * screenHeightTotal, left: (10 / defaultScreenWidth) * screenWidthTotal, right: (10 / defaultScreenWidth) * screenWidthTotal),
                                                                              child: Row(
                                                                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                                                crossAxisAlignment: CrossAxisAlignment.start,
                                                                                children: <Widget>[
                                                                                  Column(
                                                                                    children: <Widget>[
                                                                                      Text(
                                                                                        "Average \nTime",
                                                                                        style: TextStyle(
                                                                                          color: Color.fromARGB(255, 153, 154, 171),
                                                                                          fontSize: 12,
                                                                                          fontFamily: "IBMPlexSans",
                                                                                          fontWeight: FontWeight.w500,
                                                                                        ),
                                                                                        textAlign: TextAlign.center,
                                                                                      ),
                                                                                      getItemId[index]['averageTimeTaken'] == null ? 
                                                                                      Container(
                                                                                        padding: EdgeInsets.only(top: (10 / defaultScreenHeight) * screenHeightTotal, bottom: (10 / defaultScreenHeight) * screenHeightTotal),
                                                                                        child: Image.asset("assets/images/status_hyphen.png"),
                                                                                      ):
                                                                                      Text(
                                                                                         _printDuration(Duration(seconds: int.parse(getItemId[index]['averageTimeTaken']))),
                                                                                        style: TextStyle(
                                                                                          color: Color.fromARGB(255, 0, 3, 44),
                                                                                          fontSize: 14,
                                                                                          fontFamily: "IBMPlexSans",
                                                                                          fontWeight: FontWeight.w500,
                                                                                        ),
                                                                                        textAlign: TextAlign.center,
                                                                                      ),
                                                                                    ],
                                                                                  ),
                                                                                  Column(
                                                                                    children: <Widget>[
                                                                                      Text(
                                                                                        "Overall \nAttempts",
                                                                                        style: TextStyle(
                                                                                          color: Color.fromARGB(255, 153, 154, 171),
                                                                                          fontSize: 12,
                                                                                          fontFamily: "IBMPlexSans",
                                                                                          fontWeight: FontWeight.w500,
                                                                                        ),
                                                                                        textAlign: TextAlign.center,
                                                                                      ),
                                                                                      getItemId[index]['attemptPercentage'] == null ? 
                                                                                      Container(
                                                                                        padding: EdgeInsets.only(top: (10 / defaultScreenHeight) * screenHeightTotal, bottom: (10 / defaultScreenHeight) * screenHeightTotal),
                                                                                        child: Image.asset("assets/images/status_hyphen.png"),
                                                                                      ):
                                                                                      Text(
                                                                                        getItemId[index]['attemptPercentage'].toString() + "%",
                                                                                        style: TextStyle(
                                                                                          color: Color.fromARGB(255, 0, 3, 44),
                                                                                          fontSize: 14,
                                                                                          fontFamily: "IBMPlexSans",
                                                                                          fontWeight: FontWeight.w500,
                                                                                        ),
                                                                                        textAlign: TextAlign.center,
                                                                                      ),
                                                                                    ],
                                                                                  ),
                                                                                  Column(
                                                                                    children: <Widget>[
                                                                                      Text(
                                                                                        "Overall \nAccuracy",
                                                                                        style: TextStyle(
                                                                                          color: Color.fromARGB(255, 153, 154, 171),
                                                                                          fontSize: 12,
                                                                                          fontFamily: "IBMPlexSans",
                                                                                          fontWeight: FontWeight.w500,
                                                                                        ),
                                                                                        textAlign: TextAlign.center,
                                                                                      ),
                                                                                      getItemId[index]['correctPercentage'] == null ? 
                                                                                      Container(
                                                                                        padding: EdgeInsets.only(top: (10 / defaultScreenHeight) * screenHeightTotal, bottom: (10 / defaultScreenHeight) * screenHeightTotal),
                                                                                        child: Image.asset("assets/images/status_hyphen.png"),
                                                                                      ):
                                                                                      Text(
                                                                                        getItemId[index]['correctPercentage'].toString() + "%",
                                                                                        style: TextStyle(
                                                                                          color: Color.fromARGB(255, 0, 3, 44),
                                                                                          fontSize: 14,
                                                                                          fontFamily: "IBMPlexSans",
                                                                                          fontWeight: FontWeight.w500,
                                                                                        ),
                                                                                        textAlign: TextAlign.center,
                                                                                      ),
                                                                                    ],
                                                                                  ),
                                                                                  Column(
                                                                                    children: <Widget>[
                                                                                      Text(
                                                                                        "Overall \nP-Value",
                                                                                        style: TextStyle(
                                                                                          color: Color.fromARGB(255, 153, 154, 171),
                                                                                          fontSize: 12,
                                                                                          fontFamily: "IBMPlexSans",
                                                                                          fontWeight: FontWeight.w500,
                                                                                        ),
                                                                                        textAlign: TextAlign.center,
                                                                                      ),
                                                                                      getItemId[index]['pvalue'] == null ? 
                                                                                      Container(
                                                                                        padding: EdgeInsets.only(top: (10 / defaultScreenHeight) * screenHeightTotal, bottom: (10 / defaultScreenHeight) * screenHeightTotal),
                                                                                        child: Image.asset("assets/images/status_hyphen.png"),
                                                                                      ):
                                                                                      Text(
                                                                                        getItemId[index]['pvalue'].toString() + "%",
                                                                                        style: TextStyle(
                                                                                          color: Color.fromARGB(255, 0, 3, 44),
                                                                                          fontSize: 14,
                                                                                          fontFamily: "IBMPlexSans",
                                                                                          fontWeight: FontWeight.w500,
                                                                                        ),
                                                                                        textAlign: TextAlign.center,
                                                                                      ),
                                                                                    ],
                                                                                  )
                                                                                ],
                                                                              ),
                                                                            ),
                                                                          ),
                                                                          (getItemId[index]['topperAverageTimeTaken'] == null && getItemId[index]['topperAttemptPercentage'] == null && getItemId[index]['topperCorrectPercentage'] == getItemId[index]['topperPvalue'])
                                                                              ? Container()
                                                                              : Container(
                                                                                  decoration: BoxDecoration(
                                                                                    color: NeutralColors.ice_blue,
                                                                                    borderRadius: BorderRadius.all(Radius.circular(5)),
                                                                                  ),
                                                                                  margin: EdgeInsets.only(top: (15 / defaultScreenHeight) * screenHeightTotal),
                                                                                  child: Padding(
                                                                                    padding: EdgeInsets.only(top: (10 / defaultScreenHeight) * screenHeightTotal, bottom: (10 / defaultScreenHeight) * screenHeightTotal, left: (10 / defaultScreenWidth) * screenWidthTotal, right: (10 / defaultScreenWidth) * screenWidthTotal),
                                                                                    child: Row(
                                                                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                                                      crossAxisAlignment: CrossAxisAlignment.start,
                                                                                      children: <Widget>[
                                                                                        Column(
                                                                                          children: <Widget>[
                                                                                            Text(
                                                                                              "Topper's \n Time",
                                                                                              style: TextStyle(
                                                                                                color: Color.fromARGB(255, 153, 154, 171),
                                                                                                fontSize: 12,
                                                                                                fontFamily: "IBMPlexSans",
                                                                                                fontWeight: FontWeight.w500,
                                                                                              ),
                                                                                              textAlign: TextAlign.center,
                                                                                              maxLines: 2,
                                                                                            ),
                                                                                            getItemId[index]['topperAverageTimeTaken'] == null ? 
                                                                                            Container(
                                                                                              padding: EdgeInsets.only(top: (10 / defaultScreenHeight) * screenHeightTotal, bottom: (10 / defaultScreenHeight) * screenHeightTotal),
                                                                                              child: Image.asset("assets/images/status_hyphen.png"),
                                                                                            ):
                                                                                            Text(
                                                                                              //statusValidation(getItemId[index]['topperAverageTimeTaken']),
                                                                                              _printDuration(Duration(seconds: int.parse(getItemId[index]['topperAverageTimeTaken']))),
                                                                                              style: TextStyle(
                                                                                                color: Color.fromARGB(255, 0, 3, 44),
                                                                                                fontSize: 14,
                                                                                                fontFamily: "IBMPlexSans",
                                                                                                fontWeight: FontWeight.w500,
                                                                                              ),
                                                                                              textAlign: TextAlign.center,
                                                                                            ),
                                                                                          ],
                                                                                        ),
                                                                                        Column(
                                                                                          children: <Widget>[
                                                                                            Text(
                                                                                              "Topper's \nAttempts",
                                                                                              style: TextStyle(
                                                                                                color: Color.fromARGB(255, 153, 154, 171),
                                                                                                fontSize: 12,
                                                                                                fontFamily: "IBMPlexSans",
                                                                                                fontWeight: FontWeight.w500,
                                                                                              ),
                                                                                              textAlign: TextAlign.center,
                                                                                            ),
                                                                                            getItemId[index]['topperAttemptPercentage'] == null ? 
                                                                                            Container(
                                                                                              padding: EdgeInsets.only(top: (10 / defaultScreenHeight) * screenHeightTotal, bottom: (10 / defaultScreenHeight) * screenHeightTotal),
                                                                                              child: Image.asset("assets/images/status_hyphen.png"),
                                                                                            ):
                                                                                            Text(
                                                                                              getItemId[index]['topperAttemptPercentage'].toString() + "%",
                                                                                              style: TextStyle(
                                                                                                color: Color.fromARGB(255, 0, 3, 44),
                                                                                                fontSize: 14,
                                                                                                fontFamily: "IBMPlexSans",
                                                                                                fontWeight: FontWeight.w500,
                                                                                              ),
                                                                                              textAlign: TextAlign.center,
                                                                                              maxLines: 2,
                                                                                            ),
                                                                                          ],
                                                                                        ),
                                                                                        Column(
                                                                                          children: <Widget>[
                                                                                            Text(
                                                                                              "Topper's \nAccuracy",
                                                                                              style: TextStyle(
                                                                                                color: Color.fromARGB(255, 153, 154, 171),
                                                                                                fontSize: 12,
                                                                                                fontFamily: "IBMPlexSans",
                                                                                                fontWeight: FontWeight.w500,
                                                                                              ),
                                                                                              textAlign: TextAlign.center,
                                                                                              maxLines: 2,
                                                                                            ),
                                                                                            getItemId[index]['topperCorrectPercentage'] == null ? 
                                                                                            Container(
                                                                                              padding: EdgeInsets.only(top: (10 / defaultScreenHeight) * screenHeightTotal, bottom: (10 / defaultScreenHeight) * screenHeightTotal),
                                                                                              child: Image.asset("assets/images/status_hyphen.png"),
                                                                                            ):
                                                                                            Text(
                                                                                              getItemId[index]['topperCorrectPercentage'].toString() + "%",
                                                                                              style: TextStyle(
                                                                                                color: Color.fromARGB(255, 0, 3, 44),
                                                                                                fontSize: 14,
                                                                                                fontFamily: "IBMPlexSans",
                                                                                                fontWeight: FontWeight.w500,
                                                                                              ),
                                                                                              textAlign: TextAlign.center,
                                                                                            ),
                                                                                          ],
                                                                                        ),
                                                                                        Column(
                                                                                          children: <Widget>[
                                                                                            Text(
                                                                                              "Topper's \n P-Value",
                                                                                              style: TextStyle(
                                                                                                color: Color.fromARGB(255, 153, 154, 171),
                                                                                                fontSize: 12,
                                                                                                fontFamily: "IBMPlexSans",
                                                                                                fontWeight: FontWeight.w500,
                                                                                              ),
                                                                                              textAlign: TextAlign.center,
                                                                                              maxLines: 2,
                                                                                            ),
                                                                                            getItemId[index]['topperPvalue'] == null ? 
                                                                                            Container(
                                                                                              padding: EdgeInsets.only(top: (10 / defaultScreenHeight) * screenHeightTotal, bottom: (10 / defaultScreenHeight) * screenHeightTotal),
                                                                                              child: Image.asset("assets/images/status_hyphen.png"),
                                                                                            ):
                                                                                            Text(
                                                                                              getItemId[index]['topperPvalue'].toString() + "%",
                                                                                              style: TextStyle(
                                                                                                color: Color.fromARGB(255, 0, 3, 44),
                                                                                                fontSize: 14,
                                                                                                fontFamily: "IBMPlexSans",
                                                                                                fontWeight: FontWeight.w500,
                                                                                              ),
                                                                                              textAlign: TextAlign.center,
                                                                                            ),
                                                                                          ],
                                                                                        )
                                                                                      ],
                                                                                    ),
                                                                                  ),
                                                                                ),
                                                                        ],
                                                                      )
                                                                    : Container(),
                                                              ],
                                                            ),
                                                          ),
                                                        ),
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              );
                                            },
                                            separatorBuilder: (context, index) {
                                              return Divider();
                                            },
                                          )
                                        : Center(
                                            child: Text("No Data"),
                                          ),
                                  ),
                          ),
                        )
                      ],
                    ),
                  ),

                  TimeManagemt(
                      overalltimetakenforcorrectanswer,
                      overalltimetakenforincorrectanswer,
                      overalltimetakenforskippedanswer,
                      overalltotalnumberfquestionsforcorrectanswer,
                      overalltotalnumberofquestionsforincorrectanswer,
                      overalltotalnumberofquestionsforskippedanswer,
                      listofsectionname,
                      listofareanames,
                      listoftopicnames,
                    listoftotaltopictotalnumberofquestionsforcorrectanswer,
                   listoftotalareatotalnumberofquestionsforcorrectanswer,
                   listofsectiontotalnumberofquestionsforcorrectanswer,
                   listoftotaltopictotalnumberofquestionsforincorrectanswer,
                  listoftotalareatotalnumberofquestionsforincorrectanswer,
                   listofsectiontotalnumberofquestionsforincorrectanswer,
                   listoftotaltopictotalnumberofquestionsforskippedanswer,
                  listoftotalareatotalnumberofquestionsforskippedanswer,
                   listofsectiontotalnumberofquestionsforskippedanswer,
                   listoftotaltopictimetakenforcorrectanswer,
                   listoftotalareatimetakenforcorrectanswer,
                    listofsectiontimetakenforcorrectanswer,
                    listoftotaltopictimetakenforincorrectanswer,
                   listoftotalareatimetakenforincorrectanswer,
                   listofsectiontimetakenforincorrectanswer,
                    listoftotaltopictimetakenforskippedanswer,
                   listoftotalareatimetakenforskippedanswer,
                    listofsectiontimetakenforskippedanswer,

                   ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

import 'package:flutter/material.dart';

var screenWidthTotal;
var screenHeightTotal;

var _safeAreaHorizontal;

var _safeAreaVertical;

var screenHeight;

var screenWidth;

class AnalyticsEfficiencyTracker extends StatefulWidget {
  @override
  AnalyticsEfficiencyTrackerState createState() => AnalyticsEfficiencyTrackerState();
}

class AnalyticsEfficiencyTrackerState extends State<AnalyticsEfficiencyTracker> {

  @override
  Widget build(BuildContext context) {
    var _mediaQueryData = MediaQuery.of(context);
    screenWidthTotal = _mediaQueryData.size.width;
    screenHeightTotal = _mediaQueryData.size.height;
    _safeAreaHorizontal =
        _mediaQueryData.padding.left + _mediaQueryData.padding.right;
    _safeAreaVertical =
        _mediaQueryData.padding.top + _mediaQueryData.padding.bottom;

    screenHeight = screenHeightTotal - _safeAreaVertical;
    screenWidth = screenWidthTotal - _safeAreaHorizontal;


    return Scaffold(
      
      body: Container(),
    );
  }
}


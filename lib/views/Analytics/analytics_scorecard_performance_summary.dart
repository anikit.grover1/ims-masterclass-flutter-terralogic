import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:imsindia/api/bloc/services.dart';
import 'package:imsindia/components/connectivity_Status.dart';
import 'package:imsindia/components/custom_expansion_panel.dart';
import 'package:imsindia/components/custom_expansion_panel_analytics.dart';
import 'package:imsindia/utils/Url.dart';
import 'package:imsindia/utils/colors.dart';
import 'package:imsindia/utils/text_styles.dart';
import 'package:imsindia/utils/global.dart' as global;
import 'package:imsindia/views/GK_Zone/gk_zone_demo.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../utils/colors.dart';
import 'analytics_performance_analytics.dart';
import 'analytics_performance_analytics.dart';

var screenWidthTotal;
var screenHeightTotal;

var _safeAreaHorizontal;

var _safeAreaVertical;

var screenHeight;

var screenWidth;
var i;


class SummaryTable extends StatefulWidget {

  var listofsectionname = [];
  var listofareanames = [];
  var listoftopicnames = [];
  var listofareamarks = [];
  var listoftotallength = [];
  var listofareaattempts = [];
  var listofareaaccuracy = [];
  var listofareatimespent = [];
  var listofareatimespentNotNull = [];
  var listoftopicmarks = [];
  var listoftopiclength = [];
  var listoftopicattempts = [];
  var listoftopicaccuracy = [];
  var listoftopictimespent = [];
  var listoftopictimespentNotNull = [];
  var listofsectionmarks = [];
  var listofsectionattempts = [];
  var listofsectionaccuracy = [];
  var listofsectiontimetaken = [];
  var listofsectiontimetakenNotNull = [];
  var overallmarks;
  var overallattempts;
  var overallaccuracy;
  var overalltimetaken;
  var overalltimetakenNotNull;
  var selectedLearningObject;
  var testTemplate;
  var testTemplateForXat;
  SummaryTable(this.testTemplate,this.selectedLearningObject,this.testTemplateForXat,this.listofsectionname,this.listofareanames,this.listoftopicnames,this.overallmarks,this.overallattempts,this.overallaccuracy,this.overalltimetaken,this.overalltimetakenNotNull,this.listofsectionmarks,this.listofsectionattempts,this.listofsectionaccuracy,this.listofsectiontimetaken,this.listofsectiontimetakenNotNull,this.listofareamarks,this.listoftotallength,this.listofareaattempts,this.listofareaaccuracy,this.listofareatimespent,this.listofareatimespentNotNull,this.listoftopicmarks,this.listoftopiclength,this.listoftopicattempts,this.listoftopicaccuracy,this.listoftopictimespent,this.listoftopictimespentNotNull);
  @override
  SummaryTableState createState() => SummaryTableState();
}

class SummaryTableState extends State<SummaryTable> {
  bool connected;
  String testId;
  final GlobalKey<ScaffoldState> _scaffoldstate =
      new GlobalKey<ScaffoldState>();
  var refreshKey = GlobalKey<RefreshIndicatorState>();
  var connectivityResult;
//  var test = new Set();
  var section=new Map();
  var area= new Map();
  var areaName = new Map();
  var topicNames = new Map();
  var topic = new Map();
  var sectionName = ["All section"];
  String course;

  @override
  void initState() {
    super.initState();
    //calling api method
    //print('init state called');
  }

  String _printDuration(Duration duration) {
    if(duration!=null){
      String twoDigits(int n) {
        if (n >= 10) return "$n";
        return "0$n";
      }

      String twoDigitMinutes = twoDigits(duration.inMinutes.remainder(60));
      String twoDigitSeconds = twoDigits(duration.inSeconds.remainder(60));
      if(twoDigits(duration.inHours).toString()=="00"){
        return "$twoDigitMinutes:$twoDigitSeconds";
      }else{
        return "${twoDigits(duration.inHours)}:$twoDigitMinutes:$twoDigitSeconds";
      }
    }else{
      return "";
    }

  }

  // on dragging page refresh function will be activated

  int activeMeterIndex;
  int activeMeterIndexForSecondExpansion;
  var value;

  @override
  Widget build(BuildContext context) {
    var _mediaQueryData = MediaQuery.of(context);
    screenWidthTotal = _mediaQueryData.size.width;
    screenHeightTotal = _mediaQueryData.size.height;
    _safeAreaHorizontal =
        _mediaQueryData.padding.left + _mediaQueryData.padding.right;
    _safeAreaVertical =
        _mediaQueryData.padding.top + _mediaQueryData.padding.bottom;

    screenHeight = screenHeightTotal - _safeAreaVertical;
    screenWidth = screenWidthTotal - _safeAreaHorizontal;
    TrackingScrollController _scrollController;

    return Scaffold(
      body: Column(
        children: <Widget>[
          Expanded(
            //height: 590 / 720 * screenHeight,
            child: SingleChildScrollView(
              child: Container(
                child: Column(
                  children: [
                    widget.testTemplateForXat==true?Container(
                      margin: EdgeInsets.only(top: 20/720 * screenHeight, left: 10/360 * screenWidth, right: 10/360 * screenWidth,bottom: 20/720 * screenHeight),
                      child: Text(
                        "Performance Analysis section does not deduct penalty for skipped questions to give a true representation of your area-wise efficiency. The overall score is a total of all sections including GK. Refer to the Scorecard section above for your scores as per official exam criteria.",
                        style: TextStyle(
                          fontSize: 14/720 * screenHeight,
                          fontFamily: "IBMPlexSans",
                          fontWeight: FontWeight.w400,
                          color: NeutralColors.gunmetal,
                        ),
                        textAlign: TextAlign.left,
                      ),
                    ):widget.testTemplate==true?
                    Container(
                      margin: EdgeInsets.only(top: 20/720 * screenHeight, left: 10/360 * screenWidth, right: 10/360 * screenWidth,bottom: 20/720 * screenHeight),
                      child: Text(
                        "Not applicable as the selected test is adaptive with no negative marking and all questions should be attempted.",
                        style: TextStyle(
                          fontSize: 14/720 * screenHeight,
                          fontFamily: "IBMPlexSans",
                          fontWeight: FontWeight.w400,
                          color: NeutralColors.gunmetal,
                        ),
                        textAlign: TextAlign.left,
                      ),
                    ):Container(),
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Container(
                          decoration: BoxDecoration(
                            color: Color.fromARGB(255, 255, 255, 255),
                            boxShadow: [
                              BoxShadow(
                                color: Color.fromARGB(61, 193, 188, 164),
                                offset: Offset(10, 0),
                                blurRadius: 22,
                              ),
                            ],
                          ),
                          width: screenWidthTotal / 3,
                          child: Column(
                            children: <Widget>[
                              Container(
                                child: ListView.builder(
                                    physics: NeverScrollableScrollPhysics(),
                                    //controller: _scrollController,
                                    shrinkWrap: true,
                                    itemCount: widget.listofsectionname.length+2,
                                    itemBuilder: (context, k) {
                                      Color color;
                                      if (k % 2 == 0) {
                                        color = Colors.white;
                                      } else if (k % 2 == 1) {
                                        color = NeutralColors.sun_yellow
                                            .withOpacity(0.1);
                                      }
                                      if (k == 0) {
                                        return Container(
                                          color: Colors.white,
                                          height: 80/720 * screenHeightTotal,
                                          child: Row(
                                            mainAxisAlignment:
                                            MainAxisAlignment.spaceEvenly,
                                            mainAxisSize: MainAxisSize.max,
                                            children: <Widget>[
                                              Container(
                                                width: 85 / 360 * screenWidthTotal,
                                                margin: EdgeInsets.only(
                                                    left: 10 / 360 * screenWidthTotal,
                                                    top: 26 / 720 * screenHeightTotal,
                                                    bottom:
                                                    26 / 720 * screenHeightTotal),
                                                child: Text(
                                                  "AREA",
                                                  style: TextStyle(
                                                    fontSize:
                                                    10 / 720 * screenHeightTotal,
                                                    fontFamily:
                                                    "IBMPlexSans-Medium",
                                                    fontWeight: FontWeight.w500,
                                                    color: NeutralColors.blue_grey,
                                                  ),
                                                  textAlign: TextAlign.left,
                                                  overflow: TextOverflow.ellipsis,
                                                ),
                                              ),
                                            ],
                                          ),
                                          //padding: EdgeInsets.all(10.0),
                                        );
                                      }else if(k == 1){
                                        return Container(
                                          color: NeutralColors.sun_yellow
                                              .withOpacity(0.1),
                                          height: 50/720 * screenHeightTotal,
                                          child: Row(
                                            mainAxisAlignment:
                                            MainAxisAlignment.start,
                                            mainAxisSize: MainAxisSize.max,
                                            children: <Widget>[
                                              Container(
                                                width: 85 / 360 * screenWidthTotal,
                                                margin: EdgeInsets.only(
                                                    left: 10 / 360 * screenWidthTotal,
                                                    top: 15 / 720 * screenHeightTotal,
                                                    bottom:
                                                    15 / 720 * screenHeightTotal),
                                                child: Text(
                                                  "Overall",
                                                  style: TextStyle(
                                                    fontSize:
                                                    12 / 720 * screenHeightTotal,
                                                    fontFamily:
                                                    "IBMPlexSans",
                                                    fontWeight: FontWeight.w400,
                                                    color: NeutralColors.dark_navy_blue,
                                                  ),
                                                  textAlign: TextAlign.left,
                                                  overflow: TextOverflow.ellipsis,
                                                ),
                                              ),
                                            ],
                                          ),
                                          //padding: EdgeInsets.all(10.0),
                                        );
                                      }
                                      if(widget.listofsectionname[k-2] == "Pyschometric" || widget.listofsectionname[k-2] == "Descriptive"|| widget.listofsectionname[k-2] == "Descriptive Writing"){
                                        return Container();
                                      }else {
                                        return Container(
                                          //color: color,
                                          child: Column(
                                            children: <Widget>[
                                              Container(
                                                child: Row(
                                                  mainAxisAlignment:
                                                  MainAxisAlignment.start,
                                                  children: <Widget>[
                                                    Expanded(
                                                      child: Container(
                                                        //height: 50/720 * screenHeight,
                                                        //color: color,
                                                        child:
                                                        CustomExpansionPanelList(
                                                          expansionHeaderHeight:
                                                          50 / 720 * screenHeightTotal,
                                                          iconColor: NeutralColors.blue_grey,
                                                          backgroundColor1:  color,
                                                          backgroundColor2:  color,
                                                          expansionCallback: (int index, bool status) {
                                                            setState(() {
                                                              activeMeterIndex = activeMeterIndex == k ? null : k;
                                                            });
                                                          },
                                                          children: [
                                                            new ExpansionPanel(
                                                              canTapOnHeader: true,
                                                              isExpanded:activeMeterIndex == k,
                                                              headerBuilder: (BuildContext context, bool isExpanded) =>
                                                              new Container(
                                                                margin: EdgeInsets.only(left: 10 / 360 * screenWidthTotal, top:15/720*screenHeightTotal, bottom: 15/720*screenHeightTotal),
                                                                child: new Text(
                                                                  widget.listofsectionname[k-2].toString(),
                                                                  style: new TextStyle(
                                                                      fontSize: (12 / 720) * screenHeightTotal,
                                                                      fontFamily: "IBMPlexSans",
                                                                      fontWeight: FontWeight.w400,
                                                                      color: NeutralColors.dark_navy_blue
                                                                  ),
                                                                  overflow: TextOverflow.ellipsis,
                                                                ),
                                                                //padding: EdgeInsets.all(10.0),
                                                              ),
                                                              body: ListView.builder(
                                                                  physics: NeverScrollableScrollPhysics(),
                                                                  //controller: _scrollController,
                                                                  shrinkWrap: true,
                                                                  itemCount: widget.listofareanames[k-2].length,
                                                                  itemBuilder: (context, j) {
//                                                              Color color;
//                                                              if (j % 2 == 0) {
//                                                                color = Colors.white;
//                                                              } else if (j % 2 == 1) {
//                                                                color = NeutralColors.sun_yellow
//                                                                    .withOpacity(0.1);
//                                                              }
                                                                    return Container(
                                                                      color: color,
                                                                      child: Column(
                                                                        children: <Widget>[
                                                                          Container(
                                                                            child: Row(
                                                                              mainAxisAlignment:
                                                                              MainAxisAlignment.start,
                                                                              children: <Widget>[
                                                                                Expanded(
                                                                                  child: Container(
                                                                                    //height: 50/720 * screenHeight,
                                                                                    //color: color,
                                                                                    child: CustomExpansionPanelList(
                                                                                      expansionHeaderHeight: 50 / 720 * screenHeightTotal,
                                                                                      iconColor:  NeutralColors.blue_grey,
                                                                                      backgroundColor1:  color,
                                                                                      backgroundColor2: color,
                                                                                      expansionCallback: (int index, bool status) {
                                                                                        setState(() {
                                                                                          activeMeterIndexForSecondExpansion = activeMeterIndexForSecondExpansion == j ? null : j;
                                                                                        });
                                                                                      },
                                                                                      children: [
                                                                                        new ExpansionPanel(
                                                                                          canTapOnHeader: true,
                                                                                          isExpanded:activeMeterIndexForSecondExpansion == j,
                                                                                          headerBuilder: (BuildContext context, bool isExpanded) =>
                                                                                          new Container(
                                                                                            margin: EdgeInsets.only(left: 10 / 360 * screenWidthTotal, top:15/720*screenHeightTotal, bottom: 15/720*screenHeightTotal),
                                                                                            child: new Text(
                                                                                              widget.listofareanames[k-2][j].toString(),
                                                                                              style: new TextStyle(
                                                                                                  fontSize: (12 / 720) * screenHeightTotal,
                                                                                                  fontFamily: "IBMPlexSans",
                                                                                                  fontWeight: FontWeight.w400,
                                                                                                  color: NeutralColors.dark_navy_blue
                                                                                              ),
                                                                                              overflow: TextOverflow.ellipsis,
                                                                                            ),
                                                                                            //padding: EdgeInsets.all(10.0),
                                                                                          ),
                                                                                          body: new Container(
                                                                                            //color: color,
                                                                                            child: new ListView.builder(
                                                                                              shrinkWrap: true,
                                                                                              physics:
                                                                                              NeverScrollableScrollPhysics(),
//                                                                    itemExtent: (50.0 /
//                                                                        720) *
//                                                                        screenHeightTotal,
                                                                                              itemBuilder: (BuildContext context, int i) {
                                                                                                return  Container(
//                                                                  margin: EdgeInsets.only(
//                                                                      left: 10 /
//                                                                          360 *
//                                                                          screenWidth,
//                                                                      top: 15 /
//                                                                          720 *
//                                                                          screenHeight,
//                                                                    bottom: 15 /
//                                                                        720 *
//                                                                        screenHeight,
//                                                                          ),
                                                                                                  child: Text(
                                                                                                    widget.listoftopicnames[k-2][j][i].toString(),
                                                                                                    textAlign: TextAlign.left,
                                                                                                    style: new TextStyle(
                                                                                                        fontSize: (12 / 720) * screenHeightTotal,
                                                                                                        fontFamily: "IBMPlexSans",
                                                                                                        fontWeight: FontWeight.w400,
                                                                                                        color: NeutralColors.blue_grey
                                                                                                    ),
                                                                                                    overflow: TextOverflow.ellipsis,
                                                                                                  ),
                                                                                                  padding: EdgeInsets.all(10.0),
                                                                                                );
                                                                                              },
                                                                                              itemCount: widget.listoftopicnames[k-2][j].length,
                                                                                            ),
                                                                                            //padding: EdgeInsets.all(10.0),
                                                                                          ),
                                                                                        ),
                                                                                      ],
                                                                                    ),
                                                                                    //padding: EdgeInsets.all(10.0),
                                                                                  ),
                                                                                ),
                                                                              ],
                                                                            ),
                                                                          ),
                                                                        ],
                                                                      ),
                                                                    );
                                                                  }),
                                                            ),
                                                          ],
                                                        ),
                                                        //padding: EdgeInsets.all(10.0),
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                            ],
                                          ),
                                        );
                                      }
                                    }),
                              ),
                            ],
                          ),
                        ),
                        Container(
                          decoration: BoxDecoration(
                            color: Color.fromARGB(255, 255, 255, 255),
                            boxShadow: [
                              BoxShadow(
                                color: Color.fromARGB(61, 193, 188, 164),
                                offset: Offset(10, 0),
                                blurRadius: 22,
                              ),
                            ],
                          ),
                          width: 240 / 360 * screenWidthTotal,
                          //color: Colors.white,
                          child: Column(
                            children: <Widget>[
                              Container(
                                child: Scrollbar(
                                  //ListView
                                  child: SingleChildScrollView(
                                    scrollDirection: Axis.horizontal,
                                    child: Container(
                                      width: 670 / 360 * screenWidthTotal,
                                      child: ListView.builder(
                                          physics: NeverScrollableScrollPhysics(),
                                          //controller: _scrollController,
                                          shrinkWrap: true,
                                          itemCount: widget.listofsectionname.length+2,
                                          itemBuilder: (context, k) {
                                            //final item = datas[index];
                                            Color color;
                                            if (k % 2 == 0) {
                                              color = Colors.white;
                                            } else if (k % 2 == 1) {
                                              color = NeutralColors.sun_yellow
                                                  .withOpacity(0.1);
                                            }
                                            if (k == 0) {
                                              return Container(
                                                color: Colors.white,
                                                height: 80/720 * screenHeightTotal,
                                                child: Row(
                                                  children: <Widget>[
                                                    Container(
                                                      width: 70 / 360 * screenWidthTotal,
                                                      margin: EdgeInsets.only(
                                                          left: 18 /
                                                              360 *
                                                              screenWidthTotal,
                                                          top: 15 /
                                                              720 *
                                                              screenHeightTotal,
                                                          bottom: 13 /
                                                              720 *
                                                              screenHeightTotal),
                                                      child: Row(
                                                        crossAxisAlignment:
                                                        CrossAxisAlignment
                                                            .start,
                                                        children: [
                                                          Text(
                                                            "MARKS",
                                                            style: TextStyle(
                                                              fontSize: 10 /
                                                                  720 *
                                                                  screenHeightTotal,
                                                              fontFamily:
                                                              "IBMPlexSans-Medium",
                                                              fontWeight:
                                                              FontWeight.w500,
                                                              color: NeutralColors
                                                                  .blue_grey,
                                                            ),
                                                            textAlign:
                                                            TextAlign.center,
                                                          ),
                                                        ],
                                                      ),
                                                    ),
                                                    Container(
                                                      width: 70 / 360 * screenWidthTotal,
                                                      margin: EdgeInsets.only(
                                                          left: 10 /
                                                              360 *
                                                              screenWidthTotal,
                                                          top: 15 /
                                                              720 *
                                                              screenHeightTotal,
                                                          bottom: 13 /
                                                              720 *
                                                              screenHeightTotal),
                                                      child: Row(
                                                          crossAxisAlignment:
                                                          CrossAxisAlignment
                                                              .start,
                                                          children: [
                                                            Text(
                                                              "ATTEMPTS",
                                                              style: TextStyle(
                                                                fontSize: 10 /
                                                                    720 *
                                                                    screenHeightTotal,
                                                                fontFamily:
                                                                "IBMPlexSans-Medium",
                                                                fontWeight:
                                                                FontWeight.w500,
                                                                color: NeutralColors
                                                                    .blue_grey,
                                                              ),
                                                              textAlign:
                                                              TextAlign.center,
                                                            ),
                                                          ]),
                                                    ),
                                                    Container(
                                                      width: 70 / 360 * screenWidthTotal,
                                                      margin: EdgeInsets.only(
                                                          left: 10 /
                                                              360 *
                                                              screenWidthTotal,
                                                          top: 15 /
                                                              720 *
                                                              screenHeightTotal,
                                                          bottom: 13 /
                                                              720 *
                                                              screenHeightTotal),
                                                      child: Row(
                                                          crossAxisAlignment:
                                                          CrossAxisAlignment
                                                              .start,
                                                          children: [
                                                            Text(
                                                              "CORRECT",
                                                              style: TextStyle(
                                                                fontSize: 10 /
                                                                    720 *
                                                                    screenHeightTotal,
                                                                fontFamily:
                                                                "IBMPlexSans-Medium",
                                                                fontWeight:
                                                                FontWeight.w500,
                                                                color: NeutralColors
                                                                    .blue_grey,
                                                              ),
                                                              textAlign:
                                                              TextAlign.center,
                                                            ),
                                                          ]),
                                                    ),
                                                    Container(
                                                      width: 80 / 360 * screenWidthTotal,
                                                      margin: EdgeInsets.only(
                                                          left: 10 /
                                                              360 *
                                                              screenWidthTotal,
                                                          top: 15 /
                                                              720 *
                                                              screenHeightTotal,
                                                          bottom: 13 /
                                                              720 *
                                                              screenHeightTotal),
                                                      child: Row(
                                                        crossAxisAlignment:
                                                        CrossAxisAlignment
                                                            .start,
                                                        children: <Widget>[
                                                          Text(
                                                            "ACCURACY",
                                                            style: TextStyle(
                                                              fontSize: 10 /
                                                                  720 *
                                                                  screenHeightTotal,
                                                              fontFamily:
                                                              "IBMPlexSans-Medium",
                                                              fontWeight:
                                                              FontWeight.w500,
                                                              color: NeutralColors
                                                                  .blue_grey,
                                                            ),
                                                            textAlign:
                                                            TextAlign.center,
                                                          ),
                                                        ],
                                                      ),
                                                    ),
                                                    Container(
                                                      width: 90 / 360 * screenWidthTotal,
                                                      margin: EdgeInsets.only(
                                                          left: 10 /
                                                              360 *
                                                              screenWidthTotal,
                                                          top: 15 /
                                                              720 *
                                                              screenHeightTotal,
                                                          bottom: 13 /
                                                              720 *
                                                              screenHeightTotal),
                                                      child: Row(
                                                        crossAxisAlignment:
                                                        CrossAxisAlignment
                                                            .start,
                                                        children: <Widget>[
                                                          Text(
                                                            "TOTAL TIME",
                                                            style: TextStyle(
                                                              fontSize: 10 /
                                                                  720 *
                                                                  screenHeightTotal,
                                                              fontFamily:
                                                              "IBMPlexSans-Medium",
                                                              fontWeight:
                                                              FontWeight.w500,
                                                              color: NeutralColors
                                                                  .blue_grey,
                                                            ),
                                                            textAlign:
                                                            TextAlign.center,
                                                          ),
                                                        ],
                                                      ),
                                                    ),
                                                    Container(
                                                      width: 100 / 360 * screenWidthTotal,
                                                      margin: EdgeInsets.only(
                                                          left: 10 /
                                                              360 *
                                                              screenWidthTotal,
                                                          top: 15 /
                                                              720 *
                                                              screenHeightTotal,
                                                          //bottom: 13 / 720 * screenHeightTotal
                                                      ),
                                                      child: Row(
                                                        crossAxisAlignment:
                                                        CrossAxisAlignment
                                                            .start,
                                                        children: <Widget>[
                                                          Container(
                                                            width: 85/360 * screenWidthTotal,
                                                            child: Text(
                                                              "Time Per Attempted Question",
                                                              style: TextStyle(
                                                                fontSize: 10 /
                                                                    720 *
                                                                    screenHeightTotal,
                                                                fontFamily:
                                                                "IBMPlexSans-Medium",
                                                                fontWeight:
                                                                FontWeight.w500,
                                                                color: NeutralColors
                                                                    .blue_grey,
                                                              ),
                                                              textAlign:
                                                              TextAlign.center,
                                                            ),
                                                          ),
                                                        ],
                                                      ),
                                                    ),
                                                    Container(
                                                      width: 92 / 360 * screenWidthTotal,
                                                      margin: EdgeInsets.only(
                                                          left: 10 /
                                                              360 *
                                                              screenWidthTotal,
                                                          top: 15 /
                                                              720 *
                                                              screenHeightTotal,
                                                          bottom: 13 /
                                                              720 *
                                                              screenHeightTotal),
                                                      child: Row(
                                                        crossAxisAlignment:
                                                        CrossAxisAlignment
                                                            .start,
                                                        children: <Widget>[
                                                          Text(
                                                            "Marks per Minute",
                                                            style: TextStyle(
                                                              fontSize: 10 /
                                                                  720 *
                                                                  screenHeightTotal,
                                                              fontFamily:
                                                              "IBMPlexSans-Medium",
                                                              fontWeight:
                                                              FontWeight.w500,
                                                              color: NeutralColors
                                                                  .blue_grey,
                                                            ),
                                                            textAlign:
                                                            TextAlign.center,
                                                          ),
                                                        ],
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                                padding: EdgeInsets.all(10.0),
                                              );
                                            }
                                            if (k == 1 ) {
                                              return Container(
                                                color: NeutralColors.sun_yellow
                                                    .withOpacity(0.1),
                                                height: 50/720 * screenHeightTotal,
                                                child: Row(
                                                  children: <Widget>[
                                                    Container(
                                                      width: 70 / 360 * screenWidthTotal,
                                                      margin: EdgeInsets.only(
                                                          left: 3 /
                                                              360 *
                                                              screenWidthTotal),
                                                      child: Text(
                                                        widget.testTemplate == true?"NA":widget.selectedLearningObject.contains("NMAT")?widget.overallmarks.round().toString()+".00":widget.overallmarks.toStringAsFixed(2).toString(),
                                                        style: TextStyle(
                                                          fontSize: 12 /
                                                              720 *
                                                              screenHeightTotal,
                                                          fontFamily:
                                                          "IBMPlexSans",
                                                          fontWeight:
                                                          FontWeight.w400,
                                                          color: NeutralColors
                                                              .dark_navy_blue,
                                                        ),
                                                        textAlign:
                                                        TextAlign.center,
                                                      ),
                                                    ),
                                                    Container(
                                                      width: 70 / 360 * screenWidthTotal,
                                                      margin: EdgeInsets.only(
                                                          left: 10 /
                                                              360 *
                                                              screenWidthTotal),
                                                      child: Text(
                                                        widget.overallattempts.toString(),
                                                        style: TextStyle(
                                                          fontSize: 12 /
                                                              720 *
                                                              screenHeightTotal,
                                                          fontFamily:
                                                          "IBMPlexSans",
                                                          fontWeight:
                                                          FontWeight.w400,
                                                          color: NeutralColors
                                                              .dark_navy_blue,
                                                        ),
                                                        textAlign:
                                                        TextAlign.center,
                                                      ),
                                                    ),
                                                    Container(
                                                      width: 70 / 360 * screenWidthTotal,
                                                      margin: EdgeInsets.only(
                                                          left: 10 /
                                                              360 *
                                                              screenWidthTotal),
                                                      child: Text(
                                                        widget.overallaccuracy.toString(),
                                                        style: TextStyle(
                                                          fontSize: 12 /
                                                              720 *
                                                              screenHeightTotal,
                                                          fontFamily:
                                                          "IBMPlexSans",
                                                          fontWeight:
                                                          FontWeight.w400,
                                                          color: NeutralColors
                                                              .dark_navy_blue,
                                                        ),
                                                        textAlign:
                                                        TextAlign.center,
                                                      ),
                                                    ),
                                                    Container(
                                                      width: 80 / 360 * screenWidthTotal,
                                                      margin: EdgeInsets.only(
                                                          left: 10 /
                                                              360 *
                                                              screenWidthTotal),
                                                      child: Text(
                                                      widget.overallattempts == 0?"0%":double.parse((widget.overallaccuracy/widget.overallattempts*100).toStringAsFixed(2)).toString()+"%",
                                                        style: TextStyle(
                                                          fontSize: 12 /
                                                              720 *
                                                              screenHeightTotal,
                                                          fontFamily:
                                                          "IBMPlexSans",
                                                          fontWeight:
                                                          FontWeight.w400,
                                                          color: NeutralColors
                                                              .dark_navy_blue,
                                                        ),
                                                        textAlign:
                                                        TextAlign.center,
                                                      ),
                                                    ),
                                                    Container(
                                                      width: 90 / 360 * screenWidthTotal,
                                                      margin: EdgeInsets.only(
                                                          left: 10 /
                                                              360 *
                                                              screenWidthTotal),
                                                      child: Text(
                                                        _printDuration(Duration(seconds:widget.overalltimetaken)),
                                                        style: TextStyle(
                                                          fontSize: 12 /
                                                              720 *
                                                              screenHeightTotal,
                                                          fontFamily:
                                                          "IBMPlexSans",
                                                          fontWeight:
                                                          FontWeight.w400,
                                                          color: NeutralColors
                                                              .dark_navy_blue,
                                                        ),
                                                        textAlign:
                                                        TextAlign.center,
                                                      ),
                                                    ),
                                                    Container(
                                                      width: 100 / 360 * screenWidthTotal,
                                                      margin: EdgeInsets.only(
                                                          left: 10 /
                                                              360 *
                                                              screenWidthTotal),
                                                      child: Container(
                                                        width: 85/360 * screenWidthTotal,
                                                        child: Text(
                                                            widget.overalltimetakenNotNull == 0?"00:00":_printDuration(Duration(seconds:(widget.overalltimetakenNotNull/widget.overallattempts).round().toInt())),
                                                          style: TextStyle(
                                                            fontSize: 12 /
                                                                720 *
                                                                screenHeightTotal,
                                                            fontFamily:
                                                            "IBMPlexSans",
                                                            fontWeight:
                                                            FontWeight.w400,
                                                            color: NeutralColors
                                                                .dark_navy_blue,
                                                          ),
                                                          textAlign:
                                                          TextAlign.center,
                                                        ),
                                                      ),
                                                    ),
                                                    Container(
                                                      width: 80 / 360 * screenWidthTotal,
                                                      margin: EdgeInsets.only(
                                                          left: 10 /
                                                              360 *
                                                              screenWidthTotal),
                                                      child: Text(
                                                        widget.testTemplate == true?"NA":widget.overalltimetaken == 0 ?"0.00": double.parse((widget.overallmarks/widget.overalltimetaken*60).toStringAsFixed(2)).toString(),
                                                        style: TextStyle(
                                                          fontSize: 12 /
                                                              720 *
                                                              screenHeightTotal,
                                                          fontFamily:
                                                          "IBMPlexSans",
                                                          fontWeight:
                                                          FontWeight.w400,
                                                          color: NeutralColors
                                                              .dark_navy_blue,
                                                        ),
                                                        textAlign:
                                                        TextAlign.center,
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                                padding: EdgeInsets.all(10.0),
                                              );
                                            }
                                            return Container(
                                                child: Column(
                                                  children: <Widget>[
                                                    Container(
                                                      //color: color,
                                                      child: Row(
                                                        mainAxisAlignment: MainAxisAlignment.start,
                                                        children: <Widget>[
                                                          Expanded(
                                                            child: Container(
                                                              //margin: EdgeInsets.only(left: 11 / 360 * screenWidth),
                                                              child: CustomExpansionPanelLists(
                                                                expansionHeaderHeight: (50 / 720) * screenHeightTotal,
                                                                iconColor:  NeutralColors.blue_grey,
                                                                backgroundColor1:  color,
                                                                backgroundColor2:  color,
                                                                expansionCallback: (int index, bool status) {
                                                                  setState(() {
                                                                    activeMeterIndex = activeMeterIndex == k ? null : k;
                                                                  });
                                                                },
                                                                children: [
                                                                  new ExpansionPanel(
                                                                    canTapOnHeader: true,
                                                                    isExpanded: activeMeterIndex == k,
                                                                    headerBuilder: (BuildContext context, bool isExpanded) =>
                                                                    new Container(
                                                                      //color: color,
                                                                      height: 50 / 720 * screenHeightTotal,
                                                                      child: Row(
                                                                        mainAxisAlignment: MainAxisAlignment.start,
                                                                        mainAxisSize: MainAxisSize.max,
                                                                        children: <Widget>[
                                                                          Container(
                                                                            width: 70 / 360 * screenWidthTotal,
                                                                            margin: EdgeInsets.only(left: 3 / 360 * screenWidthTotal),
                                                                            child: Text(
                                                                              widget.testTemplate == true?"NA":widget.selectedLearningObject.contains("NMAT")?widget.listofsectionmarks[k-2].round().toString()+".00":widget.listofsectionmarks[k-2].toStringAsFixed(2).toString(),
                                                                              style: TextStyle(
                                                                                  fontSize: 12 / 720 * screenHeightTotal,
                                                                                  fontFamily: "IBMPlexSans",
                                                                                  color: NeutralColors.gunmetal,
                                                                                  fontWeight: FontWeight.w500
                                                                              ),
                                                                              textAlign: TextAlign.center,
                                                                            ),
                                                                          ),
                                                                          Container(
                                                                            width: 70 / 360 * screenWidthTotal,
                                                                            margin: EdgeInsets.only(left: 10 / 360 * screenWidthTotal),
                                                                            child: Text(
                                                                              widget.listofsectionattempts[k-2].toString(),
                                                                              style: TextStyle(
                                                                                fontSize: 12 / 720 * screenHeightTotal,
                                                                                fontFamily: "IBMPlexSans",
                                                                                fontWeight: FontWeight.w500,
                                                                                color: NeutralColors.gunmetal,
                                                                              ),
                                                                              textAlign: TextAlign.center,
                                                                            ),
                                                                          ),
                                                                          Container(
                                                                            width: 70 / 360 * screenWidthTotal,
                                                                            margin: EdgeInsets.only(left: 10 / 360 * screenWidthTotal),
                                                                            child: Text(
                                                                              widget.listofsectionaccuracy[k-2].toString(),
                                                                              style: TextStyle(
                                                                                fontSize: 12 / 720 * screenHeightTotal,
                                                                                fontFamily: "IBMPlexSans",
                                                                                fontWeight: FontWeight.w500,
                                                                                color: NeutralColors.gunmetal,
                                                                              ),
                                                                              textAlign: TextAlign.center,
                                                                            ),
                                                                          ),
                                                                          Container(
                                                                            width: 80 / 360 * screenWidthTotal,
                                                                            margin: EdgeInsets.only(left: 10 / 360 * screenWidthTotal),
                                                                            child: Text(
                                                                              widget.listofsectionattempts[k-2] == 0?"0%":double.parse((widget.listofsectionaccuracy[k-2]/widget.listofsectionattempts[k-2]*100).toStringAsFixed(2)).toString()+"%",
                                                                              style: TextStyle(
                                                                                fontSize: 12 / 720 * screenHeightTotal,
                                                                                fontFamily: "IBMPlexSans",
                                                                                fontWeight: FontWeight.w500,
                                                                                color: NeutralColors.gunmetal,
                                                                              ),
                                                                              textAlign: TextAlign.center,
                                                                            ),
                                                                          ),
                                                                          Container(
                                                                            width: 90 / 360 * screenWidthTotal,
                                                                            margin: EdgeInsets.only(left: 10 / 360 * screenWidthTotal),
                                                                            child: Text(
                                                                              _printDuration(Duration(seconds:widget.listofsectiontimetaken[k-2])),
                                                                              style: TextStyle(
                                                                                fontSize: 12 / 720 * screenHeightTotal,
                                                                                fontFamily: "IBMPlexSans",
                                                                                fontWeight: FontWeight.w500,
                                                                                color: NeutralColors.gunmetal,
                                                                              ),
                                                                              textAlign: TextAlign.center,
                                                                            ),
                                                                          ),
                                                                          Container(
                                                                            width: 100 / 360 * screenWidthTotal,
                                                                            margin: EdgeInsets.only(left: 10 / 360 * screenWidthTotal),
                                                                            child: Text(
                                                                              //_printDuration(Duration(seconds:listofareatimespentNotNull[j-1]/listofareaattempts[j-1])),
                                                                              widget.listofsectiontimetakenNotNull[k-2] == 0?"00:00":_printDuration(Duration(seconds:(widget.listofsectiontimetakenNotNull[k-2]/widget.listofsectionattempts[k-2]).round().toInt())),
                                                                              style: TextStyle(
                                                                                fontSize: 12 / 720 * screenHeightTotal,
                                                                                fontFamily: "IBMPlexSans",
                                                                                fontWeight: FontWeight.w500,
                                                                                color: NeutralColors.gunmetal,
                                                                              ),
                                                                              textAlign: TextAlign.center,
                                                                            ),
                                                                          ),
                                                                          Container(
                                                                            width: 80 / 360 * screenWidthTotal,
                                                                            margin: EdgeInsets.only(left: 10 / 360 * screenWidthTotal),
                                                                            child: Text(
                                                                              widget.testTemplate == true?"NA":widget.listofsectiontimetaken[k-2] == 0 ?"0.00": double.parse((widget.listofsectionmarks[k-2]/widget.listofsectiontimetaken[k-2]*60).toStringAsFixed(2)).toString(),
                                                                              style: TextStyle(
                                                                                fontSize: 12 / 720 * screenHeightTotal,
                                                                                fontFamily: "IBMPlexSans",
                                                                                fontWeight: FontWeight.w500,
                                                                                color: NeutralColors.gunmetal,
                                                                              ),
                                                                              textAlign: TextAlign.center,
                                                                            ),
                                                                          ),
                                                                        ],
                                                                      ),
                                                                      padding: EdgeInsets.all(10.0),
                                                                    ),
                                                                    body: ListView.builder(
                                                                        physics: NeverScrollableScrollPhysics(),
                                                                        //controller: _scrollController,
                                                                        shrinkWrap: true,
                                                                        itemCount: widget.listofareamarks[k-2].length,
                                                                        itemBuilder: (context, j) {
                                                                          //final item = datas[index];
//                                                                  Color color;
//                                                                  if (j % 2 == 0) {
//                                                                    color = Colors.white;
//                                                                  } else if (j % 2 == 1) {
//                                                                    color = NeutralColors.sun_yellow
//                                                                        .withOpacity(0.1);
//                                                                  }
                                                                          return Container(
                                                                            child: Column(
                                                                              children: <Widget>[
                                                                                Container(
                                                                                  color: color,
                                                                                  child: Row(
                                                                                    mainAxisAlignment:
                                                                                    MainAxisAlignment.start,
                                                                                    children: <Widget>[
                                                                                      Expanded(
                                                                                        child: Container(
                                                                                          //margin: EdgeInsets.only(left: 11 / 360 * screenWidth),
                                                                                          child:
                                                                                          CustomExpansionPanelLists(
                                                                                            expansionHeaderHeight: (50 / 720) * screenHeightTotal,
                                                                                            iconColor:  NeutralColors.blue_grey,
                                                                                            backgroundColor1:  color,
                                                                                            backgroundColor2:  color,
                                                                                            expansionCallback: (int index, bool status) {
                                                                                              setState(() {
                                                                                                activeMeterIndexForSecondExpansion = activeMeterIndexForSecondExpansion == j ? null : j;
                                                                                              });
                                                                                            },
                                                                                            children: [
                                                                                              new ExpansionPanel(
                                                                                                canTapOnHeader: true,
                                                                                                isExpanded: activeMeterIndexForSecondExpansion == j,
                                                                                                headerBuilder: (BuildContext context, bool isExpanded) =>
                                                                                                new Container(
                                                                                                  //color: color,
                                                                                                  height: 50 / 720 * screenHeightTotal,
                                                                                                  child: Row(
                                                                                                    mainAxisAlignment: MainAxisAlignment.start,
                                                                                                    mainAxisSize: MainAxisSize.max,
                                                                                                    children: <Widget>[
                                                                                                      Container(
                                                                                                        width: 70 / 360 * screenWidthTotal,
                                                                                                        margin: EdgeInsets.only(left: 3 / 360 * screenWidthTotal),
                                                                                                        child: Text(
                                                                                                          widget.testTemplate == true?"NA":widget.listofareamarks[k-2][j].toStringAsFixed(2).toString(),
                                                                                                          style: TextStyle(
                                                                                                              fontSize: 12 / 720 * screenHeightTotal,
                                                                                                              fontFamily: "IBMPlexSans",
                                                                                                              color: NeutralColors.gunmetal,
                                                                                                              fontWeight: FontWeight.w500
                                                                                                          ),
                                                                                                          textAlign: TextAlign.center,
                                                                                                        ),
                                                                                                      ),
                                                                                                      Container(
                                                                                                        width: 70 / 360 * screenWidthTotal,
                                                                                                        margin: EdgeInsets.only(left: 10 / 360 * screenWidthTotal),
                                                                                                        child: Text(
                                                                                                          widget.listofareaattempts[k-2][j].toString(),
                                                                                                          style: TextStyle(
                                                                                                            fontSize: 12 / 720 * screenHeightTotal,
                                                                                                            fontFamily: "IBMPlexSans",
                                                                                                            fontWeight: FontWeight.w500,
                                                                                                            color: NeutralColors.gunmetal,
                                                                                                          ),
                                                                                                          textAlign: TextAlign.center,
                                                                                                        ),
                                                                                                      ),
                                                                                                      Container(
                                                                                                        width: 70 / 360 * screenWidthTotal,
                                                                                                        margin: EdgeInsets.only(left: 10 / 360 * screenWidthTotal),
                                                                                                        child: Text(
                                                                                                          widget.listofareaaccuracy[k-2][j].toString(),
                                                                                                          style: TextStyle(
                                                                                                            fontSize: 12 / 720 * screenHeightTotal,
                                                                                                            fontFamily: "IBMPlexSans",
                                                                                                            fontWeight: FontWeight.w500,
                                                                                                            color: NeutralColors.gunmetal,
                                                                                                          ),
                                                                                                          textAlign: TextAlign.center,
                                                                                                        ),
                                                                                                      ),
                                                                                                      Container(
                                                                                                        width: 80 / 360 * screenWidthTotal,
                                                                                                        margin: EdgeInsets.only(
                                                                                                            left: 10 / 360 * screenWidthTotal),
                                                                                                        child: Text(
                                                                                                          widget.listofareaattempts[k-2][j] == 0?"0%":double.parse((widget.listofareaaccuracy[k-2][j]/widget.listofareaattempts[k-2][j]*100).toStringAsFixed(2)).toString()+"%",
                                                                                                          style: TextStyle(
                                                                                                            fontSize: 12 / 720 * screenHeightTotal,
                                                                                                            fontFamily: "IBMPlexSans",
                                                                                                            fontWeight: FontWeight.w500,
                                                                                                            color: NeutralColors.gunmetal,
                                                                                                          ),
                                                                                                          textAlign: TextAlign.center,
                                                                                                        ),
                                                                                                      ),
                                                                                                      Container(
                                                                                                        width: 90 / 360 * screenWidthTotal,
                                                                                                        margin: EdgeInsets.only(left: 10 / 360 * screenWidthTotal),
                                                                                                        child: Text(
                                                                                                          _printDuration(Duration(seconds:widget.listofareatimespent[k-2][j])),
                                                                                                          style: TextStyle(
                                                                                                            fontSize: 12 / 720 * screenHeightTotal,
                                                                                                            fontFamily: "IBMPlexSans",
                                                                                                            fontWeight: FontWeight.w500,
                                                                                                            color: NeutralColors.gunmetal,
                                                                                                          ),
                                                                                                          textAlign: TextAlign.center,
                                                                                                        ),
                                                                                                      ),
                                                                                                      Container(
                                                                                                        width: 100 / 360 * screenWidthTotal,
                                                                                                        margin: EdgeInsets.only(left: 10 / 360 * screenWidthTotal),
                                                                                                        child: Text(
                                                                                                          //_printDuration(Duration(seconds:listofareatimespentNotNull[j-1]/listofareaattempts[j-1])),
                                                                                                          widget.listofareatimespentNotNull[k-2][j] == 0?"00:00":_printDuration(Duration(seconds:(widget.listofareatimespentNotNull[k-2][j]/widget.listofareaattempts[k-2][j]).round())),
                                                                                                          style: TextStyle(
                                                                                                            fontSize: 12 / 720 * screenHeightTotal,
                                                                                                            fontFamily: "IBMPlexSans",
                                                                                                            fontWeight: FontWeight.w500,
                                                                                                            color: NeutralColors.gunmetal,
                                                                                                          ),
                                                                                                          textAlign: TextAlign.center,
                                                                                                        ),
                                                                                                      ),
                                                                                                      Container(
                                                                                                        width: 80 / 360 * screenWidthTotal,
                                                                                                        margin: EdgeInsets.only(left: 10 / 360 * screenWidthTotal),
                                                                                                        child: Text(
                                                                                                          widget.testTemplate == true?"NA":widget.listofareatimespent[k-2][j] == 0 ?"0.00": double.parse((widget.listofareamarks[k-2][j]/widget.listofareatimespent[k-2][j]*60).toStringAsFixed(2)).toString(),
                                                                                                          style: TextStyle(
                                                                                                            fontSize: 12 /720 * screenHeightTotal,
                                                                                                            fontFamily: "IBMPlexSans",
                                                                                                            fontWeight: FontWeight.w500,
                                                                                                            color: NeutralColors.gunmetal,
                                                                                                          ),
                                                                                                          textAlign: TextAlign.center,
                                                                                                        ),
                                                                                                      ),
                                                                                                    ],
                                                                                                  ),
                                                                                                  padding: EdgeInsets.all(10.0),
                                                                                                ),
                                                                                                body: new Container(
                                                                                                  //height: (50 / 720) * screenHeight,
                                                                                                  //color: color,
                                                                                                  child: new ListView.builder(
                                                                                                    shrinkWrap: true,
                                                                                                    physics: NeverScrollableScrollPhysics(),
//                                                                          itemExtent: (50.0 /
//                                                                              720) *
//                                                                              screenHeightTotal,
                                                                                                    itemBuilder: (BuildContext context, int i) {
                                                                                                      return new Container(
                                                                                                        //color: color,
                                                                                                        child: Row(
                                                                                                          children: <Widget>[
                                                                                                            Container(
                                                                                                              width: 70 / 360 * screenWidthTotal,
                                                                                                              margin: EdgeInsets.only(left: 3 / 360 * screenWidthTotal),
                                                                                                              child: Text(
                                                                                                                widget.testTemplate == true?"NA":widget.listoftopicmarks[k-2][j][i].toStringAsFixed(2).toString(),
                                                                                                                style: TextStyle(
                                                                                                                  fontSize: 12 / 720 * screenHeightTotal,
                                                                                                                  fontFamily: "IBMPlexSans",
                                                                                                                  color: NeutralColors.blue_grey,
                                                                                                                ),
                                                                                                                textAlign: TextAlign.center,
                                                                                                              ),
                                                                                                            ),
                                                                                                            Container(
                                                                                                              width: 70 / 360 * screenWidthTotal,
                                                                                                              margin: EdgeInsets.only(left: 10 / 360 * screenWidthTotal),
                                                                                                              child: Text(
                                                                                                                widget.listoftopicattempts[k-2][j][i].toString(),
                                                                                                                style: TextStyle(
                                                                                                                  fontSize: 12 / 720 * screenHeightTotal,
                                                                                                                  fontFamily: "IBMPlexSans",
                                                                                                                  color: NeutralColors.blue_grey,
                                                                                                                ),
                                                                                                                textAlign: TextAlign.center,
                                                                                                              ),
                                                                                                            ),
                                                                                                            Container(
                                                                                                              width: 70 / 360 * screenWidthTotal,
                                                                                                              margin: EdgeInsets.only(left: 10 / 360 * screenWidthTotal),
                                                                                                              child: Text(
                                                                                                                widget.listoftopicaccuracy[k-2][j][i].toString(),
                                                                                                                style: TextStyle(
                                                                                                                  fontSize: 12 / 720 * screenHeightTotal,
                                                                                                                  fontFamily: "IBMPlexSans",
                                                                                                                  color: NeutralColors.blue_grey,
                                                                                                                ),
                                                                                                                textAlign: TextAlign.center,
                                                                                                              ),
                                                                                                            ),
                                                                                                            Container(
                                                                                                              width: 80 / 360 * screenWidthTotal,
                                                                                                              margin: EdgeInsets.only(left: 10 / 360 * screenWidthTotal),
                                                                                                              child: Text(
                                                                                                                widget.listoftopicattempts[k-2][j][i]==0?"0%":double.parse((widget.listoftopicaccuracy[k-2][j][i]/widget.listoftopicattempts[k-2][j][i]*100).toStringAsFixed(2)).toString()+"%",
                                                                                                                style: TextStyle(
                                                                                                                  fontSize: 12 / 720 * screenHeightTotal,
                                                                                                                  fontFamily: "IBMPlexSans",
                                                                                                                  color: NeutralColors.blue_grey,
                                                                                                                ),
                                                                                                                textAlign: TextAlign.center,
                                                                                                              ),
                                                                                                            ),
                                                                                                            Container(
                                                                                                              width: 90 / 360 * screenWidthTotal,
                                                                                                              margin: EdgeInsets.only(left: 10 / 360 * screenWidthTotal),
                                                                                                              child: Text(
                                                                                                                _printDuration(Duration(seconds:widget.listoftopictimespent[k-2][j][i].round())),
                                                                                                                style: TextStyle(
                                                                                                                  fontSize: 12 / 720 * screenHeightTotal,
                                                                                                                  fontFamily: "IBMPlexSans",
                                                                                                                  color: NeutralColors.blue_grey,
                                                                                                                ),
                                                                                                                textAlign: TextAlign.center,
                                                                                                              ),
                                                                                                            ),
                                                                                                            Container(
                                                                                                              width: 100 / 360 * screenWidthTotal,
                                                                                                              margin: EdgeInsets.only(left: 10 / 360 * screenWidthTotal),
                                                                                                              child: Text(
                                                                                                                widget.listoftopictimespentNotNull[k-2][j][i] == 0? "00.00": _printDuration(Duration(seconds: (widget.listoftopictimespentNotNull[k-2][j][i]/widget.listoftopicattempts[k-2][j][i]).round())),
                                                                                                                style: TextStyle(
                                                                                                                  fontSize: 12 / 720 * screenHeightTotal,
                                                                                                                  fontFamily: "IBMPlexSans",
                                                                                                                  color: NeutralColors.blue_grey,
                                                                                                                ),
                                                                                                                textAlign: TextAlign.center,
                                                                                                              ),
                                                                                                            ),
                                                                                                            Container(
                                                                                                              width: 80 / 360 * screenWidthTotal,
                                                                                                              margin: EdgeInsets.only(left: 10 / 360 * screenWidthTotal),
                                                                                                              child: Text(
                                                                                                                widget.testTemplate == true?"NA":widget.listoftopictimespent[k-2][j][i] == 0?"0.00":double.parse((widget.listoftopicmarks[k-2][j][i]/widget.listoftopictimespent[k-2][j][i]*60).toStringAsFixed(2)).toString(),
                                                                                                                style: TextStyle(
                                                                                                                  fontSize: 12 / 720 * screenHeightTotal,
                                                                                                                  fontFamily: "IBMPlexSans",
                                                                                                                  color: NeutralColors.blue_grey,
                                                                                                                ),
                                                                                                                textAlign: TextAlign.center,
                                                                                                              ),
                                                                                                            ),
                                                                                                          ],
                                                                                                        ),
                                                                                                        padding:
                                                                                                        EdgeInsets.all(10.0),
                                                                                                      );
                                                                                                    },
                                                                                                    itemCount: widget.listoftopicmarks[k-2][j].length,
                                                                                                  ),
                                                                                                  // padding: EdgeInsets.all(5.0),
                                                                                                ),
                                                                                              ),
                                                                                            ],
                                                                                          ),
                                                                                        ),
                                                                                      ),
                                                                                    ],
                                                                                  ),
                                                                                ),
                                                                              ],
                                                                            ),
                                                                          );
                                                                        }),
                                                                  ),
                                                                ],
                                                              ),
                                                            ),
                                                          ),
                                                        ],
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              );

                                          }),
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

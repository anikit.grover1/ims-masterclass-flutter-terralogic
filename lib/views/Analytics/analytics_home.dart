import 'package:flutter/material.dart';
import 'package:imsindia/components/bottom_navigation.dart';
import 'package:imsindia/components/navigation_bar.dart';
import 'package:imsindia/utils/colors.dart';
import 'package:flutter/cupertino.dart';
import 'package:imsindia/views/Analytics/analytics_scorecard.dart';
import 'package:imsindia/views/Analytics/analytic_scrorecardImprovement_Analysis.dart';

var screenWidthTotal;
var screenHeightTotal;

var _safeAreaHorizontal;

var _safeAreaVertical;

var screenHeight;

var screenWidth;

class AnalyticsHomeScreen extends StatefulWidget {
  @override
  AnalyticsHomeScreenState createState() => AnalyticsHomeScreenState();
}

class AnalyticsHomeScreenState extends State<AnalyticsHomeScreen> with TickerProviderStateMixin {
  List<Tab> tabList = List();
  int _index;
  int _currentIndex;

  TabController _tabController;
  ScrollController _scroll;
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  _handleTabSelection() {
    setState(() {
      _currentIndex = _tabController.index;
    });
    // entry.overlayEntry.remove();

  }
  @override
  void initState() {
    super.initState();
    _scroll = new ScrollController();
    _tabController = new TabController(vsync: this, length: tabList.length);
    _index = 0;
    tabList.add(new Tab(
      child: Text(
        "Scorecard",
      ),
    ));
    tabList.add(new Tab(
      child: Text(
        "Score Improvement Analysis",
      ),
    ));
    setState(() {
      _tabController = new TabController(vsync: this, length: tabList.length);
      _tabController.addListener(_handleTabSelection);
      this._index = _index;
    });
  }

  @override
  Widget build(BuildContext context) {
    final screenHeight = MediaQuery.of(context).size.height;
    final screenWidth = MediaQuery.of(context).size.width;
    return Scaffold(
      drawer: NavigationDrawer(),
//      bottomNavigationBar: BottomNavigation(),
      appBar: AppBar(
        backgroundColor: Colors.white,
        elevation: 0.0,
        titleSpacing: 0.0,
        brightness: Brightness.light,
        centerTitle: false,
        iconTheme: IconThemeData(
          color: Colors.black,
        ),
        title: Text(
          "Analytics",
          style: TextStyle(
            fontSize: (16 / 720 ) * screenHeight,
            fontFamily: "IBMPlexSans",
            fontWeight: FontWeight.w500,
            color: Colors.black,
          ),
          textAlign: TextAlign.left,
        ),
      ),
      body: Container(
        color: Colors.white,

        child: Column(
          children: <Widget>[
            Container(
              margin: EdgeInsets.only(left:20/360*screenWidth,
                  right: 20/360*screenWidth),
              height: (40/720*screenHeight),
              child: TabBar(
                isScrollable: true,
                controller: _tabController,
                indicatorColor: NeutralColors.purpleish_blue,
                labelColor: NeutralColors.purpleish_blue,
                unselectedLabelColor: NeutralColors.blue_grey,
                indicatorSize: TabBarIndicatorSize.tab,
                tabs: tabList,
                unselectedLabelStyle: TextStyle(
                  fontSize: 14 / 360 * screenWidth,
                  fontFamily: "IBMPlexSans",
                  fontWeight: FontWeight.w400,
                ),
                labelStyle: TextStyle(
                  fontSize: 14 / 360 * screenWidth,
                  fontFamily: "IBMPlexSans",
                  fontWeight: FontWeight.w500,
                ),
                indicator:  UnderlineTabIndicator(
                  borderSide: BorderSide(width: 2.0,color: NeutralColors.purpleish_blue ),
                  insets: EdgeInsets.symmetric(horizontal:0.0),
                ),
              ),
            ),
            Expanded(
              child: TabBarView(
                controller: _tabController,
                children: [
                  AnalyticsScorecard(),
                  AnalyticsScoreImprovement(),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

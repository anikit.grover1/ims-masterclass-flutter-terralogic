import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_email_sender/flutter_email_sender.dart';
import 'package:flutter_svg/svg.dart';
import 'package:imsindia/components/navigation_bar.dart';
import 'package:imsindia/routers/routes.dart';
import 'package:imsindia/utils/colors.dart';
import 'package:imsindia/utils/svg_images/leaderboard_svg_images.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sliding_up_panel/sliding_up_panel.dart';
import 'package:imsindia/utils/eventbooking_svg_icons.dart';

//import 'package:pdf/pdf.dart';
//import 'package:pdf/widgets.dart' as pdf;
//import 'package:printing/printing.dart';

import 'dart:typed_data';
import 'dart:ui' as ui;
import 'package:flutter/rendering.dart';
import 'dart:io';
import 'dart:convert';
import 'package:image/image.dart' as Images;
import 'package:path_provider/path_provider.dart';


import 'event_booking_widget.dart';


class EventData {
  String id;
  String name;
  String email;
  String date;
  String time;
  String test_venu;

  EventData({
    this.id,
    this.name,
    this.email,
    this.date,
    this.time,
    this.test_venu,


  });
}

class Ticket_screen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return TicketScreenWidgetState();
  }
}

class TicketScreenWidgetState extends State<Ticket_screen> {

  

  String date="";
  String nameKey = "_key_name";
  String time="";
  String testVenue="";

  final Eventcontent = EventData(
    id: "676916-WF6FC4",
    name: "Stephen Moses",
    email: "stephen@gmail.com",
    date:"02-07-2019",
    time:  "10:00 AM - 02:00 PM",
    test_venu: "Mother Tekla Auditorium, 143, General KS Thimayya Rd, Craig Park Layout, Ashok Nagar, Bengaluru, Karnataka 560025.",

  );
    
  GlobalKey _globalKey = new GlobalKey();
  static const androidMethodChannel = const MethodChannel('team.native.io/screenshot');


  bool inside = false;
  Uint8List imageInMemory;


  void getDate() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    setState(() {
      date = preferences.getString("nameDate");
      //time = preferences.getString("nameTime");
      //testVenue = preferences.getString("dropDownVal");
    });
  }

void getTime() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    setState(() {
      //date = preferences.getString("nameDate");
      time = preferences.getString("nameTime");
      //testVenue = preferences.getString("dropDownVal");
    });
  }

  Future <Uint8List> _capturePng() async {
    try {
      print('inside');
      inside = true;
      RenderRepaintBoundary boundary =
          _globalKey.currentContext.findRenderObject();
      ui.Image image = await boundary.toImage(pixelRatio: 3.0);
      ByteData byteData =
          await image.toByteData(format: ui.ImageByteFormat.png);
      Uint8List pngBytes = byteData.buffer.asUint8List();
      //String bs64 = base64Encode(pngBytes);
      //print(pngBytes);
      //print(bs64);

      print('png done');
      setState(() {
        imageInMemory = pngBytes;
        inside = false;
//        Printing.layoutPdf(
//            onLayout: buildPdf,
//        );
      });
      return pngBytes;
    } catch (e) {
      print(e);
    }
  }

  Future<void> _capturePngg() {
    return new Future.delayed(const Duration(milliseconds: 20), () async {
    RenderRepaintBoundary boundary =
    _globalKey.currentContext.findRenderObject();
    ui.Image image = await boundary.toImage();
    ByteData byteData =
    await image.toByteData(format: ui.ImageByteFormat.png);
    Uint8List pngBytes = byteData.buffer.asUint8List();
    // print(pngBytes);
    final output = await getExternalStorageDirectory();//
    // use the [path_provider (https://pub.dartlang.org/packages/path_provider) library:
    final file = File("${output.path}/example.png");
    //print('file'+file.toString());
    File myFile= await file.writeAsBytes(pngBytes);

    print('myFile'+myFile.toString());
    await FlutterEmailSender.send(email);


    });
  }

  


   void onYesPressed(BuildContext context) => Navigator.push(context,
      MaterialPageRoute(builder: (context) => EventBookingWidget()));

  final _controller = PanelController();
  double _panelHeightClosed = 0.0;
  final Email email = Email(

    body: 'Email body',
    subject: 'Email subject',
    recipients: ['example@example.com'],
    cc: ['cc@example.com'],
    bcc: ['bcc@example.com'],
    //attachmentPath: '/path/to/attachment.zip',
    
    attachmentPaths: ['/storage/emulated/0/Android/data/com.india.ims.imsindia/files/example.png'],


  );

  @override
  void initState() {
    super.initState();
    getDate();
    getTime();
    print('******************date'+date);
  }

  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      systemNavigationBarColor: Colors.grey[200],
      systemNavigationBarIconBrightness: Brightness.dark,
      systemNavigationBarDividerColor: Colors.black,
    ));
    final screenHeight = MediaQuery.of(context).size.height;
    final screenWidth = MediaQuery.of(context).size.width;

    return Scaffold(
     // drawer:NavigationDrawer(), //this will just add the Navigation Drawer Icon
      appBar: new AppBar(
        brightness: Brightness.light,
        elevation: 0.0,
        centerTitle: false,
        titleSpacing: 0.0,
        iconTheme: IconThemeData(
          color: Colors.black, //change your color here
        ),
        backgroundColor: Color.fromARGB(255, 255, 255, 255),
        title: Text(
          "Event Bookings",
          style: TextStyle(
            color: Color.fromARGB(255, 0, 0, 0),
            fontSize: screenHeight * 16 / 720,
            fontFamily: "IBMPlexSans",
            fontWeight: FontWeight.w500,
          ),
          textAlign: TextAlign.left,
        ),
        leading:  GestureDetector(
                  onTap: (){
                    Navigator.pop(context);
                  },
                  child: Container(
                          color: Colors.transparent,
                          width: 28 / 360 * screenWidth,
                          height: 24 / 720 * screenHeight,
                          child: SvgPicture.asset(
                            LeaderboardAssets.back_icon,
                            fit: BoxFit.scaleDown,
                          ),
                        ),
        ),
      ),

      body: Container(
        color: Colors.white,
        child: SlidingUpPanel(
           padding: EdgeInsets.all(0),
          maxHeight: screenHeight / 2,
          minHeight: _panelHeightClosed,
          parallaxEnabled: false,
          parallaxOffset: .5,
          //defaultPanelState: PanelState.OPEN,
          isDraggable: true,
          body: _body(),
          panel: _panel(),
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(25.0), topRight: Radius.circular(25.0)),
          //onPanelSlide: (double pos) => setState((){
          //_fabHeight =  _initFabHeight/2;
          //}),
          controller: _controller,
        ),
      ),
    );
  }

  Widget _body() {
    final screenHeight = MediaQuery.of(context).size.height;
    final screenWidth = MediaQuery.of(context).size.width;
    final defaultScreenHeight = 720;
    final defaultScreenWidth = 360;
    return SingleChildScrollView(
      child: Container(
        //height: screenHeight * 1020 / 700,
        //constraints: BoxConstraints.expand(),
        decoration: BoxDecoration(
          color: Color.fromARGB(255, 255, 255, 255),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
           
            
            _ticket(),

            Container(
              margin: EdgeInsets.only(left: 20 / defaultScreenWidth * screenWidth, top: 10 / defaultScreenHeight * screenHeight, right: 20 / defaultScreenWidth * screenWidth),
              decoration: BoxDecoration(
                color: Color.fromARGB(255, 242, 246, 248),
                borderRadius: BorderRadius.all(Radius.circular(5)),
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  Align(
                    alignment: Alignment.topLeft,
                    child: Container(
                  //    height: 22/defaultScreenHeight*screenHeight,
                      margin: EdgeInsets.only(left: 15 / defaultScreenWidth * screenWidth, top: 14 / defaultScreenHeight * screenHeight),
                      child: Text(
                        "Important Instructions",
                        style: TextStyle(
                          color: Color.fromARGB(255, 0, 3, 44),
                          fontSize: screenWidth * 12 / 360,
                          fontFamily: "IBMPlexSans",
                          fontWeight: FontWeight.w500,
                        ),
                        textAlign: TextAlign.left,
                      ),
                    ),
                  ),
                  Container(
                //    height: 54/defaultScreenHeight*screenHeight,
                    margin: EdgeInsets.only(left: 15 / defaultScreenWidth * screenWidth, top: 5/ defaultScreenHeight * screenHeight, right: 15/ defaultScreenWidth * screenWidth),
                    child: Text(
                      "Kindly reach the venue 10 minutes before the slot timing. Students who arrive late will not be allowed to enter.",
                      style: TextStyle(
                        color: Color.fromARGB(255, 87, 93, 96),
                        fontSize: screenWidth * 12 / 360,
                        fontFamily: "IBMPlexSans",
                      ),
                      textAlign: TextAlign.left,
                    ),
                  ),
                  Container(
                 //   height: 36/defaultScreenHeight*screenHeight,
                    margin: EdgeInsets.only(left: 15/ defaultScreenWidth * screenWidth, top: 10/ defaultScreenHeight * screenHeight, right: 32/ defaultScreenWidth * screenWidth),
                    child: Text(
                      "Ensure that your mobile is switched off and silence is maintained.",
                      style: TextStyle(
                        color: Color.fromARGB(255, 87, 93, 96),
                        fontSize: screenWidth * 12 / 360,
                        fontFamily: "IBMPlexSans",
                      ),
                      textAlign: TextAlign.left,
                    ),
                  ),
                  Container(
                //    height: 36/defaultScreenHeight*screenHeight,
                    margin: EdgeInsets.only(left: 15/ defaultScreenWidth * screenWidth, top: 10/ defaultScreenHeight * screenHeight, right: 15/ defaultScreenWidth * screenWidth),
                    child: Text(
                      "Please carry a prinout of the registration confirmation along with your IMS I-Card.",
                      style: TextStyle(
                        color: Color.fromARGB(255, 87, 93, 96),
                        fontSize: screenWidth * 12 / 360,
                        fontFamily: "IBMPlexSans",
                      ),
                      textAlign: TextAlign.left,
                    ),
                  ),
                  Container(
                 //   height: 36/defaultScreenHeight*screenHeight,
                    margin: EdgeInsets.only(left: 15 / defaultScreenWidth * screenWidth, top: 10 / defaultScreenHeight * screenHeight, right: 15  / defaultScreenWidth * screenWidth) ,
                    child: Text(
                      "Once a booking is confirmed, you can change it only after cancelling the existing booking.",
                      style: TextStyle(
                        color: Color.fromARGB(255, 87, 93, 96),
                        fontSize: screenWidth * 12 / 360,
                        fontFamily: "IBMPlexSans",
                      ),
                      textAlign: TextAlign.left,
                    ),
                  ),
                  Container(
                    color: NeutralColors.sun_yellow.withOpacity(0.5),
                    margin: EdgeInsets.only( top: 10 / defaultScreenHeight * screenHeight) ,
                    padding: EdgeInsets.only(left: 15 / defaultScreenWidth * screenWidth,right: 15 / defaultScreenWidth * screenWidth,top: 7 / defaultScreenHeight * screenHeight,bottom: 7 / defaultScreenHeight * screenHeight),
                    child: Text(
                            "The link for taking the test will be made available here on the test date, 11:00 AM onwards",
                            style: TextStyle(
                              color: Color.fromARGB(255, 0, 3, 44),
                              fontSize: screenWidth * 12 / 360,
                              fontFamily: "IBMPlexSans",
                            ),
                            textAlign: TextAlign.left,
                          ),
                  ),
                  Container(
                    margin: EdgeInsets.only(left: 15 / defaultScreenWidth * screenWidth, top: 10 / defaultScreenHeight * screenHeight),
                    child: Text(
                      "Cancellation History",
                      style: TextStyle(
                        color: Color.fromARGB(255, 0, 3, 44),
                        fontSize: screenWidth * 12 / 360,
                        fontFamily: "IBMPlexSans",
                        fontWeight: FontWeight.w500,
                      ),
                      textAlign: TextAlign.left,
                    ),
                  ),
                  Container(
                  // height: 40/defaultScreenHeight*screenHeight,
                    margin: EdgeInsets.only(left: 15 / defaultScreenWidth * screenWidth, top: 10 / defaultScreenHeight * screenHeight, right: 26 / defaultScreenWidth * screenWidth,bottom: 10 / defaultScreenHeight * screenHeight),
                    child: Text(
                      "No booking cancellations found for Last Mile to CAT Bangalore 2018",
                      style: TextStyle(
                        color: Color.fromARGB(255, 87, 93, 96),
                        fontSize: screenWidth * 12 / 360,
                        fontFamily: "IBMPlexSans",
                      ),
                      textAlign: TextAlign.left,
                    ),
                  ),
                ],
              ),
            ),
            Container(
              height: screenHeight * 200 / 720,
              margin: EdgeInsets.only(left: 43 / defaultScreenWidth * screenWidth, top: 40 / defaultScreenHeight * screenHeight, right: 42 / defaultScreenWidth * screenWidth),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Row(
                    children: [
                      Container(
                        width: screenWidth * 130 / 360,
                        height: screenHeight * 40 / 720,

                        child: FlatButton(
                          onPressed: () {
                            _capturePng();
                          },
                          color: Color.fromARGB(255, 0, 171, 251),
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.all(Radius.circular(3)),
                          ),
                          textColor: Color.fromARGB(255, 255, 255, 255),
                          padding: EdgeInsets.all(0),
                          child: Text(
                            "Print Ticket",
                            style: TextStyle(
                              fontSize: screenHeight * 14 / 720,
                              fontFamily: "IBMPlexSans",
                              fontWeight: FontWeight.w500,
                            ),
                            textAlign: TextAlign.center,
                          ),
                        ),
                      ),
                      Spacer(),
                      Container(
                        width: screenWidth * 130 / 360,
                        height: screenHeight * 40 / 720,
                        //margin: EdgeInsets.only(top: 40/defaultScreenHeight*screenHeight),
                        child: FlatButton(
                          onPressed: () async {
                           // _capturePngEmail();
                           //_screenShot();
                           _capturePngg();
                            //await FlutterEmailSender.send(email);

                            },
                          color: Color.fromARGB(255, 0, 171, 251),
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.all(Radius.circular(2)),
                          ),
                          textColor: Color.fromARGB(255, 255, 255, 255),
                          padding: EdgeInsets.all(0),
                          child: Text(
                            "Email Ticket",
                            style: TextStyle(
                              fontSize: screenHeight * 14 / 720,
                              fontFamily: "IBMPlexSans",
                              fontWeight: FontWeight.w500,
                            ),
                            textAlign: TextAlign.center,
                          ),
                        ),
                      ),
                    ],
                  ),
                  Container(
                  width: screenWidth * 125 / 360,
              height: screenHeight * 28 / 720,
              margin: EdgeInsets.only(
                  top: 24 / defaultScreenHeight * screenHeight),
              child: FlatButton(
                onPressed: () {
                  _controller.open();
                },
                color: Colors.transparent,
                textColor: Color.fromARGB(255, 126, 145, 154),
                padding: EdgeInsets.all(0),
                child: Text(
                  "Cancel Booking",
                  style: TextStyle(
                    fontSize: screenHeight * 14 / 720,
                    fontFamily: "IBMPlexSans",
                  ),
                  textAlign: TextAlign.center,
                ),
              ),
            ),
                ],
              ),
            ),
            
          ],
        ),
      ),
    );
  }

//  List<int> buildPdf(PdfPageFormat format) {
//    final pdf.Document doc = pdf.Document();
//
//    final img = Images.decodeImage(imageInMemory);
//final image = PdfImage(
//  doc.document,
//  image: img.data.buffer.asUint8List(),
//  width: img.width,
//  height: img.height,
//);
//    print("enter pdf");
//    doc.addPage(
//      pdf.Page(
//        pageFormat: format,
//        build: (pdf.Context context) {
//          return pdf.ConstrainedBox(
//            constraints: const pdf.BoxConstraints.expand(),
//            child: pdf.FittedBox(
//              child: pdf.Container(
//               child: pdf.Image(image)
//              ),
//            ),
//          );
//        },
//      ),
//    );
//
//    return doc.save();
//  }
  Future<Null> _screenShot() async{
    try{
      RenderRepaintBoundary boundary = _globalKey.currentContext.findRenderObject();
      ui.Image image = await boundary.toImage();
      final directory = (await getExternalStorageDirectory()).path;
      ByteData byteData = await image.toByteData(format: ui.ImageByteFormat.png);
      Uint8List pngBytes = byteData.buffer.asUint8List();
      File imgFile =new File('$directory/screenshot.png');
      imgFile.writeAsBytes(pngBytes);
      Uri fileURI= new Uri.file('$directory/screenshot.png');
      print('Screenshot Path:'+imgFile.path);
      await androidMethodChannel.invokeMethod('takeScreenshot',{'filePath':imgFile.path});
    }
    on PlatformException catch(e){
      print("Exception while taking screenshot:"+e.toString());
    }
  }

  Widget _ticket(){
    final screenHeight = MediaQuery.of(context).size.height;
    final screenWidth = MediaQuery.of(context).size.width;
    final defaultScreenHeight = 720;
    final defaultScreenWidth = 360;
   return RepaintBoundary(
           key: _globalKey,

        child: Container(
                color: Colors.white,
                //margin: EdgeInsets.only(left: 20/defaultScreenWidth*screenWidth, top: 20/defaultScreenHeight*screenHeight, right: 20/defaultScreenWidth*screenWidth),
                padding: EdgeInsets.only(left: 20/defaultScreenWidth*screenWidth, top: 10/defaultScreenHeight*screenHeight, right: 20/defaultScreenWidth*screenWidth,bottom: 10/defaultScreenHeight*screenHeight),
                child: DottedBorder(
                  color: Color.fromARGB(77, 87, 93, 96),
                  radius: Radius.circular(5),
                  strokeWidth: 1,
                  borderType: BorderType.RRect,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      Container(
                        margin: EdgeInsets.only(left: 15/defaultScreenWidth*screenWidth, top: 17.5/defaultScreenHeight*screenHeight),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          children: [
                            Align(
                              alignment: Alignment.topLeft,
                              child: Text(
                                "Admit Card Token ID",
                                style: TextStyle(
                                  color: Color.fromARGB(255, 153, 154, 171),
                                  fontSize: screenWidth * 12 / 360,
                                  fontFamily: "IBMPlexSans",
                                  fontWeight: FontWeight.w500,
                                ),
                                textAlign: TextAlign.left,
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.only(top: 4/defaultScreenHeight*screenHeight),
                              child: Text(
                                Eventcontent.id,
                                style: TextStyle(
                                  color: Color.fromARGB(255, 0, 3, 44),
                                  fontSize: screenWidth * 14 / 360,
                                  fontFamily: "IBMPlexSans",
                                  fontWeight: FontWeight.w500,
                                ),
                                textAlign: TextAlign.left,
                              ),
                            ),
                          ],
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(left: 15/defaultScreenWidth*screenWidth, top: 20/defaultScreenHeight*screenHeight),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          children: [
                            Align(
                              alignment: Alignment.topLeft,
                              child: Text(
                                "Candidate Name",
                                style: TextStyle(
                                  color: Color.fromARGB(255, 153, 154, 171),
                                  fontSize: screenWidth * 12 / 360,
                                  fontFamily: "IBMPlexSans",
                                  fontWeight: FontWeight.w500,
                                ),
                                textAlign: TextAlign.left,
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.only(top: 4/defaultScreenHeight*screenHeight),
                              child: Text(
                                Eventcontent.name,
                                style: TextStyle(
                                  color: Color.fromARGB(255, 0, 3, 44),
                                  fontSize: screenWidth * 14 / 360,
                                  fontFamily: "IBMPlexSans",
                                  fontWeight: FontWeight.w500,
                                ),
                                textAlign: TextAlign.left,
                              ),
                            ),
                          ],
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(left: 15/defaultScreenWidth*screenWidth, top: 20/defaultScreenHeight*screenHeight),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          children: [
                            Align(
                              alignment: Alignment.topLeft,
                              child: Text(
                                "Candidate Email",
                                style: TextStyle(
                                  color: Color.fromARGB(255, 153, 154, 171),
                                  fontSize: screenWidth * 12 / 360,
                                  fontFamily: "IBMPlexSans",
                                  fontWeight: FontWeight.w500,
                                ),
                                textAlign: TextAlign.left,
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.only(top: 4/defaultScreenHeight*screenHeight),
                              child: Text(
                                Eventcontent.email,
                                style: TextStyle(
                                  color: Color.fromARGB(255, 0, 3, 44),
                                  fontSize: screenWidth * 14 / 360,
                                  fontFamily: "IBMPlexSans",
                                  fontWeight: FontWeight.w500,
                                ),
                                textAlign: TextAlign.left,
                              ),
                            ),
                          ],
                        ),
                      ),

                      Container(
                        margin: EdgeInsets.only(left: 15/defaultScreenWidth*screenWidth, top: 20/defaultScreenHeight*screenHeight,right: 15/defaultScreenWidth*screenWidth,),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          children: [
                            Align(
                              alignment: Alignment.topRight,
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  Expanded(
                                      child: Container(
                                      child: Text(
                                        "Date",
                                        style: TextStyle(
                                          color: Color.fromARGB(255, 153, 154, 171),
                                          fontSize: screenWidth * 12 / 360,
                                          fontFamily: "IBMPlexSans",
                                          fontWeight: FontWeight.w500,
                                        ),
                                        textAlign: TextAlign.left,
                                      ),
                                    ),


                                  ),



                                  Expanded(
                                      child: Container(
                                      child: Text(
                                        "Time",
                                        style: TextStyle(
                                          color: Color.fromARGB(255, 153, 154, 171),
                                          fontSize: screenWidth * 12 / 360,
                                          fontFamily: "IBMPlexSans",
                                          fontWeight: FontWeight.w500,
                                        ),
                                        textAlign: TextAlign.left,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.only(top: 4/defaultScreenHeight*screenHeight),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                children: <Widget>[
                                  Expanded(
                                      child: Container(
                                      child: Text(
                                        date,
                                        style: TextStyle(
                                          color: Color.fromARGB(255, 0, 3, 44),
                                          fontSize: screenWidth * 14 / 360,
                                          fontFamily: "IBMPlexSans",
                                          fontWeight: FontWeight.w500,
                                        ),
                                        textAlign: TextAlign.left,
                                      ),
                                    ),
                                  ),
                                  Expanded(
                                      child: Container(
                                      child: Text(
                                        time,
                                        style: TextStyle(
                                          color: Color.fromARGB(255, 0, 3, 44),
                                          fontSize: screenWidth * 14 / 360,
                                          fontFamily: "IBMPlexSans",
                                          fontWeight: FontWeight.w500,
                                        ),
                                        textAlign: TextAlign.left,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),

                      
                      Container(
                        margin: EdgeInsets.only(left: 15/defaultScreenWidth*screenWidth, top: 20/defaultScreenHeight*screenHeight, right: 15/defaultScreenWidth*screenWidth, bottom: 15/defaultScreenWidth*screenWidth),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          children: [
                            Align(
                              alignment: Alignment.topLeft,
                              child: Text(
                                "Test Venue",
                                style: TextStyle(
                                  color: Color.fromARGB(255, 153, 154, 171),
                                  fontSize: screenWidth * 12 / 360,
                                  fontFamily: "IBMPlexSans",
                                  fontWeight: FontWeight.w500,
                                ),
                                textAlign: TextAlign.left,
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.only(top: 4/defaultScreenHeight*screenHeight),
                              child: Text(
                                Eventcontent.test_venu,
                                style: TextStyle(
                                  color: Color.fromARGB(255, 0, 3, 44),
                                  fontSize: screenWidth * 14 / 360,
                                  fontFamily: "IBMPlexSans",
                                  fontWeight: FontWeight.w500,
                                ),
                                textAlign: TextAlign.left,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
   );
  }

  Widget _panel() {
    final screenHeight = MediaQuery.of(context).size.height;
    final screenWidth = MediaQuery.of(context).size.width;
    final defaultScreenHeight = 720;
    final defaultScreenWidth = 360;
    return Container(
      height: screenHeight * 265 / 720,
      margin: EdgeInsets.only(bottom: 18 / defaultScreenHeight * screenHeight),
      //color: Colors.black,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.end,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Align(
            alignment: Alignment.topCenter,
            child: Container(
              width: screenWidth * 140 / 360,
              height: screenHeight * 130 / 720,
              margin: EdgeInsets.only(bottom: 18 / defaultScreenHeight * screenHeight),
              child: new SvgPicture.asset(
                getSvgIcon.cancelbooking,
                fit: BoxFit.contain,
                //color: Colors.red,
              ),
            ),
          ),
          
          Align(
            alignment: Alignment.topCenter,
            child: Container(
              margin: EdgeInsets.only(bottom: 32 / defaultScreenHeight * screenHeight),
              child: Text(
                "Are you sure you want\nto cancel booking?",
                style: TextStyle(
                  color: Color.fromARGB(255, 0, 0, 0),
                  fontSize: screenHeight * 16 / 720,
                  fontFamily: "IBMPlexSans",
                  fontWeight: FontWeight.w500,
                ),
                textAlign: TextAlign.center,
              ),
            ),
          ),
          Container(
            height: screenHeight * 40 / 720,
            margin: EdgeInsets.only(left: 43 / defaultScreenWidth * screenWidth, bottom: 40/ defaultScreenHeight * screenHeight, right: 42 / defaultScreenWidth * screenWidth),
            child: Row(
              children: [
                Container(
                  width: screenWidth * 130 / 360,
                  height: screenHeight * 40 / 720,
                  child: FlatButton(
                    onPressed: () {
                      _controller.close();
                     },
                    color: Color.fromARGB(255, 242, 244, 244),
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(Radius.circular(3)),
                    ),
                    textColor: Color.fromARGB(255, 126, 145, 154),
                    padding: EdgeInsets.all(0),
                    child: Text(
                      "No",
                      style: TextStyle(
                        fontSize: screenHeight * 14 / 720,
                        fontFamily: "IBMPlexSans",
                        fontWeight: FontWeight.w500,
                      ),
                      textAlign: TextAlign.center,
                    ),
                  ),
                ),
                Spacer(),
                Container(
                  width: screenWidth * 130 / 360,
                  height: screenHeight * 40 / 720,
                  child: FlatButton(
                    onPressed: () {
                      _controller.close();
                    //  AppRoutes.pop(context);
                     // Navigator.of(context).popUntil(ModalRoute.withName(Navigator.defaultRouteName));
                   //   Navigator.popUntil(context, ModalRoute.withName('/EventBookingWidget'));
                    int count = 0;
                      Navigator.popUntil(context, (route) {
                        return count++ == 2;
                      }
                      );
                    },
                    color: Color.fromARGB(255, 0, 171, 251),
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(Radius.circular(2)),
                    ),
                    textColor: Color.fromARGB(255, 255, 255, 255),
                    padding: EdgeInsets.all(0),
                    child: Text(
                      "Yes",
                      style: TextStyle(
                        fontSize: screenHeight * 14 / 720,
                        fontFamily: "IBMPlexSans",
                        fontWeight: FontWeight.w500,
                      ),
                      textAlign: TextAlign.center,
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}

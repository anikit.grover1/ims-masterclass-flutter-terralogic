import 'package:flutter/material.dart';
import 'package:imsindia/components/bottom_navigation.dart';
import 'package:imsindia/views/event_booking_pages/event_booking_two_widget.dart';
import 'package:imsindia/components/navigation_bar.dart';

import 'package:imsindia/components/primary_button_gradient.dart';

class Data {
  String date;
  String title;
  String description;
  String timeslot;
  Data({
    this.date,
    this.title,
    this.description,
    this.timeslot,
  });
}

class EventBookingWidget extends StatelessWidget {
  void onPressed(BuildContext context,title) => Navigator.push(context,
      MaterialPageRoute(builder: (context) => EventBookingTwoWidget(title)));

  @override
  Widget build(BuildContext context) {
    final screenHeight = MediaQuery.of(context).size.height;
    final screenWidth = MediaQuery.of(context).size.width;
    final defaultScreenHeight = 720.0;
    final defaultScreenWidth = 360.0;

    final event1 = Data(
      date: "20 APRIL 2018",
      title: "Last Mile to CAT Bangalore 2018",
      description:
          "As soon as Computerized Tomography or CT scans became accessible in the 1970s, they reformed the practice of neurology.",
      timeslot: "12:30 AM - 01:15 PM",
    );

    final event2 = Data(
      date: "20 APRIL 2018",
      title: "Second Mile to CAT Bangalore 2018",
      description:
          "As soon as Computerized Tomography or CT scans became accessible",
      timeslot: "12:30 AM - 01:15 PM",
    );

    final event3 = Data(
      date: "20 APRIL 2018",
      title: "Third Mile to CAT Bangalore 2018",
      description:
          "As soon as Computerized Tomography or CT scans became accessible",
      timeslot: "12:30 AM - 01:15 PM",
    );

    final List<Data> events = [
      event1,
      event2,
      event3,
    ];

    return Scaffold(
      appBar: new AppBar(
        brightness: Brightness.light,
        elevation: 0.0,
        centerTitle: false,
        iconTheme: IconThemeData(
          color: Colors.black, //change your color here
        ),
        backgroundColor: Color.fromARGB(255, 255, 255, 255),
        titleSpacing: 0.0,
        title: Text(
          "Event Bookings",
          style: TextStyle(
            color: Color.fromARGB(255, 0, 0, 0),
            fontSize: screenWidth * 16 / 360,
            fontFamily: "IBMPlexSans",
            fontWeight: FontWeight.w500,
          ),
          textAlign: TextAlign.left,
        ),
      ),
      drawer: NavigationDrawer(),
//      bottomNavigationBar: BottomNavigation(),
      body: Container(
        constraints: BoxConstraints.expand(),
        decoration: BoxDecoration(
          color: Color.fromARGB(255, 255, 255, 255),
          boxShadow: [
            BoxShadow(
              blurRadius: 13.0,
              color: Colors.black.withOpacity(.5),
              offset: Offset(6.0, 7.0),
              // spreadRadius: 500.0,
            ),
          ],
          //shape: BoxShape.rectangle,
          //border: Border.all(),
         // color: Colors.white,
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Align(
              alignment: Alignment.topLeft,
              child: Container(
                margin: EdgeInsets.only(left: 20 /defaultScreenWidth * screenWidth, top: 20 / defaultScreenHeight * screenHeight,bottom: 10 / defaultScreenHeight * screenHeight,),
                child: Text(
                  "Upcoming Events",
                  style: TextStyle(
                    color: Color.fromARGB(255, 0, 3, 44),
                    fontSize: screenWidth * 18 / 360,
                    fontFamily: "IBMPlexSans",
                    fontWeight: FontWeight.w700,
                  ),
                  textAlign: TextAlign.left,
                ),
              ),
            ),

            new Expanded(
              child: ListView.builder(
                itemBuilder: (build, index) {
                  final item = events[index];
                  return Container(
                //    height: 253 / defaultScreenHeight * screenHeight,
                    margin: EdgeInsets.only(left: 20/defaultScreenWidth * screenWidth, top: 10 / 720 * screenHeight, right: 20 / defaultScreenWidth * screenWidth,
                    bottom: index == events.length-1 ? 17 / 720 * screenHeight : 0,
                    ),
                    decoration: BoxDecoration(
                      color: Color.fromARGB(255, 255, 255, 255),
                      boxShadow: [
                        BoxShadow(
                          color: Color.fromARGB(255, 234, 234, 234),
                          offset: Offset(0, 0),
                          blurRadius: 10,
                        ),
                      ],
                      borderRadius: BorderRadius.all(Radius.circular(5)),
                    ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: [
                        Align(
                          alignment: Alignment.topLeft,
                          child: Container(
                            margin: EdgeInsets.only(left: 25 / defaultScreenWidth * screenWidth, top: 20 / defaultScreenHeight * screenHeight),
                            child: Text(
                              item.date,
                              style: TextStyle(
                                color: Color.fromARGB(255, 153, 154, 171),
                                fontSize: 12 / defaultScreenWidth * screenWidth,
                                fontFamily: "IBMPlexSans",
                                fontWeight: FontWeight.w500,
                              ),
                              textAlign: TextAlign.left,
                            ),
                          ),
                        ),
                        Align(
                          alignment: Alignment.topLeft,
                          child: Container(
                            margin: EdgeInsets.only(left: 25 / defaultScreenWidth * screenWidth, top: 8 / defaultScreenHeight * screenHeight),
                            child: Text(
                              item.title,
                              style: TextStyle(
                                color: Color.fromARGB(255, 0, 3, 44),
                                fontSize: 16 / defaultScreenWidth * screenWidth,
                                fontFamily: "IBMPlexSans",
                                fontWeight: FontWeight.w500,
                              ),
                              textAlign: TextAlign.left,
                            ),
                          ),
                        ),
                        Align(
                          alignment: Alignment.topLeft,
                          child: Container(
                            margin: EdgeInsets.only(left: 25 / defaultScreenWidth * screenWidth, top: 5 / defaultScreenHeight * screenHeight,right: 25/ defaultScreenWidth * screenWidth),
                            child: Text(
                              item.description,
                              style: TextStyle(
                                color: Color.fromARGB(255, 153, 154, 171),
                                fontSize: 14 / defaultScreenWidth * screenWidth,
                                fontFamily: "IBMPlexSans",
                                fontWeight: FontWeight.w400,
                              ),
                              textAlign: TextAlign.left,
                            ),
                          ),
                        ),
                        Align(
                          alignment: Alignment.topLeft,
                          child: Container(
                            margin: EdgeInsets.only(left: 25  / defaultScreenWidth * screenWidth, top: 10 / defaultScreenHeight * screenHeight),
                            child: Text(
                              item.timeslot,
                              style: TextStyle(
                                color: Color.fromARGB(255, 0, 3, 44),
                                fontSize: 14 / defaultScreenWidth * screenWidth,
                                fontFamily: "IBMPlexSans",
                                fontWeight: FontWeight.w500,
                              ),
                              textAlign: TextAlign.left,
                            ),
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(left: 25 / defaultScreenWidth * screenWidth, top: 19 / defaultScreenHeight * screenHeight, right: 25/defaultScreenWidth * screenWidth,bottom: 19 / defaultScreenHeight * screenHeight),
                          child: PrimaryButtonGradient(
                            onTap: () => this.onPressed(context,item.title),
                            text: "BOOK SLOT",
                          ),
                        ),
                      ],
                    ),
                  );
                },
                itemCount: events.length,
                scrollDirection: Axis.vertical,
              ),
            ),
            
          ],
        ),
      ),
    );
  }
}

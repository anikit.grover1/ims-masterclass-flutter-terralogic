import 'package:flutter/material.dart';
import 'package:imsindia/components/primary_button_gradient.dart';
import 'package:imsindia/components/primary_button_without_gradient.dart';
import 'package:imsindia/components/secondary_Button.dart';
import 'package:imsindia/routers/routes.dart';
import 'package:imsindia/components/primary_Button.dart';
import 'package:imsindia/resources/strings/login.dart';
import 'package:imsindia/routers/routes.dart';
import 'package:imsindia/utils/svg_icons.dart';
import 'package:imsindia/utils/validator.dart';
import 'package:imsindia/views/login_page/login_enrollmentid_widget.dart';
import '../../components/Input_forms.dart';
import 'package:imsindia/views/login_page/login_welcome_screen.dart';
import 'package:imsindia/views/login_page/login_gender_widget.dart';
import 'package:imsindia/utils/text_styles.dart';
import 'package:imsindia/utils/colors.dart';

class LoginCreateaccountfilledWidget extends StatefulWidget {
  @override
  LoginCreateaccountfilledWidgetState createState() =>
      LoginCreateaccountfilledWidgetState();
}
final TextEditingController _ims_pin_controller =  TextEditingController();
final TextEditingController _ims_password_controller = new TextEditingController();
String get imsPin => _ims_pin_controller.text;
String get imsPassword => _ims_password_controller.text;
bool imsPinValidator = false;
bool imsPasswordValidator = false;

class LoginCreateaccountfilledWidgetState
    extends State<LoginCreateaccountfilledWidget> {
  void onGroup3Pressed(BuildContext context) => Navigator.push(
      context,
      MaterialPageRoute(
          builder: (context) => LoginCreateaccountfilledWidget()));
  void initState() {
    super.initState();
    setState(() {
      _ims_pin_controller.text = '';
      _ims_password_controller.text = '';
    });
    _ims_pin_controller.addListener(() {
      setState(() {
        validateIMSPin(imsPin);
      });
    });
    _ims_password_controller.addListener(() {
      setState(() {
        validateIMSPassword(imsPassword);
      });
    });
  }
  final focus = FocusNode();
  bool _obscureText = true;
  static String validateIMSPin(String value){
    if(value.isEmpty && value.length==0){
      imsPinValidator=false;

      return null;
    }
    //Pattern pattern = r'^[0-9][0-9]{13}$';
    //RegExp regex = new RegExp(pattern);
    if (value.length<13) {
      imsPinValidator=false;

      return LoginUsingIMSPin.invalidIMSPasswordForLogin;
    }
     else{
      imsPinValidator=true;

      return null;
    }
  }
  static String validateIMSPassword(String value){
    if(value.isEmpty && value.length==0){
      imsPasswordValidator=false;

      return null;
    }else if(value.length<6){
      imsPasswordValidator=false;
      return LoginUsingIMSPin.invalidIMSPassword;
    }
    else{
      imsPasswordValidator=true;

      return null;
    }
  }


  void _toggle() {
    setState(() {
      _obscureText = !_obscureText;
    });
  }

  @override
  Widget build(BuildContext context) {
    final screenHeight = MediaQuery.of(context).size.height;
    final screenWidth = MediaQuery.of(context).size.width;
    Future<bool> _onWillPop() {
      AppRoutes.push(context, LoginEnrollmentidWidget());
    }

    return WillPopScope(
      onWillPop: _onWillPop,
      child: Scaffold(
        body: GestureDetector(
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: Container(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  Container(
                    height: screenHeight*0.140,
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: [
                        GestureDetector(
                          onTap: (){
                            AppRoutes.push(context, LoginEnrollmentidWidget());
                          },
                          child: Container(
                            child: Padding(
                              padding: const EdgeInsets.all(5.0),
                              child: Align(
                                alignment: Alignment.topLeft,
                                child: Container(
                                  width: (20/360)*screenWidth,
                                  height: (20/720)*screenHeight,
                                  margin: EdgeInsets.only(top: screenHeight*0.0738,left: screenWidth*0.062,right: 5),
                                  child: getSvgIcon.backSvgIcon,
                                ),
                              ),
                            ),
                          ),
                        ),
                        Spacer(),
                        Align(
                          alignment: Alignment.topRight,
                          child: Container(
                            //   color: Color.fromRGBO(255, 45, 52, 56),

                            width: screenWidth*0.780,
                            height: screenHeight*0.9,
                            child: getSvgIcon.combinedshapeSvgIcon,
                          ),
                        ),
                      ],
                    ),
                  ),
                  Expanded(
                    child: SingleChildScrollView(
                      child: Container(
                        child: Column(
                          children: <Widget>[
                            Padding(
                              padding: EdgeInsets.only(
                                  left: screenWidth * 40 / 360,
                                  top: screenHeight * 43 / 720,
                                  right: screenWidth * 40 / 360),
                              child: Center(
                                child: Text(
                                  "Hi, there!\nWe’ve been expecting you.",
                                  style: TextStyle(
                                    color: Color.fromARGB(255, 45, 52, 56),
                                    fontSize: 21 / 720 * screenHeight,
                                    fontFamily: "IBMPlexSans",
                                    fontWeight: FontWeight.w700,
                                  ),
                                  textAlign: TextAlign.center,
                                ),
                              ),
                            ),
                            Padding(
                              padding: EdgeInsets.only(
                                  left: 50 / 360 * screenWidth,
                                  top: 6 / 720 * screenHeight,
                                  right: 50 / 360 * screenWidth),
                              child: Center(
                                child: Text(
                                  "Enter the IMS pin and password\n sent to you via email",
                                  style: TextStyle(
                                    color: Color.fromARGB(255, 126, 145, 154),
                                    fontSize: 14 / 720 * screenHeight,
                                    fontFamily: "IBMPlexSans",
                                  ),
                                  textAlign: TextAlign.center,
                                ),
                              ),
                            ),
                            Padding(
                              //height: 49/720 * screenHeight,
                              padding: EdgeInsets.only(
                                  left: 45 / 360 * screenWidth,
                                  top: 69 / 720 * screenHeight,
                                  right: 45 / 360 * screenWidth),
                              child: TextInputFormField(
                                controller: _ims_pin_controller,
                                //  focusNode: focus,
                                fontStyle: TextStyles.editTextStyle,
                                lableStyle: TextStyles.labelStyle,
                                keyboardType: TextInputType.text,
                                lableText: LoginAppLabels.imsPin,
                                textInputAction: TextInputAction.next,
                                border: imsPin.isEmpty ? UnderlineInputBorder(borderSide:BorderSide(color:SemanticColors.blueGrey.withOpacity(0.3),width: 1.5 )): UnderlineInputBorder(borderSide:BorderSide(color:SemanticColors.dark_purpely,width: 1.5 ) ),
                                validator: validateIMSPin,
                                maxLength: 14,
                                autovalidate: true,
                                onFieldSubmitted: (_) =>
                                    FocusScope.of(context).requestFocus(focus),
                              //  onSaved: (value) => _email = value,
                              ),
                            ),
                            Padding(
                              //height: 49/720 * screenHeight,
                              padding: EdgeInsets.only(
                                  left: 45 / 360 * screenWidth,
                                  top: 39 / 720 * screenHeight,
                                  right: 45 / 360 * screenWidth),
                              child: TextInputFormField(
                                controller: _ims_password_controller,
                              //  focusNode: focus,
                                keyboardType: TextInputType.text,
                                textInputAction: TextInputAction.done,
                                autovalidate: true,
                                validator: validateIMSPassword,
                                 lableText: LoginAppLabels.imsPassword,
                                 lableStyle: TextStyles.labelStyle,
                                  suffixIcon: IconButton(
                                        icon: _obscureText ? getSvgIcon.lockSvgIcon:getSvgIcon.unlockSvgIcon,
                                        padding: EdgeInsets.only(left: (30/360)*screenWidth, top: 20/720 * screenHeight),
                                        //  iconSize: 15,
                                        onPressed: () {
                                          FocusScope.of(context).requestFocus(new FocusNode());
                                          _toggle();
                                        }),
                                         border: imsPassword.isEmpty ? UnderlineInputBorder(borderSide:BorderSide(color:SemanticColors.blueGrey.withOpacity(0.3),width: 1 )): UnderlineInputBorder(borderSide:BorderSide(color:SemanticColors.dark_purpely,width: 1 ) ),
                                
                                obscureText: _obscureText,
                               
                              //  onSaved: (value) => _password = value,
                              ),
                            ),
                            Container(
                              height: 40 / 720 * screenHeight,
                              margin: EdgeInsets.only(
                                  left: 45 / 360 * screenWidth,
                                  top: 38 / 720 * screenHeight,
                                  right: 45 / 360 * screenWidth),
                              child: PrimaryButton(
                                //height: screenHeight * 0.055,
                                gradient: (imsPasswordValidator == true && imsPinValidator==true)
                                    ? true
                                    : false,
                                onTap: () {
                                  if ((imsPasswordValidator == true && imsPinValidator==true)) {
                                    AppRoutes.push(context, LoginGenderWidget());
                                  } else {
                                  }
                                },
                                text: LoginAppStrings.createAccount.toUpperCase(),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
        ),
      ),
    );
  }
}

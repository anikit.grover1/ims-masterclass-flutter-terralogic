import 'package:flutter/material.dart';
import 'package:imsindia/api/bloc/services.dart';
import 'package:imsindia/components/primary_button_gradient.dart';
import 'package:imsindia/routers/routes.dart';
import 'package:imsindia/utils/Url.dart';
import 'package:imsindia/utils/colors.dart';
import 'package:imsindia/utils/svg_icons.dart';
import 'package:imsindia/views/login_page/login_forgotpassword_widget.dart';
import 'package:imsindia/views/login_page/login_forgotpassword_widget.dart'
    as prefix0;
import 'package:imsindia/views/login_page/login_welcome_screen.dart';
import 'package:shared_preferences/shared_preferences.dart';
import './login_screen.dart';
import '../../resources/strings/login.dart';
import '../../routers/routes.dart';
import 'login_welcome_screen.dart';
import 'package:imsindia/utils/global.dart' as globals;

//import 'package:imsindia-app/views/login_page/login_imspinwidget.dart';

class LoginResentlinksentWidget extends StatefulWidget {
  final String emailToDispaly;
  LoginResentlinksentWidget({this.emailToDispaly});
  @override
  LoginResentlinksentWidgetState createState() =>
      LoginResentlinksentWidgetState();
}

class LoginResentlinksentWidgetState extends State<LoginResentlinksentWidget> {
  var resetLinkEmail;
  var resetLinkName;
  var resetLinkURL;
  final GlobalKey<ScaffoldState> _scaffoldstate =
      new GlobalKey<ScaffoldState>();
  void setResentLinkDetails() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    resetLinkEmail = prefs.getString("resentLinkEmail");
    resetLinkName = prefs.getString("resentLinkName");
    resetLinkURL = prefs.getString("resentLinkURL");
  }

  void showSuccessMessage(String value) {
    _scaffoldstate.currentState.showSnackBar(new SnackBar(
      content: new Text(value),
      duration: new Duration(seconds: 3),
    ));
  }

  void showErrorMessage(String value) {
    _scaffoldstate.currentState.showSnackBar(new SnackBar(
      content: new Text(value),
      duration: new Duration(seconds: 3),
    ));
  }

  void resendLinkAgain() {
    Map postdata = {
      "companyCode": "ims",
      "studentName": resetLinkName,
      "emailId": resetLinkEmail,
      "url": resetLinkURL
    };
    ApiService()
        .postAPI(URL.ResetPasswordLink, postdata, globals.headers)
        .then((result) {
      setState(() {
        if (result[0] == 'Email Sent Successfully') {
          showSuccessMessage(result[0] + ' To ' + '$resetLinkEmail');
        } else {
          showErrorMessage(result[0]);
        }
      });
    });
  }

  @override
  void initState() {
    setResentLinkDetails();
  }

  @override
  Widget build(BuildContext context) {
    final screenHeight = MediaQuery.of(context).size.height;
    final screenWidth = MediaQuery.of(context).size.width;
    final defaultScreenHeight = 720;
    final defaultScreenWidth = 360;

    @override
    Future<bool> _onWillPop() {
      AppRoutes.push(context, LoginForgotPasswordWidget());
    }

    return WillPopScope(
      onWillPop: _onWillPop,
      child: Scaffold(
        key: _scaffoldstate,
        
        body: Container(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              Container(
                height: screenHeight * 0.140,
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    GestureDetector(
                      onTap: () {
                        AppRoutes.push(context, LoginForgotPasswordWidget());
                      },
                      child: Container(
                        child: Padding(
                          padding: const EdgeInsets.all(5.0),
                          child: Align(
                            alignment: Alignment.topLeft,
                            child: Container(
                              width: (20 / 360) * screenWidth,
                              height: (20 / 720) * screenHeight,
                              margin: EdgeInsets.only(
                                  top: screenHeight * 0.0738,
                                  left: screenWidth * 0.062,
                                  right: 5),
                              child: getSvgIcon.backSvgIcon,
                            ),
                          ),
                        ),
                      ),
                    ),
                    Spacer(),
                    Align(
                      alignment: Alignment.topRight,
                      child: Container(
                        width: screenWidth * 0.780,
                        height: screenHeight * 0.9,
                        child: getSvgIcon.combinedshapeSvgIcon,
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                margin: EdgeInsets.only(
                    left: (45 / defaultScreenWidth) * screenWidth,
                    right: (45 / defaultScreenWidth) * screenWidth),
                child: Column(
                  children: <Widget>[
                    Container(
                      margin: EdgeInsets.only(top: screenHeight * 0.09),
                      child: Text(
                        "Reset Link Sent!",
                        style: TextStyle(
                          color: NeutralColors.charcoal_grey,
                          fontSize: (21 / defaultScreenWidth) * screenWidth,
                          fontFamily: "IBMPlexSans",
                          fontWeight: FontWeight.w700,
                        ),
                        textAlign: TextAlign.center,
                      ),
                    ),
                    Container(
                      width: (0.75 * screenWidth),
                      margin: EdgeInsets.only(top: 7 / 720 * screenHeight),
                      child: Center(
                        child: RichText(
                            textAlign: TextAlign.center,
                            text: new TextSpan(children: [
                              new TextSpan(
                                text:
                                    "A reset link has been sent to\nyour email ID",
                                style: TextStyle(
                                  color: NeutralColors.blue_grey,
                                  fontSize:
                                      (14 / defaultScreenWidth) * screenWidth,
                                  fontFamily: "IBMPlexSans",
                                  fontWeight: FontWeight.w400,
                                ),
                              ),
                              new TextSpan(
                                text: widget.emailToDispaly,
                                style: TextStyle(
                                  color: NeutralColors.charcoal_grey,
                                  fontSize:
                                      (14 / defaultScreenWidth) * screenWidth,
                                  fontFamily: "IBMPlexSans",
                                  fontWeight: FontWeight.w400,
                                ),
                              )
                            ])),
                      ),
                    ),
                    Container(
                      width: (0.75 * screenWidth),
                      margin: EdgeInsets.only(top: 15 / 720 * screenHeight),
                      child: FlatButton(
                        onPressed: () => {
                          AppRoutes.push(context, LoginForgotPasswordWidget()),
                        },
                        color: Colors.transparent,
                        textColor: Color.fromARGB(255, 0, 171, 251),
                        padding: EdgeInsets.all(0),
                        child: Text(
                          "Not your Email?",
                          style: TextStyle(
                              color: PrimaryColors.azure_Dark,
                              fontSize: (14 / defaultScreenWidth) * screenWidth,
                              fontFamily: "IBMPlexSans",
                              fontWeight: FontWeight.w400),
                          textAlign: TextAlign.center,
                        ),
                      ),
                    ),
                    Container(
                        height: (40 / defaultScreenHeight) * screenHeight,
                        width: (270 / defaultScreenWidth) * screenWidth,
                        margin: EdgeInsets.only(
                            top: (40 / defaultScreenHeight) * screenHeight),
                        child: PrimaryButtonGradient(
                          onTap: () {
                            AppRoutes.push(context, LoginWelcomeWidget());
                          },
                          text:
                              LoginResetLinkLables.continueLable.toUpperCase(),
                        ) // Group 4
                        ),
                    GestureDetector(
                      child: Container(
                        //height: 40/720 * screenHeight,
                        margin: EdgeInsets.only(top: 20 / 720 * screenHeight),
                        child: Center(
                          child: RichText(
                            textAlign: TextAlign.center,
                            text: new TextSpan(children: [
                              new TextSpan(
                                text: "Haven’t received a reset link?\n",
                                style: TextStyle(
                                  fontSize:
                                      (12 / defaultScreenWidth) * screenWidth,
                                  fontFamily: "IBMPlexSans",
                                  color: NeutralColors.gunmetal,
                                  fontWeight: FontWeight.w400,
                                ),
                              ),
                              new TextSpan(
                                text: "Resend",
                                style: TextStyle(
                                  fontSize:
                                      (12 / defaultScreenWidth) * screenWidth,
                                  fontFamily: "IBMPlexSans",
                                  color: PrimaryColors.azure_Dark,
                                  fontWeight: FontWeight.w400,
                                ),
                              ),
                            ]),
                          ),
                        ),
                      ),
                      onTap: () {
                        resendLinkAgain();
                      },
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

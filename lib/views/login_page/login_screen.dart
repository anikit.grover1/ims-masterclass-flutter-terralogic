import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:imsindia/api/bloc/services.dart';
import 'package:imsindia/components/Input_forms.dart';
import 'package:imsindia/components/custom_DropDown.dart';
import 'package:imsindia/components/primary_Button.dart';
import 'package:imsindia/resources/strings/login.dart';
import 'package:imsindia/routers/routes.dart';
import 'package:imsindia/utils/Url.dart';
import 'package:imsindia/utils/colors.dart';
import 'package:imsindia/utils/global.dart' as globals;
import 'package:imsindia/utils/svg_icons.dart';
import 'package:imsindia/utils/text_styles.dart';
import 'package:imsindia/views/Arun/ims_utility.dart';
import 'package:imsindia/views/home_pages/home_widget.dart';
import 'package:imsindia/views/home_pages/scrollTabComponents/base_widget.dart';
import 'package:imsindia/views/login_page/login_forgotpassword_widget.dart';
import 'package:imsindia/views/login_page/login_welcome_screen.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:imsindia/views/Arun/IMSSharedPref.dart';

import 'data.dart';

class LoginFillpasswordWidget extends StatefulWidget {
  @override
  LoginFillpasswordWidgetState createState() => LoginFillpasswordWidgetState();
}

final TextEditingController _email = new TextEditingController();
final TextEditingController _password = new TextEditingController();
final TextEditingController _courses = new TextEditingController();

String get email => _email.text;
String get password => _password.text;
String get course => _courses.text;
String courseId;
String userName;
String courseName;
bool passwordChecked = false;
bool emailID = false;
bool selectCourse = false;
//List<String> myListOfStrings;

class LoginFillpasswordWidgetState extends State<LoginFillpasswordWidget> {
  final GlobalKey<ScaffoldState> _scaffoldstate =
      new GlobalKey<ScaffoldState>();

  SharedPreferences prefs;

  // void onForgotPasswordPressed(BuildContext context) =>
  //  Navigator.push(context,
  //     MaterialPageRoute(builder: (context) => LoginForgotPasswordWidget()));
  void showErrorMessage(String value) {
    _scaffoldstate.currentState.showSnackBar(new SnackBar(
      content: new Text(value),
      duration: new Duration(seconds: 3),
    ));
  }

  void setLoginStatusTrue() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setBool("LoginStatus", true);
  }

  void setUserID(value) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString("userId", value);
  }

  void setCourseId(value) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString("courseId", value);
  }

  void setCourseName(value) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString("courseName", value);
  }

  void setCurrentUserEmail(value) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString(ImsStrings.sCurrentUserEmail, value);
  }
  void setUsernameasStudent(value)async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString("_studentName", value);
  }

  void setCurrentUserLoginId(value) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString(ImsStrings.sCurrentUserLoginId, value);
  }

  void setToken(value) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString("loginAccessToken", value);
   // globals.loginAccessToken = value;
     print(globals.loginAccessToken);
     //globals.getToken;
  }

  // void setUserName(value) async {
  //   SharedPreferences prefs = await SharedPreferences.getInstance();
  //   userName=prefs.getString("userName");
  // }
  void getUserName() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    userName = prefs.getString(ImsStrings.sCurrentUserId);
  }

//  var t = '"';
  var t = '';
  var courses = [];
  var data = [];
    CourseData() {
    ApiService().getAPI(URL.GET_COURSE_NAMES, globals.headers).then((result) {
      courses = [];
      data = [];
      setState(() {
        for (int i = 0; i < result[1]['data'].length; i++) {
          courses.add(result[1]['data'][i]['courseName']);
          data.add(result[1]['data'][i]['id']);
        }
      });
      print("Courses"+courses.toString());
    });
  }

  void apiCall() async {}
  void checkLogin() {
    Map postdata = {
      "loginId": t + userName + t,
      "password": t + password + t,
      "courseId": t + courseId + t,
      "loginMode": "1",
      "companyCode": "ims"
    };
    ApiService().postAPI(URL.Login, postdata, globals.headers).then((result) {
      print("Login result value"+result.toString());
      setState(() {
        if (result[0] == 'login successful') {
          print("======================================LOGIN STATUS");
          print(result[1]['data']['userData']);
          //globals.loginAccessToken = result[1]['data']['token'];
          // globals.courseId = courseId;
          // globals.sample = courseId;
          setLoginStatusTrue();
          setCourseId(courseId);
          setCourseName(courseName);
          setUserID(result[1]['data']['userData']['userId']);
          setToken(result[1]['data']['token']);
          setCurrentUserEmail(result[1]['data']['userData']['emailId']);
          setUsernameasStudent(result[1]['data']['userData']['userName']);
          setCurrentUserLoginId(result[1]['data']['userData']['loginId']);
          IMSSharedPref().set_limitCellularDataUsage(false);

          // AppRoutes.push(context, LoginEnrollmentidWidget());
          AppRoutes.push(context, HomeWidget());
//          AppRoutes.push(context, BaseWidget());
        } else {
          showErrorMessage(result[0]);
        }
      });
    });
  }

  //void onGroup4Pressed(BuildContext context) => Navigator.push(context, MaterialPageRoute(builder: (context) => LoginCorrectpasswordWidget()));
  final focus = FocusNode();
  bool _obscureText = true;
  void _toggle() {
    setState(() {
      _obscureText = !_obscureText;
    });
  }

  ScrollController _scrollController = new ScrollController();
  @override
  void initState() {
    super.initState();
    getUserName();
    CourseData();
    setState(() {
      _email.text = '';
      _password.text = '';
      //_password.text = '22222';
      _courses.clear();
    });
    _email.addListener(() {
      setState(() {
        validateEmail(email);
      });
    });
    _password.addListener(() {
      setState(() {
        validatePassword(password);
      });
    });
    _courses.addListener(() {
      // SystemChannels.textInput.invokeMethod('TextInput.hide');

      setState(() {
        if (_courses.text != '' && _courses.text != null) {
          selectCourse = true;
        } else {
          selectCourse = false;
        }
      });
    });
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
  }

  static String validateEmail(String value) {
    // if (email.isEmpty || email.length == 0) {
    //   emailID = false;
    //   return null;
    // }

    // Pattern pattern =
    //     r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';

    // RegExp regex = new RegExp(pattern);

    // if (!regex.hasMatch(email.trim())) {
    //   emailID = false;

    //   return LoginAppLabels.invalidID;
    // }
    // if (regex.hasMatch(email.trim())) {
    //   emailID = true;
    //   return null;
    // }
    if (email.isEmpty && email.length == 0) {
      emailID = false;
      return null;
    } else if (email.length < 12) {
      emailID = false;
      return LoginUsingIMSPin.invalidIMSPasswordForLogin;
    } else {
      emailID = true;
      return null;
    }
  }

  static String validatePassword(String value) {
    if (password.isEmpty && password.length == 0) {
      passwordChecked = false;
      return null;
    }
//    else if (password.length < 5) {
//      passwordChecked = false;
//      return LoginUsingIMSPin.invalidLoginPassword;
//    }
    else {
      passwordChecked = true;
      return null;
    }
  }

  @override
  Widget build(BuildContext context) {
    final screenHeight = MediaQuery.of(context).size.height;
    final screenWidth = MediaQuery.of(context).size.width;
    Future<bool> _onWillPop() {
      //back
      AppRoutes.push(context, LoginWelcomeWidget());
    }

    return Scaffold(
      key: _scaffoldstate,
      //  
      body: courses.length == 0?
      Container(
//          margin: EdgeInsets.only(
//              top: (300.0 / 720) * screenHeight),
          child:
          Center(child: CircularProgressIndicator()))
          :SingleChildScrollView(
        controller: _scrollController,
        child: Container(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              Padding(
                padding: EdgeInsets.only(top: screenHeight * 80 / 720),
                child: Center(
                  child: Text(
                    "Login to myIMS",
                    style: TextStyle(
                      color: Color.fromARGB(255, 31, 37, 43),
                      fontSize: 21 / 720 * screenHeight,
                      fontFamily: "IBMPlexSans",
                      fontWeight: FontWeight.w700,
                    ),
                    textAlign: TextAlign.center,
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(top: screenHeight * 40.5 / 720),
                child: Container(
                    height: screenHeight * 0.20,
                    width: screenWidth * 0.50,
                    child: getSvgIcon.welcomeSvgIcon),
              ),
              // Padding(
              //   padding: EdgeInsets.only(
              //       left: screenWidth * 45 / 360,
              //       top: screenHeight * 47 / 720,
              //       right: screenWidth * 45 / 360),
              //   child: TextInputFormField(
              //     controller: _email,
              //     keyboardType: TextInputType.emailAddress,
              //     lableText: LoginAppLabels.emailID,
              //     textInputAction: TextInputAction.next,
              //     border: email.isEmpty ? UnderlineInputBorder(borderSide:BorderSide(color:SemanticColors.blueGrey.withOpacity(0.3),width: 1.5 )): UnderlineInputBorder(borderSide:BorderSide(color:SemanticColors.dark_purpely,width: 1.5 ) ),
              //     autovalidate: true,
              //     validator: validateEmail,
              //     onSaved: (value) => {},
              //     onFieldSubmitted: (val) {
              //       setState(() {
              //       });
              //       FocusScope.of(context).requestFocus(focus);
              //     },
              //   ),
              // ),
              Padding(
                padding: EdgeInsets.only(
                    left: screenWidth * 45 / 360,
                    top: screenHeight * 47 / 720,
                    right: screenWidth * 45 / 360),
                child: TextInputFormField(
                  controller: _password,
                  focusNode: focus,
                  keyboardType: TextInputType.visiblePassword,
                  textInputAction: TextInputAction.done,
                  autovalidate: true,
                  //validator: validatePassword,
                  lableText: 'Password',
                  border: password.isEmpty
                      ? UnderlineInputBorder(
                          borderSide: BorderSide(
                              color: SemanticColors.blueGrey.withOpacity(0.3),
                              width: 1.5))
                      : UnderlineInputBorder(
                          borderSide: BorderSide(
                              color: SemanticColors.dark_purpely,
                              width: 1.5)),
                  // decoration: InputDecoration(
                  //     focusedBorder: UnderlineInputBorder(
                  //         borderSide: BorderSide(
                  //             color: SemanticColors.dark_purpely,
                  //             width: 1.5)),
                  // errorStyle: TextStyles.errorTextStyle,
                  //     labelText: LoginAppLabels.password,

                  //     enabledBorder: password.isEmpty
                  //         ? UnderlineInputBorder(
                  //             borderSide: BorderSide(
                  //                 color: SemanticColors.blueGrey
                  //                     .withOpacity(0.3),
                  //                 width: 1.5))
                  //         : UnderlineInputBorder(
                  //             borderSide: BorderSide(
                  //                 color: SemanticColors.dark_purpely,
                  //                 width: 1.5)),
                  //     contentPadding: EdgeInsets.symmetric(vertical: 10.0),
                  suffixIcon: IconButton(
                      icon: _obscureText
                          ? getSvgIcon.lockSvgIcon
                          : getSvgIcon.unlockSvgIcon,
                      padding: EdgeInsets.only(
                        top: 20 / 720 * screenHeight,
                        left: (30 / 360) * screenWidth,
                      ),
                      //  iconSize: 15,
                      onPressed: () {
                        FocusScope.of(context).requestFocus(new FocusNode());
                        _toggle();
                      }),
                  //border: const UnderlineInputBorder(),

                  //  textAlign: TextAlign.left,
                  obscureText: _obscureText,
                  fontStyle: TextStyles.editTextStyle,
                  lableStyle: TextStyles.labelStyle,
                  prefixStyle: TextStyles.prefixStyle,
                  onSaved: (value) => {},
//                      onFieldSubmitted: (val) {
//                        setState(() {
//
//                        });
//                      },
                ),
              ),
              Padding(
                  padding: EdgeInsets.only(
                      left: screenWidth * 45 / 360,
                      top: screenHeight * 47 / 720,
                      right: screenWidth * 45 / 360),
                  child: DropDownFormField(
                    getImmediateSuggestions: true,
                    // autoFlipDirection: true,
                    textFieldConfiguration: TextFieldConfiguration(
                        controller: _courses,
                        label: LoginAppLabels.selectCourse),
                    suggestionsBoxDecoration: SuggestionsBoxDecoration(
                        dropDownHeight: 150,
                        borderRadius: new BorderRadius.circular(5.0),
                        color: Colors.white),
                    suggestionsCallback: (pattern) {
                      return courses;
                    },
                    itemBuilder: (context, suggestion) {
                      return ListTile(
                        dense: true,
                        contentPadding: EdgeInsets.only(left: 10),
                        title: Text(
                          suggestion,
                          style: TextStyle(
                            fontFamily: "IBMPlexSans",
                            fontWeight: FontWeight.w400,
                            fontSize: (13 / 720) * screenHeight,
                            color: NeutralColors.bluey_grey,
                            textBaseline: TextBaseline.alphabetic,
                            letterSpacing: 0.0,
                            inherit: false,
                          ),
                        ),
                      );
                    },
                    hideOnLoading: true,
                    debounceDuration: Duration(milliseconds: 100),
                    transitionBuilder: (context, suggestionsBox, controller) {
                      return suggestionsBox;
                    },
                    onSuggestionSelected: (suggestion) {
                      for (int i = 0; i < courses.length; i++) {
                        if (courses[i] == suggestion) {
                          courseId = data[i];
                        }
                      }
                      courseName = suggestion;
                      _courses.text = suggestion;
                      // setState(() {});
                    },
                    onSaved: (value) => {print("something")},
                  )),
              Container(
                  height: 40 / 720 * screenHeight,
                  margin: EdgeInsets.only(
                      left: 45 / 360 * screenWidth,
                      top: 45 / 720 * screenHeight,
                      right: 45 / 360 * screenWidth),
                  child: PrimaryButton(
                    gradient:
                        (passwordChecked == true && selectCourse == true)
                            ? true
                            : false,
                    onTap: () {
                      if (passwordChecked == true && selectCourse == true) {
                        checkLogin();
                        //AppRoutes.push(context, LoginEnrollmentidWidget());
                      }
                    },
                    text: LoginAppStrings.loginButton,
                  ) //
                  ),
              Align(
                alignment: Alignment.topLeft,
                child: Container(
                  margin: EdgeInsets.only(
                      left: 45 / 360 * screenWidth,
                      top: 15 / 720 * screenHeight),
                  child: FlatButton(
                    onPressed: ()   { try {
                          launchURL('${ImsStrings.URL_FORGOTPASSWORD}');
                        } catch (e) {
                          print(e.toString());
                        }},
                    //this.onForgotPasswordPressed(context),
                    color: Colors.transparent,
                    textColor: Color.fromARGB(255, 0, 171, 251),
                    padding: EdgeInsets.all(0),
                    child: Text(
                      "Forgot password?",
                      style: TextStyle(
                        fontSize: 14 / 720 * screenHeight,
                        fontFamily: "IBMPlexSans",
                      ),
                      textAlign: TextAlign.center,
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class AlwaysDisabledFocusNode extends FocusNode {
  @override
  bool get hasFocus => false;
}

import 'package:flutter/material.dart';
import 'package:imsindia/views/login_page/login_educational_details.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:imsindia/components/custom_DropDown.dart';

import '../../components/Input_forms.dart';
import '../../components/custom_expansion_panel.dart';
import '../../resources/strings/login.dart';
import '../../utils/colors.dart';
import '../../utils/text_styles.dart';
import '../../utils/validator.dart';
import 'data.dart';
import 'login_education.dart';

const actualHeight = 925;
const actualWidth = 360;

var screenWidthTotal;
var screenHeightTotal;
var _safeAreaHorizontal;
var _safeAreaVertical;
var screenHeight;
var screenWidth;
int _degreeCount = 1;
bool isSwitchedProfessionalDegree = false;

class NoOfAttempts extends StatefulWidget {
  @override
  _NoOfAttemptsState createState() =>
      _NoOfAttemptsState();
}

class _NoOfAttemptsState extends State<NoOfAttempts> {
   
  final TextEditingController _degreeInProfessionalDetails = new TextEditingController();
  @override
  void initState() {
    _degreeInProfessionalDetails.clear();
    setProfessionalDetailsFilledFalse();
    _degreeInProfessionalDetails.addListener(() {
      setState(() {
        checkProfessionalDetails();
      });
    });
    
    setState(() {
      isSwitchedProfessionalDegree=false;
    });
  }
    void setProfessionalDetailsFilledTrue() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setBool("professionalDetailsFilled", true);
  }

  void setProfessionalDetailsFilledFalse() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setBool("professionalDetailsFilled", false);
  }
  checkProfessionalDetails() async {
    setState(() {
      if ((_degreeInProfessionalDetails.text != '' && _degreeInProfessionalDetails.text != null)
      ||isSwitchedProfessionalDegree==true) {
        setProfessionalDetailsFilledTrue();
      } else {
        setProfessionalDetailsFilledFalse();
      }
    });
  }
       Widget addDegree(BuildContext context) {

    var _mediaQueryData = MediaQuery.of(context);
    screenWidthTotal = _mediaQueryData.size.width;
    screenHeightTotal = _mediaQueryData.size.height;
    _safeAreaHorizontal =
        _mediaQueryData.padding.left + _mediaQueryData.padding.right;
    _safeAreaVertical =
        _mediaQueryData.padding.top + _mediaQueryData.padding.bottom;

    screenHeight = screenHeightTotal - _safeAreaVertical;
    screenWidth = screenWidthTotal - _safeAreaHorizontal;
    return new Container(
      height: (60 / 720) * screenHeight,
      margin: EdgeInsets.only(left: 15/360 * screenWidthTotal, top: 10/720 * screenHeightTotal, right: 130/360 * screenWidthTotal),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Expanded(
              flex: 1,
              child: Container(
                width: (133 / 360) * screenWidth,
                height: (52 / 720) * screenHeight,
                margin: EdgeInsets.only(top: 0, bottom: 0),
                child: isSwitchedProfessionalDegree?new TextInputFormField(
                                                enabled: false,
                                                lableText: "Degree",
                                              ):new DropDownFormField(
                  getImmediateSuggestions: true,
                  textFieldConfiguration: TextFieldConfiguration(
                    label: "Degree",
                    controller: this._degreeInProfessionalDetails,
                  ),
                   suggestionsBoxDecoration: SuggestionsBoxDecoration(
                          dropDownHeight: 150,
                          borderRadius: new BorderRadius.circular(5.0),
                          color: Colors.white),
                  suggestionsCallback: (pattern) {
                    _degreeInProfessionalDetails.clear();

                    return DegreeService.getSuggestions(pattern);
                  },
                  itemBuilder: (context, suggestion) {
                    return ListTile(
                          dense: true,
                          contentPadding: EdgeInsets.only(left: 10/360 * screenWidthTotal),
                          title: Text(
                            suggestion,
                            style: TextStyle(
                              fontFamily: "IBMPlexSans",
                              fontWeight: FontWeight.w400,
                              fontSize: (13 / 720) * screenHeight,
                              color: NeutralColors.bluey_grey,
                              textBaseline: TextBaseline.alphabetic,
                              letterSpacing: 0.0,
                              inherit: false,
                              
                            ),
                          ),
                        );
                  },
                  transitionBuilder: (context, suggestionsBox, controller) {
                    return suggestionsBox;
                  },
                  onSuggestionSelected: (suggestion) {
                    this._degreeInProfessionalDetails.text = suggestion;
                    // this._typeAheadController.dispose();
                  },
                ),
              )),
        ],
      ),
    );
  }


// @override
// void didUpdateWidget(ProfessionalDegreeDetails build) {
  
//     // TODO: implement didUpdateWidget
//     super.didUpdateWidget(build);
//   }
  @override
  Widget build(BuildContext context) {
      List<Widget> _addDegree =
      new List.generate(_degreeCount, (int i) => addDegree(context));
    return Container(
      margin: EdgeInsets.only(left: 0, top: 0, right: 0, bottom: 0),
      decoration: BoxDecoration(
        color: Color.fromARGB(255, 255, 255, 255),
        boxShadow: [
          BoxShadow(
            color: Color.fromARGB(255, 234, 234, 234),
            offset: Offset(0, 0),
            blurRadius: 10,
          ),
        ],
        borderRadius: BorderRadius.all(Radius.circular(5)),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Align(
            alignment: Alignment.topLeft,
            child: Container(
              width: (150 / 360) * screenWidth,
              height: (20 / 720) * screenHeight,
              margin: EdgeInsets.only(left: 15/360 * screenWidthTotal, top: 14/720 * screenHeightTotal),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    margin: EdgeInsets.only(top: 0),
                    child: Text(
                      "Not Applicable",
                      style: TextStyle(
                          color: NeutralColors.gunmetal,
                          fontWeight: FontWeight.w400,
                          fontFamily: "IBMPlexSans",
                          fontStyle: FontStyle.normal,
                          fontSize: (11 / 360) * screenWidth),
                      textAlign: TextAlign.center,
                    ),
                  ),
                  Transform.scale(
                    scale: 0.78,
                    child: Container(
                      child: Switch(
                        value: isSwitchedProfessionalDegree,
                        onChanged: (value) {
                          setState(() {
                            isSwitchedProfessionalDegree = value;
                            checkProfessionalDetails();
                          });
                        },
                        activeTrackColor: SemanticColors.iceBlue,
                        activeColor: NeutralColors.mango,
                        inactiveThumbColor: NeutralColors.blue_grey,
                        inactiveTrackColor: SemanticColors.iceBlue,
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
           Column(children: _addDegree
              ),
          Align(
            alignment: Alignment.topLeft,
            child: Container(
              width: (140 / 360) * screenWidth,
              height: (20 / 720) * screenHeight,
              margin: EdgeInsets.only(left: 15/360 * screenWidthTotal, top: 22/720 * screenHeightTotal, bottom: 20.0/360 * screenWidthTotal),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  Align(
                    alignment: Alignment.topLeft,
                    child: CircleAvatar(
                      backgroundColor: Colors.blue,
                      child: Icon(
                        Icons.add,
                        color: Colors.white,
                        size: 20.0,
                      ),
                    ),
                  ),
                  Align(
                    alignment: Alignment.topLeft,
                    child: InkWell(
                      child: Container(
                        margin: EdgeInsets.only(left: 2/360 * screenWidthTotal, top: 2/360 * screenWidthTotal),
                        child: Text(
                          "Add Degree",
                          style: TextStyle(
                            color: Color.fromARGB(255, 0, 171, 251),
                            fontSize: 14/720 * screenHeightTotal,
                            fontFamily: "IBM Plex Sans",
                          ),
                          textAlign: TextAlign.center,
                        ),
                      ),
                      onTap: () {
                        setState(() {
                          _degreeCount = _degreeCount + 1;
                        });
                      },
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}


import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:imsindia/components/custom_DropDown.dart';
import 'package:imsindia/views/login_page/login_education.dart';
import 'package:imsindia/views/login_page/login_educational_details.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../components/Input_forms.dart';
import '../../components/custom_expansion_panel.dart';
import '../../resources/strings/login.dart';
import '../../utils/colors.dart';
import '../../utils/text_styles.dart';
import '../../utils/validator.dart';
import 'data.dart';
//final keyTenth = new GlobalKey<TenthClassDetailsState>();

const actualHeight = 720;
const actualWidth = 360;

var screenWidthTotal;
var screenHeightTotal;
var _safeAreaHorizontal;
var _safeAreaVertical;
var screenHeight;
var screenWidth;
var indexInTenth = '';

class FourthAttemptDetails extends StatefulWidget {
  // TenthClassDetails({Key key}) : super(key: key);

  @override
  FourthAttemptDetailsState createState() => FourthAttemptDetailsState();
}

class FourthAttemptDetailsState extends State<FourthAttemptDetails> {
  @override
  void initState() {
    _percentileInFourthAttempts.clear();
    _yearInFourthAttempts.clear();
    //  print('some'+_gradeInTenthDetails.text.toString());
    _percentileInFourthAttempts.addListener(() {
      setState(() {
        checkFourthAttemptDetails();
      });
    });
    _yearInFourthAttempts.addListener(() {
      setState(() {
        checkFourthAttemptDetails();
      });
    });
    setState(() {
      checkFourthAttemptDetails();
    });
  }

  final TextEditingController _yearInFourthAttempts =
  new TextEditingController();
  final TextEditingController _percentileInFourthAttempts =
  new TextEditingController();

  void setVariableTrue() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setBool("fourthDetailsFilled", true);
  }

  void setVariableFalse() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setBool("fourthDetailsFilled", false);
  }

  checkFourthAttemptDetails() async {
    print('check');
    setState(() {
      if (_percentileInFourthAttempts.text != '' &&
          _yearInFourthAttempts.text != '' &&
          _percentileInFourthAttempts.text != null &&
          _yearInFourthAttempts.text != null) {
        setVariableTrue();
      } else {
        print('else');
        setVariableFalse();
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    CalculateScreen(context);
    return Container(
      height: (80 / 720) * screenHeight,
      decoration: BoxDecoration(
        color: Color.fromARGB(255, 255, 255, 255),
        boxShadow: [
          BoxShadow(
            color: Color.fromARGB(255, 234, 234, 234),
            offset: Offset(0, 0),
            blurRadius: 10,
          ),
        ],
        borderRadius: BorderRadius.all(Radius.circular(5)),
      ),
      // margin: EdgeInsets.only(
      //     left: 15,
      //     top: (11 / 720) * screenHeight,
      //     right: 15),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
           Container(
                width: (133 / 360) * screenWidthTotal,
                height: (52 / 720) * screenHeightTotal,
                margin: EdgeInsets.only(left: 15/360 * screenWidthTotal, right: 15/360 * screenWidthTotal,top: 7/720 * screenHeightTotal),
                child:  DropDownField(
                  getImmediateSuggestions: true,
              textFieldConfiguration: new TextFieldConfiguration(
                label: "Year",
                maxLength: 4,
                controller: _yearInFourthAttempts

              ),
               suggestionsBoxDecoration: SuggestionsBoxDecoration(
                          dropDownHeight: 150,
                          borderRadius: new BorderRadius.circular(5.0),
                          color: Colors.white),
            
                  suggestionsCallback: (pattern) {
                    _yearInFourthAttempts.clear();

                    return YearService.getSuggestions(pattern);
                  },
                  itemBuilder: (context, suggestion) {
                    return ListTile(
                          dense: true,
                          contentPadding: EdgeInsets.only(left: 10),
                          title: Text(
                            suggestion,
                            style: TextStyle(
                              fontFamily: "IBMPlexSans",
                              fontWeight: FontWeight.w400,
                              fontSize: (13 / 720) * screenHeight,
                              color: NeutralColors.bluey_grey,
                              textBaseline: TextBaseline.alphabetic,
                              letterSpacing: 0.0,
                              inherit: false,
                              
                            ),
                          ),
                        );
                  },
                  transitionBuilder: (context, suggestionsBox, controller) {
                    return suggestionsBox;
                  },
                  onSuggestionSelected: (suggestion) {
                    this._yearInFourthAttempts.text = suggestion;
                    // this._typeAheadController.dispose();
                  },
                ),
              ),
          Container(
            width: (80 / 360) * screenWidthTotal,
            height: (50 / 720) * screenHeightTotal,
            margin: EdgeInsets.only(right: 15/360 * screenWidthTotal,top: 19/720 * screenHeightTotal),
            child: TextInputFormField(
              keyboardType: TextInputType.number,
              lableText: LoginAppLabels.percentile,
              controller: _percentileInFourthAttempts,
              textInputAction: TextInputAction.done,
              maxLength: 2,
                  fontStyle: TextStyles.loginAlternativeOption,
              lableStyle: TextStyles.labelStyle,
              border: _percentileInFourthAttempts.text.isEmpty ? UnderlineInputBorder(borderSide:BorderSide(color:SemanticColors.blueGrey.withOpacity(0.3),width: 1 )): UnderlineInputBorder(borderSide:BorderSide(color:SemanticColors.dark_purpely,width: 1 ) ),
              autovalidate: true,
              validator: FieldValidator.validateGrade,
              onSaved: (value) {
                //   FocusScope.of(context).requestFocus(new FocusNode());
              },
              onEditingComplete: () {
                setState(() {});
              },
//                                    onFieldSubmitted: (value)
//                                    {
//                                    setState(() {
//                                    value=gradeTenth;
//                                    print("asd");
//                                    print(value);
//                                    print(gradeTenth);
//                                    print("tenth grade on submited");
//                                    });
//                                    }

              onFieldSubmitted: (val) {
                FocusScope.of(context).requestFocus(new FocusNode());
              },
            ),
          )
        ],
      ),
    );
  }
}

void CalculateScreen(BuildContext context) {
  var _mediaQueryData = MediaQuery.of(context);
  screenWidthTotal = _mediaQueryData.size.width;
  screenHeightTotal = _mediaQueryData.size.height;
  _safeAreaHorizontal =
      _mediaQueryData.padding.left + _mediaQueryData.padding.right;
  _safeAreaVertical =
      _mediaQueryData.padding.top + _mediaQueryData.padding.bottom;

  screenHeight = screenHeightTotal - _safeAreaVertical;
  screenWidth = screenWidthTotal - _safeAreaHorizontal;
}

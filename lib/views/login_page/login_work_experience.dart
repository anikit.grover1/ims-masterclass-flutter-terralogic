import 'package:flutter/material.dart';
import 'package:imsindia/components/Input_forms.dart';
import 'package:imsindia/components/custom_DropDown.dart';
import 'package:imsindia/components/primary_Button.dart';
import 'package:imsindia/components/primary_button_gradient.dart';
import 'package:imsindia/components/work_experience_button.dart';
import 'package:imsindia/resources/strings/login.dart';
import 'package:imsindia/routers/routes.dart';
import 'package:imsindia/utils/colors.dart';
import 'package:imsindia/utils/svg_icons.dart';
import 'package:imsindia/utils/text_styles.dart';
import 'package:imsindia/utils/validator.dart';
import 'package:imsindia/views/login_page/login_confirmaddress_widget.dart';
import 'package:imsindia/views/login_page/login_education.dart' as prefix0;
import 'package:imsindia/views/login_page/login_education.dart';
import 'package:imsindia/views/login_page/login_enrollmentid_widget.dart';

import 'data.dart';
import 'login_create_profile.dart';
//import 'package:ims_login3/screens/login_professional_degree_widget.dart';
var text = new RichText(
  text: new TextSpan(
    // Note: Styles for TextSpans must be explicitly defined.
    // Child text spans will inherit styles from parent
    style: new TextStyle(
      fontSize: 14.0,
      color: Colors.black,
    ),
    children: <TextSpan>[
      new TextSpan(text: 'Total Experience'),
      new TextSpan(text: 'in months', style: new TextStyle(fontWeight: FontWeight.bold,fontStyle: FontStyle.italic)),
    ],
  ),
 );
class LoginWorkexperience extends StatefulWidget {
  @override
  LoginWorkexperiencestate createState() => LoginWorkexperiencestate();
}
final TextEditingController _technology =  TextEditingController();

final TextEditingController _experience_controller =  TextEditingController();
final TextEditingController _organisation_controller = new TextEditingController();
String get experience => _experience_controller.text;
String get organisation => _organisation_controller.text;
bool experienceValidator = false;
bool organisationValidator = false;
class LoginWorkexperiencestate extends State<LoginWorkexperience> {
  bool isSwitched = false;
  bool isActive = false;
  String _name, totalExperience;
  List<String> _colors = <String>[
    'Information Technology',
    'Computer Application',
    'Business Management',
    'Civil Service',
    'Marketing',
  ];
  String _color = 'Information Technology';
  FocusNode focus = new FocusNode();
  @override
  void initState() {
    super.initState();
    setState(() {
      _experience_controller.text = '';
      _organisation_controller.text = '';
    });
    _experience_controller.addListener(() {
      setState(() {
        validateExperience(experience);
      });
    });
    _organisation_controller.addListener(() {
      setState(() {
        validateOrganisation(organisation);
      });
    });
  }
  static String validateExperience(String value){
    if (value.isEmpty) {
      experienceValidator=false;
      return null;
    }
    Pattern pattern = r'^[1-9][0-9]*$';
    RegExp regex = new RegExp(pattern);
    if (!regex.hasMatch(value.trim())) {
      experienceValidator=false;
      return LoginWorkexperienceLables.invalidExperience;
    }
    if (regex.hasMatch(value.trim())) {
      experienceValidator=true;


      return null;
    }
  }

  static String validateOrganisation(String value){
    if (value.isEmpty) {
      organisationValidator=false;
      return null;
    }
    else if(value.length<5){
      organisationValidator=false;
      return LoginWorkexperienceLables.invalidOrganisationlength;
    }
    Pattern pattern = r'^[0-9]*$';
    RegExp regex = new RegExp(pattern);
    if (regex.hasMatch(value.trim())) {
      organisationValidator=false;
      return LoginWorkexperienceLables.organisation;
    }
    if (!regex.hasMatch(value.trim())) {
      organisationValidator=true;

      return null;
    }
  }
//  void onGroup3Pressed(BuildContext context) => Navigator.push(context, MaterialPageRoute(builder: (context) => LoginProfessionalDegreeWidget()));

  @override
  Widget build(BuildContext context) {
    final screenHeight = MediaQuery.of(context).size.height;
    final screenWidth = MediaQuery.of(context).size.width;
    Future<bool> _onWillPop() {
      AppRoutes.push(context, LoginEnrollmentidWidget());
    }

    return WillPopScope(
      onWillPop: _onWillPop,
      child: Scaffold(
        body: GestureDetector(
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: Container(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                Container(
                  height: (82 / 720) * screenHeight,
                 // margin: EdgeInsets.only(left: 8),
                  child: Row(
                    children: [
                      GestureDetector(
                        onTap:(){
                          AppRoutes.push(context, LoginEnrollmentidWidget());
                        },
                        child: Align(
                          alignment: Alignment.topLeft,
                          child: Container(
                            width: (20 / 360) * screenWidth,
                            height: (20 / 720) * screenHeight,
                            margin: EdgeInsets.only(top: screenHeight*0.0738,left: screenWidth*0.062,right: 5),
                            child: getSvgIcon.backSvgIcon,
                          ),
                        ),
                      ),
                    //  Spacer(),
                    ],
                  ),
                ),
                Expanded(
                  child: SingleChildScrollView(
                    child: Container(
                      child: Column(
                        children: <Widget>[
                          Container(
                            margin: EdgeInsets.only(
                                left: screenWidth * 45 / 360,
                                top: screenHeight * 18 / 720,
                                right: screenWidth * 45 / 360),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.stretch,
                              children: [
                                Align(
                                  alignment: Alignment.topCenter,
                                  child: Text(
                                    "Tell us about\nyour work experience",
                                    style: TextStyle(
                                      color: Color.fromARGB(255, 45, 52, 56),
                                      fontSize: screenHeight * 21 / 720,
                                      fontFamily: "IBMPlexSans",
                                      fontWeight: FontWeight.w700,
                                    ),
                                    textAlign: TextAlign.center,
                                  ),
                                ),
                                Align(
                                  alignment: Alignment.topLeft,
                                  child: Container(
                                    width: double.infinity,
                                    height: screenHeight * 22 / 720,
                                    margin: EdgeInsets.only(
                                        top: screenHeight * 41 / 720),
                                    child: Row(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Container(
                                          margin: EdgeInsets.only(
                                              top: screenHeight * 4 / 720),
                                          child:
                                          Text(
                                            LoginWorkexperienceLables
                                                .notApplicable,
                                            style: TextStyle(
                                                color: NeutralColors.gunmetal,
                                                fontWeight: FontWeight.w400,
                                                fontFamily: "IBMPlexSans",
                                                fontStyle: FontStyle.normal,
                                                fontSize:
                                                    (12 / 360) * screenWidth),
                                            textAlign: TextAlign.center,
                                          ),
                                        ),
                                        Switch(
                                          value: isSwitched,
                                          onChanged: (value) {
                                            setState(() {
                                              isSwitched = value;
                                              (isSwitched);
                                              _experience_controller.text='';
                                              _organisation_controller.text='';
                                              _technology.text='';
                                            });
                                          },
                                          activeTrackColor:
                                              SemanticColors.iceBlue,
                                          activeColor: NeutralColors.mango,
                                          inactiveThumbColor:
                                              NeutralColors.blue_grey,
                                          inactiveTrackColor:
                                              SemanticColors.iceBlue,
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                            //  isSwitched ? Container() :
                              Container(
                                  child: Column(
                                    children: <Widget>[
                                      Container(
                                        width: double.infinity,
                                        //  height: screenHeight*49/720,
                                        margin: EdgeInsets.only(
                                            top: screenHeight * 42 / 720),
                                        child: TextInputFormField(
                                          controller: _experience_controller,
                                          enabled: isSwitched ? false : true,
                                          keyboardType: TextInputType.number,
                                          //lableStyle: TextStyles.labelStyleWithItalic,
                                          lableText:                                          
                                          LoginWorkexperienceLables.totalExperience,
                                          border: _experience_controller.text.isEmpty ? UnderlineInputBorder(borderSide:BorderSide(color:SemanticColors.blueGrey.withOpacity(0.3),width: 1 )): UnderlineInputBorder(borderSide:BorderSide(color:SemanticColors.dark_purpely,width: 1 ) ),
                                          textInputAction: TextInputAction.next,
                                          autovalidate: true,
                                          maxLength: 2,
                                          fontStyle: TextStyles.editTextStyle,
                                          lableStyle: TextStyles.labelStyle,
                                          onSaved: (value) => totalExperience = value,
                                          validator: validateExperience,
                                          onFieldSubmitted: (_) =>
                                              FocusScope.of(context)
                                                  .requestFocus(focus),
                                        ),
                                      ),
                                      Container(
                                        //color: Colors.red,
                                        width: double.infinity,
                                        //  height: screenHeight*49/720,
                                        margin: EdgeInsets.only(
                                            top: screenHeight * 19.5 / 720),
                                        child: TextInputFormField(
                                          controller: _organisation_controller,
                                          enabled: isSwitched ? false : true,
                                          focusNode: focus,
                                          keyboardType: TextInputType.text,
                                          border: _organisation_controller.text.isEmpty ? UnderlineInputBorder(borderSide:BorderSide(color:SemanticColors.blueGrey.withOpacity(0.3),width: 1 )): UnderlineInputBorder(borderSide:BorderSide(color:SemanticColors.dark_purpely,width: 1 ) ),
                                          lableText:
                                          LoginWorkexperienceLables.organisation,
                                              fontStyle: TextStyles.editTextStyle,
                                          lableStyle: TextStyles.labelStyle,
                                          textInputAction: TextInputAction.done,
                                          autovalidate: true,
                                         // onSaved: (value) => organisation = value,
                                          validator:
                                          validateOrganisation,
                                          //  onFieldSubmitted: (_) => FocusScope.of(context).requestFocus(),
                                        ),
                                      ),
                                      isSwitched ?
                                      Container(
                                        width: double.infinity,
                                        //  height: screenHeight*49/720,
                                        margin: EdgeInsets.only(
                                            top: screenHeight * 19.5 / 720),
                                        child: TextInputFormField(
                                            enabled: false,
                                            keyboardType: TextInputType.text,
                                            lableStyle: TextStyles.labelStyle,
                                            lableText:
                                            LoginWorkexperienceLables.sector,))

                                            :
                                      Container(
                                        //height: screenHeight*60/720,
                                        margin: EdgeInsets.only(
                                            top: screenHeight * 19 / 720),
                                        child: DropDownFormField(
                      getImmediateSuggestions: true,

                      // autoFlipDirection: true,
                      textFieldConfiguration: TextFieldConfiguration(
                          controller: _technology,
                          label: LoginWorkexperienceLables.sector),
                      suggestionsBoxDecoration: SuggestionsBoxDecoration(
                          dropDownHeight: 150,
                          borderRadius: new BorderRadius.circular(5.0),
                          color: Colors.white),
                      suggestionsCallback: (pattern) {
                        return TechnologyService.getSuggestions(pattern);
                      },
                      itemBuilder: (context, suggestion) {
                        return ListTile(
                          dense: true,
                          contentPadding: EdgeInsets.only(left: 10),
                          title: Text(
                            suggestion,
                            style: TextStyle(
                              fontFamily: "IBMPlexSans",
                              fontWeight: FontWeight.w400,
                              fontSize: (13 / 720) * screenHeight,
                              color: NeutralColors.bluey_grey,
                              textBaseline: TextBaseline.alphabetic,
                              letterSpacing: 0.0,
                              inherit: false,
                              
                            ),
                          ),
                        );
                      },
                      hideOnLoading: true,
                      debounceDuration: Duration(milliseconds: 100),
                      transitionBuilder: (context, suggestionsBox, controller) {
                        return suggestionsBox;
                      },
                      onSuggestionSelected: (suggestion) {
                        for (int i = 0; i < TechnologyService.technology.length; i++) {
                          if (TechnologyService.technology[i] == suggestion) {
                          }
                        }
                        _technology.text = suggestion;
                        // setState(() {});
                      },
                      onSaved: (value) => {},
                    )
                                      ),
                                    ],
                                  ),
                                ),
                                Container(
                                  // height: screenHeight*45/720,
                                  margin: EdgeInsets.only(
                                    top: 40,
                                  ),
                                  child: isSwitched ? PrimaryButtonGradient(
                                    text: "CONFIRM DETAILS",
                                    onTap: () {
                                  //    if ((FieldValidator.organisation == true &&
                                   //       FieldValidator.experience == true)) {
                                        AppRoutes.push(
                                            context, LoginConfirmaddressWidget());
                                    //  }
                                    },
                                  ) :PrimaryButton(
                                    text: "CONFIRM DETAILS",
                                    gradient: (organisationValidator == true &&
                                        experienceValidator == true)
                                        ? true
                                        : false,
                                    onTap: () {
                                      if ((organisationValidator == true &&
                                          experienceValidator == true)) {
                                        AppRoutes.push(
                                            context, LoginConfirmaddressWidget());
                                      }
                                    },
                                  ),
                                )
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

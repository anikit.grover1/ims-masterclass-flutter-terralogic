import 'package:flutter/material.dart';
import 'package:imsindia/components/custom_DropDown.dart';
import 'package:imsindia/views/Discussion/discussion_comments.dart' as prefix0;
import 'package:imsindia/views/login_page/login_education.dart';
import 'package:imsindia/views/login_page/login_educational_details.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../components/Input_forms.dart';
import '../../components/custom_expansion_panel.dart';
import '../../resources/strings/login.dart';
import '../../utils/colors.dart';
import '../../utils/text_styles.dart';
import '../../utils/validator.dart';
import 'data.dart';

const actualHeight = 925;
const actualWidth = 360;

var screenWidthTotal;
var screenHeightTotal;
var _safeAreaHorizontal;
var _safeAreaVertical;
var screenHeight;
var screenWidth;
bool isSwitched = false;
bool check=false;

class GraduationDetails extends StatefulWidget {
  @override
  _GraduationDetailsState createState() => _GraduationDetailsState();
}

class _GraduationDetailsState extends State<GraduationDetails> {
  final TextEditingController _degreeInGraduationDetails =
      new TextEditingController();

  final TextEditingController _yearInGraduationDetails =
      new TextEditingController();

  final TextEditingController _nameOfCollegeInGraduationDetails =
      new TextEditingController();

  final TextEditingController _gradeSystemInGraduationDetails =
      new TextEditingController();
  final TextEditingController _percentageInGraduationDetails =
      new TextEditingController();
  FocusNode grade = new FocusNode();
  FocusNode percentile = new FocusNode();
  @override
  void initState() {
    _degreeInGraduationDetails.clear();
    _yearInGraduationDetails.clear();
    _nameOfCollegeInGraduationDetails.clear();
    _gradeSystemInGraduationDetails.clear();
    _percentageInGraduationDetails.clear();
    setGraduationFilledFalse();
    _degreeInGraduationDetails.addListener(() {
      setState(() {
        checkGraduationDetails();
      });
    });
    _yearInGraduationDetails.addListener(() {
      setState(() {
        checkGraduationDetails();
      });
    });
    _nameOfCollegeInGraduationDetails.addListener(() {
      setState(() {
        checkGraduationDetails();
      });
    });
    _gradeSystemInGraduationDetails.addListener(() {
      setState(() {
        checkGraduationDetails();
      });
    });
    _percentageInGraduationDetails.addListener(() {
      setState(() {
        checkGraduationDetails();
      });
    });
    setState(() {
      isSwitched = false;
    });
  }

  void setGraduationFilledTrue() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setBool("graduationDetailsFilled", true);
  }

  void setGraduationFilledFalse() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setBool("graduationDetailsFilled", false);
  }

  checkGraduationDetails() async {
    setState(() {
      if ((_degreeInGraduationDetails.text != '' &&
              _gradeSystemInGraduationDetails.text != '' &&
              _nameOfCollegeInGraduationDetails.text != '' &&
              _yearInGraduationDetails.text != '' &&
              _percentageInGraduationDetails.text != '') ||
          isSwitched == true) {
        setGraduationFilledTrue();
      } else {
        setGraduationFilledFalse();
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    CalculateScreen(context);
    return Container(
      height: 270/720 * screenHeightTotal,
      margin: EdgeInsets.only(top: 0, right: 0),
      child: Stack(
        alignment: Alignment.center,
        children: [
          Positioned(
            left: 0,
            right: 0,
            child: Container(
              height: 270/720 * screenHeightTotal,
              decoration: BoxDecoration(
                color: Color.fromARGB(255, 255, 255, 255),
                boxShadow: [
                  BoxShadow(
                    color: Color.fromARGB(255, 234, 234, 234),
                    offset: Offset(0, 0),
                    blurRadius: 10,
                  ),
                ],
                borderRadius: BorderRadius.all(Radius.circular(5)),
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  Align(
                    alignment: Alignment.topLeft,
                    child: Container(
                      width: 150/360 * screenWidthTotal,
                      height: 20/720 * screenHeightTotal,
                      margin: EdgeInsets.only(left: 15/360 * screenWidthTotal, top: 14/720 * screenHeightTotal),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(
                            margin: EdgeInsets.only(top: 0),
                            child: Text(
                              "Not Applicable",
                              style: TextStyle(
                                  color: NeutralColors.gunmetal,
                                  fontWeight: FontWeight.w400,
                                  fontFamily: "IBMPlexSans",
                                  fontStyle: FontStyle.normal,
                                  
                                  fontSize: (12 / 360) * screenWidth),
                                  
                              textAlign: TextAlign.center,
                            ),
                          ),
                          Transform.scale(
                            scale: 0.78,
                            child: Container(
                              child: Switch(
                                value: isSwitched,
                                onChanged: (value) {
                                  setState(() {
                                    isSwitched = value;
                                    _degreeInGraduationDetails.text='';
                                    _gradeSystemInGraduationDetails.text='';
                                    _nameOfCollegeInGraduationDetails.text='';
                                    _percentageInGraduationDetails.text='';
                                    _yearInGraduationDetails.text='';
                                    checkGraduationDetails();
                                  });
                                },
                                activeTrackColor: SemanticColors.iceBlue,
                                activeColor: NeutralColors.mango,
                                inactiveThumbColor: NeutralColors.blue_grey,
                                inactiveTrackColor: SemanticColors.iceBlue,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  Container(
                    height: 49/720 * screenHeightTotal,
                    margin: EdgeInsets.only(left: 15/360 * screenWidthTotal, top: 20/720 * screenHeightTotal, right: 15/360 * screenWidthTotal),
                    child: TextInputFormField(
                        enabled: isSwitched ? false : true,
                        controller: _nameOfCollegeInGraduationDetails,
                        keyboardType: TextInputType.text,
                        lableText: LoginAppLabels.nameOfCollege,
                           lableStyle: TextStyles.labelStyle,
                           fontStyle: TextStyles.loginAlternativeOption,
                        border: _nameOfCollegeInGraduationDetails.text.isEmpty ? UnderlineInputBorder(borderSide:BorderSide(color:SemanticColors.blueGrey.withOpacity(0.3),width: 1 )): UnderlineInputBorder(borderSide:BorderSide(color:SemanticColors.dark_purpely,width: 1 ) ),
                        textInputAction: TextInputAction.done,
                        onFieldSubmitted: (_) {
                          FocusScope.of(context).requestFocus(new FocusNode());
                        }),
                  ),
                ],
              ),
            ),
          ),
          Positioned(
            left: 15/360 * screenWidthTotal,
            top: (110/720)*screenHeightTotal,
            right: 15/360 * screenWidthTotal,
            child: Container(
              height: (140/720)*screenHeightTotal,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  Container(
                    height: (60/720)*screenHeightTotal,
                    margin: EdgeInsets.only(left: 3/360 * screenWidthTotal, top: 3/720 * screenHeightTotal, right: 3/360 * screenWidthTotal),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Expanded(
                            child: Container(
                          width: (133 / 360) * screenWidthTotal,
                          //height: (50 / 720) * screenHeightTotal,
                          margin: EdgeInsets.only(top: 0, bottom: 0),
                          child: isSwitched
                              ? new Container(
                                margin:  EdgeInsets.only(top: (13/ 720) * screenHeightTotal, bottom: 0),
                                child:TextInputFormField(
                                 fontStyle: TextStyles.loginAlternativeOption,
                                 lableStyle: TextStyles.labelStyle,
                                  enabled: false,
                                  lableText: "Degree",
                                ))
                              : new DropDownFormField(
                                  getImmediateSuggestions: true,
                                  closedropDown:true,
                                  textFieldConfiguration:
                                      TextFieldConfiguration(
                                   label: 'Degree',
                                    controller: this._degreeInGraduationDetails,
                                  ),
                                   suggestionsBoxDecoration: SuggestionsBoxDecoration(
                          dropDownHeight: 150,
                          borderRadius: new BorderRadius.circular(5.0),
                          color: Colors.white),
                                  suggestionsCallback: (pattern) {
                                    return DegreeService.getSuggestions(
                                        pattern);
                                  },
                                  itemBuilder: (context, suggestion) {
                                    return ListTile(
                          dense: true,
                          contentPadding: EdgeInsets.only(left: 10),
                          title: Text(
                            suggestion,
                            style: TextStyle(
                              fontFamily: "IBMPlexSans",
                              fontWeight: FontWeight.w400,
                              fontSize: (13 / 720) * screenHeight,
                              color: NeutralColors.bluey_grey,
                              textBaseline: TextBaseline.alphabetic,
                              letterSpacing: 0.0,
                              inherit: false,
                              
                            ),
                          ),
                        );
                                  },
                                  transitionBuilder:
                                      (context, suggestionsBox, controller) {
                                    return suggestionsBox;
                                  },
                                  onSuggestionSelected: (suggestion) {
                                    check=true;
                                    this._degreeInGraduationDetails.text =
                                        suggestion;
                                    setState(() {});

                                    // this._typeAheadController.dispose();
                                  },
                                  onSaved: (value) => {print("something")},
                                ),
                        )),
                        Container(
                          width: (70 / 360) * screenWidthTotal,
                          //height: (50 / 720) * screenHeightTotal,
                          margin: EdgeInsets.only(
                            left: 10/360 * screenWidthTotal,
                            top: grade.hasFocus || _yearInGraduationDetails.text.isNotEmpty || isSwitched ?13/720*screenHeightTotal:0
                          ),
                          child: TextInputFormField(
                              enabled: isSwitched ? false : true,
                              controller: _yearInGraduationDetails,
                              focusNode: grade,
                              keyboardType: TextInputType.number,
                              lableText: "Year",
                              maxLength: 4,
                                 lableStyle: TextStyles.labelStyle,
                              fontStyle: TextStyles.loginAlternativeOption,
                              border: _yearInGraduationDetails.text.isEmpty ? UnderlineInputBorder(borderSide:BorderSide(color:SemanticColors.blueGrey.withOpacity(0.3),width: 1 )): UnderlineInputBorder(borderSide:BorderSide(color:SemanticColors.dark_purpely,width: 1 ) ),
                              textInputAction: TextInputAction.done,
                              onSaved: (value) => {
                                    // _gradeValue = value,
                                  },
                              onFieldSubmitted: (_) {
                                FocusScope.of(context)
                                    .requestFocus(new FocusNode());
                              }),
                        )
                      ],
                    ),
                  ),
                  Container(
                    height: 60/720 * screenHeightTotal,
                    margin: EdgeInsets.only(left: 3/360 * screenWidthTotal, top: 13/720 * screenHeightTotal, right: 3/360 * screenWidthTotal),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Expanded( 
                            child: Container(
                          width: (133 / 360) * screenWidthTotal,
                          //height: (50 / 720) * screenHeightTotal,
                          margin: EdgeInsets.only(top: 0, bottom: 0),
                          child: isSwitched
                              ? new Container(
                                margin:  EdgeInsets.only(top: (13/ 720) * screenHeightTotal, bottom: 0),
                                child:new TextInputFormField(
                                  enabled: false,
                                    fontStyle: TextStyles.loginAlternativeOption,
                                 lableStyle: TextStyles.labelStyle,
                                  lableText: "Grade System",
                                ))
                              : new DropDownFormField(
                                
                                  getImmediateSuggestions: true, 
                                  textFieldConfiguration:
                                      TextFieldConfiguration(
                                        label: "Grade System",
                                                                       controller:
                                        this._gradeSystemInGraduationDetails,
                                  ),
                                   suggestionsBoxDecoration: SuggestionsBoxDecoration(
                          dropDownHeight: 100,
                          borderRadius: new BorderRadius.circular(5.0),
                          color: Colors.white),
                                  suggestionsCallback: (pattern) {
                                    return GradeService.getSuggestions(pattern);
                                  },
                                  itemBuilder: (context, suggestion) {
                                    return ListTile(
                          dense: true,
                          contentPadding: EdgeInsets.only(left: 10),
                          title: Text(
                            suggestion,
                            style: TextStyle(
                              fontFamily: "IBMPlexSans",
                              fontWeight: FontWeight.w400,
                              fontSize: (13 / 720) * screenHeight,
                              color: NeutralColors.bluey_grey,
                              textBaseline: TextBaseline.alphabetic,
                              letterSpacing: 0.0,
                              inherit: false,
                              
                            ),
                          ),
                        );
                                  },
                                  transitionBuilder:
                                      (context, suggestionsBox, controller) {
                                    return suggestionsBox;
                                  },
                                  onSuggestionSelected: (suggestion) {
                                    this._gradeSystemInGraduationDetails.text =
                                        suggestion;
                                    setState(() {});

                                    // this._typeAheadController.dispose();
                                  },
                                  onSaved: (value) => {},
                                ),
                        )),
                        Container(
                          width: (70 / 360) * screenWidthTotal,
                          //height: (50 / 720) * screenHeightTotal,
                          margin: EdgeInsets.only(left: 10/360 * screenWidthTotal,top: percentile.hasFocus || _percentageInGraduationDetails.text.isNotEmpty || isSwitched?13/720*screenHeightTotal:0),
                          child: TextInputFormField(
                            enabled: isSwitched ? false : true,
                            controller: _percentageInGraduationDetails,
                            focusNode: percentile,
                            keyboardType: TextInputType.number,
                            lableText: 'Percentage',
                            maxLength: 2,
                               lableStyle: TextStyles.labelStyle,
                              fontStyle: TextStyles.loginAlternativeOption,
                            border: _percentageInGraduationDetails.text.isEmpty ? UnderlineInputBorder(borderSide:BorderSide(color:SemanticColors.blueGrey.withOpacity(0.3),width: 1 )): UnderlineInputBorder(borderSide:BorderSide(color:SemanticColors.dark_purpely,width: 1 ) ),
                            textInputAction: TextInputAction.done,
                            onSaved: (value) => {},
                            //  _gradeValue = value,
                            onFieldSubmitted: (_) {
                              FocusScope.of(context)
                                  .requestFocus(new FocusNode());
                            },
                          ),
                        )
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}

void CalculateScreen(BuildContext context) {
  var _mediaQueryData = MediaQuery.of(context);
  screenWidthTotal = _mediaQueryData.size.width;
  screenHeightTotal = _mediaQueryData.size.height;
  _safeAreaHorizontal =
      _mediaQueryData.padding.left + _mediaQueryData.padding.right;
  _safeAreaVertical =
      _mediaQueryData.padding.top + _mediaQueryData.padding.bottom;

  screenHeight = screenHeightTotal - _safeAreaVertical;
  screenWidth = screenWidthTotal - _safeAreaHorizontal;
}

import 'package:flutter/material.dart';
import 'package:imsindia/components/primary_Button.dart';
import 'package:imsindia/components/primary_button_gradient.dart';
import 'package:imsindia/components/primary_button_without_gradient.dart';
import 'package:imsindia/routers/routes.dart';
import 'package:imsindia/utils/svg_icons.dart';
import 'package:imsindia/utils/text_styles.dart';
import 'package:imsindia/views/login_page/login_screen.dart';
import 'package:imsindia/views/login_page/three_step_login.dart';
import 'package:imsindia/views/splash_screen/splash_screen.dart';
import '../../resources/strings/login.dart';
import '../../utils/validator.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:imsindia/utils/colors.dart';
import 'package:imsindia/views/login_page/login_createaccountfilled_widget.dart';
import 'package:imsindia/views/home_pages/home_widget.dart';
//import 'package:ims_login3/screens/login_confirmdetails_widget.dart';

List titles = [
  'Yes, I have an enrollment ID',
  'No, I am a New User',
];

class LoginEnrollmentidWidget extends StatefulWidget {
  @override
  LoginEnrollmentidWidgetState createState() => LoginEnrollmentidWidgetState();
}

class LoginEnrollmentidWidgetState extends State<LoginEnrollmentidWidget> {
  void onArrowPointToRightPressed(BuildContext context) => Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => LoginFillpasswordWidget()));
  String _enroll = "";
  bool isSelected = false;
  int _selectedIndex;

  _onSelected(int index) {
    setState(() => _selectedIndex = index);
  }

  @override
  Widget build(BuildContext context) {
    final screenHeight = MediaQuery.of(context).size.height;
    final screenWidth = MediaQuery.of(context).size.width;
    Future<bool> _onWillPop() {
      AppRoutes.push(context, SplashScreen());
    }

    return WillPopScope(
      onWillPop: _onWillPop,
      child: Scaffold(
        
        
        body: Container(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              Container(
                height: screenHeight*0.140,
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    GestureDetector(
                      onTap:(){
                        AppRoutes.push(context, SplashScreen());
                      },
                      child: Container(
                        child: Padding(
                          padding: const EdgeInsets.all(5.0),
                          child: Align(
                            alignment: Alignment.topLeft,
                            child: Container(
                              width: (20/360)*screenWidth,
                              height: (20/720)*screenHeight,
                              margin: EdgeInsets.only(top: screenHeight*0.0738,left: screenWidth*0.062,right: 5),
                                  child: getSvgIcon.backSvgIcon,
                            ),
                          ),
                        ),
                      ),
                    ),
                    Spacer(),
                    Align(
                      alignment: Alignment.topRight,
                      child: Container(
                        //   color: Color.fromRGBO(255, 45, 52, 56),

                        width: screenWidth*0.780,
                        height: screenHeight*0.9,
                        child: getSvgIcon.combinedshapeSvgIcon,
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                margin: EdgeInsets.only(
                    left: 62 / 360 * screenWidth,
                    top: 56 / 720 * screenHeight,
                    right: 62 / 360 * screenWidth),
                child: Center(
                  //alignment: Alignment.topCenter,
                  child: Text(
                    "Have you enrolled for an IMS program?",
                    style: TextStyle(
                      color: Color.fromARGB(255, 45, 52, 56),
                      fontSize: 21 / 720 * screenHeight,
                      fontFamily: "IBMPlexSans",
                      fontWeight: FontWeight.w700,
                    ),
                    textAlign: TextAlign.center,
                  ),
                ),
              ),
              Container(
                  height: 170 / 720 * screenHeight,
                  margin: EdgeInsets.only(
                      left: 45 / 360 * screenWidth,
                      top: 40 / 720 * screenHeight,
                      right: 45 / 360 * screenWidth),
                  child: new ListView.builder(
                    itemBuilder: (BuildContext context, int index) {
                      return GestureDetector(
                        onTap: () {
                          _onSelected(index);
                          isSelected = true;
                          _enroll = titles[index];
                          print(_enroll);
                        },
                        child: new Container(
                          //width: (320/360)*screenWidth,
                          height: 50 / 720 * screenHeight,
                          margin: EdgeInsets.only(top: 15 / 720 * screenHeight),
//                      color: _selectedIndex != null && _selectedIndex == index
//                          ? const Color(0xFFff982a).withOpacity(0.15)
//                          : Color.fromARGB(255, 242, 244, 244).withOpacity(0.15),
                          decoration: _selectedIndex != null &&
                                  _selectedIndex == index
                              ? BoxDecoration(
                                  border: Border.fromBorderSide(
                                    BorderSide(color: Colors.transparent),
                                  ),
                                //  color: NeutralColors.off_white,
                            color: Color.fromARGB(13, 255, 152, 42),
                            borderRadius:
                                      BorderRadius.all(Radius.circular(5.0)),
                                )
                              : BoxDecoration(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(5.0)),
                                  border: Border.fromBorderSide(
                                    BorderSide(color: NeutralColors.ice_blue),
                                  ),
                                ),
                          child: Center(
                            child: Container(
                              child: new Text(
                                titles[index],
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                  color: _selectedIndex != null &&
                                          _selectedIndex == index
                                      ? NeutralColors.mango
                                      : NeutralColors.dark_navy_blue,
                                  fontSize: 14 / 360 * screenWidth,
                                  fontFamily: "IBMPlexSans",
                                ),
                              ),
                            ),
                          ),
                        ),
                      );
                    },
                    physics: const NeverScrollableScrollPhysics(),
                    itemCount: titles.length,
                  )),
//          !isSelected ? InkWell(
//            onTap:  () {},
//            child: Container(
//              width:screenWidth * 270/360,
//              height: screenHeight * 40/720,
//              margin: EdgeInsets.only(
//                  left: 45 / 360 * screenWidth,
//                  top: 40 / 720 * screenHeight,
//                  right: 45 / 360 * screenWidth),
//              child: Center(
//                child: Text(
//                  LoginAppStrings.nextButton,
//                  style: TextStyles.buttonTextStyle,
//                ),
//              ),
//              decoration: BoxDecoration(
//                gradient: LinearGradient(
//                    colors: [SemanticColors.iceBlue, SemanticColors.iceBlue]),
//                borderRadius: BorderRadius.all(Radius.circular(2)),
//              ),
//            ),
//          ) :
              Container(
                height: 50 / 720 * screenHeight,
                margin: EdgeInsets.only(
                    left: 45 / 360 * screenWidth,
                    top: 40 / 720 * screenHeight,
                    right: 45 / 360 * screenWidth),
                child: isSelected
                    ? Center(
                        child: PrimaryButtonGradient(
                          onTap: () {
                            if (_enroll == titles[0]) {
                              AppRoutes.push(
                                  context, LoginCreateaccountfilledWidget());
                            } else {
                              AppRoutes.push(context, ThreeStepLogin());
                            }
                          },
                          text: LoginAppStrings.nextButton.toUpperCase(),
                        ),
                      )
                    : Center(
                        child: PrimaryButtonWithoutGradient(
                          onTap: () {},
                          text: LoginAppStrings.nextButton.toUpperCase(),
                        ),
                      ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

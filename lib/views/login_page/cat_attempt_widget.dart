// import 'package:flutter/foundation.dart';
// import 'package:flutter/material.dart';
// import 'package:flutter_typeahead/flutter_typeahead.dart';
// import 'package:imsindia/views/login_page/login_educational_details.dart';

// import '../../components/Input_forms.dart';
// import '../../components/custom_expansion_panel.dart';
// import '../../resources/strings/login.dart';
// import '../../utils/colors.dart';
// import '../../utils/text_styles.dart';
// import '../../utils/validator.dart';
// import 'data.dart';
// final keyB = new GlobalKey<CatAttemptWidgetLoginState>();

// const actualHeight = 925;
// const actualWidth = 360;

// var screenWidthTotal;
// var screenHeightTotal;
// var _safeAreaHorizontal;
// var _safeAreaVertical;
// var screenHeight;
// var screenWidth;
// var indexInTenth='';


// class CatAttemptWidgetLogin extends StatefulWidget {
//   CatAttemptWidgetLogin({Key key}) : super(key: key);


//   @override
//   CatAttemptWidgetLoginState createState() => CatAttemptWidgetLoginState();
// }

// class CatAttemptWidgetLoginState extends State<CatAttemptWidgetLogin> {
//   final TextEditingController _stateInTenthDetails =      new TextEditingController();
//   final TextEditingController _gradeInTenthDetails =      new TextEditingController();
//   @override
//   void initState(){


//   }
//   @override
//   Widget build(BuildContext context) {
//     CalculateScreen(context);
//     return Container(
//       height: (70 / 720) * screenHeight,
//       margin: EdgeInsets.only(
//           left: 15,
//           top: (11 / 720) * screenHeight,
//           right: 15),
//       child: Row(
//         crossAxisAlignment: CrossAxisAlignment.start,
//         children: [
//           Expanded(
//               child: Container(
//                 width: (140 / 360) * screenWidthTotal,
//                 height: (49 / 720) * screenHeightTotal,
//                 margin: EdgeInsets.only(top: 0, bottom: 0),
//                 child: TypeAheadFormField(
//                   getImmediateSuggestions: true,
//                   textFieldConfiguration:
//                   TextFieldConfiguration(
//                     cursorColor: Colors.white,
//                     cursorWidth: 0.0,
//                     decoration: InputDecoration(
//                         focusedBorder: UnderlineInputBorder(
//                             borderSide: BorderSide(
//                                 color:
//                                 SemanticColors.dark_purpely,
//                                 width: 1.5)),
//                         errorStyle: TextStyles.errorTextStyle,
//                         labelText: 'Year',
//                         labelStyle: TextStyles.labelStyle,
//                         contentPadding: EdgeInsets.symmetric(
//                             vertical: 10.0),
//                         suffixIcon: IconButton(
//                             icon: new Image.asset(
//                                 "assets/images/shape-copy-3-2.png"),
//                             iconSize: 5,
//                             onPressed: () {})
//                       //border: const UnderlineInputBorder(),
//                     ),
//                     controller: this._stateInTenthDetails,
//                   ),
//                   suggestionsCallback: (pattern) {
//                     return StatesService.getSuggestions(
//                         pattern);
//                   },
//                   itemBuilder: (context, suggestion) {
//                     return ListTile(
//                       title: Text(suggestion),
//                     );
//                   },
//                   transitionBuilder:
//                       (context, suggestionsBox, controller) {
//                     return suggestionsBox;
//                   },
//                   onSuggestionSelected: (suggestion) {
//                     this._stateInTenthDetails.text = suggestion;
//                     setState(() {});

//                     // this._typeAheadController.dispose();
//                   },
//                 ),
//               )),
//           Container(
//             width: (80 / 360) * screenWidthTotal,
//             height: (49 / 720) * screenHeightTotal,
//             margin: EdgeInsets.only(left: 20),
//             child: TextInputFormField(
//               controller: _gradeInTenthDetails,
//               keyboardType: TextInputType.number,
//               lableText: CreateProfileLabels.percentile,
//               textInputAction: TextInputAction.done,
//               autovalidate: true,
//               // validator: FieldValidator.validateEmail,
//               validator: FieldValidator.validateGrade,
//               onSaved: (value) {
//               },
//               onEditingComplete: () {
//               },

//               onFieldSubmitted: (val) {
//                 FocusScope.of(context)
//                     .requestFocus(new FocusNode());
//               },
//             ),
//           )
//         ],
//       ),
//     );
//   }
// }
// void CalculateScreen(BuildContext context) {
//   var _mediaQueryData = MediaQuery.of(context);
//   screenWidthTotal = _mediaQueryData.size.width;
//   screenHeightTotal = _mediaQueryData.size.height;
//   _safeAreaHorizontal =
//       _mediaQueryData.padding.left + _mediaQueryData.padding.right;
//   _safeAreaVertical =
//       _mediaQueryData.padding.top + _mediaQueryData.padding.bottom;

//   screenHeight = screenHeightTotal - _safeAreaVertical;
//   screenWidth = screenWidthTotal - _safeAreaHorizontal;
// }
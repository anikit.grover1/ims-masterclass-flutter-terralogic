import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:imsindia/components/custom_DropDown.dart';
import 'package:imsindia/views/login_page/login_education.dart';
import 'package:imsindia/views/login_page/login_educational_details.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../components/Input_forms.dart';
import '../../components/custom_expansion_panel.dart';
import '../../resources/strings/login.dart';
import '../../utils/colors.dart';
import '../../utils/text_styles.dart';
import '../../utils/validator.dart';
import 'data.dart';
//final keyTenth = new GlobalKey<TenthClassDetailsState>();

const actualHeight = 925;
const actualWidth = 360;

var screenWidthTotal;
var screenHeightTotal;
var _safeAreaHorizontal;
var _safeAreaVertical;
var screenHeight;
var screenWidth;
var indexInTenth = '';


class TenthClassDetails extends StatefulWidget {
  // TenthClassDetails({Key key}) : super(key: key);

  @override
  TenthClassDetailsState createState() => TenthClassDetailsState();
}

class TenthClassDetailsState extends State<TenthClassDetails> {
  @override
  void initState() {
    _gradeInTenthDetails.clear();
    _stateInTenthDetails.clear();
    setTenthFilledFalse();
    //  print('some'+_gradeInTenthDetails.text.toString());
    _gradeInTenthDetails.addListener(() {
      setState(() {
        checkTenthDetails();
      });
    });
    _stateInTenthDetails.addListener(() {
      setState(() {
        checkTenthDetails();
      });
    });
    setState(() {
      checkTenthDetails();
    });
  }

  final TextEditingController _stateInTenthDetails =
      new TextEditingController();
  final TextEditingController _gradeInTenthDetails =
      new TextEditingController();
  FocusNode _focusNode = new FocusNode();

  void setTenthFilledTrue() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setBool("tenthDetailsFilled", true);
  }

  void setTenthFilledFalse() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setBool("tenthDetailsFilled", false);
  }

  checkTenthDetails() async {
    setState(() {
      if (_gradeInTenthDetails.text != '' &&
          _stateInTenthDetails.text != '' &&
          _gradeInTenthDetails.text != null &&
          _stateInTenthDetails.text != null) {
        setTenthFilledTrue();
      } else {
        setTenthFilledFalse();
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    CalculateScreen(context);
    return Container(
      height: (70 / 720) * screenHeight,
      decoration: BoxDecoration(
        color: Color.fromARGB(255, 255, 255, 255),
        boxShadow: [
          BoxShadow(
            color: Color.fromARGB(255, 234, 234, 234),
            offset: Offset(0, 0),
            blurRadius: 10,
          ),
        ],
        borderRadius: BorderRadius.all(Radius.circular(5)),
      ),
      // margin: EdgeInsets.only(
      //     left: 15,
      //     top: (11 / 720) * screenHeight,
      //     right: 15),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
                width: (140 / 360) * screenWidthTotal,
                //height: (50 / 720) * screenHeightTotal,
                margin: EdgeInsets.only(left: 15/360 * screenWidthTotal, right: 15/360 * screenWidthTotal,top:5/720 * screenHeightTotal),
                child: DropDownFormField(
                  getImmediateSuggestions: true,
                  textFieldConfiguration: TextFieldConfiguration(
                   label: "State",
                    controller: this._stateInTenthDetails,
                  ),
                  suggestionsCallback: (pattern) {
                    return StatesService.getSuggestions(pattern);
                  },
                   suggestionsBoxDecoration: SuggestionsBoxDecoration(
                              dropDownHeight: 150,
                              borderRadius: new BorderRadius.circular(5.0),
                              color: Colors.white),
                  itemBuilder: (context, suggestion) {
                    return ListTile(
                              dense: true,
                              contentPadding: EdgeInsets.only(left: 10/360 * screenWidthTotal),
                              title: Text(
                                suggestion,
                                style: TextStyle(
                                  fontFamily: "IBMPlexSans",
                                  fontWeight: FontWeight.w400,
                                  fontSize: (13 / 720) * screenHeight,
                                  color: NeutralColors.bluey_grey,
                                  textBaseline: TextBaseline.alphabetic,
                                  letterSpacing: 0.0,
                                  inherit: false,

                                ),
                              ),
                            );
                  },
                  transitionBuilder: (context, suggestionsBox, controller) {
                    return suggestionsBox;
                  },
                  onSuggestionSelected: (suggestion) {
                    this._stateInTenthDetails.text = suggestion;
                    setState(() {
                      checkTenthDetails();
                    });

                    // this._typeAheadController.dispose();
                  },
                ),
              ),
          Container(
            width: (70 / 360) * screenWidthTotal,
            //height: (60 / 720) * screenHeightTotal,
            margin: EdgeInsets.only(right: 15/360 * screenWidthTotal,top:_focusNode.hasFocus || _gradeInTenthDetails.text.isNotEmpty?15/720 * screenHeightTotal:5/720*screenHeightTotal),
            child: TextInputFormField(
              focusNode: _focusNode,
              keyboardType: TextInputType.text,
              lableText: LoginAppLabels.grade,
              controller: _gradeInTenthDetails,
              textInputAction: TextInputAction.done,
              maxLength: 1,
             lableStyle: TextStyles.labelStyle,
             fontStyle: TextStyles.editTextStyle,
              border: _gradeInTenthDetails.text.isEmpty ? UnderlineInputBorder(borderSide:BorderSide(color:SemanticColors.blueGrey.withOpacity(0.3),width: 1 )): UnderlineInputBorder(borderSide:BorderSide(color:SemanticColors.dark_purpely,width: 1 ) ),
              autovalidate: true,
              validator: FieldValidator.validateGrade,
              onSaved: (value) {
                //   FocusScope.of(context).requestFocus(new FocusNode());
              },
              onEditingComplete: () {
                setState(() {});
              },
//                                    onFieldSubmitted: (value)
//                                    {
//                                    setState(() {
//                                    value=gradeTenth;
//                                    print("asd");
//                                    print(value);
//                                    print(gradeTenth);
//                                    print("tenth grade on submited");
//                                    });
//                                    }

              onFieldSubmitted: (val) {
                FocusScope.of(context).requestFocus(new FocusNode());
              },
            ),
          )
        ],
      ),
    );
  }
}

void CalculateScreen(BuildContext context) {
  var _mediaQueryData = MediaQuery.of(context);
  screenWidthTotal = _mediaQueryData.size.width;
  screenHeightTotal = _mediaQueryData.size.height;
  _safeAreaHorizontal =
      _mediaQueryData.padding.left + _mediaQueryData.padding.right;
  _safeAreaVertical =
      _mediaQueryData.padding.top + _mediaQueryData.padding.bottom;

  screenHeight = screenHeightTotal - _safeAreaVertical;
  screenWidth = screenWidthTotal - _safeAreaHorizontal;
}

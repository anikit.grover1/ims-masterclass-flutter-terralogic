import 'package:flutter/material.dart';
import 'package:imsindia/components/custom_DropDown.dart';
import 'package:imsindia/views/login_page/login_educational_details.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../components/Input_forms.dart';
import '../../components/custom_expansion_panel.dart';
import '../../resources/strings/login.dart';
import '../../utils/colors.dart';
import '../../utils/text_styles.dart';
import '../../utils/validator.dart';
import 'data.dart';

const actualHeight = 925;
const actualWidth = 360;

var screenWidthTotal;
var screenHeightTotal;
var _safeAreaHorizontal;
var _safeAreaVertical;
var screenHeight;
var screenWidth;

class TwelthClassDetails extends StatefulWidget {
  @override
  _TwelthClassDetailsState createState() => _TwelthClassDetailsState();
}

class _TwelthClassDetailsState extends State<TwelthClassDetails> {
  final TextEditingController _stateInTwelthDetails =
      new TextEditingController();
  final TextEditingController _gradeInTwelthDetails =
      new TextEditingController();
  FocusNode _focusNode = new FocusNode();

  @override
  void initState() {
    _gradeInTwelthDetails.clear();
    _stateInTwelthDetails.clear();
    setTwelthFilledFalse();
    _gradeInTwelthDetails.addListener(() {
      setState(() {
        checkTwelthDetails();
      });
    });
    _stateInTwelthDetails.addListener(() {
      setState(() {
        checkTwelthDetails();
      });
    });
    setState(() {
      checkTwelthDetails();
    });
  }

  void setTwelthFilledTrue() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    print('inside shared');
    prefs.setBool("twelthDetailsFilled", true);
  }

  void setTwelthFilledFalse() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    print('inside else');
    prefs.setBool("twelthDetailsFilled", false);
  }

  checkTwelthDetails() async {
    setState(() {
      if (_gradeInTwelthDetails.text != '' &&
          _stateInTwelthDetails.text != '' &&
          _gradeInTwelthDetails.text != null &&
          _stateInTwelthDetails.text != null) {
        setTwelthFilledTrue();
      } else {
        setTwelthFilledFalse();
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    CalculateScreen(context);
    return Container(
      height: (70 / 720) * screenHeight,
      decoration: BoxDecoration(
        color: Color.fromARGB(255, 255, 255, 255),
        boxShadow: [
          BoxShadow(
            color: Color.fromARGB(255, 234, 234, 234),
            offset: Offset(0, 0),
            blurRadius: 10,
          ),
        ],
        borderRadius: BorderRadius.all(Radius.circular(5)),
      ),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            width: (140 / 360) * screenWidthTotal,
            //height: (50 / 720) * screenHeightTotal,
            margin: EdgeInsets.only(left: 15/360 * screenWidthTotal, right: 15/360 * screenWidthTotal,top: 5/720 * screenHeightTotal),
            child: DropDownFormField(
              getImmediateSuggestions: true,
              textFieldConfiguration: TextFieldConfiguration(
              label: "State",
                controller: this._stateInTwelthDetails,
              ),
               suggestionsBoxDecoration: SuggestionsBoxDecoration(
                          dropDownHeight: 150,
                          borderRadius: new BorderRadius.circular(5.0),
                          color: Colors.white),
              suggestionsCallback: (pattern) {
                return StatesService.getSuggestions(pattern);
              },
              itemBuilder: (context, suggestion) {
                return ListTile(
                          dense: true,
                          contentPadding: EdgeInsets.only(left: 10/360 * screenWidthTotal),
                          title: Text(
                            suggestion,
                            style: TextStyle(
                              fontFamily: "IBMPlexSans",
                              fontWeight: FontWeight.w400,
                              fontSize: (13 / 720) * screenHeight,
                              color: NeutralColors.bluey_grey,
                              textBaseline: TextBaseline.alphabetic,
                              letterSpacing: 0.0,
                              inherit: false,
                              
                            ),
                          ),
                        );
              },
              transitionBuilder: (context, suggestionsBox, controller) {
                return suggestionsBox;
              },
              onSuggestionSelected: (suggestion) {
                this._stateInTwelthDetails.text = suggestion;
                setState(() {});

                // this._typeAheadController.dispose();
              },
              onSaved: (value) => {},
            ),
          ),
          Container(
            width: (70 / 360) * screenWidthTotal,
            //height: (50 / 720) * screenHeightTotal,
            margin: EdgeInsets.only(right: 15/360 * screenWidthTotal,top: _focusNode.hasFocus || _gradeInTwelthDetails.text.isNotEmpty?15/720 * screenHeightTotal:5/720*screenHeightTotal),
            child: TextInputFormField(
              focusNode: _focusNode,
                controller: _gradeInTwelthDetails,
                keyboardType: TextInputType.text,
                lableText: LoginAppLabels.grade,
                maxLength: 1,
               lableStyle: TextStyles.labelStyle,
             fontStyle: TextStyles.editTextStyle,
                border: _gradeInTwelthDetails.text.isEmpty ? UnderlineInputBorder(borderSide:BorderSide(color:SemanticColors.blueGrey.withOpacity(0.3),width: 1 )): UnderlineInputBorder(borderSide:BorderSide(color:SemanticColors.dark_purpely,width: 1 ) ),
                textInputAction: TextInputAction.done,
                onSaved: (value) {
                  //   FocusScope.of(context).requestFocus(new FocusNode());
                },
                onFieldSubmitted: (_) {
                  FocusScope.of(context).requestFocus(new FocusNode());
                }),
          )
        ],
      ),
    );
  }
}

void CalculateScreen(BuildContext context) {
  var _mediaQueryData = MediaQuery.of(context);
  screenWidthTotal = _mediaQueryData.size.width;
  screenHeightTotal = _mediaQueryData.size.height;
  _safeAreaHorizontal =
      _mediaQueryData.padding.left + _mediaQueryData.padding.right;
  _safeAreaVertical =
      _mediaQueryData.padding.top + _mediaQueryData.padding.bottom;

  screenHeight = screenHeightTotal - _safeAreaVertical;
  screenWidth = screenWidthTotal - _safeAreaHorizontal;
}

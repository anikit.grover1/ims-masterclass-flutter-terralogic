import 'package:flutter/material.dart';
import 'package:flutter/material.dart';
import 'package:imsindia/components/create_profile_button.dart';
import 'package:imsindia/components/custom_DropDown.dart';

import 'package:imsindia/components/custom_expansion_panel.dart';
import 'package:imsindia/components/primary_button_gradient.dart';
import 'package:imsindia/resources/strings/login.dart';
import 'package:imsindia/utils/svg_icons.dart';
import 'package:imsindia/views/login_page/login_enrollmentid_widget.dart';
import 'package:imsindia/views/login_page/login_fifthattempt_details.dart';
import 'package:imsindia/views/login_page/login_firstattempt_details.dart';
import 'package:imsindia/views/login_page/login_fourthattempt_details.dart';
import 'package:imsindia/views/login_page/login_secondattempt_details.dart';
import 'package:imsindia/views/login_page/login_thirdattempt_details.dart';
import 'package:imsindia/views/login_page/three_step_login.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../../components/Input_forms.dart';
import '../../components/primary_Button.dart';
import '../../components/login_appbar.dart';
import '../../components/cat_attempt_widget.dart';
import '../../resources/strings/login.dart';
import '../../utils/validator.dart';
import '../../utils/colors.dart';
import '../../utils/text_styles.dart';
import 'package:image_picker/image_picker.dart';
import 'dart:convert';
import './login_congratulations.dart';
import '../../routers/routes.dart';

import 'dart:io';

import 'data.dart';

class LoginCreateProfile extends StatefulWidget {
  @override
  _LoginCreateProfileState createState() => _LoginCreateProfileState();
}

final TextEditingController _name_controller = TextEditingController();
final TextEditingController _college_controller = new TextEditingController();
final TextEditingController _highestQualification = new TextEditingController();
final TextEditingController _year = new TextEditingController();
final TextEditingController _percentage = new TextEditingController();

String get name => _name_controller.text;

String get collegeName => _college_controller.text;
bool collegeValidator = false;
bool isSwitched = false;
bool expandFlag = false;
bool isActive = false;
bool oneFlag = false,
    twoFlag = false,
    threeFlag = false,
    fourFlag = false,
    fiveFlag = false;
bool nextButton = true;
bool nameValidator = false;

int _activeMeterIndex;
String title;
String _gradeValue;
String _grade = 'Percentage';
String _state = '2012';
int i = 1;
List titles = [
  'First Attempt',
  'Second',
  'Third',
  'Fourth',
  'Fifth',
];

List<String> _states = <String>['2012', '2013', '2014', '2015'];

class _LoginCreateProfileState extends State<LoginCreateProfile> {
  File _imageFile;
  bool firstAttemptDetails = false;
  bool secondAttemptDetails = false;
  bool thirdAttemptDetails = false;
  bool fourthAttemptDetails = false;
  bool fifthAttemptDetails = false;
  String _name;
  List<String> _colors = <String>[
    'B.S.C IT',
    'B.Tech',
    'Post Graduation',
    'Degree',
  ];
  String _color = 'B.S.C IT';
  FocusNode focus = new FocusNode();

  getFirstAttemptDetailFilled() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    firstAttemptDetails = prefs.getBool("firstDetailsFilled");
    //('Hell0' + firstAttemptDetails.toString());
  }

  getSecondAttemptDetailFilled() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    secondAttemptDetails = prefs.getBool("secondDetailsFilled");
    //('Hell0' + secondAttemptDetails.toString());
  }

  getThirdAttemptDetailFilled() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    thirdAttemptDetails = prefs.getBool("thirdDetailsFilled");
    //('Hell0' + thirdAttemptDetails.toString());
  }

  getFourthAttemptDetailFilled() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    fourthAttemptDetails = prefs.getBool("fourthDetailsFilled");
    //('Hell0' + fourthAttemptDetails.toString());
  }

  getFifthAttemptDetailFilled() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    fifthAttemptDetails = prefs.getBool("fifthDetailsFilled");
    //('Hell0' + fifthAttemptDetails.toString());
  }

  @override
  void initState() {
    super.initState();
    setState(() {
      isSwitched = false;
      nextButton = true;
    });
    setState(() {
      _college_controller.text = '';
      _name_controller.text = '';
    });
    _college_controller.addListener(() {
      setState(() {
        validateCollege(collegeName);
      });
    });
    _name_controller.addListener(() {
      setState(() {
        validateName(name);
      });
    });
  }

  static String validateName(String value) {
    Pattern pattern = r'(^[a-zA-Z ]*$)';
    RegExp regex = new RegExp(pattern);
    if (value.isEmpty || value.length == 0) {
      nameValidator = false;
      //('1st');
      return null;
    }
    if (!regex.hasMatch(value.trim())) {
      nameValidator = false;
      //('2nd');

      return CreateProfileLabels.invalidName;
    }
    // else if (regex.hasMatch(value.trim())) {
    if (value.length > 3) {
      if (!regex.hasMatch(value.trim())) {
        nameValidator = false;
        //('2nd');

        return CreateProfileLabels.invalidName;
      }
      if (regex.hasMatch(value.trim())) {
        nameValidator = true;
        //('3rd');
        return null;
      }
    } else {
      nameValidator = false;
      //('4th');
      return CreateProfileLabels.nameLengh;
    }
    // }
  }

  static String validateCollege(String value) {
    if (value.isEmpty) {
      collegeValidator = false;
      return null;
    } else if (value.length < 5) {
      collegeValidator = false;
      return CreateProfileLabels.invalidCollegelength;
    }
    Pattern pattern = r'^[0-9]*$';
    RegExp regex = new RegExp(pattern);
    if (regex.hasMatch(value.trim())) {
      collegeValidator = false;
      return CreateProfileLabels.invalidCollege;
    }
    if (!regex.hasMatch(value.trim())) {
      collegeValidator = true;

      return null;
    }
  }

  void _showDialog() {
    // flutter defined function
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return SimpleDialog(
          title: new Text(
            'Pick image from',
            style: TextStyles.editTextStyle,
          ),
          children: <Widget>[
            new SimpleDialogOption(
              child: new Text(
                'Camera',
                style: TextStyles.editTextStyle,
              ),
              onPressed: () {
                //("tapped in dialog");
                openCamera();
                Navigator.of(context).pop();
              },
            ),
            new SimpleDialogOption(
              child: new Text(
                'Gallery',
                style: TextStyles.editTextStyle,
              ),
              onPressed: () async {
                //("tapped in dialog");
                await openGalley();
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  openCamera() async {
    //("tapped in openCamera");
    _upload();
    final File imageFile =
        await ImagePicker.pickImage(source: ImageSource.camera);
    //('my image' + imageFile.toString());
    setState(() {
      this._imageFile = imageFile;
    });
  }

  void _upload() {
    if (_imageFile == null) return;
    String base64Image = base64Encode(_imageFile.readAsBytesSync());
    String fileName = _imageFile.path.split("/").last;
    //('base64Image' + base64Image);
    //('fileName' + fileName);
//    http.post(phpEndPoint, body: {
//      "image": base64Image,
//      "name": fileName,
//    }).then((res) {
//      //(res.statusCode);
//    }).catchError((err) {
//      //(err);
//    });
  }

  openGalley() async {
    _upload();
    //("tapped in openGalley");
    final File imageFile =
        await ImagePicker.pickImage(source: ImageSource.gallery);
    //('my image' + imageFile.toString());

    setState(() {
      this._imageFile = imageFile;
    });
  }

  @override
  Future<bool> _onWillPop() {
    AppRoutes.push(context, LoginEnrollmentidWidget());
  }

  @override
  Widget build(BuildContext context) {
    final screenHeight = MediaQuery.of(context).size.height;
    final screenWidth = MediaQuery.of(context).size.width;
    final defaultScreenHeight = 720;
    final defaultScreenWidth = 360;
    return WillPopScope(
      onWillPop: _onWillPop,
      child: Scaffold(
//      resizeToAvoidBottomPadding: false,
//      resizeToAvoidBottomInset: false,
        body: Container(
          //   constraints: BoxConstraints.expand(),
          // decoration: BoxDecoration(
          //  color: Color.fromARGB(255, 255, 255, 255),
          //  ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              // LoginAppbar(),
              Container(
                height: screenHeight * 0.140,
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    GestureDetector(
                      onTap: () {
                        AppRoutes.push(context, LoginEnrollmentidWidget());
                      },
                      child: Container(
                        child: Padding(
                          padding: const EdgeInsets.all(5.0),
                          child: Align(
                            alignment: Alignment.topLeft,
                            child: Container(
                              width: (20 / 360) * screenWidth,
                              height: (20 / 720) * screenHeight,
                              margin: EdgeInsets.only(
                                  top: screenHeight * 0.0738,
                                  left: screenWidth * 0.062,
                                  right: 5),
                              child: getSvgIcon.backSvgIcon,
                            ),
                          ),
                        ),
                      ),
                    ),
                    Spacer(),
                    Align(
                      alignment: Alignment.topRight,
                      child: Container(
                        //   color: Color.fromRGBO(255, 45, 52, 56),

                        width: screenWidth * 0.780,
                        height: screenHeight * 0.9,
                        child: getSvgIcon.combinedshapeSvgIcon,
                      ),
                    ),
                  ],
                ),
              ),
              Expanded(
                child: Container(
                  child: SingleChildScrollView(
                    padding: EdgeInsets.only(bottom: 40),
                    child: Container(
                      margin: EdgeInsets.only(
                        right: (45 / defaultScreenWidth) * screenWidth,
                        left: (45 / defaultScreenWidth) * screenWidth,
                      ),
                      child: Column(
                        children: [
                          Container(
                            //height: (30 / defaultScreenHeight) * screenHeight,
                            // width: (236 / defaultScreenWidth) * screenWidth,
                            margin: EdgeInsets.only(
                                top: (43 / defaultScreenHeight) * screenHeight),
                            child: Text(
                              CreateProfileLabels.title,
                              style: TextStyle(
                                  color: NeutralColors.charcoal_grey,
                                  fontWeight: FontWeight.w700,
                                  fontFamily: "IBMPlexSans",
                                  fontStyle: FontStyle.normal,
                                  fontSize: (21 / defaultScreenHeight) *
                                      screenHeight),
                              textAlign: TextAlign.center,
                            ),
                          ),
                          Stack(
                            children: <Widget>[
                              Align(
                                alignment: Alignment.topCenter,
                                child: Container(
                                  width: 132,
                                  height: 109,
                                  margin: EdgeInsets.only(top: 34),
                                  child: Column(
                                    // crossAxisAlignment: CrossAxisAlignment.stretch,
                                    children: [
                                      Container(
                                        height: 109,
                                        margin: EdgeInsets.symmetric(
                                            horizontal: 16),
                                        child: Stack(
                                          alignment: Alignment.topCenter,
                                          children: [
                                            Positioned(
                                              left: 0,
                                              top: 0,
                                              child: Container(
                                                //height: 101,
                                                child: GestureDetector(
                                                  onTap: _showDialog,
                                                  child: CircleAvatar(
                                                      backgroundColor:
                                                          Colors.transparent,
                                                      radius: 50,
                                                      backgroundImage: this
                                                                  ._imageFile ==
                                                              null
                                                          ? AssetImage(
                                                              'assets/images/avatar.png')
                                                          : FileImage(
                                                              this._imageFile)),
                                                ),
                                              ),
                                            ),
                                            Positioned(
                                              top: 83,
                                              child: GestureDetector(
                                                onTap: _showDialog,
                                                child: Container(
                                                  width: 28,
                                                  height: 24,
                                                  child:
                                                      getSvgIcon.cameraSvgIcon,
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ],
                          ),
                          GestureDetector(
                            onTap: () {
                              _showDialog();
                            },
                            child: Container(
                              margin: EdgeInsets.only(
                                  top:
                                      (5 / defaultScreenHeight) * screenHeight),
                              child: Text(
                                nextButton
                                    ? CreateProfileLabels.uploadProfilePhoto
                                    : CreateProfileLabels.changeProfilePhoto,
                                style: TextStyles.continueImsPsw,
                                textAlign: TextAlign.center,
                              ),
                            ),
                          ),
                          Container(
                            width: double.infinity,
                            margin: EdgeInsets.only(
                                top: (35 / defaultScreenHeight) * screenHeight),
                            child: Column(
                              children: <Widget>[
                                TextInputFormField(
                                  controller: _name_controller,
                                  keyboardType: TextInputType.text,
                                  lableText: CreateProfileLabels.yourName,
                                  obscureText: false,
                                  textInputAction: TextInputAction.done,
                                  border: _name_controller.text.isEmpty
                                      ? UnderlineInputBorder(
                                          borderSide: BorderSide(
                                              color: SemanticColors.blueGrey
                                                  .withOpacity(0.3),
                                              width: 1))
                                      : UnderlineInputBorder(
                                          borderSide: BorderSide(
                                              color:
                                                  SemanticColors.dark_purpely,
                                              width: 1)),
                                  autovalidate: true,
                                  fontStyle: TextStyles.editTextStyle,
                                  lableStyle: TextStyles.labelStyle,
                                  validator: validateName,
                                  onSaved: (value) => _name = value,
                                  onFieldSubmitted: (_) =>
                                      FocusScope.of(context)
                                          .requestFocus(focus),
                                ),
                                Container(
                                  margin: nextButton
                                      ? EdgeInsets.only(
                                          top: (2.5 / defaultScreenHeight) *
                                              screenHeight)
                                      : EdgeInsets.only(top: 0.0),
                                  child: Align(
                                    alignment: Alignment.centerLeft,
                                    child: Text(
                                      nextButton
                                          ? CreateProfileLabels.inputText
                                          : CreateProfileLabels.emptyText,
                                      style: TextStyle(
                                        color: NeutralColors.blue_grey,
                                        fontWeight: FontWeight.w400,
                                        fontFamily: "IBMPlexSans",
                                        fontStyle: FontStyle.normal,
                                        fontSize: (12 / defaultScreenWidth) *
                                            screenWidth,
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Container(
                            child: nextButton
                                ? Container()
                                : Container(
                                    child: Column(
                                      children: <Widget>[
                                        Container(
                                          // width: (140/360)*screenWidth,
                                          height: (50 / 720) * screenHeight,
                                          margin: EdgeInsets.only(
                                              top: (5 / defaultScreenHeight) *
                                                  screenHeight),
                                          child: DropDownFormField(
                                            getImmediateSuggestions: true,
                                            textFieldConfiguration:
                                                TextFieldConfiguration(
                                              label: 'Highest Qualification',
                                              style: TextStyle(
                                                fontFamily: "IBMPlexSans",
                                                fontWeight: FontWeight.w400,
                                                fontSize:
                                                    (12 / 720) * screenHeight,
                                                color: NeutralColors.bluey_grey,
                                                letterSpacing: 0.0,
                                                inherit: false,
                                              ),
                                              controller: _highestQualification,
                                            ),
                                            suggestionsBoxDecoration:
                                                SuggestionsBoxDecoration(
                                                    dropDownHeight: 150,
                                                    borderRadius:
                                                        new BorderRadius
                                                            .circular(5.0),
                                                    color: Colors.white),
                                            suggestionsCallback: (pattern) {
                                              return DegreeService
                                                  .getSuggestions(pattern);
                                            },
                                            itemBuilder: (context, suggestion) {
                                              return ListTile(
                                                dense: true,
                                                contentPadding:
                                                    EdgeInsets.only(left: 10),
                                                title: Text(
                                                  suggestion,
                                                  style: TextStyle(
                                                    fontFamily: "IBMPlexSans",
                                                    fontWeight: FontWeight.w400,
                                                    fontSize: (13 / 720) *
                                                        screenHeight,
                                                    color: NeutralColors
                                                        .bluey_grey,
                                                    textBaseline:
                                                        TextBaseline.alphabetic,
                                                    letterSpacing: 0.0,
                                                    inherit: false,
                                                  ),
                                                ),
                                              );
                                            },
                                            transitionBuilder: (context,
                                                suggestionsBox, controller) {
                                              return suggestionsBox;
                                            },
                                            onSuggestionSelected: (suggestion) {
                                              _highestQualification.text =
                                                  suggestion;
                                              setState(() {});

                                              // this._typeAheadController.dispose();
                                            },
                                            onSaved: (value) => {
                                              //("something")}
                                            },
                                          ),
                                        ),
                                        Container(
                                          width: double.infinity,
                                          margin: EdgeInsets.only(
                                              top:
                                                  (19.5 / defaultScreenHeight) *
                                                      screenHeight),
                                          child: TextInputFormField(
                                            controller: _college_controller,
                                            //  focusNode: focus,
                                            keyboardType: TextInputType.text,
                                            fontStyle: TextStyles.editTextStyle,
                                            lableStyle: TextStyles.labelStyle,
                                            textInputAction:
                                                TextInputAction.done,
                                            border: collegeName.isEmpty
                                                ? UnderlineInputBorder(
                                                    borderSide: BorderSide(
                                                        color: SemanticColors
                                                            .blueGrey
                                                            .withOpacity(0.3),
                                                        width: 1))
                                                : UnderlineInputBorder(
                                                    borderSide: BorderSide(
                                                        color: SemanticColors
                                                            .dark_purpely,
                                                        width: 1)),
                                            autovalidate: true,
                                            validator:
                                                FieldValidator.validateCollege,
                                            onSaved: (value) => _name = value,
                                            lableText:
                                                CreateProfileLabels.collegeName,
                                          ),
                                        ),
                                        Container(
                                          height: (27 / defaultScreenHeight) *
                                              screenHeight,
                                          margin: EdgeInsets.only(
                                              top:
                                                  (29.5 / defaultScreenHeight) *
                                                      screenHeight),
                                          //  width: double.infinity,
                                          child: Container(
                                            width: (270 / defaultScreenWidth) *
                                                screenWidth,
                                            child: Row(
                                              children: <Widget>[
                                                Container(
                                                  //   color: Colors.pinkAccent,
//                                          height: (27 / defaultScreenHeight) *
//                                              screenHeight,
//                                          width: (130 / defaultScreenWidth) *
//                                              screenWidth,
                                                  child: Padding(
                                                    padding: EdgeInsets.only(
                                                        top: (4 /
                                                                defaultScreenHeight) *
                                                            screenHeight,
                                                        bottom: (1 /
                                                                defaultScreenHeight) *
                                                            screenHeight,
                                                        right: (2 /
                                                                defaultScreenWidth) *
                                                            screenWidth),
                                                    child: Text(
                                                      CreateProfileLabels
                                                          .catExam,
                                                      style: TextStyle(
                                                          color: NeutralColors
                                                              .black,
                                                          fontWeight:
                                                              FontWeight.w700,
                                                          fontFamily:
                                                              "IBMPlexSans",
                                                          fontStyle:
                                                              FontStyle.normal,
                                                          fontSize: screenWidth >
                                                                  350
                                                              ? (16 / defaultScreenWidth) *
                                                                  screenWidth
                                                              : (14 / defaultScreenWidth) *
                                                                  screenWidth),
                                                      textAlign:
                                                          TextAlign.start,
                                                    ),
                                                  ),
                                                ),
                                                Container(
                                                  child: Row(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment.start,
                                                    crossAxisAlignment:
                                                        CrossAxisAlignment
                                                            .center,
                                                    children: <Widget>[
                                                      Container(
                                                        //   width: (80/defaultScreenWidth)*screenWidth,
                                                        // color: Colors.yellowAccent,
                                                        child: Align(
                                                          alignment:
                                                              Alignment.center,
                                                          child: Center(
                                                            child: Padding(
                                                              padding: EdgeInsets.only(
                                                                  top: (8 /
                                                                          defaultScreenHeight) *
                                                                      screenHeight,
                                                                  bottom: (2 /
                                                                          defaultScreenHeight) *
                                                                      screenHeight,
                                                                  right: (1 /
                                                                          defaultScreenWidth) *
                                                                      screenWidth),
                                                              child: Text(
                                                                CreateProfileLabels
                                                                    .notApplicable,
                                                                style: TextStyle(
                                                                    color: NeutralColors
                                                                        .gunmetal,
                                                                    fontWeight:
                                                                        FontWeight
                                                                            .w400,
                                                                    fontFamily:
                                                                        "IBMPlexSans",
                                                                    fontStyle:
                                                                        FontStyle
                                                                            .normal,
                                                                    fontSize: (12 /
                                                                            defaultScreenWidth) *
                                                                        screenWidth),
                                                                textAlign:
                                                                    TextAlign
                                                                        .end,
                                                              ),
                                                            ),
                                                          ),
                                                        ),
                                                      ),
                                                      //Spacer(),
                                                      Container(
                                                        //  color: Colors.red,
                                                        child: Padding(
                                                          padding:
                                                              EdgeInsets.only(
                                                            top: (8 /
                                                                    defaultScreenHeight) *
                                                                screenHeight,
                                                            bottom: (0 /
                                                                    defaultScreenHeight) *
                                                                screenHeight,
                                                          ),
                                                          child:
                                                              Transform.scale(
                                                            scale: 0.78,
                                                            alignment: Alignment
                                                                .centerRight,
                                                            child: Container(
                                                              child: Switch(
                                                                value:
                                                                    isSwitched,
                                                                onChanged:
                                                                    (value) {
                                                                  setState(() {
                                                                    isSwitched =
                                                                        value;
                                                                    oneFlag =
                                                                        false;
                                                                    twoFlag =
                                                                        false;
                                                                    threeFlag =
                                                                        false;
                                                                    fourFlag =
                                                                        false;
                                                                    fiveFlag =
                                                                        false;
                                                                  });
                                                                },
                                                                activeTrackColor:
                                                                    SemanticColors
                                                                        .iceBlue,
                                                                activeColor:
                                                                    NeutralColors
                                                                        .mango,
                                                                inactiveThumbColor:
                                                                    NeutralColors
                                                                        .blue_grey,
                                                                inactiveTrackColor:
                                                                    SemanticColors
                                                                        .iceBlue,
                                                              ),
                                                            ),
                                                          ),
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                        ),
                                        //),
                                        Container(
                                          // height: (45/defaultScreenHeight)*screenHeight,
                                          // color: Colors.pink,
                                          margin: EdgeInsets.only(top: (19)),
                                          //  padding: EdgeInsets.only(top:(3/defaultScreenHeight)*screenHeight,bottom: (5/defaultScreenHeight)*screenHeight),
                                          child: isSwitched
                                              ? Container()
                                              : Row(
                                                  children: <Widget>[
                                                    Text(
                                                      CreateProfileLabels
                                                          .noOFAttempts,
                                                      style: TextStyle(
                                                          color: NeutralColors
                                                              .dark_navy_blue,
                                                          fontWeight:
                                                              FontWeight.w400,
                                                          fontFamily:
                                                              "IBMPlexSans",
                                                          fontStyle:
                                                              FontStyle.normal,
                                                          fontSize: (14 /
                                                                  defaultScreenWidth) *
                                                              screenWidth),
                                                    ),
                                                    Spacer(),
                                                    GestureDetector(
                                                      onTap: () {
                                                        setState(() {
                                                          //("tapped one");
                                                          oneFlag = true;
                                                          twoFlag = false;
                                                          threeFlag = false;
                                                          fourFlag = false;
                                                          fiveFlag = false;
                                                        });
                                                      },
                                                      child: Container(
                                                        //    padding: EdgeInsets.only(top:(3/defaultScreenHeight)*screenHeight,bottom: (4/defaultScreenHeight)*screenHeight),
                                                        height: (26 /
                                                                defaultScreenHeight) *
                                                            screenHeight,
                                                        width: (26 /
                                                                defaultScreenWidth) *
                                                            screenWidth,
                                                        decoration: oneFlag
                                                            ? BoxDecoration(
                                                                border: Border
                                                                    .fromBorderSide(
                                                                  BorderSide(
                                                                      color: NeutralColors
                                                                          .mango),
                                                                ),
                                                                color: NeutralColors
                                                                    .off_white,
                                                                borderRadius: BorderRadius
                                                                    .all(Radius
                                                                        .circular(
                                                                            2.0)),
                                                              )
                                                            : BoxDecoration(
                                                                border: Border
                                                                    .fromBorderSide(
                                                                  BorderSide(
                                                                      color: Colors
                                                                          .transparent),
                                                                ),
                                                              ),
                                                        //  color: isActive ? NeutralColors.mango : Colors.white,

//                          highlightedBorderColor: NeutralColors.mango,
//                          highlightColor: NeutralColors.off_white,
                                                        child: Center(
                                                          //   padding: EdgeInsets.only(top:(3/defaultScreenHeight)*screenHeight,bottom: (4/defaultScreenHeight)*screenHeight),
                                                          child: Text(
                                                            "1",
                                                            style: TextStyle(
                                                                color: NeutralColors
                                                                    .dark_navy_blue,
                                                                fontWeight:
                                                                    FontWeight
                                                                        .w400,
                                                                fontFamily:
                                                                    "IBMPlexSans",
                                                                fontStyle:
                                                                    FontStyle
                                                                        .normal,
                                                                fontSize: (14 /
                                                                        defaultScreenWidth) *
                                                                    screenWidth),
                                                            textAlign: TextAlign
                                                                .center,
                                                          ),
                                                        ),
                                                      ),
                                                    ),
                                                    GestureDetector(
                                                      onTap: () {
                                                        setState(() {
                                                          //("tapped two");
                                                          oneFlag = false;
                                                          twoFlag = true;
                                                          threeFlag = false;
                                                          fourFlag = false;
                                                          fiveFlag = false;
                                                        });
                                                      },
                                                      child: Container(
                                                        height: (26 /
                                                                defaultScreenHeight) *
                                                            screenHeight,
                                                        width: (26 /
                                                                defaultScreenWidth) *
                                                            screenWidth,
                                                        decoration: twoFlag
                                                            ? BoxDecoration(
                                                                border: Border
                                                                    .fromBorderSide(
                                                                  BorderSide(
                                                                      color: NeutralColors
                                                                          .mango),
                                                                ),
                                                                color: NeutralColors
                                                                    .off_white,
                                                                borderRadius: BorderRadius
                                                                    .all(Radius
                                                                        .circular(
                                                                            2.0)),
                                                              )
                                                            : BoxDecoration(
                                                                border: Border
                                                                    .fromBorderSide(
                                                                  BorderSide(
                                                                      color: Colors
                                                                          .transparent),
                                                                ),
                                                              ),
                                                        //  color: isActive ? NeutralColors.mango : Colors.white,
//                          highlightedBorderColor: NeutralColors.mango,
//                          highlightColor: NeutralColors.off_white,
                                                        child: Center(
                                                          child: Text(
                                                            "2",
                                                            style: TextStyle(
                                                                color: NeutralColors
                                                                    .dark_navy_blue,
                                                                fontWeight:
                                                                    FontWeight
                                                                        .w400,
                                                                fontFamily:
                                                                    "IBMPlexSans",
                                                                fontStyle:
                                                                    FontStyle
                                                                        .normal,
                                                                fontSize: (14 /
                                                                        defaultScreenWidth) *
                                                                    screenWidth),
                                                            textAlign: TextAlign
                                                                .center,
                                                          ),
                                                        ),
                                                      ),
                                                    ),
                                                    GestureDetector(
                                                      onTap: () {
                                                        setState(() {
                                                          //("tapped three");
                                                          oneFlag = false;
                                                          twoFlag = false;
                                                          threeFlag = true;
                                                          fourFlag = false;
                                                          fiveFlag = false;
                                                        });
                                                      },
                                                      child: Container(
                                                        height: (26 /
                                                                defaultScreenHeight) *
                                                            screenHeight,
                                                        width: (26 /
                                                                defaultScreenWidth) *
                                                            screenWidth,
                                                        decoration: threeFlag
                                                            ? BoxDecoration(
                                                                border: Border
                                                                    .fromBorderSide(
                                                                  BorderSide(
                                                                      color: NeutralColors
                                                                          .mango),
                                                                ),
                                                                color: NeutralColors
                                                                    .off_white,
                                                                borderRadius: BorderRadius
                                                                    .all(Radius
                                                                        .circular(
                                                                            2.0)),
                                                              )
                                                            : BoxDecoration(
                                                                border: Border
                                                                    .fromBorderSide(
                                                                  BorderSide(
                                                                      color: Colors
                                                                          .transparent),
                                                                ),
                                                              ),
//                          highlightedBorderColor: NeutralColors.mango,
//                          highlightColor: NeutralColors.off_white,
                                                        child: Center(
                                                          child: Text(
                                                            "3",
                                                            style: TextStyle(
                                                                color: NeutralColors
                                                                    .dark_navy_blue,
                                                                fontWeight:
                                                                    FontWeight
                                                                        .w400,
                                                                fontFamily:
                                                                    "IBMPlexSans",
                                                                fontStyle:
                                                                    FontStyle
                                                                        .normal,
                                                                fontSize: (14 /
                                                                        defaultScreenWidth) *
                                                                    screenWidth),
                                                            textAlign: TextAlign
                                                                .center,
                                                          ),
                                                        ),
                                                      ),
                                                    ),
                                                    GestureDetector(
                                                      onTap: () {
                                                        setState(() {
                                                          //("tapped four");
                                                          oneFlag = false;
                                                          twoFlag = false;
                                                          threeFlag = false;
                                                          fourFlag = true;
                                                          fiveFlag = false;
                                                        });
                                                      },
                                                      child: Container(
                                                        height: (26 /
                                                                defaultScreenHeight) *
                                                            screenHeight,
                                                        width: (26 /
                                                                defaultScreenWidth) *
                                                            screenWidth,
                                                        decoration: fourFlag
                                                            ? BoxDecoration(
                                                                border: Border
                                                                    .fromBorderSide(
                                                                  BorderSide(
                                                                      color: NeutralColors
                                                                          .mango),
                                                                ),
                                                                color: NeutralColors
                                                                    .off_white,
                                                                borderRadius: BorderRadius
                                                                    .all(Radius
                                                                        .circular(
                                                                            2.0)),
                                                              )
                                                            : BoxDecoration(
                                                                border: Border
                                                                    .fromBorderSide(
                                                                  BorderSide(
                                                                      color: Colors
                                                                          .transparent),
                                                                ),
                                                              ),
//                          highlightedBorderColor: NeutralColors.mango,
//                          highlightColor: NeutralColors.off_white,
                                                        child: Center(
                                                          child: Text(
                                                            "4",
                                                            style: TextStyle(
                                                                color: NeutralColors
                                                                    .dark_navy_blue,
                                                                fontWeight:
                                                                    FontWeight
                                                                        .w400,
                                                                fontFamily:
                                                                    "IBMPlexSans",
                                                                fontStyle:
                                                                    FontStyle
                                                                        .normal,
                                                                fontSize: (14 /
                                                                        defaultScreenWidth) *
                                                                    screenWidth),
                                                            textAlign: TextAlign
                                                                .center,
                                                          ),
                                                        ),
                                                      ),
                                                    ),
                                                    GestureDetector(
                                                      onTap: () {
                                                        setState(() {
                                                          //("tapped five");
                                                          oneFlag = false;
                                                          twoFlag = false;
                                                          threeFlag = false;
                                                          fourFlag = false;
                                                          fiveFlag = true;
                                                        });
                                                      },
                                                      child: Container(
                                                        height: (26 /
                                                                defaultScreenHeight) *
                                                            screenHeight,
                                                        width: (26 /
                                                                defaultScreenWidth) *
                                                            screenWidth,
                                                        decoration: fiveFlag
                                                            ? BoxDecoration(
                                                                border: Border
                                                                    .fromBorderSide(
                                                                  BorderSide(
                                                                      color: NeutralColors
                                                                          .mango),
                                                                ),
                                                                color: NeutralColors
                                                                    .off_white,
                                                                borderRadius: BorderRadius
                                                                    .all(Radius
                                                                        .circular(
                                                                            2.0)),
                                                              )
                                                            : BoxDecoration(
                                                                border: Border
                                                                    .fromBorderSide(
                                                                  BorderSide(
                                                                      color: Colors
                                                                          .transparent),
                                                                ),
                                                              ),
//                          highlightedBorderColor: NeutralColors.mango,
//                          highlightColor: NeutralColors.off_white,
                                                        child: Center(
                                                          child: Text(
                                                            "5",
                                                            style: TextStyle(
                                                                color: NeutralColors
                                                                    .dark_navy_blue,
                                                                fontWeight:
                                                                    FontWeight
                                                                        .w400,
                                                                fontFamily:
                                                                    "IBMPlexSans",
                                                                fontStyle:
                                                                    FontStyle
                                                                        .normal,
                                                                fontSize: (14 /
                                                                        defaultScreenWidth) *
                                                                    screenWidth),
                                                            textAlign: TextAlign
                                                                .center,
                                                          ),
                                                        ),
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                        ),

                                        isSwitched
                                            ? Container()
                                            : Container(
                                                margin: EdgeInsets.only(
                                                    top: (22 /
                                                            defaultScreenHeight) *
                                                        screenHeight),
                                                child: Column(
                                                  children: <Widget>[
                                                    oneFlag
                                                        ? Container(
                                                            margin:
                                                                EdgeInsets.only(
                                                                    top: 10.0,
                                                                    left: 0.0,
                                                                    right: 0.0),
                                                            child:
                                                                CustomExpansionPanelList(
                                                              expansionHeaderHeight:
                                                                  45.0,
                                                              iconColor:
                                                                  (_activeMeterIndex ==
                                                                          0)
                                                                      ? Colors
                                                                          .white
                                                                      : Colors
                                                                          .black,
                                                              backgroundColor1: (_activeMeterIndex ==
                                                                      0)
                                                                  ? const Color(
                                                                      0xFF6979f8)
                                                                  : (_year.text !=
                                                                              '' &&
                                                                          _percentage.text !=
                                                                              '')
                                                                      ? NeutralColors
                                                                          .mango
                                                                          .withOpacity(
                                                                              0.05)
                                                                      : const Color(
                                                                              0xFF3380cc)
                                                                          .withOpacity(
                                                                              0.10),
                                                              backgroundColor2: (_activeMeterIndex ==
                                                                      0)
                                                                  ? const Color(
                                                                      0xFF6979f8)
                                                                  : (_year.text !=
                                                                              '' &&
                                                                          _percentage.text !=
                                                                              '')
                                                                      ? NeutralColors
                                                                          .mango
                                                                          .withOpacity(
                                                                              0.05)
                                                                      : const Color(
                                                                              0xFF886dd7)
                                                                          .withOpacity(
                                                                              0.10),
                                                              expansionCallback:
                                                                  (int index,
                                                                      bool
                                                                          status) {
                                                                setState(() {
                                                                  getFirstAttemptDetailFilled();
                                                                  _activeMeterIndex =
                                                                      _activeMeterIndex ==
                                                                              0
                                                                          ? null
                                                                          : 0;
                                                                });
                                                              },
                                                              children: [
                                                                new ExpansionPanel(
                                                                  canTapOnHeader:
                                                                      true,
                                                                  isExpanded:
                                                                      _activeMeterIndex ==
                                                                          0,
                                                                  headerBuilder: (BuildContext
                                                                              context,
                                                                          bool
                                                                              isExpanded) =>
                                                                      new Container(
                                                                    alignment:
                                                                        Alignment
                                                                            .centerLeft,
                                                                    child:
                                                                        new Padding(
                                                                      padding: EdgeInsets.only(
                                                                          left: (15 / 320) *
                                                                              screenWidth),
                                                                      child: new Text(
                                                                          'First Attempt',
                                                                          style: new TextStyle(
                                                                              fontSize: (12.5 / 360) * screenWidth,
                                                                              fontFamily: "IBMPlexSans",
                                                                              fontWeight: FontWeight.w500,
                                                                              color: (_activeMeterIndex == 0) ? Colors.white : Colors.black)),
                                                                    ),
                                                                  ),
                                                                  body:
                                                                      FirstAttemptDetails(),
                                                                ),
                                                              ],
                                                            ),
                                                          )
                                                        : Container(),
                                                    twoFlag
                                                        ? Container(
                                                            child: Column(
                                                              children: <
                                                                  Widget>[
                                                                Container(
                                                                  margin: EdgeInsets.only(
                                                                      top: 10.0,
                                                                      left: 0.0,
                                                                      right:
                                                                          0.0),
                                                                  child:
                                                                      CustomExpansionPanelList(
                                                                    expansionHeaderHeight:
                                                                        45.0,
                                                                    iconColor: (_activeMeterIndex ==
                                                                            0)
                                                                        ? Colors
                                                                            .white
                                                                        : Colors
                                                                            .black,
                                                                    backgroundColor1: (_activeMeterIndex ==
                                                                            0)
                                                                        ? const Color(
                                                                            0xFF6979f8)
                                                                        : (_year.text != '' &&
                                                                                _percentage.text != '')
                                                                            ? NeutralColors.mango.withOpacity(0.05)
                                                                            : const Color(0xFF3380cc).withOpacity(0.10),
                                                                    backgroundColor2: (_activeMeterIndex ==
                                                                            0)
                                                                        ? const Color(
                                                                            0xFF6979f8)
                                                                        : (_year.text != '' &&
                                                                                _percentage.text != '')
                                                                            ? NeutralColors.mango.withOpacity(0.05)
                                                                            : const Color(0xFF886dd7).withOpacity(0.10),
                                                                    expansionCallback:
                                                                        (int index,
                                                                            bool
                                                                                status) {
                                                                      setState(
                                                                          () {
                                                                        getFirstAttemptDetailFilled();
                                                                        _activeMeterIndex = _activeMeterIndex ==
                                                                                0
                                                                            ? null
                                                                            : 0;
                                                                      });
                                                                    },
                                                                    children: [
                                                                      new ExpansionPanel(
                                                                        canTapOnHeader:
                                                                            true,
                                                                        isExpanded:
                                                                            _activeMeterIndex ==
                                                                                0,
                                                                        headerBuilder:
                                                                            (BuildContext context, bool isExpanded) =>
                                                                                new Container(
                                                                          alignment:
                                                                              Alignment.centerLeft,
                                                                          child:
                                                                              new Padding(
                                                                            padding:
                                                                                EdgeInsets.only(left: (15 / 320) * screenWidth),
                                                                            child:
                                                                                new Text('First Attempt', style: new TextStyle(fontSize: (12.5 / 360) * screenWidth, fontFamily: "IBMPlexSans", fontWeight: FontWeight.w500, color: (_activeMeterIndex == 0) ? Colors.white : Colors.black)),
                                                                          ),
                                                                        ),
                                                                        body:
                                                                            FirstAttemptDetails(),
                                                                      ),
                                                                    ],
                                                                  ),
                                                                ),
                                                                Container(
                                                                  margin: EdgeInsets.only(
                                                                      top: 10.0,
                                                                      left: 0.0,
                                                                      right:
                                                                          0.0),
                                                                  child:
                                                                      CustomExpansionPanelList(
                                                                    expansionHeaderHeight:
                                                                        45.0,
                                                                    iconColor: (_activeMeterIndex ==
                                                                            1)
                                                                        ? Colors
                                                                            .white
                                                                        : Colors
                                                                            .black,
                                                                    backgroundColor1: (_activeMeterIndex ==
                                                                            1)
                                                                        ? const Color(
                                                                            0xFF6979f8)
                                                                        : (_year.text != '' &&
                                                                                _percentage.text != '')
                                                                            ? NeutralColors.mango.withOpacity(0.05)
                                                                            : const Color(0xFF3380cc).withOpacity(0.10),
                                                                    backgroundColor2: (_activeMeterIndex ==
                                                                            1)
                                                                        ? const Color(
                                                                            0xFF6979f8)
                                                                        : (_year.text != '' &&
                                                                                _percentage.text != '')
                                                                            ? NeutralColors.mango.withOpacity(0.05)
                                                                            : const Color(0xFF886dd7).withOpacity(0.10),
                                                                    expansionCallback:
                                                                        (int index,
                                                                            bool
                                                                                status) {
                                                                      setState(
                                                                          () {
                                                                        getSecondAttemptDetailFilled();
                                                                        _activeMeterIndex = _activeMeterIndex ==
                                                                                1
                                                                            ? null
                                                                            : 1;
                                                                      });
                                                                    },
                                                                    children: [
                                                                      new ExpansionPanel(
                                                                        canTapOnHeader:
                                                                            true,
                                                                        isExpanded:
                                                                            _activeMeterIndex ==
                                                                                1,
                                                                        headerBuilder:
                                                                            (BuildContext context, bool isExpanded) =>
                                                                                new Container(
                                                                          alignment:
                                                                              Alignment.centerLeft,
                                                                          child:
                                                                              new Padding(
                                                                            padding:
                                                                                EdgeInsets.only(left: (15 / 320) * screenWidth),
                                                                            child:
                                                                                new Text('Second Attempt', style: new TextStyle(fontSize: (12.5 / 360) * screenWidth, fontFamily: "IBMPlexSans", fontWeight: FontWeight.w500, color: (_activeMeterIndex == 1) ? Colors.white : Colors.black)),
                                                                          ),
                                                                        ),
                                                                        body:
                                                                            SecondAttemptDetails(),
                                                                      ),
                                                                    ],
                                                                  ),
                                                                )
                                                              ],
                                                            ),
                                                          )
                                                        : Container(),
                                                    threeFlag
                                                        ? Container(
                                                            child: Column(
                                                              children: <
                                                                  Widget>[
                                                                Container(
                                                                  margin: EdgeInsets.only(
                                                                      top: 10.0,
                                                                      left: 0.0,
                                                                      right:
                                                                          0.0),
                                                                  child:
                                                                      CustomExpansionPanelList(
                                                                    expansionHeaderHeight:
                                                                        45.0,
                                                                    iconColor: (_activeMeterIndex ==
                                                                            0)
                                                                        ? Colors
                                                                            .white
                                                                        : Colors
                                                                            .black,
                                                                    backgroundColor1: (_activeMeterIndex ==
                                                                            0)
                                                                        ? const Color(
                                                                            0xFF6979f8)
                                                                        : (_year.text != '' &&
                                                                                _percentage.text != '')
                                                                            ? NeutralColors.mango.withOpacity(0.05)
                                                                            : const Color(0xFF3380cc).withOpacity(0.10),
                                                                    backgroundColor2: (_activeMeterIndex ==
                                                                            0)
                                                                        ? const Color(
                                                                            0xFF6979f8)
                                                                        : (_year.text != '' &&
                                                                                _percentage.text != '')
                                                                            ? NeutralColors.mango.withOpacity(0.05)
                                                                            : const Color(0xFF886dd7).withOpacity(0.10),
                                                                    expansionCallback:
                                                                        (int index,
                                                                            bool
                                                                                status) {
                                                                      setState(
                                                                          () {
                                                                        getFirstAttemptDetailFilled();
                                                                        _activeMeterIndex = _activeMeterIndex ==
                                                                                0
                                                                            ? null
                                                                            : 0;
                                                                      });
                                                                    },
                                                                    children: [
                                                                      new ExpansionPanel(
                                                                        canTapOnHeader:
                                                                            true,
                                                                        isExpanded:
                                                                            _activeMeterIndex ==
                                                                                0,
                                                                        headerBuilder:
                                                                            (BuildContext context, bool isExpanded) =>
                                                                                new Container(
                                                                          alignment:
                                                                              Alignment.centerLeft,
                                                                          child:
                                                                              new Padding(
                                                                            padding:
                                                                                EdgeInsets.only(left: (15 / 320) * screenWidth),
                                                                            child:
                                                                                new Text('First Attempt', style: new TextStyle(fontSize: (12.5 / 360) * screenWidth, fontFamily: "IBMPlexSans", fontWeight: FontWeight.w500, color: (_activeMeterIndex == 0) ? Colors.white : Colors.black)),
                                                                          ),
                                                                        ),
                                                                        body:
                                                                            FirstAttemptDetails(),
                                                                      ),
                                                                    ],
                                                                  ),
                                                                ),
                                                                Container(
                                                                  margin: EdgeInsets.only(
                                                                      top: 10.0,
                                                                      left: 0.0,
                                                                      right:
                                                                          0.0),
                                                                  child:
                                                                      CustomExpansionPanelList(
                                                                    expansionHeaderHeight:
                                                                        45.0,
                                                                    iconColor: (_activeMeterIndex ==
                                                                            1)
                                                                        ? Colors
                                                                            .white
                                                                        : Colors
                                                                            .black,
                                                                    backgroundColor1: (_activeMeterIndex ==
                                                                            1)
                                                                        ? const Color(
                                                                            0xFF6979f8)
                                                                        : (_year.text != '' &&
                                                                                _percentage.text != '')
                                                                            ? NeutralColors.mango.withOpacity(0.05)
                                                                            : const Color(0xFF3380cc).withOpacity(0.10),
                                                                    backgroundColor2: (_activeMeterIndex ==
                                                                            1)
                                                                        ? const Color(
                                                                            0xFF6979f8)
                                                                        : (_year.text != '' &&
                                                                                _percentage.text != '')
                                                                            ? NeutralColors.mango.withOpacity(0.05)
                                                                            : const Color(0xFF886dd7).withOpacity(0.10),
                                                                    expansionCallback:
                                                                        (int index,
                                                                            bool
                                                                                status) {
                                                                      setState(
                                                                          () {
                                                                        getSecondAttemptDetailFilled();
                                                                        _activeMeterIndex = _activeMeterIndex ==
                                                                                1
                                                                            ? null
                                                                            : 1;
                                                                      });
                                                                    },
                                                                    children: [
                                                                      new ExpansionPanel(
                                                                        canTapOnHeader:
                                                                            true,
                                                                        isExpanded:
                                                                            _activeMeterIndex ==
                                                                                1,
                                                                        headerBuilder:
                                                                            (BuildContext context, bool isExpanded) =>
                                                                                new Container(
                                                                          alignment:
                                                                              Alignment.centerLeft,
                                                                          child:
                                                                              new Padding(
                                                                            padding:
                                                                                EdgeInsets.only(left: (15 / 320) * screenWidth),
                                                                            child:
                                                                                new Text('Second Attempt', style: new TextStyle(fontSize: (12.5 / 360) * screenWidth, fontFamily: "IBMPlexSans", fontWeight: FontWeight.w500, color: (_activeMeterIndex == 1) ? Colors.white : Colors.black)),
                                                                          ),
                                                                        ),
                                                                        body:
                                                                            SecondAttemptDetails(),
                                                                      ),
                                                                    ],
                                                                  ),
                                                                ),
                                                                Container(
                                                                  margin: EdgeInsets.only(
                                                                      top: 10.0,
                                                                      left: 0.0,
                                                                      right:
                                                                          0.0),
                                                                  child:
                                                                      CustomExpansionPanelList(
                                                                    expansionHeaderHeight:
                                                                        45.0,
                                                                    iconColor: (_activeMeterIndex ==
                                                                            2)
                                                                        ? Colors
                                                                            .white
                                                                        : Colors
                                                                            .black,
                                                                    backgroundColor1: (_activeMeterIndex ==
                                                                            2)
                                                                        ? const Color(
                                                                            0xFF6979f8)
                                                                        : (_year.text != '' &&
                                                                                _percentage.text != '')
                                                                            ? NeutralColors.mango.withOpacity(0.05)
                                                                            : const Color(0xFF3380cc).withOpacity(0.10),
                                                                    backgroundColor2: (_activeMeterIndex ==
                                                                            2)
                                                                        ? const Color(
                                                                            0xFF6979f8)
                                                                        : (_year.text != '' &&
                                                                                _percentage.text != '')
                                                                            ? NeutralColors.mango.withOpacity(0.05)
                                                                            : const Color(0xFF886dd7).withOpacity(0.10),
                                                                    expansionCallback:
                                                                        (int index,
                                                                            bool
                                                                                status) {
                                                                      setState(
                                                                          () {
                                                                        getThirdAttemptDetailFilled();
                                                                        _activeMeterIndex = _activeMeterIndex ==
                                                                                2
                                                                            ? null
                                                                            : 2;
                                                                      });
                                                                    },
                                                                    children: [
                                                                      new ExpansionPanel(
                                                                        canTapOnHeader:
                                                                            true,
                                                                        isExpanded:
                                                                            _activeMeterIndex ==
                                                                                2,
                                                                        headerBuilder:
                                                                            (BuildContext context, bool isExpanded) =>
                                                                                new Container(
                                                                          alignment:
                                                                              Alignment.centerLeft,
                                                                          child:
                                                                              new Padding(
                                                                            padding:
                                                                                EdgeInsets.only(left: (15 / 320) * screenWidth),
                                                                            child:
                                                                                new Text('Third Attempt', style: new TextStyle(fontSize: (12.5 / 360) * screenWidth, fontFamily: "IBMPlexSans", fontWeight: FontWeight.w500, color: (_activeMeterIndex == 2) ? Colors.white : Colors.black)),
                                                                          ),
                                                                        ),
                                                                        body:
                                                                            ThirdAttemptDetails(),
                                                                      ),
                                                                    ],
                                                                  ),
                                                                ),
                                                              ],
                                                            ),
                                                          )
                                                        : Container(),
                                                    fourFlag
                                                        ? Container(
                                                            child: Column(
                                                              children: <
                                                                  Widget>[
                                                                Container(
                                                                  margin: EdgeInsets.only(
                                                                      top: 10.0,
                                                                      left: 0.0,
                                                                      right:
                                                                          0.0),
                                                                  child:
                                                                      CustomExpansionPanelList(
                                                                    expansionHeaderHeight:
                                                                        45.0,
                                                                    iconColor: (_activeMeterIndex ==
                                                                            0)
                                                                        ? Colors
                                                                            .white
                                                                        : Colors
                                                                            .black,
                                                                    backgroundColor1: (_activeMeterIndex ==
                                                                            0)
                                                                        ? const Color(
                                                                            0xFF6979f8)
                                                                        : (_year.text != '' &&
                                                                                _percentage.text != '')
                                                                            ? NeutralColors.mango.withOpacity(0.05)
                                                                            : const Color(0xFF3380cc).withOpacity(0.10),
                                                                    backgroundColor2: (_activeMeterIndex ==
                                                                            0)
                                                                        ? const Color(
                                                                            0xFF6979f8)
                                                                        : (_year.text != '' &&
                                                                                _percentage.text != '')
                                                                            ? NeutralColors.mango.withOpacity(0.05)
                                                                            : const Color(0xFF886dd7).withOpacity(0.10),
                                                                    expansionCallback:
                                                                        (int index,
                                                                            bool
                                                                                status) {
                                                                      setState(
                                                                          () {
                                                                        getFirstAttemptDetailFilled();
                                                                        _activeMeterIndex = _activeMeterIndex ==
                                                                                0
                                                                            ? null
                                                                            : 0;
                                                                      });
                                                                    },
                                                                    children: [
                                                                      new ExpansionPanel(
                                                                        canTapOnHeader:
                                                                            true,
                                                                        isExpanded:
                                                                            _activeMeterIndex ==
                                                                                0,
                                                                        headerBuilder:
                                                                            (BuildContext context, bool isExpanded) =>
                                                                                new Container(
                                                                          alignment:
                                                                              Alignment.centerLeft,
                                                                          child:
                                                                              new Padding(
                                                                            padding:
                                                                                EdgeInsets.only(left: (15 / 320) * screenWidth),
                                                                            child:
                                                                                new Text('First Attempt', style: new TextStyle(fontSize: (12.5 / 360) * screenWidth, fontFamily: "IBMPlexSans", fontWeight: FontWeight.w500, color: (_activeMeterIndex == 0) ? Colors.white : Colors.black)),
                                                                          ),
                                                                        ),
                                                                        body:
                                                                            FirstAttemptDetails(),
                                                                      ),
                                                                    ],
                                                                  ),
                                                                ),
                                                                Container(
                                                                  margin: EdgeInsets.only(
                                                                      top: 10.0,
                                                                      left: 0.0,
                                                                      right:
                                                                          0.0),
                                                                  child:
                                                                      CustomExpansionPanelList(
                                                                    expansionHeaderHeight:
                                                                        45.0,
                                                                    iconColor: (_activeMeterIndex ==
                                                                            1)
                                                                        ? Colors
                                                                            .white
                                                                        : Colors
                                                                            .black,
                                                                    backgroundColor1: (_activeMeterIndex ==
                                                                            1)
                                                                        ? const Color(
                                                                            0xFF6979f8)
                                                                        : (_year.text != '' &&
                                                                                _percentage.text != '')
                                                                            ? NeutralColors.mango.withOpacity(0.05)
                                                                            : const Color(0xFF3380cc).withOpacity(0.10),
                                                                    backgroundColor2: (_activeMeterIndex ==
                                                                            1)
                                                                        ? const Color(
                                                                            0xFF6979f8)
                                                                        : (_year.text != '' &&
                                                                                _percentage.text != '')
                                                                            ? NeutralColors.mango.withOpacity(0.05)
                                                                            : const Color(0xFF886dd7).withOpacity(0.10),
                                                                    expansionCallback:
                                                                        (int index,
                                                                            bool
                                                                                status) {
                                                                      setState(
                                                                          () {
                                                                        getSecondAttemptDetailFilled();
                                                                        _activeMeterIndex = _activeMeterIndex ==
                                                                                1
                                                                            ? null
                                                                            : 1;
                                                                      });
                                                                    },
                                                                    children: [
                                                                      new ExpansionPanel(
                                                                        canTapOnHeader:
                                                                            true,
                                                                        isExpanded:
                                                                            _activeMeterIndex ==
                                                                                1,
                                                                        headerBuilder:
                                                                            (BuildContext context, bool isExpanded) =>
                                                                                new Container(
                                                                          alignment:
                                                                              Alignment.centerLeft,
                                                                          child:
                                                                              new Padding(
                                                                            padding:
                                                                                EdgeInsets.only(left: (15 / 320) * screenWidth),
                                                                            child:
                                                                                new Text('Second Attempt', style: new TextStyle(fontSize: (12.5 / 360) * screenWidth, fontFamily: "IBMPlexSans", fontWeight: FontWeight.w500, color: (_activeMeterIndex == 1) ? Colors.white : Colors.black)),
                                                                          ),
                                                                        ),
                                                                        body:
                                                                            SecondAttemptDetails(),
                                                                      ),
                                                                    ],
                                                                  ),
                                                                ),
                                                                Container(
                                                                  margin: EdgeInsets.only(
                                                                      top: 10.0,
                                                                      left: 0.0,
                                                                      right:
                                                                          0.0),
                                                                  child:
                                                                      CustomExpansionPanelList(
                                                                    expansionHeaderHeight:
                                                                        45.0,
                                                                    iconColor: (_activeMeterIndex ==
                                                                            2)
                                                                        ? Colors
                                                                            .white
                                                                        : Colors
                                                                            .black,
                                                                    backgroundColor1: (_activeMeterIndex ==
                                                                            2)
                                                                        ? const Color(
                                                                            0xFF6979f8)
                                                                        : (_year.text != '' &&
                                                                                _percentage.text != '')
                                                                            ? NeutralColors.mango.withOpacity(0.05)
                                                                            : const Color(0xFF3380cc).withOpacity(0.10),
                                                                    backgroundColor2: (_activeMeterIndex ==
                                                                            2)
                                                                        ? const Color(
                                                                            0xFF6979f8)
                                                                        : (_year.text != '' &&
                                                                                _percentage.text != '')
                                                                            ? NeutralColors.mango.withOpacity(0.05)
                                                                            : const Color(0xFF886dd7).withOpacity(0.10),
                                                                    expansionCallback:
                                                                        (int index,
                                                                            bool
                                                                                status) {
                                                                      setState(
                                                                          () {
                                                                        getThirdAttemptDetailFilled();
                                                                        _activeMeterIndex = _activeMeterIndex ==
                                                                                2
                                                                            ? null
                                                                            : 2;
                                                                      });
                                                                    },
                                                                    children: [
                                                                      new ExpansionPanel(
                                                                        canTapOnHeader:
                                                                            true,
                                                                        isExpanded:
                                                                            _activeMeterIndex ==
                                                                                2,
                                                                        headerBuilder:
                                                                            (BuildContext context, bool isExpanded) =>
                                                                                new Container(
                                                                          alignment:
                                                                              Alignment.centerLeft,
                                                                          child:
                                                                              new Padding(
                                                                            padding:
                                                                                EdgeInsets.only(left: (15 / 320) * screenWidth),
                                                                            child:
                                                                                new Text('Third Attempt', style: new TextStyle(fontSize: (12.5 / 360) * screenWidth, fontFamily: "IBMPlexSans", fontWeight: FontWeight.w500, color: (_activeMeterIndex == 2) ? Colors.white : Colors.black)),
                                                                          ),
                                                                        ),
                                                                        body:
                                                                            ThirdAttemptDetails(),
                                                                      ),
                                                                    ],
                                                                  ),
                                                                ),
                                                                Container(
                                                                  margin: EdgeInsets.only(
                                                                      top: 10.0,
                                                                      left: 0.0,
                                                                      right:
                                                                          0.0),
                                                                  child:
                                                                      CustomExpansionPanelList(
                                                                    expansionHeaderHeight:
                                                                        45.0,
                                                                    iconColor: (_activeMeterIndex ==
                                                                            3)
                                                                        ? Colors
                                                                            .white
                                                                        : Colors
                                                                            .black,
                                                                    backgroundColor1: (_activeMeterIndex ==
                                                                            3)
                                                                        ? const Color(
                                                                            0xFF6979f8)
                                                                        : (_year.text != '' &&
                                                                                _percentage.text != '')
                                                                            ? NeutralColors.mango.withOpacity(0.05)
                                                                            : const Color(0xFF3380cc).withOpacity(0.10),
                                                                    backgroundColor2: (_activeMeterIndex ==
                                                                            3)
                                                                        ? const Color(
                                                                            0xFF6979f8)
                                                                        : (_year.text != '' &&
                                                                                _percentage.text != '')
                                                                            ? NeutralColors.mango.withOpacity(0.05)
                                                                            : const Color(0xFF886dd7).withOpacity(0.10),
                                                                    expansionCallback:
                                                                        (int index,
                                                                            bool
                                                                                status) {
                                                                      setState(
                                                                          () {
                                                                        getFourthAttemptDetailFilled();
                                                                        _activeMeterIndex = _activeMeterIndex ==
                                                                                3
                                                                            ? null
                                                                            : 3;
                                                                      });
                                                                    },
                                                                    children: [
                                                                      new ExpansionPanel(
                                                                        canTapOnHeader:
                                                                            true,
                                                                        isExpanded:
                                                                            _activeMeterIndex ==
                                                                                3,
                                                                        headerBuilder:
                                                                            (BuildContext context, bool isExpanded) =>
                                                                                new Container(
                                                                          alignment:
                                                                              Alignment.centerLeft,
                                                                          child:
                                                                              new Padding(
                                                                            padding:
                                                                                EdgeInsets.only(left: (15 / 320) * screenWidth),
                                                                            child:
                                                                                new Text('Fourth Attempt', style: new TextStyle(fontSize: (12.5 / 360) * screenWidth, fontFamily: "IBMPlexSans", fontWeight: FontWeight.w500, color: (_activeMeterIndex == 3) ? Colors.white : Colors.black)),
                                                                          ),
                                                                        ),
                                                                        body:
                                                                            FourthAttemptDetails(),
                                                                      ),
                                                                    ],
                                                                  ),
                                                                ),
                                                              ],
                                                            ),
                                                          )
                                                        : Container(),
                                                    fiveFlag
                                                        ? Container(
                                                            child: Column(
                                                              children: <
                                                                  Widget>[
                                                                Container(
                                                                  margin: EdgeInsets.only(
                                                                      top: 10.0,
                                                                      left: 0.0,
                                                                      right:
                                                                          0.0),
                                                                  child:
                                                                      CustomExpansionPanelList(
                                                                    expansionHeaderHeight:
                                                                        (45.0 / 720) *
                                                                            screenHeight,
                                                                    iconColor: (_activeMeterIndex ==
                                                                            0)
                                                                        ? Colors
                                                                            .white
                                                                        : Colors
                                                                            .black,
                                                                    backgroundColor1: (_activeMeterIndex ==
                                                                            0)
                                                                        ? const Color(
                                                                            0xFF6979f8)
                                                                        : (_year.text != '' &&
                                                                                _percentage.text != '')
                                                                            ? NeutralColors.mango.withOpacity(0.05)
                                                                            : const Color(0xFF3380cc).withOpacity(0.10),
                                                                    backgroundColor2: (_activeMeterIndex ==
                                                                            0)
                                                                        ? const Color(
                                                                            0xFF6979f8)
                                                                        : (_year.text != '' &&
                                                                                _percentage.text != '')
                                                                            ? NeutralColors.mango.withOpacity(0.05)
                                                                            : const Color(0xFF886dd7).withOpacity(0.10),
                                                                    expansionCallback:
                                                                        (int index,
                                                                            bool
                                                                                status) {
                                                                      setState(
                                                                          () {
                                                                        getFirstAttemptDetailFilled();
                                                                        _activeMeterIndex = _activeMeterIndex ==
                                                                                0
                                                                            ? null
                                                                            : 0;
                                                                      });
                                                                    },
                                                                    children: [
                                                                      new ExpansionPanel(
                                                                        canTapOnHeader:
                                                                            true,
                                                                        isExpanded:
                                                                            _activeMeterIndex ==
                                                                                0,
                                                                        headerBuilder:
                                                                            (BuildContext context, bool isExpanded) =>
                                                                                new Container(
                                                                          alignment:
                                                                              Alignment.centerLeft,
                                                                          child:
                                                                              new Padding(
                                                                            padding:
                                                                                EdgeInsets.only(left: (15 / 320) * screenWidth),
                                                                            child:
                                                                                new Text('First Attempt', style: new TextStyle(fontSize: (12.5 / 360) * screenWidth, fontFamily: "IBMPlexSans", fontWeight: FontWeight.w500, color: (_activeMeterIndex == 0) ? Colors.white : Colors.black)),
                                                                          ),
                                                                        ),
                                                                        body:
                                                                            FirstAttemptDetails(),
                                                                      ),
                                                                    ],
                                                                  ),
                                                                ),
                                                                Container(
                                                                  margin: EdgeInsets.only(
                                                                      top: 10.0,
                                                                      left: 0.0,
                                                                      right:
                                                                          0.0),
                                                                  child:
                                                                      CustomExpansionPanelList(
                                                                    expansionHeaderHeight:
                                                                        (45.0 / 720) *
                                                                            screenHeight,
                                                                    iconColor: (_activeMeterIndex ==
                                                                            1)
                                                                        ? Colors
                                                                            .white
                                                                        : Colors
                                                                            .black,
                                                                    backgroundColor1: (_activeMeterIndex ==
                                                                            1)
                                                                        ? const Color(
                                                                            0xFF6979f8)
                                                                        : (_year.text != '' &&
                                                                                _percentage.text != '')
                                                                            ? NeutralColors.mango.withOpacity(0.05)
                                                                            : const Color(0xFF3380cc).withOpacity(0.10),
                                                                    backgroundColor2: (_activeMeterIndex ==
                                                                            1)
                                                                        ? const Color(
                                                                            0xFF6979f8)
                                                                        : (_year.text != '' &&
                                                                                _percentage.text != '')
                                                                            ? NeutralColors.mango.withOpacity(0.05)
                                                                            : const Color(0xFF886dd7).withOpacity(0.10),
                                                                    expansionCallback:
                                                                        (int index,
                                                                            bool
                                                                                status) {
                                                                      setState(
                                                                          () {
                                                                        getSecondAttemptDetailFilled();
                                                                        _activeMeterIndex = _activeMeterIndex ==
                                                                                1
                                                                            ? null
                                                                            : 1;
                                                                      });
                                                                    },
                                                                    children: [
                                                                      new ExpansionPanel(
                                                                        canTapOnHeader:
                                                                            true,
                                                                        isExpanded:
                                                                            _activeMeterIndex ==
                                                                                1,
                                                                        headerBuilder:
                                                                            (BuildContext context, bool isExpanded) =>
                                                                                new Container(
                                                                          alignment:
                                                                              Alignment.centerLeft,
                                                                          child:
                                                                              new Padding(
                                                                            padding:
                                                                                EdgeInsets.only(left: (15 / 320) * screenWidth),
                                                                            child:
                                                                                new Text('Second Attempt', style: new TextStyle(fontSize: (12.5 / 360) * screenWidth, fontFamily: "IBMPlexSans", fontWeight: FontWeight.w500, color: (_activeMeterIndex == 1) ? Colors.white : Colors.black)),
                                                                          ),
                                                                        ),
                                                                        body:
                                                                            SecondAttemptDetails(),
                                                                      ),
                                                                    ],
                                                                  ),
                                                                ),
                                                                Container(
                                                                  margin: EdgeInsets.only(
                                                                      top: 10.0,
                                                                      left: 0.0,
                                                                      right:
                                                                          0.0),
                                                                  child:
                                                                      CustomExpansionPanelList(
                                                                    expansionHeaderHeight:
                                                                        (45.0 / 720) *
                                                                            screenHeight,
                                                                    iconColor: (_activeMeterIndex ==
                                                                            2)
                                                                        ? Colors
                                                                            .white
                                                                        : Colors
                                                                            .black,
                                                                    backgroundColor1: (_activeMeterIndex ==
                                                                            2)
                                                                        ? const Color(
                                                                            0xFF6979f8)
                                                                        : (_year.text != '' &&
                                                                                _percentage.text != '')
                                                                            ? NeutralColors.mango.withOpacity(0.05)
                                                                            : const Color(0xFF3380cc).withOpacity(0.10),
                                                                    backgroundColor2: (_activeMeterIndex ==
                                                                            2)
                                                                        ? const Color(
                                                                            0xFF6979f8)
                                                                        : (_year.text != '' &&
                                                                                _percentage.text != '')
                                                                            ? NeutralColors.mango.withOpacity(0.05)
                                                                            : const Color(0xFF886dd7).withOpacity(0.10),
                                                                    expansionCallback:
                                                                        (int index,
                                                                            bool
                                                                                status) {
                                                                      setState(
                                                                          () {
                                                                        getThirdAttemptDetailFilled();
                                                                        _activeMeterIndex = _activeMeterIndex ==
                                                                                2
                                                                            ? null
                                                                            : 2;
                                                                      });
                                                                    },
                                                                    children: [
                                                                      new ExpansionPanel(
                                                                        canTapOnHeader:
                                                                            true,
                                                                        isExpanded:
                                                                            _activeMeterIndex ==
                                                                                2,
                                                                        headerBuilder:
                                                                            (BuildContext context, bool isExpanded) =>
                                                                                new Container(
                                                                          alignment:
                                                                              Alignment.centerLeft,
                                                                          child:
                                                                              new Padding(
                                                                            padding:
                                                                                EdgeInsets.only(left: (15 / 320) * screenWidth),
                                                                            child:
                                                                                new Text('Third Attempt', style: new TextStyle(fontSize: (12.5 / 360) * screenWidth, fontFamily: "IBMPlexSans", fontWeight: FontWeight.w500, color: (_activeMeterIndex == 2) ? Colors.white : Colors.black)),
                                                                          ),
                                                                        ),
                                                                        body:
                                                                            ThirdAttemptDetails(),
                                                                      ),
                                                                    ],
                                                                  ),
                                                                ),
                                                                Container(
                                                                  margin: EdgeInsets.only(
                                                                      top: 10.0,
                                                                      left: 0.0,
                                                                      right:
                                                                          0.0),
                                                                  child:
                                                                      CustomExpansionPanelList(
                                                                    expansionHeaderHeight:
                                                                        (45.0 / 720) *
                                                                            screenHeight,
                                                                    iconColor: (_activeMeterIndex ==
                                                                            3)
                                                                        ? Colors
                                                                            .white
                                                                        : Colors
                                                                            .black,
                                                                    backgroundColor1: (_activeMeterIndex ==
                                                                            3)
                                                                        ? const Color(
                                                                            0xFF6979f8)
                                                                        : (_year.text != '' &&
                                                                                _percentage.text != '')
                                                                            ? NeutralColors.mango.withOpacity(0.05)
                                                                            : const Color(0xFF3380cc).withOpacity(0.10),
                                                                    backgroundColor2: (_activeMeterIndex ==
                                                                            3)
                                                                        ? const Color(
                                                                            0xFF6979f8)
                                                                        : (_year.text != '' &&
                                                                                _percentage.text != '')
                                                                            ? NeutralColors.mango.withOpacity(0.05)
                                                                            : const Color(0xFF886dd7).withOpacity(0.10),
                                                                    expansionCallback:
                                                                        (int index,
                                                                            bool
                                                                                status) {
                                                                      setState(
                                                                          () {
                                                                        getFourthAttemptDetailFilled();
                                                                        _activeMeterIndex = _activeMeterIndex ==
                                                                                3
                                                                            ? null
                                                                            : 3;
                                                                      });
                                                                    },
                                                                    children: [
                                                                      new ExpansionPanel(
                                                                        canTapOnHeader:
                                                                            true,
                                                                        isExpanded:
                                                                            _activeMeterIndex ==
                                                                                3,
                                                                        headerBuilder:
                                                                            (BuildContext context, bool isExpanded) =>
                                                                                new Container(
                                                                          alignment:
                                                                              Alignment.centerLeft,
                                                                          child:
                                                                              new Padding(
                                                                            padding:
                                                                                EdgeInsets.only(left: (15 / 320) * screenWidth),
                                                                            child:
                                                                                new Text('Fourth Attempt', style: new TextStyle(fontSize: (12.5 / 360) * screenWidth, fontFamily: "IBMPlexSans", fontWeight: FontWeight.w500, color: (_activeMeterIndex == 3) ? Colors.white : Colors.black)),
                                                                          ),
                                                                        ),
                                                                        body:
                                                                            FourthAttemptDetails(),
                                                                      ),
                                                                    ],
                                                                  ),
                                                                ),
                                                                Container(
                                                                  margin: EdgeInsets.only(
                                                                      top: 10.0,
                                                                      left: 0.0,
                                                                      right:
                                                                          0.0),
                                                                  child:
                                                                      CustomExpansionPanelList(
                                                                    expansionHeaderHeight:
                                                                        (45.0 / 720) *
                                                                            screenHeight,
                                                                    iconColor: (_activeMeterIndex ==
                                                                            4)
                                                                        ? Colors
                                                                            .white
                                                                        : Colors
                                                                            .black,
                                                                    backgroundColor1: (_activeMeterIndex ==
                                                                            4)
                                                                        ? const Color(
                                                                            0xFF6979f8)
                                                                        : (_year.text != '' &&
                                                                                _percentage.text != '')
                                                                            ? NeutralColors.mango.withOpacity(0.05)
                                                                            : const Color(0xFF3380cc).withOpacity(0.10),
                                                                    backgroundColor2: (_activeMeterIndex ==
                                                                            4)
                                                                        ? const Color(
                                                                            0xFF6979f8)
                                                                        : (_year.text != '' &&
                                                                                _percentage.text != '')
                                                                            ? NeutralColors.mango.withOpacity(0.05)
                                                                            : const Color(0xFF886dd7).withOpacity(0.10),
                                                                    expansionCallback:
                                                                        (int index,
                                                                            bool
                                                                                status) {
                                                                      setState(
                                                                          () {
                                                                        getFifthAttemptDetailFilled();
                                                                        _activeMeterIndex = _activeMeterIndex ==
                                                                                4
                                                                            ? null
                                                                            : 4;
                                                                      });
                                                                    },
                                                                    children: [
                                                                      new ExpansionPanel(
                                                                        canTapOnHeader:
                                                                            true,
                                                                        isExpanded:
                                                                            _activeMeterIndex ==
                                                                                4,
                                                                        headerBuilder:
                                                                            (BuildContext context, bool isExpanded) =>
                                                                                new Container(
                                                                          alignment:
                                                                              Alignment.centerLeft,
                                                                          child:
                                                                              new Padding(
                                                                            padding:
                                                                                EdgeInsets.only(left: (15 / 320) * screenWidth),
                                                                            child:
                                                                                new Text('Fifth Attempt', style: new TextStyle(fontSize: (12.5 / 360) * screenWidth, fontFamily: "IBMPlexSans", fontWeight: FontWeight.w500, color: (_activeMeterIndex == 4) ? Colors.white : Colors.black)),
                                                                          ),
                                                                        ),
                                                                        body:
                                                                            FifthAttemptDetails(),
                                                                      ),
                                                                    ],
                                                                  ),
                                                                ),
                                                              ],
                                                            ),
                                                          )
                                                        : Container(),
                                                  ],
                                                ),
                                              ),
                                      ],
                                    ),
                                  ),
                          ),
                          Container(
                            child: nextButton
                                ? Container(
                                    //  height: (40/defaultScreenHeight)*screenHeight,
                                    margin: EdgeInsets.only(
                                        top: (40 / defaultScreenHeight) *
                                            screenHeight),
                                    child: PrimaryButton(
                                      gradient: (nameValidator == true)
                                          ? true
                                          : false,
                                      onTap: () {
                                        //(_imageFile);
                                        if (nameValidator == true) {
                                          setState(() {
                                            nextButton = false;
                                          });
                                          //('NextButton Clicked');
                                        } else {}
                                      },
                                      text: CreateProfileLabels.nextBtn
                                          .toUpperCase(),
                                    ) // Group 4
                                    )
                                : Container(
                                    //  height: (40/defaultScreenHeight)*screenHeight,
                                    margin: EdgeInsets.only(
                                        top: (40 / defaultScreenHeight) *
                                            screenHeight),
                                    child: PrimaryButton(
                                      gradient: (nameValidator == true &&
                                              collegeValidator == true &&
                                              (((oneFlag == true &&
                                                          firstAttemptDetails ==
                                                              true) ||
                                                      (twoFlag == true &&
                                                          firstAttemptDetails ==
                                                              true &&
                                                          secondAttemptDetails ==
                                                              true) ||
                                                      (threeFlag == true &&
                                                          firstAttemptDetails ==
                                                              true &&
                                                          secondAttemptDetails ==
                                                              true &&
                                                          thirdAttemptDetails ==
                                                              true) ||
                                                      (fourFlag == true &&
                                                          firstAttemptDetails ==
                                                              true &&
                                                          secondAttemptDetails ==
                                                              true &&
                                                          thirdAttemptDetails ==
                                                              true &&
                                                          fourthAttemptDetails ==
                                                              true) ||
                                                      (fiveFlag == true &&
                                                          firstAttemptDetails ==
                                                              true &&
                                                          secondAttemptDetails ==
                                                              true &&
                                                          thirdAttemptDetails ==
                                                              true &&
                                                          fourthAttemptDetails ==
                                                              true &&
                                                          fifthAttemptDetails ==
                                                              true)) ||
                                                  isSwitched == true))
                                          ? true
                                          : false,
                                      onTap: () {
                                        //(firstAttemptDetails);
                                        if (nameValidator == true &&
                                            collegeValidator == true &&
                                            (((oneFlag == true &&
                                                        firstAttemptDetails ==
                                                            true) ||
                                                    (twoFlag == true &&
                                                        firstAttemptDetails ==
                                                            true &&
                                                        secondAttemptDetails ==
                                                            true) ||
                                                    (threeFlag == true &&
                                                        firstAttemptDetails ==
                                                            true &&
                                                        secondAttemptDetails ==
                                                            true &&
                                                        thirdAttemptDetails ==
                                                            true) ||
                                                    (fourFlag == true &&
                                                        firstAttemptDetails ==
                                                            true &&
                                                        secondAttemptDetails ==
                                                            true &&
                                                        thirdAttemptDetails ==
                                                            true &&
                                                        fourthAttemptDetails ==
                                                            true) ||
                                                    (fiveFlag == true &&
                                                        firstAttemptDetails ==
                                                            true &&
                                                        secondAttemptDetails ==
                                                            true &&
                                                        thirdAttemptDetails ==
                                                            true &&
                                                        fourthAttemptDetails ==
                                                            true &&
                                                        fifthAttemptDetails ==
                                                            true)) ||
                                                isSwitched == true)) {
                                          AppRoutes.push(
                                              context, LoginCongratulations());
                                        } else {}
                                      },
                                      text: CreateProfileLabels.confirmDetails
                                          .toUpperCase(),
                                    ) // Group 4
                                    ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

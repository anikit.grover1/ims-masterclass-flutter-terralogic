import 'package:flutter/material.dart';
import 'package:imsindia/components/custom_DropDown.dart';
import 'package:imsindia/utils/colors.dart';

class SampleService {
  static final List<String> degrees = [
    'Arun',
    'Anusha',
    'Vetri',
    'Kavya',
  ];
  static List<String> getSuggestions(String query) {
    List<String> matches = List();
    matches.addAll(degrees);

    //  matches.retainWhere((s) => s.toLowerCase().contains(query.toLowerCase()));
    return matches;
  }
}

class sampleDropdown extends StatefulWidget {
  @override
  _sampleDropdownState createState() => _sampleDropdownState();
}

class _sampleDropdownState extends State<sampleDropdown> {
  final TextEditingController _sample = new TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(children: <Widget>[
        Text('sample DropDown Example'),
        Container(margin: EdgeInsets.only(top:150),
          child:
          Padding(
                    padding: EdgeInsets.only(
                        left:45,
                        top: 45,
                        right: 45),
                    child: DropDownFormField(
                      getImmediateSuggestions: true,
                      // autoFlipDirection: true,
                      textFieldConfiguration: TextFieldConfiguration(
                          controller: _sample,
                          label: "Select some"),
                      suggestionsBoxDecoration: SuggestionsBoxDecoration(
                          borderRadius: new BorderRadius.circular(5.0),
                          color: Colors.white),
                      suggestionsCallback: (pattern) {
                        return SampleService.getSuggestions(pattern);
                      },
                      itemBuilder: (context, suggestion) {
                        return ListTile(
                          title: Text(
                            suggestion,
                            style: TextStyle(
                              fontFamily: "IBMPlexSans",
                              fontWeight: FontWeight.w400,
                              fontSize: 13,
                              color: NeutralColors.bluey_grey,
                              textBaseline: TextBaseline.alphabetic,
                              letterSpacing: 0.0,
                              inherit: false,
                            ),
                          ),
                        );
                      },
                      hideOnLoading: true,
                      debounceDuration: Duration(milliseconds: 100),
                      transitionBuilder: (context, suggestionsBox, controller) {
                        return suggestionsBox;
                      },
                      onSuggestionSelected: (suggestion) {
                        for (int i = 0; i < SampleService.degrees.length; i++) {
                          if (SampleService.degrees[i] == suggestion) {
                          }
                        }
                        print(suggestion);
                        _sample.text = suggestion;
                        // setState(() {});
                      },
                      onSaved: (value) => {print("something")},
                    )),
        )
      ],),
      
    );
  }
}
import 'dart:math';

import 'package:imsindia/api/bloc/services.dart';
import 'package:imsindia/utils/Url.dart';
import 'package:imsindia/utils/global.dart' as globals;

class BackendService {
  static Future<List> getSuggestions(String query) async {
    await Future.delayed(Duration(seconds: 1));

    return List.generate(3, (index) {
      return {'name': query + index.toString(), 'price': Random().nextInt(100)};
    });
  }
}

class StatesService {
  static final List<String> states = [
    'Maharastra',
    'Telangana',
    'Andhra Pradesh',
    'Delhi',
  ];

  static List<String> getSuggestions(String query) {
    List<String> matches = List();
    matches.addAll(states);

    // matches.retainWhere((s) => s.toLowerCase().contains(query.toLowerCase()));
    return matches;
  }
}

class DegreeService {
  static final List<String> degrees = [
    'B.com',
    'B.A',
    'M.Tech',
    'B.Tech',
  ];

  static List<String> getSuggestions(String query) {
    List<String> matches = List();
    matches.addAll(degrees);

    //  matches.retainWhere((s) => s.toLowerCase().contains(query.toLowerCase()));
    return matches;
  }
}

class GradeService {
  static final List<String> grades = [
    'CGPA',
    'Percentage',
  ];

  static List<String> getSuggestions(String query) {
    List<String> matches = List();
    matches.addAll(grades);

    //  matches.retainWhere((s) => s.toLowerCase().contains(query.toLowerCase()));
    return matches;
  }
}
class TechnologyService {
  static final List<String> technology = [
    'Information Technology',
    'Admin',
    'Professor'
  ];

  static List<String> getSuggestions(String query) {
    List<String> matches = List();
    matches.addAll(technology);

    //  matches.retainWhere((s) => s.toLowerCase().contains(query.toLowerCase()));
    return matches;
  }
}

class CourseService {
  static final List<String> courses = [];
  static final List<dynamic> data = [];
  CourseData() {
    ApiService().getAPI(URL.GET_COURSE_NAMES, globals.headers).then((result) {
      for (int i = 0; i < result[1]['data'].length; i++) {
        courses.add(result[1]['data'][i]['courseName']);
        data.add(result[1]['data'][i]['id']);
      }
      //  print(result[1]['data'][i]['courseName']);}
    });
  }

  static List<String> getSuggestions() {
    CourseService().CourseData();
    List<String> matches = List();
    matches.addAll(courses);
    //  matches.retainWhere((s) => s.toLowerCase().contains(query.toLowerCase()));
    return courses;
  }
}

class YearService {
  static final List<String> years = [
    '2012',
    '2013',
    '2014',
    '2015',
  ];

  static List<String> getSuggestions(String query) {
    List<String> matches = List();
    matches.addAll(years);

    //  matches.retainWhere((s) => s.toLowerCase().contains(query.toLowerCase()));
    return matches;
  }
}

import 'package:flutter/material.dart';
import 'package:imsindia/components/custom_DropDown.dart' as prefix1;
import 'package:imsindia/views/login_page/login_education.dart' as prefix0;
import 'package:imsindia/views/login_page/login_educational_details.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:imsindia/components/custom_DropDown.dart';

import '../../components/Input_forms.dart';
import '../../components/custom_expansion_panel.dart';
import '../../resources/strings/login.dart';
import '../../utils/colors.dart';
import '../../utils/text_styles.dart';
import '../../utils/validator.dart';
import 'data.dart';

const actualHeight = 925;
const actualWidth = 360;

var screenWidthTotal;
var screenHeightTotal;
var _safeAreaHorizontal;
var _safeAreaVertical;
var screenHeight;
var screenWidth;
bool isSwitchedPostGraduation = false;

class PostGraduationDetails extends StatefulWidget {
  @override
  _PostGraduationDetailsState createState() => _PostGraduationDetailsState();
}

class _PostGraduationDetailsState extends State<PostGraduationDetails> {
  final TextEditingController _degreeInPostGraduationDetails =
      new TextEditingController();

  final TextEditingController _yearInPostGraduationDetails =
      new TextEditingController();

  final TextEditingController _nameOfCollegeInPostGraduationDetails =
      new TextEditingController();
  final TextEditingController _gradeSystemInPostGraduationDetails =
      new TextEditingController();
  final TextEditingController _percentageInPostGraduationDetails =
      new TextEditingController();
  FocusNode grade = new FocusNode();
  FocusNode percentile = new FocusNode();

  @override
  void initState() {
    _degreeInPostGraduationDetails.clear();
    _yearInPostGraduationDetails.clear();
    _nameOfCollegeInPostGraduationDetails.clear();
    _gradeSystemInPostGraduationDetails.clear();
    _percentageInPostGraduationDetails.clear();
    setPostGraduationFilledFalse();
    _degreeInPostGraduationDetails.addListener(() {
      setState(() {
        checkPostGraduationDetails();
      });
    });
    _yearInPostGraduationDetails.addListener(() {
      setState(() {
        checkPostGraduationDetails();
      });
    });
    _nameOfCollegeInPostGraduationDetails.addListener(() {
      setState(() {
        checkPostGraduationDetails();
      });
    });
    _gradeSystemInPostGraduationDetails.addListener(() {
      setState(() {
        checkPostGraduationDetails();
      });
    });
    _percentageInPostGraduationDetails.addListener(() {
      setState(() {
        checkPostGraduationDetails();
      });
    });
    setState(() {
      isSwitchedPostGraduation = false;
    });
  }

  void setPostGraduationFilledTrue() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setBool("postgraduationDetailsFilled", true);
  }

  void setPostGraduationFilledFalse() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setBool("postgraduationDetailsFilled", false);
  }

  checkPostGraduationDetails() async {
    setState(() {
      if ((_degreeInPostGraduationDetails.text != '' &&
              _gradeSystemInPostGraduationDetails.text != '' &&
              _nameOfCollegeInPostGraduationDetails.text != '' &&
              _yearInPostGraduationDetails.text != '' &&
              _percentageInPostGraduationDetails.text != '') ||
          isSwitchedPostGraduation == true) {
        setPostGraduationFilledTrue();
      } else {
        setPostGraduationFilledFalse();
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    CalculateScreen(context);
    return Container(
      height: 270/720 * screenHeightTotal,
      margin: EdgeInsets.only(top: 0, right: 0),
      child: Stack(
        alignment: Alignment.center,
        children: [
          Positioned(
            left: 0,
            right: 0,
            child: Container(
              height: 270/720 * screenHeightTotal,
              decoration: BoxDecoration(
                color: Color.fromARGB(255, 255, 255, 255),
                boxShadow: [
                  BoxShadow(
                    color: Color.fromARGB(255, 234, 234, 234),
                    offset: Offset(0, 0),
                    blurRadius: 10,
                  ),
                ],
                borderRadius: BorderRadius.all(Radius.circular(5)),
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  Align(
                    alignment: Alignment.topLeft,
                    child: Container(
                      width: 150/360 * screenWidthTotal,
                      height: 20/720 * screenHeightTotal,
                      margin: EdgeInsets.only(left: 15/360 * screenWidthTotal, top: 14/720 * screenHeightTotal),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(
                            margin: EdgeInsets.only(top: 0),
                            child: Text(
                              "Not Applicable",
                              style: TextStyle(
                                  color: NeutralColors.gunmetal,
                                  fontWeight: FontWeight.w400,
                                  fontFamily: "IBMPlexSans",
                                  fontStyle: FontStyle.normal,
                                  fontSize: (12 / 360) * screenWidth),
                              textAlign: TextAlign.center,
                            ),
                          ),
                          Transform.scale(
                            scale: 0.78,
                            child: Container(
                              child: Switch(
                                value: isSwitchedPostGraduation,
                                onChanged: (value) {
                                  setState(() {
                                    isSwitchedPostGraduation = value;
                                        _degreeInPostGraduationDetails.text='';
                                       _yearInPostGraduationDetails.text='';
                                       _nameOfCollegeInPostGraduationDetails.text='';
                                       _gradeSystemInPostGraduationDetails.text='';
                                       _percentageInPostGraduationDetails.text='';
                                    checkPostGraduationDetails();
                                  });
                                },
                                activeTrackColor: SemanticColors.iceBlue,
                                activeColor: NeutralColors.mango,
                                inactiveThumbColor: NeutralColors.blue_grey,
                                inactiveTrackColor: SemanticColors.iceBlue,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  Container(
                    height: 49/720 * screenHeightTotal,
                    margin: EdgeInsets.only(left: 15/360 * screenWidthTotal, top: 20/720 * screenHeightTotal, right: 15/360 * screenWidthTotal),
                    child: TextInputFormField(
                        enabled: isSwitchedPostGraduation ? false : true,
                        controller: _nameOfCollegeInPostGraduationDetails,
                        keyboardType: TextInputType.text,
                        border: _nameOfCollegeInPostGraduationDetails.text.isEmpty ? UnderlineInputBorder(borderSide:BorderSide(color:SemanticColors.blueGrey.withOpacity(0.3),width: 1 )): UnderlineInputBorder(borderSide:BorderSide(color:SemanticColors.dark_purpely,width: 1 ) ),
                          fontStyle: TextStyles.loginAlternativeOption,
                                 lableStyle: TextStyles.labelStyle,
                        lableText: LoginAppLabels.nameOfCollege,
                        textInputAction: TextInputAction.done,
                        onSaved: (value) => {}
                        //onFieldSubmitted: (_) => FocusScope.of(context).requestFocus(),
                        ),
                  ),
                ],
              ),
            ),
          ),
          Positioned(
            left: 15/360 * screenWidthTotal,
            top: 110/720 * screenHeightTotal,
            right: 15/360 * screenWidthTotal,
            child: Container(
              height: 140/720 * screenHeightTotal,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  Container(
                    height: 60/720 * screenHeightTotal,
                    margin: EdgeInsets.only(left: 3/360 * screenWidthTotal, top: 3/720 * screenHeightTotal, right: 3/360 * screenWidthTotal),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Expanded(
                            child: Container(
                          width: (133 / 360) * screenWidthTotal,
                          //height: (52 / 720) * screenHeightTotal,
                          margin: EdgeInsets.only(top: 0, bottom: 0),
                          child: isSwitchedPostGraduation
                              ?  new Container(
                                margin:  EdgeInsets.only(top: (13/ 720) * screenHeightTotal, bottom: 0),
                                child:new TextInputFormField(
                                  enabled: false,
                                    fontStyle: TextStyles.loginAlternativeOption,
                                 lableStyle: TextStyles.labelStyle,
                                  lableText: "Degree",
                                ))
                              : new DropDownFormField(
                                  getImmediateSuggestions: true,
                                  textFieldConfiguration:
                                      TextFieldConfiguration(
                                                                    label: "Degree",

                                    controller:
                                        this._degreeInPostGraduationDetails,
                                  ),
                                   suggestionsBoxDecoration: SuggestionsBoxDecoration(
                          dropDownHeight: 150,
                          borderRadius: new BorderRadius.circular(5.0),
                          color: Colors.white),
                                  suggestionsCallback: (pattern) {
                                    return DegreeService.getSuggestions(
                                        pattern);
                                  },
                                  itemBuilder: (context, suggestion) {
                                    return ListTile(
                          dense: true,
                          contentPadding: EdgeInsets.only(left: 10/360 * screenWidthTotal),
                          title: Text(
                            suggestion,
                            style: TextStyle(
                              fontFamily: "IBMPlexSans",
                              fontWeight: FontWeight.w400,
                              fontSize: (13 / 720) * screenHeight,
                              color: NeutralColors.bluey_grey,
                              textBaseline: TextBaseline.alphabetic,
                              letterSpacing: 0.0,
                              inherit: false,
                              
                            ),
                          ),
                        );
                                  },
                                  transitionBuilder:
                                      (context, suggestionsBox, controller) {
                                    return suggestionsBox;
                                  },
                                  onSuggestionSelected: (suggestion) {
                                    this._degreeInPostGraduationDetails.text =
                                        suggestion;
                                    setState(() {});

                                    // this._typeAheadController.dispose();
                                  },
                                  onSaved: (value) => {print("something")},
                                ),
                        )),
                        Container(
                          width: (70 / 360) * screenWidthTotal,
                          //height: (52 / 720) * screenHeightTotal,
                          margin: EdgeInsets.only(
                            left: 10/360 * screenWidthTotal,
                             top:grade.hasFocus || _yearInPostGraduationDetails.text.isNotEmpty || isSwitchedPostGraduation?(13/ 720) * screenHeightTotal:0
                          ),
                          child: TextInputFormField(
                              enabled: isSwitchedPostGraduation ? false : true,
                              focusNode: grade,
                              controller: _yearInPostGraduationDetails,
                              keyboardType: TextInputType.number,
                              lableText: "Year",
                              maxLength: 4,
                                fontStyle: TextStyles.loginAlternativeOption,
                                 lableStyle: TextStyles.labelStyle,
                              border: _yearInPostGraduationDetails.text.isEmpty ? UnderlineInputBorder(borderSide:BorderSide(color:SemanticColors.blueGrey.withOpacity(0.3),width: 1 )): UnderlineInputBorder(borderSide:BorderSide(color:SemanticColors.dark_purpely,width: 1 ) ),

                              textInputAction: TextInputAction.done,
                              onSaved: (value) => {},
                              onFieldSubmitted: (_) {
                                FocusScope.of(context)
                                    .requestFocus(new FocusNode());
                              }),
                        )
                      ],
                    ),
                  ),
                  Container(
                    height: 60/720 * screenHeightTotal,
                    margin: EdgeInsets.only(left: 3/360 * screenWidthTotal,top: (3/ 720) * screenHeightTotal, right: 3/360 * screenWidthTotal),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Expanded(
                            child: Container(
                          width: (133 / 360) * screenWidth,
                          //height: (52 / 720) * screenHeight,
                          margin: EdgeInsets.only(top: 0, bottom: 0),
                          child: isSwitchedPostGraduation
                              ?  new Container(
                                margin:  EdgeInsets.only(top: (13/ 720) * screenHeightTotal, bottom: 0),
                                child:new TextInputFormField(
                                  enabled: false,
                                    fontStyle: TextStyles.loginAlternativeOption,
                                 lableStyle: TextStyles.labelStyle,
                                  lableText: "Grade System",
                                ))
                              : new DropDownFormField(
                                  getImmediateSuggestions: true,
                                  textFieldConfiguration:
                                      TextFieldConfiguration(
                                  label: "Grade System",
                                    controller: this
                                        ._gradeSystemInPostGraduationDetails,
                                  ),
                                   suggestionsBoxDecoration: SuggestionsBoxDecoration(
                          dropDownHeight: 100,
                          borderRadius: new BorderRadius.circular(5.0),
                          color: Colors.white),
                                  suggestionsCallback: (pattern) {
                                    return GradeService.getSuggestions(pattern);
                                  },
                                  itemBuilder: (context, suggestion) {
                                    return ListTile(
                          dense: true,
                          contentPadding: EdgeInsets.only(left: 10),
                          title: Text(
                            suggestion,
                            style: TextStyle(
                              fontFamily: "IBMPlexSans",
                              fontWeight: FontWeight.w400,
                              fontSize: (13 / 720) * screenHeight,
                              color: NeutralColors.bluey_grey,
                              textBaseline: TextBaseline.alphabetic,
                              letterSpacing: 0.0,
                              inherit: false,
                              
                            ),
                          ),
                        );
                                  },
                                  transitionBuilder:
                                      (context, suggestionsBox, controller) {
                                    return suggestionsBox;
                                  },
                                  onSuggestionSelected: (suggestion) {
                                    this
                                        ._gradeSystemInPostGraduationDetails
                                        .text = suggestion;
                                    setState(() {});

                                    // this._typeAheadController.dispose();
                                  },
                                  onSaved: (value) => {print("something")},
                                ),
                        )),
                        Container(
                          width: (70 / 360) * screenWidthTotal,
                          //height: (52 / 720) * screenHeightTotal,
                          margin: EdgeInsets.only(
                            left: 10/360 * screenWidthTotal,
                           top: percentile.hasFocus || _percentageInPostGraduationDetails.text.isNotEmpty || isSwitchedPostGraduation?(13/ 720) * screenHeightTotal:0
                          ),
                          child: TextInputFormField(
                            enabled: isSwitchedPostGraduation ? false : true,
                            controller: _percentageInPostGraduationDetails,
                            focusNode: percentile,
                            keyboardType: TextInputType.number,
                            maxLength: 2,
                              fontStyle: TextStyles.loginAlternativeOption,
                                 lableStyle: TextStyles.labelStyle,
                            lableText: 'Percentage',
                            border: _percentageInPostGraduationDetails.text.isEmpty ? UnderlineInputBorder(borderSide:BorderSide(color:SemanticColors.blueGrey.withOpacity(0.3),width: 1 )): UnderlineInputBorder(borderSide:BorderSide(color:SemanticColors.dark_purpely,width: 1 ) ),

                            textInputAction: TextInputAction.done,
                            onSaved: (value) => {},
                            onFieldSubmitted: (_) {
                              FocusScope.of(context)
                                  .requestFocus(new FocusNode());
                            },
                          ),
                        )
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}

void CalculateScreen(BuildContext context) {
  var _mediaQueryData = MediaQuery.of(context);
  screenWidthTotal = _mediaQueryData.size.width;
  screenHeightTotal = _mediaQueryData.size.height;
  _safeAreaHorizontal =
      _mediaQueryData.padding.left + _mediaQueryData.padding.right;
  _safeAreaVertical =
      _mediaQueryData.padding.top + _mediaQueryData.padding.bottom;

  screenHeight = screenHeightTotal - _safeAreaVertical;
  screenWidth = screenWidthTotal - _safeAreaHorizontal;
}

import 'package:flutter/material.dart';
import 'package:imsindia/components/Input_forms.dart';
import 'package:imsindia/components/confirm_address_button.dart';
import 'package:imsindia/components/custom_DropDown.dart';
import 'package:imsindia/components/primary_Button.dart';
import 'package:imsindia/components/primary_button_gradient.dart';
import 'package:imsindia/resources/strings/login.dart';
import 'package:imsindia/routers/routes.dart';
import 'package:imsindia/utils/colors.dart';
import 'package:imsindia/utils/svg_icons.dart';
import 'package:imsindia/utils/text_styles.dart';
import 'package:imsindia/utils/validator.dart';
import 'package:imsindia/views/login_page/login_congratulations.dart';
import 'package:imsindia/views/login_page/login_enrollmentid_widget.dart';
import 'package:imsindia/views/login_page/login_work_experience.dart';

import 'data.dart';

class LoginConfirmaddressWidget extends StatefulWidget {
  @override
  LoginConfirmaddressWidgetState createState() =>
      LoginConfirmaddressWidgetState();
}
final TextEditingController _houseNo_controller =  TextEditingController();
final TextEditingController _streetName_controller = new TextEditingController();
final TextEditingController _landMark_controller = new TextEditingController();
final TextEditingController _city_controller = new TextEditingController();
final TextEditingController _pincode_controller = new TextEditingController();

String get houseNo => _houseNo_controller.text;
String get landMark => _landMark_controller.text;
String get streetName => _streetName_controller.text;
String get city => _city_controller.text;
String get pincode => _pincode_controller.text;

bool houseNoValidator = false;
bool streetNameValidator = false;
bool landMarkValidator = false;
bool cityValidator = false;
bool pinCodeValidator = false;

class LoginConfirmaddressWidgetState extends State<LoginConfirmaddressWidget> {
  final TextEditingController controller = new TextEditingController();
final TextEditingController _states = new TextEditingController();

  List<String> _colors = <String>[
    'Maharashtra',
    'Bangalore',
    'Chenai',
    'Delhi',
    'Goa',
  ];
  String _color = 'Maharashtra';
  final streetFocus = FocusNode();
  final landMarkFocus = FocusNode();
  final cityFocus = FocusNode();
  void initState() {
    super.initState();
    setState(() {
      _houseNo_controller.text = '';
      _streetName_controller.text = '';
      _landMark_controller.text = '';
      _city_controller.text = '';
      _pincode_controller.text = '';
    });
    _landMark_controller.addListener(() {
      setState(() {
        validateLandmarkAddress(landMark);
      });
    });
    _city_controller.addListener(() {
      setState(() {
        validateCityAddress(city);
      });
    });
    _houseNo_controller.addListener(() {
      setState(() {
        validateHomeAddress(houseNo);
      });
    });
    _streetName_controller.addListener(() {
      setState(() {
        validateStreetAddress(streetName);
      });
    });
    _pincode_controller.addListener(() {
      setState(() {
        validatePinCode(pincode);
      });
    });
  }
  static String validateHomeAddress(String value){
    if (value.isEmpty && value.length==0) {
      houseNoValidator=false;
      //('isEmpty');
      return null;
    }else if(value.length<5){
      houseNoValidator=false;
      return ThreeStepLoginLables.invalidAddresslength;

    }
    Pattern pattern = r'(^[a-zA-Z0-9\s,#().-]*$)';
    RegExp regex = new RegExp(pattern);
    if (!regex.hasMatch(value.trim())) {
      //('isEmptyfsgsrgfsgfdfgdfg');
      houseNoValidator=false;
     }
    if (regex.hasMatch(value.trim())) {
      //('isEmptygbbdghd');
      //(houseNoValidator);
      //(streetNameValidator);
      //(landMarkValidator);
      //(cityValidator);

      houseNoValidator=true;
      streetNameValidator=true;
      landMarkValidator=true;
      cityValidator=true;
      return null;
    }
  }
  static String validateStreetAddress(String value){
    if (value.isEmpty && value.length==0) {
      streetNameValidator=false;

      //('isEmpty');
      return null;
    }else if(value.length<5){
      streetNameValidator=false;
      return ThreeStepLoginLables.invalidAddresslength;

    }
    Pattern pattern = r'(^[a-zA-Z0-9\s,#().-]*$)';
    RegExp regex = new RegExp(pattern);
    if (!regex.hasMatch(value.trim())) {
      streetNameValidator=false;
      return ThreeStepLoginLables.invalidAddress;
    }
    if (regex.hasMatch(value.trim())) {
      //(streetNameValidator);
      streetNameValidator=true;
      return null;
    }
  }
  static String validateLandmarkAddress(String value){
    if (value.isEmpty && value.length==0) {
      landMarkValidator=false;
        //('isEmpty');
      return null;
    }else if(value.length<5){
      landMarkValidator=false;
      return ThreeStepLoginLables.invalidAddresslength;

    }
    Pattern pattern = r'(^[a-zA-Z0-9\s,#().-]*$)';
    RegExp regex = new RegExp(pattern);
    if (!regex.hasMatch(value.trim())) {
      landMarkValidator=false;
      return ThreeStepLoginLables.invalidAddress;
    }
    if (regex.hasMatch(value.trim())) {
      //(landMarkValidator);
      landMarkValidator=true;
      return null;
    }
  }
  static String validateCityAddress(String value){
    if (value.isEmpty && value.length==0) {
      cityValidator=false;

      //('isEmpty');
      return null;
    }else if(value.length<5){
      cityValidator=false;
      return ThreeStepLoginLables.invalidAddresslength;

    }
    Pattern pattern = r'(^[a-zA-Z0-9\s,#().-]*$)';
    RegExp regex = new RegExp(pattern);
    if (!regex.hasMatch(value.trim())) {
      //('isEmptyfsgsrgfsgfdfgdfg');
      cityValidator=false;
      return ThreeStepLoginLables.invalidAddress;
    }
    if (regex.hasMatch(value.trim())) {
      //(cityValidator);
      cityValidator=true;
      return null;
    }
  }
  static String validatePinCode(String value){
    if (value.isEmpty) {
      pinCodeValidator=false;
      return null;
    }
//    else if(value.length<5){
//      pinCodeValidator=false;
//      return LoginConfirmaddressLables.pincodeLength;
//    }
    Pattern pattern = r'^[1-9][0-9]{5}$';
    RegExp regex = new RegExp(pattern);
    if (!regex.hasMatch(value.trim())) {
      pinCodeValidator=false;

      return ThreeStepLoginLables.invalidPincode;
    }
    if (regex.hasMatch(value.trim())) {
      pinCodeValidator=true;

      return null;
    }
  }
  @override
  Widget build(BuildContext context) {
    Future<bool> _onWillPop() {
      AppRoutes.push(context, LoginEnrollmentidWidget());
    }

    final screenHeight = MediaQuery.of(context).size.height;
    final screenWidth = MediaQuery.of(context).size.width;

    return WillPopScope(
      onWillPop: _onWillPop,
      child: Scaffold(
        //    
        body: GestureDetector(
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: Container(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                Container(
                  height: (82 / 720) * screenHeight,
                  margin: EdgeInsets.only(left: 8),
                  child: Row(
                    children: [
                      GestureDetector(
                        onTap: () {
                          AppRoutes.push(context, LoginEnrollmentidWidget());

                        },
                        child: Align(
                          alignment: Alignment.topLeft,
                          child: Container(
                            width: (20 / 360) * screenWidth,
                            height: (20 / 720) * screenHeight,
                            margin: EdgeInsets.only(
                                top: screenHeight * (52/720),
                                left: (15/360)*screenWidth,
                              right: (15/360)*screenWidth,

                            ),
                            child: getSvgIcon.backSvgIcon,
                          ),
                        ),
                      ),
                      Spacer(),
                    ],
                  ),
                ),
                Expanded(
                  child: Container(
                    child: SingleChildScrollView(
                      child: Container(
                        child: Column(
                          children: <Widget>[
                            Container(
                              // height: screenHeight * 480 / 720,
                              margin: EdgeInsets.only(
                                  left: screenWidth * 43 / 360,
                                  right: screenWidth * 41 / 360),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.stretch,
                                children: [
                                  Align(
                                    alignment: Alignment.topCenter,
                                    child: Container(
                                      margin: EdgeInsets.only(
                                          top: screenHeight * 12 / 720),
                                      child: Text(
                                        "Where should we deliver\nyou study material?",
                                        style: TextStyle(
                                          color:
                                              Color.fromARGB(255, 45, 52, 56),
                                          fontSize: screenWidth * (21 / 360),
                                          fontFamily: "IBMPlexSans",
                                          fontWeight: FontWeight.w700,
                                        ),
                                        textAlign: TextAlign.center,
                                      ),
                                    ),
                                  ),
                                  Container(
                                    width: double.infinity,
                                    //   height: screenHeight * 49 / 720,
                                    margin: EdgeInsets.only(
                                        top: screenHeight * 42 / 720),
                                    child: TextInputFormField(
                                      controller: _houseNo_controller,
                                      validator: validateHomeAddress,
                                      keyboardType: TextInputType.text,
                                      lableText:
                                          LoginConfirmaddressLables.houseNo,
                                          fontStyle: TextStyles.editTextStyle,
                                          lableStyle: TextStyles.labelStyle,
                                      border:_houseNo_controller.text.isEmpty ? UnderlineInputBorder(borderSide:BorderSide(color:SemanticColors.blueGrey.withOpacity(0.3),width: 1 )): UnderlineInputBorder(borderSide:BorderSide(color:SemanticColors.dark_purpely,width: 1 ) ),
                                      textInputAction: TextInputAction.next,
                                      autovalidate: true,
                                    //  onSaved: (value) => houseNo = value,
                                      onFieldSubmitted: (_) =>
                                          FocusScope.of(context)
                                              .requestFocus(streetFocus),
                                    ),
                                  ),
                                  Container(
                                    width: double.infinity,
                                    //  height: screenHeight * 49 / 720,
                                    margin: EdgeInsets.only(
                                        top: screenHeight * 19 / 720),
                                    child: TextInputFormField(
                                      controller: _streetName_controller,
                                      focusNode: streetFocus,
                                       fontStyle: TextStyles.editTextStyle,
                                          lableStyle: TextStyles.labelStyle,
                                      validator: validateStreetAddress,
                                      keyboardType: TextInputType.text,
                                      lableText:
                                          LoginConfirmaddressLables.StreetName,
                                      border: _streetName_controller.text.isEmpty ? UnderlineInputBorder(borderSide:BorderSide(color:SemanticColors.blueGrey.withOpacity(0.3),width: 1 )): UnderlineInputBorder(borderSide:BorderSide(color:SemanticColors.dark_purpely,width: 1 ) ),
                                      textInputAction: TextInputAction.next,
                                      autovalidate: true,
                                    //  onSaved: (value) => streetName = value,
                                      onFieldSubmitted: (_) =>
                                          FocusScope.of(context)
                                              .requestFocus(landMarkFocus),
                                    ),
                                  ),
                                  Container(
                                    width: double.infinity,
                                    //  height: screenHeight * 49 / 720,
                                    margin: EdgeInsets.only(
                                        top: screenHeight * 19 / 720),
                                    child: TextInputFormField(
                                      controller: _landMark_controller,
                                      focusNode: landMarkFocus,
                                       fontStyle: TextStyles.editTextStyle,
                                          lableStyle: TextStyles.labelStyle,
                                      keyboardType: TextInputType.text,
                                      validator: validateLandmarkAddress,
                                      lableText:
                                          LoginConfirmaddressLables.Landmark,
                                      textInputAction: TextInputAction.next,
                                      border: _landMark_controller.text.isEmpty ? UnderlineInputBorder(borderSide:BorderSide(color:SemanticColors.blueGrey.withOpacity(0.3),width: 1 )): UnderlineInputBorder(borderSide:BorderSide(color:SemanticColors.dark_purpely,width: 1 ) ),
                                      autovalidate: true,
                                     // onSaved: (value) => landmark = value,
                                      onFieldSubmitted: (_) =>
                                          FocusScope.of(context)
                                              .requestFocus(cityFocus),
                                    ),
                                  ),
                                  Container(
                                    width: double.infinity,
                                    //  height: screenHeight * 49 / 720,
                                    margin: EdgeInsets.only(
                                        top: screenHeight * 19 / 720),
                                    child: TextInputFormField(
                                      controller: _city_controller,
                                      focusNode: cityFocus,
                                      validator: validateCityAddress,
                                      keyboardType: TextInputType.text,
                                      lableText: LoginConfirmaddressLables.city,
                                       fontStyle: TextStyles.editTextStyle,
                                          lableStyle: TextStyles.labelStyle,
                                      textInputAction: TextInputAction.done,
                                      border: _city_controller.text.isEmpty ? UnderlineInputBorder(borderSide:BorderSide(color:SemanticColors.blueGrey.withOpacity(0.3),width: 1 )): UnderlineInputBorder(borderSide:BorderSide(color:SemanticColors.dark_purpely,width: 1 ) ),
                                      autovalidate: true,
                                     // onSaved: (value) => city = value,
                                      // onFieldSubmitted: (_) => FocusScope.of(context).requestFocus(),
                                    ),
                                  ),
                                  Container(
                                    //  height: (79/720)*screenHeight,
                                    margin:
                                        EdgeInsets.only(top: 9.5, right: 00),
                                    child: Row(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Expanded(
                                            child: Container(
                                          width: (125 / 360) * screenWidth,
                                          //  height: (69/720)*screenHeight,
                                          margin: EdgeInsets.only(
                                              top: 8, bottom: 0),
                                          child: DropDownFormField(
                      getImmediateSuggestions: true,

                      // autoFlipDirection: true,
                      textFieldConfiguration: TextFieldConfiguration(
                          controller: _states,
                          label: "State"),
                      suggestionsBoxDecoration: SuggestionsBoxDecoration(
                          dropDownHeight: 150,
                          borderRadius: new BorderRadius.circular(5.0),
                          color: Colors.white),
                      suggestionsCallback: (pattern) {
                        return StatesService.getSuggestions(pattern);
                      },
                      itemBuilder: (context, suggestion) {
                        return ListTile(
                          dense: true,
                          contentPadding: EdgeInsets.only(left: 10),
                          title: Text(
                            suggestion,
                            style: TextStyle(
                              fontFamily: "IBMPlexSans",
                              fontWeight: FontWeight.w400,
                              fontSize: (13 / 720) * screenHeight,
                              color: NeutralColors.bluey_grey,
                              textBaseline: TextBaseline.alphabetic,
                              letterSpacing: 0.0,
                              inherit: false,
                              
                            ),
                          ),
                        );
                      },
                      hideOnLoading: true,
                      debounceDuration: Duration(milliseconds: 100),
                      transitionBuilder: (context, suggestionsBox, controller) {
                        return suggestionsBox;
                      },
                      onSuggestionSelected: (suggestion) {
                        for (int i = 0; i < StatesService.states.length; i++) {
                          if (StatesService.states[i] == suggestion) {
                          }
                        }
                        print(suggestion);
                        _states.text = suggestion;
                        // setState(() {});
                      },
                      onSaved: (value) => {print("something")},
                    )
                                        )),
                                        Container(
                                          width: (125 / 360) * screenWidth,
                                          //   height: (69/720)*screenHeight,
                                          margin: EdgeInsets.only(
                                              left: 20, top: 16),
                                          child: TextInputFormField(
                                            controller: _pincode_controller,
                                            keyboardType: TextInputType.number,
                                            lableText: "Pincode",
                                            maxLength: 6,
                                            lableStyle: TextStyles.labelStyle,
                                            fontStyle: TextStyles.editTextStyle,
                                            textInputAction:
                                                TextInputAction.done,
                                            border: _pincode_controller.text.isEmpty ? UnderlineInputBorder(borderSide:BorderSide(color:SemanticColors.blueGrey.withOpacity(0.3),width: 1 )): UnderlineInputBorder(borderSide:BorderSide(color:SemanticColors.dark_purpely,width: 1 ) ),
                                            autovalidate: true,
                                            validator:
                                                validatePinCode,
                                            onSaved: (value) => _color = value,
                                            //onFieldSubmitted: (_) => FocusScope.of(context).requestFocus(FocusNode()),
                                          ),
                                        )
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
//                          Container(
//                            height: screenHeight * 45 / 720,
//                            margin: EdgeInsets.only(
//                                left: 47, top: (39.5 * screenHeight) / 720, right: 43),
//                            child: ConfirmAddressButton(
//                              //height: screenHeight * 0.055,
//                              onTap: () {
//                                if ((FieldValidator.address == true && FieldValidator.pinCode==true)) {
//                                  AppRoutes.push(context, LoginCongratulations());
//                                  //("hello");
//                                } else {
//                                  //("bye");
//                                }
//                              },
//                              text: LoginAppStrings.createAccount,
//                            ),
//                          ),
                            Container(
                              height: screenHeight * 45 / 720,
                              margin: EdgeInsets.only(
                                  left: 47,
                                  top: (39.5 * screenHeight) / 720,
                                  right: 43),
                              child: PrimaryButton(
                                //height: screenHeight * 0.055,
                                gradient: (houseNoValidator == true &&
                                    streetNameValidator == true && cityValidator == true &&
                                    landMarkValidator == true && pinCodeValidator == true
                                )
                                    ? true
                                    : false,
                                onTap: () {
                                  if ((houseNoValidator == true &&
                                      streetNameValidator == true && cityValidator == true &&
                                      landMarkValidator == true && pinCodeValidator == true
                                      )) {
                                    AppRoutes.push(
                                        context, LoginCongratulations());
                                    //("hello");
                                  } else {
                                    //("bye");
                                  }
                                },
                                text: LoginConfirmaddressLables.confirmAddress
                                    .toUpperCase(),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

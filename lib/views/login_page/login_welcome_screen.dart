import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_facebook_login/flutter_facebook_login.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:http/http.dart' as http;
import 'package:imsindia/api/bloc/services.dart';
import 'package:imsindia/components/Input_forms.dart';
import 'package:imsindia/components/custom_DropDown.dart';
import 'package:imsindia/components/primary_Button.dart';
import 'package:imsindia/resources/strings/login.dart';
import 'package:imsindia/routers/routes.dart';
import 'package:imsindia/utils/colors.dart';
import 'package:imsindia/utils/svg_icons.dart';
import 'package:imsindia/utils/text_styles.dart';
import 'package:imsindia/views/Arun/ims_utility.dart';
import 'package:imsindia/views/login_page/login_screen.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LoginWelcomeWidget extends StatefulWidget {
  @override
  _LoginWelcomeWidgetState createState() => _LoginWelcomeWidgetState();
}

final TextEditingController _user = new TextEditingController();
String get username => _user.text;
bool emailID = false;

class _LoginWelcomeWidgetState extends State<LoginWelcomeWidget> {
  static final FacebookLogin facebookSignIn = new FacebookLogin();
  final GlobalKey<ScaffoldState> _scaffoldstate =
      new GlobalKey<ScaffoldState>();
  var facebookLogin = FacebookLogin();
  void initiateFacebookLogin() async {
    final FacebookLoginResult result =
        await facebookSignIn.logIn(['email']);

    switch (result.status) {
      case FacebookLoginStatus.loggedIn:
        print(result.accessToken.userId);
        showErrorMessage(result.accessToken.userId);
        print(result.accessToken.token);
        final graphResponse = await http.get(Uri.parse('https://graph.facebook.com/v2.12/me?fields=name,first_name,last_name,email&access_token=${result.accessToken.token}'));
        var results = json.decode(graphResponse.body);
        showErrorMessage(results['email']);
        break;
      case FacebookLoginStatus.cancelledByUser:
        break;
      case FacebookLoginStatus.error:
        break;
    }
  }

  GoogleSignIn _googleSignIn = GoogleSignIn(
    scopes: [
      'email',
      'https://www.googleapis.com/auth/contacts.readonly',
    ],
  );
  Future<void> _handleSignIn() async {
    try {
      await _googleSignIn.signIn();
      showErrorMessage(_googleSignIn.currentUser.email);
      print(_googleSignIn.currentUser);
      _googleSignIn.signOut();
    } catch (error) {
      print(error);
    }
  }

  void getUserName() async {
    //[Arun : what is this ?]
    //praveen: If user coming from password page we can display previously entered email as pre entered value
    SharedPreferences prefs = await SharedPreferences.getInstance();
    // _user.text = prefs.getString('userName');
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    setState(() {
       _user.text = '';
      //_user.text = 'IMS0000542307';

      getUserName();
    });
    _user.addListener(() {
      setState(() {
        validateEmailOrPin(username);
      });
    });
  }

  @override
  void didChangeDependencies() {
    //getUserName();
    //saveMenus();
    super.didChangeDependencies();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    //   FieldValidator.emailID;
  }

  void showErrorMessage(String value) {
    _scaffoldstate.currentState.showSnackBar(new SnackBar(
      content: new Text(value),
      duration: new Duration(seconds: 3),
    ));
  }

  void setUserName(value) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString(ImsStrings.sCurrentUserId, value);
    debugPrint('Arun : user id : ${value}');
    prefs.setString(ImsStrings.sCurrentUserName, value);
  }

  void check_Details_Using_Email_or_Pin() {
    Map postdata = {"txtemail": username, "txtpin": username};
    ApiService().Details_Using_Email_Or_Pin(postdata).then((result) {
      setState(() {
        print("Result value"+result.toString());
        if (result[0] == 'success') {
          setUserName(username);
          // globals.loginAccessToken=result[1]['data']['token'];
          // globals.courseId=courseId;
          //globals.sample=courseId;
          // setLoginStatusTrue();
          // setCourseId(courseId);
          //setToken(result[1]['data']['token']);
          AppRoutes.push(context, LoginFillpasswordWidget());
        } else {
          print('failed');
          showErrorMessage(result[0]);
        }
      });
    });
  }

  static String validateEmailOrPin(String value) {
    if (username.isEmpty || username.length == 0) {
      emailID = false;
      return null;
    }else{
      emailID = true;
      return null;
    }
//    if (_user.text.contains('@')) {
//      Pattern pattern =
//          r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
//
//      RegExp regex = new RegExp(pattern);
//
//      if (!regex.hasMatch(username.trim())) {
//        emailID = false;
//        return LoginAppLabels.invalidID;
//      }
//      if (regex.hasMatch(username.trim())) {
//        emailID = true;
//        return null;
//      }
//    } else {
//      if (username.length < 13) {
//        emailID = false;
//
//        return LoginUsingIMSPin.invalidIMSPasswordForLogin;
//      } else {
//        emailID = true;
//
//        return null;
//      }
//    }
  }

  @override
  Widget build(BuildContext context) {
    var screenWidth = MediaQuery.of(context).size.width;
    var screenHeight = MediaQuery.of(context).size.height;
    Future<bool> _onWillPop() {
      SystemChannels.platform.invokeMethod('SystemNavigator.pop');
    }

    return WillPopScope(
      onWillPop: _onWillPop,
      child: Scaffold(
        key: _scaffoldstate,
//      
//      
        body: SingleChildScrollView(
          padding: EdgeInsets.only(bottom: 40),
          child: Container(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
// Extra Container because we removed sign up, google, fb buttons                
                Container(
                  height: 100/720 * screenHeight,
                ),
                Padding(
                  padding: EdgeInsets.only(top: screenHeight * 80 / 720),
                  child: Center(
                    child: Text(
                      "Welcome to myIMS!",
                      style: TextStyle(
                        color: Color.fromARGB(255, 31, 37, 43),
                        fontSize: 21 / 720 * screenHeight,
                        fontFamily: "IBMPlexSans",
                        fontWeight: FontWeight.w700,
                      ),
                      textAlign: TextAlign.left,
                    ),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(top: screenHeight * 5 / 720),
                  child: Center(
                    child: Text(
                      "There is a lot to learn",
                      style: TextStyle(
                        color: Color.fromARGB(255, 126, 145, 154),
                        fontSize: 14 / 720 * screenHeight,
                        fontFamily: "IBMPlexSans",
                      ),
                      textAlign: TextAlign.left,
                    ),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(top: screenHeight * 12 / 720),
                  child: Center(
                    //margin: EdgeInsets.only(top: screenHeight * 0.0166),
                    child: getSvgIcon.welcomeSvgIcon,
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(
                      left: screenWidth * 45 / 360,
                      top: screenHeight * 47 / 720,
                      right: screenWidth * 45 / 360),
                  child: TextInputFormField(
                    controller: _user,
                    keyboardType: TextInputType.emailAddress,
                    lableText: LoginAppLabels.emailID,
                    textInputAction: TextInputAction.done,
                    border: username.isEmpty
                        ? UnderlineInputBorder(
                            borderSide: BorderSide(
                                color: SemanticColors.blueGrey.withOpacity(0.3),
                                width: 1.5))
                        : UnderlineInputBorder(
                            borderSide: BorderSide(
                                color: SemanticColors.dark_purpely,
                                width: 1.5)),
                    autovalidate: true,
                    //validator: validateEmailOrPin,
                    onSaved: (value) {},
                    lableStyle: TextStyles.labelStyle,
                    fontStyle: TextStyles.editTextStyle,
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(
                      left: screenWidth * 45 / 360,
                      top: screenHeight * 37.5 / 720,
                      right: screenWidth * 45 / 360),
                  child: Center(
                      child: PrimaryButton(
                    gradient: (emailID == true) ? true : false,

                    //height: screenHeight * 0.055,
                    onTap: () {
                      if (emailID == true) {
                        check_Details_Using_Email_or_Pin();
                        //   AppRoutes.push(context, LoginFillpasswordWidget());
                      } else {}
                    },
                    text: LoginAppStrings.loginButton,
                  ) // Group 4
                      ),
                ),
                // Align(
                //   alignment: Alignment.topCenter,
                //   child: Container(
                //     width: screenWidth * 0.33,
                //     height: screenHeight * 0.025,
                //     margin: EdgeInsets.only(top: screenHeight * 0.047),
                //     child: Stack(
                //       alignment: Alignment.center,
                //       children: [
                //         Positioned(
                //           child: Text(
                //             "or",
                //             style: TextStyle(
                //               color: Color.fromARGB(255, 0, 3, 44),
                //               fontSize: 14 / 720 * screenHeight,
                //               fontFamily: "IBMPlexSans",
                //             ),
                //             textAlign: TextAlign.left,
                //           ),
                //         ),
                //         Positioned(
                //           left: 0,
                //           right: 0,
                //           child: Container(
                //             height: screenHeight * 0.0013,
                //             child: Row(
                //               crossAxisAlignment: CrossAxisAlignment.stretch,
                //               children: [
                //                 Align(
                //                   alignment: Alignment.centerLeft,
                //                   child: Container(
                //                     width: screenWidth * 0.138,
                //                     height: screenHeight * 0.0013,
                //                     decoration: BoxDecoration(
                //                       color: Color.fromARGB(255, 126, 145, 154),
                //                     ),
                //                     child: Container(),
                //                   ),
                //                 ),
                //                 Spacer(),
                //                 Align(
                //                   alignment: Alignment.centerLeft,
                //                   child: Container(
                //                     width: screenWidth * 0.138,
                //                     height: screenHeight * 0.0013,
                //                     decoration: BoxDecoration(
                //                       color: Color.fromARGB(255, 126, 145, 154),
                //                     ),
                //                     child: Container(),
                //                   ),
                //                 ),
                //               ],
                //             ),
                //           ),
                //         ),
                //       ],
                //     ),
                //   ),
                // ),
                // Padding(
                //   padding: EdgeInsets.only(top: screenHeight * 22 / 720),
                //   child: Center(
                //     child: Text(
                //       "Login using",
                //       style: TextStyle(
                //         color: Color.fromARGB(255, 126, 145, 154),
                //         fontSize: 14 / 720 * screenHeight,
                //         fontFamily: "IBMPlexSans",
                //       ),
                //       textAlign: TextAlign.center,
                //     ),
                //   ),
                // ),
                // Padding(
                //   //height: screenHeight * 0.055,
                //   padding: EdgeInsets.only(
                //       left: screenWidth * 45 / 360,
                //       top: screenHeight * 15 / 720,
                //       right: screenWidth * 45 / 360),
                //   child: Row(
                //     crossAxisAlignment: CrossAxisAlignment.start,
                //     children: [
                //       Container(
                //         width: screenWidth * 130 / 360,
                //         height: screenHeight * 40 / 720,
                //         child: GestureDetector(
                //             onTap: () {
                //               _handleSignIn();
                //             },
                //             child: getSvgIcon.googleSvgIcon),
                //       ),
                //       Spacer(),
                //       Container(
                //         width: screenWidth * 130 / 360,
                //         height: screenHeight * 40 / 720,
                //         child: GestureDetector(
                //           onTap: () {
                //             initiateFacebookLogin();
                //           },
                //           child: getSvgIcon.facebookSvgIcon,
                //         ),
                //       ),
                //     ],
                //   ),
                // ),
                // // Padding(
                // //   padding: EdgeInsets.only(
                // //       left: 106/360 * screenWidth,
                // //       right: 106/360 * screenWidth,
                // //       top: 50/720 * screenHeight),
                // //   child: Text(
                // //     "Dont have an account?",
                // //     style: TextStyle(
                // //       color: Color.fromARGB(255, 87, 93, 96),
                // //       fontSize: 12/720 * screenHeight,
                // //       fontFamily: "IBMPlexSans",
                // //     ),
                // //     textAlign: TextAlign.center,
                // //   ),
                // // ),
                // // Padding(
                // //   padding: EdgeInsets.only(
                // //       top: 0),
                // //   child: Center(
                // //     child: new RichText(
                // //       text: new TextSpan(
                // //         children: [
                // //           new TextSpan(
                // //             text: "Signup",
                // //             style: new TextStyle(
                // //               fontSize: 12/720 * screenHeight,
                // //               fontFamily: "IBMPlexSans",
                // //               color: Color.fromARGB(255, 0, 171, 251),
                // //             ),
                // //             recognizer: new TapGestureRecognizer()
                // //               ..onTap = () {
                // //                 //AppRoutes.push(context, LoginCreateaccountfilledWidget());
                // //               },
                // //           ),
                // //           new TextSpan(
                // //             text: " through our website",
                // //             style: new TextStyle(
                // //               color: Color.fromARGB(255, 87, 93, 96),
                // //               fontSize: 12/720 * screenHeight,
                // //               fontFamily: "IBMPlexSans",
                // //             ),
                // //           ),
                // //         ],
                // //       ),
                // //     ),
                // //   ),

                // // ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  onFacebookPressed(BuildContext context) {}
}


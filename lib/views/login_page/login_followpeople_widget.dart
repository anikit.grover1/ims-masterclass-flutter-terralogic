
import 'package:flutter/material.dart';
import 'package:imsindia/routers/routes.dart';
import 'package:imsindia/utils/svg_icons.dart';
import 'package:imsindia/utils/colors.dart';
import 'package:imsindia/views/home_pages/home_widget.dart';
import 'package:imsindia/views/login_page/login_congratulations.dart';
//import 'package:ims_login3/screens/login_profilecreated_widget.dart';


class LoginFollowpeopleWidget extends StatefulWidget {
  @override
  LoginFollowpeopleWidgetState createState() => LoginFollowpeopleWidgetState();
}


class LoginFollowpeopleWidgetState extends State<LoginFollowpeopleWidget>{
  bool followSelected = false;
  List<dynamic> name = [
    "Delia Chambers",
    "Chase Evans",
    "Minnie Anderson",
    "Wesley Riley",
    "Luis Hodges",
    ""
  ];
  List<dynamic> city = [
    "St. Xaviers, Mumbai",
    "Rachna Sansad, Mumbai",
    "National College, Mumbai",
    "Birla College, Mumbai",
    "Rachna Sansad, Mumbai",
    ""
  ];
  List<dynamic> follow = [];
  @override
  Widget build(BuildContext context) {
    final screenHeight = MediaQuery.of(context).size.height;
    final screenWidth = MediaQuery.of(context).size.width;
  
    return Scaffold(
      
      body: Container(
        constraints: BoxConstraints.expand(),
        decoration: BoxDecoration(
          color: Color.fromARGB(255, 255, 255, 255),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Container(
              height: (82 / 720) * screenHeight,
              margin: EdgeInsets.only(left: 8),
              child: Row(
                children: [
                  GestureDetector(
                    onTap: () {
                      Navigator.pop(context);
                    },
                    child: Container(
                      child: Padding(
                        padding: const EdgeInsets.all(5.0),
                        child: Align(
                          alignment: Alignment.topLeft,
                          child: Container(
                            width: (20 / 360) * screenWidth,
                            height: (20 / 720) * screenHeight,
                            margin: EdgeInsets.only(
                                top: screenHeight * 0.0738,
                                left: 8,
                                right: 5),
                            child: getSvgIcon.backSvgIcon,
                          ),
                        ),
                      ),
                    ),
                  ),
                  Spacer(),
                ],
              ),
            ),
            Container(
              //height: 581/720 * screenHeight,
              margin: EdgeInsets.only(left: 22/360 * screenWidth, top: 12/720 * screenHeight, right: 23/360 * screenWidth),
              child: Stack(
                alignment: Alignment.center,
                children: [
                   Container(
                      height: 581/720 * screenHeight,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: [
                          Container(
                            margin: EdgeInsets.only(left: 31/360 * screenWidth, top: 5/720 * screenHeight, right: 29/360 * screenWidth),
                            child: Text(
                              "We’ve found a few people\nyou could follow",
                              style: TextStyle(
                                color: NeutralColors.charcoal_grey,
                                fontSize: 21/720 * screenHeight,
                                fontFamily: "IBMPlexSans",
                                fontWeight: FontWeight.bold,
                              ),
                              textAlign: TextAlign.center,
                            ),
                          ),
                          Expanded(
                            child: Container(
                              //margin: EdgeInsets.only(top: 20/720 * screenHeight),
                              child: ListView.builder(
                                  itemCount: name.length,
                                  physics: NeverScrollableScrollPhysics(),
                                  itemBuilder: (context,index){
                                    return name.indexOf(name[index]) == name.length -1?
                                    Container(
                                      height: 80/720 * screenHeight,
                                      margin: EdgeInsets.only(top: 40/720 * screenHeight),
                                      child: Column(
                                        crossAxisAlignment: CrossAxisAlignment.stretch,
                                        children: [
                                          GestureDetector(
                                            onTap: (){
                                              AppRoutes.push(context, HomeWidget());
                                            },
                                            child: Container(
                                              height: 40/720 * screenHeight,
                                              margin: EdgeInsets.only(left: 25/360 * screenWidth, right: 25/360 * screenWidth),
                                              decoration: BoxDecoration(
                                                gradient: LinearGradient(
                                                  begin: Alignment(-0.011, 0.494),
                                                  end: Alignment(1.031, 0.516),
                                                  stops: [
                                                    0,
                                                    1,
                                                  ],
                                                  colors: [
                                                    Color.fromARGB(255, 51, 128, 204),
                                                    Color.fromARGB(255, 137, 110, 216),
                                                  ],
                                                ),
                                                borderRadius: BorderRadius.all(Radius.circular(2)),
                                              ),
                                              child: Column(
                                                mainAxisAlignment: MainAxisAlignment.center,
                                                children: [
                                                  Text(
                                                    "DONE",
                                                    style: TextStyle(
                                                      color: Colors.white,
                                                      fontSize: 14/720 * screenHeight,
                                                      fontFamily: "IBMPlexSans",
                                                      fontWeight: FontWeight.w500,
                                                    ),
                                                    textAlign: TextAlign.center,
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ),
                                          GestureDetector(
                                            onTap: (){
                                              AppRoutes.push(context, HomeWidget());
                                            },
                                            child: Container(
                                              margin: EdgeInsets.only(left: 96/360 * screenWidth, top: 17/720 * screenHeight, right: 95/360 * screenWidth),
                                              child: Text(
                                                "Skip for now",
                                                style: TextStyle(
                                                  color: NeutralColors.blue_grey,
                                                  fontSize: 14/720 * screenHeight,
                                                  fontFamily: "IBMPlexSans",
                                                ),
                                                textAlign: TextAlign.center,
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                    )
                                     : Container(
                                      height: 40/720 * screenHeight,
                                      margin: EdgeInsets.only(left: 18/360 * screenWidth, top: 30/720 * screenHeight, right: 17/360 * screenWidth),
                                      child: Row(
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        children: [
                                          Container(
                                            width: 41/360 * screenWidth,
                                            height: 41/720 * screenHeight,
                                            child: Image.asset(
                                              "assets/images/oval-2.png",
                                              fit: BoxFit.none,
                                            ),
                                          ),
                                          Expanded(
                                            flex: 1,
                                            child: Container(
                                              height: 38/720 * screenHeight,
                                              margin: EdgeInsets.only(left: 10/360 * screenWidth, right: 10/360 * screenWidth),
                                              child: Column(
                                                crossAxisAlignment: CrossAxisAlignment.stretch,
                                                children: [
                                                  Container(
                                                    margin: EdgeInsets.only(right: 7/360 * screenWidth),
                                                    child: Text(
                                                      name[index],
                                                      style: TextStyle(
                                                        color: NeutralColors.dark_navy_blue,
                                                        fontSize: 14/720 * screenHeight,
                                                        fontFamily: "IBMPlexSans",
                                                      ),
                                                      textAlign: TextAlign.left,
                                                    ),
                                                  ),
                                                  Spacer(),
                                                  Align(
                                                    alignment: Alignment.topLeft,
                                                    child: Text(
                                                      city[index],
                                                      style: TextStyle(
                                                        color: NeutralColors.blue_grey,
                                                        fontSize: 12/720 * screenHeight,
                                                        fontFamily: "IBMPlexSans",
                                                      ),
                                                      textAlign: TextAlign.left,
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ),
                                          Align(
                                            alignment: Alignment.centerLeft,
                                            child: Container(
                                              width: 70/360 * screenWidth,
                                              height: 25/720 * screenHeight,
                                              child: FlatButton(
                                                onPressed: () {
                                                  setState(() {
                                                    print(index);
                                                    if(follow.contains(index)){
                                                      follow.remove(index);
                                                    }
                                                    else{
                                                      follow.add(index);
                                                    }
                                                    followSelected = !followSelected;
                                                  });
                                                },
                                                color: follow.contains(index)?Colors.transparent:Color.fromARGB(255, 0, 171, 251),
                                                shape: follow.contains(index)?
                                                RoundedRectangleBorder(
                                                  borderRadius: BorderRadius.all(Radius.circular(12.5)),
                                                  side: BorderSide(
                                                    width: 1,
                                                    color: Color.fromARGB(255, 0, 171, 251),
                                                    style: BorderStyle.solid,
                                                  ),
                                                )
                                                  :RoundedRectangleBorder(
                                                    borderRadius: BorderRadius.all(Radius.circular(12.5)),
                                                  ),
                                                textColor: follow.contains(index)?Color.fromARGB(255, 0, 171, 251):Color.fromARGB(255, 255, 255, 255),
                                                padding: EdgeInsets.all(0),
                                                child: Text(
                                                  follow.contains(index)?"Unfollow":"Follow",
                                                  style: TextStyle(
                                                    fontSize: 14/720 * screenHeight,
                                                    fontFamily: "IBMPlexSans",
                                                  ),
                                                  textAlign: TextAlign.center,
                                                ),
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                    );
                                  }),
                            ),
                          ),
//                          Container(
//                            height: 40/720 * screenHeight,
//                            margin: EdgeInsets.only(left: 18/360 * screenWidth, top: 42/720 * screenHeight, right: 17/360 * screenWidth),
//                            child: Row(
//                              crossAxisAlignment: CrossAxisAlignment.start,
//                              children: [
//                                Container(
//                                  width: 41/360 * screenWidth,
//                                  height: 41/720 * screenHeight,
//                                  child: Image.asset(
//                                    "assets/images/oval-2.png",
//                                    fit: BoxFit.none,
//                                  ),
//                                ),
//                                Expanded(
//                                  flex: 1,
//                                  child: Container(
//                                    height: 38/720 * screenHeight,
//                                    margin: EdgeInsets.only(left: 10/360 * screenWidth, right: 10/360 * screenWidth),
//                                    child: Column(
//                                      crossAxisAlignment: CrossAxisAlignment.stretch,
//                                      children: [
//                                        Container(
//                                          margin: EdgeInsets.only(right: 7/360 * screenWidth),
//                                          child: Text(
//                                            "Delia Chambers",
//                                            style: TextStyle(
//                                              color: NeutralColors.dark_navy_blue,
//                                              fontSize: 14/720 * screenHeight,
//                                              fontFamily: "IBM Plex Sans",
//                                            ),
//                                            textAlign: TextAlign.left,
//                                          ),
//                                        ),
//                                        Spacer(),
//                                        Align(
//                                          alignment: Alignment.topLeft,
//                                          child: Text(
//                                            "St. Xaviers, Mumbai",
//                                            style: TextStyle(
//                                              color: NeutralColors.blue_grey,
//                                              fontSize: 12/720 * screenHeight,
//                                              fontFamily: "IBM Plex Sans",
//                                            ),
//                                            textAlign: TextAlign.left,
//                                          ),
//                                        ),
//                                      ],
//                                    ),
//                                  ),
//                                ),
//                                Align(
//                                  alignment: Alignment.centerLeft,
//                                  child: Container(
//                                    width: 70/360 * screenWidth,
//                                    height: 25/720 * screenHeight,
//                                    child: FlatButton(
//                                      onPressed: () {},
//                                      color: Colors.transparent,
//                                      shape: RoundedRectangleBorder(
//                                        borderRadius: BorderRadius.all(Radius.circular(12.5)),
//                                        side: BorderSide(
//                                          width: 1,
//                                          color: Color.fromARGB(255, 0, 171, 251),
//                                          style: BorderStyle.solid,
//                                        ),
//                                      ),
//                                      textColor: Color.fromARGB(255, 0, 171, 251),
//                                      padding: EdgeInsets.all(0),
//                                      child: Text(
//                                        "Unfollow",
//                                        style: TextStyle(
//                                          fontSize: 14/720 * screenHeight,
//                                          fontFamily: "IBM Plex Sans",
//                                        ),
//                                        textAlign: TextAlign.center,
//                                      ),
//                                    ),
//                                  ),
//                                ),
//                              ],
//                            ),
//                          ),
//                          Container(
//                            height: 40/720 * screenHeight,
//                            margin: EdgeInsets.only(left: 18/360 * screenWidth, top: 30/720 * screenHeight, right: 17/360 * screenWidth),
//                            child: Row(
//                              crossAxisAlignment: CrossAxisAlignment.start,
//                              children: [
//                                Container(
//                                  width: 41/360 * screenWidth,
//                                  height: 41/720 * screenHeight,
//                                  child: Image.asset(
//                                    "assets/images/oval-2.png",
//                                    fit: BoxFit.none,
//                                  ),
//                                ),
//                                Expanded(
//                                  flex: 1,
//                                  child: Container(
//                                    height: 38/720 * screenHeight,
//                                    margin: EdgeInsets.only(left: 10/360 * screenWidth, right: 10/360 * screenWidth),
//                                    child: Column(
//                                      crossAxisAlignment: CrossAxisAlignment.start,
//                                      children: [
//                                        Text(
//                                          "Chase Evans",
//                                          style: TextStyle(
//                                            color: NeutralColors.dark_navy_blue,
//                                            fontSize: 14/720 * screenHeight,
//                                            fontFamily: "IBM Plex Sans",
//                                          ),
//                                          textAlign: TextAlign.left,
//                                        ),
//                                        Spacer(),
//                                        Text(
//                                          "Rachna Sansad, Mumbai",
//                                          style: TextStyle(
//                                            color: NeutralColors.blue_grey,
//                                            fontSize: 12/720 * screenHeight,
//                                            fontFamily: "IBM Plex Sans",
//                                          ),
//                                          textAlign: TextAlign.left,
//                                        ),
//                                      ],
//                                    ),
//                                  ),
//                                ),
//                                Align(
//                                  alignment: Alignment.centerLeft,
//                                  child: Container(
//                                    width: 70/360 * screenWidth,
//                                    height: 25/720 * screenHeight,
//                                    child: FlatButton(
//                                     onPressed: () {},
//                                      color: Color.fromARGB(255, 0, 171, 251),
//                                      shape: RoundedRectangleBorder(
//                                        borderRadius: BorderRadius.all(Radius.circular(12.5)),
//                                      ),
//                                      textColor: Color.fromARGB(255, 255, 255, 255),
//                                      padding: EdgeInsets.all(0),
//                                      child: Text(
//                                        "Follow",
//                                        style: TextStyle(
//                                          fontSize: 14/720 * screenHeight,
//                                          fontFamily: "IBM Plex Sans",
//                                        ),
//                                        textAlign: TextAlign.center,
//                                      ),
//                                    ),
//                                  ),
//                                ),
//                              ],
//                            ),
//                          ),
//                          Container(
//                            height: 40/720 * screenHeight,
//                            margin: EdgeInsets.only(left: 18/360 * screenWidth, top: 30/720 * screenHeight, right: 17/360 * screenWidth),
//                            child: Row(
//                              crossAxisAlignment: CrossAxisAlignment.start,
//                              children: [
//                                Container(
//                                  width: 41/360 * screenWidth,
//                                  height: 41/720 * screenHeight,
//                                  child: Image.asset(
//                                    "assets/images/oval-2.png",
//                                    fit: BoxFit.none,
//                                  ),
//                                ),
//                                Expanded(
//                                  flex: 1,
//                                  child: Container(
//                                    height: 38/720 * screenHeight,
//                                    margin: EdgeInsets.only(left: 10/360 * screenWidth, right: 10/360 * screenWidth),
//                                    child: Column(
//                                      crossAxisAlignment: CrossAxisAlignment.start,
//                                      children: [
//                                        Text(
//                                          "Minnie Anderson",
//                                          style: TextStyle(
//                                            color: NeutralColors.dark_navy_blue,
//                                            fontSize: 14/720 * screenHeight,
//                                            fontFamily: "IBM Plex Sans",
//                                          ),
//                                          textAlign: TextAlign.left,
//                                        ),
//                                        Spacer(),
//                                        Text(
//                                          "National College, Mumbai",
//                                          style: TextStyle(
//                                            color: NeutralColors.blue_grey,
//                                            fontSize: 12/720 * screenHeight,
//                                            fontFamily: "IBM Plex Sans",
//                                          ),
//                                          textAlign: TextAlign.left,
//                                        ),
//                                      ],
//                                    ),
//                                  ),
//                                ),
//                                Align(
//                                  alignment: Alignment.centerLeft,
//                                  child: Container(
//                                    width: 70/360 * screenWidth,
//                                    height: 25/720 * screenHeight,
//                                    child: FlatButton(
//                                      onPressed: () {},
//                                      color: Color.fromARGB(255, 0, 171, 251),
//                                      shape: RoundedRectangleBorder(
//                                        borderRadius: BorderRadius.all(Radius.circular(12.5)),
//                                      ),
//                                      textColor: Color.fromARGB(255, 255, 255, 255),
//                                      padding: EdgeInsets.all(0),
//                                      child: Text(
//                                        "Follow",
//                                        style: TextStyle(
//                                          fontSize: 14/720 * screenHeight,
//                                          fontFamily: "IBM Plex Sans",
//                                        ),
//                                        textAlign: TextAlign.center,
//                                      ),
//                                    ),
//                                  ),
//                                ),
//                              ],
//                            ),
//                          ),
//                          Container(
//                            height: 40/720 * screenHeight,
//                            margin: EdgeInsets.only(left: 18/360 * screenWidth, top: 30/720 * screenHeight, right: 17/360 * screenWidth),
//                            child: Row(
//                              crossAxisAlignment: CrossAxisAlignment.start,
//                              children: [
//                                Container(
//                                  width: 41/360 * screenWidth,
//                                  height: 41/720 * screenHeight,
//                                  child: Image.asset(
//                                    "assets/images/oval-2.png",
//                                    fit: BoxFit.none,
//                                  ),
//                                ),
//                                Expanded(
//                                  flex: 1,
//                                  child: Container(
//                                    height: 38/720 * screenHeight,
//                                    margin: EdgeInsets.only(left: 10/360 * screenWidth, right: 10/360 * screenWidth),
//                                    child: Column(
//                                      crossAxisAlignment: CrossAxisAlignment.start,
//                                      children: [
//                                        Text(
//                                          "Wesley Riley",
//                                          style: TextStyle(
//                                            color: NeutralColors.dark_navy_blue,
//                                            fontSize: 14/720 * screenHeight,
//                                            fontFamily: "IBM Plex Sans",
//                                          ),
//                                          textAlign: TextAlign.left,
//                                        ),
//                                        Spacer(),
//                                        Text(
//                                          "Birla College, Mumbai",
//                                          style: TextStyle(
//                                            color: NeutralColors.blue_grey,
//                                            fontSize: 12/720 * screenHeight,
//                                            fontFamily: "IBM Plex Sans",
//                                          ),
//                                          textAlign: TextAlign.left,
//                                        ),
//                                      ],
//                                    ),
//                                  ),
//                                ),
//                                Align(
//                                  alignment: Alignment.centerLeft,
//                                  child: Container(
//                                    width: 70/360 * screenWidth,
//                                    height: 25/720 * screenHeight,
//                                    child: FlatButton(
//                                      onPressed: () {},
//                                      color: Color.fromARGB(255, 0, 171, 251),
//                                      shape: RoundedRectangleBorder(
//                                        borderRadius: BorderRadius.all(Radius.circular(12.5)),
//                                      ),
//                                      textColor: Color.fromARGB(255, 255, 255, 255),
//                                      padding: EdgeInsets.all(0),
//                                      child: Text(
//                                        "Follow",
//                                        style: TextStyle(
//                                          fontSize: 14/720 * screenHeight,
//                                          fontFamily: "IBM Plex Sans",
//                                        ),
//                                        textAlign: TextAlign.center,
//                                      ),
//                                    ),
//                                  ),
//                                ),
//                              ],
//                            ),
//                          ),
//                          Container(
//                            height: 40/720 * screenHeight,
//                            margin: EdgeInsets.only(left: 18/360 * screenWidth, top: 30/720 * screenHeight, right: 17/360 * screenWidth),
//                            child: Row(
//                              crossAxisAlignment: CrossAxisAlignment.start,
//                              children: [
//                                Container(
//                                  width: 41/360 * screenWidth,
//                                  height: 41/720 * screenHeight,
//                                  child: Image.asset(
//                                    "assets/images/oval-2.png",
//                                    fit: BoxFit.none,
//                                  ),
//                                ),
//                                Expanded(
//                                  flex: 1,
//                                  child: Container(
//                                    height: 38/720 * screenHeight,
//                                    margin: EdgeInsets.only(left: 10/360 * screenWidth, right: 10/360 * screenWidth),
//                                    child: Column(
//                                      crossAxisAlignment: CrossAxisAlignment.start,
//                                      children: [
//                                        Text(
//                                          "Luis Hodges",
//                                          style: TextStyle(
//                                            color: NeutralColors.dark_navy_blue,
//                                            fontSize: 14/720 * screenHeight,
//                                            fontFamily: "IBM Plex Sans",
//                                          ),
//                                          textAlign: TextAlign.left,
//                                        ),
//                                        Spacer(),
//                                        Text(
//                                          "Rachna Sansad, Mumbai",
//                                          style: TextStyle(
//                                            color: NeutralColors.blue_grey,
//                                            fontSize: 12/720 * screenHeight,
//                                            fontFamily: "IBM Plex Sans",
//                                          ),
//                                          textAlign: TextAlign.left,
//                                        ),
//                                      ],
//                                    ),
//                                  ),
//                                ),
//                                Container(
//                                  width: 70/360 * screenWidth,
//                                  height: 25/720 * screenHeight,
//                                  margin: EdgeInsets.only(top: 8/720 * screenHeight),
//                                  child: FlatButton(
//                                    onPressed: () {},
//                                    color: Color.fromARGB(255, 0, 171, 251),
//                                    shape: RoundedRectangleBorder(
//                                      borderRadius: BorderRadius.all(Radius.circular(12.5)),
//                                    ),
//                                    textColor: Color.fromARGB(255, 255, 255, 255),
//                                    padding: EdgeInsets.all(0),
//                                    child: Text(
//                                      "Follow",
//                                      style: TextStyle(
//                                        fontSize: 14/720 * screenHeight,
//                                        fontFamily: "IBM Plex Sans",
//                                      ),
//                                      textAlign: TextAlign.center,
//                                    ),
//                                  ),
//                                ),
//                              ],
//                            ),
//                          ),
                        ],
                      ),
                    ),
//                  Positioned(
//                    left: 23/360 * screenWidth,
//                    top: 467/720 * screenHeight,
//                    right: 22/360 * screenWidth,
//                    child: Container(
//                      height: 80/720 * screenHeight,
//                      child: Column(
//                        crossAxisAlignment: CrossAxisAlignment.stretch,
//                        children: [
//                          GestureDetector(
//                            onTap: (){
//                              AppRoutes.push(context, HomeWidget());
//                            },
//                            child: Container(
//                              height: 40/720 * screenHeight,
//                              decoration: BoxDecoration(
//                                gradient: LinearGradient(
//                                  begin: Alignment(-0.011, 0.494),
//                                  end: Alignment(1.031, 0.516),
//                                  stops: [
//                                    0,
//                                    1,
//                                  ],
//                                  colors: [
//                                    Color.fromARGB(255, 51, 128, 204),
//                                    Color.fromARGB(255, 137, 110, 216),
//                                  ],
//                                ),
//                                borderRadius: BorderRadius.all(Radius.circular(2)),
//                              ),
//                              child: Column(
//                                mainAxisAlignment: MainAxisAlignment.center,
//                                children: [
//                                  Text(
//                                    "DONE",
//                                    style: TextStyle(
//                                      color: Colors.white,
//                                      fontSize: 14/720 * screenHeight,
//                                      fontFamily: "IBM Plex Sans",
//                                      fontWeight: FontWeight.w500,
//                                    ),
//                                    textAlign: TextAlign.center,
//                                  ),
//                                ],
//                              ),
//                            ),
//                          ),
//                          Container(
//                            margin: EdgeInsets.only(left: 96/360 * screenWidth, top: 17/720 * screenHeight, right: 95/360 * screenWidth),
//                            child: Text(
//                              "Skip for now",
//                              style: TextStyle(
//                                color: NeutralColors.blue_grey,
//                                fontSize: 14/720 * screenHeight,
//                                fontFamily: "IBM Plex Sans",
//                              ),
//                              textAlign: TextAlign.center,
//                            ),
//                          ),
//                        ],
//                      ),
//                    ),
//                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
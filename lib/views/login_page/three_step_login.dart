import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:imsindia/components/custom_DropDown.dart';
import 'package:imsindia/components/confirm_details_button.dart';
import 'package:imsindia/components/navigation_bar.dart' as prefix0;
import 'package:imsindia/components/primary_Button.dart';
import 'package:imsindia/components/primary_button_gradient.dart';
import 'package:imsindia/components/primary_button_without_gradient.dart';
import 'package:imsindia/utils/svg_icons.dart';
import 'package:imsindia/utils/text_styles.dart';
import 'package:imsindia/utils/validator.dart';
import 'package:imsindia/views/login_page/data.dart';
import '../../components/Input_forms.dart';
import '../../routers/routes.dart';
import '../../utils/colors.dart';
import 'package:imsindia/resources/strings/login.dart';
import 'package:imsindia/views/login_page/login_create_profile.dart';

import 'login_enrollmentid_widget.dart';

class ThreeStepLogin extends StatefulWidget {
  @override
  _ThreeStepLoginState createState() => _ThreeStepLoginState();
}

final TextEditingController _email = TextEditingController();
final TextEditingController _phone = new TextEditingController();
String get email => _email.text;
String get phoneNo => _phone.text;
bool phoneNoChecked = false;
bool emailID = false;

class _ThreeStepLoginState extends State<ThreeStepLogin> {
  final TextEditingController _courses = new TextEditingController();

  @override
  void initState() {
    super.initState();
    _email.text = '';
    _phone.text = '';
    setState(() {
      _email.text = '';
      _phone.text = '';
      _courses.clear();
      CourseService.getSuggestions();
    });
    _email.addListener(() {
      setState(() {
        validateEmail(email);
      });
    });
    _phone.addListener(() {
      setState(() {
        validateMobileNo(phoneNo);
      });
    });
    _courses.addListener(() {});
  }

  List<String> _colors = <String>[
    'CAT entrance',
    'Bank Exams',
    'IELTS',
  ];
  String _color = 'CAT entrance';
  final focus = FocusNode();

  final _formKey = GlobalKey<FormState>();

  @override
  Future<bool> _onWillPop() {
    AppRoutes.push(context, LoginEnrollmentidWidget());
  }

  static String validateEmail(String value) {
    if (email.isEmpty || email.length == 0) {
      emailID = false;
      return null;
    }

    Pattern pattern =
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';

    RegExp regex = new RegExp(pattern);

    if (!regex.hasMatch(email.trim())) {
      emailID = false;

      return LoginAppLabels.invalidID;
    }
    if (regex.hasMatch(email.trim())) {
      emailID = true;
      return null;
    }
  }

  static String validateMobileNo(String value) {
    if (value.isEmpty) {
      phoneNoChecked = false;
      return null;
    }
    Pattern pattern = r'^[6-9]\d{9}$';
    RegExp regex = new RegExp(pattern);
    if (!regex.hasMatch(value.trim())) {
      phoneNoChecked = false;

      return ThreeStepLoginLables.invalidMobileno;
    }
    if (regex.hasMatch(value.trim())) {
      phoneNoChecked = true;

      return null;
    }
  }

  @override
  Widget build(BuildContext context) {
    final screenHeight = MediaQuery.of(context).size.height;
    final screenWidth = MediaQuery.of(context).size.width;
    final defaultScreenHeight = 720;
    final defaultScreenWidth = 360;

    return WillPopScope(
      onWillPop: _onWillPop,
      child: Scaffold(
        // 
        body: GestureDetector(
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: Container(
            // constraints: BoxConstraints.expand(),
//            decoration: BoxDecoration(
//              color: Color.fromARGB(255, 255, 255, 255),
//            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                Container(
                  height: screenHeight * 0.140,
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      GestureDetector(
                        onTap: () {
                          AppRoutes.push(context, LoginEnrollmentidWidget());
                        },
                        child: Container(
                          child: Padding(
                            padding: const EdgeInsets.all(5.0),
                            child: Align(
                              alignment: Alignment.topLeft,
                              child: Container(
                                width: (20 / 360) * screenWidth,
                                height: (20 / 720) * screenHeight,
                                margin: EdgeInsets.only(
                                    top: screenHeight * 0.0738,
                                    left: screenWidth * 0.062,
                                    right: 5),
                                child: getSvgIcon.backSvgIcon,
                              ),
                            ),
                          ),
                        ),
                      ),
                      Spacer(),
                      Align(
                        alignment: Alignment.topRight,
                        child: Container(
                          //   color: Color.fromRGBO(255, 45, 52, 56),
                          width: screenWidth * 0.780,
                          height: screenHeight * 0.9,
                          child: getSvgIcon.combinedshapeSvgIcon,
                        ),
                      ),
                    ],
                  ),
                ),
                Expanded(
                  child: SingleChildScrollView(
                    child: Container(
                      margin: EdgeInsets.only(
                          left: (45 / defaultScreenWidth) * screenWidth,
                          right: (45 / defaultScreenWidth) * screenWidth),
                      child: Column(
                        children: <Widget>[
                          Container(
                            // height:(60/defaultScreenHeight)*screenHeight,
                            width: (248 / defaultScreenWidth) * screenWidth,
                            margin: EdgeInsets.only(
                                top: (43 / defaultScreenHeight) * screenHeight),
                            child: Text(
                              ThreeStepLoginLables.title,
                              style: TextStyle(
                                color: NeutralColors.charcoal_grey,
                                fontSize:
                                    (21 / defaultScreenWidth) * screenWidth,
                                fontFamily: "IBMPlexSans",
                                fontWeight: FontWeight.w700,
                              ),
                              textAlign: TextAlign.center,
                            ),
                          ),
                          Form(
                            key: _formKey,
                            child: Column(
                              children: <Widget>[
                                Container(
                                  width: double.infinity,
                                  margin: EdgeInsets.only(
                                      top: (40 / defaultScreenHeight) *
                                          screenHeight),
                                  child: TextInputFormField(
                                    controller: _email,
                                    keyboardType: TextInputType.emailAddress,
                                    lableText: ThreeStepLoginLables.emailId,
                                    textInputAction: TextInputAction.next,
                                    border: email.isEmpty
                                        ? UnderlineInputBorder(
                                            borderSide: BorderSide(
                                                color: SemanticColors.blueGrey
                                                    .withOpacity(0.3),
                                                width: 1))
                                        : UnderlineInputBorder(
                                            borderSide: BorderSide(
                                                color:
                                                    SemanticColors.dark_purpely,
                                                width: 1)),
                                    autovalidate: true,
                                    validator: validateEmail,
                                    onFieldSubmitted: (val) {
                                      FocusScope.of(context)
                                          .requestFocus(focus);
                                    },
                                    fontStyle: TextStyles.editTextStyle,
                                    lableStyle: TextStyles.labelStyle,
                                  ),
                                ),
                                Container(
                                  width: double.infinity,
                                  margin: EdgeInsets.only(
                                      top: (19.5 / defaultScreenHeight) *
                                          screenHeight),
                                  child: Center(
                                    child: TextInputFormField(
                                      controller: _phone,
                                      focusNode: focus,
                                      keyboardType: TextInputType.number,
                                      lableText: ThreeStepLoginLables.mobileNo,
                                      border: _phone.text.isEmpty
                                          ? UnderlineInputBorder(
                                              borderSide: BorderSide(
                                                  color: SemanticColors.blueGrey
                                                      .withOpacity(0.3),
                                                  width: 1))
                                          : UnderlineInputBorder(
                                              borderSide: BorderSide(
                                                  color: SemanticColors
                                                      .dark_purpely,
                                                  width: 1)),
                                      textInputAction: TextInputAction.done,
                                      autovalidate: true,
                                      prefixText: '+91',
                                      maxLength: 10,
                                      validator: validateMobileNo,
                                      fontStyle: TextStyles.editTextStyle,
                                      lableStyle: TextStyles.labelStyle,
                                      prefixStyle: TextStyles.prefixStyle,
                                      //  onSaved: (value) => _mobileNo = value,
                                      //onFieldSubmitted: (_) => FocusScope.of(context).requestFocus(),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Container(
                            // height: (61/720)*screenHeight,
                            //    width: double.infinity,
                            margin: EdgeInsets.only(
                                top: (19.5 / defaultScreenHeight) *
                                    screenHeight),
                            child: DropDownFormField(
                              getImmediateSuggestions: true,
                              textFieldConfiguration: TextFieldConfiguration(
                                label: "Product you are interested in",
                                controller: this._courses,
                              ),
                              suggestionsBoxDecoration:
                                  SuggestionsBoxDecoration(
                                      dropDownHeight: 150,
                                      borderRadius:
                                          new BorderRadius.circular(5.0),
                                      color: Colors.white),
                              suggestionsCallback: (pattern) {
                                return CourseService.getSuggestions();
                              },
                              itemBuilder: (context, suggestion) {
                                return ListTile(
                                  dense: true,
                                  contentPadding: EdgeInsets.only(left: 10),
                                  title: Text(
                                    suggestion,
                                    style: TextStyle(
                                      fontFamily: "IBMPlexSans",
                                      fontWeight: FontWeight.w400,
                                      fontSize: (13 / 720) * screenHeight,
                                      color: NeutralColors.bluey_grey,
                                      textBaseline: TextBaseline.alphabetic,
                                      letterSpacing: 0.0,
                                      inherit: false,
                                    ),
                                  ),
                                );
                              },
                              transitionBuilder:
                                  (context, suggestionsBox, controller) {
                                return suggestionsBox;
                              },
                              onSuggestionSelected: (suggestion) {
                                this._courses.text = suggestion;
                                setState(() {});

                                // this._typeAheadController.dispose();
                              },
                              onSaved: (value) => {},
                            ),
//                               child: new FormField<String>(
// //                      validator: (String value){
// //                        if(value=='select the product'){
// //                          return 'please select the valid product';
// //                        }
// //                        return '';
// //                      },
//                                 builder: (FormFieldState<String> state) {
//                                   return InputDecorator(
//                                     decoration: InputDecoration(
//                                       labelText: ThreeStepLoginLables.product,
//                                       labelStyle: TextStyle(
//                                         color: SemanticColors.blueGrey,
//                                         fontSize:
//                                         (12 / defaultScreenWidth) * screenWidth,
//                                         fontFamily: "IBMPlexSans",
//                                         fontWeight: FontWeight.w400,
//                                       ),
//                                     ),
//                                     isEmpty: _color == 'sfsgf',
//                                     child: new DropdownButtonHideUnderline(
//                                       child: new DropdownButton<String>(
//                                         value: _color,
//                                         isDense: true,
//                                         isExpanded: false,
//                                         iconEnabledColor: SemanticColors.dusky_blue,
//                                         style: TextStyle(
//                                           color: NeutralColors.dark_navy_blue,
//                                           fontSize:
//                                           (14 / defaultScreenWidth) * screenWidth,
//                                           fontFamily: "IBMPlexSans",
//                                           fontWeight: FontWeight.w400,
//                                         ),
//                                         //iconSize: ,
//                                         icon: const Icon(Icons.keyboard_arrow_down),
//                                         onChanged: (String newValue) {
//                                           setState(() {
//                                             _color = newValue;
//                                             //  state.didChange(newValue);
//                                           });
//                                         },
//                                         items: _colors.map((String value) {
//                                           return new DropdownMenuItem<String>(
//                                             value: value,
//                                             child: new Text(value),
//                                           );
//                                         }).toList(),
//                                       ),
//                                     ),
//                                   );
//                                 },
//                               ),
                          ),
                          Container(
                            //  height: (40/defaultScreenHeight)*screenHeight,
                            width: (270 / defaultScreenWidth) * screenWidth,
                            margin: EdgeInsets.only(
                                top: (39.5 / defaultScreenHeight) *
                                    screenHeight),
                            child: PrimaryButton(
                              gradient: (emailID == true &&
                                      phoneNoChecked == true &&
                                      this._courses.text.isNotEmpty)
                                  ? true
                                  : false,
                              onTap: () {
                                if (emailID == true &&
                                    phoneNoChecked == true &&
                                    this._courses.text.isNotEmpty) {
                                  AppRoutes.push(context, LoginCreateProfile());
                                }
                              },
                              text: LoginAppStrings.getLoginPassword
                                  .toUpperCase(),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

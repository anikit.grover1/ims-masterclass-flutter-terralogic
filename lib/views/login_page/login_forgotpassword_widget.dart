import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:imsindia/api/bloc/services.dart';
import 'package:imsindia/components/Input_forms.dart';
import 'package:imsindia/components/primary_Button.dart';
import 'package:imsindia/resources/strings/login.dart';
import 'package:imsindia/routers/routes.dart';
import 'package:imsindia/utils/Url.dart';
import 'package:imsindia/utils/colors.dart';
import 'package:imsindia/utils/global.dart' as globals;
import 'package:imsindia/utils/svg_icons.dart';
import 'package:imsindia/utils/text_styles.dart';
import 'package:imsindia/views/login_page/login_resentlinksent_widget.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'login_screen.dart';

var studentName;
var studentEmail;
var url;

class LoginForgotPasswordWidget extends StatefulWidget {
  @override
  LoginForgotPasswordScreenState createState() =>
      LoginForgotPasswordScreenState();
}

final TextEditingController _recoveryEmail = new TextEditingController();
final TextEditingController _imsPin = new TextEditingController();
String get recoveryEmail => _recoveryEmail.text;
String get imsPin => _imsPin.text;
bool recoveryEmailID = false;
bool imsPinNumber = false;

class LoginForgotPasswordScreenState extends State<LoginForgotPasswordWidget> {
  final GlobalKey<ScaffoldState> _scaffoldstate =
      new GlobalKey<ScaffoldState>();

  void showErrorMessage(String value) {
    _scaffoldstate.currentState.showSnackBar(new SnackBar(
      content: new Text(value),
      duration: new Duration(seconds: 3),
    ));
  }

  //void onBackPressed(BuildContext context) => Navigator.push(context, MaterialPageRoute(builder: (context) => LoginFillpasswordWidget()));
  FocusNode _emailFocusNode = new FocusNode();
  FocusNode _imsPinFocusNode = new FocusNode();
  void setResentLinkDetails() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('resentLinkEmail', studentEmail);
    prefs.setString('resentLinkName', studentName[0]);
    prefs.setString('resentLinkURL', url);
  }

  void setResentLinkDetailsToEmpty() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('resentLinkEmail', null);
    prefs.setString('resentLinkName', null);
    prefs.setString('resentLinkURL', null);
  }

  void checkForgotPassword() {
    Map postdata = {};
    if (_recoveryEmail.text != '') {
      postdata = {"txtemail": recoveryEmail};
    } else {
      postdata = {"txtpin": imsPin};
    }
    ApiService().Details_Using_Email_Or_Pin(postdata).then((result) {
      setState(() {
        if (result[0] == 'success') {
          studentName = result[1]['studentdetails']['Name'].split(" ");
          studentEmail = result[1]['studentdetails']['Email'];
          url = URL.ResetLinkEmailBase64 +
              base64.encode(utf8.encode(studentEmail));
          setResentLinkDetails();
          Map postdata = {
            "companyCode": "ims",
            "studentName": studentName[0],
            "emailId": studentEmail,
            "url": url
          };
          ApiService()
              .postAPI(URL.ResetPasswordLink, postdata, globals.headers)
              .then((result) {
            setState(() {
              if (result[0] == 'Email Sent Successfully') {
                AppRoutes.push(
                    context,
                    LoginResentlinksentWidget(
                      emailToDispaly: studentEmail,
                    ));
              } else {
                showErrorMessage(result[0]);
              }
            });
          });
        } else {
          showErrorMessage(result[0]);
        }
      });
    });
  }

  @override
  void initState() {
    super.initState();
    // _emailFocusNode = new FocusNode();
    setResentLinkDetailsToEmpty();
    setState(() {
      _recoveryEmail.text = '';
      _imsPin.text = '';
    });
    _recoveryEmail.addListener(() {
      setState(() {
        validateRecoveryEmail(recoveryEmail);
      });
    });
    _imsPin.addListener(() {
      setState(() {
        validateImsPin(imsPin);
      });
    });
    _imsPinFocusNode.addListener(() {
      if (_imsPinFocusNode.hasFocus) {
        _recoveryEmail.text = '';
        validateRecoveryEmail(_recoveryEmail.text);
      }
    });
    _emailFocusNode.addListener(() {
      if (_emailFocusNode.hasFocus) {
        _imsPin.clear();
        validateImsPin(_imsPin.text);
      }
    });
  }

  @override
  void dispose() {
    //_emailFocusNode.dispose();
    super.dispose();
  }

  static String validateRecoveryEmail(String value) {
    if (recoveryEmail.isEmpty || recoveryEmail.length == 0) {
      recoveryEmailID = false;
      return null;
    }

    Pattern pattern =
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';

    RegExp regex = new RegExp(pattern);

    if (!regex.hasMatch(recoveryEmail.trim())) {
      recoveryEmailID = false;

      return LoginAppLabels.invalidID;
    }
    if (regex.hasMatch(recoveryEmail.trim())) {
      recoveryEmailID = true;

      return null;
    }
  }

  static String validateImsPin(String value) {
    if (imsPin.isEmpty && imsPin.length == 0) {
      imsPinNumber = false;

      return null;
    } else if (imsPin.length < 13) {
      imsPinNumber = false;
      return LoginUsingIMSPin.invalidIMSPasswordForLogin;
    } else {
      imsPinNumber = true;

      return null;
    }
  }

  @override
  Widget build(BuildContext context) {
    final screenHeight = MediaQuery.of(context).size.height;
    final screenWidth = MediaQuery.of(context).size.width;
    //SystemChrome.setEnabledSystemUIOverlays([SystemUiOverlay.bottom]);
    Future<bool> _onWillPop() {
      AppRoutes.push(context, LoginFillpasswordWidget());
    }

    return WillPopScope(
      onWillPop: _onWillPop,
      child: Scaffold(
        key: _scaffoldstate,

        // 
        body: GestureDetector(
          onTap: () {
            _imsPinFocusNode.unfocus();
            _emailFocusNode.unfocus();
          },
          child: Container(
            // constraints: BoxConstraints.expand(),
//            decoration: BoxDecoration(
//              color: Color.fromARGB(255, 255, 255, 255),
//            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                Container(
                  height: screenHeight * 0.140,
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      GestureDetector(
                        onTap: () {
                          AppRoutes.push(context, LoginFillpasswordWidget());
                        },
                        child: Container(
                          child: Padding(
                            padding: const EdgeInsets.all(5.0),
                            child: Align(
                              alignment: Alignment.topLeft,
                              child: Container(
                                width: (20 / 360) * screenWidth,
                                height: (20 / 720) * screenHeight,
                                margin: EdgeInsets.only(
                                    top: screenHeight * 0.0738,
                                    left: screenWidth * 0.062,
                                    right: 5),
                                child: getSvgIcon.backSvgIcon,
                              ),
                            ),
                          ),
                        ),
                      ),
                      Spacer(),
                      Align(
                        alignment: Alignment.topRight,
                        child: Container(
                          //   color: Color.fromRGBO(255, 45, 52, 56),

                          width: screenWidth * 0.780,
                          //  height: screenHeight*0.9,
                          child: getSvgIcon.combinedshapeSvgIcon,
                        ),
                      ),
                    ],
                  ),
                ),
                Expanded(
                  child: SingleChildScrollView(
                    child: Container(
                      child: Column(
                        children: <Widget>[
                          Container(
                            margin: EdgeInsets.only(top: screenHeight * 0.079),
                            child: Text(
                              "Forgot Password?",
                              style: TextStyle(
                                color: NeutralColors.charcoal_grey,
                                fontSize: 21 / 720 * screenHeight,
                                fontFamily: "IBMPlexSans",
                                fontWeight: FontWeight.bold,
                              ),
                              textAlign: TextAlign.center,
                            ),
                          ),
                          Container(
                            // height: 66/720 * screenHeight,
                            // width: (screenWidth*0.75  ),
                            margin: EdgeInsets.only(
                                left: screenWidth * 0.125,
                                top: screenHeight * 0.009,
                                right: screenWidth * 0.125),
                            child: Text(
                              "Enter your email ID or IMS PIN registered with your myIMS account. A reset link will be sent to your email ID ",
                              style: TextStyle(
                                  color: Color.fromARGB(255, 126, 145, 154),
                                  fontSize: 14 / 720 * screenHeight,
                                  fontFamily: "IBMPlexSans",
                                  fontWeight: FontWeight.normal),
                              textAlign: TextAlign.center,
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.only(
                                left: screenWidth * 0.125,
                                top: screenHeight * 0.040,
                                right: screenWidth * 0.125),
                            child: TextInputFormField(
                              focusNode: _emailFocusNode,
                              controller: _recoveryEmail,
                              keyboardType: TextInputType.emailAddress,
                              enabled: _imsPinFocusNode.hasFocus ? false : true,
                              lableText: LoginAppLabels.email,
                              lableStyle: TextStyles.labelStyle,
                              fontStyle: TextStyles.editTextStyle,
                              textInputAction: TextInputAction.done,
                              border: recoveryEmail.isEmpty
                                  ? UnderlineInputBorder(
                                      borderSide: BorderSide(
                                          color: SemanticColors.blueGrey
                                              .withOpacity(0.3),
                                          width: 1))
                                  : UnderlineInputBorder(
                                      borderSide: BorderSide(
                                          color: SemanticColors.dark_purpely,
                                          width: 1)),
                              autovalidate: true,
                              validator: validateRecoveryEmail,
                              onSaved: (value) => {},
                              onFieldSubmitted: (_) =>
                                  FocusScope.of(context).requestFocus(),
                            ),
                          ),
                          Container(
                            child: Align(
                              alignment: Alignment.topCenter,
                              child: Container(
                                width: screenWidth * 0.338,
                                height: screenHeight * 0.025,
                                margin:
                                    EdgeInsets.only(top: screenHeight * 0.0555),
                                child: Stack(
                                  alignment: Alignment.center,
                                  children: [
                                    Positioned(
                                      child: Text(
                                        "or",
                                        style: TextStyle(
                                          color: NeutralColors.dark_navy_blue,
                                          fontSize: 14 / 720 * screenHeight,
                                          fontFamily: "IBMPlexSans",
                                        ),
                                        textAlign: TextAlign.left,
                                      ),
                                    ),
                                    Positioned(
                                      left: 0,
                                      right: 0,
                                      child: Container(
                                        height: screenHeight * 0.0013,
                                        child: Row(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.stretch,
                                          children: [
                                            Align(
                                              alignment: Alignment.centerLeft,
                                              child: Container(
                                                width: screenWidth * 0.138,
                                                height: screenHeight * 0.0013,
                                                decoration: BoxDecoration(
                                                  color:
                                                      SemanticColors.blueGrey,
                                                ),
                                                child: Container(),
                                              ),
                                            ),
                                            Spacer(),
                                            Align(
                                              alignment: Alignment.centerLeft,
                                              child: Container(
                                                width: screenWidth * 0.138,
                                                height: screenHeight * 0.0013,
                                                decoration: BoxDecoration(
                                                  color:
                                                      SemanticColors.blueGrey,
                                                ),
                                                child: Container(),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.only(
                                left: screenWidth * 0.125,
                                top: screenHeight * 0.040,
                                right: screenWidth * 0.125),
                            child: TextInputFormField(
                              focusNode: _imsPinFocusNode,
                              controller: _imsPin,
                              keyboardType: TextInputType.text,
                              lableText: LoginAppLabels.imsPin,
                              textInputAction: TextInputAction.done,
                              lableStyle: TextStyles.labelStyle,
                              fontStyle: TextStyles.editTextStyle,
                              enabled: _emailFocusNode.hasFocus ? false : true,
                              border: imsPin.isEmpty
                                  ? UnderlineInputBorder(
                                      borderSide: BorderSide(
                                          color: SemanticColors.blueGrey
                                              .withOpacity(0.3),
                                          width: 1))
                                  : UnderlineInputBorder(
                                      borderSide: BorderSide(
                                          color: SemanticColors.dark_purpely,
                                          width: 1)),
                              autovalidate: true,
                              obscureText: false,
                              validator: validateImsPin,
                              onSaved: (value) => {},
                              onFieldSubmitted: (_) =>
                                  FocusScope.of(context).requestFocus(),
                            ),
                          ),
                          Container(
                              height: screenHeight * 0.0555,
                              margin: EdgeInsets.only(
                                  left: screenWidth * 0.125,
                                  top: screenHeight * 0.054,
                                  right: screenWidth * 0.125),
                              child: PrimaryButton(
                                gradient: (recoveryEmailID == true ||
                                        imsPinNumber == true)
                                    ? true
                                    : false,
                                onTap: () {
                                  if (recoveryEmailID == true ||
                                      imsPinNumber == true) {
                                    checkForgotPassword();
                                  }
                                },
                                text:
                                    LoginAppStrings.sendResetLink.toUpperCase(),
                              ) // Group 4
                              ),
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

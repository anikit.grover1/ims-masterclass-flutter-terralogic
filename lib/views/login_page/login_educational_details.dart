import 'package:flutter/material.dart';
//import 'package:flutter_typeahead/flutter_typeahead.dart';
import 'package:imsindia/components/Input_forms.dart';
import 'package:imsindia/components/custom_expansion_panel.dart';
import 'package:imsindia/components/primary_Button.dart';
import 'package:imsindia/resources/strings/login.dart';
import 'package:imsindia/utils/colors.dart';
import 'package:imsindia/utils/text_styles.dart';
import 'package:imsindia/utils/validator.dart';
import 'package:imsindia/views/login_page/data.dart';
import 'package:imsindia/views/login_page/login_enrollmentid_widget.dart';
import 'package:imsindia/views/login_page/login_graduation_details.dart';
import 'package:imsindia/views/login_page/login_postgraduation_details.dart';
import 'package:imsindia/views/login_page/login_professional_degree_details.dart';
import 'package:imsindia/views/login_page/login_tenth_details.dart';
import 'package:imsindia/views/login_page/login_twelth_class_details.dart';
import 'package:imsindia/views/login_page/login_work_experience.dart';
import 'package:imsindia/views/login_page/sample2.dart' as prefix0;
import '../../routers/routes.dart';
//final keyC = new GlobalKey<LoginEducationWidgetState>();

var screenWidthTotal;
var screenHeightTotal;
var _safeAreaHorizontal;
var _safeAreaVertical;
var screenHeight;
var screenWidth;
var activeMeterIndex=null;


class LoginEducationDetailsWidget extends StatefulWidget {
     // LoginEducationDetailsWidget({Key key,}) : super(key: key);

  @override
  LoginEducationWidgetState createState() => LoginEducationWidgetState();
}

class LoginEducationWidgetState extends State<LoginEducationDetailsWidget> {
      final TextEditingController _stateInTenthDetails =      new TextEditingController();
       final TextEditingController _gradeInTenthDetails =      new TextEditingController();
  String indexValue = "Hello World";
    String activeIndex = "Hello World";

 String myValues = "Hello World";

  String get myValue => myValues;

  set myValue(String value) {
    myValues = value;
  }

 // String get activeMeterIndex => indexValue;



  List titles=[
  0,1,2,3,4
];
  String get exported => 'hiiiiii';

  @override
  void initState() {
    print('education page'+myValue);
    super.initState();
  }

  @override
  void didChangeDependencies() {
    setState(() {
//      pro._degreeInProfessionalDetails.text;
//      print('pro'+pro._degree.toString());
//      professionalDetails._degreeInProfessionalDetails.text;
//      print('change'+professionalDetails._degreeInProfessionalDetails.text);
    });
  }

  @override
  Widget build(BuildContext context) {

    CalculateScreen(context);
    Future<bool> _onWillPop() {
      AppRoutes.push(context, LoginEnrollmentidWidget());
    }

    return WillPopScope(
      onWillPop: _onWillPop,
      child: new Scaffold(
        body: SingleChildScrollView(
          padding: EdgeInsets.only(bottom: 40),
          child: Container(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                Container(
                  height: (82 / 720) * screenHeight,
                  margin: EdgeInsets.only(left: 8),
                  child: Row(
                    children: [
                      Align(
                        alignment: Alignment.topLeft,
                        child: Container(
                          width: (19 / 360) * screenWidth,
                          height: (14 / 720) * screenHeight,
                          margin: EdgeInsets.only(
                              left: 15, top: (65 / 720) * screenHeight),
                          child: FlatButton(
                            onPressed: () {
                              AppRoutes.push(
                                  context, LoginEnrollmentidWidget());
                            },
                            color: Colors.transparent,
                            textColor: Color.fromARGB(255, 0, 0, 0),
                            padding: EdgeInsets.all(0),
                            child: Image.asset("assets/images/back-2.png",
                                color: Colors.black),
                          ),
                        ),
                      ),
                      Spacer(),
                    ],
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(
                      top: (46 / 720) * screenHeight, left: 45, right: 45),
                  child: FittedBox(
                    fit: BoxFit.fitWidth,
                    child: Text(
                      "Fill your educational details",
                      style: TextStyle(
                        color: Color.fromARGB(255, 45, 52, 56),
                        fontSize: 21,
                        fontFamily: "IBM Plex Sans",
                        fontWeight: FontWeight.w700,
                      ),
                      textAlign: TextAlign.center,
                    ),
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(
                      top: (40 / 720) * screenHeight, left: 45.0, right: 45.0),
                  child: CustomExpansionPanelList(
                    expansionHeaderHeight: (45.0 / 720) * screenHeight,
                    iconColor:
                        (activeMeterIndex == 0)
                            ? Colors.white
                            : Colors.black,
                    backgroundColor1:
                        (activeMeterIndex == 0)
                            ? const Color(0xFF6979f8)
                            : (_gradeInTenthDetails.text != '' &&
                                    _stateInTenthDetails.text != '')
                                ? NeutralColors.mango.withOpacity(0.05)
                                : const Color(0xFF3380cc).withOpacity(0.10),
                    backgroundColor2:
                        (activeMeterIndex == 0)
                            ? const Color(0xFF6979f8)
                            : (_gradeInTenthDetails.text != '' &&
                                    _stateInTenthDetails.text != '')
                                ? NeutralColors.mango.withOpacity(0.05)
                                : const Color(0xFF886dd7).withOpacity(0.10),
                    expansionCallback: (int index, bool status) {
                      setState(() {
                        print('print'+activeMeterIndex.toString());
                        activeMeterIndex =
                            activeMeterIndex == 0
                                ? null
                                : 0;

                      });
                    },
                    children: [
                      new ExpansionPanel(
                        canTapOnHeader: true,
                        isExpanded:
                            activeMeterIndex == 0,
                        headerBuilder:
                            (BuildContext context, bool isExpanded) =>
                                new Container(
                          alignment: Alignment.centerLeft,
                          child: new Padding(
                            padding:
                                EdgeInsets.only(left: (15 / 320) * screenWidth),
                            child: new Text(titles[0],
                                style: new TextStyle(
                                    fontSize: (12.5 / 720) * screenHeight,
                                    fontFamily: "IBMPlexSans",
                                    fontWeight: FontWeight.w500,
                                    color: (ExpansionPanelIndexVariable
                                                .activeMeterIndex ==
                                            titles[0])
                                        ? Colors.white
                                        : const Color(0xFF2d3438))),
                          ),
                        ),
                        body: TenthClassDetails(),
                      ),
                    ],
                  ),
                ),
              //  TenthClassDetails(),
              //  TwelthClassDetails(),
              //  GraduationDetails(),
              //  PostGraduationDetails(),
              //  ProfessionalDegreeDetails(),
                Padding(
                  padding: EdgeInsets.only(
                      left: screenWidth * 45 / 360,
                      top: screenHeight * 37.5 / 720,
                      right: screenWidth * 45 / 360),
                  child: Center(
                      child: PrimaryButton(
                    onTap: () {
                      if (FieldValidator.buttonEnable == true) {
                        AppRoutes.push(context, LoginWorkexperience());
                      } else {}
                    },
                    text: LoginAppStrings.confirmDetails,
                  )),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
void CalculateScreen(BuildContext context) {
  var _mediaQueryData = MediaQuery.of(context);
  screenWidthTotal = _mediaQueryData.size.width;
  screenHeightTotal = _mediaQueryData.size.height;
  _safeAreaHorizontal =
      _mediaQueryData.padding.left + _mediaQueryData.padding.right;
  _safeAreaVertical =
      _mediaQueryData.padding.top + _mediaQueryData.padding.bottom;

  screenHeight = screenHeightTotal - _safeAreaVertical;
  screenWidth = screenWidthTotal - _safeAreaHorizontal;
}

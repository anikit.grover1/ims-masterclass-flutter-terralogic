import 'package:flutter/material.dart';
import 'package:imsindia/components/custom_expansion_panel.dart';
import 'package:imsindia/components/primary_Button.dart';
import 'package:imsindia/resources/strings/login.dart';
import 'package:imsindia/utils/colors.dart';
import 'package:imsindia/views/login_page/login_enrollmentid_widget.dart';
import 'package:imsindia/views/login_page/login_graduation_details.dart';
import 'package:imsindia/views/login_page/login_postgraduation_details.dart';
import 'package:imsindia/views/login_page/login_professional_degree_details.dart';
import 'package:imsindia/views/login_page/login_tenth_details.dart';
import 'package:imsindia/views/login_page/login_twelth_class_details.dart';
import 'package:imsindia/views/login_page/login_work_experience.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../../routers/routes.dart';
import 'package:imsindia/utils/svg_icons.dart';

var screenWidthTotal;
var screenHeightTotal;

var _safeAreaHorizontal;

var _safeAreaVertical;

var screenHeight;

var screenWidth;
var activeMeterIndex = null;

List titles = [
  '10th Grade',
  '12th Grade',
  'Graduation',
  'Post Graduation',
  'Professional Degree',
];

class LoginEducationWidget extends StatefulWidget {
  LoginEducationWidget({Key key}) : super(key: key);

  @override
  LoginEducationWidgetState createState() => LoginEducationWidgetState();
}

class LoginEducationWidgetState extends State<LoginEducationWidget> {
  bool tenthDetails = false;
  bool twelthDetails = false;
  bool graduationDetails = false;
  bool postgraduationDetails = false;
  bool professionalDegreeDetails = false;

  // bool get checkTenth => tenthDetails;

  getTenthDetailsFilled() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    tenthDetails = prefs.getBool("tenthDetailsFilled");
  }

  getTwelthDetailsFilled() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    twelthDetails = prefs.getBool("twelthDetailsFilled");
  }

  getGraduationDetailsFilled() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    graduationDetails = prefs.getBool("graduationDetailsFilled");
  }

  getPostGraduationDetailsFilled() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    postgraduationDetails = prefs.getBool("postgraduationDetailsFilled");
  }

  getProfessionalDegreeDetailsFilled() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    professionalDegreeDetails = prefs.getBool("professionalDetailsFilled");
  }
    setAllFilledFalse() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setBool("tenthDetailsFilled", false);
        prefs.setBool("twelthDetailsFilled", false);
    prefs.setBool("graduationDetailsFilled", false);
    prefs.setBool("postgraduationDetailsFilled", false);
    prefs.setBool("professionalDetailsFilled", false);

  }

  @override
  void initState() {
    setAllFilledFalse();
    setState(() {});

    super.initState();
  }

  @override
  void didChangeDependencies() {
    setState(() {
//      pro._degreeInProfessionalDetails.text;
//      print('pro'+pro._degree.toString());
//      professionalDetails._degreeInProfessionalDetails.text;
//      print('change'+professionalDetails._degreeInProfessionalDetails.text);
    });
  }

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    var _mediaQueryData = MediaQuery.of(context);
    screenWidthTotal = _mediaQueryData.size.width;
    screenHeightTotal = _mediaQueryData.size.height;
    _safeAreaHorizontal =
        _mediaQueryData.padding.left + _mediaQueryData.padding.right;
    _safeAreaVertical =
        _mediaQueryData.padding.top + _mediaQueryData.padding.bottom;

    screenHeight = screenHeightTotal - _safeAreaVertical;
    screenWidth = screenWidthTotal - _safeAreaHorizontal;

    Future<bool> _onWillPop() {
      AppRoutes.push(context, LoginEnrollmentidWidget());
    }

    return WillPopScope(
      onWillPop: _onWillPop,
      child: new Scaffold(
        body: SingleChildScrollView(
          padding: EdgeInsets.only(bottom: 40),
          child: Container(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                Container(
                  height: (82 / 720) * screenHeight,
                  margin: EdgeInsets.only(left: 8),
                  child: Row(
                    children: [
                      GestureDetector(
                        onTap: () {
                          AppRoutes.push(context, LoginEnrollmentidWidget());
                        },
                        child: Container(
                          child: Padding(
                            padding: const EdgeInsets.all(5.0),
                            child: Align(
                              alignment: Alignment.topLeft,
                              child: Container(
                                width: (20 / 360) * screenWidth,
                                height: (20 / 720) * screenHeight,
                                margin: EdgeInsets.only(
                                    top: screenHeight * 0.0738,
                                    left: 8,
                                    right: 5),
                                child: getSvgIcon.backSvgIcon,
                              ),
                            ),
                          ),
                        ),
                      ),
                      Spacer(),
                    ],
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(
                      top: (46 / 720) * screenHeight, left: 45, right: 45),
                  child: FittedBox(
                    fit: BoxFit.fitWidth,
                    child: Text(
                      "Fill your educational details",
                      style: TextStyle(
                        color: Color.fromARGB(255, 45, 52, 56),
                        fontSize: 21,
                        fontFamily: "IBM Plex Sans",
                        fontWeight: FontWeight.w700,
                      ),
                      textAlign: TextAlign.center,
                    ),
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(
                      top: (40 / 720) * screenHeight, left: 45.0, right: 45.0),
                  child: CustomExpansionPanelList(
                    expansionHeaderHeight: (45.0 / 720) * screenHeight,
                    iconColor: (activeMeterIndex == 0)
                        ? Colors.white
                        : (tenthDetails == true)
                            ? NeutralColors.mango
                            : Colors.black,
                    backgroundColor1: (activeMeterIndex == 0)
                        ? const Color(0xFF6979f8)
                        : (tenthDetails == true)
                            ? NeutralColors.mango.withOpacity(0.05)
                            : const Color(0xFF3380cc).withOpacity(0.10),
                    backgroundColor2: (activeMeterIndex == 0)
                        ? const Color(0xFF6979f8)
                        : (tenthDetails == true)
                            ? NeutralColors.mango.withOpacity(0.05)
                            : const Color(0xFF886dd7).withOpacity(0.10),
                    expansionCallback: (int index, bool status) {
                      setState(() {
                        getTenthDetailsFilled();
                        getTwelthDetailsFilled();
                        getGraduationDetailsFilled();
                        getPostGraduationDetailsFilled();
                        getProfessionalDegreeDetailsFilled();
                        activeMeterIndex = activeMeterIndex == 0 ? null : 0;
                      });
                    },
                    children: [
                      new ExpansionPanel(
                        canTapOnHeader: true,
                        isExpanded: activeMeterIndex == 0,
                        headerBuilder:
                            (BuildContext context, bool isExpanded) =>
                                new Container(
                          alignment: Alignment.centerLeft,
                          child: new Padding(
                            padding:
                                EdgeInsets.only(left: (15 / 320) * screenWidth),
                            child: new Text(titles[0],
                                style: new TextStyle(
                                    fontSize: (12.5 / 720) * screenHeight,
                                    fontFamily: "IBMPlexSans",
                                    fontWeight: FontWeight.w500,
                                    color: (activeMeterIndex == 0)
                                        ? Colors.white
                                        : const Color(0xFF2d3438))),
                          ),
                        ),
                        body: TenthClassDetails(),
                      ),
                    ],
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(top: 10.0, left: 45.0, right: 45.0),
                  child: CustomExpansionPanelList(
                    expansionHeaderHeight: 45.0,
                    iconColor: (activeMeterIndex == 1)
                        ? Colors.white
                        : (twelthDetails == true)
                            ? NeutralColors.mango
                            : Colors.black,
                    backgroundColor1: (activeMeterIndex == 1)
                        ? const Color(0xFF6979f8)
                        : (twelthDetails == true)
                            ? NeutralColors.mango.withOpacity(0.05)
                            : const Color(0xFF3380cc).withOpacity(0.10),
                    backgroundColor2: (activeMeterIndex == 1)
                        ? const Color(0xFF6979f8)
                        : (twelthDetails == true)
                            ? NeutralColors.mango.withOpacity(0.05)
                            : const Color(0xFF886dd7).withOpacity(0.10),
                    expansionCallback: (int index, bool status) {
                      setState(() {
                        getTenthDetailsFilled();
                        getTwelthDetailsFilled();
                        getGraduationDetailsFilled();
                        getPostGraduationDetailsFilled();
                        getProfessionalDegreeDetailsFilled();
                        activeMeterIndex = activeMeterIndex == 1 ? null : 1;
                      });
                    },
                    children: [
                      new ExpansionPanel(
                          canTapOnHeader: true,
                          isExpanded: activeMeterIndex == 1,
                          headerBuilder: (BuildContext context,
                                  bool isExpanded) =>
                              new Container(
                                alignment: Alignment.centerLeft,
                                child: new Padding(
                                  padding: EdgeInsets.only(
                                      left: (15 / 320) * screenWidth),
                                  child: new Text(titles[1],
                                      style: new TextStyle(
                                          fontSize: (12.5 / 720) * screenHeight,
                                          fontFamily: "IBMPlexSans",
                                          fontWeight: FontWeight.w500,
                                          color: (activeMeterIndex == 1)
                                              ? Colors.white
                                              : const Color(0xFF2d3438))),
                                ),
                              ),
                          body: TwelthClassDetails()),
                    ],
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(top: 10.0, left: 45.0, right: 45.0),
                  child: CustomExpansionPanelList(
                    expansionHeaderHeight: 45.0,
                    iconColor: (activeMeterIndex == 2)
                        ? Colors.white
                        : (graduationDetails == true)
                            ? NeutralColors.mango
                            : Colors.black,
                    backgroundColor1: (activeMeterIndex == 2)
                        ? const Color(0xFF6979f8)
                        : (graduationDetails == true)
                            ? NeutralColors.mango.withOpacity(0.05)
                            : const Color(0xFF3380cc).withOpacity(0.10),
                    backgroundColor2: (activeMeterIndex == 2)
                        ? const Color(0xFF6979f8)
                        : (graduationDetails == true)
                            ? NeutralColors.mango.withOpacity(0.05)
                            : const Color(0xFF886dd7).withOpacity(0.10),
                    expansionCallback: (int index, bool status) {
                      setState(() {
                         getTenthDetailsFilled();
                        getTwelthDetailsFilled();
                        getGraduationDetailsFilled();
                        getPostGraduationDetailsFilled();
                        getProfessionalDegreeDetailsFilled();
                        activeMeterIndex = activeMeterIndex == 2 ? null : 2;
                      });
                    },
                    children: [
                      new ExpansionPanel(
                          canTapOnHeader: true,
                          isExpanded: activeMeterIndex == 2,
                          headerBuilder: (BuildContext context,
                                  bool isExpanded) =>
                              new Container(
                                alignment: Alignment.centerLeft,
                                child: new Padding(
                                  padding: EdgeInsets.only(
                                      left: (15 / 320) * screenWidth),
                                  child: new Text(titles[2],
                                      style: new TextStyle(
                                          fontSize: (12.5 / 720) * screenHeight,
                                          fontFamily: "IBMPlexSans",
                                          fontWeight: FontWeight.w500,
                                          color: (activeMeterIndex == 2)
                                              ? Colors.white
                                              : const Color(0xFF2d3438))),
                                ),
                              ),
                          body: GraduationDetails()),
                    ],
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(top: 10.0, left: 45.0, right: 45.0),
                  child: CustomExpansionPanelList(
                    expansionHeaderHeight: 45.0,
                    iconColor: (activeMeterIndex == 3)
                        ? Colors.white
                        : (postgraduationDetails == true)
                            ? NeutralColors.mango
                            : Colors.black,
                    backgroundColor1: (activeMeterIndex == 3)
                        ? const Color(0xFF6979f8)
                        : (postgraduationDetails == true)
                            ? NeutralColors.mango.withOpacity(0.05)
                            : const Color(0xFF3380cc).withOpacity(0.10),
                    backgroundColor2: (activeMeterIndex == 3)
                        ? const Color(0xFF6979f8)
                        : (postgraduationDetails == true)
                            ? NeutralColors.mango.withOpacity(0.05)
                            : const Color(0xFF886dd7).withOpacity(0.10),
                    expansionCallback: (int index, bool status) {
                      setState(() {
                         getTenthDetailsFilled();
                        getTwelthDetailsFilled();
                        getGraduationDetailsFilled();
                        getPostGraduationDetailsFilled();
                        getProfessionalDegreeDetailsFilled();
                        activeMeterIndex = activeMeterIndex == 3 ? null : 3;
                      });
                    },
                    children: [
                      new ExpansionPanel(
                        canTapOnHeader: true,
                        isExpanded: activeMeterIndex == 3,
                        headerBuilder:
                            (BuildContext context, bool isExpanded) =>
                                new Container(
                          alignment: Alignment.centerLeft,
                          child: new Padding(
                            padding:
                                EdgeInsets.only(left: (15 / 320) * screenWidth),
                            child: new Text(titles[3],
                                style: new TextStyle(
                                    fontSize: (12.5 / 720) * screenHeight,
                                    fontFamily: "IBMPlexSans",
                                    fontWeight: FontWeight.w500,
                                    color: (activeMeterIndex == 3)
                                        ? Colors.white
                                        : const Color(0xFF2d3438))),
                          ),
                        ),
                        body: PostGraduationDetails(),
                      ),
                    ],
                  ),
                ),
                Container(
                    child: new LayoutBuilder(builder: (context, constraint) {
                  return new Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Container(
                        margin:
                            EdgeInsets.only(top: 10.0, left: 45.0, right: 45.0),
                        child: CustomExpansionPanelList(
                          expansionHeaderHeight: 45.0,
                          iconColor: (activeMeterIndex == 4)
                              ? Colors.white
                              : (professionalDegreeDetails == true)
                                  ? NeutralColors.mango
                                  : Colors.black,
                          backgroundColor1: (activeMeterIndex == 4)
                              ? const Color(0xFF6979f8)
                              : (professionalDegreeDetails == true)
                                  ? NeutralColors.mango.withOpacity(0.05)
                                  : const Color(0xFF3380cc).withOpacity(0.10),
                          backgroundColor2: (activeMeterIndex == 4)
                              ? const Color(0xFF6979f8)
                              : (professionalDegreeDetails == true)
                                  ? NeutralColors.mango.withOpacity(0.05)
                                  : const Color(0xFF886dd7).withOpacity(0.10),
                          expansionCallback: (int index, bool status) {
                            setState(() {
                               getTenthDetailsFilled();
                        getTwelthDetailsFilled();
                        getGraduationDetailsFilled();
                        getPostGraduationDetailsFilled();
                        getProfessionalDegreeDetailsFilled();

                              activeMeterIndex =
                                  activeMeterIndex == 4 ? null : 4;
                            });
                          },
                          children: [
                            new ExpansionPanel(
                                canTapOnHeader: true,
                                isExpanded: activeMeterIndex == 4,
                                headerBuilder: (BuildContext context,
                                        bool isExpanded) =>
                                    new Container(
                                      alignment: Alignment.centerLeft,
                                      child: new Padding(
                                        padding: EdgeInsets.only(
                                            left: (15 / 320) * screenWidth),
                                        child: new Text("Professional Degree",
                                            style: new TextStyle(
                                                fontSize:
                                                    (12.5 / 720) * screenHeight,
                                                fontFamily:
                                                    "IBMPlexSans",
                                                fontWeight: FontWeight.w500,
                                                color: (activeMeterIndex == 4)
                                                    ? Colors.white
                                                    : const Color(0xFF2d3438))),
                                      ),
                                    ),
                                body: ProfessionalDegreeDetails()),
                          ],
                        ),
                      ),
                    ],
                  );
                })),
                Padding(
                  padding: EdgeInsets.only(
                      left: screenWidth * 45 / 360,
                      top: screenHeight * 37.5 / 720,
                      right: screenWidth * 45 / 360,
                      bottom: screenHeight * 15 / 360),
                  child: Center(
                      child: PrimaryButton(
                    //gradient: true,
                    gradient: (tenthDetails == true &&
                            twelthDetails == true &&
                            graduationDetails == true &&
                            postgraduationDetails == true &&
                            professionalDegreeDetails == true)
                        ? true
                        : false,
                    //height: screenHeight * 0.055,
                    onTap: () {
                      //AppRoutes.push(context, LoginWorkexperience());
                      //FieldValidator.buttonEnable = true;
                      if (tenthDetails == true &&
                          twelthDetails == true &&
                          graduationDetails == true &&
                          postgraduationDetails == true &&
                          professionalDegreeDetails == true) {
                        AppRoutes.push(context, LoginWorkexperience());
                      }
                    },
                    text: LoginAppStrings.confirmDetails,
                  )),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:imsindia/components/primary_Button.dart';
import 'package:imsindia/components/primary_button_gradient.dart';
import 'package:imsindia/utils/svg_icons.dart';
import 'package:imsindia/utils/text_styles.dart';
import 'package:imsindia/views/home_pages/home_widget.dart';
import 'package:imsindia/views/login_page/login_enrollmentid_widget.dart';
import 'package:imsindia/views/login_page/login_followpeople_widget.dart';
import '../../components/Input_forms.dart';
import '../../resources/strings/login.dart';
import '../../routers/routes.dart';
import '../../utils/validator.dart';
import '../../utils/colors.dart';
import 'package:imsindia/resources/strings/login.dart';
import './login_create_profile.dart';

class LoginCongratulations extends StatefulWidget {
  @override
  _LoginCongratulationsState createState() => _LoginCongratulationsState();
}

final TextEditingController _password_controller =  TextEditingController();
String get password => _password_controller.text;
bool passwordValidator = false;


String _password = "";
class _LoginCongratulationsState extends State<LoginCongratulations> {
  @override
  Future<bool> _onWillPop() {
    AppRoutes.push(context, LoginEnrollmentidWidget());

  }

  void initState() {
    super.initState();
    setState(() {
      _password_controller.text = '';
    });
    _password_controller.addListener(() {
      setState(() {
        validatePassword(password);
      });
    });
  }

  TextEditingController mycontroller = new TextEditingController();
  final _formKey = GlobalKey<FormState>();
  bool _obscureText = true;

  void _toggle() {
    setState(() {
      _obscureText = !_obscureText;
    });
  }

  static String validatePassword(String value){
    if (value.isEmpty && value.length==0) {
      passwordValidator = false;
      return null;
   }else if(value.length<6){
      passwordValidator=false;
      return LoginUsingIMSPin.invalidIMSPassword;
    }
    else{
      passwordValidator=true;
    }
//    Pattern pattern = r'^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[!@#\$&*~]).{8,}$';
//    RegExp regex = new RegExp(pattern);
//    if (!regex.hasMatch(value.trim())) {
//      passwordValidator = false;
//      return CongratulationsLabels.invalidPassword;
//    }
//    if (regex.hasMatch(value.trim())) {
//      passwordValidator=true;
//      return null;
//    }
  }


  @override
  Widget build(BuildContext context) {
    final screenHeight = MediaQuery.of(context).size.height;
    final screenWidth = MediaQuery.of(context).size.width;
    final defaultScreenHeight=720;
    final defaultScreenWidth=360;
    if(mycontroller!=null){
    }
    return WillPopScope(
      onWillPop: _onWillPop,
      child: Scaffold(
       // 
       // 
        body: SingleChildScrollView(
            child: Container(
             // constraints: BoxConstraints.expand(),
//          decoration: BoxDecoration(
//            color: Color.fromARGB(255, 255, 255, 255),
//          ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  Container(
                    height: screenHeight*0.140,
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: [
                        GestureDetector(
                          onTap: (){
                            AppRoutes.push(context, LoginEnrollmentidWidget());
                          },
                          child: Container(
                            child: Padding(
                              padding: const EdgeInsets.all(5.0),
                              child: Align(
                                alignment: Alignment.topLeft,
                                child: Container(
                                  width: (20/360)*screenWidth,
                                  height: (20/720)*screenHeight,
                                  margin: EdgeInsets.only(top: screenHeight*0.0738,left: screenWidth*0.062,right: 5),
                                  child: getSvgIcon.backSvgIcon,
                                ),
                              ),
                            ),
                          ),
                        ),
                        Spacer(),
                        Align(
                         // alignment: Alignment.topRight,
                          child: Container(
                            //   color: Color.fromRGBO(255, 45, 52, 56),

                            width: screenWidth*0.780,
                            height: screenHeight*0.9,
                            child: getSvgIcon.combinedshapeSvgIcon,
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(right: (45/defaultScreenWidth)*screenWidth,left: (45/defaultScreenWidth)*screenWidth,),
                    child: Column(
                      children: [
                        Container(
                        // height:(27/defaultScreenHeight)*screenHeight,
                        //  width: (165/defaultScreenWidth)*screenWidth,
                          margin: EdgeInsets.only(top:(55/defaultScreenHeight)*screenHeight),
                          child: Text(
                            CongratulationsLabels.congratulationsTitle,
                            style: TextStyle(
                              color: NeutralColors.charcoal_grey,
                              fontSize: (21/defaultScreenWidth)*screenWidth,
                              fontFamily: "IBMPlexSans",
                              fontWeight: FontWeight.w700,
                            ),
                            textAlign: TextAlign.center,
                          ),
                        ),

                        Container(
                           // height: (88/defaultScreenHeight)*screenHeight,
                           // width: (270/defaultScreenWidth)*screenWidth,
                            child:Column(
                              children: <Widget>[
                                Container(
                                  margin: EdgeInsets.only(top: (7/defaultScreenHeight)*screenHeight),
                                  child: Text(
                                    CongratulationsLabels.congratulationsSubText,
                                    style: TextStyle (
                                      color:  NeutralColors.blue_grey,
                                      fontWeight: FontWeight.w400,
                                      fontFamily: "IBMPlexSans",
                                      fontStyle:  FontStyle.normal,
                                      fontSize: (14/defaultScreenWidth)*screenWidth,
                                    ),
                                    textAlign: TextAlign.center,
                                  ),
                                ),
                                Container(
                                  margin: EdgeInsets.only(top: (20/defaultScreenHeight)*screenHeight),
                                  child:
                                  Text(
                                    CongratulationsLabels.congratulationSubTitle,
                                    style: TextStyle (
                                      color:  NeutralColors.blue_grey,
                                      fontWeight: FontWeight.w400,
                                      fontFamily: "IBMPlexSans",
                                      fontStyle:  FontStyle.normal,
                                      fontSize: (14/defaultScreenWidth)*screenWidth,
                                    ),
                                    textAlign: TextAlign.center,
                                  ),

                                ),
                                Container(
                                  margin: EdgeInsets.only(top: (5/defaultScreenHeight)*screenHeight),
                                  child:Center(
                                    child:  Row(
                                        mainAxisAlignment: MainAxisAlignment.center,
                                        children: <Widget>[
                                          Text(
                                            "you login,",
                                            style: TextStyle (
                                              color:  NeutralColors.blue_grey,
                                              fontWeight: FontWeight.w400,
                                              fontFamily: "IBMPlexSans",
                                              fontStyle:  FontStyle.normal,
                                              fontSize: (14/defaultScreenWidth)*screenWidth,
                                            ),
                                            textAlign: TextAlign.right,
                                          ),
                                          Text(" stephen@gmail.com",
                                            style: TextStyle(
                                                fontFamily: "IBMPlexSans",
                                                color: NeutralColors.charcoal_grey,
                                                fontSize: (14/defaultScreenWidth)*screenWidth,
                                                fontWeight: FontWeight.w500),
                                            textAlign: TextAlign.center,)
                                        ]),
                                  ),
                                ),
                              ],
                            )
                        ),
                        Container(
                            width: double.infinity,
                            margin: EdgeInsets.only(top:(63/defaultScreenHeight)*screenHeight),
                            child: TextInputFormField(
                              onFieldSubmitted: (val){
                              },
                              controller: _password_controller,
                              autovalidate: true,
                              validator: validatePassword,
                             // focusNode: focus,
                              keyboardType: TextInputType.text,
                              textInputAction: TextInputAction.done,
                               suffixIcon:IconButton(
                                      padding: EdgeInsets.only(left: (30/360)*screenWidth,top: 20/720 * screenHeight),
                                      icon: _obscureText ? getSvgIcon.lockSvgIcon : getSvgIcon.unlockSvgIcon,
                                      iconSize: 5,
                                      onPressed: () {
                                        _toggle();
                                      }),
                                       lableText: CongratulationsLabels.setNewPsw,
                                  lableStyle: TextStyles.labelStyle,
                                  border:  _password_controller.text.isEmpty ? UnderlineInputBorder(borderSide:BorderSide(color:SemanticColors.blueGrey.withOpacity(0.3),width: 1 )): UnderlineInputBorder(borderSide:BorderSide(color:SemanticColors.dark_purpely,width: 1 ) ),
                             
                              obscureText: _obscureText,
                              fontStyle:  TextStyles.editTextStyle,
                              onSaved: (value) => _password = value,
                            ),
                          ),
                        
                        Container(
                            height: (40/defaultScreenHeight)*screenHeight,
                            width: (270/defaultScreenWidth)*screenWidth,
                            margin: EdgeInsets.only(top: (40/defaultScreenHeight)*screenHeight),
                            child: PrimaryButton(
                              gradient: (passwordValidator == true)
                                  ? true
                                  : false,
                              onTap: () {
                                if (passwordValidator==true) {
                                  AppRoutes.push(context, LoginFollowpeopleWidget());
                                }
                              },
                              text: CongratulationsLabels.btnText.toUpperCase(),
                            ) // Group 4
                        ),
                        GestureDetector(
                          onTap: (){
                            AppRoutes.push(context, HomeWidget());
                          },
                          child: Container(
                            height:  (18/defaultScreenHeight)*screenHeight,
                            width: (270/defaultScreenWidth)*screenWidth,
                            margin: EdgeInsets.only(top: (15/defaultScreenHeight)*screenHeight),
                            child:Align(
                              alignment: Alignment.topLeft,
                              child:Text(
                                CongratulationsLabels.continueIMS,
                                style: TextStyle(
                                  color:  PrimaryColors.azure_Dark,
                                  fontSize: (14/defaultScreenWidth)*screenWidth,
                                  fontFamily: "IBMPlexSans",
                                  fontWeight: FontWeight.w400,
                                ),
                                textAlign: TextAlign.left,
                              ),),

                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
      ),
    );
  }
}

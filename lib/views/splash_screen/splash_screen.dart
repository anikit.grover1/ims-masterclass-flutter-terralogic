import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_windowmanager/flutter_windowmanager.dart';
import 'package:imsindia/routers/routes.dart';
import 'package:imsindia/views/Arun/BlogContent.dart';
import 'package:imsindia/views/Arun/SqliteHelper.dart';
import 'package:imsindia/views/Arun/search/LocalSearchContect.dart';
import 'package:imsindia/views/Arun/search/SqliteHelper_LocalSearchContent.dart';
import 'package:imsindia/views/home_pages/home_widget.dart';
import 'package:imsindia/views/home_pages/scrollTabComponents/base_widget.dart';
import 'package:imsindia/views/login_page/login_welcome_screen.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SplashScreen extends StatefulWidget {
  @override
  SplashScreenState createState() => SplashScreenState();
}

var obj;

List<BlogContent> itemsBlogs;
List<LocalSearchContent> itemsLocalSearch;

class SplashScreenState extends State<SplashScreen> {
  bool LoginStatus;
//   var printbase=base64.encode(utf8.encode(hi));
  void onArrowPointToRightPressed(BuildContext context) => {};
  void getLoginStatus() async {
    //      print(printbase);
    SharedPreferences prefs = await SharedPreferences.getInstance();
    if (prefs.getBool("LoginStatus") == true) {
      //[Arun ] You should check for login status and
      //// authorization token, state of login we have two step login process we need to be able identify where we are
      ////Praveen: authorization token will be saved until logged out, for two step login process i am not clear arun
      //AppRoutes.push(context, HomeWidget());
      AppRoutes.push(context, HomeWidget());
    } else {
      AppRoutes.push(context, LoginWelcomeWidget());
    }
  }
  Future<void> secureScreen() async {
    await FlutterWindowManager.addFlags(FlutterWindowManager.FLAG_SECURE);
  }
  @override
  void initState() {
    getLoginStatus();
    secureScreen();//[Arun ]Why are we calling it====Praveen: for checking login status
    super.initState();
    obj = SqliteHelper('imsApp.db');
    obj.create();
    //InserDataBlog("a", "b", "c", "d");
    //InserDataSearch("a1");
    //InserDataBlog("a2", "b2", "c2", "d2");
    //GetLocalSearchContent();
  }

  @override
  void didChangeDependencies() {
    getLoginStatus(); //[Arun ]Why are we calling it======Praveen: for checking login status
  }

  @override
  Widget build(BuildContext context) {
    //[Arun here you should show splash for may be 5 seconds then should transfer
    //praveen: i am thinking initstate will call before widget loading it self,please correct me if i am wrong
    // control to ne=xt screen logic of init should be kept here.
    Future<bool> _onWillPop() {}
    return WillPopScope(
        onWillPop: _onWillPop,
        child: Scaffold(
          body: Container(),
        ));
  }

  void GetLocalSearchContentBlog() async {
    itemsBlogs = await obj.getDataBlog();
    for (int i = 0; i < itemsBlogs.length; i++) {
      print('Arun in GetBlogSearchContent :: ${itemsBlogs[i]}');
    }
  }

  void GetLocalSearchContentSearch() async {
    itemsLocalSearch = await obj.getDataSearch();
    for (int i = 0; i < itemsLocalSearch.length; i++) {
      print('Arun in GetLocalSearchContent :: ${itemsLocalSearch[i]}');
    }
  }

  void InserDataBlog(String strContent, String strContent1, String strContent2,
      String strContent3) async {
    var bb = new BlogContent(
      content: strContent,
      content1: strContent1,
      content2: strContent2,
      content3: strContent3,
    );
    bb.toString();
    await obj.insertBlog(bb);
    GetLocalSearchContentBlog();
  }

  void InserDataSearch(String strContent) async {
    var bb = new LocalSearchContent(
      content: strContent,
    );
    bb.toString();
    await obj.insertSearch(bb);
    GetLocalSearchContentSearch();
  }


  void printmsgininterval() {
    debugPrint('Arun :: Hello in printmsgininterval');
  }
}

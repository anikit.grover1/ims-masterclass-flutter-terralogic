import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:imsindia/resources/strings/channel_labels.dart';
import 'package:imsindia/resources/strings/prepare.dart';
import 'package:imsindia/routers/routes.dart';
import 'package:imsindia/utils/colors.dart';
import 'package:imsindia/utils/screen_util.dart';
import 'package:imsindia/utils/svg_images/prepare_svg_images.dart';
import 'package:imsindia/views/Arun/ReadMoreText.dart';
import 'package:imsindia/views/Discussion/discussion_comments.dart';
import 'package:imsindia/views/Discussion/discussion_new_post.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:imsindia/utils/svg_icons.dart';

bool value = false;

class DiscussionTopic extends StatefulWidget {
  @override
  DiscussionTopicState createState() => DiscussionTopicState();
}

class DiscussionTopicState extends State<DiscussionTopic> {

  List likes = [
    324,
    324,
    324,
    324
  ];

  //var likes = 324;
  bool isSelected = false;
  bool bookmarkSelected = false;
  List<dynamic> like = [];
  List<dynamic> bookmark = [];
  final GlobalKey _menuKey = new GlobalKey();
  bool showFilter = false;
  getFilterDetails() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    bool boolVal = prefs.getBool("FilterClicked");
    setState(() {
      showFilter = boolVal;
    });
    print("ffffff"+showFilter.toString());
  }
  void setFilterFalse() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setBool("FilterClicked", false);
    getFilterFalse();
  }
  void getFilterFalse() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    showFilter= prefs.getBool("FilterClicked");
  }

  @override
  void initState(){
    super.initState();
    //setFilterFalse();
    getFilterDetails();
  }
  void didChangeDependencies() {
    super.didChangeDependencies();
    getFilterDetails();
    //setFilterFalse();
    print("hiiiiiiiiiiii");
  }
  Widget build(BuildContext context) {
    final screenHeight = MediaQuery.of(context).size.height;
    final screenWidth = MediaQuery.of(context).size.width;

    return Scaffold(
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        onPressed: () {
          print('Clicked');
          AppRoutes.push(context, DiscussionNewPost());
        },
        backgroundColor: NeutralColors.purpleish_blue,
      ),
      body: Container(
        //color: Colors.white,
        child: Column(
          children: <Widget>[
            showFilter == true?
            Container(
              height: 41/720 * screenHeight,
              margin: EdgeInsets.only(left: 20/360 * screenWidth, top: 20/720 * screenHeight, right: 20/360 * screenWidth),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  Align(
                    alignment: Alignment.topLeft,
                    child: Container(
                      width: 40/360 * screenWidth,
                      height: 40/720 * screenHeight,
                      child: Stack(
                        alignment: Alignment.center,
                        children: [
                          Positioned(
                            left: 0,
                            top: 0,
                            child: Opacity(
                              opacity: 0.1,
                              child: Container(
                                width: 40/360 * screenWidth,
                                height: 40/720 * screenHeight,
                                decoration: BoxDecoration(
                                  color: Color.fromARGB(255, 86, 72, 235),
                                  borderRadius: BorderRadius.all(Radius.circular(20)),
                                ),
                                child: Container(),
                              ),
                            ),
                          ),
                          Positioned(
                            left: 14/360 * screenWidth,
                            top: 6/720 * screenHeight,
                            child: Text(
                              "#",
                              style: TextStyle(
                                color: NeutralColors.purpleish_blue,
                                fontSize: 18/720 * screenHeight,
                                fontFamily: "IBMPlexSans",
                                fontWeight: FontWeight.w700,
                              ),
                              textAlign: TextAlign.left,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  Align(
                    alignment: Alignment.topLeft,
                    child: Container(
                      width: 88/360 * screenWidth,
                      height: 41/720 * screenHeight,
                      margin: EdgeInsets.only(left: 12/360 * screenWidth),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            "4 posts",
                            style: TextStyle(
                              color: NeutralColors.blue_grey,
                              fontSize: 12/720 * screenHeight,
                              fontFamily: "IBM Plex Sans",
                              fontWeight: FontWeight.w500,
                            ),
                            textAlign: TextAlign.left,
                          ),
                          Container(
                            margin: EdgeInsets.only(top: 3/720 * screenHeight),
                            child: Text(
                              "#IIM",
                              style: TextStyle(
                                color: NeutralColors.dark_navy_blue,
                                fontSize: 14/720 * screenHeight,
                                fontFamily: "IBM Plex Sans",
                                fontWeight: FontWeight.w500,
                              ),
                              textAlign: TextAlign.left,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  Spacer(),
                  Align(
                    alignment: Alignment.topLeft,
                    child: GestureDetector(
                      onTap: (){
                        setState(() {
                          setFilterFalse();
                        });
                      },
                      child: Container(
                        width: 23/360 * screenWidth,
                        height: 23/720 * screenHeight,
                        margin: EdgeInsets.only(top: 9/720 * screenHeight),
                        child: Image.asset(
                          "assets/images/close.png",
                          fit: BoxFit.contain,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ):Container(),
            showFilter == true?
            Padding(
              padding: EdgeInsets.only(top: 10/720 * screenHeight),
              child: Divider(),
            ): Container(),
            Expanded(
              child: ListView.separated(
                itemCount: 4,
                padding: EdgeInsets.only(bottom: (40)),
                itemBuilder: (context, index) {
                  final item = likes[index];
                  return index % 2 == 0 ?
                  Container(
                    margin: EdgeInsets.only(
                      left: (20 / 360) * screenWidth,
                      top: (30 / Constant.defaultScreenHeight) *
                          screenHeight,
                      right: (0 / Constant.defaultScreenWidth) *
                          screenWidth,
                    ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        /**profile image and name colum**/
                        Container(
                          //color: Colors.pink,
                          child: Row(
                            mainAxisAlignment:
                            MainAxisAlignment.spaceBetween,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Container(
//                                    margin: EdgeInsets.only(
//                                        right: (20 / 360) * screenWidth),
                                child: Row(
                                  crossAxisAlignment:
                                  CrossAxisAlignment.start,
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: <Widget>[
                                    /**Profile pic**/
                                    Container(
                                      height: (40 /
                                          Constant.defaultScreenHeight) *
                                          screenHeight,
                                      width:
                                      (40 / Constant.defaultScreenWidth) *
                                          screenWidth,
                                      margin: EdgeInsets.only(
                                          top: (0 / 720) * screenHeight),
                                      decoration: new BoxDecoration(
                                          shape: BoxShape.circle,
                                          image: new DecorationImage(
                                              fit: BoxFit.cover,
                                              image: AssetImage(
                                                  'assets/images/oval-2.png'))),
                                    ),
                                    Container(
                                      child: Column(
                                        children: <Widget>[
                                          Container(
                                            height: (20 /
                                                Constant
                                                    .defaultScreenHeight) *
                                                screenHeight,
                                            width: (100 / 360) * screenWidth,
                                            margin: EdgeInsets.only(
                                                top: (0 /
                                                    Constant
                                                        .defaultScreenHeight) *
                                                    screenHeight,
                                                left:
                                                (12 / 360) * screenWidth),
                                            child: Text(
                                              SubQueriesCommentsScreenStrings
                                                  .Text_UserName,
                                              style: TextStyle(
                                                color: NeutralColors
                                                    .dark_navy_blue,
                                                fontWeight: FontWeight.w500,
                                                fontFamily: "IBMPlexSans",
                                                fontStyle: FontStyle.normal,
                                                fontSize: (14 /
                                                    Constant
                                                        .defaultScreenWidth) *
                                                    screenWidth,
                                              ),
                                              textAlign: TextAlign.start,
                                            ),
                                          ),
                                          Container(
                                            height: (18 /
                                                Constant
                                                    .defaultScreenHeight) *
                                                screenHeight,
                                            width: (100 / 360) * screenWidth,
                                            margin: EdgeInsets.only(
                                                top: (5 /
                                                    Constant
                                                        .defaultScreenHeight) *
                                                    screenHeight,
                                                left:
                                                (12 / 360) * screenWidth),
                                            child: Text(
                                              SubQueriesCommentsScreenStrings
                                                  .Text_MsgdTime,
                                              style: TextStyle(
                                                color:
                                                NeutralColors.blue_grey,
                                                fontWeight: FontWeight.w500,
                                                fontFamily: "IBMPlexSans",
                                                fontStyle: FontStyle.normal,
                                                fontSize: (12 /
                                                    Constant
                                                        .defaultScreenWidth) *
                                                    screenWidth,
                                              ),
                                              textAlign: TextAlign.start,
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              Align(
                                alignment: Alignment.topRight,
                                child: Container(
                                  //color: Colors.blue,
                                  //width: 10/360 * screenWidth,
                                  //height: 11/720 * screenHeight,
                                  margin:EdgeInsets.only(left: 115/360 * screenWidth,top: 5/720 * screenHeight,right: 0/360 * screenWidth),
                                  child: GestureDetector(
                                    onTap: (){
                                      setState(() {
                                        print(index);
                                        if(bookmark.contains(index)){
                                          bookmark.remove(index);
                                        }
                                        else{
                                          bookmark.add(index);
                                        }
                                        bookmarkSelected = !bookmarkSelected;
                                      });
                                    },
                                    child:Container(
                                      //width: 10/360 * screenWidth,
                                      //height: 11/720 * screenHeight,
                                        child: bookmark.contains(index)?
                                        SvgPicture.asset(
                                            getPrepareSvgImages.bookmarkActive
                                        ):
                                        Image.asset(
                                          'assets/images/icon-bookmark2.png',
                                        )
                                    ),
                                  ),
                                ),
                              ),
                              Align(
                                alignment: Alignment.topRight,
//                                    margin: EdgeInsets.only(
//                                      right: 0,
//                                      top: 0,
//                                    ),
                                child: GestureDetector(
                                  onTap: () {
                                    // This is a hack because _PopupMenuButtonState is private.
                                    dynamic state = _menuKey.currentState;
                                    state.showButtonMenu();
                                  },
                                  child: Container(
                                    //color: Colors.red,
                                    margin: EdgeInsets.only(top: 0, right: 0),
                                    height: 25/720 * screenHeight,
                                    child: PopupMenuButton(
                                        icon: Icon(Icons.more_vert,color: NeutralColors.blue_grey),
                                        padding: EdgeInsets.all(0),
                                        // key: _menuKey,
                                        itemBuilder: (_) =>
                                        <PopupMenuItem<String>>[
                                          new PopupMenuItem<String>(
                                              child: const Text(
                                                'Report',
                                                style: TextStyle(
                                                  color: NeutralColors
                                                      .gunmetal,
                                                  fontFamily:
                                                  "IBMPlexSans",
                                                  fontWeight:
                                                  FontWeight
                                                      .normal,
                                                  fontSize: 14,
                                                ),
                                              ),
                                              value: 'Report'),
                                        ],
                                        onSelected: (_) {}),

                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),

                        Container(
                          //height: 46/720 * screenHeight,
                          width: 320/360 * screenHeight,
                          margin: EdgeInsets.only( top: 13/720 * screenHeight, right: 20/360 * screenWidth),
                          child: Text(
                            'What should be my required percentile to get into a IIM A, B, C in CAT 2019.',
                            style: TextStyle(
                              color: NeutralColors
                                  .dark_navy_blue,
                              fontFamily:
                              "IBMPlexSans",
                              height: 1,
                              fontWeight: FontWeight.w400,
                              fontSize: 14/720 * screenHeight,
                            ),
                          ),
                        ),
                        /**Extended Paragraph colum**/

                        Container(
                          margin: EdgeInsets.only(
                              top: (10 / Constant.defaultScreenHeight) *
                                  screenHeight,
                              right: (20 / 360) * screenWidth),
                          child: SingleChildScrollView(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Padding(
                                  padding: const EdgeInsets.all(1.0),
                                  child: ReadMoreText(
                                    SubQueriesCommentsScreenStrings
                                        .ReadMoreText_Content,
                                    trimLines: 3,
                                    colorClickableText: Colors.blue,
                                    trimMode: TrimMode.Line,
                                    trimCollapsedText:
                                    SubQueriesCommentsScreenStrings
                                        .Text_ReadMore,
                                    trimExpandedText:
                                    SubQueriesCommentsScreenStrings
                                        .Text_ReadLess,
                                    style: TextStyle(
                                      color: NeutralColors.gunmetal,
                                      fontWeight: FontWeight.w500,
                                      fontFamily: "IBMPlexSans",
                                      fontStyle: FontStyle.normal,
                                      height: 1.5,
                                      fontSize: (12/720)*screenHeight,
                                    ),
                                  ),
                                )
                              ],
                            ),
                          ),
                        ),
                        /**upvotes and reply colum**/
                        Container(
                                margin: EdgeInsets.only(
                                    bottom: 24 / 720 * screenHeight),
                                child:InkWell(
                                  onTap: () {
                                    setState(() {
                                      if (like.contains(index)) {
                                        like.remove(index);
                                      } else {
                                        like.add(index);
                                      }
                                      isSelected = !isSelected;
                                    });
                                  },
                                  child: Row(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    children: <Widget>[
                                      Container(
                                        margin: EdgeInsets.only(
                                            top: like.contains(index)
                                                ? 25 /
                                                    Constant
                                                        .defaultScreenHeight *
                                                    screenHeight
                                                : 26 /
                                                    Constant
                                                        .defaultScreenHeight *
                                                    screenHeight),
                                        child: Container(
                                          height: 11 / 720 * screenHeight,
                                          width: 11 / 360 * screenWidth,
                                          child: like.contains(index)
                                              ? Icon(
                                                  Icons.thumb_up,
                                                  color: NeutralColors
                                                      .deep_sky_blue,
                                                  size: 11 / 360 * screenWidth,
                                                )
                                              : SvgPicture.asset(
                                                  getSvgIcon.like,
                                                  fit: BoxFit.contain,
                                                ),
                                        ),
                                      ),
                                      Container(
                                          margin: EdgeInsets.only(
                                              top: 18 /
                                                  Constant.defaultScreenHeight *
                                                  screenHeight,
                                              left: 5 / 360 * screenWidth),
                                          decoration: BoxDecoration(
                                            color: Colors.transparent,
                                            borderRadius: BorderRadius.all(
                                                Radius.circular(2)),
                                          ),
                                          child: Container(
                                            margin: EdgeInsets.only(
                                                top: 6 /
                                                    Constant
                                                        .defaultScreenHeight *
                                                    screenHeight),
                                            child: Text(
                                              // SubQueriesCommentsScreenStrings.Text_Liked,
                                              like.contains(index)
                                                  ? '${likes[index] + 1} Likes'
                                                  : '${likes[index]} Likes',
                                              style: TextStyle(
                                                color: like.contains(index)
                                                    ? NeutralColors
                                                        .deep_sky_blue
                                                    : NeutralColors.black,
                                                fontWeight: FontWeight.normal,
                                                fontFamily: "IBMPlexSans",
                                                fontStyle: FontStyle.normal,
                                                fontSize: (12 /
                                                        Constant
                                                            .defaultScreenWidth) *
                                                    screenWidth,
                                              ),
                                              textAlign: TextAlign.left,
                                            ),
                                          )),

                                    Container(
                                      margin: EdgeInsets.only(
                                          top: (26 /
                                                  Constant
                                                      .defaultScreenHeight) *
                                              screenHeight,
                                          left: (20 /
                                                  Constant.defaultScreenWidth) *
                                              screenWidth),
                                      child: Icon(
                                        Icons.chat_bubble_outline,
                                        color: NeutralColors.black,
                                        size: 11 / 360 * screenWidth,
                                      ),
                                    ),
                                    Container(
                                        margin: EdgeInsets.only(
                                            top: 15 /
                                                Constant.defaultScreenHeight *
                                                screenHeight,
                                            left: 5 / 360 * screenWidth),
                                        decoration: BoxDecoration(
                                          color: Colors.transparent,
                                          borderRadius: BorderRadius.all(
                                              Radius.circular(2)),
                                        ),
                                        child: GestureDetector(
                                          onTap: () {
                                            AppRoutes.push(
                                                context, DiscussionComments());
                                          },
                                          child: Container(
                                            margin: EdgeInsets.only(
                                                top: 6 /
                                                    Constant
                                                        .defaultScreenHeight *
                                                    screenHeight),
                                            child: Text(
                                              '232 Comments',
                                              style: TextStyle(
                                                color: NeutralColors
                                                    .dark_navy_blue,
                                                fontWeight: FontWeight.normal,
                                                fontFamily: "IBMPlexSans",
                                                fontStyle: FontStyle.normal,
                                                fontSize: (12 /
                                                        Constant
                                                            .defaultScreenWidth) *
                                                    screenWidth,
                                              ),
                                              textAlign: TextAlign.left,
                                            ),
                                          ),
                                        )),
                                  ],
                                ),
                              ),),

                      ],
                    ),
                  ):
                  Container(
                    margin: EdgeInsets.only(
                      left: (20 / 360) * screenWidth,
                      top: (24.5 / Constant.defaultScreenHeight) *
                          screenHeight,
                      right: (0 / Constant.defaultScreenWidth) *
                          screenWidth,
                    ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        /**profile image and name colum**/
                        Container(
                          // color: Colors.pink,
                          child: Row(
                            mainAxisAlignment:
                            MainAxisAlignment.spaceBetween,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Container(
//                                    margin: EdgeInsets.only(
//                                        right: (20 / 360) * screenWidth),
                                child: Row(
                                  crossAxisAlignment:
                                  CrossAxisAlignment.start,
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: <Widget>[
                                    /**Profile pic**/
                                    Container(
                                      height: (40 /
                                          Constant.defaultScreenHeight) *
                                          screenHeight,
                                      width:
                                      (40 / Constant.defaultScreenWidth) *
                                          screenWidth,
                                      margin: EdgeInsets.only(
                                          top: (0 / 720) * screenHeight),
                                      decoration: new BoxDecoration(
                                          shape: BoxShape.circle,
                                          image: new DecorationImage(
                                              fit: BoxFit.cover,
                                              image: AssetImage(
                                                  'assets/images/oval-2.png'))),
                                    ),
                                    Container(
                                      child: Column(
                                        children: <Widget>[
                                          Container(
                                            height: (18 /
                                                Constant
                                                    .defaultScreenHeight) *
                                                screenHeight,
                                            width: (100 / 360) * screenWidth,
                                            margin: EdgeInsets.only(
                                                top: (0 /
                                                    Constant
                                                        .defaultScreenHeight) *
                                                    screenHeight,
                                                left:
                                                (12 / 360) * screenWidth),
                                            child: Text(
                                              SubQueriesCommentsScreenStrings
                                                  .Text_UserName,
                                              style: TextStyle(
                                                color: NeutralColors
                                                    .dark_navy_blue,
                                                fontWeight: FontWeight.w500,
                                                fontFamily: "IBMPlexSans",
                                                fontStyle: FontStyle.normal,
                                                fontSize: (14 /
                                                    Constant
                                                        .defaultScreenWidth) *
                                                    screenWidth,
                                              ),
                                              textAlign: TextAlign.start,
                                            ),
                                          ),
                                          Container(
                                            height: (18 /
                                                Constant
                                                    .defaultScreenHeight) *
                                                screenHeight,
                                            width: (100 / 360) * screenWidth,
                                            margin: EdgeInsets.only(
                                                top: (5 /
                                                    Constant
                                                        .defaultScreenHeight) *
                                                    screenHeight,
                                                left:
                                                (12 / 360) * screenWidth),
                                            child: Text(
                                              SubQueriesCommentsScreenStrings
                                                  .Text_MsgdTime,
                                              style: TextStyle(
                                                color:
                                                NeutralColors.blue_grey,
                                                fontWeight: FontWeight.w500,
                                                fontFamily: "IBMPlexSans",
                                                fontStyle: FontStyle.normal,
                                                fontSize: (12 /
                                                    Constant
                                                        .defaultScreenWidth) *
                                                    screenWidth,
                                              ),
                                              textAlign: TextAlign.start,
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              Align(
                                alignment: Alignment.topRight,
                                child: Container(
                                  //width: 10/360 * screenWidth,
                                  //height: 11/720 * screenHeight,
                                  margin:EdgeInsets.only(left: 115/360 * screenWidth,top: 5/720 * screenHeight,right: 0/360 * screenWidth),
                                  child: GestureDetector(
                                    onTap: (){
                                      setState(() {
                                        print(index);
                                        if(bookmark.contains(index)){
                                          bookmark.remove(index);
                                        }
                                        else{
                                          bookmark.add(index);
                                        }
                                        bookmarkSelected = !bookmarkSelected;
                                      });
                                    },
                                    child:Container(
                                      //width: 10/360 * screenWidth,
                                      //height: 11/720 * screenHeight,
                                        child: bookmark.contains(index)?
                                        SvgPicture.asset(
                                            getPrepareSvgImages.bookmarkActive
                                        ):
                                        Image.asset(
                                          'assets/images/icon-bookmark2.png',
                                        )
                                    ),
                                  ),
                                ),
                              ),
                              Align(
                                alignment: Alignment.topRight,
//                                    margin: EdgeInsets.only(
//                                      right: 0,
//                                      top: 0,
//                                    ),
                                child: GestureDetector(
                                  onTap: () {
                                    // This is a hack because _PopupMenuButtonState is private.
                                    dynamic state = _menuKey.currentState;
                                    state.showButtonMenu();
                                  },
                                  child: Container(
                                    // color: Colors.red,
                                    margin: EdgeInsets.only(top: 0, right: 0),
                                    height: 25/720 * screenHeight,
                                    child: PopupMenuButton(
                                        icon: Icon(Icons.more_vert,color: NeutralColors.blue_grey),
                                        padding: EdgeInsets.all(0),
                                        // key: _menuKey,
                                        itemBuilder: (_) =>
                                        <PopupMenuItem<String>>[
                                          new PopupMenuItem<String>(
                                              child: const Text(
                                                'Report',
                                                style: TextStyle(
                                                  color: NeutralColors
                                                      .gunmetal,
                                                  fontFamily:
                                                  "IBMPlexSans",
                                                  fontWeight:
                                                  FontWeight
                                                      .normal,
                                                  fontSize: 14,
                                                ),
                                              ),
                                              value: 'Report'),
                                        ],
                                        onSelected: (_) {}),

                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                        Container(
                          height: 180/720 * screenHeight,
                          width: 320/360 * screenWidth ,
                          margin:EdgeInsets.only( top: 14/720 * screenHeight, right: 20/360 * screenWidth),
                          child: ClipRRect(
                            borderRadius: BorderRadius.circular(5),
                            child: Image.asset(
                              'assets/images/mask.png',
                              fit: BoxFit.none,
                            ),
                          ),
                        ),

                        Container(
                          //height: 46/720 * screenHeight,
                          width: 320/360 * screenHeight,
                          margin: EdgeInsets.only( top: 8/720 * screenHeight, right: 20/360 * screenWidth),
                          child: Text(
                            'What should be my required percentile to get into a IIM A, B, C in CAT 2019.',
                            style: TextStyle(
                              color: NeutralColors
                                  .dark_navy_blue,
                              fontFamily:
                              "IBMPlexSans",
                              height: 1,
                              fontWeight: FontWeight.w400,
                              fontSize: 14/720 * screenHeight,
                            ),
                          ),
                        ),
                        /**upvotes and reply colum**/
                        Container(
                                margin: EdgeInsets.only(
                                    bottom: 24 / 720 * screenHeight),
                                child:InkWell(
                                  onTap: () {
                                    setState(() {
                                      if (like.contains(index)) {
                                        like.remove(index);
                                      } else {
                                        like.add(index);
                                      }
                                      isSelected = !isSelected;
                                    });
                                  },
                                  child: Row(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    children: <Widget>[
                                      Container(
                                        margin: EdgeInsets.only(
                                            top: like.contains(index)
                                                ? 25 /
                                                    Constant
                                                        .defaultScreenHeight *
                                                    screenHeight
                                                : 26 /
                                                    Constant
                                                        .defaultScreenHeight *
                                                    screenHeight),
                                        child: Container(
                                          height: 11 / 720 * screenHeight,
                                          width: 11 / 360 * screenWidth,
                                          child: like.contains(index)
                                              ? Icon(
                                                  Icons.thumb_up,
                                                  color: NeutralColors
                                                      .deep_sky_blue,
                                                  size: 11 / 360 * screenWidth,
                                                )
                                              : SvgPicture.asset(
                                                  getSvgIcon.like,
                                                  fit: BoxFit.contain,
                                                ),
                                        ),
                                      ),
                                      Container(
                                          margin: EdgeInsets.only(
                                              top: 18 /
                                                  Constant.defaultScreenHeight *
                                                  screenHeight,
                                              left: 5 / 360 * screenWidth),
                                          decoration: BoxDecoration(
                                            color: Colors.transparent,
                                            borderRadius: BorderRadius.all(
                                                Radius.circular(2)),
                                          ),
                                          child: Container(
                                            margin: EdgeInsets.only(
                                                top: 6 /
                                                    Constant
                                                        .defaultScreenHeight *
                                                    screenHeight),
                                            child: Text(
                                              // SubQueriesCommentsScreenStrings.Text_Liked,
                                              like.contains(index)
                                                  ? '${likes[index] + 1} Likes'
                                                  : '${likes[index]} Likes',
                                              style: TextStyle(
                                                color: like.contains(index)
                                                    ? NeutralColors
                                                        .deep_sky_blue
                                                    : NeutralColors.black,
                                                fontWeight: FontWeight.normal,
                                                fontFamily: "IBMPlexSans",
                                                fontStyle: FontStyle.normal,
                                                fontSize: (12 /
                                                        Constant
                                                            .defaultScreenWidth) *
                                                    screenWidth,
                                              ),
                                              textAlign: TextAlign.left,
                                            ),
                                          )),

                                    Container(
                                      margin: EdgeInsets.only(
                                          top: (26 /
                                                  Constant
                                                      .defaultScreenHeight) *
                                              screenHeight,
                                          left: (20 /
                                                  Constant.defaultScreenWidth) *
                                              screenWidth),
                                      child: Icon(
                                        Icons.chat_bubble_outline,
                                        color: NeutralColors.black,
                                        size: 11 / 360 * screenWidth,
                                      ),
                                    ),
                                    Container(
                                        margin: EdgeInsets.only(
                                            top: 15 /
                                                Constant.defaultScreenHeight *
                                                screenHeight,
                                            left: 5 / 360 * screenWidth),
                                        decoration: BoxDecoration(
                                          color: Colors.transparent,
                                          borderRadius: BorderRadius.all(
                                              Radius.circular(2)),
                                        ),
                                        child: GestureDetector(
                                          onTap: () {
                                            AppRoutes.push(
                                                context, DiscussionComments());
                                          },
                                          child: Container(
                                            margin: EdgeInsets.only(
                                                top: 6 /
                                                    Constant
                                                        .defaultScreenHeight *
                                                    screenHeight),
                                            child: Text(
                                              '232 Comments',
                                              style: TextStyle(
                                                color: NeutralColors
                                                    .dark_navy_blue,
                                                fontWeight: FontWeight.normal,
                                                fontFamily: "IBMPlexSans",
                                                fontStyle: FontStyle.normal,
                                                fontSize: (12 /
                                                        Constant
                                                            .defaultScreenWidth) *
                                                    screenWidth,
                                              ),
                                              textAlign: TextAlign.left,
                                            ),
                                          ),
                                        )),
                                  ],
                                ),
                              ),),

                      ],
                    ),
                  );
                },
                separatorBuilder: (context, index) {
                  return Padding(
                    padding: EdgeInsets.only(left: 20/360 * screenWidth,right: 20/360 * screenWidth),
                    child: Divider(),
                  );
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}


import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:imsindia/resources/strings/channel_labels.dart';
import 'package:imsindia/resources/strings/prepare.dart';
import 'package:imsindia/routers/routes.dart';
import 'package:imsindia/utils/colors.dart';
import 'package:imsindia/utils/screen_util.dart';
import 'package:imsindia/utils/svg_images/prepare_svg_images.dart';
import 'package:imsindia/views/Arun/ReadMoreText.dart';
import 'package:imsindia/views/Discussion/discussion_home.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:imsindia/utils/svg_icons.dart';

bool value = false;
bool bookmarkSelected = false;
List<dynamic> bookmark =[];


class DiscussionYourPosts extends StatefulWidget {
  @override
  DiscussionYourPostsState createState() => DiscussionYourPostsState();
}

class DiscussionYourPostsState extends State<DiscussionYourPosts> {

//  void didChangeDependencies() {
//    sharedMethod();
//    super.didChangeDependencies();
//  }
//  void sharedMethod() async{
//    print('sharedMethod');
//    SharedPreferences prefs = await SharedPreferences.getInstance();
//    await prefs.setBool('focusNode', _focusNode.hasFocus);
//  }


  //bool clicked = false;
  //bool isSelected = false;
  FocusNode _focusNode = new FocusNode();

  Future<bool> _onWillPop() {
   AppRoutes.push(context, DiscussionHome());
  }

  final GlobalKey _menuKey = new GlobalKey();

  @override
  Widget build(BuildContext context) {
    final screenHeight = MediaQuery.of(context).size.height;
    final screenWidth = MediaQuery.of(context).size.width;

    return  Scaffold(
          body: Container(
            child: Column(
              children: <Widget>[
                Container(
                  margin: EdgeInsets.only(
                    top: (42 / 720) * screenHeight,
                  ),
                  child: Row(
                    //crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      InkWell(
                        onTap: () {
                          Navigator.pop(context);
                        },
                        child: Container(
                          width: (58 / 360) * screenWidth,
                          height: (30.0 / 720) * screenHeight,
                          margin: EdgeInsets.only(left: 0.0),
                          child: SvgPicture.asset(
                            getPrepareSvgImages.backIcon,
                            height: (5 / 720) * screenHeight,
                            width: (14 / 360) * screenWidth,
                            fit: BoxFit.none,
                          ),
                        ),
                      ),
                      Container(
                        height: (20 / 720) * screenHeight,
                        // margin: EdgeInsets.only(left: (20 / 360) * screenWidth),
                        child: Text(
                          "Your Posts",
                          style: TextStyle(
                            color: NeutralColors.black,
                            fontSize: (16 / 720) * screenHeight,
                            fontFamily: "IBMPlexSans",
                            fontWeight: FontWeight.w700,
                          ),
                          textAlign: TextAlign.left,
                        ),
                      ),
                    ],
                  ),
                ),
                Expanded(
                  child: ListView.separated(
                    itemCount: 4,
                    padding: EdgeInsets.only(bottom: (40)),
                    itemBuilder: (context, index) {
                      return Container(
                        margin: EdgeInsets.only(
                          left: (20 / 360) * screenWidth,
                          top: (24.5 / Constant.defaultScreenHeight) *
                              screenHeight,
                          right: (0 / Constant.defaultScreenWidth) *
                              screenWidth,
                        ),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            /**profile image and name colum**/
                            Container(
                              // color: Colors.pink,
                              child: Row(
                                mainAxisAlignment:
                                MainAxisAlignment.spaceBetween,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Container(
//                                    margin: EdgeInsets.only(
//                                        right: (20 / 360) * screenWidth),
                                    child: Row(
                                      crossAxisAlignment:
                                      CrossAxisAlignment.start,
                                      mainAxisAlignment: MainAxisAlignment.start,
                                      children: <Widget>[
                                        /**Profile pic**/
                                        Container(
                                          height: (40 /
                                              Constant.defaultScreenHeight) *
                                              screenHeight,
                                          width:
                                          (40 / Constant.defaultScreenWidth) *
                                              screenWidth,
                                          margin: EdgeInsets.only(
                                              top: (0 / 720) * screenHeight),
                                          decoration: new BoxDecoration(
                                              shape: BoxShape.circle,
                                              image: new DecorationImage(
                                                  fit: BoxFit.cover,
                                                  image: AssetImage(
                                                      'assets/images/oval-2.png'))),
                                        ),
                                        Container(
                                          child: Column(
                                            children: <Widget>[
                                              Container(
                                                height: (18 /
                                                    Constant
                                                        .defaultScreenHeight) *
                                                    screenHeight,
                                                width: (100 / 360) * screenWidth,
                                                margin: EdgeInsets.only(
                                                    top: (0 /
                                                        Constant
                                                            .defaultScreenHeight) *
                                                        screenHeight,
                                                    left:
                                                    (12 / 360) * screenWidth),
                                                child: Text(
                                                  SubQueriesCommentsScreenStrings
                                                      .Text_UserName,
                                                  style: TextStyle(
                                                    color: NeutralColors
                                                        .dark_navy_blue,
                                                    fontWeight: FontWeight.w500,
                                                    fontFamily: "IBMPlexSans",
                                                    fontStyle: FontStyle.normal,
                                                    fontSize: (14 /
                                                        Constant
                                                            .defaultScreenWidth) *
                                                        screenWidth,
                                                  ),
                                                  textAlign: TextAlign.start,
                                                ),
                                              ),
                                              Container(
                                                height: (18 /
                                                    Constant
                                                        .defaultScreenHeight) *
                                                    screenHeight,
                                                width: (100 / 360) * screenWidth,
                                                margin: EdgeInsets.only(
                                                    top: (5 /
                                                        Constant
                                                            .defaultScreenHeight) *
                                                        screenHeight,
                                                    left:
                                                    (12 / 360) * screenWidth),
                                                child: Text(
                                                  SubQueriesCommentsScreenStrings
                                                      .Text_MsgdTime,
                                                  style: TextStyle(
                                                    color:
                                                    NeutralColors.blue_grey,
                                                    fontWeight: FontWeight.w500,
                                                    fontFamily: "IBMPlexSans",
                                                    fontStyle: FontStyle.normal,
                                                    fontSize: (12 /
                                                        Constant
                                                            .defaultScreenWidth) *
                                                        screenWidth,
                                                  ),
                                                  textAlign: TextAlign.start,
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  Align(
                                    alignment: Alignment.topRight,
                                    child: Container(
                                      //width: 10/360 * screenWidth,
                                      //height: 11/720 * screenHeight,
                                      margin:EdgeInsets.only(left: 115/360 * screenWidth,top: 5/720 * screenHeight,right: 0/360 * screenWidth),
                                      child: GestureDetector(
                                        onTap: (){
                                          setState(() {
                                            if(bookmark.contains(index)){
                                              bookmark.remove(index);
                                            }
                                            else{
                                              bookmark.add(index);
                                            }
                                            bookmarkSelected = !bookmarkSelected;
                                          });
                                        },
                                        child:Container(
                                          //width: 10/360 * screenWidth,
                                          //height: 11/720 * screenHeight,
                                            child: bookmark.contains(index)?
                                            SvgPicture.asset(
                                                getPrepareSvgImages.bookmarkActive
                                            ):
                                            Image.asset(
                                              'assets/images/icon-bookmark2.png',
                                            )
                                        ),
                                      ),
                                    ),
                                  ),
                                  Align(
                                    alignment: Alignment.topRight,
//                                    margin: EdgeInsets.only(
//                                      right: 0,
//                                      top: 0,
//                                    ),
                                    child: GestureDetector(
                                      onTap: () {
                                        // This is a hack because _PopupMenuButtonState is private.
                                        dynamic state = _menuKey.currentState;
                                        state.showButtonMenu();
                                      },
                                      child: Container(
                                        // color: Colors.red,
                                        margin: EdgeInsets.only(top: 0, right: 0),
                                        height: 25/720 * screenHeight,
                                        child: PopupMenuButton(
                                            icon: Icon(Icons.more_vert,color: NeutralColors.blue_grey),
                                            padding: EdgeInsets.all(0),
                                            // key: _menuKey,
                                            itemBuilder: (_) =>
                                            <PopupMenuItem<String>>[
                                              new PopupMenuItem<String>(
                                                  child: const Text(
                                                    'Report',
                                                    style: TextStyle(
                                                      color: NeutralColors
                                                          .gunmetal,
                                                      fontFamily:
                                                      "IBMPlexSans",
                                                      fontWeight:
                                                      FontWeight
                                                          .normal,
                                                      fontSize: 14,
                                                    ),
                                                  ),
                                                  value: 'Report'),
                                            ],
                                            onSelected: (_) {}),

                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),

                            Container(
                              //height: 46/720 * screenHeight,
                              width: 320/360 * screenHeight,
                              margin: EdgeInsets.only( top: 13/720 * screenHeight, right: 20/360 * screenWidth),
                              child: Text(
                                'What should be my required percentile to get into a IIM A, B, C in CAT 2019.',
                                style: TextStyle(
                                  color: NeutralColors
                                      .dark_navy_blue,
                                  fontFamily:
                                  "IBMPlexSans",
                                  height: 1,
                                  fontWeight: FontWeight.w400,
                                  fontSize: 14/720 * screenHeight,
                                ),
                              ),
                            ),
                            /**Extended Paragraph colum**/

                            Container(
                              margin: EdgeInsets.only(
                                  top: (10 / Constant.defaultScreenHeight) *
                                      screenHeight,
                                  right: (20 / 360) * screenWidth),
                              child: SingleChildScrollView(
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Padding(
                                      padding: const EdgeInsets.all(1.0),
                                      child: ReadMoreText(
                                        SubQueriesCommentsScreenStrings
                                            .ReadMoreText_Content,
                                        trimLines: 3,
                                        colorClickableText: Colors.blue,
                                        trimMode: TrimMode.Line,
                                        trimCollapsedText:
                                        SubQueriesCommentsScreenStrings
                                            .Text_ReadMore,
                                        trimExpandedText:
                                        SubQueriesCommentsScreenStrings
                                            .Text_ReadLess,
                                        style: TextStyle(
                                          color: NeutralColors.gunmetal,
                                          fontWeight: FontWeight.w500,
                                          fontFamily: "IBMPlexSans",
                                          fontStyle: FontStyle.normal,
                                          height: 1.5,
                                          fontSize: (12/720)*screenHeight,
                                        ),
                                      ),
                                    )
                                  ],
                                ),
                              ),
                            ),
                            /**upvotes and reply colum**/
                            Container(
                              margin:EdgeInsets.only(bottom: 24/720 * screenHeight),
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: <Widget>[
                                  Container(
                                    margin: EdgeInsets.only(
                                        top: (26 /
                                            Constant.defaultScreenHeight) *
                                            screenHeight),
                                    child: InkWell(
                                      onTap: () {
                                      },
                                      child: Container(
                                        height: 11/720 * screenHeight,
                                        width: 11/360 * screenWidth,
                                        child: SvgPicture.asset(
                                          getSvgIcon.like,
                                          fit: BoxFit.contain,
                                        ),
                                      ),
                                    ),
                                  ),
                                  Container(
                                      margin: EdgeInsets.only(
                                          top: 18 /
                                              Constant.defaultScreenHeight *
                                              screenHeight,
                                          left: 5 / 360 * screenWidth),
                                      decoration: BoxDecoration(
                                        color: Colors.transparent,
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(2)),
                                      ),
                                      child: Container(
                                        margin: EdgeInsets.only(
                                            top: 6 /
                                                Constant.defaultScreenHeight *
                                                screenHeight),
                                        child: Text(
                                          // SubQueriesCommentsScreenStrings.Text_Liked,
                                          '324 Likes',
                                          style: TextStyle(
                                            color:  NeutralColors.dark_navy_blue,
                                            fontWeight: FontWeight.normal,
                                            fontFamily: "IBMPlexSans",
                                            fontStyle: FontStyle.normal,
                                            fontSize: (12 /
                                                Constant
                                                    .defaultScreenWidth) *
                                                screenWidth,
                                          ),
                                          textAlign: TextAlign.left,
                                        ),
                                      )),
                                  Container(
                                    margin: EdgeInsets.only(
                                        top: (26 /
                                            Constant.defaultScreenHeight) *
                                            screenHeight,
                                        left:
                                        (20 / Constant.defaultScreenWidth) *
                                            screenWidth),
                                    child: Icon(
                                      Icons.chat_bubble_outline,
                                      color: NeutralColors.black,
                                      size: 11 / 360 * screenWidth,
                                    ),
                                  ),
                                  Container(
                                      margin: EdgeInsets.only(
                                          top: 15 /
                                              Constant.defaultScreenHeight *
                                              screenHeight,
                                          left: 5 / 360 * screenWidth),
                                      decoration: BoxDecoration(
                                        color: Colors.transparent,
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(2)),
                                      ),
                                      child: Container(
                                        margin: EdgeInsets.only(
                                            top: 6 /
                                                Constant.defaultScreenHeight *
                                                screenHeight),
                                        child: Text(
                                          '232 Comments',
                                          style: TextStyle(
                                            color: NeutralColors.dark_navy_blue,
                                            fontWeight: FontWeight.normal,
                                            fontFamily: "IBMPlexSans",
                                            fontStyle: FontStyle.normal,
                                            fontSize: (12 /
                                                Constant
                                                    .defaultScreenWidth) *
                                                screenWidth,
                                          ),
                                          textAlign: TextAlign.left,
                                        ),
                                      )),
                                ],
                              ),
                            ),
                            /**showreplies colum**/
//                            Container(
//                                width: double.infinity,
//                                margin: EdgeInsets.only(
//                                  top: 5 /
//                                      Constant.defaultScreenHeight *
//                                      screenHeight,
//                                  bottom: 27 /
//                                      Constant.defaultScreenHeight *
//                                      screenHeight,
//                                ),
//                                decoration: BoxDecoration(
//                                  //  color:Colors.pink,
//                                  borderRadius:
//                                  BorderRadius.all(Radius.circular(2)),
//                                ),
//                                child: Container(
//                                  margin: EdgeInsets.only(
//                                      top: 6 /
//                                          Constant.defaultScreenHeight *
//                                          screenHeight),
//                                  child: Text(
//                                    SubQueriesCommentsScreenStrings
//                                        .Text_ShowReplies,
//                                    style: TextStyle(
//                                      color: NeutralColors.blue_grey,
//                                      fontWeight: FontWeight.normal,
//                                      fontFamily: "IBMPlexSans",
//                                      fontStyle: FontStyle.normal,
//                                      fontSize:
//                                      (12 / Constant.defaultScreenWidth) *
//                                          screenWidth,
//                                    ),
//                                    textAlign: TextAlign.left,
//                                  ),
//                                )),
                          ],
                        ),
                      );
                    },
                    separatorBuilder: (context, index) {
                      return Padding(
                        padding: EdgeInsets.only(left: 20/360 * screenWidth, right: 20/360 * screenWidth),
                        child: Divider(),
                      );
                    },
                  ),
                ),
              ],
            ),
          ),
        );

  }
}

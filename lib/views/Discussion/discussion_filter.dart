import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:imsindia/components/navigation_bar.dart';
import 'package:imsindia/routers/routes.dart';
import 'package:imsindia/utils/colors.dart';
import 'package:imsindia/utils/screen_util.dart';
import 'package:imsindia/utils/svg_images/channel_svg_icons.dart';
import 'package:imsindia/views/Discussion/discussion_home.dart';
import 'package:shared_preferences/shared_preferences.dart';




var screenWidthTotal;
var screenHeightTotal;

var _safeAreaHorizontal;

var _safeAreaVertical;

var screenHeight;

var screenWidth;
bool general = true;
bool recent = false;
bool popular = true;

List<String> _trendingDiscussion = <String>[
  '#simCAT102',
  '#XATResults',
  '#simCAT05Results',
  '#QASectional',
  '#CAT2019',
  '#CAT',
  '#simCAT102',
  '#XATResults',
  '#simCAT05Results',
  '#QASectional',
  '#CAT2019',
  '#CAT',
  '#simCAT102',
  '#XATResults',
  '#simCAT05Results',
];



class DiscussionFilter extends StatefulWidget {
  @override
  DiscussionFilterState createState() => DiscussionFilterState();
}

class DiscussionFilterState extends State<DiscussionFilter>{
  List<int> _selectedIndexList = List();
  bool selectionMode = false;
  void _changeSelection({bool selectionMode, int index}) {
   // _selectionMode = enable;
    if (_selectedIndexList.contains(index)) {
      _selectedIndexList.remove(index);
    } else if (index == -1){
      _selectedIndexList.clear();
    } else{
      _selectedIndexList.add(index);
    }
  }


  @override
  void initState(){
    super.initState();
  }
  void dispose(){
    super.dispose();
    setFilterFalse();
  }
  void setFilterTrue() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setBool("FilterClicked", true);
    print("hii");
  }

  void setFilterFalse() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setBool("FilterClicked", false);
  }

  checkFilterDetails() async {
    setState(() {
      setFilterTrue();
    });
  }
  Future<bool> _onWillPop() {
    //back
    AppRoutes.push(context, DiscussionHome());
  }
  Widget build(BuildContext context) {
    var _mediaQueryData = MediaQuery.of(context);
    screenWidthTotal = _mediaQueryData.size.width;
    screenHeightTotal = _mediaQueryData.size.height;
    _safeAreaHorizontal =
        _mediaQueryData.padding.left + _mediaQueryData.padding.right;
    _safeAreaVertical =
        _mediaQueryData.padding.top + _mediaQueryData.padding.bottom;

    screenHeight = screenHeightTotal - _safeAreaVertical;
    screenWidth = screenWidthTotal - _safeAreaHorizontal;
    void _modalBottomSheetMenu() {
      showModalBottomSheet(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.only(topRight: Radius.circular(20),topLeft: Radius.circular(20)),
          ),
          context: context,
          builder: (builder) {
            return new Container(
              height: 120.0,
              color: Colors.transparent, //could change this to Color(0xFF737373),
              child: new Center(
                child:Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    GestureDetector(
                      onTap: () {
                        _changeSelection(selectionMode: false, index: -1);
                        setState(() {

                        });
                        Navigator.of(context).pop();

                      //AppRoutes.push(context, Channel_quicktips());

                      },
                      child: Container(
                        height:(40/Constant.defaultScreenHeight)*screenHeight,
                        width: (130/Constant.defaultScreenWidth)*screenWidth,
                        decoration: BoxDecoration(
                          color: NeutralColors.ice_blue,
                          borderRadius: BorderRadius.all(Radius.circular(2)),
                        ),
                        child: Center(
                          child: Text(
                            "Reset",
                            style: TextStyle(
                              fontWeight:FontWeight.w500,
                              fontFamily: "IBMPlexSans",
                              color: NeutralColors.blue_grey,
                            ),
                          ),
                        ),
                      ),
                    ),
                    GestureDetector(
                      onTap: () {
                        //Navigator.of(context).pop();
                        checkFilterDetails();
                        var count = 0;
                        Navigator.of(context).popUntil((_) => count ++ >= 2);
                      },
                      child: Container(
                        margin: EdgeInsets.only(left: (15/Constant.defaultScreenWidth)*screenWidth),
                        height:(40/Constant.defaultScreenHeight)*screenHeight,
                        width: (130/Constant.defaultScreenWidth)*screenWidth,
                        decoration: BoxDecoration(
                          color: PrimaryColors.azure_Dark,
                          borderRadius: BorderRadius.all(Radius.circular(2)),
                        ),
                        child: Center(
                          child: Text(
                            "Done",
                            style: TextStyle(
                              fontWeight:FontWeight.w500,
                              fontFamily: "IBMPlexSans",
                              color: Colors.white,
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            );
          }
      );
    }
    return  Scaffold(
        body: Container(
          color: Colors.white,
          child: Column(
            children: <Widget>[
              Align(
                alignment: Alignment.topLeft,
                child: Container(
                  margin: EdgeInsets.only(left: 20/360 * screenWidth, top: 41/720 * screenHeight),
                  child: Text(
                    'Sort by',
                    style: TextStyle(
                      color: NeutralColors.dark_navy_blue,
                      fontSize: 14/720 * screenHeight,
                      fontFamily: 'IBMPlexSans',
                      fontWeight: FontWeight.w500
                    ),
                  ),
                ),
              ),
              Align(
                alignment: Alignment.topLeft,
                child: Container(
                  width: 320/360 * screenWidth,
                  height: 40/720 * screenHeight,
                  margin: EdgeInsets.only(left: 20/360 * screenWidth, top: 15/720 * screenHeight),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      InkWell(
                        onTap: () {
                          setState(() {
                            recent = !recent;
                            popular = !popular;
                          }
                          );
                          _modalBottomSheetMenu();
                        },
                        child: Container(
                          width: 78 / 360 * screenWidth,
                          height: 40 / 720 * screenHeight,
                          decoration: BoxDecoration(
                            color: recent
                                ? Colors.transparent
                                : NeutralColors.purpleish_blue,
                            border: Border.all(
                              color: recent
                                  ? Color.fromARGB(255, 242, 244, 244)
                                  : Colors.transparent,
                              width: 1,
                            ),
                            borderRadius:
                            BorderRadius.all(Radius.circular(20)),
                          ),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment:
                            CrossAxisAlignment.stretch,
                            children: [
                              Container(
                                margin:
                                EdgeInsets.symmetric(horizontal: 15),
                                child: Text(
                                  'Recent',
                                  style: recent
                                      ? TextStyle(
                                    color: Color(0xFF646464),
                                    fontSize:
                                    14 / 720 * screenHeight,
                                    fontFamily:
                                    "IBMPlexSans",
                                    fontWeight: FontWeight.w500,
                                  )
                                      : TextStyle(
                                    color: Color(0xFFffffff),
                                    fontSize:
                                    14 / 720 * screenHeight,
                                    fontFamily:
                                    "IBMPlexSans",
                                    fontWeight: FontWeight.w500,
                                  ),
                                  textAlign: TextAlign.center,
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                      InkWell(
                        onTap: () {
                          setState(() {
                            popular = !popular;
                            recent = !recent;
                          }
                          );
                          _modalBottomSheetMenu();
                        },
                        child: Container(
                          width: 82 / 360 * screenWidth,
                          height: 40 / 720 * screenHeight,
                          margin: EdgeInsets.only(left: 10/360 * screenHeight),
                          decoration: BoxDecoration(
                            color: popular
                                ? Colors.transparent
                                : NeutralColors.purpleish_blue,
                            border: Border.all(
                              color: popular
                                  ? Color.fromARGB(255, 242, 244, 244)
                                  : Colors.transparent,
                              width: 1,
                            ),
                            borderRadius:
                            BorderRadius.all(Radius.circular(20)),
                          ),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment:
                            CrossAxisAlignment.stretch,
                            children: [
                              Container(
                                margin:
                                EdgeInsets.symmetric(horizontal: 15),
                                child: Text(
                                  'Popular',
                                  style: popular
                                      ? TextStyle(
                                    color: Color(0xFF646464),
                                    fontSize:
                                    14 / 720 * screenHeight,
                                    fontFamily:
                                    "IBMPlexSans",
                                    fontWeight: FontWeight.w500,
                                  )
                                      : TextStyle(
                                    color: Color(0xFFffffff),
                                    fontSize:
                                    14 / 720 * screenHeight,
                                    fontFamily:
                                    "IBMPlexSans",
                                    fontWeight: FontWeight.w500,
                                  ),
                                  textAlign: TextAlign.center,
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              Align(
                alignment: Alignment.topLeft,
                child: Container(
                  margin: EdgeInsets.only(left: 20/360 * screenWidth, top: 40/720 * screenHeight),
                  child: Text(
                    'Trending Discussions',
                    style: TextStyle(
                        color: NeutralColors.dark_navy_blue,
                        fontSize: 14/720 * screenHeight,
                        fontFamily: 'IBMPlexSans',
                        fontWeight: FontWeight.w500
                    ),
                  ),
                ),
              ),
              Expanded(
                child: Container(
                  margin: EdgeInsets.only(right: 20/720 * screenWidth),
                  child: new GridView.builder(
                      //physics: NeverScrollableScrollPhysics(),
                      itemCount: _trendingDiscussion.length,
                      gridDelegate:
                      new SliverGridDelegateWithFixedCrossAxisCount(
                        crossAxisCount: 2,
                        childAspectRatio: 15 / (10 / 2),
                      ),
                      itemBuilder: (BuildContext context, int index) {
                        final item = _trendingDiscussion[index];
                        return Container(
                          margin: EdgeInsets.only(left: 10/360 * screenWidth,bottom: 10/720 * screenHeight),
                          child: InkWell(
                            onTap: ()  {
                              setState((){
                                       _changeSelection(selectionMode: true, index: index);
                              });
                              _modalBottomSheetMenu();
                            },
                            child: Flex(
                                direction: Axis.horizontal,
                                mainAxisAlignment: MainAxisAlignment.start,
                              mainAxisSize: MainAxisSize.min,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Container(
                                  //width: double.infinity,
                                  height: 40 / 720 * screenHeight,
                                  padding: EdgeInsets.only(left: 15/360 * screenWidth, top: 11/720 * screenHeight, bottom: 11/720 * screenHeight, right: 15/360 * screenWidth),
                                  constraints: BoxConstraints(
                                    maxWidth: screenWidth * 152/360,
                                  ),
                                  decoration: BoxDecoration(
                                    color: _selectedIndexList.contains(index)
                                        ? NeutralColors.purpleish_blue
                                        : Colors.transparent,
                                    border: Border.all(
                                      color: _selectedIndexList.contains(index)
                                          ? Colors.transparent
                                          : Color.fromARGB(255, 242, 244, 244),
                                      width: 1,
                                    ),
                                    borderRadius:
                                    BorderRadius.all(Radius.circular(20)),
                                  ),
                                  child: Text(
                                    _trendingDiscussion[index],
                                    style: _selectedIndexList.contains(index)
                                        ? TextStyle(
                                      color: Color(0xFFffffff),
                                      fontSize:
                                      14 / 720 * screenHeight,
                                      fontFamily:
                                      "IBMPlexSans",
                                      fontWeight: FontWeight.w500,
                                    ):TextStyle(
                                      color: Color(0xFF646464),
                                      fontSize:
                                      14 / 720 * screenHeight,
                                      fontFamily:
                                      "IBMPlexSans",
                                      fontWeight: FontWeight.w500,
                                    ),
                                    textAlign: TextAlign.center,
                                  ),
                              ),
                            ]
                            ),
                          ),
                        );
                    }
               ),
                ),
              ),
            ],
          ),
        ),
      );

  }
}
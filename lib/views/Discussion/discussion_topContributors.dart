import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:imsindia/components/navigation_bar.dart';
import 'package:imsindia/routers/routes.dart';
import 'package:imsindia/utils/colors.dart';
import 'package:imsindia/utils/svg_images/channel_svg_icons.dart';
import 'package:imsindia/utils/svg_images/prepare_svg_images.dart';
import 'package:imsindia/views/Discussion/discussion_home.dart';




var screenWidthTotal;
var screenHeightTotal;

var _safeAreaHorizontal;

var _safeAreaVertical;

var screenHeight;

var screenWidth;

class Data {
  String name;
  String city;
  String contributionNo;
  String contribution;

  Data({
    this.name,
    this.city,
    this.contributionNo,
    this.contribution,
  });
}



class DiscussionTopContributors extends StatefulWidget {
  @override
  DiscussionTopContributorsState createState() => DiscussionTopContributorsState();
}

class DiscussionTopContributorsState extends State<DiscussionTopContributors>{


  @override
  Future<bool> _onWillPop() {
    //back
    AppRoutes.push(context, DiscussionHome());
  }
  Widget build(BuildContext context) {
    var _mediaQueryData = MediaQuery.of(context);
    screenWidthTotal = _mediaQueryData.size.width;
    screenHeightTotal = _mediaQueryData.size.height;
    _safeAreaHorizontal =
        _mediaQueryData.padding.left + _mediaQueryData.padding.right;
    _safeAreaVertical =
        _mediaQueryData.padding.top + _mediaQueryData.padding.bottom;

    screenHeight = screenHeightTotal - _safeAreaVertical;
    screenWidth = screenWidthTotal - _safeAreaHorizontal;

    final data1 = Data(
      name: "Juan Gregory",
      city: "Mumbai",
      contributionNo: "89",
      contribution: "Contributions",
    );


    final List<Data> datas = [
      data1,
    ];


    return  Scaffold(
        body: Container(
          child: Column(
            children: <Widget>[
              Container(
                margin: EdgeInsets.only(
                  top: (42 / 720) * screenHeight,
                ),
                child: Row(
                  //crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    InkWell(
                      onTap: () {
                        Navigator.pop(context);
                      },
                      child: Container(
                        width: (58 / 360) * screenWidth,
                        height: (30.0 / 720) * screenHeight,
                        margin: EdgeInsets.only(left: 0.0),
                        child: SvgPicture.asset(
                          getPrepareSvgImages.backIcon,
                          height: (5 / 720) * screenHeight,
                          width: (14 / 360) * screenWidth,
                          fit: BoxFit.none,
                        ),
                      ),
                    ),
                    Container(
                      height: (20 / 720) * screenHeight,
                      // margin: EdgeInsets.only(left: (20 / 360) * screenWidth),
                      child: Text(
                        "Top Contributors",
                        style: TextStyle(
                          color: NeutralColors.black,
                          fontSize: (16 / 720) * screenHeight,
                          fontFamily: "IBMPlexSans",
                          fontWeight: FontWeight.w500,
                        ),
                        textAlign: TextAlign.left,
                      ),
                    ),
                  ],
                ),
              ),
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    Expanded(
                      child: ListView.builder(
                        addRepaintBoundaries: true,
                        addAutomaticKeepAlives: true,
                        itemCount: 10,
                        itemBuilder: (context, index) {
                          //final item = datas[index];
                          final listNo = index + 1 ;
                          Color color;
                          if (index % 2 == 0) {
                            color = NeutralColors.ice_blue;
                          } else {
                            color = Colors.white;
                          }
                          return Container(
                            height: 71/720 * screenHeight,
                            //margin: EdgeInsets.only(top: 30/720 * screenHeight),
                            child: Stack(
                              alignment: Alignment.center,
                              children: [
                                Container(
                                    height: 71/720 * screenHeight,
                                    decoration: BoxDecoration(
                                      color: color,
                                    ),
                                    child: Container(),
                                  ),
                                Container(
                                    width: 324/360 * screenHeight,
                                    height: 43/720 * screenHeight,
                                    margin: EdgeInsets.only(left: 20/360 * screenWidth, top: 13/720 * screenHeight,),
                                    child: Row(
                                      crossAxisAlignment: CrossAxisAlignment.stretch,
                                      children: [
                                        Align(
                                          alignment: Alignment.topLeft,
                                          child: Container(
                                            margin: EdgeInsets.only(top: 2/720 * screenHeight, right: 2/360 * screenWidth),
                                            child: Text(
                                              listNo.toString() + ".",
                                              style: TextStyle(
                                                color: NeutralColors.dark_navy_blue,
                                                fontSize: 14/720 * screenHeight,
                                                fontFamily: "IBMPlexSans",
                                                fontWeight: FontWeight.w500,
                                              ),
                                              textAlign: TextAlign.left,
                                            ),
                                          ),
                                        ),
                                        Align(
                                          alignment: Alignment.topLeft,
                                          child: Container(
                                            //width: 138/360 * screenWidth,
                                            height: 41/720 * screenHeight,
                                            margin: EdgeInsets.only(left: 4/360 * screenWidth, top: 0/720 * screenHeight),
                                            child: Row(
                                              crossAxisAlignment: CrossAxisAlignment.start,
                                              children: [
                                                Container(
                                                  width: 41/360 * screenWidth,
                                                  height: 41/720 * screenHeight,
                                                  margin: EdgeInsets.only(top: 1/720 * screenHeight),
                                                  decoration: new BoxDecoration(
                                                      shape: BoxShape.circle,
                                                      image: new DecorationImage(
                                                          fit: BoxFit.cover,
                                                          image: AssetImage(
                                                              'assets/images/group-10-4.png'))),
//                                                child: Image.asset(
//                                                  "assets/images/group-10-4.png",
//                                                  fit: BoxFit.none,
//                                                ),
                                                ),
                                                Container(
                                                  width: 150/360 * screenWidth,
                                                  height: 41/720 * screenHeight,
                                                  margin: EdgeInsets.only(left: 9/360 * screenHeight),
                                                  child: Column(
                                                    crossAxisAlignment: CrossAxisAlignment.start,
                                                    children: [
                                                      Text(
                                                        "Juan Gregory",
                                                        style: TextStyle(
                                                          color: NeutralColors.dark_navy_blue,
                                                          fontSize: 14/720 * screenHeight,
                                                          fontFamily: "IBMPlexSans",
                                                          fontWeight: FontWeight.w500,
                                                        ),
                                                        textAlign: TextAlign.left,
                                                      ),
                                                      Container(
                                                        margin: EdgeInsets.only(top: 5/720 * screenHeight),
                                                        child: Text(
                                                          "Mumbai",
                                                          style: TextStyle(
                                                            color: NeutralColors.blue_grey,
                                                            fontSize: 12/720 * screenHeight,
                                                            fontFamily: "IBMPlexSans",
                                                            fontWeight: FontWeight.w500,
                                                          ),
                                                          textAlign: TextAlign.left,
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                        ),
                                        Align(
                                          alignment: Alignment.topLeft,
                                          child: Container(
                                            width: 88/360 * screenWidth,
                                            height: 41/720 * screenHeight,
                                            margin: EdgeInsets.only(left: 10/360 * screenWidth, top: 2/720 * screenHeight),
                                            child: Column(
                                              crossAxisAlignment: CrossAxisAlignment.start,
                                              children: [
                                                Text(
                                                  "89",
                                                  style: TextStyle(
                                                    color: NeutralColors.dark_navy_blue,
                                                    fontSize: 14/720 * screenHeight,
                                                    fontFamily: "IBMPlexSans",
                                                    fontWeight: FontWeight.w500,
                                                  ),
                                                  textAlign: TextAlign.left,
                                                ),
                                                Container(
                                                  margin: EdgeInsets.only(top: 5/720 * screenHeight),
                                                  child: Text(
                                                    "Contributions",
                                                    style: TextStyle(
                                                      color: NeutralColors.blue_grey,
                                                      fontSize: 12/720 * screenHeight,
                                                      fontFamily: "IBMPlexSans",
                                                      fontWeight: FontWeight.w500,
                                                    ),
                                                    textAlign: TextAlign.left,
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                              ],
                            ),
                          );
                        },
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      );

  }
}
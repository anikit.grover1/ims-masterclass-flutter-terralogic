import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:imsindia/utils/home_svg_icons.dart';
import 'package:imsindia/components/navigation_bar.dart';
import 'package:imsindia/routers/routes.dart';
import 'package:imsindia/utils/colors.dart';
import 'package:imsindia/utils/svg_images/channel_svg_icons.dart';
import 'package:imsindia/views/Arun/search/search_widget.dart' as prefix0;
import 'package:imsindia/views/Arun/search/search_widget.dart';
import 'package:imsindia/views/Discussion/discussion_community.dart';
import 'package:imsindia/views/Discussion/discussion_filter.dart';
import 'package:imsindia/views/Discussion/discussion_general.dart';
import 'package:imsindia/views/Discussion/discussion_new_post.dart';
import 'package:imsindia/views/Discussion/discussion_test.dart';
import 'package:imsindia/views/Discussion/discussion_topContributors.dart';
import 'package:imsindia/views/Discussion/discussion_topic.dart';
import 'package:imsindia/views/Discussion/discussion_your_posts.dart';
import 'package:imsindia/views/channel/channel_comments.dart';
import 'package:imsindia/views/home_pages/home_widget.dart';
import 'package:shared_preferences/shared_preferences.dart';




var screenWidthTotal;
var screenHeightTotal;

var _safeAreaHorizontal;

var _safeAreaVertical;

var screenHeight;

var screenWidth;
var showSearch = false;
TextEditingController controller = TextEditingController();





class DiscussionHome extends StatefulWidget {
  @override
  DiscussionHomeState createState() => DiscussionHomeState();
}

class DiscussionHomeState extends State<DiscussionHome> with TickerProviderStateMixin {
  TabController _tabController;

  List<Tab> tabList = [
    new Tab(
      child: Text(
        "General",
      ),
    ),
    new Tab(
      child: Text(
        "Topic",
      ),
    ),
    new Tab(
      child: Text(
        "Test",
      ),
    ),
    new Tab(
      child: Text(
        "Community",
      ),
    )
  ];
  bool showFilter = false;

  @override
  void initState() {
    _tabController = new TabController(vsync: this, length: tabList.length);
    super.initState();
    controller.text = "";
    //setFilterFalse();
  }



  @override
  Future<bool> _onWillPop() {
    //back
    AppRoutes.push(context, HomeWidget());
  }
  Widget build(BuildContext context) {
    var _mediaQueryData = MediaQuery.of(context);
    screenWidthTotal = _mediaQueryData.size.width;
    screenHeightTotal = _mediaQueryData.size.height;
    _safeAreaHorizontal =
        _mediaQueryData.padding.left + _mediaQueryData.padding.right;
    _safeAreaVertical =
        _mediaQueryData.padding.top + _mediaQueryData.padding.bottom;

    screenHeight = screenHeightTotal - _safeAreaVertical;
    screenWidth = screenWidthTotal - _safeAreaHorizontal;

    return Scaffold(
        

        appBar: showSearch == false?
        new AppBar(
          brightness: Brightness.light,
          elevation: 0.0,
          centerTitle: false,
          iconTheme: IconThemeData(
            color: Colors.black, //change your color here
          ),
          title: Text(
            "Discussions",
            style: TextStyle(
              color: Color.fromARGB(255, 0, 0, 0),
              fontSize: 16 / 360 * screenWidth,
              fontFamily: "IBMPlexSans",
              fontWeight: FontWeight.w500,
            ),
          ),
          titleSpacing: 0.0,
          actions: <Widget>[
            Container(
              // width:0.130*screenWidth,
                margin: EdgeInsets.only(right: 20 / 360  * screenWidth),
                child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Container(
                        child: InkWell(
                          onTap: () {
                            print('navi');
                            Navigator.push(
                              context,
                              MaterialPageRoute(builder: (context) => Search()),
                            );
                          },
                          child: Container(
                            height: 20 / 720 * screenHeight,
                            width: 20 / 360 * screenWidth,
                            child: new SvgPicture.asset(
                              getSvgIcon.search,
                              fit: BoxFit.contain,
                              //color: Colors.red,
                            ),
                          ),
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.only(left: 18 / 360 * screenWidth),
                        child: InkWell(
                          onTap: () {
                            AppRoutes.push(context, DiscussionFilter());
                          },
                          child: Container(
                            height: 20 / 720 * screenHeight,
                            width: 20 / 360 * screenWidth,
                            child: new SvgPicture.asset(
                              getSvgIcon.filter,
                              fit: BoxFit.contain,
                              color: Colors.black,
                            ),
                          ),
                        ),
                      ),
                    ])),
          ],
          backgroundColor: Color.fromARGB(255, 255, 255, 255),
        ):
//        AppBar(
//          brightness: Brightness.light,
//          elevation: 0.0,
//          centerTitle: false,
//          titleSpacing: 0.0,
//          backgroundColor: Colors.white,
//          iconTheme: IconThemeData(
//            color: Colors.black, //change your color here
//          ),
//          title: Row(
//            children: <Widget>[
//              Text(
//                "Discussions",
//                style: TextStyle(
//                    color: Colors.black,
//                    fontFamily: "IBMPlexSans",
//                    fontWeight: FontWeight.w500,
//                    fontSize: 16.0),
//              ),
//            ],
//          ),
//          actions: <Widget>[
//            Align(
//              alignment: Alignment.centerRight,
//              child: Container(
//                margin:
//                EdgeInsets.only(right: 10/360 * screenHeight),
//                height: (16 / 720) * screenHeight,
//                width: (16 / 360) * screenWidth,
//                child: GestureDetector(
//                  child: Image.asset(
//                    'assets/images/search.png',
//                    fit: BoxFit.contain,
//                  ),
//                  onTap: () {
//                    print("search");
//                    Navigator.push(
//                      context,
//                      MaterialPageRoute(builder: (context) => Search()),
//                    );
////                    setState(() {
////                      showSearch = true;
////                    });
//                  },
//                ),
//              ),
//            ),
//            Align(
//              alignment: Alignment.centerRight,
//              child: GestureDetector(
//                onTap: (){
//                  AppRoutes.push(context, DiscussionFilter());
//                },
//                child: Container(
//                  margin:
//                  EdgeInsets.only(right: 15/360 * screenHeight),
//                  height: (14 / 720) * screenHeight,
//                  width: (16 / 360) * screenWidth,
//                  child: Image.asset(
//                    "assets/images/icon-filter.png",
//                    fit: BoxFit.contain,
//                    color: Colors.black,
//                  ),
//                ),
//              ),
//            ),
//          ],
//        )

        AppBar(
        brightness: Brightness.light,
        centerTitle: false,
        titleSpacing: 0.0,
        automaticallyImplyLeading: false,
        backgroundColor: Colors.white,
        title: Row(
          children: <Widget>[
            Container(
              height: (40 / 720) * screenHeight,
              width: (250.0 / 360) * screenWidth,
              //margin: EdgeInsets.only(right: (6 / 360) * screenWidth),
              child: Center(
                child: TextField(
                  onSubmitted: (value) {

                  },
                  textAlign: TextAlign.start,
                  autofocus: true,
                  controller: controller,
                  textInputAction: TextInputAction.send,
                  autocorrect: false,
                  decoration: InputDecoration(
                    focusedBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.all(
                        Radius.circular(30),
                      ),
                      borderSide: BorderSide(
                          color: Color(0xfff2f4f4), width: 1.5),
                    ),
                    contentPadding: const EdgeInsets.all(11.0),
                    hintText: "Search Discussions",
                    alignLabelWithHint: true,
                    border: OutlineInputBorder(
                      borderSide: BorderSide(
                          color: Color(0xfff2f4f4), width: 1.5),
                      borderRadius: BorderRadius.all(
                        Radius.circular(30),
                      ),
                    ),
                    hintStyle: TextStyle(
                        fontWeight: FontWeight.normal,
                        color: Color(0xFF7e919a),
                        fontSize: 14.0),
                  ),
                  style: TextStyle(
                      color: NeutralColors.dark_navy_blue,
                      fontWeight: FontWeight.normal,
                      fontSize: 12),
                ),
              ),
            ),
          ],
        ),
        leading: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Container(
            height: (14 / 720) * screenHeight,
            width: (14 / 360) * screenWidth,
            margin: EdgeInsets.only(
                left: (16.0 / 360) * screenWidth,
                right: (0 / 360) * screenWidth),
              child: Image.asset(
                'assets/images/search.png',
                fit: BoxFit.contain,
              ),
          ),
        ),
        actions: <Widget>[
          GestureDetector(
            onTap: () {
             // Navigator.pop(context, false);
              setState(() {
                controller.text = "";
                showSearch = false;
                FocusScope.of(context)
                    .requestFocus();
              });
            },
            child: Container(
              height: (22 / 720) * screenHeight,
              width: (22 / 360) * screenWidth,
              margin: EdgeInsets.only(right: (19 / 360) * screenWidth),
              child: Image.asset(
                'assets/images/close.png',
                fit: BoxFit.contain,
              ),
            ),
          ),
        ],
      ),
//        drawer: NavigationDrawer(),
        body: Container(
          color: Colors.white,
          child: Column(
            children: <Widget>[
              Container(
                height: 80/720 * screenHeight,
                margin: EdgeInsets.only(left: 20/360 * screenWidth, top: 15/720 * screenHeight, right: 20/360 * screenWidth),
                decoration: BoxDecoration(
                  gradient: LinearGradient(
                    begin: Alignment(0, 0),
                    end: Alignment(1.049, 1.008),
                    stops: [
                      0,
                      1,
                    ],
                    colors: [
                      Color.fromARGB(255, 168, 174, 35).withOpacity(0.1),
                      Color.fromARGB(255, 201, 110, 216).withOpacity(0.1),
                    ],
                  ),
                  borderRadius: BorderRadius.all(Radius.circular(5)),
                ),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      width: 39/360 * screenWidth,
                      height: 44/720 * screenHeight,
                      margin: EdgeInsets.only(left: 20/360 * screenHeight, top: 18/720 * screenHeight),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(
                            margin: EdgeInsets.only(left: 14/360 * screenWidth),
                            child: Text(
                              "5",
                              style: TextStyle(
                                color: NeutralColors.dark_navy_blue,
                                fontSize: 20/720 * screenHeight,
                                fontFamily: "IBM Plex Sans",
                                fontWeight: FontWeight.w700,
                              ),
                              textAlign: TextAlign.center,
                            ),
                          ),
                          Align(
                            alignment: Alignment.topCenter,
                            child: Container(
                              margin: EdgeInsets.only( top: 4/720 * screenHeight),
                              child: Text(
                                "POSTS",
                                style: TextStyle(
                                  color: NeutralColors.gunmetal,
                                  fontSize: 12/720 * screenHeight,
                                  fontFamily: "IBM Plex Sans",
                                  fontWeight: FontWeight.w500,
                                ),
                                textAlign: TextAlign.center,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(left: 15/360 * screenHeight, top: 17/720 * screenHeight),
                      child: Opacity(
                        opacity: 0.15,
                        child: Container(
                          width: 1,
                          height: 48/720 * screenHeight,
                          decoration: BoxDecoration(
                            color: Color.fromARGB(255, 122, 113, 213),
                          ),
                          child: Container(),
                        ),
                      ),
                    ),
                    Container(
                      width: 66/360 * screenWidth,
                      height: 56/720 * screenHeight,
                      margin: EdgeInsets.only(left: 20/360 * screenWidth, top: 18/720 * screenHeight),
                      child: Column(
                        children: [
                          Text(
                            "5",
                            style: TextStyle(
                              color: NeutralColors.dark_navy_blue,
                              fontSize: 20/720 * screenHeight,
                              fontFamily: "IBM Plex Sans",
                              fontWeight: FontWeight.w700,
                            ),
                            textAlign: TextAlign.center,
                          ),
                          Align(
                            alignment: Alignment.topCenter,
                            child: Container(
                              margin: EdgeInsets.only(top: 4/720 * screenHeight),
                              child: Text(
                                "COMMENTS",
                                style: TextStyle(
                                  color: NeutralColors.gunmetal,
                                  fontSize: 12/720 * screenHeight,
                                  fontFamily: "IBM Plex Sans",
                                  fontWeight: FontWeight.w500,
                                ),
                                textAlign: TextAlign.center,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(left: 20/360 * screenWidth, top: 17/720 * screenHeight),
                      child: Opacity(
                        opacity: 0.15,
                        child: Container(
                          width: 1,
                          height: 48/720 * screenHeight,
                          decoration: BoxDecoration(
                            color: Color.fromARGB(255, 122, 113, 213),
                          ),
                          child: Container(),
                        ),
                      ),
                    ),
                    Container(
                      width: 71/360 * screenWidth,
                      height: 56/720 * screenHeight,
                      margin: EdgeInsets.only(left: 17/360 * screenWidth, top: 18/720 * screenHeight),
                      child: Column(
                        children: [
                          Text(
                            "25",
                            style: TextStyle(
                              color: NeutralColors.dark_navy_blue,
                              fontSize: 20/720 * screenHeight,
                              fontFamily: "IBM Plex Sans",
                              fontWeight: FontWeight.w700,
                            ),
                            textAlign: TextAlign.center,
                          ),
                          Align(
                            alignment: Alignment.topCenter,
                            child: Container(
                              margin: EdgeInsets.only(left: 1/360 * screenWidth, top: 4/720 * screenHeight),
                              child: Text(
                                "FOLLOWERS",
                                style: TextStyle(
                                  color: NeutralColors.gunmetal,
                                  fontSize: 12/720 * screenHeight,
                                  fontFamily: "IBM Plex Sans",
                                  fontWeight: FontWeight.w500,
                                ),
                                textAlign: TextAlign.center,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              GestureDetector(
                onTap: (){
                  AppRoutes.push(context, DiscussionTopContributors());
                },
                child: Container(
                  height: 30/720 * screenHeight,
                  margin: EdgeInsets.only(left: 20/360 * screenWidth, top: 10/720 * screenHeight, right: 20/360 * screenWidth),
                  decoration: BoxDecoration(
                    border: Border.all(
                      color: NeutralColors.gunmetal.withOpacity(0.1),
                      width: 1,
                    ),
                    borderRadius: BorderRadius.all(Radius.circular(5)),
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.end,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Align(
                        alignment: Alignment.topCenter,
                       // margin: EdgeInsets.only(left: 106/360 * screenWidth, bottom: 5/720 * screenHeight, right: 106/360 * screenWidth),
                        child: Padding(
                          padding: EdgeInsets.only(bottom: 5/720 * screenHeight),
                          child: Text(
                            "Top Contributors",
                            style: TextStyle(
                              color: PrimaryColors.azure_Dark,
                              fontSize: 14/720 * screenHeight,
                              fontFamily: "IBM Plex Sans",
                              fontWeight: FontWeight.w500,
                            ),
                            textAlign: TextAlign.center,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              Align(
                alignment: Alignment.topLeft,
                child: Container(
                  width: 320/360 * screenWidth,
                  height: 40/720 * screenHeight,
                  margin: EdgeInsets.only(left: 20/360 * screenWidth, top: 10/720 * screenHeight),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      GestureDetector(
                        onTap: (){
                          AppRoutes.push(context, DiscussionYourPosts());
                        },
                        child: Container(
                          width: 155/360 * screenWidth,
                          height: 40/720 * screenHeight,
                          decoration: BoxDecoration(
                            border: Border.all(
                              color: NeutralColors.gunmetal.withOpacity(0.1),
                              width: 1,
                            ),
                            borderRadius: BorderRadius.all(Radius.circular(5)),
                          ),
                          child: Row(
                            children: [
                              Container(
                                margin: EdgeInsets.only(left: 34/360 * screenWidth),
                                child: Text(
                                  "Your Posts",
                                  style: TextStyle(
                                    color: NeutralColors.dark_navy_blue,
                                    fontSize: 14/720 * screenHeight,
                                    fontFamily: "IBM Plex Sans",
                                    fontWeight: FontWeight.w500,
                                  ),
                                  textAlign: TextAlign.left,
                                ),
                              ),
                              Container(
                                width: 7/360 * screenWidth,
                                height: 11/720 * screenHeight,
                                margin: EdgeInsets.only(left: 11/360 * screenWidth),
                                child: Image.asset(
                                  "assets/images/shape-copy-3-5.png",
                                  color: Colors.black,
                                  fit: BoxFit.contain,
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                      Container(
                        width: 155/360 * screenWidth,
                        height: 40/720 * screenHeight,
                        margin: EdgeInsets.only(left: 10/360 * screenWidth),
                        decoration: BoxDecoration(
                          border: Border.all(
                            color: NeutralColors.gunmetal.withOpacity(0.1),
                            width: 1,
                          ),
                          borderRadius: BorderRadius.all(Radius.circular(5)),
                        ),
                        child: Row(
                          children: [
                            Container(
                              margin: EdgeInsets.only(left: 15/360 * screenHeight),
                              child: Text(
                                "Bookmarked",
                                style: TextStyle(
                                  color: NeutralColors.dark_navy_blue,
                                  fontSize: 14/720 * screenHeight,
                                  fontFamily: "IBM Plex Sans",
                                  fontWeight: FontWeight.w500,
                                ),
                                textAlign: TextAlign.left,
                              ),
                            ),
                            Container(
                              width: 7/360 * screenWidth,
                              height: 11/720 * screenHeight,
                              margin: EdgeInsets.only(left: 10/360 * screenWidth),
                              child: Image.asset(
                                "assets/images/shape-copy-3-5.png",
                                color: Colors.black,
                                fit: BoxFit.contain,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              Expanded(
                //width: screenWidth,
//                padding: EdgeInsets.only(
//                  // top: (326 / 764) * screenHeight,
//                  // top: 20.0,
//                ),
                child: new Column(
                  children: <Widget>[
                    new Container(
                      height: 36 / 720 * screenHeight,
                      margin: EdgeInsets.only( top: 30/720 * screenHeight),

                      //width: double.infinity,
                      // decoration: new BoxDecoration(color: Theme.of(context).primaryColor),
                      child: new TabBar(

                        controller: _tabController,
                        indicatorColor: NeutralColors.purpleish_blue,
                        labelColor: NeutralColors.purpleish_blue,
                        unselectedLabelColor: NeutralColors.blue_grey,
                        indicatorSize: TabBarIndicatorSize.tab,
                        tabs: tabList,
                        indicatorPadding: EdgeInsets.only(
                            left: 5.0 / 360 * screenWidth,
                            right: 5.0 / 360 * screenWidth),
                        isScrollable: true,
                        unselectedLabelStyle: TextStyle(
                          fontSize: 14 / 360 * screenWidth,
                          fontFamily: "IBM Plex Sans",
                          fontWeight: FontWeight.w400,
                        ),
                        labelStyle: TextStyle(
                          fontSize: 14 / 360 * screenWidth,
                          fontFamily: "IBM Plex Sans",
                          fontWeight: FontWeight.w500,
                        ),

                      ),
                    ),
                    Expanded(
                      //color: Colors.red,
                      //height: 380 / 720 * screenHeight,
                      //margin: EdgeInsets.only(top: 10/720 * screenHeight, bottom: 0),
                      child: new TabBarView(
                        controller: _tabController,
                        children: <Widget>[
                          DiscussionGeneral(),
                          DiscussionTopic(),
                          DiscussionTest(),
                          DiscussionCommunity(),
                        ],
                      ),
                    )
                  ],
                ),
              ),
            ],
          ),
        ),
      );
  }
}
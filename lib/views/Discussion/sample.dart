import 'package:flutter/material.dart';
import 'package:imsindia/components/navigation_bar.dart';
import 'package:imsindia/routers/routes.dart';
import 'package:imsindia/utils/colors.dart';
import 'package:imsindia/views/Discussion/discussion_community.dart';
import 'package:imsindia/views/Discussion/discussion_filter.dart';
import 'package:imsindia/views/Discussion/discussion_general.dart';
import 'package:imsindia/views/Discussion/discussion_test.dart';
import 'package:imsindia/views/Discussion/discussion_topic.dart';

class Sample extends StatefulWidget {
  @override
  SampleState createState() => SampleState();
}

var screenWidthTotal;
var screenHeightTotal;

var _safeAreaHorizontal;

var _safeAreaVertical;

var screenHeight;

var screenWidth;
var showSearch = false;
TextEditingController controller = TextEditingController();

class SampleState extends State<Sample> {
  TabController _tabController;

  List<Tab> tabList = [
    new Tab(
      child: Text(
        "General",
      ),
    ),
    new Tab(
      child: Text(
        "Topic",
      ),
    ),
    new Tab(
      child: Text(
        "Test",
      ),
    ),
    new Tab(
      child: Text(
        "Community",
      ),
    )
  ];
  @override
  Widget build(BuildContext context) {
    var _mediaQueryData = MediaQuery.of(context);
    screenWidthTotal = _mediaQueryData.size.width;
    screenHeightTotal = _mediaQueryData.size.height;
    _safeAreaHorizontal =
        _mediaQueryData.padding.left + _mediaQueryData.padding.right;
    _safeAreaVertical =
        _mediaQueryData.padding.top + _mediaQueryData.padding.bottom;

    screenHeight = screenHeightTotal - _safeAreaVertical;
    screenWidth = screenWidthTotal - _safeAreaHorizontal;
    return Scaffold(
//      drawer: NavigationDrawer(),
      body: DefaultTabController(
        length: tabList.length,
        child: NestedScrollView(
          headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
            return <Widget>[
              SliverAppBar(
                expandedHeight: 200.0,
                floating: true,
                pinned: true,
                snap: false,
                flexibleSpace: showSearch == false?
                AppBar(
                  brightness: Brightness.light,
                  elevation: 0.0,
                  centerTitle: false,
                  titleSpacing: 0.0,
                  backgroundColor: Colors.white,
                  iconTheme: IconThemeData(
                    color: Colors.black, //change your color here
                  ),
                  title: Row(
                    children: <Widget>[
                      Text(
                        "Discussions",
                        style: TextStyle(
                            color: Colors.black,
                            fontFamily: "IBMPlexSans",
                            fontWeight: FontWeight.w500,
                            fontSize: 16.0),
                      ),
                    ],
                  ),
                  actions: <Widget>[
                    Align(
                      alignment: Alignment.centerRight,
                      child: Container(
                        margin:
                        EdgeInsets.only(right: 10/360 * screenHeight),
                        height: (16 / 720) * screenHeight,
                        width: (16 / 360) * screenWidth,
                        child: GestureDetector(
                          child: Image.asset(
                            'assets/images/search.png',
                            fit: BoxFit.contain,
                          ),
                          onTap: () {
                            setState(() {
                              showSearch = true;
                            });
                          },
                        ),
                      ),
                    ),
                    Align(
                      alignment: Alignment.centerRight,
                      child: Container(
                        margin:
                        EdgeInsets.only(right: 15/360 * screenHeight),
                        height: (14 / 720) * screenHeight,
                        width: (16 / 360) * screenWidth,
                        child: GestureDetector(
                          child: Image.asset(
                            "assets/images/icon-filter.png",
                            fit: BoxFit.contain,
                            color: Colors.black,
                          ),
                          onTap: () {
                            AppRoutes.push(context, DiscussionFilter());
                          },
                        ),
                      ),
                    ),
                  ],
                ):
                AppBar(
                  brightness: Brightness.light,
                  centerTitle: false,
                  titleSpacing: 0.0,
                  automaticallyImplyLeading: false,
                  backgroundColor: Colors.white,
                  title: Row(
                    children: <Widget>[
                      Container(
                        height: (40 / 720) * screenHeight,
                        width: (250.0 / 360) * screenWidth,
                        //margin: EdgeInsets.only(right: (6 / 360) * screenWidth),
                        child: Center(
                          child: TextField(
                            onSubmitted: (value) {

                            },
                            textAlign: TextAlign.start,
                            autofocus: true,
                            controller: controller,
                            textInputAction: TextInputAction.send,
                            autocorrect: false,
                            decoration: InputDecoration(
                              focusedBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.all(
                                  Radius.circular(30),
                                ),
                                borderSide: BorderSide(
                                    color: Color(0xfff2f4f4), width: 1.5),
                              ),
                              contentPadding: const EdgeInsets.all(11.0),
                              hintText: "Search Discussions",
                              alignLabelWithHint: true,
                              border: OutlineInputBorder(
                                borderSide: BorderSide(
                                    color: Color(0xfff2f4f4), width: 1.5),
                                borderRadius: BorderRadius.all(
                                  Radius.circular(30),
                                ),
                              ),
                              hintStyle: TextStyle(
                                  fontWeight: FontWeight.normal,
                                  color: Color(0xFF7e919a),
                                  fontSize: 14.0),
                            ),
                            style: TextStyle(
                                color: NeutralColors.dark_navy_blue,
                                fontWeight: FontWeight.normal,
                                fontSize: 12),
                          ),
                        ),
                      ),
                    ],
                  ),
                  leading: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Container(
                      height: (14 / 720) * screenHeight,
                      width: (14 / 360) * screenWidth,
                      margin: EdgeInsets.only(
                          left: (16.0 / 360) * screenWidth,
                          right: (0 / 360) * screenWidth),
                      child: Image.asset(
                        'assets/images/search.png',
                        fit: BoxFit.contain,
                      ),
                    ),
                  ),
                  actions: <Widget>[
                    GestureDetector(
                      onTap: () {
                        // Navigator.pop(context, false);
                        setState(() {
                          controller.text = "";
                          showSearch = false;
                          FocusScope.of(context)
                              .requestFocus();
                        });
                      },
                      child: Container(
                        height: (22 / 720) * screenHeight,
                        width: (22 / 360) * screenWidth,
                        margin: EdgeInsets.only(right: (19 / 360) * screenWidth),
                        child: Image.asset(
                          'assets/images/close.png',
                          fit: BoxFit.contain,
                        ),
                      ),
                    ),
                  ],
                ),

//                FlexibleSpaceBar(
//                    centerTitle: true,
//                    title: Text("Collapsing Toolbar",
//                        style: TextStyle(
//                          color: Colors.white,
//                          fontSize: 16.0,
//                        )),
//                    background: Image.network(
//                      "https://images.pexels.com/photos/396547/pexels-photo-396547.jpeg?auto=compress&cs=tinysrgb&h=350",
//                      fit: BoxFit.cover,
//                    )
//                ),
              ),
//              Container(
//                height: 80/720 * screenHeight,
//                margin: EdgeInsets.only(left: 20/360 * screenWidth, top: 15/720 * screenHeight, right: 20/360 * screenWidth),
//                decoration: BoxDecoration(
//                  gradient: LinearGradient(
//                    begin: Alignment(0, 0),
//                    end: Alignment(1.049, 1.008),
//                    stops: [
//                      0,
//                      1,
//                    ],
//                    colors: [
//                      Color.fromARGB(255, 168, 174, 35).withOpacity(0.1),
//                      Color.fromARGB(255, 201, 110, 216).withOpacity(0.1),
//                    ],
//                  ),
//                  borderRadius: BorderRadius.all(Radius.circular(5)),
//                ),
//                child: Row(
//                  crossAxisAlignment: CrossAxisAlignment.start,
//                  children: [
//                    Container(
//                      width: 39/360 * screenWidth,
//                      height: 44/720 * screenHeight,
//                      margin: EdgeInsets.only(left: 20/360 * screenHeight, top: 18/720 * screenHeight),
//                      child: Column(
//                        crossAxisAlignment: CrossAxisAlignment.start,
//                        children: [
//                          Container(
//                            margin: EdgeInsets.only(left: 14/360 * screenWidth),
//                            child: Text(
//                              "5",
//                              style: TextStyle(
//                                color: NeutralColors.dark_navy_blue,
//                                fontSize: 20/720 * screenHeight,
//                                fontFamily: "IBM Plex Sans",
//                                fontWeight: FontWeight.w700,
//                              ),
//                              textAlign: TextAlign.center,
//                            ),
//                          ),
//                          Align(
//                            alignment: Alignment.topCenter,
//                            child: Container(
//                              margin: EdgeInsets.only( top: 4/720 * screenHeight),
//                              child: Text(
//                                "POSTS",
//                                style: TextStyle(
//                                  color: NeutralColors.gunmetal,
//                                  fontSize: 12/720 * screenHeight,
//                                  fontFamily: "IBM Plex Sans",
//                                  fontWeight: FontWeight.w500,
//                                ),
//                                textAlign: TextAlign.center,
//                              ),
//                            ),
//                          ),
//                        ],
//                      ),
//                    ),
//                    Container(
//                      margin: EdgeInsets.only(left: 15/360 * screenHeight, top: 17/720 * screenHeight),
//                      child: Opacity(
//                        opacity: 0.15,
//                        child: Container(
//                          width: 1,
//                          height: 48/720 * screenHeight,
//                          decoration: BoxDecoration(
//                            color: Color.fromARGB(255, 122, 113, 213),
//                          ),
//                          child: Container(),
//                        ),
//                      ),
//                    ),
//                    Container(
//                      width: 66/360 * screenWidth,
//                      height: 56/720 * screenHeight,
//                      margin: EdgeInsets.only(left: 20/360 * screenWidth, top: 18/720 * screenHeight),
//                      child: Column(
//                        children: [
//                          Text(
//                            "5",
//                            style: TextStyle(
//                              color: NeutralColors.dark_navy_blue,
//                              fontSize: 20/720 * screenHeight,
//                              fontFamily: "IBM Plex Sans",
//                              fontWeight: FontWeight.w700,
//                            ),
//                            textAlign: TextAlign.center,
//                          ),
//                          Align(
//                            alignment: Alignment.topCenter,
//                            child: Container(
//                              margin: EdgeInsets.only(top: 4/720 * screenHeight),
//                              child: Text(
//                                "COMMENTS",
//                                style: TextStyle(
//                                  color: NeutralColors.gunmetal,
//                                  fontSize: 12/720 * screenHeight,
//                                  fontFamily: "IBM Plex Sans",
//                                  fontWeight: FontWeight.w500,
//                                ),
//                                textAlign: TextAlign.center,
//                              ),
//                            ),
//                          ),
//                        ],
//                      ),
//                    ),
//                    Container(
//                      margin: EdgeInsets.only(left: 20/360 * screenWidth, top: 17/720 * screenHeight),
//                      child: Opacity(
//                        opacity: 0.15,
//                        child: Container(
//                          width: 1,
//                          height: 48/720 * screenHeight,
//                          decoration: BoxDecoration(
//                            color: Color.fromARGB(255, 122, 113, 213),
//                          ),
//                          child: Container(),
//                        ),
//                      ),
//                    ),
//                    Container(
//                      width: 71/360 * screenWidth,
//                      height: 56/720 * screenHeight,
//                      margin: EdgeInsets.only(left: 17/360 * screenWidth, top: 18/720 * screenHeight),
//                      child: Column(
//                        children: [
//                          Text(
//                            "25",
//                            style: TextStyle(
//                              color: NeutralColors.dark_navy_blue,
//                              fontSize: 20/720 * screenHeight,
//                              fontFamily: "IBM Plex Sans",
//                              fontWeight: FontWeight.w700,
//                            ),
//                            textAlign: TextAlign.center,
//                          ),
//                          Align(
//                            alignment: Alignment.topCenter,
//                            child: Container(
//                              margin: EdgeInsets.only(left: 1/360 * screenWidth, top: 4/720 * screenHeight),
//                              child: Text(
//                                "FOLLOWERS",
//                                style: TextStyle(
//                                  color: NeutralColors.gunmetal,
//                                  fontSize: 12/720 * screenHeight,
//                                  fontFamily: "IBM Plex Sans",
//                                  fontWeight: FontWeight.w500,
//                                ),
//                                textAlign: TextAlign.center,
//                              ),
//                            ),
//                          ),
//                        ],
//                      ),
//                    ),
//                  ],
//                ),
//              ),

              SliverPersistentHeader(
                delegate: _SliverAppBarDelegate(
                  TabBar(

                    controller: _tabController,
                    indicatorColor: NeutralColors.purpleish_blue,
                    labelColor: NeutralColors.purpleish_blue,
                    unselectedLabelColor: NeutralColors.blue_grey,
                    indicatorSize: TabBarIndicatorSize.tab,
                    tabs: tabList,
                    indicatorPadding: EdgeInsets.only(
                        left: 5.0 / 360 * screenWidth,
                        right: 5.0 / 360 * screenWidth),
                    isScrollable: true,
                    unselectedLabelStyle: TextStyle(
                      fontSize: 14 / 360 * screenWidth,
                      fontFamily: "IBM Plex Sans",
                      fontWeight: FontWeight.w400,
                    ),
                    labelStyle: TextStyle(
                      fontSize: 14 / 360 * screenWidth,
                      fontFamily: "IBM Plex Sans",
                      fontWeight: FontWeight.w500,
                    ),

                  ),
                ),
                pinned: true,
              ),
            ];
          },
          body: Container(
                     height: 380 / 720 * screenHeight,
                      //margin: EdgeInsets.only(top: 10/720 * screenHeight, bottom: 0),
                      child: new TabBarView(
                        controller: _tabController,
                        children: <Widget>[
                          DiscussionGeneral(),
                          DiscussionTopic(),
                          DiscussionTest(),
                          DiscussionCommunity(),
                        ],
                      ),
            ),
        ),
      ),
    );
  }
}

class _SliverAppBarDelegate extends SliverPersistentHeaderDelegate {
  _SliverAppBarDelegate(this._tabBar);

  final TabBar _tabBar;

  @override
  double get minExtent => _tabBar.preferredSize.height;
  @override
  double get maxExtent => _tabBar.preferredSize.height;

  @override
  Widget build(
      BuildContext context, double shrinkOffset, bool overlapsContent) {
    return new Container(
      child: _tabBar,
    );
  }

  @override
  bool shouldRebuild(_SliverAppBarDelegate oldDelegate) {
    return false;
  }
}
import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:image_picker/image_picker.dart';
import 'package:imsindia/components/Input_forms.dart';
import 'package:imsindia/components/navigation_bar.dart';
import 'package:imsindia/routers/routes.dart';
import 'package:imsindia/utils/colors.dart';
import 'package:imsindia/utils/svg_images/channel_svg_icons.dart';
import 'package:imsindia/utils/svg_images/prepare_svg_images.dart';
import 'package:imsindia/utils/text_styles.dart';
import 'package:imsindia/views/Discussion/discussion_home.dart';
import 'package:imsindia/views/Discussion/discussion_home.dart' as prefix0;




var screenWidthTotal;
var screenHeightTotal;

var _safeAreaHorizontal;

var _safeAreaVertical;

var screenHeight;

var screenWidth;
var general = false;
var topic = false;
var test = false;
bool _enabledSlot = false;
final TextEditingController _title = new TextEditingController();


class DiscussionNewPost extends StatefulWidget {
  @override
  DiscussionNewPostState createState() =>DiscussionNewPostState();
}

class DiscussionNewPostState extends State<DiscussionNewPost>{
  File _imageFile;
  List<String> _category = [
    "General",
    "Topic",
    "Test",
  ];
  int _selectedIndexSlot;
  _onSelectedSlot(int indexSlot) {
    setState(() {
      _selectedIndexSlot = indexSlot;

      _enabledSlot = true;
    });
  }

  @override
  void initState() {
    super.initState();
    setState(() {
      _title.text = '';
    });

  }
  void _showDialog() {
    // flutter defined function
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return SimpleDialog(
          title: new Text(
            'Pick image from',
            style: TextStyles.editTextStyle,
          ),
          children: <Widget>[
            new SimpleDialogOption(
              child: new Text(
                'Camera',
                style: TextStyles.editTextStyle,
              ),
              onPressed: () {
                print("tapped in dialog");
                openCamera();
                Navigator.of(context).pop();
              },
            ),
            new SimpleDialogOption(
              child: new Text(
                'Gallery',
                style: TextStyles.editTextStyle,
              ),
              onPressed: () async {
                print("tapped in dialog");
                await openGalley();
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  openCamera() async {
    print("tapped in openCamera");
    _upload();
    final File imageFile =
    await ImagePicker.pickImage(source: ImageSource.camera);
    print('my image' + imageFile.toString());
    setState(() {
      this._imageFile = imageFile;
    });
  }

  void _upload() {
    if (_imageFile == null) return;
    String base64Image = base64Encode(_imageFile.readAsBytesSync());
    String fileName = _imageFile.path.split("/").last;
    print('base64Image' + base64Image);
    print('fileName' + fileName);
//    http.post(phpEndPoint, body: {
//      "image": base64Image,
//      "name": fileName,
//    }).then((res) {
//      print(res.statusCode);
//    }).catchError((err) {
//      print(err);
//    });
  }

  openGalley() async {
    _upload();
    print("tapped in openGalley");
    final File imageFile =
    await ImagePicker.pickImage(source: ImageSource.gallery);
    print('my image' + imageFile.toString());

    setState(() {
      this._imageFile = imageFile;
    });
  }
  Future<bool> _onWillPop() {
    //back
    AppRoutes.push(context, DiscussionHome());
  }
  Widget build(BuildContext context) {
    var _mediaQueryData = MediaQuery.of(context);
    screenWidthTotal = _mediaQueryData.size.width;
    screenHeightTotal = _mediaQueryData.size.height;
    _safeAreaHorizontal =
        _mediaQueryData.padding.left + _mediaQueryData.padding.right;
    _safeAreaVertical =
        _mediaQueryData.padding.top + _mediaQueryData.padding.bottom;

    screenHeight = screenHeightTotal - _safeAreaVertical;
    screenWidth = screenWidthTotal - _safeAreaHorizontal;
    return  Scaffold(
        body: Container(
          child: Column(
            children: <Widget>[
              Container(
                margin: EdgeInsets.only(
                  top: (42 / 720) * screenHeight,
                ),
                child: Row(
                  //crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    InkWell(
                      onTap: () {
                        Navigator.pop(context);
                      },
                      child: Container(
                        width: (58 / 360) * screenWidth,
                        height: (30.0 / 720) * screenHeight,
                        margin: EdgeInsets.only(left: 0.0),
                        child: SvgPicture.asset(
                          getPrepareSvgImages.backIcon,
                          height: (5 / 720) * screenHeight,
                          width: (14 / 360) * screenWidth,
                          fit: BoxFit.none,
                        ),
                      ),
                    ),
                    Container(
                      height: (20 / 720) * screenHeight,
                      // margin: EdgeInsets.only(left: (20 / 360) * screenWidth),
                      child: Text(
                        "New Post",
                        style: TextStyle(
                          color: NeutralColors.black,
                          fontSize: (16 / 720) * screenHeight,
                          fontFamily: "IBMPlexSans",
                          fontWeight: FontWeight.w700,
                        ),
                        textAlign: TextAlign.left,
                      ),
                    ),
                  ],
                ),
              ),
              Align(
                alignment: Alignment.topLeft,
                child: Container(
                  margin: EdgeInsets.only(left: 20/360 * screenWidth, top: 48/720 * screenHeight),
                  child: Text(
                    "Pick a category",
                    style: TextStyle(
                      color: NeutralColors.dark_navy_blue,
                      fontSize: 14/720 * screenHeight,
                      fontFamily: "IBMPlexSans",
                      fontWeight: FontWeight.w500,
                    ),
                    textAlign: TextAlign.left,
                  ),
                ),
              ),
              Align(
                alignment: Alignment.topLeft,
                child: Container(
                  //color: Colors.red,
                  width: 320/360 * screenWidth,
                  height: 40/720 * screenHeight,
                  margin: EdgeInsets.only(left: 20/360 * screenWidth, top: 17/720 * screenHeight),
                 child: Row(
                   crossAxisAlignment: CrossAxisAlignment.start,
                   children: [
                     InkWell(
                       onTap: () {
                         setState(() {
                           general = !general;
                         });
                       },
                       child: Container(
                         width: 85 / 360 * screenWidth,
                         height: 30 / 720 * screenHeight,
                         decoration: BoxDecoration(
                           color: general
                               ? NeutralColors.ice_blue
                               : NeutralColors.purpleish_blue,
                           border: Border.all(
                             color: general
                                 ? Color.fromARGB(255, 242, 244, 244)
                                 : Colors.transparent,
                             width: 1,
                           ),
                           borderRadius:
                           BorderRadius.all(Radius.circular(15)),
                         ),
                         child: Column(
                           mainAxisAlignment: MainAxisAlignment.center,
                           crossAxisAlignment:
                           CrossAxisAlignment.stretch,
                           children: [
                             Container(
                               margin:
                               EdgeInsets.symmetric(horizontal: 15),
                               child: Text(
                                 'General',
                                 style: general
                                     ? TextStyle(
                                   color: Color(0xFF646464),
                                   fontSize:
                                   14 / 720 * screenHeight,
                                   fontFamily:
                                   "IBMPlexSans",
                                   fontWeight: FontWeight.w500,
                                 )
                                     : TextStyle(
                                   color: Color(0xFFffffff),
                                   fontSize:
                                   14 / 720 * screenHeight,
                                   fontFamily:
                                   "IBMPlexSans",
                                   fontWeight: FontWeight.w500,
                                 ),
                                 textAlign: TextAlign.center,
                               ),
                             ),
                           ],
                         ),
                       ),
                     ),
                     InkWell(
                       onTap: () {
                         setState(() {
                           topic = !topic;
                         });
                       },
                       child: Container(
                         width: 65 / 360 * screenWidth,
                         height: 30 / 720 * screenHeight,
                         margin: EdgeInsets.only(left: 10/360 * screenHeight),
                         decoration: BoxDecoration(
                           color: topic
                               ? NeutralColors.ice_blue
                               : NeutralColors.purpleish_blue,
                           border: Border.all(
                             color: topic
                                 ? Color.fromARGB(255, 242, 244, 244)
                                 : Colors.transparent,
                             width: 1,
                           ),
                           borderRadius:
                           BorderRadius.all(Radius.circular(15)),
                         ),
                         child: Column(
                           mainAxisAlignment: MainAxisAlignment.center,
                           crossAxisAlignment:
                           CrossAxisAlignment.stretch,
                           children: [
                             Container(
                               margin:
                               EdgeInsets.symmetric(horizontal: 10),
                               child: Text(
                                 'Topic',
                                 style: topic
                                     ? TextStyle(
                                   color: Color(0xFF646464),
                                   fontSize:
                                   14 / 720 * screenHeight,
                                   fontFamily:
                                   "IBMPlexSans",
                                   fontWeight: FontWeight.w500,
                                 )
                                     : TextStyle(
                                   color: Color(0xFFffffff),
                                   fontSize:
                                   14 / 720 * screenHeight,
                                   fontFamily:
                                   "IBMPlexSans",
                                   fontWeight: FontWeight.w500,
                                 ),
                                 textAlign: TextAlign.center,
                               ),
                             ),
                           ],
                         ),
                       ),
                     ),
                     InkWell(
                       onTap: () {
                         setState(() {
                           test = !test;
                         });
                       },
                       child: Container(
                         width: 65 / 360 * screenWidth,
                         height: 30 / 720 * screenHeight,
                         margin: EdgeInsets.only(left: 10/360 * screenWidth),
                         decoration: BoxDecoration(
                           color: test
                               ? NeutralColors.ice_blue
                               : NeutralColors.purpleish_blue,
                           border: Border.all(
                             color: test
                                 ? Color.fromARGB(255, 242, 244, 244)
                                 : Colors.transparent,
                             width: 1,
                           ),
                           borderRadius:
                           BorderRadius.all(Radius.circular(15)),
                         ),
                         child: Column(
                           mainAxisAlignment: MainAxisAlignment.center,
                           crossAxisAlignment:
                           CrossAxisAlignment.stretch,
                           children: [
                             Container(
                               margin:
                               EdgeInsets.symmetric(horizontal: 15),
                               child: Text(
                                 'Test',
                                 style: test
                                     ? TextStyle(
                                   color: Color(0xFF646464),
                                   fontSize:
                                   14 / 720 * screenHeight,
                                   fontFamily:
                                   "IBMPlexSans",
                                   fontWeight: FontWeight.w500,
                                 )
                                     : TextStyle(
                                   color: Color(0xFFffffff),
                                   fontSize:
                                   14 / 720 * screenHeight,
                                   fontFamily:
                                   "IBMPlexSans",
                                   fontWeight: FontWeight.w500,
                                 ),
                                 textAlign: TextAlign.center,
                               ),
                             ),
                           ],
                         ),
                       ),
                     ),
                   ],
                 ),
//                  Padding(
//                    padding: EdgeInsets.only(top: 10/720 * screenHeight),
//                    child: GridView.builder(
//                      //physics: NeverScrollableScrollPhysics(),
//                      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
//                        crossAxisCount: 3,
//                        crossAxisSpacing: 0.0,
//                        mainAxisSpacing: 0.0,
//                        childAspectRatio: 13 / (10 / 2),
//                      ),
//                      //physics: NeverScrollableScrollPhysics(),
//                      padding: const EdgeInsets.all(0.0),
//
//                      itemCount: _category.length,
//                      itemBuilder: (build, int indexSlot) {
//                        final item = _category[indexSlot];
//                        return Container(
//                          margin: EdgeInsets.only(left: 10/360 * screenWidth,bottom: 10/720 * screenHeight),
//                          child: InkWell(
//                            onTap: ()  {
//                              _onSelectedSlot(indexSlot);
//                              if (indexSlot != null)
//                                setState(() => _enabledSlot = true);
//                            },
//                            child: Flex(
//                                direction: Axis.horizontal,
//                                mainAxisAlignment: MainAxisAlignment.start,
//                                mainAxisSize: MainAxisSize.min,
//                                crossAxisAlignment: CrossAxisAlignment.start,
//                                children: <Widget>[
//                                  Container(
//                                    //width: double.infinity,
//                                    height: 40 / 720 * screenHeight,
//                                    padding: EdgeInsets.only(left: 7/360 * screenWidth, top: 2/720 * screenHeight, bottom: 2/720 * screenHeight, right: 7/360 * screenWidth),
//                                    constraints: BoxConstraints(
//                                      maxWidth: screenWidth * 85/360,
//                                    ),
//                                    decoration: BoxDecoration(
//                                      color: _selectedIndexSlot != null &&
//                                          _selectedIndexSlot == indexSlot
//                                          ? NeutralColors.purpleish_blue
//                                          : Colors.transparent,
//                                      border: Border.all(
//                                        color: _selectedIndexSlot != null &&
//                                            _selectedIndexSlot == indexSlot
//                                            ? Colors.transparent
//                                            : Color.fromARGB(255, 242, 244, 244),
//                                        width: 1,
//                                      ),
//                                      borderRadius:
//                                      BorderRadius.all(Radius.circular(20)),
//                                    ),
//                                    child: Text(
//                                      _category[indexSlot],
//                                      style: _selectedIndexSlot != null &&
//                                          _selectedIndexSlot == indexSlot
//                                          ? TextStyle(
//                                        color: Color(0xFFffffff),
//                                        fontSize:
//                                        14 / 720 * screenHeight,
//                                        fontFamily:
//                                        "IBMPlexSans",
//                                        fontWeight: FontWeight.w500,
//                                      ):TextStyle(
//                                        color: Color(0xFF646464),
//                                        fontSize:
//                                        14 / 720 * screenHeight,
//                                        fontFamily:
//                                        "IBMPlexSans",
//                                        fontWeight: FontWeight.w500,
//                                      ),
//                                      textAlign: TextAlign.center,
//                                    ),
//                                  ),
//                                ]
//                            ),
//                          ),
//                        );
////                              Container(
////                              height: 30,
////                              width: 100,
////                              //margin: EdgeInsets.fromLTRB(10, 20, 10, 0.0),
////                              child: FlatButton(
////                                onPressed: () {
////                                  _onSelectedSlot(indexSlot);
////                                  if (indexSlot != null)
////                                    setState(() => _enabledSlot = true);
////                                },
////                                color: _selectedIndexSlot != null &&
////                                    _selectedIndexSlot == indexSlot
////                                    ? NeutralColors.purpleish_blue
////                                    : Colors.white,
////                                shape: RoundedRectangleBorder(
////                                  borderRadius: BorderRadius.all(Radius.circular(20)),
////                                  side: BorderSide(
////                                    width: 1,
////                                    color: _selectedIndexSlot != null &&
////                                        _selectedIndexSlot == indexSlot
////                                        ? Colors.transparent
////                                        : SemanticColors.iceBlue,
////                                    style: BorderStyle.solid,
////                                  ),
////                                ),
////                                textColor: _selectedIndexSlot != null &&
////                                    _selectedIndexSlot == indexSlot
////                                    ? Colors.white
////                                    : Color.fromARGB(255, 100, 100, 100),
////                                padding: EdgeInsets.all(0),
////                                child: Text(
////                                  item,
////                                  style: TextStyle(
////                                    fontSize: 14 / 720 * screenHeight,
////                                    fontFamily: "IBM Plex Sans",
////                                  ),
////                                  textAlign: TextAlign.left,
////                                ),
////                              ),
////                            );
//                      },
//                      //padding: EdgeInsets.all(0.0),
//                    ),
//                  ),




                ),
              ),
              Container(
                height: 31/720 * screenHeight,
                margin: EdgeInsets.only(left: 20/360 * screenWidth, top: 30/720 * screenHeight, right: 20/360 * screenWidth),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    Align(
                      alignment: Alignment.topLeft,
                      child: Container(
                        width: 31/360 * screenWidth,
                        height: 31/720 * screenHeight,
                        decoration: new BoxDecoration(
                            shape: BoxShape.circle,
                            image: new DecorationImage(
                                fit: BoxFit.cover,
                                image: AssetImage(
                                    'assets/images/group-10-2.png'))),
                      ),
                    ),
                    Container(
                      //height: 27/720 * screenHeight,
                      width: 280/360 * screenWidth,
                      margin: EdgeInsets.only(left: 9/360 * screenWidth, top: 4/720 * screenHeight),
                          child: TextInputFormField(
                            controller: _title,
                            keyboardType: TextInputType.text,
                            //lableText: "Insert title here…",
                            hintText: "Insert title here…",
                            textInputAction: TextInputAction.done,
                            border: _title.text.isEmpty ? UnderlineInputBorder(borderSide:BorderSide(color:SemanticColors.blueGrey.withOpacity(0.3),width: 1.5 )): UnderlineInputBorder(borderSide:BorderSide(color:SemanticColors.dark_purpely,width: 1.5 ) ),
                            autovalidate: false,
                            onSaved: (value) {

                            },
                            onFieldSubmitted:(v){
                              FocusScope.of(context).requestFocus();
                            },
                          ),
                        ),
                  ],
                ),
              ),
              Align(
                alignment: Alignment.topLeft,
                child: Container(
                  width: 320/360 * screenWidth,
                  height: 100/720 * screenHeight,
                  margin: EdgeInsets.only(left: 20/360 * screenWidth, top: 29/720 * screenHeight),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      Align(
                        alignment: Alignment.topLeft,
                        child: Container(
                          width: 237/360 * screenWidth,
                          //height: 61/720 * screenHeight,
                          child: Stack(
                            alignment: Alignment.center,
                            children: [
                              Positioned(
                                left: 0,
                                top: 0,
                                child: Text(
                                  "Write something here…",
                                  style: TextStyle(
                                    color: NeutralColors.dark_navy_blue,
                                    fontSize: 14/720 * screenHeight,
                                    fontFamily: "IBM Plex Sans",
                                  ),
                                  textAlign: TextAlign.left,
                                ),
                              ),
                              Positioned(
                                left: 0,
                                top: 17/720 * screenHeight,
                                child: Container(
                                  width: 237/360 * screenWidth,
                                  child: Text(
                                    "Tip: Using hashtags will make this post visible to more people.",
                                    style: TextStyle(
                                      color: NeutralColors.blue_grey,
                                      fontSize: 12/720 * screenHeight,
                                      height: 1.5,
                                      fontFamily: "IBM Plex Sans",
                                      fontStyle: FontStyle.italic,
                                    ),
                                    textAlign: TextAlign.left,
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                      Align(
                        alignment: Alignment.topRight,
                        child: Container(
                          width: 74/360 * screenWidth,
                          //height: 100/720 * screenHeight,
                          margin: EdgeInsets.only(left: 9/360 * screenWidth),
                          child: Column(
                            //alignment: Alignment.center,
                            children: [
                              Container(
                                margin: EdgeInsets.only(left: 7/360 * screenWidth),

                                child: Container(
                                  width: 60/360 * screenWidth,
                                  height: 60/720 * screenHeight,
                                  child: this._imageFile == null?
                                  Image.asset("assets/images/rectangle-copy-3.png"):
                                  Image.file(this._imageFile),
                                ),
                              ),
                              Container(
                                margin: EdgeInsets.only(left: 5/360 * screenHeight, top: 2/720 * screenHeight),
                                child: GestureDetector(
                                  onTap: (){
                                    _showDialog();
                                  },
                                  child: Text(
                                    "Upload Photo",
                                    style: TextStyle(
                                      color: PrimaryColors.azure_Dark,
                                      fontSize: 12/720 * screenHeight,
                                      fontFamily: "IBM Plex Sans",
                                    ),
                                    textAlign: TextAlign.center,
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      );
  }
}

import 'package:flutter/material.dart';


class DiscussionNineWidget extends StatelessWidget {
  
  void onNewPostPressed(BuildContext context) => Navigator.push(context, MaterialPageRoute(builder: (context) => DiscussionNineWidget()));
  
  @override
  Widget build(BuildContext context) {
  
    return Scaffold(
      body: Container(
        constraints: BoxConstraints.expand(),
        decoration: BoxDecoration(
          color: Color.fromARGB(255, 255, 255, 255),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Align(
              alignment: Alignment.topLeft,
              child: Container(
                width: 108,
                height: 20,
                margin: EdgeInsets.only(left: 20, top: 42),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    Align(
                      alignment: Alignment.topLeft,
                      child: Container(
                        width: 18,
                        height: 14,
                        margin: EdgeInsets.only(top: 4),
                        child: Image.asset(
                          "assets/images/icon-hamburger.png",
                          fit: BoxFit.none,
                        ),
                      ),
                    ),
                    Align(
                      alignment: Alignment.topLeft,
                      child: Container(
                        width: 70,
                        height: 20,
                        margin: EdgeInsets.only(left: 20),
                        child: FlatButton(
                          onPressed: () => this.onNewPostPressed(context),
                          color: Colors.transparent,
                          textColor: Color.fromARGB(255, 0, 0, 0),
                          padding: EdgeInsets.all(0),
                          child: Text(
                            "New Post",
                            style: TextStyle(
                              fontSize: 16,
                              fontFamily: "IBM Plex Sans",
                              fontWeight: FontWeight.w500,
                            ),
                            textAlign: TextAlign.left,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Align(
              alignment: Alignment.topLeft,
              child: Container(
                width: 230,
                height: 68,
                margin: EdgeInsets.only(left: 20, top: 48),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      width: 155,
                      height: 70,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: [
                          Align(
                            alignment: Alignment.topLeft,
                            child: Text(
                              "Pick a category",
                              style: TextStyle(
                                color: Color.fromARGB(255, 0, 3, 44),
                                fontSize: 14,
                                fontFamily: "IBM Plex Sans",
                                fontWeight: FontWeight.w500,
                              ),
                              textAlign: TextAlign.left,
                            ),
                          ),
                          Align(
                            alignment: Alignment.topLeft,
                            child: Container(
                              width: 155,
                              height: 30,
                              margin: EdgeInsets.only(top: 17),
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.stretch,
                                children: [
                                  Align(
                                    alignment: Alignment.topLeft,
                                    child: Container(
                                      width: 80,
                                      height: 30,
                                      child: Stack(
                                        alignment: Alignment.center,
                                        children: [
                                          Positioned(
                                            left: 0,
                                            top: 0,
                                            child: Container(
                                              width: 80,
                                              height: 30,
                                              decoration: BoxDecoration(
                                                color: Color.fromARGB(255, 86, 72, 235),
                                                borderRadius: BorderRadius.all(Radius.circular(15)),
                                              ),
                                              child: Container(),
                                            ),
                                          ),
                                          Positioned(
                                            left: 15,
                                            top: 6,
                                            child: Text(
                                              "General",
                                              style: TextStyle(
                                                color: Color.fromARGB(255, 255, 255, 255),
                                                fontSize: 14,
                                                fontFamily: "IBM Plex Sans",
                                              ),
                                              textAlign: TextAlign.left,
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                  Align(
                                    alignment: Alignment.topLeft,
                                    child: Container(
                                      width: 65,
                                      height: 30,
                                      margin: EdgeInsets.only(left: 10),
                                      child: Stack(
                                        alignment: Alignment.center,
                                        children: [
                                          Positioned(
                                            left: 0,
                                            top: 0,
                                            child: Container(
                                              width: 65,
                                              height: 30,
                                              decoration: BoxDecoration(
                                                border: Border.all(
                                                  color: Color.fromARGB(255, 242, 244, 244),
                                                  width: 1,
                                                ),
                                                borderRadius: BorderRadius.all(Radius.circular(15)),
                                              ),
                                              child: Container(),
                                            ),
                                          ),
                                          Positioned(
                                            left: 15,
                                            top: 6,
                                            child: Text(
                                              "Topic",
                                              style: TextStyle(
                                                color: Color.fromARGB(255, 100, 100, 100),
                                                fontSize: 14,
                                                fontFamily: "IBM Plex Sans",
                                              ),
                                              textAlign: TextAlign.left,
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      width: 65,
                      height: 30,
                      margin: EdgeInsets.only(left: 10, top: 38),
                      child: Stack(
                        alignment: Alignment.center,
                        children: [
                          Positioned(
                            left: 0,
                            top: 0,
                            child: Container(
                              width: 65,
                              height: 30,
                              decoration: BoxDecoration(
                                border: Border.all(
                                  color: Color.fromARGB(255, 242, 244, 244),
                                  width: 1,
                                ),
                                borderRadius: BorderRadius.all(Radius.circular(15)),
                              ),
                              child: Container(),
                            ),
                          ),
                          Positioned(
                            left: 15,
                            top: 6,
                            child: Text(
                              "Tests",
                              style: TextStyle(
                                color: Color.fromARGB(255, 100, 100, 100),
                                fontSize: 14,
                                fontFamily: "IBM Plex Sans",
                              ),
                              textAlign: TextAlign.left,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Container(
              height: 31,
              margin: EdgeInsets.only(left: 20, top: 30, right: 20),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  Align(
                    alignment: Alignment.topLeft,
                    child: Container(
                      width: 31,
                      height: 31,
                      child: Image.asset(
                        "assets/images/group-10-2.png",
                        fit: BoxFit.none,
                      ),
                    ),
                  ),
                  Expanded(
                    flex: 1,
                    child: Align(
                      alignment: Alignment.topLeft,
                      child: Container(
                        height: 27,
                        margin: EdgeInsets.only(left: 9, top: 4),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          children: [
                            Align(
                              alignment: Alignment.topLeft,
                              child: Opacity(
                                opacity: 0.5,
                                child: Text(
                                  "Insert title here…",
                                  style: TextStyle(
                                    color: Color.fromARGB(255, 126, 145, 154),
                                    fontSize: 14,
                                    fontFamily: "IBM Plex Sans",
                                  ),
                                  textAlign: TextAlign.left,
                                ),
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.only(top: 8),
                              child: Opacity(
                                opacity: 0.3,
                                child: Container(
                                  height: 1,
                                  decoration: BoxDecoration(
                                    color: Color.fromARGB(255, 126, 145, 154),
                                  ),
                                  child: Container(),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Align(
              alignment: Alignment.topLeft,
              child: Container(
                width: 320,
                height: 80,
                margin: EdgeInsets.only(left: 20, top: 29),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    Align(
                      alignment: Alignment.topLeft,
                      child: Container(
                        width: 237,
                        height: 61,
                        child: Stack(
                          alignment: Alignment.center,
                          children: [
                            Positioned(
                              left: 0,
                              top: 0,
                              child: Text(
                                "Write something here…",
                                style: TextStyle(
                                  color: Color.fromARGB(255, 0, 3, 44),
                                  fontSize: 14,
                                  fontFamily: "IBM Plex Sans",
                                ),
                                textAlign: TextAlign.left,
                              ),
                            ),
                            Positioned(
                              left: 0,
                              top: 17,
                              child: Container(
                                width: 237,
                                child: Text(
                                  "Tip: Using hashtags will make this post visible to more people.",
                                  style: TextStyle(
                                    color: Color.fromARGB(255, 126, 145, 154),
                                    fontSize: 12,
                                    fontFamily: "IBM Plex Sans",
                                    fontStyle: FontStyle.italic,
                                  ),
                                  textAlign: TextAlign.left,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    Align(
                      alignment: Alignment.topLeft,
                      child: Container(
                        width: 74,
                        height: 80,
                        margin: EdgeInsets.only(left: 9),
                        child: Stack(
                          alignment: Alignment.center,
                          children: [
                            Positioned(
                              left: 7,
                              top: 0,
                              child: Container(
                                width: 60,
                                height: 60,
                                child: Image.asset(
                                  "assets/images/rectangle-copy-3.png",
                                  fit: BoxFit.none,
                                ),
                              ),
                            ),
                            Positioned(
                              left: 0,
                              top: 57,
                              child: Text(
                                "Upload Photo",
                                style: TextStyle(
                                  color: Color.fromARGB(255, 0, 171, 251),
                                  fontSize: 12,
                                  fontFamily: "IBM Plex Sans",
                                ),
                                textAlign: TextAlign.center,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
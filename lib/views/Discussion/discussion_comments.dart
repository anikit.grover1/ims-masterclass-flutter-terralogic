import 'package:flutter/material.dart';
import 'package:flutter_document_picker/flutter_document_picker.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:imsindia/components/navigation_bar.dart';
import 'package:imsindia/resources/strings/channel_labels.dart';
import 'package:imsindia/resources/strings/prepare.dart';
import 'package:imsindia/routers/routes.dart';
import 'package:imsindia/utils/colors.dart';
import 'package:imsindia/utils/screen_util.dart';
import 'package:imsindia/utils/svg_images/prepare_svg_images.dart';
import 'package:imsindia/views/Arun/ReadMoreText.dart';
import 'package:imsindia/views/Discussion/discussion_home.dart';
import 'package:imsindia/views/Discussion/discussion_home.dart' as prefix0;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:imsindia/utils/svg_icons.dart';




var screenWidthTotal;
var screenHeightTotal;

var _safeAreaHorizontal;

var _safeAreaVertical;

var screenHeight;

var screenWidth;
final TextEditingController _comment = new TextEditingController();



class DiscussionComments extends StatefulWidget {
  @override
  DiscussionCommentsState createState() => DiscussionCommentsState();
}

class DiscussionCommentsState extends State<DiscussionComments>{
  List <String> comment = <String>[
    'How do you create a direct mail advertising campaign that gets results?  The following tips on creating a direct mail advertising campaign have been street-tested and will bring you huge returns in a short period of time.',
    'How do you create a direct mail advertising campaign that gets results?  The following tips on creating a direct mail advertising campaign have been street-tested and will bring you huge returns in a short period of time.',
    'How do you create a direct mail advertising campaign that gets results?  The following tips on creating a direct mail advertising campaign have been street-tested and will bring you huge returns in a short period of time.',
    'How do you create a direct mail advertising campaign that gets results?  The following tips on creating a direct mail advertising campaign have been street-tested and will bring you huge returns in a short period of time.'
  ];
  List<int> likes = [
    324,324,324,324
  ];
  bool isClicked = false;
  bool bookmarkSelected = false;
  List<dynamic> bookmark = [];
  List<dynamic> like = [];

  void didChangeDependencies() {
    setState(() {

    });
    sharedMethod();
    super.didChangeDependencies();
  }

//  @override
//  void initState() {
//    _focusNode.addListener(() {
//      if (_focusNode.hasFocus == false) _comment.clear();
//    });
//    super.initState();
//  }
  void sharedMethod() async{
    print('sharedMethod');
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setBool('focusNode', _focusNode.hasFocus);
  }


  bool clicked = false;
  bool isSelected = false;
  FocusNode _focusNode = new FocusNode();
  String _path = '-';
  bool uploaded = false;
  bool _pickFileInProgress = false;
  bool _iosPublicDataUTI = true;
  bool _checkByCustomExtension = false;
  bool _checkByMimeType = false;
  _pickDocument() async {
    String result;
    try {
      setState(() {
        _path = '-';
        _pickFileInProgress = true;
      });
      final _utiController = TextEditingController(
        text: 'com.sidlatau.example.mwfbak',
      );

      final _extensionController = TextEditingController(
        text: 'mwfbak',
      );

      final _mimeTypeController = TextEditingController(
        text: 'application/pdf image/png',
      );
      FlutterDocumentPickerParams params = FlutterDocumentPickerParams(
        allowedFileExtensions: _checkByCustomExtension
            ? _extensionController.text
            .split(' ')
            .where((x) => x.isNotEmpty)
            .toList()
            : null,
        allowedUtiTypes: _iosPublicDataUTI
            ? null
            : _utiController.text
            .split(' ')
            .where((x) => x.isNotEmpty)
            .toList(),
        allowedMimeTypes: _checkByMimeType
            ? _mimeTypeController.text
            .split(' ')
            .where((x) => x.isNotEmpty)
            .toList()
            : null,
      );

      result = await FlutterDocumentPicker.openDocument(params: params);
    } catch (e) {
      print(e);
      result = 'Error: $e';
    } finally {
      setState(() {
        _pickFileInProgress = false;
      });
    }

    setState(() {
      uploaded = true;
      _path = result;
      print(_path.length.toString());
    });
  }


  Future<bool> _onWillPop() {
    FocusScope.of(context).requestFocus(new FocusNode());
    AppRoutes.push(context, DiscussionHome());
  }

  final GlobalKey _menuKey = new GlobalKey();

  @override
  Widget build(BuildContext context) {
    var _mediaQueryData = MediaQuery.of(context);
    screenWidthTotal = _mediaQueryData.size.width;
    screenHeightTotal = _mediaQueryData.size.height;
    _safeAreaHorizontal =
        _mediaQueryData.padding.left + _mediaQueryData.padding.right;
    _safeAreaVertical =
        _mediaQueryData.padding.top + _mediaQueryData.padding.bottom;

    screenHeight = screenHeightTotal - _safeAreaVertical;
    screenWidth = screenWidthTotal - _safeAreaHorizontal;
    return  GestureDetector(
        onTap: () {
          setState(() {
            isClicked = false;
          });
          _focusNode.unfocus();
        },
        child: Scaffold(
          body: Container(
            color: _focusNode.hasFocus
                ? Colors.black.withOpacity(0.25)
                : Colors.white,
            margin: EdgeInsets.only(
              top: 0 / Constant.defaultScreenHeight * screenHeight,
            ),
            child: Column(
              children: <Widget>[
                Container(
                  margin: EdgeInsets.only(
                    top: (42 / 720) * screenHeight,
                  ),
                  child: Row(
                    //crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      InkWell(
                        onTap: () {
                          Navigator.pop(context);
                        },
                        child: Container(
                          width: (58 / 360) * screenWidth,
                          height: (30.0 / 720) * screenHeight,
                          margin: EdgeInsets.only(left: 0.0),
                          child: SvgPicture.asset(
                            getPrepareSvgImages.backIcon,
                            height: (5 / 720) * screenHeight,
                            width: (14 / 360) * screenWidth,
                            fit: BoxFit.none,
                          ),
                        ),
                      ),
                      Container(
                        height: (20 / 720) * screenHeight,
                        // margin: EdgeInsets.only(left: (20 / 360) * screenWidth),
                        child: Text(
                          "Comments",
                          style: TextStyle(
                            color: NeutralColors.black,
                            fontSize: (16 / 720) * screenHeight,
                            fontFamily: "IBMPlexSans",
                            fontWeight: FontWeight.w700,
                          ),
                          textAlign: TextAlign.left,
                        ),
                      ),
                    ],
                  ),
                ),
                Expanded(
                  child: ListView.separated(
                    itemCount: comment.length,
                    padding: EdgeInsets.only(bottom: (40)),
                    itemBuilder: (context, index) {
                      return _focusNode.hasFocus?
                        Container(
                        margin: EdgeInsets.only(
                          left: (20 / 360) * screenWidth,
                          top: (24.5 / Constant.defaultScreenHeight) *
                              screenHeight,
                          right: (0 / Constant.defaultScreenWidth) *
                              screenWidth,
                        ),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            /**profile image and name colum**/
                            Container(
                              // color: Colors.pink,
                              child: Row(
                                mainAxisAlignment:
                                MainAxisAlignment.spaceBetween,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Container(
//                                    margin: EdgeInsets.only(
//                                        right: (20 / 360) * screenWidth),
                                    child: Row(
                                      crossAxisAlignment:
                                      CrossAxisAlignment.start,
                                      mainAxisAlignment: MainAxisAlignment.start,
                                      children: <Widget>[
                                        /**Profile pic**/
                                        Container(
                                          height: (40 /
                                              Constant.defaultScreenHeight) *
                                              screenHeight,
                                          width:
                                          (40 / Constant.defaultScreenWidth) *
                                              screenWidth,
                                          margin: EdgeInsets.only(
                                              top: (0 / 720) * screenHeight),
                                          decoration: new BoxDecoration(
                                              shape: BoxShape.circle,
                                              image: new DecorationImage(
                                                  fit: BoxFit.cover,
                                                  image: AssetImage(
                                                      'assets/images/oval-2.png'))),
                                        ),
                                        Container(
                                          child: Column(
                                            children: <Widget>[
                                              Container(
                                                height: (18 /
                                                    Constant
                                                        .defaultScreenHeight) *
                                                    screenHeight,
                                                width: (100 / 360) * screenWidth,
                                                margin: EdgeInsets.only(
                                                    top: (0 /
                                                        Constant
                                                            .defaultScreenHeight) *
                                                        screenHeight,
                                                    left:
                                                    (12 / 360) * screenWidth),
                                                child: Text(
                                                  'Jack Sparrow',
                                                  style: TextStyle(
                                                    color: NeutralColors
                                                        .dark_navy_blue,
                                                    fontWeight: FontWeight.w500,
                                                    fontFamily: "IBMPlexSans",
                                                    fontStyle: FontStyle.normal,
                                                    fontSize: (14 /
                                                        Constant
                                                            .defaultScreenWidth) *
                                                        screenWidth,
                                                  ),
                                                  textAlign: TextAlign.start,
                                                ),
                                              ),
                                              Container(
                                                height: (18 /
                                                    Constant
                                                        .defaultScreenHeight) *
                                                    screenHeight,
                                                width: (100 / 360) * screenWidth,
                                                margin: EdgeInsets.only(
                                                    top: (5 /
                                                        Constant
                                                            .defaultScreenHeight) *
                                                        screenHeight,
                                                    left:
                                                    (12 / 360) * screenWidth),
                                                child: Text(
                                                  SubQueriesCommentsScreenStrings
                                                      .Text_MsgdTime,
                                                  style: TextStyle(
                                                    color:
                                                    NeutralColors.blue_grey,
                                                    fontWeight: FontWeight.w500,
                                                    fontFamily: "IBMPlexSans",
                                                    fontStyle: FontStyle.normal,
                                                    fontSize: (12 /
                                                        Constant
                                                            .defaultScreenWidth) *
                                                        screenWidth,
                                                  ),
                                                  textAlign: TextAlign.start,
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  Align(
                                    alignment: Alignment.topRight,
                                    child: Container(
                                      //width: 10/360 * screenWidth,
                                      //height: 11/720 * screenHeight,
                                      margin:EdgeInsets.only(left: 115/360 * screenWidth,top: 5/720 * screenHeight,right: 0/360 * screenWidth),
                                      child: GestureDetector(
                                        onTap: (){
                                          setState(() {
                                            print(index);
                                            if(bookmark.contains(index)){
                                              bookmark.remove(index);
                                            }
                                            else{
                                              bookmark.add(index);
                                            }
                                            bookmarkSelected = !bookmarkSelected;
                                          });
                                        },
                                        child:Container(
                                          //width: 10/360 * screenWidth,
                                          //height: 11/720 * screenHeight,
                                            child: bookmark.contains(index)?
                                            SvgPicture.asset(
                                                getPrepareSvgImages.bookmarkActive
                                            ):
                                            Image.asset(
                                              'assets/images/icon-bookmark2.png',
                                            )
                                        ),
                                      ),
                                    ),
                                  ),
                                  Align(
                                    alignment: Alignment.topRight,
//                                    margin: EdgeInsets.only(
//                                      right: 0,
//                                      top: 0,
//                                    ),
                                    child: GestureDetector(
                                      onTap: () {
                                        // This is a hack because _PopupMenuButtonState is private.
                                        dynamic state = _menuKey.currentState;
                                        state.showButtonMenu();
                                      },
                                      child: Container(
                                        // color: Colors.red,
                                        margin: EdgeInsets.only(top: 0, right: 0),
                                        height: 25/720 * screenHeight,
                                        child: PopupMenuButton(
                                            icon: Icon(Icons.more_vert,color: NeutralColors.blue_grey),
                                            padding: EdgeInsets.all(0),
                                            // key: _menuKey,
                                            itemBuilder: (_) =>
                                            <PopupMenuItem<String>>[
                                              new PopupMenuItem<String>(
                                                  child: const Text(
                                                    'Report',
                                                    style: TextStyle(
                                                      color: NeutralColors
                                                          .gunmetal,
                                                      fontFamily:
                                                      "IBMPlexSans",
                                                      fontWeight:
                                                      FontWeight
                                                          .normal,
                                                      fontSize: 14,
                                                    ),
                                                  ),
                                                  value: 'Report'),
                                            ],
                                            onSelected: (_) {}),

                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),

                            Container(
                              //height: 46/720 * screenHeight,
                              width: 320/360 * screenHeight,
                              margin: EdgeInsets.only( top: 13/720 * screenHeight, right: 20/360 * screenWidth),
                              child: Text(
                                'What should be my required percentile to get into a IIM A, B, C in CAT 2019.',
                                style: TextStyle(
                                  color: NeutralColors
                                      .dark_navy_blue,
                                  fontFamily:
                                  "IBMPlexSans",
                                  height: 1,
                                  fontWeight: FontWeight.w400,
                                  fontSize: 14/720 * screenHeight,
                                ),
                              ),
                            ),
                            /**Extended Paragraph colum**/

                            Container(
                              margin: EdgeInsets.only(
                                  top: (10 / Constant.defaultScreenHeight) *
                                      screenHeight,
                                  right: (20 / 360) * screenWidth),
                              child: SingleChildScrollView(
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Padding(
                                      padding: const EdgeInsets.all(1.0),
                                      child: ReadMoreText(
                                        comment[index],
                                        trimLines: 3,
                                        colorClickableText: Colors.blue,
                                        trimMode: TrimMode.Line,
                                        trimCollapsedText:
                                        SubQueriesCommentsScreenStrings
                                            .Text_ReadMore,
                                        trimExpandedText:
                                        SubQueriesCommentsScreenStrings
                                            .Text_ReadLess,
                                        style: TextStyle(
                                          color: NeutralColors.gunmetal,
                                          fontWeight: FontWeight.w500,
                                          fontFamily: "IBMPlexSans",
                                          fontStyle: FontStyle.normal,
                                          height: 1.5,
                                          fontSize: (12/720)*screenHeight,
                                        ),
                                      ),
                                    )
                                  ],
                                ),
                              ),
                            ),
                            /**upvotes and reply colum**/
                            Container(
                              margin:EdgeInsets.only(bottom: 24/720 * screenHeight),
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: <Widget>[
                                  Container(
                                    margin: EdgeInsets.only(
                                        top: like.contains(index)?
                                        25 / Constant.defaultScreenHeight * screenHeight:
                                        26 / Constant.defaultScreenHeight * screenHeight
                                    ),
                                    child: InkWell(
                                      onTap: () {
                                        setState(() {
                                          if(like.contains(index)){
                                            like.remove(index);
                                          }
                                          else{
                                            like.add(index);
                                          }
                                          isSelected = !isSelected;
                                        });
                                      },
                                      child: Container(
                                        height: 11/720 * screenHeight,
                                        width: 11/360 * screenWidth,
                                        child: like.contains(index)?
                                        Icon(
                                          Icons.thumb_up,
                                          color: NeutralColors.deep_sky_blue,
                                          size: 11/360 * screenWidth,
                                        ):
                                        SvgPicture.asset(
                                          getSvgIcon.like,
                                          fit: BoxFit.contain,
                                        ),
                                      ),
                                    ),
                                  ),
                                  Container(
                                      margin: EdgeInsets.only(
                                          top: 18 /
                                              Constant.defaultScreenHeight *
                                              screenHeight,
                                          left: 5 / 360 * screenWidth),
                                      decoration: BoxDecoration(
                                        color: Colors.transparent,
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(2)),
                                      ),
                                      child: Container(
                                        margin: EdgeInsets.only(
                                            top: 6 /
                                                Constant.defaultScreenHeight *
                                                screenHeight),
                                        child: Text(
                                          // SubQueriesCommentsScreenStrings.Text_Liked,
                                          like.contains(index) ? '${likes[index]+1} Likes' : '${likes[index]} Likes',
                                          style: TextStyle(
                                            color:  like.contains(index)
                                            ? NeutralColors.deep_sky_blue
                                                : NeutralColors.black,
                                            fontWeight: FontWeight.normal,
                                            fontFamily: "IBMPlexSans",
                                            fontStyle: FontStyle.normal,
                                            fontSize: (12 /
                                                Constant
                                                    .defaultScreenWidth) *
                                                screenWidth,
                                          ),
                                          textAlign: TextAlign.left,
                                        ),
                                      )),
                                  Container(
                                    margin: EdgeInsets.only(
                                        top: (26 /
                                            Constant.defaultScreenHeight) *
                                            screenHeight,
                                        left:
                                        (20 / Constant.defaultScreenWidth) *
                                            screenWidth),
                                    child: Icon(
                                      Icons.chat_bubble_outline,
                                      color: NeutralColors.black,
                                      size: 11 / 360 * screenWidth,
                                    ),
                                  ),
                                  Container(
                                      margin: EdgeInsets.only(
                                          top: 15 /
                                              Constant.defaultScreenHeight *
                                              screenHeight,
                                          left: 5 / 360 * screenWidth),
                                      decoration: BoxDecoration(
                                        color: Colors.transparent,
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(2)),
                                      ),
                                      child: Container(
                                        margin: EdgeInsets.only(
                                            top: 6 /
                                                Constant.defaultScreenHeight *
                                                screenHeight),
                                        child: Text(
                                          '4 Replies',
                                          style: TextStyle(
                                            color: NeutralColors.dark_navy_blue,
                                            fontWeight: FontWeight.normal,
                                            fontFamily: "IBMPlexSans",
                                            fontStyle: FontStyle.normal,
                                            fontSize: (12 /
                                                Constant
                                                    .defaultScreenWidth) *
                                                screenWidth,
                                          ),
                                          textAlign: TextAlign.left,
                                        ),
                                      )),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ):
                      Container(
                        margin: EdgeInsets.only(
                          left: (20 / 360) * screenWidth,
                          top: (24.5 / Constant.defaultScreenHeight) *
                              screenHeight,
                          right: (0 / Constant.defaultScreenWidth) *
                              screenWidth,
                        ),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            /**profile image and name colum**/
                            Container(
                              // color: Colors.pink,
                              child: Row(
                                mainAxisAlignment:
                                MainAxisAlignment.spaceBetween,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Container(
//                                    margin: EdgeInsets.only(
//                                        right: (20 / 360) * screenWidth),
                                    child: Row(
                                      crossAxisAlignment:
                                      CrossAxisAlignment.start,
                                      mainAxisAlignment: MainAxisAlignment.start,
                                      children: <Widget>[
                                        /**Profile pic**/
                                        Container(
                                          height: (40 /
                                              Constant.defaultScreenHeight) *
                                              screenHeight,
                                          width:
                                          (40 / Constant.defaultScreenWidth) *
                                              screenWidth,
                                          margin: EdgeInsets.only(
                                              top: (0 / 720) * screenHeight),
                                          decoration: new BoxDecoration(
                                              shape: BoxShape.circle,
                                              image: new DecorationImage(
                                                  fit: BoxFit.cover,
                                                  image: AssetImage(
                                                      'assets/images/oval-2.png'))),
                                        ),
                                        Container(
                                          child: Column(
                                            children: <Widget>[
                                              Container(
                                                height: (18 /
                                                    Constant
                                                        .defaultScreenHeight) *
                                                    screenHeight,
                                                width: (100 / 360) * screenWidth,
                                                margin: EdgeInsets.only(
                                                    top: (0 /
                                                        Constant
                                                            .defaultScreenHeight) *
                                                        screenHeight,
                                                    left:
                                                    (12 / 360) * screenWidth),
                                                child: Text(
                                                  'Jack Sparrow',
                                                  style: TextStyle(
                                                    color: NeutralColors
                                                        .dark_navy_blue,
                                                    fontWeight: FontWeight.w500,
                                                    fontFamily: "IBMPlexSans",
                                                    fontStyle: FontStyle.normal,
                                                    fontSize: (14 /
                                                        Constant
                                                            .defaultScreenWidth) *
                                                        screenWidth,
                                                  ),
                                                  textAlign: TextAlign.start,
                                                ),
                                              ),
                                              Container(
                                                height: (18 /
                                                    Constant
                                                        .defaultScreenHeight) *
                                                    screenHeight,
                                                width: (100 / 360) * screenWidth,
                                                margin: EdgeInsets.only(
                                                    top: (5 /
                                                        Constant
                                                            .defaultScreenHeight) *
                                                        screenHeight,
                                                    left:
                                                    (12 / 360) * screenWidth),
                                                child: Text(
                                                  SubQueriesCommentsScreenStrings
                                                      .Text_MsgdTime,
                                                  style: TextStyle(
                                                    color:
                                                    NeutralColors.blue_grey,
                                                    fontWeight: FontWeight.w500,
                                                    fontFamily: "IBMPlexSans",
                                                    fontStyle: FontStyle.normal,
                                                    fontSize: (12 /
                                                        Constant
                                                            .defaultScreenWidth) *
                                                        screenWidth,
                                                  ),
                                                  textAlign: TextAlign.start,
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  Align(
                                    alignment: Alignment.topRight,
                                    child: Container(
                                      //width: 10/360 * screenWidth,
                                      //height: 11/720 * screenHeight,
                                      margin:EdgeInsets.only(left: 115/360 * screenWidth,top: 5/720 * screenHeight,right: 0/360 * screenWidth),
                                      child: GestureDetector(
                                        onTap: (){
                                          setState(() {
                                            print(index);
                                            if(bookmark.contains(index)){
                                              bookmark.remove(index);
                                            }
                                            else{
                                              bookmark.add(index);
                                            }
                                            bookmarkSelected = !bookmarkSelected;
                                          });
                                        },
                                        child:Container(
                                          //width: 10/360 * screenWidth,
                                          //height: 11/720 * screenHeight,
                                            child: bookmark.contains(index)?
                                            SvgPicture.asset(
                                                getPrepareSvgImages.bookmarkActive
                                            ):
                                            Image.asset(
                                              'assets/images/icon-bookmark2.png',
                                            )
                                        ),
                                      ),
                                    ),
                                  ),
                                  Align(
                                    alignment: Alignment.topRight,
//                                    margin: EdgeInsets.only(
//                                      right: 0,
//                                      top: 0,
//                                    ),
                                    child: GestureDetector(
                                      onTap: () {
                                        // This is a hack because _PopupMenuButtonState is private.
                                        dynamic state = _menuKey.currentState;
                                        state.showButtonMenu();
                                      },
                                      child: Container(
                                        // color: Colors.red,
                                        margin: EdgeInsets.only(top: 0, right: 0),
                                        height: 25/720 * screenHeight,
                                        child: PopupMenuButton(
                                            icon: Icon(Icons.more_vert,color: NeutralColors.blue_grey),
                                            padding: EdgeInsets.all(0),
                                            // key: _menuKey,
                                            itemBuilder: (_) =>
                                            <PopupMenuItem<String>>[
                                              new PopupMenuItem<String>(
                                                  child: const Text(
                                                    'Report',
                                                    style: TextStyle(
                                                      color: NeutralColors
                                                          .gunmetal,
                                                      fontFamily:
                                                      "IBMPlexSans",
                                                      fontWeight:
                                                      FontWeight
                                                          .normal,
                                                      fontSize: 14,
                                                    ),
                                                  ),
                                                  value: 'Report'),
                                            ],
                                            onSelected: (_) {}),

                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            /**Extended Paragraph colum**/

                            Container(
                              margin: EdgeInsets.only(
                                  top: (10 / Constant.defaultScreenHeight) *
                                      screenHeight,
                                  right: (20 / 360) * screenWidth),
                              child: SingleChildScrollView(
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Padding(
                                      padding: const EdgeInsets.all(1.0),
                                      child: ReadMoreText(
                                        comment[index],
//                                        SubQueriesCommentsScreenStrings
//                                            .ReadMoreText_Content,
                                        //trimLines: 3,
                                        //colorClickableText: Colors.blue,
                                        //trimMode: TrimMode.Line,
//                                        trimCollapsedText:
//                                        SubQueriesCommentsScreenStrings
//                                            .Text_ReadMore,
//                                        trimExpandedText:
//                                        SubQueriesCommentsScreenStrings
//                                            .Text_ReadLess,
                                        style: TextStyle(
                                          color: NeutralColors.gunmetal,
                                          fontWeight: FontWeight.w500,
                                          fontFamily: "IBMPlexSans",
                                          fontStyle: FontStyle.normal,
                                          height: 1.5,
                                          fontSize: (12/720)*screenHeight,
                                        ),
                                      ),
                                    )
                                  ],
                                ),
                              ),
                            ),
                            /**upvotes and reply colum**/
                            Container(
                              margin:EdgeInsets.only(bottom: 24/720 * screenHeight),
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: <Widget>[
                                  Container(
                                    margin: EdgeInsets.only(
                                        top: like.contains(index)?
                                        25 / Constant.defaultScreenHeight * screenHeight:
                                        26 / Constant.defaultScreenHeight * screenHeight
                                    ),
                                    child: InkWell(
                                      onTap: () {
                                        setState(() {
                                          if(like.contains(index)){
                                            like.remove(index);
                                          }
                                          else{
                                            like.add(index);
                                          }
                                          isSelected = !isSelected;
                                        });
                                      },
                                      child: Container(
                                        height: 11/720 * screenHeight,
                                        width: 11/360 * screenWidth,
                                        child: like.contains(index)?
                                        Icon(
                                          Icons.thumb_up,
                                          color: NeutralColors.deep_sky_blue,
                                          size: 11/360 * screenWidth,
                                        ):
                                        SvgPicture.asset(
                                          getSvgIcon.like,
                                          fit: BoxFit.contain,
                                        ),
                                      ),
                                    ),
                                  ),
                                  Container(
                                      margin: EdgeInsets.only(
                                          top: 18 /
                                              Constant.defaultScreenHeight *
                                              screenHeight,
                                          left: 5 / 360 * screenWidth),
                                      decoration: BoxDecoration(
                                        color: Colors.transparent,
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(2)),
                                      ),
                                      child: Container(
                                        margin: EdgeInsets.only(
                                            top: 6 /
                                                Constant.defaultScreenHeight *
                                                screenHeight),
                                        child: Text(
                                          //SubQueriesCommentsScreenStrings.Text_Liked,
                                          like.contains(index) ? '${likes[index]+1} Likes' : '${likes[index]} Likes',
                                          style: TextStyle(
                                            color:  like.contains(index) ? NeutralColors.deep_sky_blue:NeutralColors.dark_navy_blue,
                                            fontWeight: FontWeight.normal,
                                            fontFamily: "IBMPlexSans",
                                            fontStyle: FontStyle.normal,
                                            fontSize: (12 /
                                                Constant
                                                    .defaultScreenWidth) *
                                                screenWidth,
                                          ),
                                          textAlign: TextAlign.left,
                                        ),
                                      )),
                                  Container(
                                    margin: EdgeInsets.only(
                                        top: (26 /
                                            Constant.defaultScreenHeight) *
                                            screenHeight,
                                        left:
                                        (20 / Constant.defaultScreenWidth) *
                                            screenWidth),
                                    child: Icon(
                                      Icons.chat_bubble_outline,
                                      color: NeutralColors.black,
                                      size: 11 / 360 * screenWidth,
                                    ),
                                  ),
                                  Container(
                                      margin: EdgeInsets.only(
                                          top: 15 /
                                              Constant.defaultScreenHeight *
                                              screenHeight,
                                          left: 5 / 360 * screenWidth),
                                      decoration: BoxDecoration(
                                        color: Colors.transparent,
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(2)),
                                      ),
                                      child: Container(
                                        margin: EdgeInsets.only(
                                            top: 6 /
                                                Constant.defaultScreenHeight *
                                                screenHeight),
                                        child: Text(
                                          '232 Comments',
                                          style: TextStyle(
                                            color: NeutralColors.dark_navy_blue,
                                            fontWeight: FontWeight.normal,
                                            fontFamily: "IBMPlexSans",
                                            fontStyle: FontStyle.normal,
                                            fontSize: (12 /
                                                Constant
                                                    .defaultScreenWidth) *
                                                screenWidth,
                                          ),
                                          textAlign: TextAlign.left,
                                        ),
                                      )),
                                ],
                              ),
                            ),
                          ],
                        ),
                      );
                    },
                    separatorBuilder: (context, index) {
                      return Divider();
                    },
                  ),
                ),
                Container(
                  color: _focusNode.hasFocus
                      ? Colors.white
                      : Colors.transparent,
                  height: 75/720 * screenHeight,
                  margin: EdgeInsets.only(
                    //left: 20/360 * screenWidth,
                     //bottom: (10 / 720) * screenHeight,
                      top: (12 / Constant.defaultScreenHeight) * screenHeight
                  ),
                  child: Row(
                    children: <Widget>[
                      /**Circular Avatar**/
                      Container(
                        height: (30 / 720) * screenHeight,
                        width: (30 / 360) * screenWidth,
                        margin: EdgeInsets.only(
                            top: (0 / 720) * screenHeight,
                            left: (20 / 360) * screenWidth),
                        decoration: new BoxDecoration(
                            shape: BoxShape.circle,
                            image: new DecorationImage(
                                fit: BoxFit.cover,
                                image: AssetImage(
                                    'assets/images/oval-2.png'))),
                      ),
                      Container(
                        //color: _focusNode.hasFocus?Colors.white:Colors.transparent,
                        //height: (40 / 720) * screenHeight,
                        width: (280 / 360) * screenWidth,
                        margin: EdgeInsets.only(
                          left: (10 / 360) * screenWidth,
                          right: (0 / 360) * screenWidth,
                          top: 10/720 * screenHeight,
                          bottom: 10/720 * screenHeight
                        ),
                        decoration: BoxDecoration(
                          //  backgroundBlendMode: BlendMode.exclusion,
                          border: Border.all(color: AccentColors.iceBlue),
                          color: Colors.white,
                          borderRadius: BorderRadius.all(Radius.circular(5)),
                        ),
                        child: Padding(
                          padding: EdgeInsets.only(
                              left: (10 / Constant.defaultScreenWidth) *
                                  screenWidth,
                              top: (10 / Constant.defaultScreenHeight) *
                                  screenHeight,
                              bottom: (0 / Constant.defaultScreenHeight) *
                                  screenHeight),
                          child: TextFormField(
                            controller: _comment,
                            focusNode: _focusNode,
                            //keyboardType: TextInputType.multiline,
                            textInputAction: TextInputAction.done,
                            //maxLength: null,
                            //maxLines: 4,
                            autovalidate: false,
                            decoration: InputDecoration(
                              border: InputBorder.none,
                              hintText: "Comments",
                              suffixIcon: IconButton(
                                  icon: Image.asset(
                                    "assets/images/clip-4.png",
                                    fit: BoxFit.none,
                                    color: Color.fromARGB(255, 0, 171, 251),
                                  ),
                                  onPressed: () {
                                    _pickFileInProgress ? null : _pickDocument();
                                    print("pick");
                                  })
                            ),
                            style: TextStyle(
                              color: NeutralColors.blue_grey,
                              fontWeight: FontWeight.normal,
                              fontFamily: "IBMPlexSans",
                              fontStyle: FontStyle.normal,
                              fontSize: (14 / 360) * screenWidth,
                            ),
                            onFieldSubmitted: (val){
                              setState(() {
                                comment.add(val);
                                likes.add(324);
                                print(comment);
                                _comment.clear();
                              });
                              return;
                            },
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      );

  }
}
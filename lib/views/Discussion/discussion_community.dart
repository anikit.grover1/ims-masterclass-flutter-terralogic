import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:imsindia/components/navigation_bar.dart';
import 'package:imsindia/routers/routes.dart';
import 'package:imsindia/utils/colors.dart';
import 'package:imsindia/resources/strings/GDPI_labels.dart';
import 'package:imsindia/utils/svg_images/channel_svg_icons.dart';
import 'package:imsindia/utils/screen_util.dart';
import 'package:imsindia/views/Discussion/discussion_comments.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:imsindia/resources/strings/prepare.dart';
import 'package:imsindia/views/Arun/ReadMoreText.dart';
import 'package:imsindia/utils/svg_icons.dart';
import 'package:imsindia/utils/svg_images/prepare_svg_images.dart';


var screenWidthTotal;
var screenHeightTotal;

var _safeAreaHorizontal;

var _safeAreaVertical;

var screenHeight;

var screenWidth;

List<dynamic> joinCommunitiesImages = [
  "assets/images/gdpi-sample.png",
  "assets/images/gdpi-sample-2.png",
  "assets/images/gdpi-sample-3.png",
  "assets/images/mask-7.png"
];
List<dynamic> yourCommunitiesImages = [];
List<dynamic> joinCommunitiesNames = [
  "FMS-CHENNAI",
  "FMS-BENGALURU",
  "FMS-HYDERABAD",
  "FMS-DELHI"
];
List<dynamic> yourCommunitiesNames = [];
int likedCommunities = 0;

List<int> list = [324, 325];


bool isSelected =false;
bool bookmarkSelected = false;
List<dynamic> bookmark = [];
List<dynamic> like = [];




class DiscussionCommunity extends StatefulWidget {
  @override
  DiscussionCommunityState createState() => DiscussionCommunityState();
}

class DiscussionCommunityState extends State<DiscussionCommunity>{

  OverlayEntry _overlayEntry;
  bool tenthDetails = false;
  bool uploaded = false;
  bool isClicked = false;
  bool clicked = false;
  bool isSelected = false;
  bool value = false;
//List<int> numbers = [1, 2, 3, 4, 5, 6];

  void didChangeDependencies() {
    sharedMethod();
    super.didChangeDependencies();
  }
  void sharedMethod() async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setBool('focusNode', _focusNode.hasFocus);
  }



  FocusNode _focusNode = new FocusNode();

  Future<bool> _onWillPop() {
    FocusScope.of(context).requestFocus(new FocusNode());
  }

  final GlobalKey _menuKey = new GlobalKey();



  @override
  Widget build(BuildContext context) {
    final screenHeight = MediaQuery.of(context).size.height;
    final screenWidth = MediaQuery.of(context).size.width;
    var _mediaQueryData = MediaQuery.of(context);
    screenWidthTotal = _mediaQueryData.size.width;
    screenHeightTotal = _mediaQueryData.size.height;

    return  Scaffold(
          body: SingleChildScrollView(
            child: Container(
              //color: Colors.white,
              child: Column(
                //crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  Container(
                   // height: 300,
                    child:Column(
                      children: <Widget>[

                        (likedCommunities > 0)
                            ? Container(
                          margin: EdgeInsets.only(left: 25, top: 20),
                          child: Align(
                            alignment: Alignment.topLeft,
                            child: Text(
                              GDPIProcessLabels.your_Communities,
                              style: TextStyle(
                                color: Color.fromARGB(255, 0, 3, 44),
                                fontSize: 18,
                                fontFamily: "IBMPlexSans",
                                fontWeight: FontWeight.w700,
                              ),
                              textAlign: TextAlign.left,
                            ),
                          ),
                        )
                            : Text(''),
                        (likedCommunities > 0)
                            ?Container(
                          height: 160,
                       //   margin: EdgeInsets.only(right: 150),
                          child: ListView.builder(
                              scrollDirection: Axis.horizontal,
                              itemCount: yourCommunitiesImages.length,
                              itemBuilder: (context, index) {
                                return Container(
                                  width: 100,
                                  margin: EdgeInsets.only(left: 20, top: 20,right:(index==yourCommunitiesImages.length-1)?20:0 ),
                                  child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.stretch,
                                    children: [
                                      Align(
                                        alignment: Alignment.topLeft,
                                        child: Container(
                                          width: 100,
                                          height: 80,
                                          child: ClipRRect(
                                            borderRadius: BorderRadius.circular(4),
                                            child: Image.asset(
                                              yourCommunitiesImages[index],
                                              fit: BoxFit.none,
                                            ),
                                          ),
                                        ),
                                      ),
                                      Align(
                                        alignment: Alignment.topLeft,
                                        child: Container(
                                          margin: EdgeInsets.only(top: 20),
                                          width: 100,
                                          height: 35,
                                          child: Text(
                                            yourCommunitiesNames[index],
                                            maxLines: 2,
                                            style: TextStyle(
                                              color: Color.fromARGB(255, 0, 3, 44),
                                              fontSize: 14,
                                              fontFamily: "IBMPlexSans",
                                              fontWeight: FontWeight.w500,
                                            ),
                                            textAlign: TextAlign.left,
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                );
                              }),
                        )
                            : Text(''),
                        Container(
                          margin: EdgeInsets.only(left: 20,top: likedCommunities>0? 20/720 * screenHeight: 0),
                          child: Align(
                            alignment: Alignment.topLeft,
                            child: Text(
                              GDPIProcessLabels.join_Communities,
                              style: TextStyle(
                                color: Color.fromARGB(255, 0, 3, 44),
                                fontSize: 18,
                                fontFamily: "IBMPlexSans",
                                fontWeight: FontWeight.w700,
                              ),
                              textAlign: TextAlign.left,
                            ),
                          ),
                        ),
                        Container(
                          height: 160,
                          child: ListView.builder(
                              scrollDirection: Axis.horizontal,
                              itemCount: joinCommunitiesImages.length,
                              itemBuilder: (context, index) {
                                return Stack(
                                  children: [
                                    Container(
                                      width: 100,
                                      margin: EdgeInsets.only(left: 20, top: 20,right: (index==joinCommunitiesImages.length-1)?20:0),
                                      child: Column(
                                        crossAxisAlignment: CrossAxisAlignment.stretch,
                                        children: [
                                          Align(
                                            alignment: Alignment.topLeft,
                                            child: Container(
                                              width: 100,
                                              height: 80,
                                              child: ClipRRect(
                                                borderRadius: BorderRadius.circular(4),
                                                child: Image.asset(
                                                  joinCommunitiesImages[index],
                                                  fit: BoxFit.none,
                                                ),
                                              ),
                                            ),
                                          ),
                                          Align(
                                            alignment: Alignment.topLeft,
                                            child: Container(
                                              height: 35,
                                              margin: EdgeInsets.only(top: 20),
                                              width: 100,
                                              child: Text(
                                                joinCommunitiesNames[index],
                                                maxLines: 2,
                                                style: TextStyle(
                                                  color: Color.fromARGB(255, 0, 3, 44),
                                                  fontSize: 14,
                                                  fontFamily: "IBMPlexSans",
                                                  fontWeight: FontWeight.w500,
                                                ),
                                                textAlign: TextAlign.left,
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                    Positioned(
                                      top: 23,
                                      right: (index==joinCommunitiesImages.length-1)?25:4,
                                      child: GestureDetector(
                                        child: CircleAvatar(
                                          radius: 12.0,
                                          backgroundColor: Colors.white,
                                          child: Icon(
                                            Icons.add,
                                            size: 20.0,
                                          ),
                                        ),
                                        onTap: () {
                                          setState(() {
                                            setState(() {
                                              if(yourCommunitiesImages.contains(joinCommunitiesImages[index])){
                                              }else{
                                                likedCommunities = likedCommunities + 1;
                                                yourCommunitiesImages
                                                    .add(joinCommunitiesImages[index]);
                                                yourCommunitiesNames
                                                    .add(joinCommunitiesNames[index]);
                                              }});
                                          });
                                        },
                                      ),
                                    ),
                                  ],
                                );
                              }),
                        ),

                      ],
                    ),
                  ),
                  Padding(
                  padding: EdgeInsets.only(left: 20/360 * screenWidth,right: 20/360 * screenWidth,top: 39.5/720*screenHeight),
    child: Divider(),
    ),

                  Container(
                  height: 650/720 * screenHeight,
                    child: ListView.separated(
                      physics: NeverScrollableScrollPhysics(),
                      itemCount: 2,
                      padding: EdgeInsets.only(bottom: (40)),
                      itemBuilder: (context, index) {
                        return index % 2 == 1 ?
                        Container(
                          margin: EdgeInsets.only(
                            left: (20 / 360) * screenWidth,
                            top: (24.5 / Constant.defaultScreenHeight) *
                                screenHeight,
                            right: (20 / Constant.defaultScreenWidth) *
                                screenWidth,
                          ),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              /**profile image and name colum**/
                              Container(
                                // color: Colors.pink,
                                child: Row(
                                  mainAxisAlignment:
                                  MainAxisAlignment.spaceBetween,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Container(
//                                    margin: EdgeInsets.only(
//                                        right: (20 / 360) * screenWidth),
                                      child: Row(
                                        crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                        mainAxisAlignment: MainAxisAlignment.start,
                                        children: <Widget>[
                                          /**Profile pic**/
                                          Container(
                                            height: (40 /
                                                Constant.defaultScreenHeight) *
                                                screenHeight,
                                            width:
                                            (40 / Constant.defaultScreenWidth) *
                                                screenWidth,
                                            margin: EdgeInsets.only(
                                                top: (0 / 720) * screenHeight),
                                            decoration: new BoxDecoration(
                                                shape: BoxShape.circle,
                                                image: new DecorationImage(
                                                    fit: BoxFit.cover,
                                                    image: AssetImage(
                                                        'assets/images/oval-2.png'))),
                                          ),
                                          Container(
                                            child: Column(
                                              children: <Widget>[
                                                 Container(
                                            height: (20 /
                                                Constant
                                                    .defaultScreenHeight) *
                                                screenHeight,
                                            width: (100 / 360) * screenWidth,
                                            margin: EdgeInsets.only(
                                                top: (0 /
                                                    Constant
                                                        .defaultScreenHeight) *
                                                    screenHeight,
                                                left:
                                                (12 / 360) * screenWidth),
                                            child: Text(
                                              SubQueriesCommentsScreenStrings
                                                  .Text_UserName,
                                              style: TextStyle(
                                                color: NeutralColors
                                                    .dark_navy_blue,
                                                fontWeight: FontWeight.w500,
                                                fontFamily: "IBMPlexSans",
                                                fontStyle: FontStyle.normal,
                                                fontSize: (14 /
                                                    Constant
                                                        .defaultScreenWidth) *
                                                    screenWidth,
                                              ),
                                              textAlign: TextAlign.start,
                                            ),
                                          ),
                                                Container(
                                            height: (18 /
                                                Constant
                                                    .defaultScreenHeight) *
                                                screenHeight,
                                            width: (100 / 360) * screenWidth,
                                            margin: EdgeInsets.only(
                                                top: (5 /
                                                    Constant
                                                        .defaultScreenHeight) *
                                                    screenHeight,
                                                left:
                                                (12 / 360) * screenWidth),
                                            child: Text(
                                              SubQueriesCommentsScreenStrings
                                                  .Text_MsgdTime,
                                              style: TextStyle(
                                                color:
                                                NeutralColors.blue_grey,
                                                fontWeight: FontWeight.w500,
                                                fontFamily: "IBMPlexSans",
                                                fontStyle: FontStyle.normal,
                                                fontSize: (12 /
                                                    Constant
                                                        .defaultScreenWidth) *
                                                    screenWidth,
                                              ),
                                              textAlign: TextAlign.start,
                                            ),
                                          ),
                                              ],
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                    Container(
                                      //width: 10/360 * screenWidth,
                                      //height: 11/720 * screenHeight,
                                      margin:EdgeInsets.only(left: 90/360 * screenWidth,top: 5/720 * screenHeight,right: 0/360 * screenWidth),
                                      child: GestureDetector(
                                        onTap: (){
                                          setState(() {
                                            print(index);
                                            if(bookmark.contains(index)){
                                              bookmark.remove(index);
                                            }
                                            else{
                                              bookmark.add(index);
                                            }
                                            bookmarkSelected = !bookmarkSelected;
                                          });
                                        },
                                        child:Container(
                                          //width: 10/360 * screenWidth,
                                          //height: 11/720 * screenHeight,
                                            child: bookmark.contains(index)?
                                            SvgPicture.asset(
                                                getPrepareSvgImages.bookmarkActive
                                            ):
                                            Image.asset(
                                              'assets/images/icon-bookmark2.png',
                                            )
                                        ),
                                      ),
                                    ),
                                    Container(
                                      margin: EdgeInsets.only(
                                        right: 0,
                                        top: 0,
                                      ),
                                      child: GestureDetector(
                                        onTap: () {
                                          // This is a hack because _PopupMenuButtonState is private.
                                          dynamic state = _menuKey.currentState;
                                          state.showButtonMenu();
                                        },
                                        child: Container(
                                          // color: Colors.red,
                                          margin: EdgeInsets.only(top: 0, right: 0),
                                          height: 13/720 * screenHeight,
                                          child: PopupMenuButton(
                                              icon: Icon(Icons.more_vert,color: NeutralColors.blue_grey),
                                              padding: EdgeInsets.all(0),
                                              // key: _menuKey,
                                              itemBuilder: (_) =>
                                              <PopupMenuItem<String>>[
                                                new PopupMenuItem<String>(
                                                    child: const Text(
                                                      'Report',
                                                      style: TextStyle(
                                                        color: NeutralColors
                                                            .gunmetal,
                                                        fontFamily:
                                                        "IBMPlexSans",
                                                        fontWeight:
                                                        FontWeight
                                                            .normal,
                                                        fontSize: 14,
                                                      ),
                                                    ),
                                                    value: 'Report'),
                                              ],
                                              onSelected: (_) {}),

                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              Container(
                                height: 180/720 * screenHeight,
                                width: 320/360 * screenWidth ,
                                margin:EdgeInsets.only( top: 14/720 * screenHeight, right: 10/360 * screenWidth,left: 20/360),
                                child: ClipRRect(
                                  borderRadius: BorderRadius.circular(5),
                                  child: Image.asset(
                                    'assets/images/mask.png',fit: BoxFit.none,
                                  ),
                                ),
                              ),

                              Container(
                                //height: 46/720 * screenHeight,
                                width: 320/360 * screenHeight,
                                margin: EdgeInsets.only( top: 8/720 * screenHeight, right: 20/360 * screenWidth),
                                child: Text(
                                  'What should be my required percentile to get into a IIM A, B, C in CAT 2019.',
                                  style: TextStyle(
                                    color: NeutralColors
                                        .dark_navy_blue,
                                    fontFamily:
                                    "IBMPlexSans",
                                    height: 1,
                                    fontWeight: FontWeight.w400,
                                    fontSize: 14/720 * screenHeight,
                                  ),
                                ),
                              ),
                              /**upvotes and reply colum**/
                              Container(
                                margin: EdgeInsets.only(
                                    bottom: 24 / 720 * screenHeight),
                                child:InkWell(
                                  onTap: () {
                                    setState(() {
                                      if (like.contains(index)) {
                                        like.remove(index);
                                      } else {
                                        like.add(index);
                                      }
                                      isSelected = !isSelected;
                                    });
                                  },
                                  child: Row(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    children: <Widget>[
                                      Container(
                                        margin: EdgeInsets.only(
                                            top: like.contains(index)
                                                ? 25 /
                                                    Constant
                                                        .defaultScreenHeight *
                                                    screenHeight
                                                : 26 /
                                                    Constant
                                                        .defaultScreenHeight *
                                                    screenHeight),
                                        child: Container(
                                          height: 11 / 720 * screenHeight,
                                          width: 11 / 360 * screenWidth,
                                          child: like.contains(index)
                                              ? Icon(
                                                  Icons.thumb_up,
                                                  color: NeutralColors
                                                      .deep_sky_blue,
                                                  size: 11 / 360 * screenWidth,
                                                )
                                              : SvgPicture.asset(
                                                  getSvgIcon.like,
                                                  fit: BoxFit.contain,
                                                ),
                                        ),
                                      ),
                                      Container(
                                          margin: EdgeInsets.only(
                                              top: 18 /
                                                  Constant.defaultScreenHeight *
                                                  screenHeight,
                                              left: 5 / 360 * screenWidth),
                                          decoration: BoxDecoration(
                                            color: Colors.transparent,
                                            borderRadius: BorderRadius.all(
                                                Radius.circular(2)),
                                          ),
                                          child: Container(
                                            margin: EdgeInsets.only(
                                                top: 6 /
                                                    Constant
                                                        .defaultScreenHeight *
                                                    screenHeight),
                                            child: Text(
                                              // SubQueriesCommentsScreenStrings.Text_Liked,
                                              like.contains(index)
                                                  ? '${list[index] + 1} Likes'
                                                  : '${list[index]} Likes',
                                              style: TextStyle(
                                                color: like.contains(index)
                                                    ? NeutralColors
                                                        .deep_sky_blue
                                                    : NeutralColors.black,
                                                fontWeight: FontWeight.normal,
                                                fontFamily: "IBMPlexSans",
                                                fontStyle: FontStyle.normal,
                                                fontSize: (12 /
                                                        Constant
                                                            .defaultScreenWidth) *
                                                    screenWidth,
                                              ),
                                              textAlign: TextAlign.left,
                                            ),
                                          )),

                                    Container(
                                      margin: EdgeInsets.only(
                                          top: (26 /
                                                  Constant
                                                      .defaultScreenHeight) *
                                              screenHeight,
                                          left: (20 /
                                                  Constant.defaultScreenWidth) *
                                              screenWidth),
                                      child: Icon(
                                        Icons.chat_bubble_outline,
                                        color: NeutralColors.black,
                                        size: 11 / 360 * screenWidth,
                                      ),
                                    ),
                                    Container(
                                        margin: EdgeInsets.only(
                                            top: 15 /
                                                Constant.defaultScreenHeight *
                                                screenHeight,
                                            left: 5 / 360 * screenWidth),
                                        decoration: BoxDecoration(
                                          color: Colors.transparent,
                                          borderRadius: BorderRadius.all(
                                              Radius.circular(2)),
                                        ),
                                        child: GestureDetector(
                                          onTap: () {
                                            AppRoutes.push(
                                                context, DiscussionComments());
                                          },
                                          child: Container(
                                            margin: EdgeInsets.only(
                                                top: 6 /
                                                    Constant
                                                        .defaultScreenHeight *
                                                    screenHeight),
                                            child: Text(
                                              '232 Comments',
                                              style: TextStyle(
                                                color: NeutralColors
                                                    .dark_navy_blue,
                                                fontWeight: FontWeight.normal,
                                                fontFamily: "IBMPlexSans",
                                                fontStyle: FontStyle.normal,
                                                fontSize: (12 /
                                                        Constant
                                                            .defaultScreenWidth) *
                                                    screenWidth,
                                              ),
                                              textAlign: TextAlign.left,
                                            ),
                                          ),
                                        )),
                                  ],
                                ),
                              ),),

                            ],
                          ),
                        )
                        : Container(
                          margin: EdgeInsets.only(
                            left: (20 / 360) * screenWidth,
                            top: (24.5 / Constant.defaultScreenHeight) *
                                screenHeight,
                            right: (20 / Constant.defaultScreenWidth) *
                                screenWidth,
                          ),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              /**profile image and name colum**/
                              Container(
                                // color: Colors.pink,
                                child: Row(
                                  mainAxisAlignment:
                                  MainAxisAlignment.spaceBetween,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Container(
//                                    margin: EdgeInsets.only(
//                                        right: (20 / 360) * screenWidth),
                                      child: Row(
                                        crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                        mainAxisAlignment: MainAxisAlignment.start,
                                        children: <Widget>[
                                          /**Profile pic**/
                                          Container(
                                            height: (40 /
                                                Constant.defaultScreenHeight) *
                                                screenHeight,
                                            width:
                                            (40 / Constant.defaultScreenWidth) *
                                                screenWidth,
                                            margin: EdgeInsets.only(
                                                top: (0 / 720) * screenHeight),
                                            decoration: new BoxDecoration(
                                                shape: BoxShape.circle,
                                                image: new DecorationImage(
                                                    fit: BoxFit.cover,
                                                    image: AssetImage(
                                                        'assets/images/oval-2.png'))),
                                          ),
                                          Container(
                                            child: Column(
                                              children: <Widget>[
                                                       Container(
                                            height: (20 /
                                                Constant
                                                    .defaultScreenHeight) *
                                                screenHeight,
                                            width: (100 / 360) * screenWidth,
                                            margin: EdgeInsets.only(
                                                top: (0 /
                                                    Constant
                                                        .defaultScreenHeight) *
                                                    screenHeight,
                                                left:
                                                (12 / 360) * screenWidth),
                                            child: Text(
                                              SubQueriesCommentsScreenStrings
                                                  .Text_UserName,
                                              style: TextStyle(
                                                color: NeutralColors
                                                    .dark_navy_blue,
                                                fontWeight: FontWeight.w500,
                                                fontFamily: "IBMPlexSans",
                                                fontStyle: FontStyle.normal,
                                                fontSize: (14 /
                                                    Constant
                                                        .defaultScreenWidth) *
                                                    screenWidth,
                                              ),
                                              textAlign: TextAlign.start,
                                            ),
                                          ),
                                                Container(
                                            height: (18 /
                                                Constant
                                                    .defaultScreenHeight) *
                                                screenHeight,
                                            width: (100 / 360) * screenWidth,
                                            margin: EdgeInsets.only(
                                                top: (5 /
                                                    Constant
                                                        .defaultScreenHeight) *
                                                    screenHeight,
                                                left:
                                                (12 / 360) * screenWidth),
                                            child: Text(
                                              SubQueriesCommentsScreenStrings
                                                  .Text_MsgdTime,
                                              style: TextStyle(
                                                color:
                                                NeutralColors.blue_grey,
                                                fontWeight: FontWeight.w500,
                                                fontFamily: "IBMPlexSans",
                                                fontStyle: FontStyle.normal,
                                                fontSize: (12 /
                                                    Constant
                                                        .defaultScreenWidth) *
                                                    screenWidth,
                                              ),
                                              textAlign: TextAlign.start,
                                            ),
                                          ),
                                              ],
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                    Container(
                                      //width: 10/360 * screenWidth,
                                      //height: 11/720 * screenHeight,
                                      margin:EdgeInsets.only(left: 90/360 * screenWidth,top: 5/720 * screenHeight,right: 0/360 * screenWidth),
                                      child: GestureDetector(
                                        onTap: (){
                                          setState(() {
                                            if(bookmark.contains(index)){
                                              bookmark.remove(index);
                                            }
                                            else{
                                              bookmark.add(index);
                                            }
                                            bookmarkSelected = !bookmarkSelected;
                                          });
                                        },
                                        child:Container(
                                          //width: 10/360 * screenWidth,
                                          //height: 11/720 * screenHeight,
                                            child: bookmark.contains(index)?
                                            SvgPicture.asset(
                                                getPrepareSvgImages.bookmarkActive
                                            ):
                                            Image.asset(
                                              'assets/images/icon-bookmark2.png',
                                            )
                                        ),
                                      ),
                                    ),
                                    Container(
                                      margin: EdgeInsets.only(
                                        right: 0,
                                        top: 0,
                                      ),
                                      child: GestureDetector(
                                        onTap: () {
                                          // This is a hack because _PopupMenuButtonState is private.
                                          dynamic state = _menuKey.currentState;
                                          state.showButtonMenu();
                                        },
                                        child: Container(
                                          // color: Colors.red,
                                          margin: EdgeInsets.only(top: 0, right: 0),
                                          height: 13/720 * screenHeight,
                                          child: PopupMenuButton(
                                              icon: Icon(Icons.more_vert,color: NeutralColors.blue_grey),
                                              padding: EdgeInsets.all(0),
                                              // key: _menuKey,
                                              itemBuilder: (_) =>
                                              <PopupMenuItem<String>>[
                                                new PopupMenuItem<String>(
                                                    child: const Text(
                                                      'Report',
                                                      style: TextStyle(
                                                        color: NeutralColors
                                                            .gunmetal,
                                                        fontFamily:
                                                        "IBMPlexSans",
                                                        fontWeight:
                                                        FontWeight
                                                            .normal,
                                                        fontSize: 14,
                                                      ),
                                                    ),
                                                    value: 'Report'),
                                              ],
                                              onSelected: (_) {}),

                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),

                              Container(
                                //height: 46/720 * screenHeight,
                                width: 320/360 * screenHeight,
                                margin: EdgeInsets.only( top: 13/720 * screenHeight, right: 20/360 * screenWidth),
                                child: Text(
                                  'What should be my required percentile to get into a IIM A, B, C in CAT 2019.',
                                  style: TextStyle(
                                    color: NeutralColors
                                        .dark_navy_blue,
                                    fontFamily:
                                    "IBMPlexSans",
                                    height: 1,
                                    fontWeight: FontWeight.w400,
                                    fontSize: 14/720 * screenHeight,
                                  ),
                                ),
                              ),

                              Container(
                                margin: EdgeInsets.only(
                                    top: (10 / Constant.defaultScreenHeight) *
                                        screenHeight,
                                    right: (20 / 360) * screenWidth),
                                child: SingleChildScrollView(
                                  child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: <Widget>[
                                      Padding(
                                        padding: const EdgeInsets.all(1.0),
                                        child: ReadMoreText(
                                          SubQueriesCommentsScreenStrings
                                              .ReadMoreText_Content,
                                          trimLines: 3,
                                          colorClickableText: Colors.blue,
                                          trimMode: TrimMode.Line,
                                          trimCollapsedText:
                                          SubQueriesCommentsScreenStrings
                                              .Text_ReadMore,
                                          trimExpandedText:
                                          SubQueriesCommentsScreenStrings
                                              .Text_ReadLess,
                                          style: TextStyle(
                                            color: NeutralColors.gunmetal,
                                            fontWeight: FontWeight.w500,
                                            fontFamily: "IBMPlexSans",
                                            fontStyle: FontStyle.normal,
                                            height: 1.5,
                                            fontSize: (12/720)*screenHeight,
                                          ),
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                              ),
                              /**upvotes and reply colum**/
                             Container(
                                margin: EdgeInsets.only(
                                    bottom: 24 / 720 * screenHeight),
                                child:InkWell(
                                  onTap: () {
                                    setState(() {
                                      if (like.contains(index)) {
                                        like.remove(index);
                                      } else {
                                        like.add(index);
                                      }
                                      isSelected = !isSelected;
                                    });
                                  },
                                  child: Row(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    children: <Widget>[
                                      Container(
                                        margin: EdgeInsets.only(
                                            top: like.contains(index)
                                                ? 25 /
                                                    Constant
                                                        .defaultScreenHeight *
                                                    screenHeight
                                                : 26 /
                                                    Constant
                                                        .defaultScreenHeight *
                                                    screenHeight),
                                        child: Container(
                                          height: 11 / 720 * screenHeight,
                                          width: 11 / 360 * screenWidth,
                                          child: like.contains(index)
                                              ? Icon(
                                                  Icons.thumb_up,
                                                  color: NeutralColors
                                                      .deep_sky_blue,
                                                  size: 11 / 360 * screenWidth,
                                                )
                                              : SvgPicture.asset(
                                                  getSvgIcon.like,
                                                  fit: BoxFit.contain,
                                                ),
                                        ),
                                      ),
                                      Container(
                                          margin: EdgeInsets.only(
                                              top: 18 /
                                                  Constant.defaultScreenHeight *
                                                  screenHeight,
                                              left: 5 / 360 * screenWidth),
                                          decoration: BoxDecoration(
                                            color: Colors.transparent,
                                            borderRadius: BorderRadius.all(
                                                Radius.circular(2)),
                                          ),
                                          child: Container(
                                            margin: EdgeInsets.only(
                                                top: 6 /
                                                    Constant
                                                        .defaultScreenHeight *
                                                    screenHeight),
                                            child: Text(
                                              // SubQueriesCommentsScreenStrings.Text_Liked,
                                              like.contains(index)
                                                  ? '${list[index] + 1} Likes'
                                                  : '${list[index]} Likes',
                                              style: TextStyle(
                                                color: like.contains(index)
                                                    ? NeutralColors
                                                        .deep_sky_blue
                                                    : NeutralColors.black,
                                                fontWeight: FontWeight.normal,
                                                fontFamily: "IBMPlexSans",
                                                fontStyle: FontStyle.normal,
                                                fontSize: (12 /
                                                        Constant
                                                            .defaultScreenWidth) *
                                                    screenWidth,
                                              ),
                                              textAlign: TextAlign.left,
                                            ),
                                          )),

                                    Container(
                                      margin: EdgeInsets.only(
                                          top: (26 /
                                                  Constant
                                                      .defaultScreenHeight) *
                                              screenHeight,
                                          left: (20 /
                                                  Constant.defaultScreenWidth) *
                                              screenWidth),
                                      child: Icon(
                                        Icons.chat_bubble_outline,
                                        color: NeutralColors.black,
                                        size: 11 / 360 * screenWidth,
                                      ),
                                    ),
                                    Container(
                                        margin: EdgeInsets.only(
                                            top: 15 /
                                                Constant.defaultScreenHeight *
                                                screenHeight,
                                            left: 5 / 360 * screenWidth),
                                        decoration: BoxDecoration(
                                          color: Colors.transparent,
                                          borderRadius: BorderRadius.all(
                                              Radius.circular(2)),
                                        ),
                                        child: GestureDetector(
                                          onTap: () {
                                            AppRoutes.push(
                                                context, DiscussionComments());
                                          },
                                          child: Container(
                                            margin: EdgeInsets.only(
                                                top: 6 /
                                                    Constant
                                                        .defaultScreenHeight *
                                                    screenHeight),
                                            child: Text(
                                              '232 Comments',
                                              style: TextStyle(
                                                color: NeutralColors
                                                    .dark_navy_blue,
                                                fontWeight: FontWeight.normal,
                                                fontFamily: "IBMPlexSans",
                                                fontStyle: FontStyle.normal,
                                                fontSize: (12 /
                                                        Constant
                                                            .defaultScreenWidth) *
                                                    screenWidth,
                                              ),
                                              textAlign: TextAlign.left,
                                            ),
                                          ),
                                        )),
                                  ],
                                ),
                              ),),

                            ],
                          ),
                        ) ;
                      },
                      separatorBuilder: (context, index) {
                        return Padding(
                          padding: EdgeInsets.only(left: 20/360 * screenWidth,right: 20/360 * screenWidth),
                          child: Divider(),
                        );
                      },
                    ),
                  ),

                ],
              ),
            ),
          ));

  }
}
void CalculateScreen(BuildContext context) {
  var _mediaQueryData = MediaQuery.of(context);
  screenWidthTotal = _mediaQueryData.size.width;
  screenHeightTotal = _mediaQueryData.size.height;
  _safeAreaHorizontal =
      _mediaQueryData.padding.left + _mediaQueryData.padding.right;
  _safeAreaVertical =
      _mediaQueryData.padding.top + _mediaQueryData.padding.bottom;

  screenHeight = screenHeightTotal - _safeAreaVertical;
  screenWidth = screenWidthTotal - _safeAreaHorizontal;
}
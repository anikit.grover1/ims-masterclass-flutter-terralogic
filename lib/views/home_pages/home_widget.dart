import 'dart:async';
import 'dart:convert';
// ignore: avoid_web_libraries_in_flutter
import 'package:carousel_pro/carousel_pro.dart';
import 'package:http/http.dart' as http;
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/svg.dart';
import 'package:imsindia/api/bloc/services.dart';
import 'package:imsindia/blocs/home_screen/now_playing_live_bloc.dart';
import 'package:imsindia/components/bottom_navigation.dart';
import 'package:imsindia/components/error_screen.dart';
import 'package:imsindia/components/navigation_bar.dart';
import 'package:imsindia/models/home_screen/now_playing_live_response_model.dart';
import 'package:imsindia/networking/handlers/api-response.dart';
import 'package:imsindia/routers/routes.dart';
import 'package:imsindia/utils/Url.dart';
import 'package:imsindia/utils/colors.dart';
import 'package:imsindia/utils/error.dart';
import 'package:imsindia/utils/global.dart' as global;
import 'package:imsindia/utils/home_svg_icons.dart';
import 'package:imsindia/utils/loading.dart';
import 'package:imsindia/views/Analytics/analytics_home.dart';
import 'package:imsindia/views/Arun/BlogContent.dart';
//import 'package:imsindia/views/Arun/Samples/SampleApp/GoogleLoginScreen.dart';
import 'package:imsindia/views/Arun/SqliteHelper.dart';
import 'package:imsindia/views/Arun/blogs/Blogs.dart';
import 'package:imsindia/views/Arun/blogs/BlogsFeatured.dart';
import 'package:imsindia/views/Arun/ims_utility.dart';
import 'package:imsindia/views/Arun/myPLAN/myPLAN_ErrorScreen.dart';
import 'package:imsindia/views/Arun/myPLAN/myPLAN_home_Screen.dart';
import 'package:imsindia/views/Arun/notification/Notification.dart';
import 'package:imsindia/views/Arun/search/search_widget.dart';
import 'package:imsindia/views/Discussion/discussion_home.dart';
import 'package:imsindia/views/Emaximiser/emaximizer_home_screen.dart';
import 'package:imsindia/views/GDPI/GDPI_home_screen.dart';
import 'package:imsindia/views/GDPI/Gdpi_HomeScreen.dart';
import 'package:imsindia/views/GK_Zone/gk_home_screen.dart';
import 'package:imsindia/views/channel/channel_home.dart';
import 'package:imsindia/views/event_booking_pages/event_booking_widget.dart';
import 'package:imsindia/views/leaderboard/leaderboard_screen.dart';
import 'package:imsindia/views/practice_pages/practice_home_screen.dart';
import 'package:imsindia/views/prepare_pages/prepare_home_screen.dart';
import 'package:imsindia/views/splash_screen/splash_screen.dart';
import 'package:intl/intl.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:imsindia/views/home_pages/scrollTabComponents/container_container.dart';
import 'package:imsindia/views/home_pages/scrollTabComponents/shape_container.dart';
import 'package:connectivity/connectivity.dart';

import 'package:f_logs/f_logs.dart';


// class CourseData {
//   String title;
//   List<String> subtitle;
//   CourseData({
//     this.title,
//     this.subtitle,
//   });
// }





class HomeWidget extends StatefulWidget {
  @override
  HomeWidgetState createState() => HomeWidgetState();
}

class HomeWidgetState extends State<HomeWidget> with TickerProviderStateMixin{

  SharedPreferences prefs;
  var getLearFirstHiraLevelTitles = [];
  var getLearFirstHiraLevelIds = [];
  var getLearnFirstHiraLevelDesc = [];
  var authorizationInvalidMsg;
  String course;
  String courseName;
  String menuId = "";
  String menuDesc = "";
  List<String> myListOfStrings = [];
  List<String> myListOfIdsget = [];
  List<String> myListOfStringsget = [];
  List<String> myListofDescget = [];
  List<String> myListofDescs = [];

  List<String> myListOfIds = [];
  List<String> courses = [];
  List<String> data = [];


  var usernames;
  var userpins;
  var checkUsernames = '';
  var checkUserpins = '';
  var checkcourses = '';
  var obj;

  var bookingsavailabledata;
  var dataaddstudent;
  var imsCourseId;
  List availableSlots=[];
  List slotsUsed=[];
  List totalSlots=[];
  bool buttonClickOnlyOnce = false;
  bool loadForMyPlan = false;
  NowPlayingLiveBloc _nowPlayingLiveBloc;
  ScrollController _scrollController;


  Connectivity connectivity = Connectivity();
  final GlobalKey<ScaffoldState> _scaffoldstatee = new GlobalKey<ScaffoldState>();
  var refreshKeyyy = GlobalKey<RefreshIndicatorState>();

  //Entire OrderList of Modules in HomeScreen
  List<String> orderList = ["Prepare", "Practice", "E-maximiser", "Test", "myPLAN", "GK-Zone", "GD-PI" ];

  //Order List of LAW and UG course(Gk Zone not required)
  List<String> orderListt = ["Prepare", "Practice", "E-maximiser","Test","GD-PI", "myPLAN"];

  //Functionality: Gets the Course ID and User token from login
  void getCourseId() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    course = prefs.getString("courseId");
    courseName = prefs.getString("courseName");
    global.getToken.then((t){
      getCourseIdData(t);
      getUserCourseId(t);
    });

    //getCourseIdData();
  }



  ///api for add student on myPlan////
  void addStudent() async{
    Map<String, dynamic> potsadd_student;
    potsadd_student = {
      "studentImsPin": userpins//studentImsPin //"99a77771"
    };
    print("userpins........................."+userpins);
    var _headers = {"Content-Type": "application/json"};
    ApiService().postAPITerr(URL.myPLAN_API_ADD_STUDENT, potsadd_student, _headers).then((result) {
       print("resulktttttttttttt");
       print(result);
       print("url");
       print(URL.myPLAN_API_ADD_STUDENT);
      if (result[0]['success']==true){
        setState(() {
          dataaddstudent = result[0]['data'];
          print(result);
          print("-------------------------------print post data of students");
          print(URL.myPLAN_API_ADD_STUDENT);
          print(potsadd_student);
          print(result);
          getBookingsAvailable();
        });
      }else{
     //  ErrorScreen();
        print("error handling");
      }
    });
  }




  //booking available//
  getBookingsAvailable() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    imsCourseId = prefs.getString("courseId");

    // var result = await ApiService().getAPITerr('https://getProjectList');
    Map<String, dynamic> postData;
    postData = {
      "studentImsPin":userpins,
      "myImsCourseId":imsCourseId,
    };
    final result = await ApiService().postAPITerr(URL.myPLAN_API_BOOKINGS_AVAILABLE, postData, global.headers);
    if (result[0]['success']==true){
      setState(() {
        bookingsavailabledata=result[0]['data'];
        buttonClickOnlyOnce = false;
        loadForMyPlan=false;
        if(bookingsavailabledata[0]['totalSlots']==0){
          Navigator.push(context,
              MaterialPageRoute(
                  builder: (context) =>
                      myPlanErrorScreen()));
        }else{
          Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) =>
                    myPLANHome(false,bookingsavailabledata[0]['availableSlots'],bookingsavailabledata[0]['slotsUsed']),
              ));
        }

        print(result);
        print("dataaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa");
      });
    }
    else{
      if(result[0].containsKey("message")){
        buttonClickOnlyOnce = false;
        loadForMyPlan=false;
        if(result[0]['message']=='No Record found'){
          Navigator.push(context,
              MaterialPageRoute(
                  builder: (context) =>
                      myPlanErrorScreen()));
        }else if(result[0]['message']=='available bookings are Empty'){
          Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) =>
                    myPLANHome(false,0,1),
              ));
        }else{
          print("Error Handling");
        }
      }else{
        print("Error Handling");
      }

    }
  }




  // API : Gets Modules to be shown to user
  void getCourseIdData(Map t) async {
    // print('inside api'+course);
    ApiService()
        .getAPI(
        URL.GET_MENU_ITEMS + course, t)
        .then((returnValue) {
      setState(() {
        print(returnValue);
        print('hello');
        if (returnValue[0].toString().toLowerCase() == 'Data Fetch Successfully'.toLowerCase()) {
          getLearFirstHiraLevelTitles = new List();
          getLearFirstHiraLevelIds =  new List();
          getLearnFirstHiraLevelDesc = new List();
          // print(returnValue[0]);
          for (var index = 0;
          index < returnValue[1]['data'].length;
          index++) {
            if(returnValue[1]['data'][index]['Title'] == "Removed Channels Videos"){
              getLearFirstHiraLevelTitles.add("");
              getLearFirstHiraLevelTitles.remove("");
              getLearnFirstHiraLevelDesc.add("");
              getLearnFirstHiraLevelDesc.remove("");
              getLearFirstHiraLevelIds.add("");
              getLearFirstHiraLevelIds.remove("");
            }else{
              getLearFirstHiraLevelTitles.add(returnValue[1]['data'][index]['Title']);
              getLearFirstHiraLevelIds.add(returnValue[1]['data'][index]['id']);
              getLearnFirstHiraLevelDesc.add(returnValue[1]['data'][index]['description'] == null ? "" : returnValue[1]['data'][index]['description']);
              //print(returnValue[1]['data'][index]['Title']);
              // print(" testtttttt" + global.courseId);
              // saveMenuIDs();
            }
          }
          //Added myPLAN module statically for the following courses
          if(courseName == "GRE" || courseName == "GMAT" || courseName == "CET 2020" || courseName == "CET 2021" || courseName == "CMAT 2020" || courseName == "CMAT 2021" || courseName == "GDPI")
          {
              getLearFirstHiraLevelTitles.add("myPLAN");
              getLearnFirstHiraLevelDesc.add("");
              getLearFirstHiraLevelIds.add("");
          }
          saveMenus();
          getMenus();
        } else {
          print("return value");
          print(returnValue[0]);
          authorizationInvalidMsg = returnValue[0];
        }
      });
    });

  }

  //API: Gets the courses for the user
  void getUserCourseId(Map t){
    ApiService().getAPI(URL.GET_USER_COURSE_NAMES, t).then((result) {
      for (int i = 0; i < result[1]['data'].length; i++) {
        if(result[1]['data'][i]['courseName'] == "GMAT_Old" || result[1]['data'][i]['courseName'] == "CAT Demo" || result[1]['data'][i]['courseName'] == "BBA" || result[1]['data'][i]['courseName'] == "CLAT"|| result[1]['data'][i]['courseName'] == "CLAT Prepzone" || result[1]['data'][i]['courseName'] == "GQuest"){
          courses.add("");
          courses.remove("");
          data.add("");
          data.remove("");
        }else{
          courses.add(result[1]['data'][i]['courseName']);
          data.add(result[1]['data'][i]['id']);
        }
      }
      // print("not sorted name" + courses.toString());
      // print("not sorted ids" + data.toString());
      //  print(result[1]['data'][i]['courseName']);}
      setUserCourseId();
    });

  }

  void setUserCourseId() async {
    var coursesToSort = courses.map((i) => i.toString()).toSet(); //Removes the repeated data
    courses = coursesToSort.toList();
    //print("sorted name" + courses.length.toString());
    var dataToSort = data.map((i) => i.toString()).toSet(); //Removes the repeated data
    data = dataToSort.toList();
    //print("sorted ids" + data.length.toString());
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setStringList('usercourseName', courses);
    prefs.setStringList('usercourseId', data);
  }



  // void saveMenuIDs() async {
  //   myListOfIds =
  //       getLearFirstHiraLevelIds.map((i) => i.toString()).toList();
  //       print("unsorted ids"+myListOfStrings.toString());

  //       prefs = await SharedPreferences.getInstance();
  //   await prefs.setStringList('myidlist', myListOfIds);
  // }

  //  void getMenu() async {
  //
  //    SharedPreferences prefs = await SharedPreferences.getInstance();
  //    myListOfStringsget = prefs.getStringList('mylist');
  //    myListOfIdsget = prefs.getStringList('myidlist');
  //
  //    prefs = await SharedPreferences.getInstance();
  //
  //  }




  // Functionality : Condition to show database data or call API for Blogs Data
  void getCourseHome() async
  {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    // Get course , username and userpin from Login of the Current User
    course = prefs.getString("courseId");
    usernames = prefs.getString(ImsStrings.sCurrentUserName);
    userpins = prefs.getString(ImsStrings.sCurrentUserId);

    // Get course , username and userpin of the Previous User
    checkUsernames = prefs.getString("PrevioustUserName");
    checkUserpins = prefs.getString("PreviousUserId");
    checkcourses = prefs.getString("PreviouscourseId");

    //Checks if previous user and current user has same username and userpin / If true , shows the database blog data
    if(usernames != checkUsernames && userpins != checkUserpins){
      setUserID();
      saveMenus();
      print("if part Home");
    }

    //Checks if user has  selected same course / If true , shows the database blog data
    else if(course != checkcourses){
      setUserID();
      saveMenus();
      print("else if part Home");
    }

    //If both conditions , blog API is called
    else{
      getMenus();
      print("else part Home from db");
    }

    //print('course::::::'+prefs.getString("courseId"));
  }

  //Sets the course , username and userpin from login of the Current User
  void setUserID() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString("PreviousUserName", usernames);
    prefs.setString("PreviousUserId", userpins);
    prefs.setString("PreviouscourseId", course);
  }

  //Sets the modules for the current user
  void saveMenus() async {
    var listMenu = new List();
    listMenu = getLearFirstHiraLevelTitles.map((i) => i.toString()).toList();

    var listId = new List();
    listId = getLearFirstHiraLevelIds.map((i) => i.toString()).toList();

    var listDesc = new List();
    listDesc = getLearnFirstHiraLevelDesc.map((i) => i.toString()).toList();

    // print("unsorted names"+listMenu.length.toString());
    //  print("unsorted ids"+listId.length.toString());
    myListOfStringsget = new List();
    myListOfIdsget = new List();
    myListofDescget = new List();

    if(courseName == "LAW" || courseName == "UG"){
      for(var i = 0; i< orderListt.length;i++){

      for(var j =0 ; j < listMenu.length; j++){
        if(orderListt[i] == listMenu[j]){
          myListOfStringsget.add(listMenu[j].toString());
          myListOfIdsget.add(listId[j].toString());
          myListofDescget.add(listDesc[j].toString());
        }
      }
    }
    } else {
      for(var i = 0; i< orderList.length;i++){

        for(var j =0 ; j < listMenu.length; j++){
          if(orderList[i] == listMenu[j]){
            myListOfStringsget.add(listMenu[j].toString());
            myListOfIdsget.add(listId[j].toString());
            myListofDescget.add(listDesc[j].toString());
          }
        }
      }
    }
    //  print("=================>"+myListOfStrings.length.toString());
    //  print("=================>"+myListOfIds.length.toString());

    prefs = await SharedPreferences.getInstance();
    prefs.setStringList('mylist',  myListOfStringsget);
    prefs.setStringList('myidlist',myListOfIdsget);
    prefs.setStringList('mydesclist', myListofDescget);
  }

  //Gets the modules for the current user to show in UI
  void getMenus() async {

    prefs = await SharedPreferences.getInstance();
    myListOfStrings = prefs.getStringList('mylist');
    myListOfIds = prefs.getStringList('myidlist');
    myListofDescs = prefs.getStringList('mydesclist');

    this. myListOfStrings =  myListOfStrings;
    this. myListOfIds =   myListOfIds;
    this. myListofDescs = myListofDescs;

    print("====dfsd>");
    print("====dfsd>" + myListOfStrings.toString());
    print("===sdfsdf=>" +myListOfIds.toString());
    print("===sdfsdf=>"+ myListofDescs.toString());
  }


  // Sets the Module Id for chosen module in Home Screen
  void setIds(String id) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('moduleId', id);
  }

  // Sets the Module Desc for chosen module in Home Screen
  void setDesc(String desc) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('moduleDesc', desc);
  }


  // BlogTab API call which uses WordPress API

  TabController _tabController;

  var listoftabs = [];
  var listoftabids = [];
  var listofalltabs = [];
  var listofalltabids = [];
  var selectedcoursesforblog  ;
  var datacourse = [];
  var datatab = [];
  var dataposts = [];
  var listoftitles = [];
  var listofalltitles = [];
  var listofposts = [];
  var listoflinks = [];
  var listofalllinks = [];
  var listofDataLinks = [];
  List<Tab> tabs = List();
  //var course;
  var username;
  var userpin;
  var checkUsername = '';
  var checkUserpin = '';
  var checkcourse = '';
  //var obj;
  List<Widget> fetchData = [] ;

  //API (Course Category): To get ID for corresponding course
  void getBlogs() async {
    FLog.info(
      dataLogType: "Debug",
      text: "getblogs - 1",
    );
    print("getblogs - 1");
    var getcoursesforblog = await http.get(Uri.parse(URL.BLOGS_API_COURSES_LIST + "100"));

    datacourse = jsonDecode(getcoursesforblog.body);

    print(datacourse.length);

    setState(() {
      if(datacourse != null){
        print("entered blog courses");
        print("course" + course.toString());


        for (var index = 0; index < datacourse.length;index ++){
          print("course from API"+datacourse.toList()[index]["acf"]["course_id"]);
          if(datacourse.toList()[index]["acf"]["course_id"] == course){
            selectedcoursesforblog = datacourse.toList()[index]["id"];
          }
        }
      }
    });
    getTabs();
    print(selectedcoursesforblog);
    print("selectedcoursesforblog");
  }

  var tabvalue ;
  var tabList = [];
  var articleTitle ;
  var articleId ;

  //API (Blog Category): To get a list of categories for the corresponding course
  void getTabs() async {
    FLog.info(
      dataLogType: "Debug",
      text: "getTabs - 2",
    );
    print("getTabs - 2");
    var gettabsforblog = await http.get(Uri.parse(URL.BLOGS_API_TAB_LIST + "100"));
    datatab = jsonDecode(gettabsforblog.body);
    print(datatab.length);
    setState(()  {
      if(datatab != null){
        print("entered tabs");
        tabs = new List();
        listoftabids = new List();
        listoftabs =  new List();
        for (var index = 0; index < datatab.length;index ++){

          // print(datatab.toList()[index]["acf"]["course"].toString());
          if(datatab.toList()[index]["name"]== "Uncategorized"){
            print("not required");
            tabList.add("");
          }
          else{
            tabList.add(datatab[index]);
            print("required");
            tabvalue = tabList[index]["acf"]["course"];
            print(tabvalue);
            if(tabList[index]["acf"]["course"] == false){
              print("false string");
            }
            else{
              //tabs = new List();

              for(var i = 0; i < tabList[index]["acf"]["course"].length ; i++){

                if(tabList[index]["acf"]["course"][i].toString() == selectedcoursesforblog.toString()){
                  listofalltabids.add(datatab.toList()[index]["id"].toString());
                  listofalltabs.add(datatab.toList()[index]["name"]);
                  if(datatab.toList()[index]["id"].toString() == "95")
                  {
                    print("Tab not required for article of the week");
                    articleTitle = datatab.toList()[index]["name"];
                    articleId = datatab.toList()[index]["id"].toString();
                  }
                  else{
                    listoftabids.add(datatab.toList()[index]["id"].toString());
                    listoftabs.add(datatab.toList()[index]["name"]);
                    tabs.add( new Tab(
                      child: Text(
                        datatab.toList()[index]["name"],
                      ),
                    ));
                  }
                }
              }
            }
          }
        }
      }
    });
    print(tabs);
    print(tabs.length);
    print(tabList);
    print(tabList.length);
    getPosts();
    print(listoftabids);
    print(listoftabs);

  }

  var articleDataTitle = "";
  var articleDataLink = "";
  var listofimageapi = [];
  var totalimageapi = [];
  var listoftimes = [];
  var articleDataImageAPI = "";
  var articleDataTime;
  var listoftotaltimes = [];

  var articleDataTitleList = [];
  var articleDataLinkList = [];
  var articleDataTimeList = [];
  var articleDataImageAPIList = [];

  // API (Blog Posts) : To get all the posts for the corresponding categories of the course
  void getPosts() async {
    FLog.info(
      dataLogType: "Debug",
      text: "getposts - 3",
    );
    print("getposts - 3");
    var getpostsforblog = await http.get(Uri.parse(URL.BLOGS_API_POST_LIST + "100"));
    dataposts = jsonDecode(getpostsforblog.body);
    print(dataposts.length);
    setState(()  {
      if(dataposts != null){
        print("entered posts");
        listofposts = new List();
        listofDataLinks = new List();
        totalimageapi = new List();
        listoftotaltimes = new List();

        for (var j = 0; j < listoftabids.length;j++){
          listoftitles = new List();
          listoflinks = new List();
          listofimageapi = new List();
          listoftimes = new List();
          for (var index = 0; index < dataposts.length;index ++){

            for (var i = 0 ; i < dataposts.toList()[index]["categories"].length ; i++){
              // print(listoftabids.contains(dataposts.toList()[index]["categories"][i].toString()));
              //  print("Category Id = "+dataposts.toList()[index]["categories"][i].toString() + " Cat Index i " + i.toString());
              //  print("Sub Cat ID = "+listoftabids[j].toString() + " J Values = "+ j.toString());


              if(listoftabids[j].toString() == dataposts.toList()[index]["categories"][i].toString()){

                // print("ListOfTitles = "+listoftabids[j].toString());
                // print("title rendered = "+dataposts.toList()[index]["title"]["rendered"].toString());
                for(var k = 0 ; k < dataposts.toList()[index]["ims_courses"].length ; k ++){

                  if(selectedcoursesforblog.toString() == dataposts.toList()[index]["ims_courses"][k].toString()){
                    listoftitles.add(dataposts.toList()[index]["title"]["rendered"].toString());
                    listoflinks.add(dataposts.toList()[index]["acf"]["external_link"].toString());
                    if(dataposts.toList()[index]["_links"]["wp:featuredmedia"] != null)
                    {
                      listofimageapi.add(dataposts.toList()[index]["_links"]["wp:featuredmedia"][0]["href"].toString());
                    }
                    else{
                      listofimageapi.add("");
                    }
                    listoftimes.add(dataposts.toList()[index]["date"]);
                  }
                }
              }

              if(articleId.toString() == dataposts.toList()[index]["categories"][i].toString()){

                for(var k = 0 ; k < dataposts.toList()[index]["ims_courses"].length ; k ++){

                  if(selectedcoursesforblog.toString() == dataposts.toList()[index]["ims_courses"][k].toString()){
                    articleDataTitleList.add(dataposts.toList()[index]["title"]["rendered"].toString());
                    articleDataLinkList.add(dataposts.toList()[index]["acf"]["external_link"].toString());
                    articleDataTimeList.add(dataposts.toList()[index]["date"].toString());
                    if(dataposts.toList()[index]["_links"]["wp:featuredmedia"] != null)
                    {
                      articleDataImageAPIList.add(dataposts.toList()[index]["_links"]["wp:featuredmedia"][0]["href"].toString());
                    }else{
                      articleDataImageAPIList.add("");
                    }
                  }
                  else{
                    articleDataImageAPI = "";
                  }
                }
              }
              print(listoftitles);
              print(listofimageapi);
              //break;
            }
          }
          listofposts.add(listoftitles);
          listofDataLinks.add(listoflinks);
          totalimageapi.add(listofimageapi);
          listoftotaltimes.add(listoftimes);
        }

        // for (var j = 0; j < listofalltabids.length;j++){
        //   for (var index = 0; index < dataposts.length;index ++){
        //       for(var i = 0; i < dataposts.toList()[index]["ims_courses"].length;i ++){
        //          if (dataposts.toList()[index]["ims_courses"][i].toString() == selectedcoursesforblog.toString()){
        //              for (var k = 0 ; k < dataposts.toList()[index]["categories"].length ; k++){

        //                 if(listofalltabids[j].toString() == dataposts.toList()[index]["categories"][k].toString()){
        //                     listofalltitles.add(dataposts.toList()[index]["title"]["rendered"].toString());
        //                     listofalllinks.add(dataposts.toList()[index]["acf"]["external_link"].toString());
        //                   }
        //                  }
        //            }

        //          }
        //     }
        // }
      }
    });
    if(articleDataTitleList.length != 0 && articleDataTitleList.length != 0 && articleDataTimeList.length != 0 && articleDataImageAPIList.length != 0 )
    {
      articleDataTitle = articleDataTitleList[0];
      articleDataLink = articleDataLinkList[0];
      articleDataTime = articleDataTimeList[0];
      articleDataImageAPI = articleDataImageAPIList[0];
    }

    print(articleDataTitle.toString());
    print(listoftitles.length);
    print(listofposts.length);
    print(listoftitles);
    print(listofposts);
    print(listofalltitles);
    print(totalimageapi);
    print(listofalltitles.length);
    print(totalimageapi.length);
    //getData();
    //saveDataDB();
    getImageList();
    // GetLocalSearchContentBlog();
  }

  var imageList = [];
  var totalImageList = [];
  var articleDataImage;
  var articleTime;

  //To get the list of image of each post
  getImageList() async{
    print("getImageList - 4");
    //for(var k = 0 ; k < totalimageapi.length; k++){
    if(articleDataImageAPI != ""){
      articleDataImage = await imageApicall(articleDataImageAPI);
    }
    for(var i = 0 ; i < totalimageapi.length; i++){
      print(totalimageapi);
      imageList = new List();
      for(var j = 0; j < totalimageapi[i].length;j++){
        for(var k = 0 ; k < totalimageapi[i].length; k++){
          if(totalimageapi[i][j] != ""){
            imageList.add(await imageApicall(totalimageapi[i][j]));
          }
          else{
            imageList.add("");
          }
          break;
        }
      }
      totalImageList.add(imageList);
    }
    print("totalList"+totalImageList.toString());
    print("articleDataImage"+articleDataImage.toString());
    //saveDataDB();
    getDateList();
  }

  //API : To get the image thorugh link
  Future imageApicall(String api) async
  {
    var response = await http.get(Uri.parse(api));
    var decodeRes = jsonDecode(response.body);
    //print("decodedededed"+decodeRes["guid"]["rendered"].toString());
    if(decodeRes["guid"] != null){
      return decodeRes["guid"]["rendered"].toString();
    }
    else{
      return "";
    }
  }

  var timeList = [];
  var totalTimeList = [];

  //To get the list of date of each post
  getDateList() async{
    print("getDateList - 5");
    if(articleDataTime != null){
      articleTime = await dateConvert(articleDataTime);
    }
    for(var i = 0 ; i < listoftotaltimes.length; i++){
      print(listoftotaltimes);
      timeList = new List();
      for(var j = 0; j < listoftotaltimes[i].length;j++){
        for(var k = 0 ; k < listoftotaltimes[i].length; k++){
          timeList.add(await dateConvert(listoftotaltimes[i][j]));
          break;
        }
      }
      totalTimeList.add(timeList);
    }
    print("finished loading");
    saveDataDB();
  }

  //Function to convert the date from to required format(dd-mon-yyyy)
  Future dateConvert(String dateString) async
  {
    var monthList = ["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"];
    var mon;
    var day = DateTime.parse(dateString).day;
    var month = DateTime.parse(dateString).month;
    var year = DateTime.parse(dateString).year;
    // print("time convert day "+ day.toString());
    // print("time convert month "+ month.toString());
    // print("time convert year "+ year.toString());
    for(var i=0; i < monthList.length ;i++){
      if(i + 1 == month){
        setState(() {
          mon = monthList[i];
          print(mon);
        });

      }
    }
    if(day <= 9){
      //print("0$day-$mon-$year");
      return "0$day-$mon-$year";
    }
    else{
      //print("$day-$mon-$year");
      return "$day-$mon-$year";
    }
  }

  //Function to save blog data in database
  void saveDataDB() async {
    print("saveDataDB - 6");
    await obj.deleteAllBlogs();
    print("should be 3 "+ listofposts.length.toString());
    print("should be 4 " + listofalltabs.length.toString());
    //If posts are available , there are inserted into the database
    if(listofposts.length != 0){
      for (var j = 0; j < listofposts.length;j++){
        for (var k = 0; k < listofposts[j].length ; k++){
          InserDataBlog(listofposts[j][k],listofDataLinks[j][k],listoftabids[j], listoftabs[j],totalImageList[j][k],totalTimeList[j][k]);
        }
      }
      if(articleDataTitle != null && articleDataLink != null){
        InserDataBlog(articleDataTitle, articleDataLink, articleId, articleTitle,articleDataImage,articleTime);
      }
      GetLocalSearchContentBlog(); //Blog data is fetched from database to show in UI
    }
    //else data is fetched from the database to show in UI
    else{
      getData();
    }
  }

  //data is fetched from the database to show in UI
  void getData() async {
    fetchData = new List();
    if(databaseTablist.length != 0){
      for (var i = 0;i < databaseTablist.length;i++){
        fetchData.add(
          //Container(child: Text(i.toString()),));
          //Container(child: Text(i.toString()),);
            PostWidget(i,databasePostlist[i],databaseDataLinks[i],databaseImagelist[i],databaseTimelist[i]));
      }
    }
    else{
      databaseTablist = [];
      setState(() {
        _tabController =  new TabController(vsync: this, length: databaseTablist.length);
      });

      fetchData = [];
    }
    finishedLoad();
  }

  //Flag : Set once the data is loaded
  void finishedLoad() async {
    bool flag = true;
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setBool("flagCheck",flag);
    prefs.setBool("flagLoad", flag);
    print(flag);
  }

  //Flag : Set once the data is starting to load
  void loading() async {
    bool flag = false;
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setBool("flagLoad",flag);
    print(flag);
  }

  //Function to insert data in the database
  void InserDataBlog(String strContent, String strContent1, String strContent2,
      String strContent3,String strContent4,String strContent5) async {
    var bb = new BlogContent(
      content: strContent,
      content1: strContent1,
      content2: strContent2,
      content3: strContent3,
      content4: strContent4,
      content5: strContent5,
    );
    bb.toString();
    await obj.insertBlog(bb);
  }

  var databaseTabNameList = [];
  var databasePostlist = [];
  var databaseImagelist = [];
  var databaseTimelist = [];
  List<Tab> databaseTablist = List();
  var databaseDataLinks = [];
  var listoflinksss= [];
  var listoftitlesss = [];
  var listofimages = [];
  var listoftimess = [];
  var uniqueTabs ;
  var ttabs;
  var databasearticleDataTitle;
  var databasearticleDataLink;
  var databasearticleDataImage;
  var databasearticleDataTime;

  //Function to fetch Blog data from database to show in UI
  void GetLocalSearchContentBlog() async {
    itemsBlogs = await obj.getDataBlog();
    for (var i = 0; i < itemsBlogs.length; i++) {
      print(itemsBlogs.length);
      print('GetBlogSearchContent :: ${itemsBlogs[i]}');
      ttabs = itemsBlogs.map((o) => o.content3).toSet(); //This provides the unique value of input list
      print(ttabs.toList().toString());
      if(ttabs.toList().contains("Article of the week")){
        ttabs.remove("Article of the week");
        uniqueTabs = ttabs.toList();
      }
    }
    if(uniqueTabs != null){
      databaseTablist = new List();
      for (var i = 0; i < uniqueTabs.length; i++) {
        databaseTablist.add(
          new Tab(
            child: Text(uniqueTabs[i]),
          ),
        );
      }
    }
    setState(() {
      _tabController =  new TabController(vsync: this, length: databaseTablist.length);
    });
    if(ttabs != null){
      databasePostlist = new List();
      databaseDataLinks = new List();
      databaseImagelist = new List();
      databaseTimelist = new List();
      for (var j = 0; j < ttabs.length;j++){
        listoftitlesss = new List();
        listoflinksss = new List();
        listofimages = new List();
        listoftimess = new List();
        for (var index = 0; index < itemsBlogs.length;index ++){

          for (var i = 0 ; i < itemsBlogs[index].content3.length ; i++){

            if(ttabs.toList()[j].toString() == itemsBlogs[index].content3.toString()){


              listoftitlesss.add(itemsBlogs[index].content.toString());
              listoflinksss.add(itemsBlogs[index].content1.toString());
              listofimages.add(itemsBlogs[index].content4.toString());
              listoftimess.add(itemsBlogs[index].content5.toString());
            }
            if("Article of the week" == itemsBlogs[index].content3.toString()){
              databasearticleDataTitle =  itemsBlogs[index].content.toString();
              databasearticleDataLink = itemsBlogs[index].content1.toString();
              databasearticleDataImage = itemsBlogs[index].content4.toString();
              databasearticleDataTime = itemsBlogs[index].content5.toString();
            }
            print(listoftitlesss);
            break;
          }
        }
        databasePostlist.add(listoftitlesss);
        databaseDataLinks.add(listoflinksss);
        databaseImagelist.add(listofimages);
        databaseTimelist.add(listoftimess);
      }
    }
    print(databasePostlist);
    getData();
  }



  //print('course::::::'+prefs.getString("courseId"));



  void getCourseIdd() async
  {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    course = prefs.getString("courseId");
    username = prefs.getString(ImsStrings.sCurrentUserName);
    userpin = prefs.getString(ImsStrings.sCurrentUserId);
    checkUsername = prefs.getString("PrevioustUserName");
    checkUserpin = prefs.getString("PreviousUserId");
    checkcourse = prefs.getString("PreviouscourseId");
//print('username::::::'+prefs.getString(ImsStrings.sCurrentUserName));
//print('username::::::'+username );
//print(course + " equals " +checkcourse);
//print('checkUsername::::::'+checkUsername);
//print('checkUsername::::::'+prefs.getString("PreviousUserName"));
    bool value = prefs.getBool("flagLoad");
    if(username != checkUsername && userpin != checkUserpin){
      setUserIDforCheck();
      loading();
      getBlogs();

      print("if part");
    }
    else if(course != checkcourse){
      setUserIDforCheck();
      loading();
      getBlogs();

      print("else if part");
    }
    else{
      print(value);
      if(value == true){
        GetLocalSearchContentBlog();
        print("else part true ");
      }
      else{
        setUserIDforCheck();
        loading();
        getBlogs();

        print("else part false");
      }
      print("else part");
    }

//print('course::::::'+prefs.getString("courseId"));
  }

  void setUserIDforCheck() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString("PreviousUserName", username);
    prefs.setString("PreviousUserId", userpin);
    prefs.setString("PreviouscourseId", course);
  }

  Future<Null> checkInternet() async {
    var connectivityResult = await (Connectivity().checkConnectivity());
    if (connectivityResult.toString() != 'ConnectivityResult.none') {
      setState(() {
        print('Clicked on intenet');

        //Again calling api while refreshing

      });
    } else {
      noInternetMessage('Internet Connection Lost');
    }

    return null;
  }
  void noInternetMessage(String value) {
    _scaffoldstatee.currentState.showSnackBar(new SnackBar(
      content: new Text(value),
      action: SnackBarAction(
        textColor: Colors.blueAccent,
        label: "Dismiss",
        onPressed: () {
          // Some code to undo the change.
        },
      ),
      backgroundColor: Colors.blueGrey,
      duration: Duration(days: 365),
    ));
  }
  void showRefreshMessage() {
    _scaffoldstatee.currentState.showSnackBar(new SnackBar(
      content: new Text("Click here to refresh"),
      action: SnackBarAction(
        textColor: Colors.blueAccent,
        label: "Refresh",
        onPressed: () {

          getCourseId();
          getBlogs();
        },
      ),
      duration: new Duration(seconds: 05),
    ));
  }






  var initstate;


  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    // print(global.courseId);
    // print('hiiiii INIT HOME');
    //print(global.headersWithAuthorizationKey);
    _nowPlayingLiveBloc = NowPlayingLiveBloc();
    _scrollController = ScrollController(initialScrollOffset: 0);

    obj = SqliteHelper('imsApp.db');
    obj.create();
    setState(() {
      //saveMenus();
      getCourseId();
      //getMenus();
      getCourseHome();
      getCourseIdd();
      initstate = true;//Flag : Set to not show the toast message when coming to Home Screen for the first time
      print('******************myinitState');
    });


    //checkInternet();
    print('init state called');
    connectivity.onConnectivityChanged.listen((result) {
      if (result.toString() == 'ConnectivityResult.none') {
        print('connection lost');
        noInternetMessage('Internet Connection Lost');
      } else {
        print(_scaffoldstatee.currentState) ;
        print("_scaffoldstate.currentState");
        _scaffoldstatee.currentState.removeCurrentSnackBar();
        print('connection exist in notifier');
        if(initstate == true){
          initstate = false;
        }else{
          showRefreshMessage();
        }


      }
    });
    // this.course = global.courseId;

  }

  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {

    print("Build method called...");

    final screenHeight = MediaQuery.of(context).size.height;
    final screenWidth = MediaQuery.of(context).size.width;

    // final course1 = CourseData(
    //   title: "Introduction to Summary",
    //   subtitle: [
    //     "Pie Chart",
    //     "Prepare",
    //   ],
    // );

    // final course2 = CourseData(
    //   title: "Critical Reasoning",
    //   subtitle: [
    //     "Reading Comprehension",
    //     "Practice",
    //   ],
    // );

    // final course3 = CourseData(
    //   title: "Introduction to Summary",
    //   subtitle: [
    //     "Algebra",
    //     "eMaximiser",
    //   ],
    // );

    // final List<CourseData> courses = [
    //   course1,
    //   course2,
    //   course3,
    // ];

    // on dragging page refresh function will be activated
    Future<Null> refreshList() async {
      FLog.info(
        dataLogType: "Debug",
        text: "Clicked on refresh",
      );
      refreshKeyyy.currentState?.show(atTop: true);
      print('Clicked on refresh');
      // GetLocalSearchContentBlog();
      obj = SqliteHelper('imsApp.db');
      obj.create();
      getCourseId();
      getBlogs();
      await Future.delayed(Duration(seconds: 2));
      return null;
    }

    //Functionality: Exits the application
    Future<bool> _onWillPop() {
      global.getStatus.then((flag){
        if(flag == true){
          SystemChannels.platform.invokeMethod('SystemNavigator.pop');
        }
      });
    }


    //UI Widget: Contains all the widget in HomeScreen
    body(){

      return RefreshIndicator(
        displacement: 50,
        color: SemanticColors.blueGrey,
        backgroundColor: SemanticColors.iceBlue,
        key: refreshKeyyy,
        onRefresh: refreshList,
        child:Container(
          color: Colors.white,
          child: new LayoutBuilder(
            builder: (BuildContext context, BoxConstraints viewportConstraints) {
              return SingleChildScrollView(
                child: ConstrainedBox(
                  constraints:
                  BoxConstraints(minHeight: viewportConstraints.maxHeight),
                  child: Column(
                      children: [

                        SizedBox(
                            height: 200.0,
                            width: double.infinity,
                            child: Carousel(
                              images: [
                                NetworkImage('https://static.vecteezy.com/system/resources/previews/000/677/372/non_2x/pink-wave-fluid-background-with-geometric-shapes.jpg'),
                                NetworkImage('https://static.vecteezy.com/system/resources/previews/001/058/378/non_2x/purple-gradient-overlapping-shape-horizontal-banner-vector.jpg'),
                                NetworkImage('https://static.vecteezy.com/system/resources/previews/001/349/487/non_2x/purple-low-poly-abstract-banner-free-vector.jpg'),
//                                ExactAssetImage("assets/images/LaunchImage.jpg")
                              ],
                              dotSize: 4.0,
                              dotSpacing: 15.0,
                              dotColor: Colors.lightGreenAccent,
                              indicatorBgPadding: 5.0,
                              borderRadius: false,
                              dotBgColor: Colors.transparent,
                              dotPosition: DotPosition.bottomLeft,
                              dotVerticalPadding: 15.0,
                              dotHorizontalPadding: 15.0,
                              noRadiusForIndicator: true,
                            )
                        ),


//                    Container(
//                      height: 56.0 / 720 * screenHeight,
//                      margin: EdgeInsets.only(
//                          left: 20 * 360 / screenWidth,
//                          top: 25 * 720 / screenHeight,
//                          right: 20 * 360 / screenWidth),
//                      decoration: BoxDecoration(
//                        gradient: LinearGradient(
//                          // begin: Alignment(-0.015, 0.489),
//                          // end: Alignment(1.035, 0.521),
//                          stops: [
//                            0,
//                            1,
//                          ],
//                          colors: [
//                            Color.fromARGB(255, 51, 128, 204),
//                            Color.fromARGB(255, 137, 110, 216),
//                          ],
//                        ),
//                        borderRadius: BorderRadius.all(Radius.circular(5)),
//                      ),
//                      child: PageIndicatorContainer(
//                        align: IndicatorAlign.bottomRight,
//                        length: 3,
//                        indicatorSpace: 2.0 / 360 * screenWidth,
//                        indicatorColor: Colors.white.withOpacity(0.46),
//                        indicatorSelectorColor: NeutralColors.sun_yellow,
//                        //padding: EdgeInsets.only(bottom: 10.0),
//                        shape: IndicatorShape.roundRectangleShape(
//                            size: Size(8.0 / 360 * screenWidth,
//                                2.0 / 720 * screenHeight),
//                            cornerSize: Size.fromRadius(1)),
//                        child: PageView(
//                          pageSnapping: true,
//                          children: <Widget>[
//                            ScrollTabWidget(),
//                          ],
//                        ),
//                      ),
//                    ),
//










//                    Container(
//                      height: screenHeight * (231 / 720),
//                      margin: EdgeInsets.only(
//                          left: 20 * 360 / screenWidth,
//                          top: 40 * 720 / screenHeight,
//                          right: 20 * 360 / screenWidth),
//                      child: Column(
//                        crossAxisAlignment: CrossAxisAlignment.stretch,
//                        children: [
//                          Container(
//                            child: Text(
//                              "Continue Courses",
//                              style: TextStyle(
//                                color: Color.fromARGB(255, 0, 3, 44),
//                                fontSize: 18 / 360 * screenWidth,
//                                fontFamily: "IBMPlexSans",
//                                fontWeight: FontWeight.w700,
//                              ),
//                              textAlign: TextAlign.left,
//                            ),
//                          ),
//                          Container(
//                            child: ListView.builder(
//                              shrinkWrap: true,
//                              physics: NeverScrollableScrollPhysics(),
//                              itemCount: courses.length,
//                              itemBuilder: (context, index) {
//                                final item = courses[index];
//                                Color color;
//                                if (index % 2 == 0) {
//                                  color = SemanticColors.dark_purpely;
//                                } else if (index % 2 == 1) {
//                                  color = const Color(0xFFff5757);
//                                }
//                                return GestureDetector(
//                                  onTap: (){
//                                    if ( courses[index] == course1)
//                                    {
//                                      Navigator.push(context, MaterialPageRoute(builder: (context) => PrepareHomeScreenHtml(),));
//                                    }
//
//                                    if ( courses[index] == course2)
//                                    {
//                                      Navigator.push(context, MaterialPageRoute(builder: (context) => PracticeHomeScreen(),));
//                                    }
//
//                                    if ( courses[index] == course3)
//                                    {
//                                      Navigator.push(context, MaterialPageRoute(builder: (context) => EmaximiserHomeScreen(),));
//                                    }
//                                  },
//                                  child: Container(
//                                    height: 40 / 720 * screenHeight,
//                                    margin: EdgeInsets.only(
//                                        top: 20 / 720 * screenHeight),
//                                    child: Row(
//                                      children: [
//                                        Container(
//                                          width: 3 / 360 * screenWidth,
//                                          height: 40 / 720 * screenHeight,
//                                          decoration: BoxDecoration(
//                                            color: color,
//                                            borderRadius: BorderRadius.all(
//                                                Radius.circular(1.5)),
//                                          ),
//                                          child: Container(),
//                                        ),
//                                        Container(
//                                          color: Colors.transparent,
//                                          width: screenWidth * .7,
//                                          height: 40 / 720 * screenHeight,
//                                          margin: EdgeInsets.only(
//                                              left: 10 / 360 * screenWidth),
//                                          child: Column(
//                                            crossAxisAlignment:
//                                                CrossAxisAlignment.stretch,
//                                            children: [
//                                              Text(
//                                                item.title,
//                                                style: TextStyle(
//                                                  color:
//                                                      Color.fromARGB(255, 0, 3, 44),
//                                                  fontSize: 14 / 720 * screenHeight,
//                                                  fontFamily: "IBMPlexSans",
//                                                ),
//                                                textAlign: TextAlign.left,
//                                              ),
//                                              Spacer(),
//                                              Container(
//                                                color: Colors.transparent,
//                                                width: screenWidth * .7,
//                                                child: Row(
//                                                  crossAxisAlignment:
//                                                      CrossAxisAlignment.center,
//                                                    mainAxisAlignment: MainAxisAlignment.start,
//                                                  children: [
//                                                    Text(
//                                                      item.subtitle[0],
//                                                      style: TextStyle(
//                                                        color: Color.fromARGB(
//                                                            255, 153, 154, 171),
//                                                        fontSize: 12 /
//                                                            720 *
//                                                            screenHeight,
//                                                        fontFamily:
//                                                            "IBMPlexSans",
//                                                        fontWeight:
//                                                            FontWeight.w500,
//                                                      ),
//                                                      textAlign: TextAlign.left,
//                                                    ),
//                                                    Align(
//                                                      alignment:
//                                                          Alignment.centerLeft,
//                                                      child: Container(
//                                                        width: 3 / 360 * screenWidth,
//                                                        height: 3 / 720 * screenHeight,
//                                                        margin: EdgeInsets.only(left: 8),
//                                                        decoration: BoxDecoration(
//                                                          color: Color.fromARGB(255, 216, 216, 216),
//                                                          borderRadius:
//                                                              BorderRadius.all(
//                                                                  Radius.circular(
//                                                                      1.5)),
//                                                        ),
//                                                        child: Container(),
//                                                      ),
//                                                    ),
//                                                    Container(
//                                                      margin: EdgeInsets.only(
//                                                          left: 8 /
//                                                              360 *
//                                                              screenWidth),
//                                                      child: Text(
//                                                        item.subtitle[1],
//                                                        style: TextStyle(
//                                                          color: Color.fromARGB(
//                                                              255, 153, 154, 171),
//                                                          fontSize: 12 /
//                                                              720 *
//                                                              screenHeight,
//                                                          fontFamily:
//                                                              "IBMPlexSans",
//                                                          fontWeight:
//                                                              FontWeight.w500,
//                                                        ),
//                                                        textAlign: TextAlign.left,
//                                                      ),
//                                                    ),
//                                                  ],
//                                                ),
//                                              ),
//                                            ],
//                                          ),
//                                        ),
//                                        Spacer(),
//                                        Container(
//                                          width: 7 / 360 * screenWidth,
//                                          height: 11 / 720 * screenHeight,
//                                          child: Opacity(
//                                            opacity: 0.5,
//                                            child: SvgPicture.asset(
//                                              "assets/images/shape_copy_3.svg",
//                                              fit: BoxFit.contain,
//                                            ),
//                                          ),
//                                        ),
//                                      ],
//                                    ),
//                                  ),
//                                );
//                              },
//                            ),
//                          ),
//                        ],
//                      ),
//                    ),

                        Column(

                            children: [

                              Container(

                                margin: EdgeInsets.only(left: 15 / 360 * screenWidth, top: 17 / 720 * screenHeight),

                                child: Align(
                                  alignment: Alignment.topLeft,
                                  child: Text(
                                    "Now Playing Live",
                                    style: TextStyle(
                                      color: Color.fromARGB(255, 0, 3, 44),
                                      fontSize: 18 / 720 * screenHeight,
                                      fontFamily: "IBMPlexSans",
                                      fontWeight: FontWeight.w700,
                                    ),
                                    textAlign: TextAlign.left,
                                  ),
                                ),
                              ),

                              Container(

                                height: 200 / 720 * screenHeight,

                                child: StreamBuilder<ApiResponse<NowPlayingLiveResponseModel>>(
                                    stream: _nowPlayingLiveBloc.nowPlayingLiveStream,
                                    builder: (context, snapshot) {

                                      if (snapshot.hasData) {

                                        switch(snapshot.data.status) {

                                          case Status.LOADING:

                                            return Loading(loadingMessage: snapshot.data.message,);

                                            break;

                                          case Status.COMPLETED:

                                            return Container(
                                              margin: EdgeInsets.fromLTRB(15.0, 0.0, 15.0, 0.0),
                                              child: RawScrollbar(
                                                
                                                isAlwaysShown: true,
                                                thickness: 5,
                                                radius: Radius.circular(20.0),
                                                thumbColor: Colors.lightBlueAccent,
                                                controller: _scrollController,

                                                child: ListView.builder(
//                                            itemCount: snapshot.data.data.data.length,
                                                      itemCount: 10,
                                                      shrinkWrap: true,
                                                      physics: ClampingScrollPhysics(),
                                                      scrollDirection: Axis.horizontal,

                                                      itemBuilder: (context, index) {

                                                        var date = DateFormat('dd MMM yyyy | HH:mm a').format(snapshot.data.data.data[index].timeStamp);

                                                        return Container(

                                                          width: screenWidth/1.25,
                                                          padding: EdgeInsets.all(10.0),
                                                          margin: EdgeInsets.fromLTRB(0.0, 15.0, 15.0, 15.0),

                                                          decoration: BoxDecoration(

                                                            color: Colors.lightBlueAccent.withAlpha(25),
                                                            borderRadius: BorderRadius.all(Radius.circular(10.0)),

                                                          ),

                                                          child: Row(

                                                            mainAxisAlignment: MainAxisAlignment.center,
                                                            children: <Widget>[

                                                              Image.asset("assets/images/avatar-2.png"),

/*                                                          Image.network(snapshot.data.data.data[index].component.thumbnail,

                                                                errorBuilder: (BuildContext context, Object exception, StackTrace stackTrace) {

                                                                  return Image.asset("/assets/images/avatar-2.png");
                                                                },
                                                              ),*/

                                                              SizedBox(width: 10.0,),

                                                              Flexible(
                                                                child: Column(

//                                                              mainAxisAlignment: MainAxisAlignment.start,
                                                                  crossAxisAlignment: CrossAxisAlignment.start,
                                                                  mainAxisSize: MainAxisSize.max,
                                                                  children: <Widget>[

                                                                    Text(
                                                                          snapshot.data.data.data[index].component.title,
                                                                          style: TextStyle(
                                                                            color: Color.fromARGB(255, 0, 3, 44),
                                                                            fontSize: 15 / 720 * screenHeight,
                                                                            fontFamily: "IBMPlexSans",
                                                                            fontWeight: FontWeight.w700,
                                                                          ),
                                                                          textAlign: TextAlign.left,
                                                                      ),

                                                                    SizedBox(height: 10.0,),

                                                                    Text(
                                                                          date,
                                                                          style: TextStyle(
                                                                            color: Color.fromARGB(255, 0, 3, 44),
                                                                            fontSize: 15 / 720 * screenHeight,
                                                                            fontFamily: "IBMPlexSans",
                                                                            fontWeight: FontWeight.w700,
                                                                          ),
                                                                          textAlign: TextAlign.left,
                                                                      ),

                                                                    Spacer(),

                                                                    Expanded(
                                                                      child: Align(

                                                                        alignment: Alignment.bottomCenter,

                                                                        child: Row(

                                                                            crossAxisAlignment: CrossAxisAlignment.start,
                                                                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                                            children: <Widget>[

                                                                              Row(

                                                                                children: <Widget>[

                                                                                  Icon(Icons.play_circle_fill, color: Colors.red,),
                                                                                  Text("Join Now", style: TextStyle(
                                                                                    color: Color.fromARGB(255, 0, 3, 44),
                                                                                    fontSize: 15 / 720 * screenHeight,
                                                                                    fontFamily: "IBMPlexSans",
                                                                                    fontWeight: FontWeight.w700,
                                                                                  ),)

                                                                                ],

                                                                              ),

                                                                              Icon(Icons.insert_drive_file_outlined, color: Colors.lightBlueAccent,),

                                                                            ],

                                                                          ),
                                                                      ),
                                                                    )

                                                                  ],

                                                                ),
                                                              )

                                                            ],

                                                          ),



                                                        );

                                                      }
                                                    ),
                                              ),
                                            );

                                            break;

                                          case Status.ERROR:

                                            print("Error Occurred");

                                            return Error(
                                              errorMessage: snapshot.data.message,
                                              onRetryPressed: () => _nowPlayingLiveBloc.fetchNowPlayingLive(),
                                            );

                                            break;
                                        }

                                      }

                                      return Container();

                                    }),
                              ),

                              Container(

                                margin: EdgeInsets.all(15.0),

                                child: Row(

                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: <Widget>[

                                    Image.asset("assets/images/channel_headline.png",),
                                    Image.asset("assets/images/search_icon.png"),

                                  ],

                                ),
                              ),

                              Container(

                                color: Colors.white,
                                height: MediaQuery.of(context).size.height / 2,
                                child: DefaultTabController(
                                  length: 6,
                                  child: Scaffold(
                                    backgroundColor: Colors.white,
                                    appBar: PreferredSize(

                                      preferredSize: Size.fromHeight(kToolbarHeight),
                                      child: TabBar(

                                        isScrollable: true,
                                        labelColor: Colors.blue,
                                        unselectedLabelColor: Colors.grey,
                                        indicatorColor: Colors.blue,
                                        indicatorSize: TabBarIndicatorSize.label,
                                        indicator: BoxDecoration(

                                          border: Border.all(color: Colors.blue, width: 1.0),
                                          borderRadius: BorderRadius.all(Radius.circular(50.0)),

                                        ),

                                        tabs: [

                                        Container(

                                          width: 125.0,

                                          decoration: BoxDecoration(

                                            border: Border.all(color: Colors.grey, width: 0.5),
                                            borderRadius: BorderRadius.all(Radius.circular(50.0)),

                                          ),

                                          child: Align(
                                              alignment: Alignment.center,
                                              child: Text("All", )),
                                        ),

                                        Container(

                                          width: 125.0,

                                          decoration: BoxDecoration(

                                            border: Border.all(color: Colors.grey, width: 0.5),
                                            borderRadius: BorderRadius.all(Radius.circular(50.0)),

                                          ),

                                          child: Align(
                                              alignment: Alignment.center,
                                              child: Text("Live Classes", )),
                                        ),

                                        Container(

                                          width: 125.0,

                                          decoration: BoxDecoration(

                                            border: Border.all(color: Colors.grey, width: 0.5),
                                            borderRadius: BorderRadius.all(Radius.circular(50.0)),

                                          ),

                                          child: Align(
                                              alignment: Alignment.center,
                                              child: Text("Master Classes", )),
                                        ),

                                        Container(

                                          width: 125.0,

                                          decoration: BoxDecoration(

                                            border: Border.all(color: Colors.grey, width: 0.5),
                                            borderRadius: BorderRadius.all(Radius.circular(50.0)),

                                          ),

                                          child: Align(
                                              alignment: Alignment.center,
                                              child: Text("Future You", )),
                                        ),

                                        Container(

                                          width: 125.0,

                                          decoration: BoxDecoration(

                                            border: Border.all(color: Colors.grey, width: 0.5),
                                            borderRadius: BorderRadius.all(Radius.circular(50.0)),

                                          ),

                                          child: Align(
                                              alignment: Alignment.center,
                                              child: Text("Last Mile to CAT", )),
                                        ),

                                        Container(

                                          width: 125.0,

                                          decoration: BoxDecoration(

                                            border: Border.all(color: Colors.grey, width: 0.5),
                                            borderRadius: BorderRadius.all(Radius.circular(50.0)),

                                          ),

                                          child: Align(
                                              alignment: Alignment.center,
                                              child: Text("Exam Analysis", )
                                          ),
                                        ),

                                      ],),

                                    ),

                                  ),

                                )


/*                                TabBarView(children: [

                                  Tab(text: "All",),
                                  Tab(text: "Live Classes",),
                                  Tab(text: "Master Classes",),
                                  Tab(text: "Future You",),
                                  Tab(text: "Last Mile to CAT",),
                                  Tab(text: "Exam Analysis",),

                                ], ),*/

                              )

                            ],
                          ),

                        Container(
                          //height: 343 / 720 * screenHeight,
//                          margin: EdgeInsets.only(top: 40 / 720 * screenHeight),
                          child: Stack(
                            alignment: Alignment.topCenter,
                            children: [
                              Positioned(
                                left: 0,
                                top: 29 / 720 * screenHeight,
                                right: 0,
                                child: Container(
                                  //height: 314 / 720 * screenHeight,
                                  child: Image.asset(
                                    "assets/images/bg-5.png",
                                    fit: BoxFit.cover,
                                  ),
                                ),
                              ),
                              Positioned(
                                child: (myListOfStrings == null) ? Container(child: LinearProgressIndicator(),) :Container(
                                  // height: 343,
                                  //margin: EdgeInsets.only(left:20/ 360 * screenWidth, right: 20 / 360 * screenWidth),
                                  child: GridView.builder(
                                    gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                                      crossAxisCount: 3,
                                    ),
                                    itemCount: myListOfStrings.length,
                                    shrinkWrap: true,
                                    padding: EdgeInsets.only(left:10/ 360 * screenWidth, right: 10 / 360 * screenWidth),
                                    physics: NeverScrollableScrollPhysics(),
                                    itemBuilder: (context,index){
                                      return GestureDetector(
                                        onTap: () {

                                          if (myListOfStrings[index] == 'Prepare')
                                          {
                                            setState(() {
                                              menuId = myListOfIds[index];
                                              menuDesc = myListofDescs[index];
                                            });
                                            print("Menu ID" + menuId);
                                            print("Menu Desc" + menuDesc);
                                            setIds(menuId);
                                            setDesc(menuDesc);
                                            Navigator.push(
                                                context,
                                                MaterialPageRoute(
                                                  builder: (context) =>
                                                      PrepareHomeScreenHtml(),
                                                ));
                                          }
                                          if (myListOfStrings[index] == 'Prepare - html')
                                          {
                                            Navigator.push(
                                                context,
                                                MaterialPageRoute(
                                                  builder: (context) =>
                                                      PrepareHomeScreenHtml(),
                                                ));
                                          }
                                          if (myListOfStrings[index] == 'E-maximiser')
                                            //if (myListOfStrings[index] == 'E-maximiser')
                                              {
                                            setState(() {
                                              menuId = myListOfIds[index];
                                              menuDesc = myListofDescs[index];
                                            });
                                            print("Menu ID" + menuId);
                                            print("Menu Desc" + menuDesc);
                                            setIds(menuId);
                                            setDesc(menuDesc);
                                            Navigator.push(
                                                context,
                                                MaterialPageRoute(
                                                  builder: (context) =>
                                                      EmaximiserHomeScreen(),
                                                ));
                                          }

                                          if (myListOfStrings[index] == 'Practice')
                                            //if (myListOfStrings[index] == 'Practice')
                                              {
                                            setState(() {
                                              menuId = myListOfIds[index];
                                              menuDesc = myListofDescs[index];
                                            });
                                            print("Menu ID" + menuId);
                                            print("Menu Desc" + menuDesc);
                                            setIds(menuId);
                                            setDesc(menuDesc);
                                            Navigator.push(
                                                context,
                                                MaterialPageRoute(
                                                  builder: (context) =>
                                                      PracticeHomeScreen(),
                                                ));
                                          }
                                          if (myListOfStrings[index] == 'GK-Zone')
                                            //if (myListOfStrings[index] == 'GK-Zone')
                                              {
                                            // setState(() {
                                            //   menuId = myListOfIds[index];
                                            //   menuDesc = myListofDescs[index];
                                            // });
                                            // print("Menu ID" + menuId);
                                            // print("Menu Desc" + menuDesc);
                                            // setIds(menuId);
                                            // setDesc(menuDesc);
                                              Navigator.push(
                                                  context,
                                                  MaterialPageRoute(
                                                    builder: (context) =>
                                                        GKZoneHomePage(),
                                                  ));
                                          }
                                          if (myListOfStrings[index] == 'Channels')
                                            //if (myListOfStrings[index] == 'Channels')
                                              {
                                            setState(() {
                                              menuId = myListOfIds[index];
                                              menuDesc = myListofDescs[index];
                                            });
                                            print("Menu ID" + menuId);
                                            print("Menu Desc" + menuDesc);
                                            setIds(menuId);
                                            setDesc(menuDesc);
//                                           Navigator.push(
//                                               context,
//                                               MaterialPageRoute(
//                                                 builder: (context) =>
//                                                     ChannelHome(),
//                                               ));
                                          }
                                          if (myListOfStrings[index] == 'Test')
                                            //if (myListOfStrings[index] == 'Analyse')
                                              {
                                            setState(() {
                                              menuId = myListOfIds[index];
                                              menuDesc = myListofDescs[index];
                                            });
                                            print("Menu ID" + menuId);
                                            print("Menu Desc" + menuDesc);
                                            setIds(menuId);
                                            setDesc(menuDesc);
                                            Navigator.push(
                                                context,
                                                MaterialPageRoute(
                                                  builder: (context) =>
                                                      AnalyticsHomeScreen(),
                                                ));
                                          }
                                          //if (myListOfStrings[index] == 'Discussions')
                                          if (myListOfStrings[index] == 'Discussions')
                                          {
//                                           Navigator.push(
//                                               context,
//                                               MaterialPageRoute(
//                                                 builder: (context) =>
//                                                     DiscussionHome(),
//                                               ));
                                          }
                                          //if (myListOfStrings[index] == 'Leaderboard')
                                          if (myListOfStrings[index] == 'Leaderboard')
                                          {
//                                           Navigator.push(
//                                               context,
//                                               MaterialPageRoute(
//                                                 builder: (context) =>
//                                                     LeaderboardScreen(),
//                                               ));
                                          }
                                          //if (myListOfStrings[index] == 'GDPI')
                                          if (myListOfStrings[index] == 'GD-PI')
                                          {
                                            setState(() {
                                              menuId = myListOfIds[index];
                                              menuDesc = myListofDescs[index];
                                            });
                                            print("Menu ID" + menuId);
                                            print("Menu Desc" + menuDesc);
                                            setIds(menuId);
                                            setDesc(menuDesc);
                                          Navigator.push(
                                              context,
                                              MaterialPageRoute(
                                                builder: (context) =>
                                                    Gdpi_HomeScreen(),
                                              ));
                                          }
                                          //if (myListOfStrings[index] == 'Blogs')
                                          if (myListOfStrings[index] == 'Blogs')
                                          {
                                            Navigator.push(
                                                context,
                                                MaterialPageRoute(
                                                  builder: (context) =>
                                                      Blogs(),
                                                ));
                                          }

                                          if (myListOfStrings[index] == 'myPLAN')
                                            //if (myListOfStrings[index] == 'Channels')
                                              {
                                            setState(() {
                                              menuId = myListOfIds[index];
                                              menuDesc = myListofDescs[index];
                                            });
                                            print("Menu ID" + menuId);
                                            print("Menu Desc" + menuDesc);
                                            setIds(menuId);
                                            setDesc(menuDesc);
                                            if (buttonClickOnlyOnce == false) {
                                              setState(() {
                                                buttonClickOnlyOnce = true;
                                                loadForMyPlan=true;
                                              });
                                              addStudent();
                                            }
                                          }
                                          //if (myListOfStrings[index] == 'Event Booking')
                                          if (myListOfStrings[index] == 'Event Booking')
                                          {
//                                           Navigator.push(
//                                               context,
//                                               MaterialPageRoute(
//                                                 builder: (context) =>
//                                                     EventBookingWidget(),
//                                               ));
                                          }
                                          //  if (myListOfStrings[index] == 'Sample API Loading')
                                          //  {
                                          //    Navigator.push(
                                          //        context,
                                          //        MaterialPageRoute(
                                          //          builder: (context) =>
                                          //              GoogleLoginScreen(),
                                          //        ));
                                          //  }
                                        },
                                        child: Container(
                                          height: screenHeight * 100 / 720,
                                          width: screenWidth *100/360,
                                          margin: EdgeInsets.all( 5/ 360 * screenWidth),
                                          decoration: BoxDecoration(
                                            color: Color.fromARGB(
                                                255, 255, 255, 255),
                                            boxShadow: [
                                              BoxShadow(
                                                color: Color.fromARGB(
                                                    128, 224, 224, 224),
                                                offset: Offset(0, 0),
                                                blurRadius: 16,
                                              ),
                                            ],
                                            borderRadius:
                                            BorderRadius.all(
                                                Radius.circular(5)),
                                          ),

                                          child: Column(
                                            children: <Widget>[
                                              Align(
                                                alignment: Alignment.centerLeft,
                                                child: Container(
                                                  margin: EdgeInsets.only(top: 20 /720 * screenHeight),
                                                  child: GestureDetector(
                                                    child: Column(
                                                      crossAxisAlignment: CrossAxisAlignment.center,
                                                      mainAxisAlignment: MainAxisAlignment.center,
                                                      children: [
                                                        Container(
                                                            width: screenWidth * 35 / 360,
                                                            height: screenHeight * 35/ 720,
                                                            margin: EdgeInsets.only(left: 25/360*screenWidth,right:25/360*screenWidth,bottom: 10 /720 * screenHeight),
                                                            child:
                                                            Center(
                                                              child: new SvgPicture.asset(
                                                                myListOfStrings[index] == 'Prepare - Web'
                                                                //myListOfStrings[index] == 'Prepare'
                                                                    ? getSvgIcon.prepare

                                                                    : //myListOfStrings[index] == 'Prepare'
                                                                myListOfStrings[index] == 'Prepare'
                                                                    ? getSvgIcon.prepare

                                                                    : myListOfStrings[index] == 'Prepare - html'
                                                                //myListOfStrings[index] == 'Prepare'
                                                                    ? getSvgIcon.prepare
                                                                    : myListOfStrings[index] == 'E-maximiser - Web'
                                                                //myListOfStrings[index] == 'E-maximiser'
                                                                    ? getSvgIcon
                                                                    .maximiser

                                                                    : //myListOfStrings[index] == 'E-maximiser'
                                                                myListOfStrings[index] == 'E-maximiser'
                                                                    ? getSvgIcon
                                                                    .maximiser
                                                                    : //taticlist[index] == 'Practice'
                                                                myListOfStrings[index] == 'Practice'
                                                                    ? getSvgIcon
                                                                    .practice
                                                                    : //myListOfStrings[index] == 'GK-Zone'
                                                                myListOfStrings[index] == 'GK-Zone'
                                                                    ? getSvgIcon
                                                                    .gkzone
                                                                    : //myListOfStrings[index] == 'Channels'
                                                                myListOfStrings[index] == 'Channels'
                                                                    ? getSvgIcon.chanel
                                                                    : //myListOfStrings[index] == 'Analyse'
                                                                myListOfStrings[index] == 'Test'
                                                                    ? getSvgIcon.analysis
                                                                    : //myListOfStrings[index] == 'Discussions'
                                                                myListOfStrings[index] == 'Discussions'
                                                                    ? getSvgIcon
                                                                    .maximiser
                                                                    : myListOfStrings[index] == 'Leaderboard'
                                                                //myListOfStrings[index] == 'Leaderboard'
                                                                    ? getSvgIcon
                                                                    .practice
                                                                    : myListOfStrings[index] == 'GD-PI'
                                                                //myListOfStrings[index] == 'GDPI'
                                                                    ? getSvgIcon
                                                                    .gdpi
                                                                    : myListOfStrings[index] == 'Blogs'
                                                                //myListOfStrings[index] == 'Blogs'
                                                                    ? getSvgIcon.blogs
                                                                    : //myListOfStrings[index] == 'Event Booking'
                                                                myListOfStrings[index] == 'Event Booking'
                                                                    ? getSvgIcon.chanel
                                                                    : getSvgIcon.practice,

                                                                fit: BoxFit.contain,
                                                                //color: Colors.red,
                                                              ),
                                                            )),
                                                        Center(
                                                          child: Container(
                                                            // margin: EdgeInsets.only(left: 17 / 360 * screenWidth, top: 4 / 720 * screenHeight,left: 17 / 360 * screenWidth),
                                                            //  margin: EdgeInsets.only(top: 4 / 720 * screenHeight),
                                                            child: Text(
                                                              //myListOfStrings[index]
                                                              myListOfStrings[index] == "Test" ? "Analytics" : myListOfStrings[index] == "Prepare" ? "Learn" : myListOfStrings[index] == "GK-Zone" ? "GK Zone" : myListOfStrings[index] == "Channels" ? "Channel" :  ((myListOfStrings[index] == "E-maximiser") && (courseName != "PGDBA"))  ? "e-Maximiser": ((myListOfStrings[index] == "E-maximiser") && (courseName == "PGDBA")) ? "PGDBA Workshops": ((myListOfStrings[index] == "myPLAN") && (courseName != "GDPI")) ?loadForMyPlan==true?"Loading":"myPLAN":((myListOfStrings[index] == "myPLAN") && (courseName == "GDPI")) ?loadForMyPlan==true?"Loading":"PI Slot Booking":myListOfStrings[index],
                                                              style: TextStyle(
                                                                color: Color.fromARGB(
                                                                    255, 0, 3, 44),
                                                                fontSize: 14 /
                                                                    720 *
                                                                    screenHeight,
                                                                fontFamily:
                                                                "IBMPlexSans",
                                                              ),
                                                              textAlign: TextAlign.center,
                                                              overflow: TextOverflow.ellipsis,
                                                              maxLines: 1,
                                                            ),
                                                          ),
                                                        ),
                                                      ],
                                                    ),
                                                  ),
                                                ),
                                              ),
//                                 index == mylist.length - 1
//                                 //index == mylist.length - 1
//                                     ? Container(
//                                   height: 1 / 720 * screenHeight,
//                                 )
//                                     : Container(
//                                     height: 1 / 720 * screenHeight,
//                                     margin: EdgeInsets.only(
//                                         bottom:
//                                         15 / 720 * screenHeight,
//                                         right:
//                                         20 / 360 * screenWidth),
//                                     decoration: BoxDecoration(
//                                       color: Color.fromARGB(
//                                           255, 242, 244, 244),
//                                     )),
                                            ],
                                          ),
                                        ),
                                      );
                                    },
                                  ),
                                ),

                              )
                            ],
                          ),
                        ),

                        //Blogs Title and See All button
                        Container(
                          height: 35 / 720 * screenHeight,
                          margin: EdgeInsets.only(top: 17 / 720 * screenHeight),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.stretch,
                            children: [
                              Container(
                                height: 23 / 720 * screenHeight,
                                margin: EdgeInsets.symmetric(
                                    horizontal: 20 / 360 * screenWidth),
                                child: Row(
                                  crossAxisAlignment: CrossAxisAlignment.stretch,
                                  children: [
                                    Align(
                                      alignment: Alignment.topLeft,
                                      child: Text(
                                        "Blogs",
                                        style: TextStyle(
                                          color: Color.fromARGB(255, 0, 3, 44),
                                          fontSize: 18 / 720 * screenHeight,
                                          fontFamily: "IBMPlexSans",
                                          fontWeight: FontWeight.w700,
                                        ),
                                        textAlign: TextAlign.left,
                                      ),
                                    ),
                                    Spacer(),
                                    Align(
                                      alignment: Alignment.centerRight,
                                      child: GestureDetector(
                                        onTap: () {
                                          Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                                builder: (context) => Blogs()),
                                          );
                                        },
                                        child: Container(
                                          child: Center(
                                            child: Text(
                                              "See All",
                                              style: TextStyle(
                                                color:
                                                Color.fromARGB(255, 0, 171, 251),
                                                fontSize: 12 / 720 * screenHeight,
                                                fontFamily: "IBMPlexSans",
                                                fontWeight: FontWeight.w500,
                                              ),
                                              textAlign: TextAlign.left,
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),

                        //Blogs section and Article of the week
                        Container(
                          //height: 430.0 / 720 * screenHeight,
                            child: BlogTabWidget(this._tabController, this.databaseTablist, this.fetchData,this.databasearticleDataTitle,this.databasearticleDataLink,this.databasearticleDataTime,this.databasearticleDataImage,this.tabList)
                        ),


                      ]),
                ),
              );
            },
          ),
        ),

      );
    }

    return WillPopScope(
        onWillPop: _onWillPop,
        child: Scaffold(
            key: _scaffoldstatee,
//            drawerScrimColor: Color(0xFF0f4f8d).withOpacity(0.83),
            appBar: new AppBar(
              brightness: Brightness.light,
              elevation: 0.0,
              centerTitle: false,
              iconTheme: IconThemeData(
                color: Colors.black, //change your color here
              ),
              title: Text(
                "Hi, Welcome Back",
                style: TextStyle(
                  color: Color.fromARGB(255, 0, 0, 0),
                  fontSize: 16 / 720 * screenHeight,
                  fontFamily: "IBMPlexSans",
                  fontWeight: FontWeight.w500,
                ),
              ),
              titleSpacing: 0.0,
              actions: <Widget>[

                Row(

                  children: <Widget>[

                    IconButton(icon: Icon(Icons.search, color: Colors.black,), onPressed: () => debugPrint("Search")),
                    IconButton(icon: Icon(Icons.notifications_none_outlined, color: Colors.black,), onPressed: () => debugPrint("Notification")),

                  ],

                )



//            Container(
//                // width:0.130*screenWidth,
//                margin: EdgeInsets.only(right: 20 / 360  * screenWidth),
//                child: Row(
//                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                    children: [
//                      Container(
//                        child: InkWell(
//                          onTap: () {
//                            Navigator.push(
//                              context,
//                              MaterialPageRoute(builder: (context) => Search()),
//                            );
//                          },
//                          child: Padding(
//                            padding: const EdgeInsets.all(5.0),
//                            child: Container(
//                              height: 20 / 720 * screenHeight,
//                              width: 20 / 360 * screenWidth,
//                              child: new SvgPicture.asset(
//                                getSvgIcon.search,
//                                fit: BoxFit.contain,
//                                //color: Colors.red,
//                              ),
//                            ),
//                          ),
//                        ),
//                      ),
//
//                      Container(
//
//                        child: Stack(
//                          alignment: Alignment.center,
//                          children: [
//
//                            Positioned(
//                              child:   Container(
//                               // padding: EdgeInsets.only(left: 18 / 360 * screenWidth),
//                                child: InkWell(
//                                  onTap: () {
//                                    Navigator.push(
//                                      context,
//                                      MaterialPageRoute(
//                                          builder: (context) => Notifications()),
//                                    );
//                                  },
//                                  child: Padding(
//                                    padding: const EdgeInsets.all(5.0),
//                                    child: Container(
//                                      height: 20 / 720 * screenHeight,
//                                      width: 20 / 360 * screenWidth,
//                                      child: new SvgPicture.asset(
//                                        getSvgIcon.notification,
//                                        fit: BoxFit.contain,
//                                        //color: Colors.red,
//                                      ),
//                                    ),
//                                  ),
//                                ),
//                              ),
//                            ),
//
//                            Positioned(
//                              right: 8.7 / 360 * screenWidth,
//                              top:8.5 / 720 * screenHeight,
////                              child:CircleAvatar(
////                                radius: 3.0,
////                                backgroundColor:NeutralColors.grapefruit,
////                              ),
//                            child:Container(
//                              height:5 / 360 * screenWidth,
//                              width: 5 / 720 * screenHeight,
//                              decoration: new BoxDecoration(
//                                shape: BoxShape.circle,
//                                color: NeutralColors.grapefruit,
//                              ),
//                            ),
//                            ),
//                          ],
//                        ),
//
//
//
//                      ),
//                    ])),
              ],
              backgroundColor: Colors.white,
            ),
            drawer: NavigationDrawer(), //this will just add the Navigation Drawer Icon
            bottomNavigationBar: BottomNavigation(),
            body: body())
    );
  }
}


//UI Widget: Banner on the top of home screen
class ScrollTabWidget extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return ScrollTabWidgetState();
  }
}

class ScrollTabWidgetState extends State<ScrollTabWidget> {
  int _currentPage = 0;
  PageController _pageController = PageController(
    initialPage: 0,
  );

  @override
  void initState() {
    super.initState();
    Timer.periodic(Duration(seconds: 3), (Timer timer) {
      if (_currentPage <= 2) {
        _currentPage++;
      } else {
        _currentPage = 0;
      }
      _currentPage == 3 ? null :
      _pageController.animateToPage(
        _currentPage,
        duration: Duration(milliseconds: 550),
        curve: Curves.easeIn,
      );
    });
  }

  @override
  Widget build(BuildContext context) {
    final screenHeight = MediaQuery.of(context).size.height;
    final screenWidth = MediaQuery.of(context).size.width;
    return PageView.builder(
      controller: _pageController,
      itemCount: 3,
      itemBuilder: (context,index){
        return  Container(
          child: Stack(
            alignment: Alignment.bottomRight,
            children: [
              Positioned(
                left: 15 / 360 * screenWidth,
                top: 6 / 720 * screenHeight,
                right: 26 / 360 * screenWidth,
                bottom: 10 / 720 * screenHeight,
                child: Stack(
                  children: [
                    Positioned(
                      left: 0,
                      top: 0,
                      child: Text(
                        "Last Mile to CAT Mumbai 2018 bookings open now!",
                        style: TextStyle(
                          color: Color.fromARGB(255, 255, 255, 255),
                          fontSize: 12 / 720 * screenHeight,
                          fontFamily: "IBMPlexSans",
                          fontWeight: FontWeight.w500,
                        ),
                        textAlign: TextAlign.left,
                      ),
                    ),
                    Positioned(
                      left: 0,
                      top: 18 / 720 * screenHeight,
                      child: Text(
                        "Click here to book a slot",
                        style: TextStyle(
                          color: Color.fromARGB(255, 255, 255, 255),
                          fontSize: 12 / 720 * screenHeight,
                          fontFamily: "IBMPlexSans",
                        ),
                        textAlign: TextAlign.left,
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        );
      },
    );
  }
}


//UI Widget: Blogs section with three blogs for each topic
class BlogTabWidget extends StatefulWidget {
  TabController _tabController;
  List databaseTablist;
  List fetchData;
  List tabList;
  String databasearticleDataTitle;
  String databasearticleDataLink;
  String databasearticleDataTime;
  String databasearticleDataImage;
  BlogTabWidget(this._tabController, this.databaseTablist, this.fetchData,this.databasearticleDataTitle,this.databasearticleDataLink,this.databasearticleDataTime,this.databasearticleDataImage,this.tabList);
  @override
  State<StatefulWidget> createState() {
    return BlogTabWidgetState();
  }
}



class BlogTabWidgetState extends State<BlogTabWidget>
    with TickerProviderStateMixin {



  @override
  void initState() {
    print("blog init state called");
    // obj = SqliteHelper('imsApp.db');
    // obj.create();

    widget._tabController;
    //= new TabController(vsync: this, length: widget.tabList.length);
    super.initState();
  }

  void dispose() {
    widget._tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final screenHeight = MediaQuery.of(context).size.height;
    final screenWidth = MediaQuery.of(context).size.width;

    return widget._tabController == null ? Center(child: CircularProgressIndicator()) :   Container(
      color: Colors.white,
      width: screenWidth,
      padding: EdgeInsets.only(
        // top: (326 / 764) * screenHeight,
        // top: 20.0,
      ),
      child: new Column(
        children: <Widget>[
          widget._tabController == null ? Center(child: CircularProgressIndicator()) :  new Container(
            height: 36 / 720 * screenHeight,
            margin: EdgeInsets.only(left: 20.0 / 360 * screenWidth),

            width: double.infinity,
            // decoration: new BoxDecoration(color: Theme.of(context).primaryColor),
            child: new TabBar(
              controller: widget._tabController,
              indicatorColor: NeutralColors.purpleish_blue,
              labelColor: NeutralColors.purpleish_blue,
              unselectedLabelColor: NeutralColors.blue_grey,
              indicatorSize: TabBarIndicatorSize.tab,
              tabs: widget.databaseTablist,

              isScrollable: true,
              unselectedLabelStyle: TextStyle(
                fontSize: 14 / 720 * screenHeight,
                fontFamily: "IBMPlexSans",
                fontWeight: FontWeight.w400,
              ),
              labelStyle: TextStyle(
                fontSize: 14 / 720 * screenHeight,
                fontFamily: "IBMPlexSans",
                fontWeight: FontWeight.w500,
              ),
            ),
          ),
          new Container(
            height: 390.0 / 720 * screenHeight,
            child: widget.fetchData.length == widget.databaseTablist.length && widget.fetchData.length != 0 ? new TabBarView(
                controller: widget._tabController,
                children: widget.fetchData
            ) : Container(
              child: Text("No blogs to display",
                style: TextStyle(
                  fontSize: 12 / 720 * screenHeight,
                  fontFamily: "IBMPlexSans",
                  fontWeight: FontWeight.w500,
                ),),
            ),
          ),
          //Article of the week
          widget.databasearticleDataTitle != null ?   Container(
            //height: 258 / 720 * screenHeight,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                Container(
                  margin:
                  EdgeInsets.only(left: 18.0 / 360 * screenWidth, ),
                  child: Text(
                    "Article of the week",
                    style: TextStyle(
                      color: Color.fromARGB(255, 0, 3, 44),
                      fontSize: 18 / 720 * screenHeight,
                      fontFamily: "IBMPlexSans",
                      fontWeight: FontWeight.w700,
                    ),
                    textAlign: TextAlign.left,
                  ),
                ),
                GestureDetector(
                  onTap: (){
                    if(widget.databasearticleDataLink != ""){
                      var string = widget.databasearticleDataLink;
                      try {
                        launchURL('$string');
                      } catch (e) {
                        print(e.toString());
                      }
                    }
                  },
                  child: Container(
                    //color: Colors.amber,
                    height: 180 / 720 * screenHeight,
                    margin: EdgeInsets.only(
                        top: 20 / 720 * screenHeight,
                        right: 7 / 360 * screenWidth,
                        bottom: 40 / 720 * screenHeight),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: [
                        Align(
                          alignment: Alignment.topLeft,
                          child: Container(
                            width: 3 / 360 * screenWidth,
                            height: 40 / 720 * screenHeight,
                            margin: EdgeInsets.only(
                                top: 8 / 720 * screenHeight, left: 20.0 / 360 * screenWidth ),
                            decoration: BoxDecoration(
                              color: Color.fromARGB(255, 136, 109, 215),
                              borderRadius:
                              BorderRadius.all(Radius.circular(1.5)),
                            ),
                            child: Container(),
                          ),
                        ),
                        Container(
                          //color: Colors.red,
                          width: 140 / 360 * screenWidth,
                          margin: EdgeInsets.only(
                              top: 4 / 720 * screenHeight,
                              left: 10 / 360 * screenWidth,
                              right: 10 / 360 * screenWidth,
                              bottom: 20 / 720 * screenHeight),
                          child: Column(
                            crossAxisAlignment:
                            CrossAxisAlignment.stretch,
                            children: [
                              Align(
                                alignment: Alignment.topLeft,
                                child: Container(
                                  //width: 140 / 720 * screenHeight,
                                  child: Text(
                                    widget.databasearticleDataTitle.toString(),
                                    style: TextStyle(
                                      color:
                                      Color.fromARGB(255, 0, 3, 44),
                                      fontSize: 14 / 720 * screenHeight,
                                      fontFamily: "IBMPlexSans",
                                      fontWeight: FontWeight.w500,
                                    ),
                                    textAlign: TextAlign.left,
                                  ),
                                ),
                              ),
                              Spacer(),
                              Align(
                                alignment: Alignment.topLeft,
                                child: Opacity(
                                  opacity: 0.6,
                                  child: Container(
                                    // margin: EdgeInsets.only(bottom: 60 / 360 * screenWidth),
                                    child: Text(
                                      widget.databasearticleDataTime,
                                      style: TextStyle(
                                        color:
                                        Color.fromARGB(255, 0, 3, 44),
                                        fontSize: 12 / 720 * screenHeight,
                                        fontFamily: "IBMPlexSans",
                                        fontWeight: FontWeight.w500,
                                      ),
                                      textAlign: TextAlign.left,
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                        widget.databasearticleDataImage != null ? Expanded(
                          flex: 1,
                          child: Align(
                            alignment: Alignment.topRight,
                            child: Container(
                              //color: Colors.blue,
                                height: 180 / 720 * screenHeight,
                                margin: EdgeInsets.only(
                                    right: 16 / 360 * screenWidth),
//                              child: Image.network(
//
//                                widget.databasearticleDataImage.toString(),
//                                fit: BoxFit.fill,
//                              ),
                                child: widget.databasearticleDataImage.toString() == "null" ?
                                Image.asset('assets/images/imageplaceholder.jpg')
                                    : FadeInImage.assetNetwork(
                                  placeholder: 'assets/images/imageplaceholder.jpg',
                                  image:  widget.databasearticleDataImage.toString(),
                                  imageErrorBuilder: (context, error, stackTrace) {
                                    return Container(
                                      child: Image.asset("assets/images/imageplaceholder.jpg"),
                                    );
                                  },
                                )
                            ),
                          ),
                        ): Container(),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ) : Container(),
        ],
      ),
    );
  }
}


var screenWidthTotal;
var screenHeightTotal;
var _safeAreaHorizontal;
var _safeAreaVertical;
var screenHeight;
var screenWidth;

//UI Widget: Section with three blogs for each topic
class PostWidget extends StatefulWidget {
  int i;
  List listofposts;
  List listoflink;
  List listofimages;
  List listoftime;
  PostWidget(this.i, this.listofposts, this.listoflink,this.listofimages,this.listoftime);
  @override
  State<StatefulWidget> createState() {
    return PostWidgetState();
  }
}



void CalculateScreen(BuildContext context) {
  var _mediaQueryData = MediaQuery.of(context);
  screenWidthTotal = _mediaQueryData.size.width;
  screenHeightTotal = _mediaQueryData.size.height;
  _safeAreaHorizontal =
      _mediaQueryData.padding.left + _mediaQueryData.padding.right;
  _safeAreaVertical =
      _mediaQueryData.padding.top + _mediaQueryData.padding.bottom;

  screenHeight = screenHeightTotal - _safeAreaVertical;
  screenWidth = screenWidthTotal - _safeAreaHorizontal;
}

class PostWidgetState extends State<PostWidget>{


  @override
  Widget build(BuildContext context) {
    CalculateScreen(context);

    return Container(
      child: ListView.builder(
        physics: NeverScrollableScrollPhysics(),
        itemCount: widget.listofposts.length > 3 ? 3 : widget.listofposts.length,
        itemBuilder: (context,index){
          return GestureDetector(
            onTap: (){
              setState(() {
                try {
                  launchURL('${widget.listoflink[index]}');
                } catch (e) {
                  print(e.toString());
                }
              });
            },
            child: Container(
                height: 125.0 / 720 * screenHeight,
                margin: EdgeInsets.only(
                    left: 20.0 / 360 * screenWidth,
                    right: 20.0 / 360 * screenWidth),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Container(
                      height: 71.0 / 720 * screenHeight,
                      child: Column(
                        mainAxisAlignment:
                        MainAxisAlignment.spaceAround,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Container(
                            //height: 49 / 720 * screenHeight,
                            width: 210 / 360 * screenWidth,
                            child: Text(
                              widget.listofposts[index].toString(),
                              maxLines: 2,
                              overflow: TextOverflow.ellipsis,
                              style: TextStyle(
                                color: NeutralColors.dark_navy_blue,
                                fontSize: 14 / 720 * screenHeight,
                                fontFamily: "IBMPlexSans",
                                fontWeight: FontWeight.w500,
                              ),
                              textAlign: TextAlign.left,
                            ),
                          ),
                          Spacer(),
                          Container(
                            child: Row(
                              crossAxisAlignment:
                              CrossAxisAlignment.start,
                              children: [
                                Align(
                                  alignment: Alignment.topLeft,
                                  child: Container(
                                    child: Text(
                                      widget.listoftime[index],
                                      style: TextStyle(
                                        color: Color.fromARGB(
                                            255, 153, 154, 171),
                                        fontSize:
                                        12 / 720 * screenHeight,
                                        fontFamily:
                                        "IBMPlexSans",
                                        fontWeight: FontWeight.w500,
                                      ),
                                      textAlign: TextAlign.left,
                                    ),
                                  ),
                                ),
                                // Align(
                                //   alignment: Alignment.center,
                                //   child: Container(
                                //     width: 3 / 360 * screenWidth,
                                //     height: 3 / 720 * screenHeight,
                                //     margin: EdgeInsets.only(
                                //         left: 8 / 360 * screenWidth,
                                //         top: 6 / 720 * screenHeight),
                                //     decoration: BoxDecoration(
                                //       color: Color.fromARGB(
                                //           255, 216, 216, 216),
                                //       borderRadius: BorderRadius.all(
                                //           Radius.circular(1.5)),
                                //     ),
                                //     child: Container(),
                                //   ),
                                // ),
                                // Align(
                                //   alignment: Alignment.topLeft,
                                //   child: Container(
                                //     margin: EdgeInsets.only(
                                //         left: 8 / 360 * screenWidth),
                                //     child: Text(
                                //       "8 min read",
                                //       style: TextStyle(
                                //         color: Color.fromARGB(
                                //             255, 153, 154, 171),
                                //         fontSize:
                                //             12 / 360 * screenWidth,
                                //         fontFamily:
                                //             "IBMPlexSans",
                                //         fontWeight: FontWeight.w500,
                                //       ),
                                //       textAlign: TextAlign.left,
                                //     ),
                                //   ),
                                // ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                    Container(
                        height: 100 / 720 * screenHeight,
                        width: 90 / 360 * screenWidth,
                        child: widget.listofimages[index] == '' ?
                        Image.asset("assets/images/imageplaceholder.jpg")
                            :FadeInImage.assetNetwork(
                          placeholder: 'assets/images/imageplaceholder.jpg',
                          image:  widget.listofimages[index],
                          imageErrorBuilder: (context, error, stackTrace) {
                            return Container(
                              child: Image.asset("assets/images/imageplaceholder.jpg"),
                            );
                          },
                        )
                    ),
                  ],
                )
            ),
          );
        },
      ),
    );
  }
}


import 'package:flutter/material.dart';
import 'package:imsindia/views/home_pages/home_widget.dart';


class NotificationsWidget extends StatelessWidget {
  
  void onRectanglePressed(BuildContext context) {
  
  }
  
  void onArrowPointToRightPressed(BuildContext context) => Navigator.push(context, MaterialPageRoute(builder: (context) => HomeWidget()));
  
  @override
  Widget build(BuildContext context) {
  
    return Scaffold(
      body: Container(
        constraints: BoxConstraints.expand(),
        decoration: BoxDecoration(
          color: Color.fromARGB(255, 255, 255, 255),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Container(
              height: 57,
              margin: EdgeInsets.only(left: 8, top: 5, right: 8),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    width: 18,
                    height: 14,
                    margin: EdgeInsets.only(left: 12, top: 41),
                    child: FlatButton(
                      onPressed: () => this.onArrowPointToRightPressed(context),
                      color: Colors.transparent,
                      textColor: Color.fromARGB(255, 0, 0, 0),
                      padding: EdgeInsets.all(0),
                      child: Image.asset("assets/images/arrow-point-to-right.png",),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(left: 20, top: 37),
                    child: Text(
                      "Notifications",
                      style: TextStyle(
                        color: Color.fromARGB(255, 0, 0, 0),
                        fontSize: 16,
                        fontFamily: "IBMPlexSans",
                        fontWeight: FontWeight.w500,
                      ),
                      textAlign: TextAlign.left,
                    ),
                  ),
                ],
              ),
            ),
            Container(
              height: 30,
              margin: EdgeInsets.only(left: 20, top: 30),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  Container(
                    width: 105,
                    child: Stack(
                      alignment: Alignment.center,
                      children: [
                        Positioned(
                          left: 0,
                          top: 0,
                          child: Container(
                            width: 105,
                            height: 30,
                            child: FlatButton(
                              onPressed: () => this.onRectanglePressed(context),
                              color: Color.fromARGB(255, 86, 71, 235),
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.all(Radius.circular(15)),
                              ),
                              textColor: Color.fromARGB(255, 0, 0, 0),
                              padding: EdgeInsets.all(0),
                              child: Text("",),
                            ),
                          ),
                        ),
                        Positioned(
                          left: 14,
                          top: 4,
                          bottom: 6,
                          child: Text(
                            "Top Priority",
                            style: TextStyle(
                              color: Color.fromARGB(255, 255, 255, 255),
                              fontSize: 14,
                              fontFamily: "IBMPlexSans",
                              fontWeight: FontWeight.w500,
                            ),
                            textAlign: TextAlign.left,
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    width: 56,
                    margin: EdgeInsets.only(left: 10),
                    child: Stack(
                      alignment: Alignment.center,
                      children: [
                        Positioned(
                          left: 0,
                          top: 0,
                          child: Container(
                            width: 56,
                            height: 30,
                            decoration: BoxDecoration(
                              color: Color.fromARGB(255, 242, 246, 248),
                              borderRadius: BorderRadius.all(Radius.circular(15)),
                            ),
                            child: Container(),
                          ),
                        ),
                        Positioned(
                          left: 16,
                          top: 2,
                          bottom: 6,
                          child: Container(
                            width: 38,
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.stretch,
                              children: [
                                Container(
                                  margin: EdgeInsets.only(top: 2),
                                  child: Opacity(
                                    opacity: 0.7,
                                    child: Text(
                                      "IMS",
                                      style: TextStyle(
                                        color: Color.fromARGB(255, 126, 145, 154),
                                        fontSize: 14,
                                        fontFamily: "IBM Plex Sans",
                                      ),
                                      textAlign: TextAlign.left,
                                    ),
                                  ),
                                ),
                                Align(
                                  alignment: Alignment.topLeft,
                                  child: Container(
                                    width: 5,
                                    height: 5,
                                    margin: EdgeInsets.only(left: 7),
                                    decoration: BoxDecoration(
                                      color: Color.fromARGB(255, 0, 171, 251),
                                      borderRadius: BorderRadius.all(Radius.circular(2.5)),
                                    ),
                                    child: Container(),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    width: 88,
                    margin: EdgeInsets.only(left: 10),
                    child: Stack(
                      alignment: Alignment.center,
                      children: [
                        Positioned(
                          left: 0,
                          top: 0,
                          child: Container(
                            width: 88,
                            height: 30,
                            decoration: BoxDecoration(
                              color: Color.fromARGB(255, 242, 246, 248),
                              borderRadius: BorderRadius.all(Radius.circular(15)),
                            ),
                            child: Container(),
                          ),
                        ),
                        Positioned(
                          left: 16,
                          top: 4,
                          bottom: 6,
                          child: Opacity(
                            opacity: 0.7,
                            child: Text(
                              "B-School",
                              style: TextStyle(
                                color: Color.fromARGB(255, 126, 145, 154),
                                fontSize: 14,
                                fontFamily: "IBM Plex Sans",
                              ),
                              textAlign: TextAlign.left,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    width: 98,
                    margin: EdgeInsets.only(left: 10),
                    child: Stack(
                      alignment: Alignment.center,
                      children: [
                        Positioned(
                          left: 0,
                          top: 0,
                          child: Container(
                            width: 98,
                            height: 30,
                            decoration: BoxDecoration(
                              color: Color.fromARGB(255, 242, 246, 248),
                              borderRadius: BorderRadius.all(Radius.circular(15)),
                            ),
                            child: Container(),
                          ),
                        ),
                        Positioned(
                          left: 15,
                          top: 4,
                          bottom: 6,
                          child: Opacity(
                            opacity: 0.7,
                            child: Text(
                              "Personal",
                              style: TextStyle(
                                color: Color.fromARGB(255, 126, 145, 154),
                                fontSize: 14,
                                fontFamily: "IBM Plex Sans",
                              ),
                              textAlign: TextAlign.left,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            Container(
              height: 544,
              margin: EdgeInsets.only(left: 20, top: 40, right: 20),
              child: SingleChildScrollView(
                padding: EdgeInsets.all(0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    Container(
                      height: 220,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: [
                          Container(
                            height: 58,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.stretch,
                              children: [
                                Align(
                                  alignment: Alignment.topLeft,
                                  child: Container(
                                    width: 272,
                                    height: 51,
                                    child: Row(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                        Container(
                                          width: 3,
                                          height: 40,
                                          margin: EdgeInsets.only(top: 1),
                                          decoration: BoxDecoration(
                                            color: Color.fromARGB(255, 255, 87, 87),
                                            borderRadius: BorderRadius.all(Radius.circular(1.5)),
                                          ),
                                          child: Container(),
                                        ),
                                        Expanded(
                                          flex: 1,
                                          child: Container(
                                            height: 42,
                                            margin: EdgeInsets.only(left: 10),
                                            child: Column(
                                              crossAxisAlignment: CrossAxisAlignment.stretch,
                                              children: [
                                                Container(
                                                  margin: EdgeInsets.only(right: 1),
                                                  child: Text(
                                                    "IMS CAT 2018- Slot 2 Percentile Mapper!",
                                                    style: TextStyle(
                                                      color: Color.fromARGB(255, 0, 3, 44),
                                                      fontSize: 14,
                                                      fontFamily: "IBM Plex Sans",
                                                    ),
                                                    textAlign: TextAlign.left,
                                                  ),
                                                ),
                                                Spacer(),
                                                Align(
                                                  alignment: Alignment.topLeft,
                                                  child: Text(
                                                    "1 Minute ago",
                                                    style: TextStyle(
                                                      color: Color.fromARGB(255, 153, 154, 171),
                                                      fontSize: 12,
                                                      fontFamily: "IBMPlexSans",
                                                      fontWeight: FontWeight.w500,
                                                    ),
                                                    textAlign: TextAlign.left,
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                                Spacer(),
                                Container(
                                  height: 1,
                                  margin: EdgeInsets.only(bottom: 1),
                                  decoration: BoxDecoration(
                                    color: Color.fromARGB(255, 242, 244, 244),
                                  ),
                                  child: Container(),
                                ),
                              ],
                            ),
                          ),
                          Container(
                            height: 58,
                            margin: EdgeInsets.only(top: 15),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.stretch,
                              children: [
                                Align(
                                  alignment: Alignment.topLeft,
                                  child: Container(
                                    width: 227,
                                    height: 42,
                                    child: Row(
                                      crossAxisAlignment: CrossAxisAlignment.stretch,
                                      children: [
                                        Align(
                                          alignment: Alignment.centerLeft,
                                          child: Container(
                                            width: 3,
                                            height: 40,
                                            decoration: BoxDecoration(
                                              color: Color.fromARGB(255, 255, 87, 87),
                                              borderRadius: BorderRadius.all(Radius.circular(1.5)),
                                            ),
                                            child: Container(),
                                          ),
                                        ),
                                        Expanded(
                                          flex: 1,
                                          child: Container(
                                            margin: EdgeInsets.only(left: 10, right: 2),
                                            child: Column(
                                              crossAxisAlignment: CrossAxisAlignment.stretch,
                                              children: [
                                                Text(
                                                  "ADMATs Section is live on myIMS!",
                                                  style: TextStyle(
                                                    color: Color.fromARGB(255, 0, 3, 44),
                                                    fontSize: 14,
                                                    fontFamily: "IBM Plex Sans",
                                                  ),
                                                  textAlign: TextAlign.left,
                                                ),
                                                Spacer(),
                                                Align(
                                                  alignment: Alignment.topLeft,
                                                  child: Text(
                                                    "2 Minutes ago",
                                                    style: TextStyle(
                                                      color: Color.fromARGB(255, 153, 154, 171),
                                                      fontSize: 12,
                                                      fontFamily: "IBMPlexSans",
                                                      fontWeight: FontWeight.w500,
                                                    ),
                                                    textAlign: TextAlign.left,
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                                Spacer(),
                                Container(
                                  height: 1,
                                  margin: EdgeInsets.only(bottom: 1),
                                  decoration: BoxDecoration(
                                    color: Color.fromARGB(255, 242, 244, 244),
                                  ),
                                  child: Container(),
                                ),
                              ],
                            ),
                          ),
                          Spacer(),
                          Container(
                            height: 74,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.stretch,
                              children: [
                                Align(
                                  alignment: Alignment.topLeft,
                                  child: Container(
                                    width: 228,
                                    height: 62,
                                    child: Row(
                                      children: [
                                        Container(
                                          width: 3,
                                          height: 60,
                                          decoration: BoxDecoration(
                                            color: Color.fromARGB(255, 255, 87, 87),
                                            borderRadius: BorderRadius.all(Radius.circular(1.5)),
                                          ),
                                          child: Container(),
                                        ),
                                        Expanded(
                                          flex: 1,
                                          child: Container(
                                            height: 62,
                                            margin: EdgeInsets.only(left: 10),
                                            child: Column(
                                              crossAxisAlignment: CrossAxisAlignment.stretch,
                                              children: [
                                                Container(
                                                  margin: EdgeInsets.only(right: 3),
                                                  child: Text(
                                                    "SimCAT 102 Test available today. \nTake the test now!",
                                                    style: TextStyle(
                                                      color: Color.fromARGB(255, 0, 3, 44),
                                                      fontSize: 14,
                                                      fontFamily: "IBM Plex Sans",
                                                    ),
                                                    textAlign: TextAlign.left,
                                                  ),
                                                ),
                                                Spacer(),
                                                Align(
                                                  alignment: Alignment.topLeft,
                                                  child: Text(
                                                    "2 Minutes ago",
                                                    style: TextStyle(
                                                      color: Color.fromARGB(255, 153, 154, 171),
                                                      fontSize: 12,
                                                      fontFamily: "IBMPlexSans",
                                                      fontWeight: FontWeight.w500,
                                                    ),
                                                    textAlign: TextAlign.left,
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                                Spacer(),
                                Container(
                                  height: 1,
                                  margin: EdgeInsets.only(bottom: 1),
                                  decoration: BoxDecoration(
                                    color: Color.fromARGB(255, 242, 244, 244),
                                  ),
                                  child: Container(),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      height: 309,
                      margin: EdgeInsets.only(top: 15),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: [
                          Container(
                            height: 58,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.stretch,
                              children: [
                                Align(
                                  alignment: Alignment.topLeft,
                                  child: Container(
                                    width: 272,
                                    height: 42,
                                    child: Row(
                                      crossAxisAlignment: CrossAxisAlignment.stretch,
                                      children: [
                                        Align(
                                          alignment: Alignment.centerLeft,
                                          child: Container(
                                            width: 3,
                                            height: 40,
                                            decoration: BoxDecoration(
                                              color: Color.fromARGB(255, 242, 246, 248),
                                              borderRadius: BorderRadius.all(Radius.circular(1.5)),
                                            ),
                                            child: Container(),
                                          ),
                                        ),
                                        Expanded(
                                          flex: 1,
                                          child: Container(
                                            margin: EdgeInsets.only(left: 10, right: 1),
                                            child: Column(
                                              crossAxisAlignment: CrossAxisAlignment.stretch,
                                              children: [
                                                Text(
                                                  "IMS CAT 2018- Slot 2 Percentile Mapper!",
                                                  style: TextStyle(
                                                    color: Color.fromARGB(255, 0, 3, 44),
                                                    fontSize: 14,
                                                    fontFamily: "IBM Plex Sans",
                                                  ),
                                                  textAlign: TextAlign.left,
                                                ),
                                                Spacer(),
                                                Align(
                                                  alignment: Alignment.topLeft,
                                                  child: Text(
                                                    "30 Minute ago",
                                                    style: TextStyle(
                                                      color: Color.fromARGB(255, 153, 154, 171),
                                                      fontSize: 12,
                                                      fontFamily: "IBMPlexSans",
                                                      fontWeight: FontWeight.w500,
                                                    ),
                                                    textAlign: TextAlign.left,
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                                Spacer(),
                                Container(
                                  height: 1,
                                  margin: EdgeInsets.only(bottom: 1),
                                  decoration: BoxDecoration(
                                    color: Color.fromARGB(255, 242, 244, 244),
                                  ),
                                  child: Container(),
                                ),
                              ],
                            ),
                          ),
                          Container(
                            height: 57,
                            margin: EdgeInsets.only(top: 16),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.stretch,
                              children: [
                                Align(
                                  alignment: Alignment.topLeft,
                                  child: Container(
                                    width: 201,
                                    height: 51,
                                    child: Row(
                                      crossAxisAlignment: CrossAxisAlignment.stretch,
                                      children: [
                                        Align(
                                          alignment: Alignment.topLeft,
                                          child: Container(
                                            width: 3,
                                            height: 40,
                                            decoration: BoxDecoration(
                                              color: Color.fromARGB(255, 242, 246, 248),
                                              borderRadius: BorderRadius.all(Radius.circular(1.5)),
                                            ),
                                            child: Container(),
                                          ),
                                        ),
                                        Expanded(
                                          flex: 1,
                                          child: Container(
                                            margin: EdgeInsets.only(left: 10, bottom: 10),
                                            child: Column(
                                              crossAxisAlignment: CrossAxisAlignment.stretch,
                                              children: [
                                                Text(
                                                  "GDPI Section goes live today! ",
                                                  style: TextStyle(
                                                    color: Color.fromARGB(255, 0, 3, 44),
                                                    fontSize: 14,
                                                    fontFamily: "IBM Plex Sans",
                                                  ),
                                                  textAlign: TextAlign.left,
                                                ),
                                                Spacer(),
                                                Align(
                                                  alignment: Alignment.topLeft,
                                                  child: Text(
                                                    "1 day ago",
                                                    style: TextStyle(
                                                      color: Color.fromARGB(255, 153, 154, 171),
                                                      fontSize: 12,
                                                      fontFamily: "IBMPlexSans",
                                                      fontWeight: FontWeight.w500,
                                                    ),
                                                    textAlign: TextAlign.left,
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                                Spacer(),
                                Container(
                                  height: 1,
                                  margin: EdgeInsets.only(bottom: 1),
                                  decoration: BoxDecoration(
                                    color: Color.fromARGB(255, 242, 244, 244),
                                  ),
                                  child: Container(),
                                ),
                              ],
                            ),
                          ),
                          Container(
                            height: 74,
                            margin: EdgeInsets.only(top: 15),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.stretch,
                              children: [
                                Align(
                                  alignment: Alignment.topLeft,
                                  child: Container(
                                    width: 255,
                                    height: 62,
                                    child: Row(
                                      children: [
                                        Container(
                                          width: 3,
                                          height: 60,
                                          decoration: BoxDecoration(
                                            color: Color.fromARGB(255, 242, 246, 248),
                                            borderRadius: BorderRadius.all(Radius.circular(1.5)),
                                          ),
                                          child: Container(),
                                        ),
                                        Expanded(
                                          flex: 1,
                                          child: Container(
                                            height: 62,
                                            margin: EdgeInsets.only(left: 10),
                                            child: Column(
                                              crossAxisAlignment: CrossAxisAlignment.stretch,
                                              children: [
                                                Container(
                                                  margin: EdgeInsets.only(right: 1),
                                                  child: Text(
                                                    "Announcement of simCAT 103 results \nwill be released at 5 PM today.",
                                                    style: TextStyle(
                                                      color: Color.fromARGB(255, 0, 3, 44),
                                                      fontSize: 14,
                                                      fontFamily: "IBM Plex Sans",
                                                    ),
                                                    textAlign: TextAlign.left,
                                                  ),
                                                ),
                                                Spacer(),
                                                Align(
                                                  alignment: Alignment.topLeft,
                                                  child: Text(
                                                    "2 days ago",
                                                    style: TextStyle(
                                                      color: Color.fromARGB(255, 153, 154, 171),
                                                      fontSize: 12,
                                                      fontFamily: "IBMPlexSans",
                                                      fontWeight: FontWeight.w500,
                                                    ),
                                                    textAlign: TextAlign.left,
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                                Spacer(),
                                Container(
                                  height: 1,
                                  margin: EdgeInsets.only(bottom: 1),
                                  decoration: BoxDecoration(
                                    color: Color.fromARGB(255, 242, 244, 244),
                                  ),
                                  child: Container(),
                                ),
                              ],
                            ),
                          ),
                          Spacer(),
                          Container(
                            height: 73,
                            child: Stack(
                              alignment: Alignment.centerLeft,
                              children: [
                                Positioned(
                                  left: 0,
                                  child: Container(
                                    width: 213,
                                    height: 71,
                                    child: Row(
                                      crossAxisAlignment: CrossAxisAlignment.stretch,
                                      children: [
                                        Align(
                                          alignment: Alignment.topLeft,
                                          child: Container(
                                            width: 3,
                                            height: 60,
                                            decoration: BoxDecoration(
                                              color: Color.fromARGB(255, 242, 246, 248),
                                              borderRadius: BorderRadius.all(Radius.circular(1.5)),
                                            ),
                                            child: Container(),
                                          ),
                                        ),
                                        Expanded(
                                          flex: 1,
                                          child: Container(
                                            margin: EdgeInsets.only(left: 10, right: 1, bottom: 10),
                                            child: Column(
                                              crossAxisAlignment: CrossAxisAlignment.stretch,
                                              children: [
                                                Text(
                                                  "IIM-B course for batch of 2019.\nOpen till 26th September!",
                                                  style: TextStyle(
                                                    color: Color.fromARGB(255, 0, 3, 44),
                                                    fontSize: 14,
                                                    fontFamily: "IBM Plex Sans",
                                                  ),
                                                  textAlign: TextAlign.left,
                                                ),
                                                Spacer(),
                                                Align(
                                                  alignment: Alignment.topLeft,
                                                  child: Text(
                                                    "2 months ago",
                                                    style: TextStyle(
                                                      color: Color.fromARGB(255, 153, 154, 171),
                                                      fontSize: 12,
                                                      fontFamily: "IBMPlexSans",
                                                      fontWeight: FontWeight.w500,
                                                    ),
                                                    textAlign: TextAlign.left,
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                                Positioned(
                                  left: 0,
                                  right: 0,
                                  bottom: 1,
                                  child: Container(
                                    height: 1,
                                    decoration: BoxDecoration(
                                      color: Color.fromARGB(255, 242, 244, 244),
                                    ),
                                    child: Container(),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
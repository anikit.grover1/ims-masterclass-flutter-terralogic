import 'package:flutter/material.dart';
import 'package:imsindia/components/bottom_navigation.dart';

class BaseWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: BottomNavigation(),
    );
  }
}

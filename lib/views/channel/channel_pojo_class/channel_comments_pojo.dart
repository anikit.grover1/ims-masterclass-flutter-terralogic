import 'package:flutter/material.dart';

class ChannelCommentsOptions{
  String nameofperson;
  String durationofcomment;
  String comment_text;
  int numberofvotes;
  String replyofcomment;
  String replies;
  var imageURL;
  ChannelCommentsOptions({
    this.nameofperson,
    this.durationofcomment,
    this.comment_text,
    this.numberofvotes,
    this.replyofcomment,
    this.replies,
    this.imageURL
  });

  ChannelCommentsOptions.Comment(this.nameofperson,this.durationofcomment,this.comment_text,this.numberofvotes);
  ChannelCommentsOptions.replies(this.replies);
}

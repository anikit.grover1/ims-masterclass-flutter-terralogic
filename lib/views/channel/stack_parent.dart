import 'package:flutter/material.dart';
import 'package:imsindia/views/channel/stack_child.dart';

import 'channel_filter.dart';
import 'channel_video_session.dart';
class ParentWidget extends StatefulWidget {

  @override
  _ParentWidgetState createState() => _ParentWidgetState();
}

class _ParentWidgetState extends State<ParentWidget> with TickerProviderStateMixin{
  List<Tab> tabList = List();
  int _index;
  int _currentIndex;

  TabController _tabController;
  _handleTabSelection() {
    setState(() {
      _currentIndex = _tabController.index;
      print("index"+_currentIndex.toString());
    });
    // entry.overlayEntry.remove();

  }
  @override
  void initState() {
    super.initState();
    _tabController = new TabController(vsync: this, length: tabList.length);
    _index = 0;
    tabList.add(new Tab(
      child: Text(
        "Tab1",
      ),
    ));
    tabList.add(new Tab(
      child: Text(
        "Tab2",
      ),
    ));
    tabList.add(new Tab(
      child: Text(
        "Tab3",
      ),
    ));
    setState(() {
      _tabController = new TabController(vsync: this, length: tabList.length);
      _tabController.addListener(_handleTabSelection);

      this._index = _index;


      print(_index);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      
      appBar: AppBar(
        backgroundColor: Colors.white,
        elevation: 0.0,
        titleSpacing: 0.0,
        brightness: Brightness.light,
        centerTitle: false,
        iconTheme: IconThemeData(
          color: Colors.black,
        ),
          leading: IconButton(
            icon: Icon(
              Icons.arrow_back,
              color: Colors.black,
              size: 30.0,
            ),
            onPressed: () => Navigator.pop(context),
          ),
        title: Text(
          "Demo",
          style: TextStyle(
            fontSize: 16,
            fontFamily: "IBMPlexSans",
            fontWeight: FontWeight.w500,
            color: Colors.black,
          ),
          textAlign: TextAlign.left,
        ),
      ),
      body: Container(
        color: Colors.white,
        child: Column(
          children: <Widget>[
          Container(
            margin: EdgeInsets.symmetric(horizontal: 20),
          height: 150.0,
          decoration: BoxDecoration(
            image: DecorationImage(
                fit: BoxFit.cover,
                image: NetworkImage(
                  'https://img.etimg.com/thumb/msid-66398917,width-643,imgsize-702055,resizemode-4/bill-gates-wants-to-reinvent-the-toilet-save-233-billion-while-at-it.jpg'
                )
            ),
            borderRadius: BorderRadius.all(Radius.circular(8.0)),
            color: Colors.redAccent,
          ),
          ),
            Container(
              margin: EdgeInsets.symmetric(horizontal: 20,vertical: 10),
              child: Text(
                "Description: William Henry Gates III (born October 28, 1955) is an American business magnate, software developer, investor, and philanthropist. He is best known as the co-founder of Microsoft Corporation.",
                style: TextStyle(
                  fontSize: 12,
                  color: Colors.black54,
                  fontWeight: FontWeight.w400,
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.symmetric(horizontal: 20,vertical: 10),
              height: 150.0,
              decoration: BoxDecoration(
                image: DecorationImage(
                    fit: BoxFit.cover,
                    image: NetworkImage(
                        'https://image.businessinsider.com/5c62d63c2628982cad752452?width=1100&format=jpeg&auto=webp'
                    )
                ),
                borderRadius: BorderRadius.all(Radius.circular(8.0)),
                color: Colors.redAccent,
              ),
            ),
            Container(
              margin: EdgeInsets.symmetric(horizontal: 20,vertical: 10),
              child: Text(
                "Description: William Henry Gates III (born October 28, 1955) is an American business magnate, software developer, investor, and philanthropist. He is best known as the co-founder of Microsoft Corporation.",
                style: TextStyle(
                  fontSize: 12,
                  color: Colors.black54,
                  fontWeight: FontWeight.w400,
                ),
              ),
            ),

            Container(
              margin: EdgeInsets.only(left:20,
                  right: 20),
              height: (40),
              child: TabBar(
                isScrollable: true,
                controller: _tabController,
                indicatorColor: Colors.blueAccent,
                labelColor: Colors.blueAccent,
                unselectedLabelColor: Colors.black54,
                indicatorSize: TabBarIndicatorSize.tab,
                tabs: tabList,
                unselectedLabelStyle: TextStyle(
                  fontSize: 14,
                  fontFamily: "IBMPlexSans",
                  fontWeight: FontWeight.w400,
                ),
                labelStyle: TextStyle(
                  fontSize: 14,
                  fontFamily: "IBMPlexSans",
                  fontWeight: FontWeight.w500,
                ),
                indicator:  UnderlineTabIndicator(
                  borderSide: BorderSide(width: 2.0,color: Colors.blueAccent ),
                  insets: EdgeInsets.symmetric(horizontal:0.0),

                ),
              ),
            ),
            Expanded(
              child: TabBarView(
                controller: _tabController,
                children: [
                  ChildWidget(),
                  ChildWidget(),
                  ChildWidget(),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:imsindia/utils/colors.dart';
import 'package:imsindia/utils/screen_util.dart';
import 'package:smooth_star_rating/smooth_star_rating.dart';

bool value = false;
GlobalKey _keyRed = GlobalKey();
GlobalKey _keyRed1 = GlobalKey();

List titles = [
  'Date',
  'Upvotes',
];

//List titles = [
//  'a',
//  'b',
//  'c',
//  'd',
//  'e',
//  'a',
//  'b',
//  'c',
//  'd',
//];

class ChannelDropDown extends StatefulWidget {
  @override
  CountriesFieldState createState() => CountriesFieldState();
}

class CountriesFieldState extends State<ChannelDropDown> {
  OverlayEntry _overlayEntry;
  int selectedValue ;
  String selectedString ="Sort by";
  double ratingValue=0;


  OverlayEntry _createOverlayEntry() {
    RenderBox renderBox = context.findRenderObject();

    var size = _getSizes();

    var offset = _getPositions();

    return OverlayEntry(

        builder: (context) => Positioned(
          top: 226,
         // left: offset.dx - (10/Constant.defaultScreenWidth)*screenWidth,
          child: Center(
            child: Container(
              // padding: EdgeInsets.only(top: 5),
              width: (320/Constant.defaultScreenWidth)*screenWidth,
              height: (60/Constant.defaultScreenHeight)*screenHeight,
              child: Scaffold(
                body: Container(
                  color: Colors.cyan,
                  margin: EdgeInsets.only(top: 0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          new Text(
                            'Camera',
                            //  style: TextStyles.editTextStyle,
                          ),
//                          Container(
//                            child: RatingBar(
//                              onRatingChanged: (v) {
//                                ratingValue = v;
//                                print('v+++++++++++++'+v.toString());
//                                setState(() {
//                                  ratingValue = v;
//                                  print('rating-----------'+ratingValue.toString());
//
//                                });
//                              },
//                              filledIcon: Icons.star,
//                              emptyIcon: Icons.star_border,
//                              halfFilledIcon: Icons.star_half,
//                              isHalfAllowed: true,
//                              filledColor: Colors.green,
//                              emptyColor: Colors.redAccent,
//                              halfFilledColor: Colors.amberAccent,
//                              size: 48,
//                            ),
//                          )
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Container(
          padding: EdgeInsets.only(bottom: 0),
          child: Column(
            children: [
              Align(
                alignment: Alignment.topLeft,
                child: Container(
                  child: Container(
                    //  color: Colors.cyan,
                    margin: EdgeInsets.only(top: (100/defaultScreenHeight)*screenHeight),
                    width: (100/Constant.defaultScreenWidth)*screenWidth,
                    height: 35,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      //  crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: <Widget>[
                        Center(
                          child: InkWell(
                            onTap: () {
                              this._overlayEntry = this._createOverlayEntry();
                              Overlay.of(context).insert(this._overlayEntry);
                            },
                            child: Container(
                              // color: Colors.pink,
                              child: Padding(
                                padding: EdgeInsets.only(
                                    top: (12 / Constant.defaultScreenHeight) *
                                        screenHeight,
                                    bottom: (9 / Constant.defaultScreenHeight) *
                                        screenHeight,
                                    left: (12 / Constant.defaultScreenWidth) *
                                        screenWidth,
                                    right: (17 / Constant.defaultScreenWidth) *
                                        screenWidth),
                                child: Text(
                                  selectedString.toUpperCase(),
                                  key: _keyRed,
                                  style: TextStyle(
                                      fontSize: (10 / Constant.defaultScreenHeight) *
                                          screenHeight,
                                      fontFamily: "IBMPlexSans",
                                      fontWeight: FontWeight.w500,
                                      color: SemanticColors.blueGrey),
                                  overflow:(  selectedString.length>10) ?  TextOverflow.ellipsis :
                                  TextOverflow.visible ,
                                ),
                              ),
                            ),
                          ),
                        ),
                        Spacer(),
                        Container(
                          //   color: Colors.red,
                          //  margin: EdgeInsets.all(9.0),
                          child: Icon(
                            Icons.keyboard_arrow_down,
                            color: SemanticColors.blueGrey,
                            size: 20.0,
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

_getPositions() {
  final RenderBox renderBoxRed = _keyRed.currentContext.findRenderObject();
  final positionRed = renderBoxRed.localToGlobal(Offset.zero);
  print("POSITION of Red: $positionRed ");
  return positionRed;
}

_getSizes() {
  final RenderBox renderBoxRed = _keyRed.currentContext.findRenderObject();
  final sizeRed = renderBoxRed.size;

  print("SIZE of Red: $sizeRed");
  return sizeRed.height;
}

_getPositions1() {
  final RenderBox renderBoxRed = _keyRed1.currentContext.findRenderObject();
  final positionRed = renderBoxRed.localToGlobal(Offset.zero);
  print("POSITION of Red: $positionRed ");
  return positionRed;
}

_getSizes1() {
  final RenderBox renderBoxRed = _keyRed1.currentContext.findRenderObject();
  final sizeRed = renderBoxRed.size;

  print("SIZE of Red: $sizeRed");
  return sizeRed.height;
}

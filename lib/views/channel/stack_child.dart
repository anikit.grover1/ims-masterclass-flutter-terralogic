import 'package:flutter/material.dart';
class ChildWidget extends StatefulWidget {
  @override
  _ChildWidgetState createState() => _ChildWidgetState();
}

class _ChildWidgetState extends State<ChildWidget> {


  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Container(
          margin: EdgeInsets.only(left: 20, top: 20),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              /**Circular Avatar**/
              Container(
                height: 30,
                width: 30,
                margin: EdgeInsets.only(left: 0, top: 0),

                decoration: new BoxDecoration(
                    shape: BoxShape.circle,
                    image: new DecorationImage(
                        fit: BoxFit.cover,
                        image: NetworkImage(
                            'https://img.etimg.com/thumb/msid-66398917,width-643,imgsize-702055,resizemode-4/bill-gates-wants-to-reinvent-the-toilet-save-233-billion-while-at-it.jpg'))),
              ),
              Expanded(
                child: Container(
                  height: 40,
                  margin: EdgeInsets.only(left: 20,right: 20),
                  decoration: BoxDecoration(
                    border: Border.all(color: Colors.black),
                    color:Colors.white,
                    borderRadius: BorderRadius.all(Radius.circular(5)),
                  ),
                  child: Row(
                    children: <Widget>[
                      Container(
                        height: 18,
                        width: 200,
                        margin: EdgeInsets.only(left: 13,top: 10,bottom: 10),
                        child: GestureDetector(
                          child: TextFormField(
                            style: TextStyle(
                              color: Colors.black38,
                              fontWeight: FontWeight.bold,
                              fontStyle: FontStyle.normal,
                              fontSize: 14,
                            ),
                            textInputAction: TextInputAction.done,
                            decoration: new InputDecoration.collapsed
                              (
                              hintText:  "Post Query",
                              hintStyle: TextStyle (
                                color: Colors.white30,
                                fontWeight: FontWeight.w400,
                                fontStyle: FontStyle.normal,
                                fontSize: 14,
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
        Expanded(
          child: ListView.separated(
            itemCount: 1000,
            itemBuilder: (context, index) {
              return    ListTile(
                leading: Icon(Icons.account_circle),
                title: Text('Sun'),
              );
            },
            separatorBuilder: (context, index) {
              return Divider();
            },
          ),
        ),
      ],
    );
  }
}

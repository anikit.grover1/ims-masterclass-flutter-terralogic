import 'package:flutter/material.dart';
import 'package:imsindia/components/custom_expansion_panel.dart';
import 'package:imsindia/routers/routes.dart';
import 'package:imsindia/utils/colors.dart';
import 'package:imsindia/utils/emaximiser_svg_icons.dart';
import 'package:imsindia/utils/screen_util.dart';
import 'package:imsindia/utils/svg_images/channel_svg_icons.dart';
import 'package:imsindia/utils/svg_images/gk_zone_svg_images.dart';
import 'package:imsindia/views/channel/channel_home.dart';
import 'package:imsindia/views/channel/channel_quickTips.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'channel_replies.dart';

double screenWidth;
double screenHeight;

class ChannelFilter extends StatefulWidget {
  @override
  _ChannelFilterState createState() => _ChannelFilterState();
}

class _ChannelFilterState extends State<ChannelFilter> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  static List<String> _selectCategorys = List();
  Map<String, bool> values = {
    'Geometry': true,
    'titles': true,
  };

  int _activeMeterIndex;
  int insideListActiveIndex;
  bool isSelected = false;
  bool filtersFlag = true;
  List titles = [
    'Subjects',
    'Exams',
    'Category',
  ];
  List insideList = [
    [
      'Algebra and Formulas',
      'Geometry',
      'Strategy',
      'activeMeterIndex',
      'titles',
    ],
    [
      'surface',
      'Setting',
      'Dataspace',
      'Connection',
      'established',
    ],
    [
      'Priority',
      'HostConnection',
      'Disconnecting',
      'Metadata',
      'Performing',
    ],
  ];

  void _onCategorySelected(bool selected, category_id) {
    print('*******selected'+selected.toString());
    if (selected == true) {
      setState(() {
        _selectCategorys.add(category_id);
        print(_selectCategorys);
      });
    } else {
      setState(() {
        _selectCategorys.remove(category_id);
      });
    }
  }

  void sharedMethod() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setStringList('selectedFilters', _selectCategorys);
    await prefs.setBool('fromFilters', true);
  }


  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getValuesSF();
  }

  getValuesSF() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    List list = prefs.getStringList('selectedFilters');
    bool boolVal = prefs.getBool('fromFilters');
    setState(() {
      if (list != null && boolVal != null) {
        _selectCategorys = list;
        filtersFlag = boolVal;
      }
    });
    // print(result);
  }

  @override
  Widget build(BuildContext context) {
    var _mediaQueryData = MediaQuery.of(context);
    screenWidth = _mediaQueryData.size.width;
    screenHeight = _mediaQueryData.size.height;
    void _modalBottomSheetMenu() {
      showModalBottomSheet(
        backgroundColor:  Colors.white,

          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.only(
                topRight: Radius.circular(20), topLeft: Radius.circular(20),),
          ),
          context: context,
          builder: (builder) {
            return new Container(
              height: 120.0,
              //could change this to Color(0xFF737373),
              child: new Center(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    GestureDetector(
                      onTap: () {
                        Navigator.of(context).pop();
                        setState(() {
                          isSelected = false;
                          _selectCategorys.clear();
                        });
                        sharedMethod();
                       // AppRoutes.push(context, Channel_quicktips());
                      },
                      child: Container(
                        height:
                            (40 / Constant.defaultScreenHeight) * screenHeight,
                        width:
                            (130 / Constant.defaultScreenWidth) * screenWidth,
                        decoration: BoxDecoration(
                          color: NeutralColors.ice_blue,
                          borderRadius: BorderRadius.all(Radius.circular(2)),
                        ),
                        child: Center(
                          child: Text(
                            "Reset",
                            style: TextStyle(
                              fontSize: (14 / 360) * screenWidth,
                              fontWeight: FontWeight.normal,
                              fontFamily: "IBMPlexSans",
                              color: NeutralColors.blue_grey,
                            ),
                          ),
                        ),
                      ),
                    ),
                    GestureDetector(
                      onTap: () {
                        sharedMethod();
                        var count = 0;
                        Navigator.of(context).popUntil((_) => count ++ >= 2);
                      },
                      child: Container(
                        margin: EdgeInsets.only(
                            left: (15 / Constant.defaultScreenWidth) *
                                screenWidth),
                        height:
                            (40 / Constant.defaultScreenHeight) * screenHeight,
                        width:
                            (130 / Constant.defaultScreenWidth) * screenWidth,
                        decoration: BoxDecoration(
                          color: PrimaryColors.azure_Dark,
                          borderRadius: BorderRadius.all(Radius.circular(2)),
                        ),
                        child: Center(
                          child: Text(
                            "Done",
                            style: TextStyle(
                              fontSize: (14 / 360) * screenWidth,
                              fontWeight: FontWeight.normal,
                              fontFamily: "IBMPlexSans",
                              color: Colors.white,
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            );
          });
    }

    return Scaffold(
        key: _scaffoldKey,
        appBar: AppBar(
            brightness: Brightness.light,
            elevation: 0.0,
            actions: <Widget>[
              InkWell(
                child: Padding(
                  padding: EdgeInsets.only(
                      right: (21 / Constant.defaultScreenWidth) * screenWidth),
                  child: ChannelAssets.filterSelectedSvgIcon,
                ),
              )
            ],
            automaticallyImplyLeading: true,
            backgroundColor: Colors.white,
            iconTheme: IconThemeData(
              color: Colors.black,
            ),
            leading: IconButton(
              icon: getSvgIcon.backSvgIcon,
              onPressed: () => Navigator.pop(context),
            )),
        body: Container(
          color: Colors.white,
          child: Column(
            //   mainAxisAlignment: MainAxisAlignment.center,
            mainAxisSize: MainAxisSize.max,
            children: <Widget>[
              new Expanded(
                // flex: 1,
                child: new ListView.builder(
                  shrinkWrap: true,
                  itemBuilder: (BuildContext context, int i) {
                    return Column(
                      children: <Widget>[
                        Container(
                          // color: Colors.red,
                          margin: EdgeInsets.only(
                            top: (10.0 / 720) * screenHeight,
                            left: (20.0 / 360) * screenWidth,
                            right: (20.0 / 360) * screenWidth,
                          ),

                          child: CustomExpansionPanelList(
                            expansionHeaderHeight: (45 / 720) * screenHeight,
                            iconColor: (_activeMeterIndex == i)
                                ? Colors.white
                                : Colors.black,
                            backgroundColor1: (_activeMeterIndex == i)
                                ? NeutralColors.purpleish_blue
                                : SemanticColors.light_purpely.withOpacity(0.1),
                            backgroundColor2: (_activeMeterIndex == i)
                                ? NeutralColors.purpleish_blue
                                : SemanticColors.dark_purpely.withOpacity(0.1),
                            expansionCallback: (int index, bool status) {
                              setState(() {
                                _activeMeterIndex =
                                    _activeMeterIndex == i ? null : i;
                              });
                            },
                            children: [
                              new ExpansionPanel(
                                canTapOnHeader: true,
                                isExpanded: _activeMeterIndex == i,
                                headerBuilder:
                                    (BuildContext context, bool isExpanded) =>
                                        new Container(
                                  alignment: Alignment.centerLeft,
                                  child: new Padding(
                                    padding: EdgeInsets.only(
                                        left: (15 / 320) * screenWidth),
                                    child: new Text(titles[i],
                                        style: new TextStyle(
                                            fontSize: (14 / 360) * screenWidth,
                                            fontFamily: "IBMPlexSans",
                                            fontWeight: FontWeight.w500,
                                            color: (_activeMeterIndex == i)
                                                ? NeutralColors.pureWhite
                                                : NeutralColors
                                                    .dark_navy_blue)),
                                  ),
                                ),
                                body: new Container(
                                  //color: Colors.yellowAccent,
                                  // height: (200 / Constant.defaultScreenHeight) * screenHeight,
//                                  decoration: new BoxDecoration(
//                                    boxShadow: [
//                                      new BoxShadow(
//                                        color: Colors.white,
//                                        blurRadius: 10.0,
//                                        spreadRadius: 0.0,
//                                        offset: new Offset(0.0, 10.0),
//                                      ),
//                                    ],
//                                  ),
                                  //  height: (210 / 678) * screenHeight,
                                  child: new ListView.builder(
                                    shrinkWrap: true,
                                    padding: EdgeInsets.only(
                                        bottom: (20 /
                                                Constant.defaultScreenHeight) *
                                            screenHeight),
                                    itemExtent: (45.0 / 720) * screenHeight,
                                    itemBuilder:
                                        (BuildContext context, int index) {
                                      return Container(
                                        child: CheckboxListTile(
                                          // checkColor: Colors.white,
                                          controlAffinity:
                                              ListTileControlAffinity.leading,
                                          activeColor: NeutralColors.kelly_green,
                                          onChanged: (bool selected) {
                                            setState(() {
                                              isSelected = false;
                                            });
                                            print((insideList[i]));
                                            _onCategorySelected(
                                                selected, insideList[i][index]);
                                          },
                                          title: new Text(
                                            insideList[i][index],
                                            textAlign: TextAlign.left,
                                            style: new TextStyle(
                                                fontSize:
                                                    (13 / 360) * screenWidth,
                                                fontFamily: "IBMPlexSans",
                                                fontWeight: FontWeight.w500,
                                                color: NeutralColors.blue_grey),
                                          ),
                                          value: _selectCategorys
                                              .contains(insideList[i][index]),
                                        ),
                                      );
                                    },
                                    itemCount: insideList[i].length,
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                        titles.indexOf(titles[i]) == (titles.length - 1)
                            ? InkWell(
                                onTap: () {
                                  //  _modalBottomSheetMenu();
                                  //  handleRadioValueChanged();
                                },
                                child: Container(
                                  margin: EdgeInsets.only(
                                      top: (10 / Constant.defaultScreenHeight) *
                                          screenHeight,
                                      left: (20 / Constant.defaultScreenWidth) *
                                          screenWidth,
                                      right:
                                          (20 / Constant.defaultScreenWidth) *
                                              screenWidth,
                                      bottom:
                                          (0 / Constant.defaultScreenHeight) *
                                              screenHeight),
                                  height: (45 / Constant.defaultScreenHeight) *
                                      screenHeight,
                                  decoration: BoxDecoration(
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(5)),
                                    gradient: LinearGradient(
                                      begin: Alignment.topRight,
                                      end: Alignment.bottomLeft,
                                      colors: [
                                        SemanticColors.dark_purpely
                                            .withOpacity(0.1),
                                        SemanticColors.light_purpely
                                            .withOpacity(0.1)
                                      ],
                                    ),
                                  ),
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    children: [
                                      Container(
                                        alignment: Alignment.centerLeft,
                                        child: new Padding(
                                          padding: EdgeInsets.only(
                                              left: (15 / 320) * screenWidth),
                                          child: new Text("Bookmark",
                                              style: new TextStyle(
                                                  fontSize:
                                                      (14 / 360) * screenWidth,
                                                  fontFamily: "IBMPlexSans",
                                                  fontWeight: FontWeight.w500,
                                                  color: NeutralColors
                                                      .dark_navy_blue)),
                                        ),
                                      ),
                                      GestureDetector(
                                        onTap: () {
                                          print(isSelected.toString());
                                          setState(() {
                                            isSelected = true;
                                          });
                                          if (_selectCategorys.length > 0 &&
                                              isSelected) {
                                            _modalBottomSheetMenu();
                                          }else{
                                            Scaffold.of(context).showSnackBar(SnackBar(
                                              content: Text('Select atleast one category'),
                                              duration: Duration(seconds: 3),
                                            ));
                                          }
                                        },
                                        child: Container(
                                            margin: EdgeInsets.only(
                                                right: (15 /
                                                        Constant
                                                            .defaultScreenWidth) *
                                                    screenWidth),
                                            child: _selectCategorys.length >
                                                        0 &&
                                                    isSelected
                                                ? ChannelAssets
                                                    .activeRadioButtonIcon
                                                : ChannelAssets
                                                    .inactiveRadioButtonIcon),
                                      ),
                                    ],
                                  ),
                                ),
                              )
                            : Container(),
                      ],
                    );
                  },
                  itemCount: titles.length,
                ),
              ),
            ],
          ),
        ));
  }
}

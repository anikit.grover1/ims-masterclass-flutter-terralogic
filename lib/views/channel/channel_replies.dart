import 'dart:ui' as prefix0;

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_document_picker/flutter_document_picker.dart';
import 'package:flutter_svg/svg.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:imsindia/resources/strings/prepare.dart';
import 'package:imsindia/routers/routes.dart';
import 'package:imsindia/utils/colors.dart';
import 'package:imsindia/utils/emaximiser_svg_icons.dart';
import 'package:imsindia/utils/screen_util.dart';
import 'package:imsindia/utils/svg_images/channel_svg_icons.dart';
import 'package:imsindia/views/Arun/ReadMoreText.dart';
//import '//package:keyboard_visibility/keyboard_visibility.dart';

List<dynamic> channelCommentsLikes = [];
List <String> replies = <String>[
  'How do you create a direct mail advertising campaign that gets results?  The following tips on creating a direct mail advertising campaign have been street-tested and will bring you huge returns in a short period of time.',
  'How do you create a direct mail advertising campaign that gets results?  The following tips on creating a direct mail advertising campaign have been street-tested and will bring you huge returns in a short period of time.',
];
List<int> likes = [
  324,324,
];
List<String> timeAgoList = [
  "2 Mins Ago","2 Mins Ago",
];
class ChannelReplies extends StatefulWidget {
  final String text;
  final int index;
  ChannelReplies({Key key, @required this.text,@required this.index}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _ChannelRepliesState();

}

class _ChannelRepliesState extends State<ChannelReplies> {
  bool clicked = false;
  bool isSelected = false;
  FocusNode _focusNode = new FocusNode();
  final TextEditingController _reply = new TextEditingController();
  int activeIndex = 0;
  String _screenName;
  bool isFirsttime = true;
  String _path = '-';
  bool uploaded = false;
  bool _pickFileInProgress = false;
  bool _iosPublicDataUTI = true;
  bool _checkByCustomExtension = false;
  bool _checkByMimeType = false;
  String timeAgo;
   String timeAgoSinceDate(String dateString, {bool numericDates = true}) {
     print("********************************************Coming" +dateString.toString());

     DateTime date = DateTime.parse(dateString);

    final date2 = DateTime.now();

     final difference = date2.difference(date);

    if ((difference.inDays / 365).floor() >= 2) {
      return '${(difference.inDays / 365).floor()} years ago';
    } else if ((difference.inDays / 365).floor() >= 1) {
      return (numericDates) ? '1 year ago' : 'Last year';
    } else if ((difference.inDays / 30).floor() >= 2) {
      print("********************************************monthsmonthsmonthsmonthsmonths" +dateString.toString());

      return '${(difference.inDays / 365).floor()} months ago';
    } else if ((difference.inDays / 30).floor() >= 1) {
      return (numericDates) ? '1 month ago' : 'Last month';
    } else if ((difference.inDays / 7).floor() >= 2) {
      return '${(difference.inDays / 7).floor()} weeks ago';
    } else if ((difference.inDays / 7).floor() >= 1) {
      return (numericDates) ? '1 week ago' : 'Last week';
    } else if (difference.inDays >= 2) {
      return '${difference.inDays} days ago';
    } else if (difference.inDays >= 1) {
      return (numericDates) ? '1 day ago' : 'Yesterday';
    } else if (difference.inHours >= 2) {
      return '${difference.inHours} hours ago';
    } else if (difference.inHours >= 1) {
      return (numericDates) ? '1 hour ago' : 'An hour ago';
    } else if (difference.inMinutes >= 2) {
      return '${difference.inMinutes} minutes ago';
    } else if (difference.inMinutes >= 1) {
      setState(() {
        timeAgo ='Just now';
        print('timeAgotimeAgotimeAgotimeAgo'+timeAgo);

      });
      return (numericDates) ? '1 minute ago' : 'A minute ago';
    } else if (difference.inSeconds >= 3) {
      setState(() {
        timeAgo ='${difference.inSeconds} seconds ago';
        print('timeAgotimeAgotimeAgotimeAgo'+timeAgo);
      });
      return '${difference.inSeconds} seconds ago';
    } else {
      setState(() {
        timeAgo ='Just now';
        print('timeAgotimeAgotimeAgotimeAgo'+timeAgo);

      });
      return 'Just now';
    }
  }
  _pickDocument() async {
    String result;
    try {
      setState(() {
        _path = '-';
        _pickFileInProgress = true;
      });
      final _utiController = TextEditingController(
        text: 'com.sidlatau.example.mwfbak',
      );

      final _extensionController = TextEditingController(
        text: 'mwfbak',
      );

      final _mimeTypeController = TextEditingController(
        text: 'application/pdf image/png',
      );
      FlutterDocumentPickerParams params = FlutterDocumentPickerParams(
        allowedFileExtensions: _checkByCustomExtension
            ? _extensionController.text
            .split(' ')
            .where((x) => x.isNotEmpty)
            .toList()
            : null,
        allowedUtiTypes: _iosPublicDataUTI
            ? null
            : _utiController.text
            .split(' ')
            .where((x) => x.isNotEmpty)
            .toList(),
        allowedMimeTypes: _checkByMimeType
            ? _mimeTypeController.text
            .split(' ')
            .where((x) => x.isNotEmpty)
            .toList()
            : null,
      );

      result = await FlutterDocumentPicker.openDocument(params: params);
    } catch (e) {
      print(e);
      result = 'Error: $e';
    } finally {
      setState(() {
        _pickFileInProgress = false;
      });
    }

    setState(() {
      uploaded = true;
      _path = result;
      print(_path.length.toString());
    });
  }


  final GlobalKey _menuKey = new GlobalKey();
  GlobalKey<ScaffoldState> _key;
  @override
  void initState() {
    super.initState();
    setState(() {
        _screenName = widget.text;
         });

    _key = GlobalKey<ScaffoldState>();
    // KeyboardVisibilityNotification().addNewListener(
    //   onHide: () {
    //     print(_focusNode.hasFocus);
    //     _focusNode.unfocus();
    //   },
    // );
  }

  @override
  Widget build(BuildContext context) {
    final screenHeight = MediaQuery.of(context).size.height;
    final screenWidth = MediaQuery.of(context).size.width;


    if(isFirsttime){
    }
    Future<bool> _onWillPop() {

      Navigator.pop(context,ChannelReplies());
    }
    return GestureDetector(
      onTap: () {
        print(_focusNode.hasFocus);
        _focusNode.unfocus();
      },
      child: Scaffold(
        key: _key,
        appBar: AppBar(
          brightness: Brightness.light,
          automaticallyImplyLeading: true,
          backgroundColor: _focusNode.hasFocus
              ? Colors.black.withOpacity(0.25)
              : Colors.white,
          elevation: 0.0,
          centerTitle: false,
          titleSpacing: 0.0,
          iconTheme: IconThemeData(
            color: Colors.black,
          ),
          leading: IconButton(
            icon: getSvgIcon.backSvgIcon,
            onPressed: () =>  Navigator.pop(context,ChannelReplies()),
          ),
          title: Text(
            "Replies",
            style: TextStyle(
                color: Colors.black,
              fontWeight: FontWeight.w500,
              fontFamily: "IBMPlexSans",
              fontSize: (16/Constant.defaultScreenWidth)*screenWidth,
              fontStyle: FontStyle.normal,
            ),
          ),
        ),
        body: Container(
          color: _focusNode.hasFocus
              ? Colors.black.withOpacity(0.25)
              : Colors.transparent,
          margin: EdgeInsets.only(
            top: 0 / Constant.defaultScreenHeight * screenHeight,
          ),
          child: Column(
            children: <Widget>[
              Expanded(
                child: ListView.separated(
                  itemCount: replies.length,
                  itemBuilder: (context, index) {
                    return Container(
                      //color: Colors.blue,
                      child: !_focusNode.hasFocus ? Container(
                        margin: EdgeInsets.only(
                          left: (20 / 360) * screenWidth,
                            right: (0 / 360) * screenWidth,
                          bottom: (26/720)*screenHeight
                        ),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            /**profile image and name colum**/
                            Container(
                            //  color:Colors.red,
                              margin: EdgeInsets.only(
                                  top: (24.5 / 720) * screenHeight),
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: <Widget>[
                                  /**Profile pic**/
                                  Container(
                                    height:
                                    (40 / Constant.defaultScreenHeight) *
                                        screenHeight,
                                    width: (40 / Constant.defaultScreenWidth) *
                                        screenWidth,
                                    decoration: new BoxDecoration(
                                        shape: BoxShape.circle,
                                        image: new DecorationImage(
                                            fit: BoxFit.cover,
                                            image: AssetImage(
                                                'assets/images/oval-2.png'))),
                                  ),
                                  Expanded(
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                      children: <Widget>[
                                        Container(
                                          margin: EdgeInsets.only(
                                              left: (12 / 360) * screenWidth),
                                          child: Column(
                                            crossAxisAlignment: CrossAxisAlignment.start,
                                            children: <Widget>[
                                              Container(

                                                child: Text(
                                                  SubQueriesCommentsScreenStrings
                                                      .Text_UserName,
                                                  style: TextStyle(
                                                    color:
                                                    NeutralColors.dark_navy_blue,
                                                    fontWeight: FontWeight.w700,
                                                    fontFamily: "IBMPlexSans",
                                                    fontStyle: FontStyle.normal,
                                                    fontSize: (14 /
                                                        Constant
                                                            .defaultScreenWidth) *
                                                        screenWidth,
                                                  ),
                                                  textAlign: TextAlign.start,
                                                ),
                                              ),
                                              Container(
                                                margin: EdgeInsets.only(
                                                  top: (5 /
                                                      Constant
                                                          .defaultScreenHeight) *
                                                      screenHeight,
                                                ),
                                                child: Text(
                                                  timeAgoList[index],
                                                  style: TextStyle(
                                                    color: NeutralColors.blue_grey,
                                                    fontWeight: FontWeight.w500,
                                                    fontFamily: "IBMPlexSans",
                                                    fontStyle: FontStyle.normal,
                                                    fontSize: (12 /
                                                        Constant
                                                            .defaultScreenWidth) *
                                                        screenWidth,
                                                  ),
                                                  textAlign: TextAlign.start,
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                        Container(
                                        //  color: Colors.yellowAccent,
                                          height: (25 / 720) * screenHeight,
                                          margin: EdgeInsets.only(
                                              left: (0 / 360) * screenWidth),
                                          child: PopupMenuButton(
                                              icon: Icon(Icons.more_vert,color: NeutralColors.blue_grey,size:20),
                                              padding: EdgeInsets.all(0),
                                              //  key: _menuKey,
                                              itemBuilder: (_) =>
                                              <PopupMenuItem<String>>[
                                                new PopupMenuItem<String>(
                                                    child: const Text(
                                                      'Report',
                                                      style: TextStyle(
                                                        color: NeutralColors
                                                            .gunmetal,
                                                        fontFamily:
                                                        "IBMPlexSans",
                                                        fontWeight:
                                                        FontWeight
                                                            .normal,
                                                        fontSize: 14,
                                                      ),
                                                    ),
                                                    value: 'Report'),
                                              ],
                                              onSelected: (_) {}),
                                        ),
                                      ],
                                    ),
                                  ),

                                ],
                              ),
                            ),
                            /**Extended Paragraph colum**/

                            Container(
                              margin: EdgeInsets.only(
                                  top: (12 / Constant.defaultScreenHeight) *
                                      screenHeight,
                                  right: (20 / 360) * screenWidth),
                              child: SingleChildScrollView(
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Padding(
                                      padding: const EdgeInsets.all(1.0),
                                      child: ReadMoreText(
                                        replies[index],
                                        trimLines: 3,
                                        colorClickableText: PrimaryColors.azure_Dark,
                                        trimMode: TrimMode.Line,
                                        trimCollapsedText:
                                        SubQueriesCommentsScreenStrings
                                            .Text_ReadMore,
                                        trimExpandedText:
                                        SubQueriesCommentsScreenStrings
                                            .Text_ReadLess,
                                        style: TextStyle(
                                          color: NeutralColors.gunmetal,
                                          fontWeight: FontWeight.w500,
                                          fontFamily: "IBMPlexSans",
                                          fontStyle: FontStyle.normal,
                                          height: 1.5,
                                          fontSize: (12/360)*screenWidth,
                                        ),
                                      ),
                                    )
                                  ],
                                ),
                              ),
                            ),
                            /**upvotes and reply colum**/
                            Container(
                             // color: Colors.red,
                              margin: EdgeInsets.only(
                                  top: (12 /
                                      Constant.defaultScreenHeight) *
                                      screenHeight),
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: <Widget>[
                                  Container(
                                    child: InkWell(
                                      onTap: () {
                                        setState(() {
                                          if(channelCommentsLikes.contains(index)){
                                            channelCommentsLikes.remove(index);
                                          }
                                          else{
                                            channelCommentsLikes.add(index);
                                          }
                                          isSelected = !isSelected;
                                        });
                                      },
                                      child:  SvgPicture.asset(
                                        channelCommentsLikes.contains(index) ? ChannelAssets.likeactive
                                            :ChannelAssets.likeinactive,
                                        fit: BoxFit.fill,
                                      ),

                                    ),
                                  ),
                                  Container(
                                      margin: EdgeInsets.only(
                                          top: 0 /
                                              Constant.defaultScreenHeight *
                                              screenHeight,
                                          left: 5 / 360 * screenWidth),
                                      child: Container(
                                        margin: EdgeInsets.only(
                                            top: 0 /
                                                Constant.defaultScreenHeight *
                                                screenHeight),
                                        child: Text(
                                          // SubQueriesCommentsScreenStrings.Text_Liked,
                                          channelCommentsLikes.contains(index) ? '${likes[index]+1} Upvotes' : '${likes[index]} Upvotes',
                                          style: TextStyle(
                                            color: channelCommentsLikes.contains(index)
                                                ? NeutralColors.deep_sky_blue
                                                : NeutralColors.dark_navy_blue,
                                            fontWeight: FontWeight.bold,
                                            fontFamily: "IBMPlexSans",
                                            fontStyle: FontStyle.normal,
                                            fontSize: (11 /
                                                Constant
                                                    .defaultScreenWidth) *
                                                screenWidth,
                                          ),
                                          textAlign: TextAlign.left,
                                        ),
                                      )),
                                  Container(
                                    margin: EdgeInsets.only(
                                        top: (0 /
                                            Constant.defaultScreenHeight) *
                                            screenHeight,
                                        left:
                                        (20 / Constant.defaultScreenWidth) *
                                            screenWidth),
                                    child: SvgPicture.asset(
                                      ChannelAssets.comment,
                                      fit: BoxFit.fill,
                                      color: NeutralColors.black,
                                      //size: 11 / 360 * screenWidth,
                                    ),
                                  ),
                                  Container(
                                      margin: EdgeInsets.only(
                                          top: 0 /
                                              Constant.defaultScreenHeight *
                                              screenHeight,
                                          left: 5 / 360 * screenWidth),
                                      decoration: BoxDecoration(
                                        color: Colors.transparent,
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(2)),
                                      ),
                                      child: Container(
                                        margin: EdgeInsets.only(
                                            top: 0 /
                                                Constant.defaultScreenHeight *
                                                screenHeight),
                                        child: Text(
                                          SubQueriesCommentsScreenStrings
                                              .Text_Reply,
                                          style: TextStyle(
                                            color: NeutralColors.dark_navy_blue,
                                            fontWeight: FontWeight.bold,
                                            fontFamily: "IBMPlexSans",
                                            fontStyle: FontStyle.normal,
                                            fontSize: (11 /
                                                Constant
                                                    .defaultScreenWidth) *
                                                screenWidth,
                                          ),
                                          textAlign: TextAlign.left,
                                        ),
                                      )),
                                ],
                              ),
                            ),
                            /**showreplies colum**/
                            Container(
                                width: double.infinity,
                                margin: EdgeInsets.only(
                                  top: 5 /
                                      Constant.defaultScreenHeight *
                                      screenHeight,

                                ),
                                decoration: BoxDecoration(
                                   //color:Colors.pink,
                                  borderRadius:
                                  BorderRadius.all(Radius.circular(2)),
                                ),
                                child: Container(
                                  margin: EdgeInsets.only(
                                      top: 6 /
                                          Constant.defaultScreenHeight *
                                          screenHeight),
                                  child: Text(
                                    SubQueriesCommentsScreenStrings
                                        .Text_ShowReplies,
                                    style: TextStyle(
                                      color: NeutralColors.blue_grey,
                                      fontWeight: FontWeight.w500,
                                      fontFamily: "IBMPlexSans",
                                      fontStyle: FontStyle.normal,
                                      fontSize:
                                      (12 / Constant.defaultScreenWidth) *
                                          screenWidth,
                                    ),
                                    textAlign: TextAlign.left,
                                  ),
                                )),
                          ],
                        ),
                      ) : Container(
                        margin: EdgeInsets.only(
                          left: (20/Constant.defaultScreenWidth)*screenWidth,
                          right: (0/Constant.defaultScreenHeight)*screenWidth,
                        ),
                        child: Column(
                          children: <Widget>[
                            Container(

                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Row(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: <Widget>[
                                      Container(
                                        height:
                                        (40 / Constant.defaultScreenHeight) *
                                            screenHeight,
                                        width: (40 / Constant.defaultScreenWidth) *
                                            screenWidth,
                                        margin: EdgeInsets.only(
                                            top: (24.5 / 720) * screenHeight,
                                            right: (12/Constant.defaultScreenWidth)*screenWidth),
                                        decoration: new BoxDecoration(
                                            shape: BoxShape.circle,
                                            image: new DecorationImage(
                                                fit: BoxFit.cover,
                                                image: AssetImage(
                                                    'assets/images/oval-2.png'))),
                                      ),
                                      Container(
                                         //  color:Colors.blue,
                                        width:(234/Constant.defaultScreenWidth)*screenWidth,
                                        child: Column(
                                          crossAxisAlignment: CrossAxisAlignment.start,
                                          mainAxisAlignment: MainAxisAlignment.start,
                                          children: <Widget>[
                                            Container(
                                              //   color:Colors.purple,
                                              child: Row(
                                                crossAxisAlignment: CrossAxisAlignment.center,
                                                mainAxisAlignment: MainAxisAlignment.start,
                                                children: <Widget>[
                                                  Container(

                                                    // width: (85 / 360) * screenWidth,
                                                    margin: EdgeInsets.only(
                                                        top: (24.5 /
                                                            Constant
                                                                .defaultScreenHeight) *
                                                            screenHeight,
                                                        left: (0 / 360) * screenWidth),
                                                    child: Text(
                                                      SubQueriesCommentsScreenStrings
                                                          .Text_UserName,
                                                      style: TextStyle(
                                                        color:
                                                        NeutralColors.dark_navy_blue,
                                                        fontWeight: FontWeight.bold,
                                                        fontFamily: "IBMPlexSans",
                                                        fontStyle: FontStyle.normal,
                                                        fontSize: (14 /
                                                            Constant
                                                                .defaultScreenWidth) *
                                                            screenWidth,
                                                      ),
                                                      textAlign: TextAlign.start,
                                                    ),
                                                  ),
                                                  Center(
                                                    child: Container(
                                                      width: 3 / 360 * screenWidth,
                                                      height: 3 / 720 * screenHeight,
                                                      margin: EdgeInsets.only(
                                                          left: (10/Constant.defaultScreenWidth)*screenWidth,
                                                          top:(28.5/Constant.defaultScreenHeight)*screenHeight,
                                                          right: (0/Constant.defaultScreenWidth)*screenWidth
                                                      ),

                                                      decoration: BoxDecoration(
                                                        color: NeutralColors.blue_grey,
                                                        borderRadius:
                                                        BorderRadius.all(
                                                            Radius.circular(
                                                                1.5)),
                                                      ),
                                                      child: Container(),
                                                    ),
                                                  ),
                                                  Container(
                                                    height: (18 /
                                                        Constant
                                                            .defaultScreenHeight) *
                                                        screenHeight,
                                                    // width: (85 / 360) * screenWidth,
                                                    margin: EdgeInsets.only(
                                                        top: (26.5  /
                                                            Constant
                                                                .defaultScreenHeight) *
                                                            screenHeight,
                                                        left: (12 / 360) * screenWidth),
                                                    child: Text(
                                                      timeAgoList[index],
                                                      style: TextStyle(
                                                        color: NeutralColors.blue_grey,
                                                        fontWeight: FontWeight.bold,
                                                        fontFamily: "IBMPlexSans",
                                                        fontStyle: FontStyle.normal,
                                                        fontSize: (12 /
                                                            Constant
                                                                .defaultScreenWidth) *
                                                            screenWidth,
                                                      ),
                                                      textAlign: TextAlign.center,
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ),
                                            Container(
                                              //    color: Colors.pink,
                                              margin: EdgeInsets.only(
                                                top: (12 / Constant.defaultScreenHeight) *
                                                    screenHeight,

                                              ),
                                              child: Container(
                                                //   color:Colors.cyan,
                                                width:double.infinity,
                                                margin:EdgeInsets.only( left: (0/Constant.defaultScreenWidth)*screenWidth ,
                                                    right: (0 / 360) * screenWidth),
                                                child: Column(
                                                  crossAxisAlignment: CrossAxisAlignment.start,
                                                  children: <Widget>[
                                                    Padding(
                                                      padding: const EdgeInsets.all(1.0),
                                                      child: ReadMoreText(
                                                        SubQueriesCommentsScreenStrings
                                                            .ReadMoreText_Content,
                                                        trimLines: 3,
                                                        colorClickableText: Colors.lightBlueAccent,
                                                        trimMode: TrimMode.Line,
                                                        trimCollapsedText:
                                                        SubQueriesCommentsScreenStrings
                                                            .Text_ReadMore,
                                                        trimExpandedText:
                                                        SubQueriesCommentsScreenStrings
                                                            .Text_ReadLess,
                                                        style: TextStyle(
                                                          color: NeutralColors.gunmetal,
                                                          fontWeight: FontWeight.w400,
                                                          fontFamily: "IBMPlexSans",
                                                          fontStyle: FontStyle.normal,
                                                          height: 1.5,
                                                          fontSize: (12/360)*screenWidth,
                                                        ),
                                                      ),
                                                    )
                                                  ],
                                                ),
                                              ),
                                            ),
                                            Container(
                                             // color:Colors.purple,
                                              margin: EdgeInsets.only(
                                                  top: (17 /
                                                      Constant.defaultScreenHeight) *
                                                      screenHeight,
                                              ),
                                              child: Row(
                                                crossAxisAlignment: CrossAxisAlignment.center,
                                                children: <Widget>[
                                                  Container(
                                                    margin: EdgeInsets.only(
                                                        top: (0 /
                                                            Constant.defaultScreenHeight) *
                                                            screenHeight,
                                                        left: 2 / 360 * screenWidth),
                                                    child: InkWell(
                                                      onTap: () {
                                                        setState(() {
                                                          if(channelCommentsLikes.contains(index)){
                                                            channelCommentsLikes.remove(index);
                                                          }
                                                          else{
                                                            channelCommentsLikes.add(index);
                                                          }
                                                          isSelected = !isSelected;
                                                        });
                                                      },
                                                      child: SvgPicture.asset(
                                                        ChannelAssets.likeinactive,
                                                        fit: BoxFit.fill,
                                                        color: channelCommentsLikes.contains(index)
                                                            ? NeutralColors.deep_sky_blue
                                                            : NeutralColors.black,
                                                        //size: 11 / 360 * screenWidth,
                                                      ),
                                                    ),
                                                  ),
                                                  Container(
                                                      margin: EdgeInsets.only(
                                                          top: 0 /
                                                              Constant.defaultScreenHeight *
                                                              screenHeight,
                                                          left: 5 / 360 * screenWidth),
                                                      child: Container(
                                                        margin: EdgeInsets.only(
                                                            top: 0 /
                                                                Constant.defaultScreenHeight *
                                                                screenHeight),
                                                        child: Text(
                                                          // SubQueriesCommentsScreenStrings.Text_Liked,
                                                          channelCommentsLikes.contains(index) && activeIndex == likes[index]? '${likes[index]+1} Votes' : '${likes[index]} Votes',
                                                          style: TextStyle(
                                                            color: channelCommentsLikes.contains(index)
                                                                ? NeutralColors.deep_sky_blue
                                                                : NeutralColors.dark_navy_blue,
                                                            fontWeight: FontWeight.bold,
                                                            fontFamily: "IBMPlexSans",
                                                            fontStyle: FontStyle.normal,
                                                            fontSize: (11 /
                                                                Constant
                                                                    .defaultScreenWidth) *
                                                                screenWidth,
                                                          ),
                                                          textAlign: TextAlign.left,
                                                        ),
                                                      )),
                                                ],
                                              ),
                                            ),
                                            Container(
                                              //width: double.infinity,
                                                margin: EdgeInsets.only(
                                                  top: 5 /
                                                      Constant.defaultScreenHeight *
                                                      screenHeight,
                                                  left: 0 /
                                                      Constant.defaultScreenWidth *
                                                      screenWidth,
                                                ),
                                                child: Container(
                                                  margin: EdgeInsets.only(
                                                      top: 6 /
                                                          Constant.defaultScreenHeight *
                                                          screenHeight),
                                                  child: Text(
                                                    SubQueriesCommentsScreenStrings
                                                        .Text_ShowReplies,
                                                    style: TextStyle(
                                                      color: NeutralColors.blue_grey,
                                                      fontWeight: FontWeight.bold,
                                                      fontFamily: "IBMPlexSans",
                                                      fontStyle: FontStyle.normal,
                                                      fontSize:
                                                      (12 / Constant.defaultScreenWidth) *
                                                          screenWidth,
                                                    ),
                                                    textAlign: TextAlign.left,
                                                  ),
                                                )),
                                          ],
                                        ),
                                      )
                                    ],
                                  ),
                                  GestureDetector(
                                    onTap: () {
                                      // This is a hack because _PopupMenuButtonState is private.
                                      dynamic state = _menuKey.currentState;
                                      state.showButtonMenu();
                                    },
                                    child: Container(
                                     //   color: Colors.red,
                                      height: (25 / 720) * screenHeight,
                                      margin: EdgeInsets.only(
                                        top: (25 / 720) * screenHeight,
                                      ),
                                      child: Align(
                                        alignment: Alignment.topRight,
                                        child: PopupMenuButton(
                                            icon: Icon(Icons.more_vert,color: NeutralColors.blue_grey,size:20),
                                             padding: EdgeInsets.all(0),
                                           //  key: _menuKey,
                                            itemBuilder: (_) =>
                                            <PopupMenuItem<String>>[
                                              new PopupMenuItem<String>(
                                                  child: const Text(
                                                    'Report',
                                                    style: TextStyle(
                                                      color: NeutralColors
                                                          .gunmetal,
                                                      fontFamily:
                                                      "IBMPlexSans",
                                                      fontWeight:
                                                      FontWeight
                                                          .normal,
                                                      fontSize: 14,
                                                    ),
                                                  ),
                                                  value: 'Report'),
                                            ],
                                            onSelected: (_) {}),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            )
                          ],
                        ),
                      ),
                    );
                  },
                  separatorBuilder: (context, index) {
                    return Container(
                      margin: EdgeInsets.only(left: 20/360*screenWidth,right: 20/360*screenWidth),
                        child: Divider());
                  },
                ),
              ),
              Container(
               // color: Colors.white,
                margin: EdgeInsets.only(
                  bottom: (10 / 720) * screenHeight,
                ),
                child: Row(
                 // mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    /**Circular Avatar**/
                    Container(
                      height: (30 / 720) * screenHeight,
                      width: (30 / 360) * screenWidth,
                      margin: EdgeInsets.only(

                          left: (20 / 360) * screenWidth),
                      decoration: new BoxDecoration(
                          shape: BoxShape.circle,
                          image: new DecorationImage(
                              fit: BoxFit.cover,
                              image: AssetImage(
                                  'assets/images/oval-2.png'))),
                    ),
                    Container(
                     //height: (40 / 720) * screenHeight,
                      width: (280 / 360) * screenWidth,
                      margin: EdgeInsets.only(
                        left: (10 / 360) * screenWidth,
                        right: (0 / 360) * screenWidth,
                        ),
                      decoration: BoxDecoration(
                        //  backgroundBlendMode: BlendMode.exclusion,
                        border: Border.all(color: AccentColors.iceBlue),
                        color: NeutralColors.pureWhite,
                        borderRadius: BorderRadius.all(Radius.circular(5)),
                      ),
                      child: Center(
                        child: Padding(
                          padding: EdgeInsets.only(
                              left: (13 / Constant.defaultScreenWidth) *
                                  screenWidth),
                          child: TextFormField(
                            controller: _reply,
                            focusNode: _focusNode,
                            autofocus: _screenName=="Reply" ? true : false ,
                            textInputAction: TextInputAction.done,
                            autovalidate: false,
                            decoration: InputDecoration(
                              border: InputBorder.none,
                              hintText: 'Reply...',
                              hintStyle: TextStyle(
                                fontSize: (14/Constant.defaultScreenWidth)*screenWidth,
                                fontFamily: "IBMPlexSans",
                                color: NeutralColors.blue_grey,
                              ),
                              suffixIcon: IconButton(
                                  icon: Icon(Icons.attach_file,color: NeutralColors.purpleish_blue,),
                                  onPressed: () {
                                    _pickFileInProgress ? null : _pickDocument();
                                    print("pick");
                                  }
                              ),
                            ),
                            style: TextStyle(
                              color: NeutralColors.blue_grey,
                              fontWeight: FontWeight.normal,
                              fontFamily: "IBMPlexSans",
                              fontStyle: FontStyle.normal,
                              fontSize: (14 / 360) * screenWidth,
                            ),
                            onFieldSubmitted: (val){
                              print(widget.index);
                              timeAgoSinceDate(DateTime.now().toString());
                              setState(() {
                                if(widget.index!=null){
                                  replies.add(val);
                                  likes.add(324);
                                  timeAgoList.add(timeAgoSinceDate(DateTime.now().toString()));
                                  print(replies);
                                  _reply.clear();
                                }
                              });
                              return;
                            },
                          ),

                        ),
                      ),
                    ),
                    /**Post of Query**/
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:imsindia/components/bottom_navigation.dart';
import 'package:imsindia/components/navigation_bar.dart';
import 'package:imsindia/resources/strings/channel_labels.dart';
import 'package:imsindia/routers/routes.dart';
import 'package:imsindia/utils/colors.dart';
import 'package:imsindia/utils/screen_util.dart';
import 'package:imsindia/utils/svg_images/channel_svg_icons.dart';
import 'package:imsindia/views/channel/channel_live_sessions.dart';
import 'package:imsindia/views/channel/channel_recorded_session.dart';
import 'package:imsindia/views/channel/channel_replies.dart';
import 'package:imsindia/views/login_page/login_enrollmentid_widget.dart';

import 'channel_filter.dart';
import 'channel_video_session.dart';
class ChannelHome extends StatefulWidget {

  @override
  _ChannelHomeState createState() => _ChannelHomeState();
}

class _ChannelHomeState extends State<ChannelHome> with TickerProviderStateMixin{
  List<Tab> tabList = List();
  int _index;
  int _currentIndex;

  TabController _tabController;
  ScrollController _scroll;
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  _handleTabSelection() {
    setState(() {
      _currentIndex = _tabController.index;
      print("index"+_currentIndex.toString());
    });
    // entry.overlayEntry.remove();

  }
  @override
  void initState() {
    super.initState();
    _scroll = new ScrollController();
    _tabController = new TabController(vsync: this, length: tabList.length);
    _index = 0;
    tabList.add(new Tab(
      child: Text(
        "Live Sessions",
      ),
    ));
    tabList.add(new Tab(
      child: Text(
        "Recorded Sessions",
      ),
    ));
    tabList.add(new Tab(
      child: Text(
        "Videos",
      ),
    ));
    setState(() {
      _tabController = new TabController(vsync: this, length: tabList.length);
      _tabController.addListener(_handleTabSelection);

      this._index = _index;


      print(_index);
    });
  }

//  @override
//  Widget build(BuildContext context) {
//    var _mediaQueryData = MediaQuery.of(context);
//    double screenWidth = _mediaQueryData.size.width;
//    double screenHeight = _mediaQueryData.size.height;
//    return DefaultTabController(
//      length: 3,
//      child: Scaffold(
//       // key: _scaffoldKey,
//        drawer: NavigationDrawer(),
//        appBar: AppBar(
//          titleSpacing: 0.0,
//          brightness: Brightness.light,
//          centerTitle: false,
//          elevation:0.0,
//          actions: <Widget>[
//            InkWell(
//              onTap: (){
//                AppRoutes.push(context, ChannelFilter());
//              },
//              child: Padding(
//                padding:  EdgeInsets.only(right: (21/Constant.defaultScreenWidth)*screenWidth),
//                child: ChannelAssets.filterIcon,
//              ),
//            )],
//          backgroundColor: Colors.white,
//          iconTheme: IconThemeData(
//            color: Colors.black,
//          ),
//          bottom: TabBar(
//            controller: _tabController,
//            indicatorColor: NeutralColors.purpleish_blue,
//            labelColor: NeutralColors.purpleish_blue,
//            unselectedLabelColor: NeutralColors.blue_grey,
//            indicatorSize: TabBarIndicatorSize.tab,
//            isScrollable: true,
//            unselectedLabelStyle: TextStyle(
//              fontSize: 14 / 360 * screenWidth,
//              fontFamily: "IBMPlexSans",
//              fontWeight: FontWeight.w400,
//            ),
//            labelStyle: TextStyle(
//              fontSize: 14 / 360 * screenWidth,
//              fontFamily: "IBMPlexSans",
//              fontWeight: FontWeight.w500,
//            ),
//            tabs: [
//              Tab(text:ChannelStrings.liveSessions),
//              Tab(text:ChannelStrings.recordedSessions),
//              Tab(text:ChannelStrings.videos),
//            ],
//          ),
//          title: Text(
//            ChannelStrings.appBarLabel,
//            style: TextStyle(
//              fontSize: (16/Constant.defaultScreenWidth)*screenWidth,
//              fontFamily: "IBMPlexSans",
//              fontWeight: FontWeight.w500,
//              color: Colors.black,
//            ),
//            textAlign: TextAlign.left,),
//        ),
//        body: TabBarView(
//          controller: _tabController,
//          children: [
//            ChannelLiveSessions(),
//            Chanel_Recorded_Session(),
//            Chanel_Video_Session(),
//
//          ],
//        ),
//      ),
//    );
//  }
  @override
  Widget build(BuildContext context) {
    var _mediaQueryData = MediaQuery.of(context);
    double screenWidth = _mediaQueryData.size.width;
    double screenHeight = _mediaQueryData.size.height;
    return Scaffold(
      drawer: NavigationDrawer(),
//      bottomNavigationBar: BottomNavigation(),
      appBar: AppBar(
        backgroundColor: Colors.white,
        elevation: 0.0,
        titleSpacing: 0.0,
        brightness: Brightness.light,
        centerTitle: false,
        iconTheme: IconThemeData(
          color: Colors.black,
        ),
        title: Text(
          ChannelStrings.appBarLabel,
          style: TextStyle(
            fontSize: (16 / Constant.defaultScreenWidth) * screenWidth,
            fontFamily: "IBMPlexSans",
            fontWeight: FontWeight.w500,
            color: Colors.black,
          ),
          textAlign: TextAlign.left,
        ),
          actions: <Widget>[
            InkWell(
              onTap: (){
                AppRoutes.push(context, ChannelFilter());
              },
              child: Padding(
                padding:  EdgeInsets.only(right: (21/Constant.defaultScreenWidth)*screenWidth),
                child: ChannelAssets.filterIcon,
              ),
            )],
      ),
    body: Container(
      color: Colors.white,

      child: Column(
        children: <Widget>[
          Container(
            margin: EdgeInsets.only(left:20/Constant.defaultScreenWidth*screenWidth,
                    right: 20/Constant.defaultScreenWidth*screenWidth),
            height: (40/Constant.defaultScreenWidth*screenWidth),
            child: TabBar(
              isScrollable: true,
              controller: _tabController,
              indicatorColor: NeutralColors.purpleish_blue,
              labelColor: NeutralColors.purpleish_blue,
              unselectedLabelColor: NeutralColors.blue_grey,
              indicatorSize: TabBarIndicatorSize.tab,
              tabs: tabList,
              unselectedLabelStyle: TextStyle(
                  fontSize: 14 / 360 * screenWidth,
                  fontFamily: "IBMPlexSans",
                  fontWeight: FontWeight.w400,
                ),
                labelStyle: TextStyle(
                  fontSize: 14 / 360 * screenWidth,
                  fontFamily: "IBMPlexSans",
                  fontWeight: FontWeight.w500,
                ),
                indicator:  UnderlineTabIndicator(
                borderSide: BorderSide(width: 2.0,color: NeutralColors.purpleish_blue ),
                insets: EdgeInsets.symmetric(horizontal:0.0),

              ),
            ),
          ),
          Expanded(
            child: TabBarView(
              controller: _tabController,
              children: [
                ChannelLiveSessions(),
                Chanel_Recorded_Session(),
                Chanel_Video_Session(),
              ],
            ),
          ),
        ],
      ),
    ),
    );
  }
}

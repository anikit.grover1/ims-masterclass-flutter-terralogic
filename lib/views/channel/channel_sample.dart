import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:imsindia/components/navigation_bar.dart';
import 'package:imsindia/resources/strings/channel_labels.dart';
import 'package:imsindia/resources/strings/gk_zone.dart';
import 'package:imsindia/routers/routes.dart';
import 'package:imsindia/utils/colors.dart';
import 'package:imsindia/utils/screen_util.dart';
import 'package:imsindia/utils/svg_images/channel_svg_icons.dart';
import 'package:imsindia/utils/svg_images/prepare_svg_images.dart';
import 'package:imsindia/views/channel/channel_comments.dart';
import 'package:smooth_star_rating/smooth_star_rating.dart';

class ChannelVideosScreens extends StatefulWidget {
  @override
  _ChannelVideosScreensState createState() => _ChannelVideosScreensState();
}

class _ChannelVideosScreensState extends State<ChannelVideosScreens> with TickerProviderStateMixin{
  int _currentIndex;
  List<Tab> tabList = [
    new Tab(
      child: Container(
        width: 100,
        child: Center(
          child: Text(
            "Queries",
          ),
        ),
      ),
    ),
    new Tab(
      child: Container(
        width: 80,
        child: Center(
          child: Text(
            "Up Next",
          ),
        ),
      ),
    ),
    new Tab(
      child: Container(
        width: 100,
        child: Center(
          child: Text(
            "Details",
          ),
        ),
      ),
    ),
  ];
  TabController _tabController;
  int _index;
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  _handleTabSelection() {
    setState(() {
      _currentIndex = _tabController.index;
      print("index"+_currentIndex.toString());
    });
    // entry.overlayEntry.remove();

  }
  @override
  void initState() {
    super.initState();
    _tabController = new TabController(vsync: this, length: tabList.length);
    _index = 0;
    setState(() {
      _tabController = new TabController(vsync: this, length: tabList.length);
      _tabController.addListener(_handleTabSelection);

      this._index = _index;


      print(_index);
    });
  }
  @override
  Widget build(BuildContext context) {
    var _mediaQueryData = MediaQuery.of(context);
    double screenWidth = _mediaQueryData.size.width;
    double screenHeight = _mediaQueryData.size.height;
    return Scaffold(
      // key: _scaffoldKey,
      
//      appBar: AppBar(
//        backgroundColor: Colors.white,
//        elevation: 0.0,
//        titleSpacing: 0.0,
//        brightness: Brightness.light,
//        centerTitle: false,
//        iconTheme: IconThemeData(
//          color: Colors.black,
//        ),
//        title: Text(
//          "",
//          style: TextStyle(
//            fontSize: (16 / Constant.defaultScreenWidth) * screenWidth,
//            fontFamily: "IBMPlexSans",
//            fontWeight: FontWeight.w500,
//            color: Colors.black,
//          ),
//          textAlign: TextAlign.left,
//        ),
//      ),
      body: Container(
        color: Colors.white,
        // height: 300.0 / 720 * screenHeight,
        margin: EdgeInsets.only(
            top: (0 / Constant.defaultScreenWidth) * screenWidth),
        child: Column(
          children: <Widget>[
            /**For video container**/
            Container(
              margin: EdgeInsets.only(
                  top: (20 / Constant.defaultScreenWidth) * screenWidth),
              child: _getChewiePlayer(false,context), // Hide snd Show widgets
            ),
            Container(
              child: Row(
                children: <Widget>[
                  Container(
                    decoration: new BoxDecoration(
                      borderRadius: new BorderRadius.circular(2.0),
                      color: NeutralColors.purpleish_blue.withOpacity(0.10),
                    ),
                    margin: EdgeInsets.only(
                        top: 10 / 720 * screenHeight,
                        left: 21 / 360 * screenWidth),
                    child: Padding(
                      padding:  EdgeInsets.only(left: (5/Constant.defaultScreenWidth)*screenWidth,
                          right: (5/Constant.defaultScreenWidth)*screenWidth,
                        top: 1 / 720 * screenHeight,
                        bottom: 1 / 720 * screenHeight,),
                      child: Text(
                        Quick_tipsScreenStrings.Text_strategy,
                        style: TextStyle(
                          color: Color.fromARGB(255, 86, 72, 235),
                          fontSize: 12 / 360 * screenWidth,
                          fontFamily: "IBM Plex Sans",
                          fontWeight: FontWeight.w500,
                        ),
                        textAlign: TextAlign.center,
                      ),
                    ),
                  ),
                  Container(
//                    onTap:(){
//                      _controller.open();
//
//                      setState(() {
//                        flag = true;
//                      });
//                    },
                    child: Container(
                      width: 15 / 360 * screenWidth,
                      height: 15 / 720 * screenHeight,
                      margin: EdgeInsets.only(
                          top: 10 / 720 * screenHeight,
                          left: 160 / 360 * screenWidth),
                      child: SvgPicture.asset(
                        ChannelAssets.downloadicon,
                        fit: BoxFit.none,
                      ),
                    ),
                  ),
                  Container(
                    child: Container(
                      height: 15 / 720 * screenHeight,
                      width: 15 / 360 * screenWidth,
                      margin: EdgeInsets.only(
                          left: 29 / 360 * screenWidth,
                          top: 10 / 720 * screenHeight),
                      child: SvgPicture.asset(
                        ChannelAssets.bookmark,
                        //fit: BoxFit.scaleDown,
                      ),
                    ),
                  ),
                  Container(
                    width: 15 / 360 * screenWidth,
                    height: 15 / 720 * screenHeight,
                    margin: EdgeInsets.only(
                        top: 10 / 720 * screenHeight,
                        left: 29 / 360 * screenWidth),
                    child: SvgPicture.asset(
                      ChannelAssets.Star,
                      fit: BoxFit.scaleDown,
                    ),
                  ),
                ],
              ),
            ),
            /**For Accuracy text container**/
            Align(
              alignment: Alignment.topLeft,
              child: Container(
               // color: Colors.red,
                margin: EdgeInsets.only(
                    top: 10 / 764 * screenHeight,
                    left: 21 / 360 * screenWidth),
                //height: 15/764 * screenHeight,
                //width: 51/360 * screenWidth,
                child: Text(
                  Quick_tipsScreenStrings.Text_title,
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontFamily: "IBMPlexSans",
                    fontStyle: FontStyle.normal,
                    fontSize: (14 / 360) * screenWidth,
                    color: NeutralColors.dark_navy_blue,
                  ),
                ),
              ),
            ),
            Container(
              //height: 15/720*screenHeight,
             // color: Colors.yellowAccent,
              margin: EdgeInsets.only(
                  top: 8 / 720 * screenHeight,
                  left: 21 / 360 * screenWidth),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Container(
                    height: (22 / Constant.defaultScreenHeight) *
                        screenHeight,
                    width: (22 / Constant.defaultScreenWidth) *
                        screenWidth,
                    margin: EdgeInsets.only(
                        top: (5 / 720) * screenHeight),
                    decoration: new BoxDecoration(
                        shape: BoxShape.circle,
                        image: new DecorationImage(
                            fit: BoxFit.cover,
                            image: AssetImage(
                                'assets/images/oval-2.png'))),
                  ),
                  InkWell(

                    child: Container(
                      // height: 18 / 720 * screenHeight,
                      // width: 112/360*screenWidth,
                      margin: EdgeInsets.only(
                          left: 8 / 360 * screenWidth,
                          top: 0 / 720 * screenHeight),
                      child: Text(
                        ChannelStrings.userName,
                        style: TextStyle(
                          color: NeutralColors.dark_navy_blue,
                          fontSize: 14 / 360 * screenWidth,
                          fontFamily: "IBMPlexSans",
                          fontWeight: FontWeight.w500,
                        ),
                        textAlign: TextAlign.left,
                      ),
                    ),
                  ),
                  Container(
                    width: 2 / 360 * screenWidth,
                    height: 2 / 720 * screenHeight,
                    margin: EdgeInsets.only(
                        left: (10 / Constant.defaultScreenWidth) *
                            screenWidth,
                        top: (0 / Constant.defaultScreenHeight) *
                            screenHeight),
                    decoration: BoxDecoration(
                      color: NeutralColors.blue_grey,
                      borderRadius:
                      BorderRadius.all(Radius.circular(1.5)),
                    ),
                    child: Container(),
                  ),
                  Container(
                    margin: EdgeInsets.only(
                        top: 0 / 720 * screenHeight,
                        left: 10 / 360 * screenWidth),
                    child: SmoothStarRating(
                      allowHalfRating: false,
                      starCount: 5,
                      rating: 3,
                      size: 17.0,
                      color: NeutralColors.sun_yellow,
                      borderColor: NeutralColors.sun_yellow,
                      spacing: 0.0,
                    ),
                  ),
                  Container(
                    // height: 18 / 720 * screenHeight,
                    // width: 65 / 360 * screenWidth,
                    margin: EdgeInsets.only(
                        left: 7 / 360 * screenWidth,
                        top: 0 / 720 * screenHeight),
                    child: Text(
                      Quick_tipsScreenStrings.Text_rating,
                      style: TextStyle(
                        color: Color.fromARGB(255, 153, 154, 171),
                        fontSize: 14 / 360 * screenWidth,
                        fontFamily: "IBMPlexSans",
                        fontWeight: FontWeight.w500,
                      ),
                      textAlign: TextAlign.left,
                    ),
                  ),
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.only(
                  left: 20 / 360 * screenWidth,
                  right: 20 / 360 * screenWidth,

                  top: (10 / Constant.defaultScreenHeight) * screenHeight),
              child: Row(
             //   crossAxisAlignment: CrossAxisAlignment.end,
                children: <Widget>[
                  Container(
                    child: TabBar(
                      //Opacity: 1.0,
                      controller: _tabController,
                      indicatorColor: NeutralColors.purpleish_blue,
                      labelColor: NeutralColors.purpleish_blue,
                      unselectedLabelColor: NeutralColors.blue_grey,
                      indicatorSize: TabBarIndicatorSize.tab,
                      tabs: tabList,
                      indicatorPadding: EdgeInsets.only(
                          left: 5.0 / 360 * screenWidth,
                          right: 5.0 / 360 * screenWidth
                      ),
                      isScrollable: true,
                      unselectedLabelStyle: TextStyle(
                        fontSize: 14 / 360 * screenWidth,
                        fontFamily: "IBMPlexSans",
                        fontWeight: FontWeight.w400,
                      ),
                      labelStyle: TextStyle(
                        fontSize: 14 / 360 * screenWidth,
                        fontFamily: "IBMPlexSans",
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                  ),

                  //  Divider(),
                ],
              ),
            ),
            Expanded(
              child: TabBarView(
                controller: _tabController,
                children: [
                  ChannelComments(),
                  ChannelComments(),
                  ChannelComments()
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
_getChewiePlayer(bool offStageFlag, BuildContext context){
  Offstage offstage = new Offstage(
    offstage: offStageFlag,
    child: Padding(
      padding: const EdgeInsets.all(10.0),
      child: Container(
        margin: EdgeInsets.only(
            left: (20 / 360) * screenWidth, right: (20 / 360) * screenWidth),
        height: 181 / 720 * screenHeight,
        child: InkWell(
          onTap: (){
          //  AppRoutes.push(context,VideoPlayerApp());
          },
          child: Container(
            constraints: new BoxConstraints.expand(
              height: (181 / 720) * screenHeight,

            ),
            alignment: Alignment.bottomLeft,
            padding: new EdgeInsets.only(left: 16.0, bottom: 8.0),
            decoration:  BoxDecoration(
              borderRadius: new BorderRadius.circular(5.0),
              image: DecorationImage(
                  alignment: Alignment(-.2, 0),
                  image: CachedNetworkImageProvider(
                      'http://d1cn01kw45hc6h.cloudfront.net/wp-content/uploads/2019/08/22161210/Quadtratic.png'),
//                  image: NetworkImage(
//                      'http://d1cn01kw45hc6h.cloudfront.net/wp-content/uploads/2019/08/22161210/Quadtratic.png'
//                  ),
                  fit: BoxFit.cover),
            ),
            child: Center(
              child: Container(
                height: (50 / 720) * screenHeight,
                width: (50 / 360) * screenWidth,
                child: SvgPicture.asset(
                  getPrepareSvgImages.playwhite,
                  fit: BoxFit.fitHeight,
                ),
              ),
            ),
          ),
        ),
      ),
    ),
  );
  return offstage;
}
import 'package:flutter/material.dart';
import 'package:imsindia/components/video_player.dart';
import 'package:imsindia/resources/strings/channel_labels.dart';
import 'package:imsindia/utils/screen_util.dart';
import 'package:imsindia/utils/screen_util.dart';
import 'package:imsindia/utils/svg_images/channel_svg_icons.dart';
import 'package:imsindia/views/channel/channel_comments.dart';
import 'package:sliding_up_panel/sliding_up_panel.dart';
import 'package:smooth_star_rating/smooth_star_rating.dart';
import 'package:video_player/video_player.dart';
import 'package:imsindia/utils/colors.dart';
import 'package:imsindia/resources/strings/prepare.dart';
import 'package:imsindia/utils/svg_images/prepare_svg_images.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'channel_comment.dart';

class Channelbottomsheet extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => new Channel_channelbottomsheetState();
}

class Channel_channelbottomsheetState extends State<Channelbottomsheet>
    with TickerProviderStateMixin {

  final _controller = PanelController();
  double _panelHeightClosed = 0.0;
  bool flag = true;

  List<Tab> tabList = List();
  TabController _tabController;

  @override
  void initState() {
    tabList.add(new Tab(
      child: Text(
        Quick_tipsScreenStrings.Tab_Queries,
        style: TextStyle(
          fontWeight: FontWeight.bold,
          fontFamily: "IBMPlexSans",
          fontStyle: FontStyle.normal,
          fontSize: 12,
        ),
        textAlign: TextAlign.left,
      ),
    ));
    tabList.add(new Tab(
      child: Text(
        Quick_tipsScreenStrings.Tab_UpNext,
        style: TextStyle(
          fontWeight: FontWeight.bold,
          fontFamily: "IBMPlexSans",
          fontStyle: FontStyle.normal,
          fontSize: 12,
        ),
        textAlign: TextAlign.left,
      ),
    ));
    tabList.add(new Tab(
      child: Text(
        Quick_tipsScreenStrings.Tab_Details,
        style: TextStyle(
          fontWeight: FontWeight.bold,
          fontFamily: "IBMPlexSans",
          fontStyle: FontStyle.normal,
          fontSize: 12,
        ),
        textAlign: TextAlign.left,
      ),
    ));
    _tabController = new TabController(vsync: this, length: tabList.length);
    super.initState();
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  double ratingValue = 0.0;
  bool isClicked = false;

  @override
  Widget build(BuildContext context) {
    final screenHeight = MediaQuery.of(context).size.height;
    final screenWidth = MediaQuery.of(context).size.width;
    void _showDialog() {
      setState(() {
        isClicked = true;
      });
      showDialog(
        context: context,
        builder: (context) {
          String contentText = "Content of Dialog";
          return StatefulBuilder(
            builder: (context, setState) {
              return SimpleDialog(
                children: <Widget>[
                  new SimpleDialogOption(
                    child: Container(
                      height:
                      (40 / Constant.defaultScreenHeight) * screenHeight,
                      width: (320 / Constant.defaultScreenWidth) * screenWidth,
//                      margin: EdgeInsets.only(left: (20/Constant.defaultScreenWidth)*screenWidth,
//                      right: (20/Constant.defaultScreenWidth)*screenWidth),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          new Text(
                            ChannelStrings.rate_this_video,
                            style: TextStyle(
                              color: NeutralColors.dark_navy_blue,
                              fontSize: (14 / Constant.defaultScreenWidth) *
                                  screenWidth,
                              fontFamily: "IBMPlexSans",
                              fontWeight: FontWeight.normal,
                            ),
                          ),
                          Container(
                            child: SmoothStarRating(
                              allowHalfRating: false,

                              onRated: (v) {
                                ratingValue = v;
                                print('v+++++++++++++' + v.toString());
                                setState(() {
                                  ratingValue = v;
                                  print('rating-----------' +
                                      ratingValue.toString());
                                });
                              },
                              starCount: 5,
                              rating: ratingValue,
                              size: 25.0/720 * screenHeight,
                              color: NeutralColors.sun_yellow,
                              borderColor: NeutralColors.blue_grey,
                              spacing: 0.0,
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                ],
              );
            },
          );
        },
      );
    }

    TextStyle label = new TextStyle(
        fontWeight: FontWeight.w700, color: NeutralColors.purpleish_blue);
    TextStyle unselectedLabel = new TextStyle(
        fontWeight: FontWeight.w200, color: NeutralColors.blue_grey);
    return GestureDetector(
      onTap: (){
        setState(() {
          isClicked = false;
        });
      },
      child: Container(
          color: NeutralColors.pureWhite,
          child: Scaffold(
            
            body:  GestureDetector(
              onTap: () {
                _controller.close();
              },
              child: Container(

                child: SlidingUpPanel(
                  maxHeight: flag ? screenHeight / 2: screenHeight/4,
                  minHeight: _panelHeightClosed,
                  parallaxEnabled: false,
                  parallaxOffset: .5,
                  //defaultPanelState: PanelState.OPEN,
                  isDraggable: true,
                  body: body(),
                  panel: flag?_panel():_panelnew(),
                  //panel:_panelnew(),
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(25.0), topRight: Radius.circular(25.0)),
                  //onPanelSlide: (double pos) => setState((){
                  //_fabHeight =  _initFabHeight/2;
                  //}),
                  controller: _controller,
                ),
              ),
            ),


          )
      ),
    );
  }
  Widget body(){
    final screenHeight = MediaQuery.of(context).size.height;
    final screenWidth = MediaQuery.of(context).size.width;

    return  SingleChildScrollView(
      child: new Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            /**For video container**/
            Container(
//                        height: 180.1/764 * screenHeight,
//                        width: 320/360 * screenWidth,
              margin: EdgeInsets.only(
                  top: 46 / 764 * screenHeight,
                  left: 20 / 360 * screenWidth,
                  right: 20 / 360 * screenWidth),
              child: ChewieListItem(
                videoPlayerController: VideoPlayerController.network(
                  Quick_tipsScreenStrings.Video_URL,
                ),
              ),
            ),
            /**For Calculation text container**/
            Container(
              child: Row(
                children: <Widget>[
                  Container(
                    width: 56 / 360 * screenWidth,
                    decoration: new BoxDecoration(
                      borderRadius: new BorderRadius.circular(2.0),
                      color: NeutralColors.pale_grey,
                    ),
                    margin: EdgeInsets.only(
                        top: 10 / 720 * screenHeight,
                        left: 21 / 360 * screenWidth),
                    child: Text(
                      Quick_tipsScreenStrings.Text_strategy,
                      style: TextStyle(
                        color: Color.fromARGB(255, 86, 72, 235),
                        fontSize: 12 / 360 * screenWidth,
                        fontFamily: "IBMPlexSans",
                        fontWeight: FontWeight.w500,
                      ),
                      textAlign: TextAlign.center,
                    ),
                  ),
                  InkWell(
//                    onTap:(){
//                      _controller.open();
//
//                      setState(() {
//                        flag = true;
//                      });
//                    },
                    child: Container(
                      width: 15 / 360 * screenWidth,
                      height: 15 / 720 * screenHeight,
                      margin: EdgeInsets.only(
                          top: 10 / 720 * screenHeight,
                          left: 205 / 360 * screenWidth),
                      child: SvgPicture.asset(
                        ChannelAssets.downloadicon,
                        fit: BoxFit.none,
                      ),
                    ),
                  ),
                  InkWell(
                    onTap:(){
                      _controller.open();
                      setState(() {
                        flag = false;
                      });
                    },
                    child: Container(
                      height: 15 / 720 * screenHeight,
                      width: 15 / 360 * screenWidth,
                      margin: EdgeInsets.only(
                          left: 29 / 360 * screenWidth,
                          top: 10 / 720 * screenHeight),
                      child: SvgPicture.asset(
                        ChannelAssets.bookmark,
                        //fit: BoxFit.scaleDown,
                      ),
                    ),
                  ),
//                  Container(
//                    width: 15 / 360 * screenWidth,
//                    height: 15 / 720 * screenHeight,
//                    margin: EdgeInsets.only(
//                        top: 10 / 720 * screenHeight,
//                        left: 29 / 360 * screenWidth),
//                    child: SvgPicture.asset(
//                      ChannelAssets.Star,
//                      fit: BoxFit.none,
//                    ),
//                  ),
                ],
              ),
            ),
            /**For Accuracy text container**/
            Container(
              margin: EdgeInsets.only(
                  top: 10 / 764 * screenHeight,
                  left: 20 / 360 * screenWidth),
              //height: 15/764 * screenHeight,
              //width: 51/360 * screenWidth,
              child: Text(
                Quick_tipsScreenStrings.Text_title,
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontFamily: "IBMPlexSans",
                  fontStyle: FontStyle.normal,
                  fontSize: (14 / 360) * screenWidth,
                  color: NeutralColors.dark_navy_blue,
                ),
                textAlign: TextAlign.left,
              ),
            ),
            Container(
              //height: 15/720*screenHeight,
              margin: EdgeInsets.only(
                  top: 8 / 720 * screenHeight,
                  left: 21 / 360 * screenWidth),
              child: Row(
                children: <Widget>[
                  Container(
                    height: (22 / Constant.defaultScreenHeight) *
                        screenHeight,
                    width: (22 / Constant.defaultScreenWidth) *
                        screenWidth,
                    margin: EdgeInsets.only(
                        top: (5 / 720) * screenHeight),
                    decoration: new BoxDecoration(
                        shape: BoxShape.circle,
                        image: new DecorationImage(
                            fit: BoxFit.cover,
                            image: AssetImage(
                                'assets/images/oval-2.png'))),
                  ),
                  InkWell(
                    onTap:(){
                      _controller.open();

                      setState(() {
                        flag = true;
                      });
                      showDialog();
                    },
                    child: Container(
                      height: 18 / 720 * screenHeight,
                      // width: 112/360*screenWidth,
                      margin: EdgeInsets.only(
                          left: 8 / 360 * screenWidth,
                          top: 5 / 720 * screenHeight),
                      child: Text(
                        ChannelStrings.userName,
                        style: TextStyle(
                          color: NeutralColors.dark_navy_blue,
                          fontSize: 14 / 360 * screenWidth,
                          fontFamily: "IBMPlexSans",
                          fontWeight: FontWeight.w500,
                        ),
                        textAlign: TextAlign.left,
                      ),
                    ),
                  ),
                  Container(
                    width: 2 / 360 * screenWidth,
                    height: 2 / 720 * screenHeight,
                    margin: EdgeInsets.only(
                        left: (10 / Constant.defaultScreenWidth) *
                            screenWidth,
                        top: (5 / Constant.defaultScreenHeight) *
                            screenHeight),
                    decoration: BoxDecoration(
                      color: NeutralColors.blue_grey,
                      borderRadius:
                      BorderRadius.all(Radius.circular(1.5)),
                    ),
                    child: Container(),
                  ),
                  Container(
                    margin: EdgeInsets.only(
                        top: 5 / 720 * screenHeight,
                        left: 10 / 360 * screenWidth),
                    child: SmoothStarRating(
                      allowHalfRating: false,
                      starCount: 5,
                      rating: 3,
                      size: 15.0,
                      color: NeutralColors.sun_yellow,
                      borderColor: NeutralColors.sun_yellow,
                      spacing: 0.0,
                    ),
                  ),
                  Container(
                    height: 18 / 720 * screenHeight,
                    width: 65 / 360 * screenWidth,
                    margin: EdgeInsets.only(
                        left: 7 / 360 * screenWidth,
                        top: 5 / 720 * screenHeight),
                    child: Text(
                      Quick_tipsScreenStrings.Text_rating,
                      style: TextStyle(
                        color: Color.fromARGB(255, 153, 154, 171),
                        fontSize: 14 / 360 * screenWidth,
                        fontFamily: "IBMPlexSans",
                        fontWeight: FontWeight.w500,
                      ),
                      textAlign: TextAlign.left,
                    ),
                  ),
                ],
              ),
            ),
            isClicked ?  Container(
              height: 85/720 * screenHeight,
              child: Column(
                children: <Widget>[
                  Container(
                      margin: EdgeInsets.only(top:(0/Constant.defaultScreenHeight)*screenHeight),
                      child: Divider()),
                  Container(
                    margin: EdgeInsets.only(left: (20/Constant.defaultScreenWidth)*screenWidth,
                      right: (20/Constant.defaultScreenWidth)*screenWidth,
                      top:(10/Constant.defaultScreenHeight)*screenHeight,
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        new Text(
                          ChannelStrings.rate_this_video,
                          style: TextStyle(
                            color: NeutralColors.dark_navy_blue,
                            fontSize: (14 / Constant.defaultScreenWidth) *
                                screenWidth,
                            fontFamily: "IBMPlexSans",
                            fontWeight: FontWeight.normal,
                          ),
                        ),
                        Container(
                          child: SmoothStarRating(
                            allowHalfRating: false,
                            starCount: 5,
                            rating: ratingValue,
                            size: (20/Constant.defaultScreenWidth)*screenWidth,
                            color: NeutralColors.sun_yellow,
                            borderColor: NeutralColors.blue_grey,
                            spacing: 0.0,
                          ),
                        )
                      ],
                    ),
                  ),
                  Container(
                      margin: EdgeInsets.only(top:(10/Constant.defaultScreenHeight)*screenHeight),
                      child: Divider()),
                ],
              ),
            ) : Container(),
            Container(
              width: screenWidth,
              margin: EdgeInsets.only(top: isClicked==false?31/720 * screenHeight:0),
              child: Padding(
                padding: const EdgeInsets.all(2.0),
                child: new Column(
                  children: <Widget>[
                    new Container(
                      height: 36/720 * screenHeight,
                      // decoration: new BoxDecoration(color: Theme.of(context).primaryColor),
                      child: new TabBar(
                          controller: _tabController,
                          indicatorColor: NeutralColors.purpleish_blue,
                          labelColor: NeutralColors.purpleish_blue,
                          unselectedLabelColor: NeutralColors.blue_grey,
                          indicatorSize: TabBarIndicatorSize.tab,
                          tabs: tabList),
                    ),
                    new Container(
                      height: 300.0 / 720 * screenHeight,
                      child: new TabBarView(
                        controller: _tabController,
                        children: <Widget>[
                          ChannelComments(),
                          ChannelComments(),
                          ChannelComments(),
                        ],
                      ),
                    )
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
  Widget _panel() {
    final screenHeight = MediaQuery.of(context).size.height;
    final screenWidth = MediaQuery.of(context).size.width;
    final defaultScreenHeight = 720;
    final defaultScreenWidth = 360;
    return Container(

      height: screenHeight * 389 / 720,
      margin: EdgeInsets.only(top: 18 / defaultScreenHeight * screenHeight,left: 20/defaultScreenWidth*screenWidth),
      //color: Colors.black,
      child: Column(
        children: [
          Container(
            height: 110/720*screenHeight,
            child: Row(
              children: <Widget>[
                Align(
                  alignment: Alignment.topCenter,
                  child: Container(
                    height: 80/defaultScreenHeight*screenHeight,
                    width: 80/defaultScreenWidth*screenWidth,

                    child: CircleAvatar(
                        backgroundColor:
                        Colors.transparent,
                        radius: 50,
                        backgroundImage:AssetImage(
                            'assets/images/bitmap-4.png')

                    ),

                  ),
                ),
                Container(

                  margin: EdgeInsets.only(left: 15/defaultScreenWidth*screenWidth),
                  child: Column(
                    children: <Widget>[
                      Container(
                        height: 15/defaultScreenHeight*screenHeight,
                        width: 71/defaultScreenWidth*screenWidth,
                        margin: EdgeInsets.only(right:174/defaultScreenWidth*screenWidth),
                        child: Text(
                            "Chief Mentor",
                            style:TextStyle(
                              color: GradientColors.blueGrey,
                              fontWeight: FontWeight.w500,
                              fontFamily: "IBMPlexSans",
                          fontStyle: FontStyle.normal,
                              fontSize: (12/defaultScreenWidth)*screenWidth,
                            )
                        ),
                      ),
                      Container(
                        width: 103/defaultScreenWidth*screenWidth,
                        height: 23/defaultScreenHeight*screenHeight,
                        margin: EdgeInsets.only(right: 142/defaultScreenWidth*screenWidth, top: 3/defaultScreenHeight*screenHeight),
                        child: Text(
                            "Professor Name",
                            style:TextStyle(
                              color: NeutralColors.dark_navy_blue,
                              fontWeight: FontWeight.w500,
                              fontFamily: "IBMPlexSans",
                              fontStyle: FontStyle.normal,
                              fontSize: (14/defaultScreenWidth)*screenWidth,
                            )
                        ),
                      ),
                      Container(
                        color: AccentColors.iceBlue,
                        width: 225/defaultScreenWidth*screenWidth,
                        height: 1/defaultScreenHeight*screenHeight,
                        margin: EdgeInsets.only( top: 9.5/defaultScreenHeight*screenHeight),

                      ),
                      Container(
                        child: Row(
                          children: <Widget>[
                            Container(
                              height: 15/defaultScreenHeight*screenHeight,
                              width: 62/defaultScreenWidth*screenWidth,
                              margin: EdgeInsets.only(right:40/defaultScreenWidth*screenWidth,top: 9.5/defaultScreenHeight*screenHeight),
                              child: Text(
                                  "Experience",
                                  style:TextStyle(
                                    color: GradientColors.blueGrey,
                                    fontWeight: FontWeight.w500,
                                    fontFamily: "IBMPlexSans",
                                    fontStyle: FontStyle.normal,
                                    fontSize: (12/defaultScreenWidth)*screenWidth,
                                  )
                              ),
                            ),
                            Container(
                              height: 15/defaultScreenHeight*screenHeight,
                              width: 91/defaultScreenWidth*screenWidth,
                              margin: EdgeInsets.only(right:52/defaultScreenWidth*screenWidth,top: 9.5/defaultScreenHeight*screenHeight),
                              child: Text(
                                  "Students Taught",
                                  style:TextStyle(
                                    color: GradientColors.blueGrey,
                                    fontWeight: FontWeight.w500,
                                    fontFamily: "IBMPlexSans",
                                    fontStyle: FontStyle.normal,
                                    fontSize: (12/defaultScreenWidth)*screenWidth,
                                  )
                              ),
                            ),
                          ],
                        ),
                      ),
                      Container(
                        child: Row(
                          children: <Widget>[
                            Container(
                              height: 23/defaultScreenHeight*screenHeight,
                              width: 40/defaultScreenWidth*screenWidth,
                              margin: EdgeInsets.only(right:62/defaultScreenWidth*screenWidth,top: 3/defaultScreenHeight*screenHeight),
                              child: Text(
                                  "14 yrs",
                                  style:TextStyle(
                                    color: NeutralColors.dark_navy_blue,
                                    fontWeight: FontWeight.w500,
                                    fontFamily: "IBMPlexSans",
                                    fontStyle: FontStyle.normal,
                                    fontSize: (14/defaultScreenWidth)*screenWidth,
                                  )
                              ),
                            ),
                            Container(
                              height: 23/defaultScreenHeight*screenHeight,
                              width: 51/defaultScreenWidth*screenWidth,
                              margin: EdgeInsets.only(right:92/defaultScreenWidth*screenWidth,top: 3/defaultScreenHeight*screenHeight),
                              child: Text(
                                  "10000+",
                                  style:TextStyle(
                                    color: NeutralColors.dark_navy_blue,
                                    fontWeight: FontWeight.w500,
                                    fontFamily: "IBMPlexSans",
                                    fontStyle: FontStyle.normal,
                                    fontSize: (14/defaultScreenWidth)*screenWidth,
                                  )
                              ),
                            ),

                          ],
                        ),

                      )
                    ],
                  ),
                )

              ],
            ),
          ),
          Container(
            height: 20/defaultScreenHeight*screenHeight,
            width: 49/defaultScreenWidth*screenWidth,
            margin: EdgeInsets.only(right:291/defaultScreenWidth*screenWidth,top: 40/defaultScreenHeight*screenHeight),
            child: Text(
                "Profile",
                style:TextStyle(
                  color: NeutralColors.dark_navy_blue,
                  fontWeight: FontWeight.w700,
                  fontFamily: "IBMPlexSans",
                  fontStyle: FontStyle.normal,
                  fontSize: (16/defaultScreenWidth)*screenWidth,
                )
            ),

          ),
          Container(
            height: 156/defaultScreenHeight*screenHeight,
            width: 320/defaultScreenWidth*screenWidth,
            margin: EdgeInsets.only(right:20/defaultScreenWidth*screenWidth,top: 10/defaultScreenHeight*screenHeight),
            child: Text(
                "Parties and family gatherings are popular this time of year. Whether you're celebrating an anniversary, a birthday, graduation, a holiday or the start of your favorite sport's season, entertaining can be quick, simple, affordable and fun if you plan ahead",
                style:TextStyle(
                  color: NeutralColors.dark_navy_blue,
                  fontWeight: FontWeight.w400,
                  fontFamily: "IBMPlexSans",
                  fontStyle: FontStyle.normal,
                  fontSize: (16/defaultScreenWidth)*screenWidth,

                )
            ),

          ),
        ],
      ),
    );
  }
  Widget _panelnew() {
    final screenHeight = MediaQuery.of(context).size.height;
    final screenWidth = MediaQuery.of(context).size.width;
    final defaultScreenHeight = 720;
    final defaultScreenWidth = 360;
    return Container(
      height: screenHeight * 170 / 720,
      margin: EdgeInsets.only(bottom: 60 / defaultScreenHeight * screenHeight,top: 60/defaultScreenHeight*screenHeight,left: 44/defaultScreenWidth*screenWidth,right: 44/defaultScreenWidth*screenWidth),
      //color: Colors.black,
      child: Center(
        child: new RichText(
          text:TextSpan(
            children: [
              new TextSpan(
                text:"To stream videos on cellular data, change settings ",
                style: TextStyle(
                  color: Colors.black,
                  fontWeight: FontWeight.w500,
                  fontFamily: "IBMPlexSans",
                  fontStyle: FontStyle.normal,
                  fontSize: (16/defaultScreenWidth)*screenWidth,

                ),
              ),
              new TextSpan(
                text: "here",
                style: TextStyle(
                  color: Colors.blue,
                  fontWeight: FontWeight.w500,
                  fontFamily: "IBMPlexSans",
                  fontStyle: FontStyle.normal,
                  fontSize: (16/defaultScreenWidth)*screenWidth,

                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
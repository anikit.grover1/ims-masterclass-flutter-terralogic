import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:imsindia/components/custom_DropDown.dart' as prefix0;
import 'package:imsindia/components/navigation_bar.dart';
import 'package:imsindia/resources/strings/channel_labels.dart';
import 'package:imsindia/resources/strings/gk_zone.dart';
import 'package:imsindia/routers/routes.dart';
import 'package:imsindia/utils/colors.dart';
import 'package:imsindia/utils/screen_util.dart';
import 'package:imsindia/utils/svg_icons.dart';
import 'package:imsindia/utils/svg_images/channel_svg_icons.dart';
import 'package:imsindia/utils/svg_images/prepare_svg_images.dart';
import 'package:imsindia/views/Arun/settings/Settings.dart' as prefix1;
import 'package:imsindia/views/Arun/settings/Settings.dart';
import 'package:imsindia/views/channel/channel_comments.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:smooth_star_rating/smooth_star_rating.dart';
import 'package:imsindia/components/custom_dialog.dart' as customDialog;

class ChannelVideosScreens extends StatefulWidget {
  @override
  _ChannelVideosScreensState createState() => _ChannelVideosScreensState();
}

class _ChannelVideosScreensState extends State<ChannelVideosScreens> with TickerProviderStateMixin{


  int _currentIndex;
  double ratingValue = 0.0;
  List<Tab> tabList = List();

  bool isClicked = false;
  final _scaffoldKey = new GlobalKey<ScaffoldState>();
  VoidCallback _showPersBottomSheetCallBack;
  VoidCallback _showSettingsBottomSheetCallBack;
  bool bottomSheetFlag = false;
  TabController _tabController;
  bool focusNodeVal = false;
  int _index;
  bool isRatingClicked = false;
  ScrollController _scroll;
  _handleTabSelection() {
    setState(() {
      _currentIndex = _tabController.index;
      print("index"+_currentIndex.toString());
    });
    // entry.overlayEntry.remove();

  }
  void didChangeDependencies() {
    getValuesSF();
    super.didChangeDependencies();
  }
  getValuesSF() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
//Return String

    bool boolValue = prefs.getBool('focusNode');
    print('****************'+boolValue.toString());

    setState(() {
      focusNodeVal = boolValue;
    });

    print('focusNodefocusNodefocusNodefocusNodefocusNodefocusNode'+boolValue.toString());
  }


  void _showBottomSheet() {
//    setState(() {
//      _showPersBottomSheetCallBack = null;
//
//    });

    _scaffoldKey.currentState
        .showBottomSheet((context) {
      var _mediaQueryData = MediaQuery.of(context);
      double screenWidth = _mediaQueryData.size.width;
      double screenHeight = _mediaQueryData.size.height;
      return    Container(
        height: (389 / Constant.defaultScreenHeight) *
            screenHeight,
        decoration: new BoxDecoration(
          boxShadow: [
            BoxShadow(
              color: Colors.black38,
              offset: Offset(0, 0),
              blurRadius: 15,
              spreadRadius: 2,
            ),
          ],
          borderRadius: BorderRadius.only(
            topRight: Radius.circular(20), topLeft: Radius.circular(20),) ,
          color: NeutralColors.pureWhite,
        ),
        child: new Container(
          margin: EdgeInsets.only(left: (20 / Constant.defaultScreenWidth) *
              screenWidth,top:(30 / Constant.defaultScreenHeight) *
              screenHeight),

          child: new Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Container(
                    height: (80 / Constant.defaultScreenHeight) *
                        screenHeight,
                    width: (80 / Constant.defaultScreenWidth) *
                        screenWidth,
                    margin: EdgeInsets.only(
                        top: (0 / 720) * screenHeight),
                    decoration: new BoxDecoration(
                        shape: BoxShape.circle,
                        image: new DecorationImage(
                            fit: BoxFit.cover,
                            image: NetworkImage(
                                'https://www.awakenthegreatnesswithin.com/wp-content/uploads/2016/06/BG-696x468.jpg'))),
                  ),
                  Expanded(
                    child: Container(
                     // color:Colors.red,
                      margin: EdgeInsets.only(left: (15 / Constant.defaultScreenWidth) *
                          screenWidth,right:(20 / Constant.defaultScreenWidth) *
                          screenWidth ),
                      child: Column(
                        mainAxisSize: MainAxisSize.max,

                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            ChannelStrings.chief_mentor,
                            style: TextStyle(
                              color: NeutralColors.blue_grey,
                              fontSize: 12 / 360 * screenWidth,
                              fontFamily: "IBMPlexSans",
                              fontWeight: FontWeight.w500,
                            ),
                            textAlign: TextAlign.left,
                          ),
                          Container(
                            margin: EdgeInsets.only(top:(3 / Constant.defaultScreenHeight) *
                                screenHeight),
                            child: Text(
                              "Professor Name",
                              style: TextStyle(
                                color: NeutralColors.dark_navy_blue,
                                fontSize: 14 / 360 * screenWidth,
                                fontFamily: "IBMPlexSans",
                                fontWeight: FontWeight.w500,
                              ),
                              textAlign: TextAlign.left,
                            ),
                          ),
                          Container(
                            height:(1 / Constant.defaultScreenHeight) *
                                screenHeight ,
                            margin: EdgeInsets.only(right: (0 / Constant.defaultScreenWidth) *
                                screenWidth,top:(9.5 / Constant.defaultScreenHeight) *
                                screenHeight),
                            color: NeutralColors.ice_blue,
                          ),
                          Container(
                            margin: EdgeInsets.only(top:(9.5 / Constant.defaultScreenHeight) *
                                screenHeight),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,

                              children: <Widget>[
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Container(
                                      margin: EdgeInsets.only(top:(3 / Constant.defaultScreenHeight) *
                                          screenHeight),
                                      child: Text(
                                        ChannelStrings.experience,
                                        style: TextStyle(
                                          color: NeutralColors.blue_grey,
                                          fontSize: 12 / 720 * screenHeight,
                                          fontFamily: "IBMPlexSans",
                                          fontWeight: FontWeight.w500,
                                        ),
                                        textAlign: TextAlign.left,
                                      ),
                                    ),
                                    Container(
                                      margin: EdgeInsets.only(top:(3 / Constant.defaultScreenHeight) *
                                          screenHeight),
                                      child: Text(
                                        "14 yrs",
                                        style: TextStyle(
                                          color: NeutralColors.dark_navy_blue,
                                          fontSize: 14 / 720 * screenHeight,
                                          fontFamily: "IBMPlexSans",
                                          fontWeight: FontWeight.w500,
                                        ),
                                        textAlign: TextAlign.left,
                                      ),
                                    ),
                                  ],
                                ),
                                Container(
                                  margin: EdgeInsets.only(left: (0 / Constant.defaultScreenWidth) *
                                      screenWidth,),
                                  child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: <Widget>[
                                      Container(
                                        margin: EdgeInsets.only(top:(3 / Constant.defaultScreenHeight) *
                                            screenHeight),
                                        child: Text(
                                          ChannelStrings.students_taught,
                                          style: TextStyle(
                                            color: NeutralColors.blue_grey,
                                            fontSize: 12 / 720 * screenHeight,
                                            fontFamily: "IBMPlexSans",
                                            fontWeight: FontWeight.w500,
                                          ),
                                          textAlign: TextAlign.left,
                                        ),
                                      ),
                                      Container(
                                        margin: EdgeInsets.only(top:(3 / Constant.defaultScreenHeight) *
                                            screenHeight),
                                        child: Text(
                                          "10000+",
                                          style: TextStyle(
                                            color: NeutralColors.dark_navy_blue,
                                            fontSize: 14 / 720 * screenHeight,
                                            fontFamily: "IBMPlexSans",
                                            fontWeight: FontWeight.w500,
                                          ),
                                          textAlign: TextAlign.left,
                                        ),
                                      ),
                                    ],
                                  ),
                                )
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  )
                ],
              ),
              Container(
                margin: EdgeInsets.only(top:(40 / Constant.defaultScreenHeight) *
                    screenHeight),
                child: Text(
                  ChannelStrings.profile,
                  style: TextStyle(
                    color: NeutralColors.dark_navy_blue,
                    fontSize: 16 / 360 * screenWidth,
                    fontFamily: "IBMPlexSans",
                    fontWeight: FontWeight.w500,
                  ),
                  textAlign: TextAlign.left,
                ),
              ),
              Container(
                margin: EdgeInsets.only(top:(10 / Constant.defaultScreenHeight) *
                    screenHeight,right: (20/Constant.defaultScreenHeight) *
                    screenHeight),
                child: Text(
                  "Parties and family gatherings are popular this time of year. Whether you’re celebrating an anniversary, a birthday, graduation, a holiday or the start of your favorite sport’s season, entertaining can be quick, simple, affordable and fun if you plan ahead.",
                  style: TextStyle(
                    color: NeutralColors.dark_navy_blue,
                    fontSize: 16 / 360 * screenWidth,
                    fontFamily: "IBMPlexSans",
                    fontWeight: FontWeight.w400,
                  ),
                  textAlign: TextAlign.left,
                ),
              ),
            ],
          ),
        ),
      );

    })
        .closed
        .whenComplete(() {
      if (mounted) {
        setState(() {
          _showPersBottomSheetCallBack = _showBottomSheet;
        });
      }
    });
  }

  Future<void> _showRatingDialog() async {

      double selectedvalue = 0;
      await showDialog<void>(
        context: context,
        builder: (BuildContext context) {
          var _mediaQueryData = MediaQuery.of(context);
          double screenWidth = _mediaQueryData.size.width;
          double screenHeight = _mediaQueryData.size.height;

          return StatefulBuilder(
            builder: (BuildContext context, StateSetter setState) {
              return customDialog.SimpleDialog(
                children: <Widget>[
                  new SimpleDialogOption(
                    child: Container(
                      width: screenWidth,
                      height:
                      (40 / Constant.defaultScreenHeight) * screenHeight,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          new Text(
                            ChannelStrings.rate_this_video,
                            style: TextStyle(
                              color: NeutralColors.dark_navy_blue,
                              fontSize: (14 / Constant.defaultScreenWidth) *
                                  screenWidth,
                              fontFamily: "IBMPlexSans",
                              fontWeight: FontWeight.normal,
                            ),
                          ),
                          Container(
                            child: SmoothStarRating(
                              allowHalfRating: false,
                              onRated: (v) {
                                ratingValue = v;
                                print('v+++++++++++++' + v.toString());
                                setState(() {
                                  ratingValue = v;
                                  selectedvalue = v;
                                  print('rating-----------' +
                                      selectedvalue.toString());
                                });
                              },
                              starCount: 5,
                              rating: ratingValue,
                              size: 20.0/360 * screenWidth,
                              color: NeutralColors.sun_yellow,
                              borderColor: NeutralColors.blue_grey,
                              spacing: 0.0,
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                ],
              );
            },
          );
        },
      ).then((val){
        setState(() {
       //   ratingValue =ratingValue ;
          isClicked = true;
        });
      });
  }
  void _showSettingsBottomSheet() {
//    setState(() {
//      _showSettingsBottomSheetCallBack = null;
//
//    });

    _scaffoldKey.currentState
        .showBottomSheet((context) {
      var _mediaQueryData = MediaQuery.of(context);
      double screenWidth = _mediaQueryData.size.width;
      double screenHeight = _mediaQueryData.size.height;
      return   Container(
        height: (170 / Constant.defaultScreenHeight) *
            screenHeight,
        decoration: new BoxDecoration(
          boxShadow: [
            BoxShadow(
              color: Colors.black38,
              offset: Offset(0, 0),
              blurRadius: 15,
              spreadRadius: 2,
            ),
          ],
          borderRadius: BorderRadius.only(
            topRight: Radius.circular(20), topLeft: Radius.circular(20),) ,
          color: NeutralColors.pureWhite,
        ),
        //color: Colors.black,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Center(
              child: new RichText(
                text:TextSpan(
                  children: [
                    new TextSpan(
                      text:"To stream videos on cellular data,",
                      style: TextStyle(
                        color: Colors.black,
                        fontWeight: FontWeight.w500,
                        fontFamily: "IBMPlexSans",
                        fontStyle: FontStyle.normal,
                        fontSize: (16/defaultScreenWidth)*screenWidth,

                      ),
                    ),
                  ],
                ),
              ),
            ),
            GestureDetector(
              onTap: (){
                //AppRoutes.push(context, Settings());
                  },
              child: Container(
                margin: EdgeInsets.only(top: 5/defaultScreenHeight*screenHeight ),
                child: Center(
                  child: new RichText(
                    text:TextSpan(
                      children: [
                        new TextSpan(
                          text:"change settings ",
                          style: TextStyle(
                            color: Colors.black,
                            fontWeight: FontWeight.w500,
                            fontFamily: "IBMPlexSans",
                            fontStyle: FontStyle.normal,
                            fontSize: (16/defaultScreenWidth)*screenWidth,

                          ),
                        ),
                         TextSpan(
                          text: "here",
                          style: TextStyle(
                            color: Colors.blue,
                            fontWeight: FontWeight.w500,
                            fontFamily: "IBMPlexSans",
                            fontStyle: FontStyle.normal,
                            fontSize: (16/defaultScreenWidth)*screenWidth,
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      );
    })
        .closed
        .whenComplete(() {
      if (mounted) {
        setState(() {
          _showSettingsBottomSheetCallBack = _showSettingsBottomSheet;
        });
      }
    });
  }

  @override
  void initState() {
    super.initState();
    _scroll = new ScrollController();

    _showPersBottomSheetCallBack = _showBottomSheet;
    _showSettingsBottomSheetCallBack = _showSettingsBottomSheet;
    _tabController = new TabController(vsync: this, length: tabList.length);
    _index = 0;
    tabList.add(new Tab(
      child: Text(
        "Queries",
        style: TextStyle (
          fontWeight: FontWeight.w100,
          fontFamily: "IBMPlexSans",
          fontStyle: FontStyle.normal,
          fontSize: 14,
        ),
        textAlign: TextAlign.left,
      ),
    ));
    tabList.add(new Tab(
      child: Text(
        "Up Next",
          style: TextStyle (
          fontWeight: FontWeight.w100,
          fontFamily: "IBMPlexSans",
          fontStyle: FontStyle.normal,
          fontSize: 14,
        ),
        textAlign: TextAlign.left,
      ),
    ));
    tabList.add(new Tab(
      child: Text(
        "Details",
        style: TextStyle (
          fontWeight: FontWeight.w100,
          fontFamily: "IBMPlexSans",
          fontStyle: FontStyle.normal,
          fontSize: 14,
        ),
        textAlign: TextAlign.left,
      ),
    ));
    setState(() {
      _tabController = new TabController(vsync: this, length: tabList.length);
      _tabController.addListener(_handleTabSelection);

      this._index = _index;


      print(_index);
    });
  }
  @override
  Widget build(BuildContext context) {
    var _mediaQueryData = MediaQuery.of(context);
    double screenWidth = _mediaQueryData.size.width;
    double screenHeight = _mediaQueryData.size.height;
    print("screenHeightscreenHeightscreenHeight"+screenHeight.toString());
    double selectedvalue = 0;

    return Scaffold(
      
       key: _scaffoldKey,
     appBar: AppBar(
       backgroundColor: Colors.white,
       elevation: 0.0,
       titleSpacing: 0.0,
       brightness: Brightness.light,
       centerTitle: false,
       iconTheme: IconThemeData(
              color: Colors.black,
            ),
       leading: IconButton(
          icon: getSvgIcon.backSvgIcon,
            onPressed: () => Navigator.pop(context),
          ),
       title: Text(
         "",
         style: TextStyle(
           fontSize: (16 / Constant.defaultScreenWidth) * screenWidth,
           fontFamily: "IBMPlexSans",
           fontWeight: FontWeight.w500,
           color: Colors.black,
         ),
         textAlign: TextAlign.left,
       ),
     ),
      body: Container(
        child: Column(
          children: <Widget>[
            /**For video container**/
            SingleChildScrollView(
              child: Column(
                children: <Widget>[
                  Container(
                    color: Colors.white,
                    child: Container(
                      decoration:  BoxDecoration(
                          borderRadius: new BorderRadius.circular(8.0/360*screenWidth)),
                      margin: EdgeInsets.only(
                          top: (0 / Constant.defaultScreenWidth) * screenWidth),
                      child: _getChewiePlayer(false,context), // Hide snd Show widgets
                    ),
                  ),
                  Container(
                    color: Colors.white,
                    child: Column(
                      children: <Widget>[
                        Container(
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Container(
                                decoration: new BoxDecoration(
                                  borderRadius: new BorderRadius.circular(2.0),
                                  color: NeutralColors.purpleish_blue.withOpacity(0.10),
                                ),
                                margin: EdgeInsets.only(
                                    top: 10 / 720 * screenHeight,
                                    left: 21 / 360 * screenWidth),
                                child: Padding(
                                  padding:  EdgeInsets.only(left: (5/Constant.defaultScreenWidth)*screenWidth,
                                    right: (5/Constant.defaultScreenWidth)*screenWidth,
                                    top: 1 / 720 * screenHeight,
                                    bottom: 1 / 720 * screenHeight,),
                                  child: Text(
                                    Quick_tipsScreenStrings.Text_strategy,
                                    style: TextStyle(
                                      color: Color.fromARGB(255, 86, 72, 235),
                                      fontSize: 12 / 360 * screenWidth,
                                      fontFamily: "IBMPlexSans",
                                      fontWeight: FontWeight.w500,
                                    ),
                                    textAlign: TextAlign.center,
                                  ),
                                ),
                              ),
                              Container(
                                margin: EdgeInsets.only(right:20 / 360 * screenWidth ),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    Container(
                                      margin: EdgeInsets.only(
                                          top: 10 / 720 * screenHeight,
                                          left: 0 / 360 * screenWidth),
                                      child: SvgPicture.asset(
                                        ChannelAssets.downloadicon,
                                        fit: BoxFit.none,
                                      ),
                                    ),
                                    InkWell(
                                      onTap: (){
                                        _showSettingsBottomSheet();
                                      },
                                      child: Container(
                                        margin: EdgeInsets.only(
                                            left: 20 / 360 * screenWidth,
                                            top: 10 / 720 * screenHeight),
                                        child: SvgPicture.asset(
                                          ChannelAssets.bookmark,
                                          //fit: BoxFit.scaleDown,
                                        ),
                                      ),
                                    ),
                                    GestureDetector(
                                      onTap: (){
                                        _showRatingDialog();
                                        setState(() {
                                          isRatingClicked = true;
                                        });
                                      },
                                      child:isRatingClicked ?Container(): Container(
                                        margin: EdgeInsets.only(
                                            top: 10 / 720 * screenHeight,
                                            left: 20 / 360 * screenWidth),
                                        child: SvgPicture.asset(
                                          ChannelAssets.Star,
                                          fit: BoxFit.scaleDown,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                        /**For Accuracy text container**/
                        Align(
                          alignment: Alignment.topLeft,
                          child: Container(
                            // color: Colors.red,
                            margin: EdgeInsets.only(
                                top: 10 / 764 * screenHeight,
                                left: 21 / 360 * screenWidth),
                            child: Text(
                              Quick_tipsScreenStrings.Text_title,
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontFamily: "IBMPlexSans",
                                fontStyle: FontStyle.normal,
                                fontSize: (14 / 360) * screenWidth,
                                color: NeutralColors.dark_navy_blue,
                              ),
                            ),
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(
                              top: 8 / 720 * screenHeight,
                              left: 21 / 360 * screenWidth),
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              Container(
                                height: (22 / Constant.defaultScreenHeight) *
                                    screenHeight,
                                width: (22 / Constant.defaultScreenWidth) *
                                    screenWidth,
                                margin: EdgeInsets.only(
                                    top: (0 / 720) * screenHeight),
                                decoration: new BoxDecoration(
                                    shape: BoxShape.circle,
                                    image: new DecorationImage(
                                        fit: BoxFit.cover,
                                        image: AssetImage(
                                            'assets/images/oval-2.png'))),
                              ),
                              InkWell(
                                onTap: (){
                                  setState(() {
                                    bottomSheetFlag = true;
                                  });
                                  _showPersBottomSheetCallBack();
                                },
                                child: Container(
                                  margin: EdgeInsets.only(
                                      left: 8 / 360 * screenWidth,
                                      top: 0 / 720 * screenHeight),
                                  child: Text(
                                    ChannelStrings.userName,
                                    style: TextStyle(
                                      color: NeutralColors.dark_navy_blue,
                                      fontSize: 14 / 360 * screenWidth,
                                      fontFamily: "IBMPlexSans",
                                      fontWeight: FontWeight.w500,
                                    ),
                                    textAlign: TextAlign.left,
                                  ),
                                ),
                              ),
                              Container(
                                width: 2 / 360 * screenWidth,
                                height: 2 / 720 * screenHeight,
                                margin: EdgeInsets.only(
                                    left: (10 / Constant.defaultScreenWidth) *
                                        screenWidth,
                                    top: (0 / Constant.defaultScreenHeight) *
                                        screenHeight),
                                decoration: BoxDecoration(
                                  color: NeutralColors.blue_grey,
                                  borderRadius:
                                  BorderRadius.all(Radius.circular(1.5)),
                                ),
                                child: Container(),
                              ),
                              Container(
                                margin: EdgeInsets.only(
                                    top: 0 / 720 * screenHeight,
                                    left: 10 / 360 * screenWidth),
                                child: SmoothStarRating(
                                  allowHalfRating: false,
                                  starCount: 5,
                                  rating: 3,
                                  size: 15.0/360 * screenWidth,
                                  color: NeutralColors.sun_yellow,
                                  borderColor: NeutralColors.sun_yellow,
                                  spacing: 0.0,
                                ),
                              ),
                              InkWell(
                                onTap: (){
                                  print("*********************working");

                                },
                                child: Container(
                                  margin: EdgeInsets.only(
                                      left: 7 / 360 * screenWidth,
                                      top: 0 / 720 * screenHeight),
                                  child: Text(
                                    Quick_tipsScreenStrings.Text_rating,
                                    style: TextStyle(
                                      color: Color.fromARGB(255, 153, 154, 171),
                                      fontSize: 14 / 360 * screenWidth,
                                      fontFamily: "IBMPlexSans",
                                      fontWeight: FontWeight.w500,
                                    ),
                                    textAlign: TextAlign.left,
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                        isClicked ? Container(
                          child: Column(
                            children: <Widget>[
                              Container(
                                height: (1/Constant.defaultScreenHeight)*screenHeight,
                                width: double.infinity,
                                color: NeutralColors.ice_blue,
                                margin: EdgeInsets.only(top:(20/Constant.defaultScreenHeight)*screenHeight),
                              ),
                              Container(
                                margin: EdgeInsets.only(left: (20/Constant.defaultScreenWidth)*screenWidth,
                                  right: (20/Constant.defaultScreenWidth)*screenWidth,
                                  top:(20/Constant.defaultScreenHeight)*screenHeight,
                                ),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    new Text(
                                      ChannelStrings.rate_this_video,
                                      style: TextStyle(
                                        color: NeutralColors.dark_navy_blue,
                                        fontSize: (14 / Constant.defaultScreenWidth) *
                                            screenWidth,
                                        fontFamily: "IBMPlexSans",
                                        fontWeight: FontWeight.w400,
                                      ),
                                    ),
                                    Container(
                                      child: SmoothStarRating(
                                        allowHalfRating: false,
                                        starCount: 5,
                                        rating: ratingValue,
                                        onRated: (v) {
                                          ratingValue = v;
                                          setState(() {
                                            ratingValue = v;
                                            selectedvalue = v;
                                          });
                                        },
                                        size: (20/Constant.defaultScreenWidth)*screenWidth,
                                        color: NeutralColors.sun_yellow,
                                        borderColor: NeutralColors.blue_grey,
                                        spacing: 0.0,
                                      ),
                                    )
                                  ],
                                ),
                              ) ,
                              Container(
                                height: (1/Constant.defaultScreenHeight)*screenHeight,
                                width: double.infinity,
                                color: NeutralColors.ice_blue,
                                margin: EdgeInsets.only(top:(20/Constant.defaultScreenHeight)*screenHeight),
                              ),
                            ],
                          ),
                        ) :
                        Container(
                          margin: EdgeInsets.only(top: (30/Constant.defaultScreenHeight)*screenHeight),
                        ) ,
                      ],
                    ),
                  ),
                ],
              ),
            ),
            Expanded(
              child: Column(
                children: <Widget>[
                  Container(
                    color:Colors.white,
                    height:(40/Constant.defaultScreenHeight)*screenHeight,
                    child: TabBar(
                      controller: _tabController,
                      indicatorColor: NeutralColors.purpleish_blue,
                      labelColor: NeutralColors.purpleish_blue,
                      unselectedLabelColor: NeutralColors.blue_grey,
                      indicatorSize: TabBarIndicatorSize.tab,
                      tabs: tabList,
                      indicator:  UnderlineTabIndicator(
                        borderSide: BorderSide(width: 2.0,color: NeutralColors.purpleish_blue ),
                        insets: EdgeInsets.symmetric(horizontal:20.0),

                      ),
                    ),
                  ),
                  Expanded(
                    child: TabBarView(
                      controller: _tabController,
                      children: [
                        ChannelComments(),
                        ChannelComments(),
                        ChannelComments()
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
_getChewiePlayer(bool offStageFlag, BuildContext context){
  var _mediaQueryData = MediaQuery.of(context);
  double screenWidth = _mediaQueryData.size.width;
  double screenHeight = _mediaQueryData.size.height;
  Offstage offstage = new Offstage(
    offstage: offStageFlag,
    child: Container(
      decoration:  BoxDecoration(
          borderRadius: new BorderRadius.circular(8.0/360*screenWidth)),
      margin: EdgeInsets.only(
          left: (20 / 360) * screenWidth, right: (20 / 360) * screenWidth),
      height: 181 / 720 * screenHeight,
      child: InkWell(
        onTap: () {
         // AppRoutes.push(context,VideoPlayerApp());
        },
        child: Container(

          constraints: new BoxConstraints.expand(
            height: (181 / 720) * screenHeight,

          ),
          alignment: Alignment.bottomLeft,
          padding: new EdgeInsets.only(left: 16.0, bottom: 8.0),
          decoration:  BoxDecoration(
            borderRadius: new BorderRadius.circular(8.0/360*screenWidth),
            image: DecorationImage(
                alignment: Alignment(-.2, 0),
                image: CachedNetworkImageProvider(
                    'http://d1cn01kw45hc6h.cloudfront.net/wp-content/uploads/2019/08/22161210/Quadtratic.png'),
//                  image: NetworkImage(
//                      'http://d1cn01kw45hc6h.cloudfront.net/wp-content/uploads/2019/08/22161210/Quadtratic.png'
//                  ),
                fit: BoxFit.cover),
          ),
          child: Center(
            child: Container(
              height: (50 / 720) * screenHeight,
              width: (50 / 360) * screenWidth,
              child: SvgPicture.asset(
                getPrepareSvgImages.playwhite,
                fit: BoxFit.fitHeight,
              ),
            ),
          ),
        ),
      ),
    ),
  );
  return offstage;
}
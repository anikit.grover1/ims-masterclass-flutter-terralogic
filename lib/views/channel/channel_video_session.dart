import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:imsindia/components/video_player.dart';
import 'package:imsindia/routers/routes.dart';
import 'package:imsindia/utils/colors.dart';
import 'package:imsindia/utils/screen_util.dart';
import 'package:imsindia/utils/svg_images/channel_svg_icons.dart';
import 'package:imsindia/utils/svg_images/prepare_svg_images.dart';
import 'package:imsindia/views/channel/channel_quickTips.dart';
import 'package:imsindia/views/channel/channel_quick_tips.dart';
import 'package:smooth_star_rating/smooth_star_rating.dart';
import 'package:video_player/video_player.dart';

var screenWidthTotal;
var screenHeightTotal;

var _safeAreaHorizontal;

var _safeAreaVertical;

var screenHeight;

var screenWidth;
var courseID;

class TestData {
  String title;
  String name;
  String Handout;
  String Bookmark;
  String ratings;
  List<String> marks;

  TestData({
    this.title,
    this.name,
    this.Handout,
    this.Bookmark,
    this.ratings,
  });
}

class Chanel_Video_Session extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => Chanel_Video_SessionState();
}

class Chanel_Video_SessionState extends State<Chanel_Video_Session> {

  bool bookmarkSelected = false;
  List<dynamic> bookmark = [];
  @override
  Widget build(BuildContext context) {
    var _mediaQueryData = MediaQuery.of(context);
    screenWidthTotal = _mediaQueryData.size.width;
    screenHeightTotal = _mediaQueryData.size.height;
    _safeAreaHorizontal =
        _mediaQueryData.padding.left + _mediaQueryData.padding.right;
    _safeAreaVertical =
        _mediaQueryData.padding.top + _mediaQueryData.padding.bottom;

    screenHeight = screenHeightTotal - _safeAreaVertical;
    screenWidth = screenWidthTotal - _safeAreaHorizontal;

    final test = TestData(
      title: "Quick tips for CAT preperation",
      name: "Stephen Grider",
      Handout: "Handout",
      Bookmark: "Bookmark",
      ratings: "18 ratings",
    );
    final test1 = TestData(
      title: "Quick tips for CAT preperation",
      name: "Stephen Grider",
      Handout: "Handout",
      Bookmark: "Bookmark",
      ratings: "18 ratings",
    );
    final test2 = TestData(
      title: "Quick tips for CAT preperation",
      name: "Stephen Grider",
      Handout: "Handout",
      Bookmark: "Bookmark",
      ratings: "18 ratings",
    );

    final List<TestData> tests = [
      test,
      test1,
      test2,
    ];

    return Scaffold(
      body: Container(
        child: Column(
          children: <Widget>[
            new Expanded(
              child: ListView.builder(
                  addRepaintBoundaries: true,
                  addAutomaticKeepAlives: true,
                  itemCount: tests.length,
                  itemBuilder: (build, index) {
                    final item = tests[index];
                    return Container(
                      //margin: EdgeInsets.only(left: 20/360*screenWidth),
                      child: Column(
                        children: <Widget>[
                          Container(
                            decoration:  BoxDecoration(
                                borderRadius: new BorderRadius.circular(8.0/360*screenWidth)),
                            margin:
                                EdgeInsets.only(top: 24.5 / 720 * screenHeight),
                            //   margin:EdgeInsets.only(top:30/720*screenHeight,left:21/360*screenWidth,right:19/360*screenWidth),
                            child: _getChewiePlayer(
                                false, context), // Hide snd Show widgets
                          ),
                          Container(
//                            height:100/720*screenHeight,
//                            width: 320/360*screenWidth,
                           // color:Colors.green,
                            margin: EdgeInsets.only(
                                right: 20 / 360 * screenWidth,
                                left: 20 / 360 * screenWidth,
                                bottom: 24.5 / 720 * screenHeight,
                                top: 12.9 / 720 * screenHeight),
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Container(
                                  height: (35 / Constant.defaultScreenHeight) *
                                      screenHeight,
                                  width: (35 / Constant.defaultScreenWidth) *
                                      screenWidth,
//                                  margin: EdgeInsets.only(
//                                      bottom: ( 10/ 720) * screenHeight),
                                  alignment: Alignment.topLeft,
                                  decoration: new BoxDecoration(
                                      shape: BoxShape.circle,
                                      image: new DecorationImage(
                                          fit: BoxFit.cover,
                                          image: AssetImage(
                                              'assets/images/oval-2.png'))),
                                ),
                                Container(
                                  //height: 18/720*screenHeight,
                                  margin: EdgeInsets.only(
                                      left: 10 / 360 * screenWidth),
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: <Widget>[
                                      Container(
                                        //color:Colors.red,
                                        //margin:EdgeInsets.only(top: 10/720*screenHeight,right: 19/360*screenWidth),
                                        child: Text(
                                          item.title,
                                          style: TextStyle(
                                            color: NeutralColors.dark_navy_blue,
                                            fontSize: 14 / 360 * screenWidth,
                                            fontFamily: "IBMPlexSans",
                                            fontWeight: FontWeight.w500,
                                          ),
                                          textAlign: TextAlign.left,
                                        ),
                                      ),
                                      Container(
                                        //height: 15/720*screenHeight,
                                        margin: EdgeInsets.only(
                                            top: 5 / 720 * screenHeight),
                                        child: Row(
                                          // crossAxisAlignment: CrossAxisAlignment.start,
                                          children: <Widget>[
                                            Container(
                                              // margin:EdgeInsets.only(left:6/360*screenWidth),
                                              child: Text(
                                                item.name,
                                                style: TextStyle(
                                                  color:
                                                      NeutralColors.blue_grey,
                                                  fontSize:
                                                      12 / 360 * screenWidth,
                                                  fontFamily: "IBMPlexSans",
                                                  fontWeight: FontWeight.w500,
                                                ),
                                                textAlign: TextAlign.left,
                                              ),
                                            ),
                                            Container(
                                              width: 2 / 360 * screenWidth,
                                              height: 2 / 720 * screenHeight,
                                              margin: EdgeInsets.only(
                                                  left: 6 / 360 * screenWidth,
                                                  top: 4 / 720 * screenHeight),
                                              decoration: BoxDecoration(
                                                color: Color.fromARGB(
                                                    255, 153, 154, 171),
                                                borderRadius: BorderRadius.all(
                                                    Radius.circular(1.5)),
                                              ),
                                              child: Container(),
                                            ),
                                            Container(
                                              margin: EdgeInsets.only(
                                                  left: 7 / 360 * screenWidth),
                                              child: SmoothStarRating(
                                                allowHalfRating: false,
                                                starCount: 5,
                                                rating: 3,
                                                size: 13.0,
                                                color: NeutralColors.sun_yellow,
                                                borderColor:
                                                    NeutralColors.sun_yellow,
                                                spacing: 0.0,
                                              ),
                                            ),

                                            //
//                                            Container(
//                                              width: 8/360*screenWidth,
//                                              height: 8/720*screenHeight,
//                                              margin: EdgeInsets.only(top: 4/720*screenHeight,left: 6/360*screenWidth),
//                                              child:SvgPicture.asset(
//                                                ChannelAssets.filledStar,
//                                                fit: BoxFit.none,
//                                              ),
//                                            ),
//                                            Container(
//                                              width: 8/360*screenWidth,
//                                              height: 8/720*screenHeight,
//                                              margin: EdgeInsets.only(top: 4/720*screenHeight,left:3/360*screenWidth),
//                                              child: SvgPicture.asset(
//                                                ChannelAssets.filledStar,
//                                                fit: BoxFit.none,
//                                              ),
//                                            ),
//                                            Container(
//                                              width: 8/360*screenWidth,
//                                              height: 8/720*screenHeight,
//                                              margin: EdgeInsets.only(top: 4/720*screenHeight,left: 3/360*screenWidth),
//                                              child: SvgPicture.asset(
//                                                ChannelAssets.filledStar,
//                                                fit: BoxFit.none,
//                                              ),
//                                            ),
//                                            Container(
//                                              width: 8/360*screenWidth,
//                                              height: 8/720*screenHeight,
//                                              margin: EdgeInsets.only(top: 4/720*screenHeight,left:3/360*screenWidth),
//                                              child: SvgPicture.asset(
//                                                ChannelAssets.emptyStar,
//                                                fit: BoxFit.none,
//                                              ),
//                                            ),
//                                            Container(
//                                              width: 8/360*screenWidth,
//                                              height: 8/720*screenHeight,
//                                              margin: EdgeInsets.only(top: 4/720*screenHeight,left: 3/360*screenWidth),
//                                              child: SvgPicture.asset(
//                                                ChannelAssets.emptyStar,
//                                                fit: BoxFit.none,
//                                              ),
//                                            ),
                                            //
                                            Container(
                                              margin: EdgeInsets.only(
                                                  left: 5 / 360 * screenWidth),
                                              child: Text(
                                                item.ratings,
                                                style: TextStyle(
                                                  color:
                                                      NeutralColors.blue_grey,
                                                  fontSize:
                                                      12 / 360 * screenWidth,
                                                  fontFamily: "IBMPlexSans",
                                                  fontWeight: FontWeight.w500,
                                                ),
                                                textAlign: TextAlign.left,
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                      Container(
//                                        color:Colors.green,
//                                        margin: EdgeInsets.only(
//                                            top: 5 / 720 * screenHeight),
                                        child: Row(
                                          children: <Widget>[
                                            Container(
                                              margin: EdgeInsets.only(
                                                  top: 5 / 720 * screenHeight),
                                              // width: 12/360*screenWidth,
                                              // height: 12/720*screenHeight,
                                              //margin: EdgeInsets.only(top: 2/720*screenHeight,right: 5/360*screenWidth),
                                              child: SvgPicture.asset(
                                                ChannelAssets.downloadicon,
                                                fit: BoxFit.fill,
                                              ),
                                            ),
                                            Container(
                                              margin: EdgeInsets.only(
                                                  left: 5 / 360 * screenWidth,top: 5 / 720 * screenHeight),
                                              child: Text(
                                                item.Handout,
                                                style: TextStyle(
                                                  color:
                                                      PrimaryColors.azure_Dark,
                                                  fontSize:
                                                      12 / 360 * screenWidth,
                                                  fontFamily: "IBMPlexSans",
                                                  fontWeight: FontWeight.w500,
                                                ),
                                                textAlign: TextAlign.center,
                                              ),
                                            ),
                                            GestureDetector(
                                              onTap: (){
                                                setState(() {
                                                  print(index);
                                                  if(bookmark.contains(index)){
                                                    bookmark.remove(index);
                                                  }
                                                  else{
                                                    bookmark.add(index);
                                                  }
                                                  bookmarkSelected = !bookmarkSelected;
                                                });
                                              },
                                              child: Container(
                                                // width: 9/360*screenWidth,
                                                // height: 11/720*screenHeight,
                                                margin: EdgeInsets.only(
                                                    left: 20 / 360 * screenWidth,top: 5 / 720 * screenHeight),

                                                  child: SvgPicture.asset(
                                                    bookmark.contains(index) ? ChannelAssets.bookmarkselected
                                                        :ChannelAssets.bookmarkblue,
                                                    fit: BoxFit.fill,
                                                  )
                                              ),
                                            ),
                                            Container(
                                              margin: EdgeInsets.only(
                                                  left: 4 / 360 * screenWidth,top: 5 / 720 * screenHeight),
                                              child: Text(
                                                item.Bookmark,
                                                style: TextStyle(
                                                  color:
                                                      PrimaryColors.azure_Dark,
                                                  fontSize:
                                                      12 / 360 * screenWidth,
                                                  fontFamily: "IBMPlexSans",
                                                  fontWeight: FontWeight.w500,
                                                ),
                                                textAlign: TextAlign.center,
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Container(
                            //   color: Colors.blue,
                            color: AccentColors.iceBlue,
                            width: double.infinity,
                            height: 1 / 720 * screenHeight,
                            margin: EdgeInsets.only(
                                left: 20 / 360 * screenWidth,
                                right: 20 / 360 * screenWidth,
                                top: 0 / 720 * screenHeight),
                          ),
                        ],
                      ),
                    );
                  }),
            ),
          ],
        ),
      ),
    );
  }
}

_getStackpage(bool offStageFlag, BuildContext context) {
  Offstage offstage = new Offstage(
      offstage: offStageFlag,
      child: ChewieListItem(
        videoPlayerController: VideoPlayerController.network(
          'https://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4',
        ),
      ));
  return offstage;
}

_getChewiePlayer(bool offStageFlag, BuildContext context){
  Offstage offstage = new Offstage(
    offstage: offStageFlag,
    child: Container(
      decoration:  BoxDecoration(
          borderRadius: new BorderRadius.circular(8.0/360*screenWidth)),
      margin: EdgeInsets.only(
          left: (20 / 360) * screenWidth, right: (20 / 360) * screenWidth),
      height: 181 / 720 * screenHeight,
      child: InkWell(
        onTap: () {
          AppRoutes.push(context,ChannelVideosScreens());
        },
        child: Container(

          constraints: new BoxConstraints.expand(
            height: (181 / 720) * screenHeight,

          ),
          alignment: Alignment.bottomLeft,
          padding: new EdgeInsets.only(left: 16.0, bottom: 8.0),
          decoration:  BoxDecoration(
            borderRadius: new BorderRadius.circular(8.0/360*screenWidth),
            image: DecorationImage(
                alignment: Alignment(-.2, 0),
                image: CachedNetworkImageProvider(
                    'http://d1cn01kw45hc6h.cloudfront.net/wp-content/uploads/2019/08/22161210/Quadtratic.png'),
//                  image: NetworkImage(
//                      'http://d1cn01kw45hc6h.cloudfront.net/wp-content/uploads/2019/08/22161210/Quadtratic.png'
//                  ),
                fit: BoxFit.cover),
          ),
          child: Center(
            child: Container(
              height: (50 / 720) * screenHeight,
              width: (50 / 360) * screenWidth,
              child: SvgPicture.asset(
                getPrepareSvgImages.playwhite,
                fit: BoxFit.fitHeight,
              ),
            ),
          ),
        ),
      ),
    ),
  );
  return offstage;
}

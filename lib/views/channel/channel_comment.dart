import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:imsindia/utils/colors.dart';
import 'package:imsindia/utils/svg_images/channel_svg_icons.dart';
import 'package:smooth_star_rating/smooth_star_rating.dart';

class demo extends StatefulWidget{
  @override
  State<StatefulWidget> createState() =>demoState();

}
class demoState extends State<demo>{
  @override
  Widget build(BuildContext context) {
    final screenHeight = MediaQuery.of(context).size.height;
    final screenWidth = MediaQuery.of(context).size.width;

    return Scaffold(
      body: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(

              //color: Colors.red,
              margin: EdgeInsets.only(top: 50/720*screenHeight,left: 20/360*screenWidth),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Container(
                    height: 35 / 720 * screenHeight,
                    width: 35 / 360 *  screenWidth,
                    alignment: Alignment.topLeft,
                    decoration: new BoxDecoration(
                        shape: BoxShape.circle,
                        image: new DecorationImage(
                            fit: BoxFit.cover,
                            image: AssetImage(
                                'assets/images/oval-2.png'))),
                  ),
                  Container(
                    margin: EdgeInsets.only(left: 10/360*screenWidth),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Container(
                          //color:Colors.red,

                          margin:EdgeInsets.only(top: 00/720*screenHeight),
                          child: Text(
                            "Quick tips for CAT preperation",
                            style: TextStyle(
                              color: NeutralColors.dark_navy_blue,
                              fontSize: 14/360*screenWidth,
                              fontFamily: "IBMPlexSans",
                              fontWeight: FontWeight.w500,
                            ),
                            textAlign: TextAlign.left,
                          ),
                        ),
                        Container(
                          //color: Colors.cyan,
                          //  height: 15/720*screenHeight,
                          margin: EdgeInsets.only(top: 5/720*screenHeight
                          ),
                          child: Row(
                            children: <Widget>[
                              Container(

                                // margin:EdgeInsets.only(bottom:1/360*screenWidth),
                                child:Text(
                                  "Stephen Grider",
                                  style: TextStyle(
                                    color: NeutralColors.blue_grey,
                                    fontSize: 12/360*screenWidth,
                                    fontFamily: "IBMPlexSans",
                                    fontWeight: FontWeight.w500,
                                  ),
                                  textAlign: TextAlign.left,
                                ),
                              ),
                              Container(
                                width: 3 / 360 * screenWidth,
                                height: 3 / 720 * screenHeight,
                                margin: EdgeInsets.only(
                                    left: 8/360*screenWidth,top: 0/720*screenHeight),
                                decoration: BoxDecoration(
                                  color: Color.fromARGB(
                                      255, 153, 154, 171),
                                  borderRadius:
                                  BorderRadius.all(
                                      Radius.circular(
                                          1.5)),
                                ),
                                child: Container(),
                              ),
                              Container(
                                margin: EdgeInsets.only(left: 7/360*screenWidth),
                                child: SmoothStarRating(
                                  allowHalfRating: false,
                                  starCount: 5,
                                  rating: 3,
                                  size: 15.0,
                                  color: NeutralColors.sun_yellow,
                                  borderColor: NeutralColors.sun_yellow,
                                  spacing: 0.0,
                                ),
                              ),

                              Container(
                                margin: EdgeInsets.only(left: 5/360*screenWidth),
                                child: Text(
                                  "18 rating",
                                  style: TextStyle(
                                    color: NeutralColors.blue_grey,
                                    fontSize: 12/360*screenWidth,
                                    fontFamily: "IBMPlexSans",
                                    fontWeight: FontWeight.w500,
                                  ),
                                  textAlign: TextAlign.left,
                                ),
                              ),


                            ],
                          ),
                        ),
                        Container(
                          //color: Colors.lightGreen,
//                          width: 183 / 360 * screenWidth,
                         // height: 15/ 720 * screenHeight,
                          margin: EdgeInsets.only(top: 5 / 720 *screenHeight),
                          child: Row(
                           // crossAxisAlignment:CrossAxisAlignment.stretch,
                            children: [
                              Container(
                                child: Row(
                                  //crossAxisAlignment:CrossAxisAlignment.stretch,
                                  children: [
                                    Container(
                                      margin: EdgeInsets.only(
                                          top: 0 /720 * screenHeight),
                                      child:
                                      ChannelAssets.downloadSvgIcon,
                                    ),
                                    Container(
                                      margin: EdgeInsets.only(
                                          left: 5 /360*
                                              screenWidth),
                                      child: Text(
                                        "Handout",
                                        style: TextStyle(
                                          color: NeutralColors.blue_grey,
                                          fontSize: 12 / 360 *
                                              screenWidth,
                                          fontFamily: "IBMPlexSans",
                                          fontWeight: FontWeight.w500,
                                        ),
                                        textAlign: TextAlign.left,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              Container(
//                                                height: (15 /
//                                                        Constant
//                                                            .defaultScreenHeight) *
//                                                    screenHeight,
                                margin: EdgeInsets.only(
                                    left: 20 / 360 * screenWidth),
                                child:
                                ChannelAssets.bookmarkblueSvgIcon,
                              ),
                              Container(
                                margin: EdgeInsets.only(
                                    left: 4 /360 * screenWidth),
                                child: Text(
                                  "Bookmark",
                                  style: TextStyle(
                                    color: Color.fromARGB(
                                        255, 0, 171, 251),
                                    fontSize: 12 / 360 *screenWidth,
                                    fontFamily: "IBMPlexSans",
                                    fontWeight: FontWeight.w500,
                                  ),
                                  textAlign: TextAlign.center,
                                ),
                              ),
                            ],
                          ),
                        ),

                      ],
                    ),
                  ),

                ],
              ),

            ),



            Container(
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Container(
                    width: 60 /360 * screenWidth,
//                    height: 50 / 720 *screenHeight,
                    margin: EdgeInsets.only(left: 20/360*screenWidth),
                    decoration: BoxDecoration(
                    border: Border.all(
                      color: NeutralColors.purpleish_blue.withOpacity(0.10),
                      // color: Colors.cyan,
                      width: (1 / 360) * screenWidth,
                    ),
                    borderRadius: BorderRadius.all(Radius.circular(3)),
                  ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: <Widget>[
                        Container(
                          //color:Colors.blue,
                          child: Text(
                            "27",
                            style: TextStyle(
                              color: Color.fromARGB(255, 71, 89, 147),
                              fontSize:18 / 360 * screenWidth,
                              fontFamily: "IBMPlexSans",
                              fontWeight: FontWeight.w700,
                            ),
                            textAlign: TextAlign.center,
                          ),
                        ),
                         Container(
                          //height :21.5/720*screenHeight,
                          //width: 60/360*screenWidth,
                          decoration: BoxDecoration(
                            gradient: LinearGradient(
                                begin: Alignment(0, 0),
                                end: Alignment(1.0199999809265137, 1.0099999904632568),
                                colors:[const Color(0xff3380cc).withOpacity(0.10),
                                  NeutralColors.purpley.withOpacity(0.10)

                                ]
                            ),

                          ),

                          child: Text(
                            "Aug 2018",
                            style: TextStyle(
                              color: Color.fromARGB(255, 0, 3, 44),
                              fontSize:
                              12 / 720 *
                                  screenHeight,
                              fontFamily: "IBMPlexSans",
                            ),
                            textAlign: TextAlign.center,
                          ),
                        ),

                      ],
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(left: 12/360*screenWidth),
                    child: Column(
                      crossAxisAlignment:CrossAxisAlignment.start,
                      children: <Widget>[
                        Container(
//                                        color:Colors.green,
                                       width: 78 / 360 * screenWidth,
//                                        height: 40 /720 * screenHeight,

                          decoration: BoxDecoration(
                            color:Colors.blue,
                            borderRadius: BorderRadius.all(
                                Radius.circular(2)),
                          ),

                          child: Text(
                            "Application",
                            style: TextStyle(
                              color: Color.fromARGB(
                                  255, 255, 255, 255),
                              fontSize: 12 /360 *screenWidth,
                              fontFamily: "IBMPlexSans",
                            ),
                            textAlign: TextAlign.center,
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(
//                              left: 72 /360 * screenWidth,
                              top: 4 /720 * screenHeight),
                          child: Text(
                            "Tips for CAT online exams",
                            style: TextStyle(
                              color: Color.fromARGB(255, 0, 3, 44),
                              fontSize: 14 / 360 * screenWidth,
                              fontFamily: "IBMPlexSans",
                              fontWeight: FontWeight.w500,
                            ),
                            textAlign: TextAlign.left,
                          ),
                        ),
                        Container(
                          //  height: 15/720*screenHeight,
                          margin: EdgeInsets.only(top: 5/720*screenHeight),
                          child: Row(
                            children: <Widget>[
                              Container(

                                // margin:EdgeInsets.only(bottom:1/360*screenWidth),
                                child:Text(
                                  "Stephen Grider",
                                  style: TextStyle(
                                    color: NeutralColors.blue_grey,
                                    fontSize: 12/360*screenWidth,
                                    fontFamily: "IBMPlexSans",
                                    fontWeight: FontWeight.w500,
                                  ),
                                  textAlign: TextAlign.left,
                                ),
                              ),
                              Container(
                                width: 3 / 360 * screenWidth,
                                height: 3 / 720 * screenHeight,
                                margin: EdgeInsets.only(
                                    left: 8/360*screenWidth,top: 2/720*screenHeight),
                                decoration: BoxDecoration(
                                  color: Color.fromARGB(
                                      255, 153, 154, 171),
                                  borderRadius:
                                  BorderRadius.all(
                                      Radius.circular(
                                          1.5)),
                                ),
                                child: Container(),
                              ),

                              Container(
                                margin: EdgeInsets.only(left: 7/360*screenWidth),
                                child: Text(
                                  "12:30 AM - 01:15 PM",
                                  style: TextStyle(
                                    color: NeutralColors.blue_grey,
                                    fontSize: 12/360*screenWidth,
                                    fontFamily: "IBMPlexSans",
                                    fontWeight: FontWeight.w500,
                                  ),
                                  textAlign: TextAlign.left,
                                ),
                              ),


                            ],
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(top: 5/720*screenHeight),
                          child: Row(
                            children: <Widget>[
                              Container(
                                // width: 12/360*screenWidth,
                                // height: 12/720*screenHeight,
                                //margin: EdgeInsets.only(top: 2/720*screenHeight,right: 5/360*screenWidth),
                                child: SvgPicture.asset(
                                  ChannelAssets.downloadicon,
                                  fit: BoxFit.fill,
                                ),
                              ),
                              Container(
                                margin: EdgeInsets.only(
                                    left: 5/360*screenWidth),
                                child: Text(
                                 "Set Reminder",
                                  style: TextStyle(
                                    color: PrimaryColors.azure_Dark,
                                    fontSize: 12/360*screenWidth,
                                    fontFamily: "IBMPlexSans",
                                    fontWeight: FontWeight.w500,
                                  ),
                                  textAlign: TextAlign.center,
                                ),
                              ),
                              Container(
                                // width: 9/360*screenWidth,
                                // height: 11/720*screenHeight,
                                margin: EdgeInsets.only(left: 20/360*screenWidth),
                                child: SvgPicture.asset(
                                  ChannelAssets.bookmarkblue,
                                  fit: BoxFit.fill,
                                ),
                              ),
                              Container(
                                margin: EdgeInsets.only(left: 4/360*screenWidth),
                                child: Text(
                                  "Handout",
                                  style: TextStyle(
                                    color: PrimaryColors.azure_Dark,
                                    fontSize: 12/360*screenWidth,
                                    fontFamily: "IBMPlexSans",
                                    fontWeight: FontWeight.w500,
                                  ),
                                  textAlign: TextAlign.center,
                                ),
                              ),


                            ],
                          ),
                        ),


                      ],
                    ),

                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

}




//Container(
////color: Colors.red,
////  width: (325 / Constant.defaultScreenWidth) * screenWidth,
////  height: (90 / Constant.defaultScreenHeight) * screenHeight,
//margin: EdgeInsets.only(
//left: (0 / Constant.defaultScreenWidth) * screenWidth,
//top: (24.5 / Constant.defaultScreenHeight) * screenHeight,
//bottom:
//(24.5 / Constant.defaultScreenHeight) * screenHeight),
//child: Stack(
//children: [
//Container(
//child: Column(
//crossAxisAlignment: CrossAxisAlignment.start,
//children: [
//Container(
//color: Colors.red,
//width: (78 / Constant.defaultScreenWidth) *
//screenWidth,
//height: (17 / Constant.defaultScreenHeight) *
//screenHeight,
//margin: EdgeInsets.only(
//left: (72 / Constant.defaultScreenWidth) *
//screenWidth),
//child: Stack(
//alignment: Alignment.center,
//children: [
//Container(
////                                        color:Colors.green,
////                                        width:
////                                            (78 / Constant.defaultScreenWidth) *
////                                                screenWidth,
////                                        height:
////                                            (40 / Constant.defaultScreenHeight) *
////                                                screenHeight,
//
//decoration: BoxDecoration(
//color:Colors.blue,
//borderRadius: BorderRadius.all(
//Radius.circular(2)),
//),
//child: Container(),
//),
//Positioned(
//left: (5 / Constant.defaultScreenWidth) *
//screenWidth,
//child: InkWell(
//onTap: () {
//print(list[index]);
//},
//child: Text(
//list[index],
//style: TextStyle(
//color: Color.fromARGB(
//255, 255, 255, 255),
//fontSize: (12 /
//Constant
//    .defaultScreenWidth) *
//screenWidth,
//fontFamily: "IBMPlexSans",
//),
//textAlign: TextAlign.center,
//),
//),
//),
//],
//),
//),
//Container(
//margin: EdgeInsets.only(
//left: (72 / Constant.defaultScreenWidth) *
//screenWidth,
//top: (4 / Constant.defaultScreenHeight) *
//screenHeight),
//child: Text(
//"Tips for CAT online exams",
//style: TextStyle(
//color: Color.fromARGB(255, 0, 3, 44),
//fontSize:
//(14 / Constant.defaultScreenWidth) *
//screenWidth,
//fontFamily: "IBMPlexSans",
//fontWeight: FontWeight.w500,
//),
//textAlign: TextAlign.left,
//),
//),
//Container(
//color:Colors.pink,
//margin: EdgeInsets.only(
//left: (72 / Constant.defaultScreenWidth) *
//screenWidth,
//top: (5 / Constant.defaultScreenHeight) *
//screenHeight,
//// bottom:(10/Constant.defaultScreenHeight)*screenHeight
//),
//child: Stack(
//alignment: Alignment.center,
//children: [
//Positioned(
//left: (90 / Constant.defaultScreenWidth) *
//screenWidth,
////                                      top: (8 / Constant.defaultScreenHeight) *
////                                          screenHeight,
//child: Container(
//color:Colors.blue,
//width:
//(2 / Constant.defaultScreenWidth) *
//screenWidth,
//height:
//(2 / Constant.defaultScreenHeight) *
//screenHeight,
////                          decoration: BoxDecoration(
////                            color: Color.fromARGB(255, 153, 154, 171),
////                            borderRadius: BorderRadius.all(Radius.circular(1)),
////                          ),
////                          child: Container(),
//),
//),
//Row(
//crossAxisAlignment: CrossAxisAlignment.center,
//children: <Widget>[
//Text(
//"Stephen Grider",
//style: TextStyle(
//color: Color.fromARGB(
//255, 153, 154, 171),
//fontSize: (12 /
//Constant
//    .defaultScreenWidth) *
//screenWidth,
//fontFamily: "IBMPlexSans",
//fontWeight: FontWeight.w500,
//),
//textAlign: TextAlign.left,
//),
//Container(
//width: 3 / 360 * screenWidth,
//height: 3 / 720 * screenHeight,
//margin: EdgeInsets.only(
//left: 6/360*screenWidth,top: 2/720*screenHeight,right: 6/360*screenWidth),
//decoration: BoxDecoration(
//color: Color.fromARGB(
//255, 153, 154, 171),
//borderRadius:
//BorderRadius.all(
//Radius.circular(
//1.5)),
//),
//child: Container(),
//),
//Text(
//"12:30 AM - 01:15 PM",
//style: TextStyle(
//color: Color.fromARGB(
//255, 153, 154, 171),
//fontSize: (12 /
//Constant
//    .defaultScreenWidth) *
//screenWidth,
//fontFamily: "IBMPlexSans",
//fontWeight: FontWeight.w500,
//),
//textAlign: TextAlign.left,
//),
//],
//),
//],
//),
//),
//Container(
//width: (183 / Constant.defaultScreenWidth) *
//screenWidth,
//height: (15/ Constant.defaultScreenHeight) *
//screenHeight,
//margin: EdgeInsets.only(
//left: (72 / Constant.defaultScreenWidth) *
//screenWidth,
//top: (10 / Constant.defaultScreenHeight) *
//screenHeight),
//child: Row(
//crossAxisAlignment:
//CrossAxisAlignment.stretch,
//children: [
//Align(
//alignment: Alignment.topLeft,
//child: Container(
//child: Row(
//crossAxisAlignment:
//CrossAxisAlignment.start,
//children: [
//Container(
//margin: EdgeInsets.only(
//top: (2 /
//Constant
//    .defaultScreenHeight) *
//screenHeight),
//child:
//ChannelAssets.calendarSvgIcon,
//),
//Container(
//margin: EdgeInsets.only(
//left: (5 /
//Constant
//    .defaultScreenWidth) *
//screenWidth),
//child: Text(
//"Set Reminder",
//style: TextStyle(
//color: Color.fromARGB(
//255, 0, 171, 251),
//fontSize: (12 /
//Constant
//    .defaultScreenWidth) *
//screenWidth,
//fontFamily: "IBMPlexSans",
//fontWeight: FontWeight.w500,
//),
//textAlign: TextAlign.left,
//),
//),
//],
//),
//),
//),
//Align(
//alignment: Alignment.topLeft,
//child: Container(
////                                          height: (15 /
////                                                  Constant.defaultScreenHeight) *
////                                              screenHeight,
//margin: EdgeInsets.only(
//left: (20 /
//Constant
//    .defaultScreenWidth) *
//screenWidth),
//child: Row(
//crossAxisAlignment:
//CrossAxisAlignment.start,
//children: [
//Container(
////                                                height: (15 /
////                                                        Constant
////                                                            .defaultScreenHeight) *
////                                                    screenHeight,
//margin: EdgeInsets.only(
//top: (2 /
//Constant
//    .defaultScreenHeight) *
//screenHeight),
//child:
//ChannelAssets.downloadSvgIcon,
//),
//Container(
//margin: EdgeInsets.only(
//left: (4 /
//Constant
//    .defaultScreenWidth) *
//screenWidth),
//child: Text(
//"Handout",
//style: TextStyle(
//color: Color.fromARGB(
//255, 0, 171, 251),
//fontSize: (12 /
//Constant
//    .defaultScreenWidth) *
//screenWidth,
//fontFamily: "IBMPlexSans",
//fontWeight: FontWeight.w500,
//),
//textAlign: TextAlign.center,
//),
//),
//],
//),
//),
//),
//],
//),
//),
//],
//),
//),
//Container(
//width:
//(60 / Constant.defaultScreenWidth) * screenWidth,
//height: (50 / Constant.defaultScreenHeight) *
//screenHeight,
//child: Stack(
//alignment: Alignment.topLeft,
//children: [
//Container(
////                                  height: (50 / Constant.defaultScreenHeight) *
////                                      screenHeight,
//child: Container(decoration: BoxDecoration(
//border: Border.all(
//color: NeutralColors.purpleish_blue.withOpacity(0.10),
//// color: Colors.cyan,
//width: (1 / 360) * screenWidth,
//),
//borderRadius: BorderRadius.all(Radius.circular(3)),
//),)
//),
//Positioned(
//left: (19 / Constant.defaultScreenWidth) *
//screenWidth,
//top: (4 / Constant.defaultScreenHeight) *
//screenHeight,
//child: Text(
//"27",
//style: TextStyle(
//color: Color.fromARGB(255, 71, 89, 147),
//fontSize:
//(18 / Constant.defaultScreenWidth) *
//screenWidth,
//fontFamily: "IBMPlexSans",
//fontWeight: FontWeight.w700,
//),
//textAlign: TextAlign.left,
//),
//),
//Positioned(
//
//top: (30 / Constant.defaultScreenHeight) *
//screenHeight,
//
//child: Container(
//height :32/720*screenHeight,
//width: 60/Constant.defaultScreenWidth*screenWidth,
//decoration: BoxDecoration(
//gradient: LinearGradient(
//begin: Alignment(0, 0),
//end: Alignment(1.0199999809265137, 1.0099999904632568),
//colors:[const Color(0xff3380cc).withOpacity(0.10),
//NeutralColors.purpley.withOpacity(0.10)
//
//]
//),
//
//),
//
//child: Text(
//"Aug 2018",
//style: TextStyle(
//color: Color.fromARGB(255, 0, 3, 44),
//fontSize:
//(12 / Constant.defaultScreenHeight) *
//screenHeight,
//fontFamily: "IBMPlexSans",
//),
//textAlign: TextAlign.center,
//),
//),
//),
//],
//),
//),
//],
//),
//);
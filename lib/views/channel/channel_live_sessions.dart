import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:imsindia/components/video_player.dart';
import 'package:imsindia/resources/strings/channel_labels.dart';
import 'package:imsindia/routers/routes.dart';
import 'package:imsindia/utils/colors.dart';
import 'package:imsindia/utils/screen_util.dart';
import 'package:imsindia/utils/svg_images/channel_svg_icons.dart';
import 'package:imsindia/utils/svg_images/prepare_svg_images.dart';
import 'package:imsindia/views/calendar/calendar_home_screen.dart';
import 'package:imsindia/views/channel/channel_quick_tips.dart';
import 'package:video_player/video_player.dart';

var screenWidthTotal;
var screenHeightTotal;

var _safeAreaHorizontal;

var _safeAreaVertical;

var screenHeight;

var screenWidth;
var courseID;
List<String> list = ["Applications", "Applications", "Applications"];

class ChannelLiveSessions extends StatefulWidget {
  @override
  _ChannelLiveSessionsState createState() => _ChannelLiveSessionsState();
}

class _ChannelLiveSessionsState extends State<ChannelLiveSessions> {

   TabController _tabController;

  @override
  Widget build(BuildContext context) {
    var _mediaQueryData = MediaQuery.of(context);
    screenWidthTotal = _mediaQueryData.size.width;
    screenHeightTotal = _mediaQueryData.size.height;
    _safeAreaHorizontal =
        _mediaQueryData.padding.left + _mediaQueryData.padding.right;
    _safeAreaVertical =
        _mediaQueryData.padding.top + _mediaQueryData.padding.bottom;

    screenHeight = screenHeightTotal - _safeAreaVertical;
    screenWidth = screenWidthTotal - _safeAreaHorizontal;
    return Scaffold(
      body: Container(
        //color: Colors.amber,
        margin: EdgeInsets.only(
          left: (0 / Constant.defaultScreenWidth) * screenWidth,
          right: (0 / Constant.defaultScreenWidth) * screenWidth,
        ),
        child: Column(
          children: <Widget>[
            Container(
              child: Column(
                children: <Widget>[
                  Container(
                decoration:  BoxDecoration(
                      borderRadius: new BorderRadius.circular(8.0/360*screenWidth)),
                    margin:EdgeInsets.only(top:24.5/720*screenHeight,left:0/360*screenWidth,right:0/360*screenWidth),
                    child: _getChewiePlayer(false,context), // Hide snd Show widgets
                  ),
                  Container(
                    margin:EdgeInsets.only(left: 20/360*screenWidth,right: 20/360*screenWidth,top:12/720*screenHeight,
                          bottom: 24.5/720*screenHeight),
                   // height:100/720*screenHeight,
                    //width: 320/360*screenWidth,
                  //  color: Colors.red,
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Container(
                          height: (35 / Constant.defaultScreenHeight) *
                              screenHeight,
                          width: (35 / Constant.defaultScreenWidth) *
                              screenWidth,
                          decoration: new BoxDecoration(
                              shape: BoxShape.circle,
                              image: new DecorationImage(
                                  fit: BoxFit.cover,
                                  image: AssetImage(
                                      'assets/images/oval-2.png'))),
                        ),
                        Container(
                         // color: Colors.yellowAccent,
                          //height: 18/720*screenHeight,
                          margin:EdgeInsets.only(left:10/360*screenWidth),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Container(
                                //margin:EdgeInsets.only(top: 10/720*screenHeight,right: 19/360*screenWidth),
                                child: Text(
                                  "Quick tips for CAT preperation",
                                  style: TextStyle(
                                    color: NeutralColors.dark_navy_blue,
                                    fontSize: 14/360*screenWidth,
                                    fontFamily: "IBMPlexSans",
                                    fontWeight: FontWeight.w500,
                                  ),
                                  textAlign: TextAlign.left,
                                ),
                              ),
                              Container(
                                margin: EdgeInsets.only(top: 5/720*screenHeight),
                                child: Row(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: <Widget>[
                                    Container(
                                      child:Text(
                                        "Stephen Grider",
                                        style: TextStyle(
                                          color: NeutralColors.blue_grey,
                                          fontSize: 12/360*screenWidth,
                                          fontFamily: "IBMPlexSans",
                                          fontWeight: FontWeight.w500,
                                        ),
                                        textAlign: TextAlign.left,
                                      ),
                                    ),
                                    Container(
                                      width: 3 / 360 * screenWidth,
                                      height: 3 / 720 * screenHeight,
                                      margin: EdgeInsets.only(
                                          left: 8/360*screenWidth,top: 2/720*screenHeight),
                                      decoration: BoxDecoration(
                                        color: Color.fromARGB(
                                            255, 153, 154, 171),
                                        borderRadius:
                                        BorderRadius.all(
                                            Radius.circular(
                                                1.5)),
                                      ),
                                      child: Container(),
                                    ),

                                    Container(
                                      margin: EdgeInsets.only(left: 7/360*screenWidth),
                                      child: Text(
                                        "12:30 AM - 01:15 PM",
                                        style: TextStyle(
                                          color: NeutralColors.blue_grey,
                                          fontSize: 12/360*screenWidth,
                                          fontFamily: "IBMPlexSans",
                                          fontWeight: FontWeight.w500,
                                        ),
                                        textAlign: TextAlign.left,
                                      ),
                                    ),


                                  ],
                                ),
                              ),
                              Container(
                                margin: EdgeInsets.only(top:5/720*screenHeight),
                                child: Row(
                                  children: <Widget>[
                                    Container(
//                                       width: 12/360*screenWidth,
//                                      height: 12/720*screenHeight,
                                     // margin: EdgeInsets.only(right: 5/360*screenWidth,bottom: 2/720*screenHeight),
                                      child: SvgPicture.asset(
                                        ChannelAssets.downloadicon,
                                        fit: BoxFit.fill,

                                      ),
                                    ),
                                    Container(
                                      margin: EdgeInsets.only(left: 5/360*screenWidth),
                                      child: Text(
                                        "Handout",
                                        style: TextStyle(
                                          color: PrimaryColors.azure_Dark,
                                          fontSize: 12/360*screenWidth,
                                          fontFamily: "IBMPlexSans",
                                          fontWeight: FontWeight.w500,
                                        ),
                                        textAlign: TextAlign.center,
                                      ),
                                    ),

                                   ],
                                ),
                              ),

                            ],
                          ),

                        ),

                      ],
                    ),
                  ),
                ],


              ),
            ),
            Expanded(
              child: Container(
               //color: Colors.blue,
                margin: EdgeInsets.only(left: 20/360*screenWidth,right: 20/360*screenWidth),
                child: ListView.builder(
                  itemCount: list.length,
                  itemBuilder: (context, index) {
                    return Column(
                      children: <Widget>[
                        Container(
                          height: (1/720*screenHeight),
                          color: NeutralColors.ice_blue,
                        ),
                        Container(
                          margin: EdgeInsets.only(top: 24.5/720*screenHeight,bottom: 24.5/720*screenHeight),
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Container(
                                width: 60 /360 * screenWidth,
//                    height: 50 / 720 *screenHeight,
                                //margin: EdgeInsets.only(left: 20/360*screenWidth),
                                decoration: BoxDecoration(
                                  border: Border.all(
                                    color: NeutralColors.purpleish_blue.withOpacity(0.10),
                                    // color: Colors.cyan,
                                    width: (1 / 360) * screenWidth,
                                  ),
                                  borderRadius: BorderRadius.all(Radius.circular(3)),
                                ),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.stretch,
                                  children: <Widget>[
                                    Container(
                                      //color:Colors.blue,
                                      child: Text(
                                        "27",
                                        style: TextStyle(
                                          color: Color.fromARGB(255, 71, 89, 147),
                                          fontSize:18 / 360 * screenWidth,
                                          fontFamily: "IBMPlexSans",
                                          fontWeight: FontWeight.w700,
                                        ),
                                        textAlign: TextAlign.center,
                                      ),
                                    ),
                                    Container(
                                      //height :21.5/720*screenHeight,
                                      //width: 60/360*screenWidth,
                                      decoration: BoxDecoration(
                                        gradient: LinearGradient(
                                            begin: Alignment(0, 0),
                                            end: Alignment(1.0199999809265137, 1.0099999904632568),
                                            colors:[const Color(0xff3380cc).withOpacity(0.10),
                                              NeutralColors.purpley.withOpacity(0.10)

                                            ]
                                        ),

                                      ),

                                      child: Text(
                                        "Aug 2018",
                                        style: TextStyle(
                                          color: Color.fromARGB(255, 0, 3, 44),
                                          fontSize:
                                          12 / 720 *
                                              screenHeight,
                                          fontFamily: "IBMPlexSans",
                                        ),
                                        textAlign: TextAlign.center,
                                      ),
                                    ),

                                  ],
                                ),
                              ),
                              Container(
                                margin: EdgeInsets.only(left: 12/360*screenWidth),
                                child: Column(
                                  crossAxisAlignment:CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Container(
//                                        color:Colors.green,
                                      width: 78 / 360 * screenWidth,
//                                        height: 40 /720 * screenHeight,

                                      decoration: BoxDecoration(
                                        color:NeutralColors.deep_sky_blue,
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(2)),
                                      ),

                                      child: Text(
                                        list[index],
                                        style: TextStyle(
                                          color: Color.fromARGB(
                                              255, 255, 255, 255),
                                          fontSize: 12 /360 *screenWidth,
                                          fontFamily: "IBMPlexSans",
                                        ),
                                        textAlign: TextAlign.center,
                                      ),
                                    ),
                                    Container(
                                      margin: EdgeInsets.only(
//                              left: 72 /360 * screenWidth,
                                          top: 4 /720 * screenHeight),
                                      child: Text(
                                        "Tips for CAT online exams",
                                        style: TextStyle(
                                          color: Color.fromARGB(255, 0, 3, 44),
                                          fontSize: 14 / 360 * screenWidth,
                                          fontFamily: "IBMPlexSans",
                                          fontWeight: FontWeight.w500,
                                        ),
                                        textAlign: TextAlign.left,
                                      ),
                                    ),
                                    Container(
                                      //  height: 15/720*screenHeight,
                                      margin: EdgeInsets.only(top: 5/720*screenHeight),
                                      child: Row(
                                        children: <Widget>[
                                          Container(

                                            // margin:EdgeInsets.only(bottom:1/360*screenWidth),
                                            child:Text(
                                              "Stephen Grider",
                                              style: TextStyle(
                                                color: NeutralColors.blue_grey,
                                                fontSize: 12/360*screenWidth,
                                                fontFamily: "IBMPlexSans",
                                                fontWeight: FontWeight.w500,
                                              ),
                                              textAlign: TextAlign.left,
                                            ),
                                          ),
                                          Container(
                                            width: 3 / 360 * screenWidth,
                                            height: 3 / 720 * screenHeight,
                                            margin: EdgeInsets.only(
                                                left: 8/360*screenWidth,top: 2/720*screenHeight),
                                            decoration: BoxDecoration(
                                              color: NeutralColors.blue_grey,
                                              borderRadius:
                                              BorderRadius.all(
                                                  Radius.circular(
                                                      1.5)),
                                            ),
                                            child: Container(),
                                          ),

                                          Container(
                                            margin: EdgeInsets.only(left: 7/360*screenWidth),
                                            child: Text(
                                              "12:30 AM - 01:15 PM",
                                              style: TextStyle(
                                                color: NeutralColors.blue_grey,
                                                fontSize: 12/360*screenWidth,
                                                fontFamily: "IBMPlexSans",
                                                fontWeight: FontWeight.w500,
                                              ),
                                              textAlign: TextAlign.left,
                                            ),
                                          ),


                                        ],
                                      ),
                                    ),
                                    Container(
                                      margin: EdgeInsets.only(top: 5/720*screenHeight),
                                      child: Row(
                                        children: <Widget>[
                                          GestureDetector(
                                            onTap: (){
                                              Navigator.push(
                                                  context,
                                                  MaterialPageRoute(
                                                    builder: (context) =>CalendarHomeScreen (),
                                                  ));

                                          },
                                            child: Container(
                                              // width: 12/360*screenWidth,
                                              // height: 12/720*screenHeight,
                                              //margin: EdgeInsets.only(top: 2/720*screenHeight,right: 5/360*screenWidth),
                                              child: SvgPicture.asset(
                                                ChannelAssets.calendaricon,
                                                fit: BoxFit.fill,
                                              ),
                                            ),
                                          ),
                                          Container(
                                            margin: EdgeInsets.only(
                                                left: 5/360*screenWidth),
                                            child: Text(
                                              "Set Reminder",
                                              style: TextStyle(
                                                color: PrimaryColors.azure_Dark,
                                                fontSize: 12/360*screenWidth,
                                                fontFamily: "IBMPlexSans",
                                                fontWeight: FontWeight.w500,
                                              ),
                                              textAlign: TextAlign.center,
                                            ),
                                          ),
                                          Container(
                                             // width: 9/360*screenWidth,
                                             // height: 11/720*screenHeight,
                                             margin: EdgeInsets.only(left: 20/360*screenWidth),
                                            child: SvgPicture.asset(
                                            ChannelAssets.downloadicon,
                                            fit: BoxFit.fill,

                                          ),
                                           ),
                                          Container(
                                            margin: EdgeInsets.only(left: 4/360*screenWidth),
                                            child: Text(
                                              "Handout",
                                              style: TextStyle(
                                                color: PrimaryColors.azure_Dark,
                                                fontSize: 12/360*screenWidth,
                                                fontFamily: "IBMPlexSans",
                                                fontWeight: FontWeight.w500,
                                              ),
                                              textAlign: TextAlign.center,
                                            ),
                                          ),


                                        ],
                                      ),
                                    ),


                                  ],
                                ),

                              ),
                            ],
                          ),
                        ),
                      ],
                    );
                  },
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}


class UnicornOutlineButton extends StatelessWidget {
  final _GradientPainter _painter;
  final Widget _child;
  // final VoidCallback _callback;
  final double _radius;

  UnicornOutlineButton({
    @required double strokeWidth,
    @required double radius,
    @required Gradient gradient,
    @required Widget child,
    //@required VoidCallback onPressed,
  })  : this._painter = _GradientPainter(strokeWidth: strokeWidth, radius: radius, gradient: gradient),
        this._child = child,
        //this._callback = onPressed,
        this._radius = radius;

  @override
  Widget build(BuildContext context) {
    return CustomPaint(
      painter: _painter,
      child: GestureDetector(
        behavior: HitTestBehavior.translucent,
        //onTap: _callback,
        child: InkWell(
          borderRadius: BorderRadius.circular(_radius),
          //onTap: _callback,
          child: Container(
            constraints: BoxConstraints(minWidth: 88, minHeight: 48),
            child: Row(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                _child,
              ],
            ),
          ),
        ),
      ),
    );
  }
}
class _GradientPainter extends CustomPainter {
  final Paint _paint = Paint();
  final double radius;
  final double strokeWidth;
  final Gradient gradient;

  _GradientPainter({@required double strokeWidth, @required double radius, @required Gradient gradient})
      : this.strokeWidth = strokeWidth,
        this.radius = radius,
        this.gradient = gradient;

  @override
  void paint(Canvas canvas, Size size) {
    // create outer rectangle equals size
    Rect outerRect = Offset.zero & size;
    var outerRRect = RRect.fromRectAndRadius(outerRect, Radius.circular(radius));

    // create inner rectangle smaller by strokeWidth
    Rect innerRect = Rect.fromLTWH(strokeWidth, strokeWidth, size.width - strokeWidth * 2, size.height - strokeWidth * 2);
    var innerRRect = RRect.fromRectAndRadius(innerRect, Radius.circular(radius - strokeWidth));

    // apply gradient shader
    _paint.shader = gradient.createShader(outerRect);

    // create difference between outer and inner paths and draw it
    Path path1 = Path()..addRRect(outerRRect);
    Path path2 = Path()..addRRect(innerRRect);
    var path = Path.combine(PathOperation.difference, path1, path2);
    canvas.drawPath(path, _paint);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) => oldDelegate != this;
}


_getChewiePlayer(bool offStageFlag, BuildContext context){
  Offstage offstage = new Offstage(
    offstage: offStageFlag,
    child: Container(
      decoration:  BoxDecoration(
          borderRadius: new BorderRadius.circular(8.0/360*screenWidth)),
      margin: EdgeInsets.only(
          left: (20 / 360) * screenWidth, right: (20 / 360) * screenWidth),
      height: 181 / 720 * screenHeight,
      child: InkWell(
        onTap: () {
          AppRoutes.push(context,ChannelVideosScreens());
        },
        child: Container(

          constraints: new BoxConstraints.expand(
            height: (181 / 720) * screenHeight,

          ),
          alignment: Alignment.bottomLeft,
          padding: new EdgeInsets.only(left: 16.0, bottom: 8.0),
          decoration:  BoxDecoration(
            borderRadius: new BorderRadius.circular(8.0/360*screenWidth),
            image: DecorationImage(
                alignment: Alignment(-.2, 0),
                image: CachedNetworkImageProvider(
                    'http://d1cn01kw45hc6h.cloudfront.net/wp-content/uploads/2019/08/22161210/Quadtratic.png'),
//                  image: NetworkImage(
//                      'http://d1cn01kw45hc6h.cloudfront.net/wp-content/uploads/2019/08/22161210/Quadtratic.png'
//                  ),
                fit: BoxFit.cover),
          ),
          child: Center(
            child: Container(
              height: (50 / 720) * screenHeight,
              width: (50 / 360) * screenWidth,
              child: SvgPicture.asset(
                getPrepareSvgImages.playwhite,
                fit: BoxFit.fitHeight,
              ),
            ),
          ),
        ),
      ),
    ),
  );
  return offstage;
}
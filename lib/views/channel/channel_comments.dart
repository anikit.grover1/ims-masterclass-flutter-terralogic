import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_document_picker/flutter_document_picker.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:imsindia/components/custom_dropdown_channel.dart';
import 'package:imsindia/resources/strings/prepare.dart';
import 'package:imsindia/utils/colors.dart';
import 'package:imsindia/utils/screen_util.dart';
import 'package:imsindia/utils/svg_images/channel_svg_icons.dart';
import 'package:imsindia/views/Arun/ReadMoreText.dart';
import 'package:imsindia/views/channel/channel_pojo_class/channel_comments_pojo.dart';
import 'package:imsindia/views/channel/channel_replies.dart';
import 'package:shared_preferences/shared_preferences.dart';
//import '//package:keyboard_visibility/keyboard_visibility.dart';

bool value = false;
GlobalKey _keyRed = GlobalKey();
GlobalKey _keyRed1 = GlobalKey();

List titles = [
  'Date',
  'Upvotes',
];
List<dynamic> channelCommentsLikes = [];
final List<ChannelCommentsOptions> list_comments=[];

List <String> comment = <String>[
  'How do you create a direct mail advertising campaign that gets results?  The following tips on creating a direct mail advertising campaign have been street-tested and will bring you huge returns in a short period of time.',
  'How do you create a direct mail advertising campaign that gets results?  The following tips on creating a direct mail advertising campaign have been street-tested and will bring you huge returns in a short period of time.',
];
List<int> likes = [
  324,324,
];
List<int> list = [123, 125];
List<String> nameList = ["Jack Sparrow", "Stephen Grider"];
class ChannelComments extends StatefulWidget {
  @override
  _ChannelCommentsState createState() => _ChannelCommentsState();
}

class _ChannelCommentsState extends State<ChannelComments> with AutomaticKeepAliveClientMixin<ChannelComments>{
  OverlayEntry _overlayEntry;
  bool _pickFileInProgress = false;
  int selectedValue;
  final TextEditingController _comment = new TextEditingController();
  final TextEditingController _sample = new TextEditingController();
  List<String> getMonths = [];
  String selectedMonth;
  String labelValue = "Sort By";
  String selectedString ;
  bool isClicked = false;
  bool clicked = false;
  int i=123;
  bool isSelected = false;
  FocusNode _focusNode = new FocusNode();
  String _path = '-';
  bool uploaded = false;
  bool _iosPublicDataUTI = true;
  bool _checkByCustomExtension = false;
  bool _checkByMimeType = false;
  GlobalKey<ScaffoldState> _key;
  bool timeago = false;
  String timeAgo;
  String timeAgoSinceDate(String dateString, {bool numericDates = true}) {
    print("********************************************Coming" +dateString.toString());

    DateTime date = DateTime.parse(dateString);

    final date2 = DateTime.now();

    final difference = date2.difference(date);

    if ((difference.inDays / 365).floor() >= 2) {
      return '${(difference.inDays / 365).floor()} years ago';
    } else if ((difference.inDays / 365).floor() >= 1) {
      return (numericDates) ? '1 year ago' : 'Last year';
    } else if ((difference.inDays / 30).floor() >= 2) {
      print("********************************************monthsmonthsmonthsmonthsmonths" +dateString.toString());

      return '${(difference.inDays / 365).floor()} months ago';
    } else if ((difference.inDays / 30).floor() >= 1) {
      return (numericDates) ? '1 month ago' : 'Last month';
    } else if ((difference.inDays / 7).floor() >= 2) {
      return '${(difference.inDays / 7).floor()} weeks ago';
    } else if ((difference.inDays / 7).floor() >= 1) {
      return (numericDates) ? '1 week ago' : 'Last week';
    } else if (difference.inDays >= 2) {
      return '${difference.inDays} days ago';
    } else if (difference.inDays >= 1) {
      return (numericDates) ? '1 day ago' : 'Yesterday';
    } else if (difference.inHours >= 2) {
      return '${difference.inHours} hours ago';
    } else if (difference.inHours >= 1) {
      return (numericDates) ? '1 hour ago' : 'An hour ago';
    } else if (difference.inMinutes >= 2) {
      return '${difference.inMinutes} minutes ago';
    } else if (difference.inMinutes >= 1) {
      setState(() {
        timeAgo ='Just now';
        print('timeAgotimeAgotimeAgotimeAgo'+timeAgo);

      });
      return (numericDates) ? '1 minute ago' : 'A minute ago';
    } else if (difference.inSeconds >= 3) {
      setState(() {
        timeAgo ='${difference.inSeconds} seconds ago';
        print('timeAgotimeAgotimeAgotimeAgo'+timeAgo);
      });
      return '${difference.inSeconds} seconds ago';
    } else {
      setState(() {
        timeAgo ='Just now';
        print('timeAgotimeAgotimeAgotimeAgo'+timeAgo);

      });
      return 'Just now';
    }
  }
  @override
  void initState() {
    super.initState();
    _key = GlobalKey<ScaffoldState>();
     labelValue = "Sort By";

    // KeyboardVisibilityNotification().addNewListener(
    //   onHide: () {
    //     print(_focusNode.hasFocus);
    //     _focusNode.unfocus();
    //   },
    // );
  }
  void didChangeDependencies() {
    sharedMethod();
    super.didChangeDependencies();
  }
  void sharedMethod() async{
    print('sharedMethod');
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setBool('focusNode', _focusNode.hasFocus);
  }
  Widget getCommentsSection(List<ChannelCommentsOptions> list_comments,screenHeight,screenWidth)
  {
    List<Widget> list = new List<Widget>();
    if(list_comments.isEmpty || list_comments.length == null){
      list.add(
          Container(
            height: 40/720*screenHeight,
            margin: EdgeInsets.only(top: 24.5/720*screenHeight),
            child: Text("No Comments",style: TextStyle(color: Colors.blue),),
          ));
    }else{
      for(var i=0; i < list_comments.length; i++){
        list.add(
          Container(
            margin: EdgeInsets.only(left: (20/360)*screenWidth),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                /**profile image and name colum**/
                Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Container(
                      //     color: Colors.pink,
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Container(
//                                    margin: EdgeInsets.only(
//                                        right: (20 / 360) * screenWidth),
                            child: Row(
                              crossAxisAlignment:
                              CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: <Widget>[
                                /**Profile pic**/
                                Container(
                                  height: (40 /
                                      Constant.defaultScreenHeight) *
                                      screenHeight,
                                  width:
                                  (40 / Constant.defaultScreenWidth) *
                                      screenWidth,
                                  decoration: new BoxDecoration(
                                      shape: BoxShape.circle,
                                      image: new DecorationImage(
                                          fit: BoxFit.cover,
                                          image: AssetImage(
                                              'assets/images/oval-2.png'))),
                                ),
                                Container(
                                  child: Column(
                                    crossAxisAlignment:
                                    CrossAxisAlignment.start,
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: <Widget>[
                                      Container(
                                        margin: EdgeInsets.only(
                                            top: (0 /
                                                Constant
                                                    .defaultScreenHeight) *
                                                screenHeight,
                                            left:
                                            (12 / 360) * screenWidth),
                                        child: Text(
                                          list_comments[i].nameofperson,
                                          style: TextStyle(
                                            color: NeutralColors
                                                .dark_navy_blue,
                                            fontWeight: FontWeight.w700,
                                            fontFamily: "IBMPlexSans",
                                            fontStyle: FontStyle.normal,
                                            fontSize: (14 /
                                                Constant
                                                    .defaultScreenWidth) *
                                                screenWidth,
                                          ),
                                          textAlign: TextAlign.start,
                                        ),
                                      ),
                                      Container(
                                        margin: EdgeInsets.only(
                                            top: (5 /
                                                Constant
                                                    .defaultScreenHeight) *
                                                screenHeight,
                                            left:
                                            (12 / 360) * screenWidth),
                                        child: Text(
                                          timeago ? timeAgoSinceDate(
                                              list_comments[i].durationofcomment)
                                              .toString() : timeAgoSinceDate(
                                              list_comments[i].durationofcomment)
                                              .toString(),
                                          style: TextStyle(
                                            color:
                                            NeutralColors.blue_grey,
                                            fontWeight: FontWeight.w700,
                                            fontFamily: "IBMPlexSans",
                                            fontStyle: FontStyle.normal,
                                            fontSize: (12 /
                                                Constant
                                                    .defaultScreenWidth) *
                                                screenWidth,
                                          ),
                                          textAlign: TextAlign.start,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),

                        ],
                      ),
                    ),
                    GestureDetector(
                      onTap: () {
                        // This is a hack because _PopupMenuButtonState is private.
                        dynamic state = _menuKey.currentState;
                        state.showButtonMenu();
                      },
                      child: Container(
                        height: (25 / 720) * screenHeight,
                        //color: Colors.red,
                        child: Align(
                          alignment: Alignment.topRight,
                          child: PopupMenuButton(
                              icon: Icon(Icons.more_vert,color: NeutralColors.blue_grey,size: 20,),
                              // padding: EdgeInsets.all(10),
                              // key: _menuKey,
                              itemBuilder: (_) =>
                              <PopupMenuItem<String>>[
                                new PopupMenuItem<String>(
                                    child: const Text(
                                      'Report',
                                      style: TextStyle(
                                        color: NeutralColors
                                            .gunmetal,
                                        fontFamily:
                                        "IBMPlexSans",
                                        fontWeight:
                                        FontWeight
                                            .w400,
                                        fontSize: 14,
                                      ),
                                    ),
                                    value: 'Report'),
                              ],
                              onSelected: (_) {}),

                        ),
                      ),
                    ),
                  ],
                ),
                /**Extended Paragraph colum**/

                Container(
                  margin: EdgeInsets.only(
                      top: (12 / Constant.defaultScreenHeight) *
                          screenHeight,
                      right: (20 / 360) * screenWidth),
                  child: SingleChildScrollView(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.all(1.0),
                          child: ReadMoreText(
                            list_comments[i].comment_text,
                            trimLines: 3,
                            colorClickableText: PrimaryColors.azure_Dark,
                            trimMode: TrimMode.Line,
                            trimCollapsedText:
                            SubQueriesCommentsScreenStrings
                                .Text_ReadMore,
                            trimExpandedText:
                            SubQueriesCommentsScreenStrings
                                .Text_ReadLess,
                            style: TextStyle(
                              color: NeutralColors.gunmetal,
                              fontWeight: FontWeight.w500,
                              fontFamily: "IBMPlexSans",
                              fontStyle: FontStyle.normal,
                              height: 1.5,
                              fontSize: (12/360)*screenWidth,
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                ),
                /**upvotes and reply colum**/
                Container(
                  //  color: Colors.yellowAccent,
                  margin: EdgeInsets.only(
                      top: (12 /
                          Constant.defaultScreenHeight) *
                          screenHeight),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      InkWell(
                          onTap: () {
                            setState(() {
                              if(channelCommentsLikes.contains(i)){
                                channelCommentsLikes.remove(i);
                              }
                              else{
                                channelCommentsLikes.add(i);
                              }
                              isSelected = !isSelected;
                            });
                          },
                          child: SvgPicture.asset(
                            channelCommentsLikes.contains(i) ? ChannelAssets.likeactive
                                :ChannelAssets.likeinactive,
                            fit: BoxFit.fill,
                          )
                      ),
                      Container(
                          margin: EdgeInsets.only(
                              top: 0 /
                                  Constant.defaultScreenHeight *
                                  screenHeight,
                              left: 5 / 360 * screenWidth),
                          decoration: BoxDecoration(
                            color: Colors.transparent,
                            borderRadius: BorderRadius.all(
                                Radius.circular(2)),
                          ),
                          child: Container(
                            margin: EdgeInsets.only(
                                top: 0 /
                                    Constant.defaultScreenHeight *
                                    screenHeight),
                            child: Text(
                              // SubQueriesCommentsScreenStrings.Text_Liked,
                              channelCommentsLikes.contains(i) ? '${likes[i]+1} Upvotes' : '${likes[i]} Upvotes',
                              style: TextStyle(
                                color: channelCommentsLikes.contains(i)
                                    ? NeutralColors.deep_sky_blue
                                    : NeutralColors.dark_navy_blue,
                                fontWeight: FontWeight.w700,
                                fontFamily: "IBMPlexSans",
                                fontStyle: FontStyle.normal,
                                fontSize: (11 /
                                    Constant
                                        .defaultScreenWidth) *
                                    screenWidth,
                              ),
                              textAlign: TextAlign.left,
                            ),
                          )),
                      GestureDetector(
                        onTap: () {
                          //  AppRoutes.push(context, ChannelReplies());
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) => ChannelReplies(text: 'Reply',index: i,),
                              ));
                        },
                        child: Container(
                          // color: Colors.red,
                          margin: EdgeInsets.only(
                            left: (20 /
                                Constant
                                    .defaultScreenWidth) *
                                screenWidth,),
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              Icon(
                                Icons.chat_bubble_outline,
                                color: NeutralColors.black,
                                size: 11 / 360 * screenWidth,
                              ),
                              Container(
                                  margin: EdgeInsets.only(
                                      left: 5 / 360 * screenWidth),
                                  child: Text(
                                    SubQueriesCommentsScreenStrings
                                        .Text_Reply,
                                    style: TextStyle(
                                      color: NeutralColors.dark_navy_blue,
                                      fontWeight: FontWeight.w700,
                                      fontFamily: "IBMPlexSans",
                                      fontStyle: FontStyle.normal,
                                      fontSize: (11 /
                                          Constant
                                              .defaultScreenWidth) *
                                          screenWidth,
                                    ),
                                    textAlign: TextAlign.left,
                                  )),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                /**showreplies colum**/
                GestureDetector(
                  onTap: () {
                    //  AppRoutes.push(context, ChannelReplies());
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => ChannelReplies(text: 'Show',index: i,),
                        ));
                  },
                  child: Container(
                    //  color: Colors.purple,
                      width: double.infinity,
                      margin: EdgeInsets.only(
                        top: 5 /
                            Constant.defaultScreenHeight *
                            screenHeight,
                        bottom: 27 /
                            Constant.defaultScreenHeight *
                            screenHeight,
                      ),
                      child: Container(
                        margin: EdgeInsets.only(
                            top: 6 /
                                Constant.defaultScreenHeight *
                                screenHeight),
                        child: Text(
                          SubQueriesCommentsScreenStrings
                              .Text_ShowReplies,
                          style: TextStyle(
                            color: NeutralColors.blue_grey,
                            fontWeight: FontWeight.w400,
                            fontFamily: "IBMPlexSans",
                            fontStyle: FontStyle.normal,
                            fontSize:
                            (12 / Constant.defaultScreenWidth) *
                                screenWidth,
                          ),
                          textAlign: TextAlign.left,
                        ),
                      )),
                ),
              ],
            ),
          ),
        );
      }
    }
    return new Column(children: list);

  }
  _pickDocument() async {
    String result;
    try {
      setState(() {
        _path = '-';
        _pickFileInProgress = true;
      });
      final _utiController = TextEditingController(
        text: 'com.sidlatau.example.mwfbak',
      );

      final _extensionController = TextEditingController(
        text: 'mwfbak',
      );

      final _mimeTypeController = TextEditingController(
        text: 'application/pdf image/png',
      );
      FlutterDocumentPickerParams params = FlutterDocumentPickerParams(
        allowedFileExtensions: _checkByCustomExtension
            ? _extensionController.text
            .split(' ')
            .where((x) => x.isNotEmpty)
            .toList()
            : null,
        allowedUtiTypes: _iosPublicDataUTI
            ? null
            : _utiController.text
            .split(' ')
            .where((x) => x.isNotEmpty)
            .toList(),
        allowedMimeTypes: _checkByMimeType
            ? _mimeTypeController.text
            .split(' ')
            .where((x) => x.isNotEmpty)
            .toList()
            : null,
      );

      result = await FlutterDocumentPicker.openDocument(params: params);
    } catch (e) {
      print(e);
      result = 'Error: $e';
    } finally {
      setState(() {
        _pickFileInProgress = false;
      });
    }

    setState(() {
      uploaded = true;
      _path = result;
      print(_path.length.toString());
    });
  }

  final GlobalKey _menuKey = new GlobalKey();

  @override
  Widget build(BuildContext context) {
    final screenHeight = MediaQuery.of(context).size.height;
    final screenWidth = MediaQuery.of(context).size.width;

    return GestureDetector(
      onTap: () {
        setState(() {
          isClicked = false;
        });
        _focusNode.unfocus();
        // if(_overlayEntry!=null){

        //  }
      },
      child: Container(
        child: Container(
          child: Column(
            children: <Widget>[
              Expanded(
                child: Container(
                  margin: EdgeInsets.only(
                      top: 10 / Constant.defaultScreenHeight * screenHeight,
                      left:  (0 / Constant.defaultScreenWidth) *
                          screenWidth,
                      right:  (0 / Constant.defaultScreenWidth) *
                          screenWidth
                  ),
                  child: Column(
                    children: <Widget>[
                      Container(
                         //color: Colors.yellowAccent,
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Container(
                            //  color:Colors.red,
                              margin: EdgeInsets.only(
                                  left: (20 / 360) * screenWidth),
                              child: Align(
                                alignment: Alignment.center,
                                child: Text(
                                  SubQueriesCommentsScreenStrings.Text_TotalComments,
                                  style: TextStyle(
                                    color: NeutralColors.dark_navy_blue,
                                    fontWeight: FontWeight.w500,
                                    fontFamily: "IBMPlexSans",
                                    fontStyle: FontStyle.normal,
                                    fontSize: (14 / 360) * screenWidth,
                                  ),
                                  textAlign: TextAlign.center,
                                ),
                              ),
                            ),
                            Container(
                            // color: Colors.blueAccent,
                              padding: EdgeInsets.all(0.0),
                              height: 40/720*screenHeight,
                              margin: EdgeInsets.only(right: (20/screenWidth)*Constant.defaultScreenWidth,top: (0/720)*screenHeight,left:  (20 / Constant.defaultScreenWidth) *
                                  screenWidth,),
                              child: DropDownFormField(
                                getImmediateSuggestions: true,
                                textFieldConfiguration: TextFieldConfiguration(
                                  controller: _sample,
                                  label: selectedString!= null ? selectedString : labelValue,

                                ),
                                // suggestionsBoxController:sugg,
                                suggestionsBoxDecoration: SuggestionsBoxDecoration(
                                    borderRadius: new BorderRadius.circular(5.0),
                                    color: Colors.white),
                                suggestionsCallback: (pattern) {
                                  return SampleService.getSuggestions(pattern);
                                },
                                itemBuilder: (context, suggestion) {
                                  print("suggestinmon"+selectedMonth.toString());
                                  return Container(
                                    color:NeutralColors.pureWhite,
                                    child: Padding(
                                      padding:  EdgeInsets.only(top: (10 / Constant.defaultScreenHeight) * screenHeight,
                                          bottom: (10 / Constant.defaultScreenHeight) * screenHeight,
                                          left:(15 / Constant.defaultScreenWidth) * screenWidth ),
                                      child: Text(
                                        suggestion,
                                        style: TextStyle(
                                          fontFamily: "IBMPlexSans",
                                          fontWeight: FontWeight.w500,
                                          fontSize: (14 / Constant.defaultScreenWidth) * screenWidth,
                                          color: NeutralColors.gunmetal,
                                          textBaseline: TextBaseline.alphabetic,
                                          letterSpacing: 0.0,
                                          inherit: false,
                                        ),
                                      ),
                                    ),
                                  );
                                },
                                hideOnLoading: true,
                                debounceDuration: Duration(milliseconds: 100),
                                transitionBuilder: (context, suggestionsBox, controller) {
                                  return suggestionsBox;
                                },
                                onSuggestionSelected: (suggestion) {
                                  _sample.text = suggestion;
                                  setState(() {
                                    selectedString = suggestion;
                                  });
                                },
                                onSaved: (value) => {print("something")},
                              ),

                            ),
                          ],
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(
                            left:  (20 / Constant.defaultScreenWidth) *
                                screenWidth,
                            top: (0 / Constant.defaultScreenHeight) * screenHeight),
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            /**Circular Avatar**/
                            Container(
                              height: (30 / 720) * screenHeight,
                              width: (30 / 360) * screenWidth,
                              margin: EdgeInsets.only(
                                  top: (0 / 720) * screenHeight,
                                  left: (0 / 360) * screenWidth),
                              decoration: new BoxDecoration(
                                  shape: BoxShape.circle,
                                  image: new DecorationImage(
                                      fit: BoxFit.cover,
                                      image: AssetImage(
                                          'assets/images/oval-2.png'))),
                            ),
                            Expanded(
                              child: Container(
                                height: (40/720)*screenHeight,
                                margin: EdgeInsets.only(left: (10/360)*screenWidth,top: (0/720)*screenHeight,right: (20/360)*screenWidth),
                                decoration: BoxDecoration(
                                  border: Border.all(color: AccentColors.iceBlue),
                                  color:Colors.white,
                                  borderRadius: BorderRadius.all(Radius.circular(5)),
                                ),
                                child: Row(
                                  children: <Widget>[
                                    Container(
                                      height: (18/720)*screenHeight,
                                      width: (200/360) * screenWidth,
                                      margin: EdgeInsets.only(left: (13/360)*screenWidth,top: (10/720)*screenHeight,bottom: (10/720)*screenHeight),
                                      child: GestureDetector(
                                        child: TextFormField(
                                          controller: _comment,
                                          focusNode: _focusNode,
                                          // controller: uploaded ? new TextEditingController(text: '$_path'): post_a_query_Controller,
                                          // controller: uploaded ? path_Controller : post_a_query_Controller,
                                          style: TextStyle(
                                           // backgroundColor: NeutralColors.pureWhite,
                                            color: NeutralColors.black,
                                            fontWeight: FontWeight.bold,
                                            fontFamily: "IBMPlexSans",
                                            fontStyle: FontStyle.normal,
                                            fontSize: (14/720)*screenHeight,
                                          ),
                                          textInputAction: TextInputAction.done,
                                          decoration: new InputDecoration.collapsed
                                            (
                                            hintText:  SubQueriesCommentsScreenStrings.Text_PostAquery,
                                            hintStyle: TextStyle (
                                              color: AccentColors.iceBlue,
                                              fontWeight: FontWeight.w400,
                                              fontFamily: "IBMPlexSans",
                                              fontStyle: FontStyle.normal,
                                              fontSize: (14/720)*screenHeight,
                                            ),
                                          ),
                                          onFieldSubmitted: (val){
                                            setState(() {
                                              var timeofreply = DateTime.now();
                                              ChannelCommentsOptions option =
                                              new ChannelCommentsOptions.Comment("Kavya",timeofreply.toString(),val,i++);
                                              list_comments.add(option);
                                              print(comment);
                                              _comment.clear();
                                            });
                                            return;
                                          },
                                        ),
                                      ),
                                    ),
                                    GestureDetector(
                                      onTap:  () {
                                        _pickFileInProgress ? null : _pickDocument();
                                        print("pick");
                                      },
                                      child: Container(
                                        height: (18 /720) * screenHeight,
                                        width: (19 / 360) * screenWidth,
                                        margin: EdgeInsets.only(left: 29/360*screenWidth,top: (8/720)*screenHeight,bottom: (10/720)*screenHeight),
                                        child: Image.asset(
                                          "assets/images/clip-4.png",
                                          fit: BoxFit.fill,
                                        ),
//                            child:Icon(
//                              Icons.attach_file,
//                              color: NeutralColors.purpley,
//                              size: 17/360*screenWidth,
//                            )
                                      ),
                                    )
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
//                      Container(
//                        margin: EdgeInsets.only(top: (16/764)*screenHeight),
//                        child: getCommentsSection(list_comments,screenHeight,screenWidth),
//                      ),
                      list_comments.isEmpty || list_comments.length == null ?
                      Container(
                        height: 40/720*screenHeight,
                        margin: EdgeInsets.only(top: 24.5/720*screenHeight),
                        child: Text("No Comments",style: TextStyle(color: Colors.blue),),
                      ):
                      Expanded(
                        child: ListView.separated(
                          itemCount: list_comments.length,
                          padding: EdgeInsets.only(bottom: (40)),
                          itemBuilder: (context, index) {
                            return Container(
                              margin: EdgeInsets.only(
                                left: (20 / 360) * screenWidth,
                                top: (24.5 / Constant.defaultScreenHeight) *
                                    screenHeight,
                                right: (0 / Constant.defaultScreenWidth) *
                                    screenWidth,
                              ),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  /**profile image and name colum**/
                                  Row(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: <Widget>[
                                      Container(
                                      //     color: Colors.pink,
                                        child: Row(
                                          crossAxisAlignment: CrossAxisAlignment.start,
                                          children: <Widget>[
                                            Container(
//                                    margin: EdgeInsets.only(
//                                        right: (20 / 360) * screenWidth),
                                              child: Row(
                                                crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                                mainAxisAlignment: MainAxisAlignment.start,
                                                children: <Widget>[
                                                  /**Profile pic**/
                                                  Container(
                                                    height: (40 /
                                                        Constant.defaultScreenHeight) *
                                                        screenHeight,
                                                    width:
                                                    (40 / Constant.defaultScreenWidth) *
                                                        screenWidth,
                                                    decoration: new BoxDecoration(
                                                        shape: BoxShape.circle,
                                                        image: new DecorationImage(
                                                            fit: BoxFit.cover,
                                                            image: AssetImage(
                                                                'assets/images/oval-2.png'))),
                                                  ),
                                                  Container(
                                                    child: Column(
                                                      crossAxisAlignment:
                                                      CrossAxisAlignment.start,
                                                      mainAxisAlignment: MainAxisAlignment.start,
                                                      children: <Widget>[
                                                        Container(
                                                          margin: EdgeInsets.only(
                                                              top: (0 /
                                                                  Constant
                                                                      .defaultScreenHeight) *
                                                                  screenHeight,
                                                              left:
                                                              (12 / 360) * screenWidth),
                                                          child: Text(
                                                            list_comments[index].nameofperson,
                                                            style: TextStyle(
                                                              color: NeutralColors
                                                                  .dark_navy_blue,
                                                              fontWeight: FontWeight.w700,
                                                              fontFamily: "IBMPlexSans",
                                                              fontStyle: FontStyle.normal,
                                                              fontSize: (14 /
                                                                  Constant
                                                                      .defaultScreenWidth) *
                                                                  screenWidth,
                                                            ),
                                                            textAlign: TextAlign.start,
                                                          ),
                                                        ),
                                                        Container(
                                                          margin: EdgeInsets.only(
                                                              top: (5 /
                                                                  Constant
                                                                      .defaultScreenHeight) *
                                                                  screenHeight,
                                                              left:
                                                              (12 / 360) * screenWidth),
                                                          child: Text(
                                                            list_comments[index].durationofcomment,
                                                            style: TextStyle(
                                                              color:
                                                              NeutralColors.blue_grey,
                                                              fontWeight: FontWeight.w700,
                                                              fontFamily: "IBMPlexSans",
                                                              fontStyle: FontStyle.normal,
                                                              fontSize: (12 /
                                                                  Constant
                                                                      .defaultScreenWidth) *
                                                                  screenWidth,
                                                            ),
                                                            textAlign: TextAlign.start,
                                                          ),
                                                        ),
                                                      ],
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ),

                                          ],
                                        ),
                                      ),
                                      GestureDetector(
                                        onTap: () {
                                          // This is a hack because _PopupMenuButtonState is private.
                                          dynamic state = _menuKey.currentState;
                                          state.showButtonMenu();
                                        },
                                        child: Container(
                                          height: (25 / 720) * screenHeight,
                                          //color: Colors.red,
                                          child: Align(
                                            alignment: Alignment.topRight,
                                            child: PopupMenuButton(
                                                icon: Icon(Icons.more_vert,color: NeutralColors.blue_grey,size: 20,),
                                                // padding: EdgeInsets.all(10),
                                                // key: _menuKey,
                                                itemBuilder: (_) =>
                                                <PopupMenuItem<String>>[
                                                  new PopupMenuItem<String>(
                                                      child: const Text(
                                                        'Report',
                                                        style: TextStyle(
                                                          color: NeutralColors
                                                              .gunmetal,
                                                          fontFamily:
                                                          "IBMPlexSans",
                                                          fontWeight:
                                                          FontWeight
                                                              .w400,
                                                          fontSize: 14,
                                                        ),
                                                      ),
                                                      value: 'Report'),
                                                ],
                                                onSelected: (_) {}),

                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                  /**Extended Paragraph colum**/

                                  Container(
                                    margin: EdgeInsets.only(
                                        top: (12 / Constant.defaultScreenHeight) *
                                            screenHeight,
                                        right: (20 / 360) * screenWidth),
                                    child: SingleChildScrollView(
                                      child: Column(
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        children: <Widget>[
                                          Padding(
                                            padding: const EdgeInsets.all(1.0),
                                            child: ReadMoreText(
                                              list_comments[index].comment_text,
                                              trimLines: 3,
                                              colorClickableText: PrimaryColors.azure_Dark,
                                              trimMode: TrimMode.Line,
                                              trimCollapsedText:
                                              SubQueriesCommentsScreenStrings
                                                  .Text_ReadMore,
                                              trimExpandedText:
                                              SubQueriesCommentsScreenStrings
                                                  .Text_ReadLess,
                                              style: TextStyle(
                                                color: NeutralColors.gunmetal,
                                                fontWeight: FontWeight.w500,
                                                fontFamily: "IBMPlexSans",
                                                fontStyle: FontStyle.normal,
                                                height: 1.5,
                                                fontSize: (12/360)*screenWidth,
                                              ),
                                            ),
                                          )
                                        ],
                                      ),
                                    ),
                                  ),
                                  /**upvotes and reply colum**/
                                  Container(
                                  //  color: Colors.yellowAccent,
                                    margin: EdgeInsets.only(
                                        top: (12 /
                                            Constant.defaultScreenHeight) *
                                            screenHeight),
                                    child: Row(
                                      crossAxisAlignment: CrossAxisAlignment.center,
                                      children: <Widget>[
                                        InkWell(
                                          onTap: () {
                                            setState(() {
                                              if(channelCommentsLikes.contains(index)){
                                                channelCommentsLikes.remove(index);
                                              }
                                              else{
                                                channelCommentsLikes.add(index);
                                              }
                                              isSelected = !isSelected;
                                            });
                                          },
                                          child: SvgPicture.asset(
                                              channelCommentsLikes.contains(index) ? ChannelAssets.likeactive
                                                  :ChannelAssets.likeinactive,
                                              fit: BoxFit.fill,
                                            )
                                        ),
                                        Container(
                                            margin: EdgeInsets.only(
                                                top: 0 /
                                                    Constant.defaultScreenHeight *
                                                    screenHeight,
                                                left: 5 / 360 * screenWidth),
                                            decoration: BoxDecoration(
                                              color: Colors.transparent,
                                              borderRadius: BorderRadius.all(
                                                  Radius.circular(2)),
                                            ),
                                            child: Container(
                                              margin: EdgeInsets.only(
                                                  top: 0 /
                                                      Constant.defaultScreenHeight *
                                                      screenHeight),
                                              child: Text(
                                                // SubQueriesCommentsScreenStrings.Text_Liked,
                                                channelCommentsLikes.contains(index) ? '${likes[index]+1} Upvotes' : '${likes[index]} Upvotes',
                                                style: TextStyle(
                                                  color: channelCommentsLikes.contains(index)
                                                      ? NeutralColors.deep_sky_blue
                                                      : NeutralColors.dark_navy_blue,
                                                  fontWeight: FontWeight.w700,
                                                  fontFamily: "IBMPlexSans",
                                                  fontStyle: FontStyle.normal,
                                                  fontSize: (11 /
                                                      Constant
                                                          .defaultScreenWidth) *
                                                      screenWidth,
                                                ),
                                                textAlign: TextAlign.left,
                                              ),
                                            )),
                                        GestureDetector(
                                          onTap: () {
                                            //  AppRoutes.push(context, ChannelReplies());
                                            Navigator.push(
                                                context,
                                                MaterialPageRoute(
                                                  builder: (context) => ChannelReplies(text: 'Reply',index: index,),
                                                ));
                                          },
                                          child: Container(
                                           // color: Colors.red,
                                            margin: EdgeInsets.only(
                                              left: (20 /
                                                  Constant
                                                      .defaultScreenWidth) *
                                                  screenWidth,),
                                            child: Row(
                                              crossAxisAlignment: CrossAxisAlignment.center,
                                              children: <Widget>[
                                                Icon(
                                                  Icons.chat_bubble_outline,
                                                  color: NeutralColors.black,
                                                  size: 11 / 360 * screenWidth,
                                                ),
                                                Container(
                                                    margin: EdgeInsets.only(
                                                        left: 5 / 360 * screenWidth),
                                                    child: Text(
                                                      SubQueriesCommentsScreenStrings
                                                          .Text_Reply,
                                                      style: TextStyle(
                                                        color: NeutralColors.dark_navy_blue,
                                                        fontWeight: FontWeight.w700,
                                                        fontFamily: "IBMPlexSans",
                                                        fontStyle: FontStyle.normal,
                                                        fontSize: (11 /
                                                            Constant
                                                                .defaultScreenWidth) *
                                                            screenWidth,
                                                      ),
                                                      textAlign: TextAlign.left,
                                                    )),
                                              ],
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  /**showreplies colum**/
                                  GestureDetector(
                                    onTap: () {
                                      //  AppRoutes.push(context, ChannelReplies());
                                      Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                            builder: (context) => ChannelReplies(text: 'Show',index: index,),
                                          ));
                                    },
                                    child: Container(
                                    //  color: Colors.purple,
                                        width: double.infinity,
                                        margin: EdgeInsets.only(
                                          top: 5 /
                                              Constant.defaultScreenHeight *
                                              screenHeight,
                                          bottom: 27 /
                                              Constant.defaultScreenHeight *
                                              screenHeight,
                                        ),
                                        child: Container(
                                          margin: EdgeInsets.only(
                                              top: 6 /
                                                  Constant.defaultScreenHeight *
                                                  screenHeight),
                                          child: Text(
                                            SubQueriesCommentsScreenStrings
                                                .Text_ShowReplies,
                                            style: TextStyle(
                                              color: NeutralColors.blue_grey,
                                              fontWeight: FontWeight.w400,
                                              fontFamily: "IBMPlexSans",
                                              fontStyle: FontStyle.normal,
                                              fontSize:
                                              (12 / Constant.defaultScreenWidth) *
                                                  screenWidth,
                                            ),
                                            textAlign: TextAlign.left,
                                          ),
                                        )),
                                  ),
                                ],
                              ),
                            );
                          },
                          separatorBuilder: (context, index) {
                            return Divider();
                          },
                        ),
                      ),
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );

  }

  @override
  // TODO: implement wantKeepAlive
  bool get wantKeepAlive => true;
}

class SampleService {
  static final List<String> degrees = [
    '',
    'Anusha',
    'Vetri',
    'Kavya',
  ];

  static List<String> getSuggestions(String query) {
    List<String> matches = List();
    matches.add("Date");
    matches.add("Upvotes");

    //  matches.retainWhere((s) => s.toLowerCase().contains(query.toLowerCase()));
    return matches;
  }
}

import 'package:flutter/material.dart';
import 'package:imsindia/views/practice_pages/practice_question_review_screen.dart';
import 'package:imsindia/views/practice_pages/practice_questions_page.dart';
import 'package:imsindia/components/stop_watch_timer.dart';
import 'package:imsindia/components/custom_scrollbar_component.dart';
import 'package:imsindia/resources/strings/prepare.dart';
import 'package:imsindia/utils/colors.dart';
import 'package:flutter_html/flutter_html.dart';

var screenWidthTotal;
var screenHeightTotal;

var _safeAreaHorizontal;

var _safeAreaVertical;

var screenHeight;

var screenWidth;
var showAnsForSolution;
var statusForQuestion;
var correctAnswerForMCQTypeQuestions;
class PracticeShowAnswerScreen extends StatefulWidget {
  final int value;
  final Stopwatch stopwatch;
  final int backtoques_num;
  String showAnswer;
  String statusForQuestion;
  String correctAnswer;
  PracticeQuestionsScreenState practiceQuestionsScreen;
  practiceQuestionReviewScreenUsingHtmlState PracticequestionsReviewScreen;

  PracticeShowAnswerScreen({this.value,this.stopwatch,this.backtoques_num,this.showAnswer,this.practiceQuestionsScreen,this.PracticequestionsReviewScreen,this.statusForQuestion,this.correctAnswer});
  @override
  _ShowAnswerScreenHtmlState createState() => _ShowAnswerScreenHtmlState();
}

class _ShowAnswerScreenHtmlState extends State<PracticeShowAnswerScreen> {
  final Dependencies dependencies = new Dependencies();
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    showAnsForSolution=widget.showAnswer;
    statusForQuestion=widget.statusForQuestion;
  }

  @override
  bool pressed = false;
  bool pressAttention = true;

  Widget build(BuildContext context) {
    var _mediaQueryData = MediaQuery.of(context);
    screenWidthTotal = _mediaQueryData.size.width;
    screenHeightTotal = _mediaQueryData.size.height;
    _safeAreaHorizontal =
        _mediaQueryData.padding.left + _mediaQueryData.padding.right;
    _safeAreaVertical =
        _mediaQueryData.padding.top + _mediaQueryData.padding.bottom;

    screenHeight = screenHeightTotal - _safeAreaVertical;
    screenWidth = screenWidthTotal - _safeAreaHorizontal;

    return  Container(
  //      height: (476/720)*screenHeight,
      margin: EdgeInsets.only(left: (20/360)*screenWidth),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Align(
            alignment: Alignment.topLeft,
            child:
            InkWell(
              child: Container(
                width: (150/360)*screenWidth,
                height: (23/720)*screenHeight,
                child: Row(
                  children: [
                    Container(
                        child:Icon(
                          Icons.arrow_back_ios,
                          color: PrimaryColors.azure_Dark,
                          size: 15/720*screenHeight,
                        )
                    ),
                    Container(
                      margin: EdgeInsets.only(left: (8/360)*screenWidth),
                      child:Text(
                        StatisticsScreenStrings.Text_BackToQuestion,
                        style: TextStyle(
                          color: Color.fromARGB(255, 0, 171, 251),
                          fontSize: (14/360)*screenWidth,
                          fontFamily: "IBMPlexSans",
                        ),
                        textAlign: TextAlign.left,


                      ),
                    ),
                  ],
                ),
              ),
              onTap: (){
                setState(() {
                  dependencies.stopwatch = widget.stopwatch;
                  if(widget.practiceQuestionsScreen != null && widget.PracticequestionsReviewScreen == null){
                    widget.practiceQuestionsScreen.setState(() {
                      widget.practiceQuestionsScreen.practiceHeader_status_question=3;
                      widget.practiceQuestionsScreen.pageViewController=PageController(initialPage: widget.backtoques_num-1);
                      widget.practiceQuestionsScreen.widget.stopwatch=widget.stopwatch;
                      widget.practiceQuestionsScreen.panelSlide();
                      widget.practiceQuestionsScreen.widget.practiceStatisticsCheck=false;

                    });
                  }
                  if(widget.PracticequestionsReviewScreen != null && widget.practiceQuestionsScreen == null){
                    widget.PracticequestionsReviewScreen.setState(() {
                      widget.PracticequestionsReviewScreen.practiceHeader_status_question=3;
                      widget.PracticequestionsReviewScreen.practiceSumbit_button_status=null;
                      widget.PracticequestionsReviewScreen.pageViewController=PageController(initialPage: widget.backtoques_num-1);
                      widget.PracticequestionsReviewScreen.widget.stopwatch=widget.stopwatch;
                      widget.PracticequestionsReviewScreen.panelSlide();
                      widget.PracticequestionsReviewScreen.widget.practiceStatisticsCheck=false;

                    });
                  }
                  print(widget.PracticequestionsReviewScreen);
                  print( widget.practiceQuestionsScreen)  ;
                });

              },
            ),

          ),
          Align(
            alignment: Alignment.topLeft,
            child:widget.correctAnswer==null?Container(
              height:(22/720)*screenHeight ,
              //width: (81/360)*screenWidth,
              margin: EdgeInsets.only(top:(21/720)*screenHeight),
              child:Text(
                ShowAnswerScreenStrings.Text_Answer+"A",
                style: TextStyle(
                  color: Color.fromARGB(255, 0, 3, 44),
                  fontSize: (16/360)*screenWidth,
                  fontFamily: "IBMPlexSans",
                  fontWeight: FontWeight.w500,
                ),
                textAlign: TextAlign.left,
              ) ,
            ):Container(
              child:
              Container(
                //height:(22/720)*screenHeight ,
                //width: (81/360)*screenWidth,
                margin: EdgeInsets.only(top:(21/720)*screenHeight),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      ShowAnswerScreenStrings.Text_Answer,
                      style: TextStyle(
                        color: Color.fromARGB(255, 0, 3, 44),
                        fontSize: (16/360)*screenWidth,
                        fontFamily: "IBMPlexSans",
                        fontWeight: FontWeight.w500,
                      ),
                      textAlign: TextAlign.left,
                    ),
                    Container(
                      margin: EdgeInsets.only(right: 20/360*screenWidth),
                      width: 180/360*screenWidth,
                      child: Html(
                        blockSpacing: 0.0,
                        useRichText: false,
                        // backgroundColor:Colors.red ,
                        data:widget.correctAnswer??'',
                        defaultTextStyle:TextStyle(
                          color: Color.fromARGB(255, 0, 3, 44),
                          fontSize: (16/360)*screenWidth,
                          fontFamily: "IBMPlexSans",
                          fontWeight: FontWeight.w500,
                        ),

                      ),
                    ),
                  ],
                ),
              ) ,
            ),
          ),
          Align(
            alignment: Alignment.topLeft,
            child:Container(
              width: (107/360)*screenWidth,
              height: (20/720)*screenHeight,
              margin: EdgeInsets.only(top:(10/720)*screenHeight),
              decoration: BoxDecoration(
                // color:  (widget.statusForQuestion=="true")? PrimaryColors.kelly_green:(widget.statusForQuestion=="false")? PrimaryColors.dark_coral: Colors.yellow,
                border: Border.all(
                  color: (widget.statusForQuestion=="true")? PrimaryColors.kelly_green:(widget.statusForQuestion=="false")? PrimaryColors.dark_coral: Colors.yellow,

                  width: 1,
                ),
                borderRadius: BorderRadius.all(Radius.circular(2)),
              ),
              child:  Center(child:Text(
                (widget.statusForQuestion=="true")? Preparequestions.correctStatus:(widget.statusForQuestion=="false")? Preparequestions.wrongStatusForReview: Preparequestions.skippedStatus,
                style: TextStyle(
                  color:(widget.statusForQuestion=="true")? PrimaryColors.kelly_green:(widget.statusForQuestion=="false")? PrimaryColors.dark_coral: Colors.yellow,
                  fontSize: (12/720)*screenHeight,
                  fontFamily: "IBMPlexSans",
                  fontWeight: FontWeight.w500,
                ),
                textAlign: TextAlign.center,
              ),),
            ),),
          Expanded(
            //alignment: Alignment.topLeft,
            child:Container(
              // width: (52/360)*screenWidth,
            //   color: Colors.red,
             height: (pressed==false)?
             (23/720)*screenHeight:null,
             margin: EdgeInsets.only(top: (20/720)*screenHeight,right:(0/360)*screenWidth),
              child: InkWell(
                  onTap: (){

                    setState(() {
                      pressed = true;
                    });
                  },
                  child:(pressed==false)?
                   Text(
                    ShowAnswerScreenStrings.Text_Solution,
                    style: TextStyle(
                      color: Color.fromARGB(255, 0, 171, 251),
                      fontSize: (14/360)*screenWidth,
                      fontFamily: "IBMPlexSans",
                    ),
                    textAlign: TextAlign.justify,
                  )
                  :_buildContainer()
              ),
            ),),
        ],
      ),
    );
  }
}
Widget _buildContainer() {
  final ScrollController controller = ScrollController();
  return DraggableScrollbar(
    controller: controller,
    heightScrollThumb: 78.0/720*screenHeight,
    weightScrollThumb: 3/360*screenWidth,
    colorScrollThumb: Color(0xffcecece),
    marginScrollThumb: EdgeInsets.only(right: 6/360*screenWidth,top:30/720*screenHeight),

    child:Column(
      children: <Widget>[
        Expanded(
          //height: (336/720)*screenHeight,
          //width: (360)*screenHeight,
          //margin: EdgeInsets.only(top:(20/720)*screenHeight ),
          child: ListView(
            controller: controller,
          //  shrinkWrap: true,
            padding: EdgeInsets.only(top:0/720*screenHeight),
            children:[
              Container(
                margin: EdgeInsets.only(right: 20/360*screenWidth),
                child: Html(
                  data:showAnsForSolution,
                  useRichText: false,
                  defaultTextStyle:TextStyle(
                    color: Color.fromARGB(255, 87, 93, 96),
                    fontSize: (16/360)*screenWidth,
                    fontFamily: "IBMPlexSans",
                  ),
                ),
              ),],
          ),),
      ],
    ),
  );
}
import 'package:flutter/material.dart';

class PracticeQuestionsOptions{
  String qid;
  String question;
  String answer;
  String paragraph;
  var options;
  PageController controller;
  int pos;
  var imageURL;
  List<Map<String, dynamic>> option;

  PracticeQuestionsOptions({
    this.qid,
    this.question,
    this.paragraph,
    this.options,
    this.controller,
    this.imageURL,
    this.answer,
    this.option
  });
  PracticeQuestionsOptions.pos(this.pos);

}

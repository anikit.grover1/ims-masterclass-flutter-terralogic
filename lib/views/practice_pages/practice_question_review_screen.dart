import 'package:flutter_html/image_properties.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:imsindia/resources/strings/prepare.dart';
import 'package:imsindia/utils/colors.dart';
import 'package:imsindia/utils/svg_images/prepare_svg_images.dart';
import 'package:imsindia/components/stop_watch_timer.dart';
import 'package:imsindia/views/practice_pages/practice_statistics_screen.dart';
import 'package:imsindia/views/practice_pages/show_answer_screen.dart';
import 'dart:async';
import 'package:sliding_up_panel/sliding_up_panel.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:imsindia/utils/Url.dart';
import 'package:imsindia/components/custom_scrollbar_component.dart';
import 'package:imsindia/utils/global.dart' as global;
import 'package:imsindia/api/bloc/services.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:wakelock/wakelock.dart';

var screenWidthTotal;
var screenHeightTotal;
var _safeAreaHorizontal;
var _safeAreaVertical;
var screenHeight;
var screenWidth;

int questionNumberInsideQuestionDropDown = 0;
bool isQuestionDropDownClickchecked = false;
var checking ;

class PracticeQuestionReviewScreenUsingHtml extends StatefulWidget {
  final int practice_header_status;
  final bool practice_sumbit_status;
  final bool timer_status;
  Stopwatch stopwatch;
  bool  practiceStatisticsCheck;
  final int backtoques_num;
  final int backtoquesnum_bargraph;
  var image_value;
  var image_value_for_option;
  PracticeQuestionReviewScreenUsingHtml({
    this.practice_header_status,
    this.timer_status,
    this.stopwatch,
    this.practice_sumbit_status,
    this.backtoques_num,
    this.backtoquesnum_bargraph,
    this.image_value,
    this.image_value_for_option,
    this.practiceStatisticsCheck,
  });

  @override
  practiceQuestionReviewScreenUsingHtmlState createState() =>
      practiceQuestionReviewScreenUsingHtmlState();
}

class practiceQuestionReviewScreenUsingHtmlState
    extends State<PracticeQuestionReviewScreenUsingHtml> {



  bool isOptionsSelected;
  String answer_status;
  double contentHeight = 0;
  double contentHeight1 = 0;
  final Dependencies dependencies = new Dependencies();
  SharedPreferences practicePrefs;
  bool practicePopUpForResumeAndBackCheck = true;
  List practiceBookMarkedQuestionsList = List<String>();
  List practiceSolutionCheckQuestionsList = List<String>();
  List practiceStastisticsCheckQuestionsList = List<String>();
  List<dynamic> practiceQuestionNoTimeList = [];
  var practiceBookMarkedQuestionsAfterSharedPref;
  var practiceSolutionCheckQuestionsAfterSharedPref;
  var practiceStatisticsCheckQuestionsAfterSharedPref;
  int practiceHeader_status_question;
  bool practiceSumbit_button_status;
  bool practiceOverLayEntryCheck = false;
  final _practiceControllerForPopUp = PanelController();
  double _practiceCpanelHeightClosed = 0.0;
  bool practiceBookMarkCheck = false;
  bool practiceSoloutionCheck = false;
  int practicePositionForQuestion = 1, practicetotalpage;
  static OverlayEntry entry = null;
  bool get isshow => entry != null;
  void show(context) => addOverlayEntry(context);
  void hide() => removeOverlay();
  bool isChecked = false;
  static const _kDuration = const Duration(milliseconds: 300);
  static const _kCurve = Curves.easeIn;
  int currentPage = 1;
  int totalpage;
  int index = 0;
  final ScrollController controller = ScrollController();
  bool iscardchecked = false;
  //final pageViewController = new PageController();
  PageController pageViewController;
  bool questionArrowCheck = false;
  var accessToken;
  var testStatus;
  var nameOfTheTopic;
  var totalnumberOfQuestions = 0;
  var totalQuestions = [];
  var totalOptions = [];
  var answerList = [];
  var timetaken = [];
  var questionsStatus=[];
  var bookmarkorUnmarkList=[];
  var selectedoptionidForTrack = [];
  var finalTotalOptionIDAccordingToQuestionsForTrack = [];
  var selectedOptionId;
  var selectedOptionIdList=[];
  var finalTotalOptionsAccordingToQuestions = [];
  var finalTotalAnswerListAccordingToQuestion = [];
  var  directionsForQuestion = [];

  var correctAnswerAccordingToQues;
  bool _isLoadingPage;
  var heightOfTheTitleWidget;
  GlobalKey _keyRed = GlobalKey();
  List paragraphCheck = [];
  List enteredText = [];
  List questionsType = [];
  List showAnswerExplanation = [];
  String filePathForImg =
      'https://imsclick2cert.blob.core.windows.net/imsitemimages/';


  List lettersForQuestions = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K','L', 'M', 'N'];
  List correctAnswerList=[];

  List<Widget> _getTiles() {
    final List<Widget> tiles = <Widget>[];

    for (int iteration = 0; iteration < totalQuestions.length; iteration++) {
      tiles.add(new GridTile(
          child: new InkResponse(
              enableFeedback: true,
              onTap: () {
                setState(() {
                  practiceQuestionNoTimeList.add([
                    iteration + 1,
                    dependencies.stopwatch.elapsedMilliseconds
                  ]);
                  dependencies.stopwatch.reset();
                  practicePositionForQuestion = iteration + 1;
                  questionNumberInsideQuestionDropDown =
                      practicePositionForQuestion;
                  isQuestionDropDownClickchecked = true;
                  if(practiceHeader_status_question!=1&&practiceHeader_status_question!=2){
                    pageViewController.jumpToPage(practicePositionForQuestion - 1);
                  }

                  practiceOverLayEntryCheck
                      ? practiceOverLayEntryCheck = false
                      : practiceOverLayEntryCheck = true;
                  hide();
                });
              },
              child: Stack(
                children: <Widget>[
                  Container(
                    decoration: BoxDecoration(
                      border: Border.all(color:(questionsStatus[iteration]==true)? PrimaryColors.kelly_green:(questionsStatus[iteration]==false)? PrimaryColors.dark_coral: Colors.yellow, width: 0.5),
                      boxShadow: [
                        BoxShadow(color: Colors.white
                          // color:Colors.blue,
                        ),
                      ],
                      //  borderSide: const BorderSide(color: Colors.grey, width: 0.0),
                      borderRadius: BorderRadius.all(Radius.circular(6)),
                    ),
                    child: Align(
                      alignment: Alignment.center,
                      child: Text(
                        (iteration + 1).toString(),
                        style: TextStyle(
                          color: Color.fromARGB(255, 0, 3, 44),
                          fontSize: 14 / 360 * screenWidth,
                          fontFamily: "IBMPlexSans",
                          fontWeight: FontWeight.w500,
                        ),
                        textAlign: TextAlign.center,
                      ),
                    ),
                  ),
                  Positioned(
                    top: 0.0,
                    right: 3.0 / 360 * screenWidth,
                    child: Align(
                      alignment: Alignment.topRight,
                      child: Container(
                          margin: EdgeInsets.only(
                              top: (2 / 720) * screenHeight,
                              left: (25 / 360) * screenWidth),
                          child: Align(
                              alignment: Alignment.topRight,
                              child: (bookmarkorUnmarkList[iteration] == true)
                                  ?Image.asset(
                                "assets/images/bookmark-selected.png",
                                height: (11.3 / 720) * screenHeight,
                                width: (7 / 360) * screenWidth,
                                fit: BoxFit.fitHeight,
                                color: (questionsStatus[iteration]==true)? PrimaryColors.kelly_green:(questionsStatus[iteration]==false)? PrimaryColors.dark_coral: Colors.yellow,
                              )
                                  : Text(""))
                      ),
                    ),
                  ),
                ],
              ))
      ));
    }
    return tiles;
  }

  _getSizes() {
    final RenderBox renderBoxRed = _keyRed.currentContext.findRenderObject();
    heightOfTheTitleWidget = renderBoxRed.size.height;
  }

  addOverlayEntry(context) {
    _getSizes();

    questionArrowCheck = true;
    if (entry != null) return;
    entry = new OverlayEntry(builder: (BuildContext context) {
      return LayoutBuilder(builder: (_, BoxConstraints constraints) {
        return Stack(
          children: <Widget>[
            Positioned(
              top: heightOfTheTitleWidget < 53.87
                  ? 150 / 720 * screenHeight
                  : 173.7 / 720 * screenHeight,
              right: 20 / 360 * screenWidth,
              left: 19 / 360 * screenWidth,
              child: Material(
                color: Colors.white,
                child: Container(
                  width: 320 / 360 * screenWidth,
                  height: totalQuestions.length > 30
                      ? 240 / 720 * screenHeight
                      : null,
                  margin: EdgeInsets.only(top: 8 / 720 * screenHeight),
                  // color: Colors.white,
                  decoration: BoxDecoration(
                    color: Colors.white,
                    //border: Border.all(color: NeutralColors.pureWhite , width: 0.5),
                    boxShadow: [
                      BoxShadow(
                        offset: Offset(0, 5),
                        blurRadius: 10,
                        color: Color(0xffeaeaea),
                        //offset: Offset.lerp(Offset(10.0,-10.0), Offset(10.0,10.0), 1),
                        // offset: Offset(0,10.0),
                        //color: Colors.orange,
                      ),
                    ],
                    borderRadius: BorderRadius.all(Radius.circular(6)),
                  ),
//                  height: 35/720*screenHeight,
//                  width: 35/360*screenWidth,
                  child: Column(
                    children: <Widget>[
                      totalQuestions.length > 30
                          ? Expanded(
                        child: new GridView.count(
                          crossAxisCount: 6,
                          childAspectRatio: (screenHeight / 600),
                          controller: new ScrollController(
                              keepScrollOffset: false),
                          shrinkWrap: true,
                          scrollDirection: Axis.vertical,
                          padding: const EdgeInsets.all(20.0),
                          mainAxisSpacing: 10.0,
                          crossAxisSpacing: 10.0,
                          children: _getTiles(),
                        ),
                      )
                          : new GridView.count(
                        crossAxisCount: 6,
                        childAspectRatio: (screenHeight / 600),
                        controller:
                        new ScrollController(keepScrollOffset: false),
                        shrinkWrap: true,
                        scrollDirection: Axis.vertical,
                        padding: const EdgeInsets.all(20.0),
                        mainAxisSpacing: 10.0,
                        crossAxisSpacing: 10.0,
                        children: _getTiles(),
                      ),
                    ],
                  ),
                ),
              ),
            )
          ],
        );
      });
    });

    addoverlay(entry, context);
  }

  static addoverlay(OverlayEntry entry, context) async {
    Overlay.of(context).insert(entry);
  }

  removeOverlay() {
    questionArrowCheck = false;
    entry?.remove();
    entry = null;
  }



  void _onScroll() {
    isOptionsSelected = false;
  }




  /// getting test status for test launch/revise api call ///
  getTestStatusToCallTestLaunchOrReviseApi() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    testStatus = prefs.getString("testStatusPractice");
  }

  /// getting access token from shared pref ///
  void getAccessTokenForReviceApi() async {
    print("inside api");
    SharedPreferences prefs = await SharedPreferences.getInstance();
    accessToken = prefs.getString("accessTokenPractice");
    print( accessToken.toString());
    print(testStatus);
    getTestStatusToCallTestLaunchOrReviseApi().then((_) {
      if(testStatus=='Completed'){
        global.getToken.then((t){
          getAllDataForReviewSection(t);
        });
      }
    });
  }


  /// revise Api ///
  void getAllDataForReviewSection(Map t) async{
    print("inside api");
    print(URL.GET_QUESTIONS_DATA_FOR_PREPARE_REVISE_SECTION);
    if (global.headersWithAuthorizationKey['Authorization'] != '') {
      Map postdata = {"token": accessToken.toString()};
      print( accessToken.toString());
      ApiService()
          .postAPI(URL.GET_QUESTIONS_DATA_FOR_PREPARE_REVISE_SECTION, postdata,
          t).then((result) {
        setState(() {
          if (result[0].toString().toLowerCase() == 'success'.toLowerCase()) {
            var totalData = result[1]['data'];
            nameOfTheTopic = totalData['testJson']['Name'];
            totalnumberOfQuestions = totalData['testJson']['SectionResponse'][0]['TotalQuestions'];
            for (var i = 0; i < (totalData['testJson']['SectionResponse'][0]['ItemResponse']).length; i++) {
              paragraphCheck.add(totalData['testJson']['SectionResponse'][0]['ItemResponse'][i]['Passage']);

              enteredText.add(totalData['testJson']['SectionResponse'][0]['ItemResponse'][i]['EnteredResponse']);
              questionsType.add(totalData['testJson']['SectionResponse'][0]['ItemResponse'][i]['ItemType']);
              if (totalData['testJson']['SectionResponse'][0]['ItemResponse'][i]['Explanation'].contains('src')) {
                if(totalData['testJson']['SectionResponse'][0]['ItemResponse'][i]['Explanation'].contains("@@@~")) {
                  String img = (totalData['testJson']['SectionResponse'][0]['ItemResponse'][i]['Explanation'].split("@@@~"))[0].replaceAll('QUIZKYIMAGEREPO/', filePathForImg);
                  showAnswerExplanation.add(img);
                }
                else{
                  String img = totalData['testJson']['SectionResponse'][0]['ItemResponse'][i]['Explanation'].replaceAll('QUIZKYIMAGEREPO/', filePathForImg);
                  showAnswerExplanation.add(img);
                }
              }
              else {
                print("explanationnnnnnnnnnnnnnnnnnnnnnnnnnnnnnn");
                print((totalData['testJson']['SectionResponse'][0]['ItemResponse'][i]['Explanation'].split("@@@~"))[0]);
                if(totalData['testJson']['SectionResponse'][0]['ItemResponse'][i]['Explanation'].contains("@@@~")){
                  showAnswerExplanation.add((totalData['testJson']['SectionResponse'][0]['ItemResponse'][i]['Explanation'].split("@@@~"))[0]);
                }else{
                  showAnswerExplanation.add(totalData['testJson']['SectionResponse'][0]['ItemResponse'][i]['Explanation']);
                }
              }
              if (totalData['testJson']['SectionResponse'][0]['ItemResponse'][i]['Items'].contains('src')) {
                String img = totalData['testJson']['SectionResponse'][0]['ItemResponse'][i]['Items'].replaceAll('QUIZKYIMAGEREPO/', filePathForImg);
//                if(img.contains("Enter your response")){
//                  var question;
//                  question=(img.split("Enter your response"))[0];
//                  if(question.contains("Rs[quizky-text]")){
//                    totalQuestions.add((question.replaceAll("Rs[quizky-text]","")));
//                  }else if(question.contains("[quizky-text]")){
//                    totalQuestions.add((question.replaceAll("[quizky-text]","")));
//                  }else{
//                    totalQuestions.add((img.split("Enter your response"))[0]);
//                  }
//                }
                 if(img.contains("Rs[quizky-text]")){
                  var question=(img.split("Rs[quizky-text]"))[0];
//                  if(question.contains("Enter your response")){
//                    totalQuestions.add((question.split("Enter your response"))[0]);
//                  }else{
                    totalQuestions.add((img.replaceAll("Rs[quizky-text]","")));
//                  }
                }
                else if(img.contains("[quizky-text]")){
                  var question=(img.split("[quizky-text]"))[0];
//                  if(question.contains("Enter your response")){
//                    totalQuestions.add((question.split("Enter your response"))[0]);
//                  }else{
                    totalQuestions.add((img.replaceAll("[quizky-text]","")));
//                  }
                }
                else{
                  totalQuestions.add(img);
                }
              } else {
//                if(totalData['testJson']['SectionResponse'][0]['ItemResponse'][i]['Items'].contains("Enter your response")){
//                  var question;
//                  question=(totalData['testJson']['SectionResponse'][0]['ItemResponse'][i]['Items'].split("Enter your response"))[0];
//                  if(question.contains("Rs[quizky-text]")){
//                    totalQuestions.add((question.replaceAll("Rs[quizky-text]","")));
//                  }else if(question.contains("[quizky-text]")){
//                    totalQuestions.add((question.replaceAll("[quizky-text]","")));
//                  }else{
//                    totalQuestions.add((totalData['testJson']['SectionResponse'][0]['ItemResponse'][i]['Items'].split("Enter your response"))[0]);
//                  }
//                }
                 if(totalData['testJson']['SectionResponse'][0]['ItemResponse'][i]['Items'].contains("Rs[quizky-text]")){
                  var question=(totalData['testJson']['SectionResponse'][0]['ItemResponse'][i]['Items'].split("Rs[quizky-text]"))[0];
//                  if(question.contains("Enter your response")){
//                    totalQuestions.add((question.split("Enter your response"))[0]);
//                  }else{
                    totalQuestions.add((totalData['testJson']['SectionResponse'][0]['ItemResponse'][i]['Items'].replaceAll("Rs[quizky-text]","")));
//                  }
                }
                else if(totalData['testJson']['SectionResponse'][0]['ItemResponse'][i]['Items'].contains("[quizky-text]")){
                  var question=(totalData['testJson']['SectionResponse'][0]['ItemResponse'][i]['Items'].replaceAll("[quizky-text]",""));
//                  if(question.contains("Enter your response")){
//                    totalQuestions.add((question.split("Enter your response"))[0]);
//                  }else{
                    totalQuestions.add((totalData['testJson']['SectionResponse'][0]['ItemResponse'][i]['Items'].replaceAll("[quizky-text]","")));
//                  }
                }
                else{
                  totalQuestions.add(totalData['testJson']['SectionResponse'][0]['ItemResponse'][i]['Items']);
                }
              }
//              if (totalData['testJson']['SectionResponse'][0]['ItemResponse'][i]['Direction'].contains('src')) {
//                String img = totalData['testJson']['SectionResponse'][0]['ItemResponse'][i]['Direction'].replaceAll('QUIZKYIMAGEREPO/', filePathForImg);
//                directionsForQuestion.add(img);
//              } else {
                //directionsForQuestion.add(totalData['testJson']['SectionResponse'][0]['ItemResponse'][i]['Direction']);
         //     }
              if(totalData['testJson']['SectionResponse'][0]['ItemResponse'][i]['Direction']!=null&&totalData['testJson']['SectionResponse'][0]['ItemResponse'][i]['Direction']!=" "&&totalData['testJson']['SectionResponse'][0]['ItemResponse'][i]['Direction']!=""){

                if (totalData['testJson']['SectionResponse'][0]['ItemResponse'][i]['Direction'].contains('src')) {
                  String img = totalData['testJson']['SectionResponse'][0]['ItemResponse'][i]['Direction'].replaceAll('QUIZKYIMAGEREPO/', filePathForImg);
                  directionsForQuestion.add(img);
                } else {
                  directionsForQuestion.add(totalData['testJson']['SectionResponse'][0]['ItemResponse'][i]['Direction']);
                }}
              else{
                directionsForQuestion.add(totalData['testJson']['SectionResponse'][0]['ItemResponse'][i]['Direction']);

              }
              for (var j = 0; j < totalData['testJson']['SectionResponse'][0]['ItemResponse'][i]['ItemOptionResponse'].length; j++) {
                if (totalData['testJson']['SectionResponse'][0]['ItemResponse'][i]['ItemOptionResponse'][j]['Options'].contains('src')) {
                  String img = totalData['testJson']['SectionResponse'][0]['ItemResponse'][i]['ItemOptionResponse'][j]['Options'].replaceAll('QUIZKYIMAGEREPO/', filePathForImg);
                  totalOptions.add(img);
                } else {
                  totalOptions.add(totalData['testJson']['SectionResponse'][0]['ItemResponse'][i]['ItemOptionResponse'][j]['Options']);
                }
                selectedoptionidForTrack.add(totalData['testJson']['SectionResponse'][0]['ItemResponse'][i]['ItemOptionResponse'][j]['ItemOptionID']);
                answerList.add(totalData['testJson']['SectionResponse'][0]['ItemResponse'][i]['ItemOptionResponse'][j]['IsCorrect']);

              }
              finalTotalOptionsAccordingToQuestions.add(totalOptions);
              finalTotalAnswerListAccordingToQuestion.add(answerList);
              finalTotalOptionIDAccordingToQuestionsForTrack.add(selectedoptionidForTrack);
              selectedoptionidForTrack = [];
              totalOptions = [];
              answerList = [];
            }
            print(finalTotalOptionIDAccordingToQuestionsForTrack);
            print("finalTotalOptionIDAccordingToQuestionsForTrackfinalTotalOptionIDAccordingToQuestionsForTrackfinalTotalOptionIDAccordingToQuestionsForTrack");

            /// to get the correct answer ///
            for(var i=0 ; i<questionsType.length;i++){
              for(var j=0; j < totalData['testJson']['SectionResponse'][0]['ItemResponse'][i]['ItemOptionResponse'].length; j++){
                if(totalData['testJson']['SectionResponse'][0]['ItemResponse'][i]['ItemOptionResponse'][j]['IsCorrect']==true){
                  if (totalData['testJson']['SectionResponse'][0]['ItemResponse'][i]['ItemOptionResponse'][j]['Options'].contains('src')) {
                    print("inside iffffffffff");
                    String img = totalData['testJson']['SectionResponse'][0]['ItemResponse'][i]['ItemOptionResponse'][j]['Options'].replaceAll('QUIZKYIMAGEREPO/', filePathForImg);
                    correctAnswerList.add(img);
                  } else {
                    correctAnswerList.add(totalData['testJson']['SectionResponse'][0]['ItemResponse'][i]['ItemOptionResponse'][j]['Options']);
                  }
                }
              }
            }
            print(correctAnswerList);
            print("correctAnswerListcorrectAnswerList");
            if(paragraphCheck[practicePositionForQuestion-1]!=null){
              checking =null;
            }else{
              checking =false;
            }
            var studentItems = totalData["studentItems"];
            for (var j = 0;j < totalnumberOfQuestions;j++){
              for(var i = 0; i < studentItems.length ; i ++){
                if(totalData['testJson']['SectionResponse'][0]['ItemResponse'][j]['ItemID'].toString() == studentItems[i]['itemId']){
                  timetaken.add(_printDuration(studentItems[i]["timeTaken"]));
                  questionsStatus.add(studentItems[i]["isCorrect"]);
                  bookmarkorUnmarkList.add(studentItems[i]["marked"]);
                  print("status valuessssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss");
                  print(studentItems[i]["isCorrect"].toString() + studentItems[i]["selectedOptionId"].toString());
                  if(studentItems[i]["isCorrect"]==null &&(studentItems[i]["selectedOptionId"]=='0'||studentItems[i]["selectedOptionId"]==null) ){
                    selectedOptionIdList.add("-1");
                  }else if(studentItems[i]["itemType"]!="MCQ"&&studentItems[i]["itemType"]!="mcq"&&studentItems[i]["itemType"]!=null ){
                    selectedOptionIdList.add(studentItems[i]["enteredText"]);
                  } else if((studentItems[i]["enteredText"]!="null"||studentItems[i]["enteredText"]!=null)&&(studentItems[i]["selectedOptionId"]!='0'||studentItems[i]["selectedOptionId"]!=null)){
                    selectedOptionIdList.add(studentItems[i]["selectedOptionId"]);
                  }
                  else{
                    for( var k=0;k<totalData['testJson']['SectionResponse'][0]['ItemResponse'][j]['ItemOptionResponse'].length;k++) {
                      if (studentItems[i]["selectedOptionId"] == (totalData['testJson']['SectionResponse'][0]['ItemResponse'][j]['ItemOptionResponse'][k]['ItemOptionID']).toString()) {
                        selectedOptionIdList.add(studentItems[i]["selectedOptionId"]);
                      }
                    }
                  }
                }
              }

            }
            print(selectedOptionIdList.length);
            print(selectedOptionIdList);
            print(questionsStatus.length);
            print(finalTotalAnswerListAccordingToQuestion);
          } else {
          }
        });
      });
    }
  }


  String _printDuration(int time) {
    Duration duration = new Duration(seconds: time);
    String twoDigits(int n) {
      if (n >= 10) return "$n";
      return "0$n";
    }

    String twoDigitMinutes = twoDigits(duration.inMinutes.remainder(60));
    String twoDigitSeconds = twoDigits(duration.inSeconds.remainder(60));
    return "${twoDigits(duration.inHours)}:$twoDigitMinutes:$twoDigitSeconds";


  }
  @override
  void initState() {
    print("practice review screen");
        Wakelock.enable();

    questionsStatus=[];
    _isLoadingPage = true;
    widget.practiceStatisticsCheck=false;
    pageViewController = PageController(
        keepPage: true,
        initialPage: 0)
      ..addListener(_onScroll);
    // TODO: implement initState
    practiceHeader_status_question = widget.practice_header_status;
    practiceSumbit_button_status = widget.practice_sumbit_status;
    super.initState();
    setState(() {
      if (practiceSoloutionCheck == true) {
        index = widget.backtoques_num;
      }
        practicePositionForQuestion = 1;


    });

    getAccessTokenForReviceApi();
  }

  panelSlide() {
    if (widget.stopwatch == null) {
      dependencies.stopwatch.start();
    } else if (widget.stopwatch != null) {
      dependencies.stopwatch = widget.stopwatch;
      dependencies.stopwatch.stop();
    }
  }

  Future<void> executeAfterBuild() async {
    if (widget.stopwatch == null) {
      dependencies.stopwatch.start();
    } else if (widget.stopwatch != null) {
      dependencies.stopwatch = widget.stopwatch;
      dependencies.stopwatch.stop();
    }
    // this code will get executed after the build method
    // because of the way async functions are scheduled
  }

  Widget build(BuildContext context) {
    var _mediaQueryData = MediaQuery.of(context);
    screenWidthTotal = _mediaQueryData.size.width;
    screenHeightTotal = _mediaQueryData.size.height;
    _safeAreaHorizontal =
        _mediaQueryData.padding.left + _mediaQueryData.padding.right;
    _safeAreaVertical =
        _mediaQueryData.padding.top + _mediaQueryData.padding.bottom;
    screenHeight = screenHeightTotal - _safeAreaVertical;
    screenWidth = screenWidthTotal - _safeAreaHorizontal;
    Future<bool> _onWillPop() {
      setState(() {
        practicePopUpForResumeAndBackCheck = false;
        practiceOverLayEntryCheck
            ? practiceOverLayEntryCheck = false
            : practiceOverLayEntryCheck = true;
        hide();
      });
      if (_practiceControllerForPopUp.isPanelClosed) {
        _practiceControllerForPopUp.open();
      } else {
        _practiceControllerForPopUp.close();
      }
    }

    return WillPopScope(
      onWillPop: _onWillPop,
      child: Scaffold(
        body: Container(
          color: Colors.white,
          child: SlidingUpPanel(
            onPanelClosed: () => setState((){
            }) ,
            onPanelSlide: (double pos) => setState((){

            }),
            onPanelOpened:  () => setState((){
            }) ,
            maxHeight: screenHeight * (0.76),
            minHeight: _practiceCpanelHeightClosed,
            parallaxEnabled: false,
            parallaxOffset: 0.5,
            //defaultPanelState: PanelState.CLOSED,
            backdropEnabled: true,
            backdropTapClosesPanel: true,
            isDraggable: true,
            body: _body(context),
            panel: (_popUpForBackButton(context)),
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(25.0),
                topRight: Radius.circular(25.0)),
            controller: _practiceControllerForPopUp,
          ),
        ),
      ),
    );
  }

  Widget _body(context) {
    return Container(
      constraints: BoxConstraints.expand(),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          totalQuestions.length == 0
              ? Container()
          /// title and time taken ///
              : GestureDetector(
            onTap: () {
              setState(() {
                practiceOverLayEntryCheck
                    ? practiceOverLayEntryCheck = false
                    : practiceOverLayEntryCheck = true;
                hide();
              });
            },
            child: Container(
              //  height: 54 / 720 * screenHeight,
              width: double.infinity,
              margin: EdgeInsets.only(top: 38 / 720 * screenHeight),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  InkWell(
                    onTap: () {
                      setState(() {
                        practicePopUpForResumeAndBackCheck = false;
                        practiceOverLayEntryCheck
                            ? practiceOverLayEntryCheck = false
                            : practiceOverLayEntryCheck = true;
                        hide();
                      });
                      if (_practiceControllerForPopUp.isPanelClosed) {
                        _practiceControllerForPopUp.open();
                      } else {
                        _practiceControllerForPopUp.close();
                      }
                    },
                    child: Container(
                      height: (14 / 678) * screenHeight,
                      width: (16 / 360) * screenWidth,
                      margin: EdgeInsets.only(
                          left: 18 / 360 * screenWidth,
                          top: 12.5 / 720 * screenHeight),
                      child: SvgPicture.asset(
                        getPrepareSvgImages.backIcon,
                        fit: BoxFit.fill,
                      ),
                    ),
                  ),
                  Container(
                    key: _keyRed,
                    width: 200 / 360 * screenWidth,
                    margin: EdgeInsets.only(
                        left: 12 / 360 * screenWidth,
                        top: 10 / 720 * screenHeight),
                    child: Text(
                      nameOfTheTopic == null
                          ? "practice"
                          : nameOfTheTopic.toString(),
                      maxLines: 3,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(
                        color: NeutralColors.black,
                        fontSize: 18 / 360 * screenWidth,
                        fontFamily: "IBMPlexSans",
                        fontWeight: FontWeight.w700,
                      ),
                      textAlign: TextAlign.left,
                    ),
                  ),
                  Container(
                    width: 73 / 360 * screenWidth,
                    margin: EdgeInsets.only(
                      top: 10 / 720 * screenHeight,
                      left: 20 / 360 * screenWidth,
                    ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: <Widget>[
                        Container(
                          child: Text(
                            "Time Taken",
                            style: TextStyle(
                              color: NeutralColors.dark_navy_blue,
                              fontSize: 12 / 360 * screenWidth,
                              fontFamily: "IBMPlexSans",
                              fontWeight: FontWeight.w500,
                            ),
                            textAlign: TextAlign.left,
                                 ),

                        ),
                        Container(
                          margin: EdgeInsets.only(
                              left: 10 / 360 * screenWidth, bottom: 0.0),
                          child: Text(timetaken[practicePositionForQuestion-1],
                            style: TextStyle(
                              color: NeutralColors.dark_navy_blue,
                              fontSize: 12 / 360 * screenWidth,
                              fontFamily: "IBMPlexSans",
                              fontWeight: FontWeight.w500,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
          totalQuestions.length == 0
              ? Container()
          /// questions card and hint symbols ///
              : GestureDetector(
            onTap: () {
              setState(() {
                practiceOverLayEntryCheck
                    ? practiceOverLayEntryCheck = false
                    : practiceOverLayEntryCheck = true;
                hide();
              });
            },
            child: Container(
              height: (30 / 640) * screenHeight,
              width: (320 / 360) * screenWidth,
              margin: EdgeInsets.only(
                  top: (30 / 640) * screenHeight,
                  left: (20 / 360) * screenWidth,
                  right: (20 / 360) * screenWidth),
              child: Row(
                children: <Widget>[
                  /// question dropdown ............ ///
                  Container(
                    decoration: BoxDecoration(
                      color: NeutralColors.purpleish_blue,
                      borderRadius: BorderRadius.all(Radius.circular(2)),
                    ),
                    height: (30 / 640) * screenHeight,
                    width: (170 / 360) * screenWidth,
                    margin: EdgeInsets.only(
                        right: (55 / 360) * screenWidth, left: 0.0),
                    child: InkWell(
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Container(
                            height: (18 / 640) * screenHeight,
                            margin: EdgeInsets.only(
                                left: (15 / 360) * screenWidth,
                                top: (6 / 640) * screenHeight,
                                bottom: (6 / 640) * screenHeight),
                            child: Text(
                              PrepareParagraphScreenStrings.question +
                                  "${practicePositionForQuestion}/${totalnumberOfQuestions.toString()}",
                              style: TextStyle(
                                color: NeutralColors.pureWhite,
                                fontSize: 14 / 640 * screenHeight,
                                fontFamily: "IBMPlexSans",
                                fontWeight: FontWeight.w500,
                              ),
                              textAlign: TextAlign.left,
                            ),
                          ),
                          Container(
                              width: (11 / 360) * screenWidth,
                              margin: EdgeInsets.only(
                                right: (15 / 360) * screenWidth,
                              ),
                              child: Center(
                                  child: Icon(
                                    (practiceHeader_status_question!=1&&practiceHeader_status_question!=2)?
                                    questionArrowCheck
                                        ? Icons.keyboard_arrow_up
                                        : Icons.keyboard_arrow_down:null,
                                    color: NeutralColors.pureWhite,
                                    size: 22.0 / 360 * screenWidth,
                                  )))
                        ],
                      ),
                      onTap: () {
                        setState(() {
                          practiceOverLayEntryCheck
                              ? practiceOverLayEntryCheck = false
                              : practiceOverLayEntryCheck = true;
                          (practiceHeader_status_question!=1&&practiceHeader_status_question!=2)?
                          practiceOverLayEntryCheck
                              ? show(context)
                              : hide():null;
                        });
                      },
                    ),
                  ),
                  Container(
                    width: 95 / 360 * screenWidth,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: <Widget>[
                        /// solution .......... ///
                        InkWell(
                          onTap: () {
                            checking != null?checking != true
                                ? setState(() {
                              dependencies.stopwatch.stop();
                              practiceHeader_status_question = 1;
                              practiceSumbit_button_status = false;
                              practiceSoloutionCheck = true;
                              practiceOverLayEntryCheck
                                  ? practiceOverLayEntryCheck = false
                                  : practiceOverLayEntryCheck = true;
                              hide();
                            })
                                : null:null;
                          },
                          child: Container(
                            child: Padding(
                              padding: const EdgeInsets.all(5.0),
                              child: Container(
                                child: SvgPicture.asset(
                                  questionsStatus[practicePositionForQuestion-1]!=null?getPrepareSvgImages
                                      .solutionInactive:getPrepareSvgImages
                                      .solutionActive,
                                  fit: BoxFit.fill,
                                  height: (19.3 / 640) * screenHeight,
                                  width: (14.4 / 360) * screenWidth,
                                  color: checking != null?checking == true
                                      ? Colors.grey
                                      : null:Colors.grey,
                                ),
                              ),
                            ),
                          ),
                        ),
                        //  Spacer(),
                        /// statistics .......... ///
                        InkWell(
                          onTap: () {
                            checking != null?checking != true
                                ? setState(() {
                              if(practiceHeader_status_question!=2) {
                                widget.practiceStatisticsCheck = true;
                              }
                              practiceHeader_status_question = 2;
                              dependencies.stopwatch.stop();
                              practiceSumbit_button_status = false;
                              widget.practiceStatisticsCheck? widget.practiceStatisticsCheck = false: widget.practiceStatisticsCheck = true;
                              practiceOverLayEntryCheck
                                  ? practiceOverLayEntryCheck = false
                                  : practiceOverLayEntryCheck = true;
                              hide();
                            })
                                : null:null;
                          },
                          child: Padding(
                            padding: const EdgeInsets.all(5.0),
                            child: Container(
                                height: (18 / 640) * screenHeight,
                                width: (16 / 360) * screenWidth,
                                child: InkWell(
                                  child: SvgPicture.asset(
                                    widget.practiceStatisticsCheck? getPrepareSvgImages
                                        .barGraphActive:getPrepareSvgImages
                                        .barGraphInactive,
                                    fit: BoxFit.fill,
                                    color: checking != null?checking == true
                                        ? Colors.grey
                                        : null:Colors.grey,
                                  ),
                                )),
                          ),
                        ),
                        // Spacer(),
                        /// bookmark .............///
                        InkWell(
                          onTap: () {},
                          child: Padding(
                            padding: const EdgeInsets.all(5.0),
                            child: Container(
                              height: (15 / 640) * screenHeight,
                              width: (11.7 / 360) * screenWidth,
                              // margin: EdgeInsets.only(right: 25.6),
                              child: SvgPicture.asset(
                                getPrepareSvgImages
                                    .bookmarkInactive,
                                fit: BoxFit.contain,
                                color:
                                Colors.grey,
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
          /// status.... ////
          GestureDetector(
            onTap: () {
              setState(() {
                practiceOverLayEntryCheck
                    ? practiceOverLayEntryCheck = false
                    : practiceOverLayEntryCheck = true;
                hide();
              });
            },
            child: Align(
              alignment: Alignment.topCenter,
              child: Container(
                margin: EdgeInsets.only(bottom: 0),
                //height: 520/720 * screenHeight,
                //margin: EdgeInsets.only(top: 40/720 * screenHeight,bottom:23/720* screenHeight),
                child: _status(),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _popUpForPauseButton(context) {
    final screenHeight = MediaQuery.of(context).size.height;
    final screenWidth = MediaQuery.of(context).size.width;

    return Container(
      height: screenHeight * 265 / 640,
      margin: EdgeInsets.only(bottom: 154 / 640 * screenHeight),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.end,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Align(
            alignment: Alignment.topCenter,
            child: Container(
              width: screenWidth * 140 / 360,
              height: screenHeight * 130 / 720,
              margin: EdgeInsets.only(bottom: 17 / 720 * screenHeight),
              child: SvgPicture.asset(
                getPrepareSvgImages.puseImageForPopUp,
                fit: BoxFit.fill,
              ),
            ),
          ),
          Align(
            alignment: Alignment.topCenter,
            child: Container(
              margin: EdgeInsets.only(bottom: 27 / 720 * screenHeight),
              child: Text(
                PrepareParagraphScreenStrings.sessionPaused,
                style: TextStyle(
                  color: NeutralColors.black,
                  fontSize: screenHeight * 16 / 678,
                  fontFamily: "IBMPlexSans",
                  fontWeight: FontWeight.w500,
                ),
                textAlign: TextAlign.center,
              ),
            ),
          ),
          Align(
            alignment: Alignment.center,
            child: Container(
              width: screenWidth * 130 / 360,
              height: screenHeight * 40 / 720,
              margin: EdgeInsets.only(top: 20 / 720 * screenHeight),
              child: FlatButton(
                onPressed: () {
                  _practiceControllerForPopUp.close();
                  //dependencies.stopwatch.start();
                },
                color: Color.fromARGB(255, 0, 171, 251),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(2)),
                ),
                textColor: NeutralColors.pureWhite,
                padding: EdgeInsets.all(0),
                child: Text(
                  PrepareParagraphScreenStrings.resume,
                  style: TextStyle(
                    fontSize: screenHeight * 14 / 720,
                    fontFamily: "IBMPlexSans",
                    fontWeight: FontWeight.w500,
                  ),
                  textAlign: TextAlign.center,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _popUpForBackButton(context) {
    final screenHeight = MediaQuery.of(context).size.height;
    final screenWidth = MediaQuery.of(context).size.width;
    return Container(
      height: screenHeight * 275 / 640,
      margin: EdgeInsets.only(bottom: 154 / 640 * screenHeight),
      //color: Colors.black,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.end,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Align(
            alignment: Alignment.topCenter,
            child: Container(
              width: screenWidth * 140 / 360,
              height: screenHeight * 130 / 720,
              margin: EdgeInsets.only(bottom: 15 / 720 * screenHeight),
              child: SvgPicture.asset(
                getPrepareSvgImages.exitImageForPopUp,
                fit: BoxFit.fill,
              ),
            ),
          ),
          Align(
            alignment: Alignment.topCenter,
            child: Container(
              margin: EdgeInsets.only(bottom: 40 / 720 * screenHeight),
              child: Text(
                PrepareParagraphScreenStrings.popUpForExit,
                style: TextStyle(
                  color: NeutralColors.black,
                  fontSize: screenHeight * 16 / 720,
                  fontFamily: "IBMPlexSans",
                  fontWeight: FontWeight.w500,
                ),
                textAlign: TextAlign.center,
              ),
            ),
          ),
          Container(
            height: screenHeight * 47.5 / 720,
            padding: EdgeInsets.only(
                left: 40.0 / 360 * screenWidth,
                right: 40.0 / 360 * screenWidth),
            child: Row(
              children: [
                Container(
                  width: screenWidth * 130 / 360,
                  height: screenHeight * 40 / 720,
                  margin: EdgeInsets.only(top: 1 / 720 * screenHeight),
                  child: FlatButton(
                    onPressed: () {
                        Wakelock.disable();
                      Navigator.pop(context,true);
                    },
                    color: NeutralColors.ice_blue,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(Radius.circular(2)),
                    ),
                    textColor: NeutralColors.blue_grey,
                    padding: EdgeInsets.all(0),
                    child: Text(
                      PrepareParagraphScreenStrings.yesForPopUpExit,
                      style: TextStyle(
                        fontSize: screenHeight * 14 / 678,
                        fontFamily: "IBMPlexSans",
                        fontWeight: FontWeight.w500,
                      ),
                      textAlign: TextAlign.center,
                    ),
                  ),
                ),
                Spacer(),
                Container(
                  width: screenWidth * 130 / 360,
                  height: screenHeight * 40 / 720,
                  // margin: EdgeInsets.only(top: 1/720*screenHeight),
                  child: FlatButton(
                    onPressed: () => _practiceControllerForPopUp.close(),
                    color: Color.fromARGB(255, 0, 171, 251),
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(Radius.circular(3)),
                    ),
                    textColor: NeutralColors.pureWhite,
                    padding: EdgeInsets.all(0.0),
                    child: Text(
                      PrepareParagraphScreenStrings.noForPopUpExit,
                      style: TextStyle(
                        fontSize: screenHeight * 14 / 678,
                        fontFamily: "IBMPlexSans",
                        fontWeight: FontWeight.w500,
                      ),
                      textAlign: TextAlign.center,
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget _status() {
    var _mediaQueryData = MediaQuery.of(context);
    screenWidthTotal = _mediaQueryData.size.width;
    screenHeightTotal = _mediaQueryData.size.height;
    _safeAreaHorizontal =
        _mediaQueryData.padding.left + _mediaQueryData.padding.right;
    _safeAreaVertical =
        _mediaQueryData.padding.top + _mediaQueryData.padding.bottom;

    screenHeight = screenHeightTotal - _safeAreaVertical;
    screenWidth = screenWidthTotal - _safeAreaHorizontal;

    if (practiceHeader_status_question == 1) {
      return Container(
        height: 505 / 720 * screenHeight,
        margin: EdgeInsets.only(
            top: 40 / 720 * screenHeight, bottom: 23 / 720 * screenHeight),
        child: PracticeShowAnswerScreen(
          value: currentPage,
          stopwatch: widget.stopwatch,
          backtoques_num: practicePositionForQuestion,
          showAnswer: showAnswerExplanation[practicePositionForQuestion - 1],
          statusForQuestion:questionsStatus[practicePositionForQuestion - 1].toString(),
          PracticequestionsReviewScreen: this,
          correctAnswer: correctAnswerList[practicePositionForQuestion-1],
        ),
      );
      //return ShowAnswerScreen(value:currentPage,);
    }
    else if (practiceHeader_status_question == 2) {
      return Container(
        height: 505 / 720 * screenHeight,
        margin: EdgeInsets.only(
            top: 40 / 720 * screenHeight, bottom: 23 / 720 * screenHeight),
        child: PracticeStatisticScreen(
          stopwatch: widget.stopwatch,
          bargraphActiveStatus: widget.practiceStatisticsCheck,
          backtoquesnum_bargraph: practicePositionForQuestion,
          PracticequestionsReviewScreen: this,
          timeTakenForQuestion:timetaken[practicePositionForQuestion-1],
          statusForQuestion:questionsStatus[practicePositionForQuestion - 1].toString(),
        ),
      );
      //return practiceStatisticScreen();
    } else {
      return totalQuestions.length == 0
          ? Container(
        // margin: EdgeInsets.only(top: 40/720 * screenHeight,bottom:23/720* screenHeight),
          height: 720 / 720 * screenHeight,
          child: Center(child: CircularProgressIndicator()))
          : GestureDetector(
        onTap: () {
          setState(() {
            practiceOverLayEntryCheck
                ? practiceOverLayEntryCheck = false
                : practiceOverLayEntryCheck = true;
            hide();
          });
        },
        child: Container(
          height: 505 / 720 * screenHeight,
          margin: EdgeInsets.only(
              top: 40 / 720 * screenHeight,
              bottom: 23 / 720 * screenHeight),
          child: Column(
            children: <Widget>[
              Container(
                height: (505 / 720) * screenHeight,
                width: (360 / 360) * screenWidth,
                child: PageView.builder(
                  controller: pageViewController,
                  physics: NeverScrollableScrollPhysics(),
                  itemBuilder: (BuildContext context, int index) {
                    // executeAfterBuild();
                    return Container(
                      // height:(450/720)*screenHeight,
                      child: (paragraphCheck[index]!=null && checking != false)
                      /// this  is for paragraph ///
                          ? Stack(
                        alignment: Alignment.bottomCenter,
                        children: <Widget>[
                          Column(
                              mainAxisSize: MainAxisSize.min,
                              mainAxisAlignment:
                              MainAxisAlignment.start,
                              crossAxisAlignment:
                              CrossAxisAlignment.stretch,
                              children: <Widget>[
                                (directionsForQuestion[index]!="")?
                                Align(
                                  alignment: Alignment.topLeft,
                                  child: Container(
                                    // color:Theme.of(context).primaryColor,
                                      margin: EdgeInsets.only(
                                          left: 20 / 360 * screenWidth,
                                          right: 20 / 360 * screenWidth,bottom:20/720*screenHeight),
                                      child: Html(
                                        blockSpacing:
                                        0.0,
                                        useRichText:
                                        false,
                                        // backgroundColor:Colors.red ,
                                        data:
                                        (directionsForQuestion[index]==null)?"":directionsForQuestion[index],
                                      defaultTextStyle:
                                        TextStyle(
                                          color: Color.fromARGB(255, 0, 3, 44),
                                          fontSize: 14 / 360 * screenWidth,
                                          fontFamily: "IBMPlexSans",
                                          fontWeight: FontWeight.w500,
                                        ),
                                      )),
                                ):Text(""),
                                Expanded(
                                  child: Container(
                                    height: 505 / 720 * screenHeight,
                                    margin: EdgeInsets.only(
                                        top: 0 / 720 * screenHeight,
                                        bottom:
                                        60 / 720 * screenHeight,
                                        left: 20 / 360 * screenWidth,
                                        right:
                                        20 / 360 * screenWidth),
                                    child: Container(
                                      child: SingleChildScrollView(
                                        //
                                          scrollDirection:
                                          Axis.vertical,
                                          child: Html(
                                            blockSpacing: 0.0,
                                            useRichText: false,
                                            // backgroundColor:Colors.red ,
                                            data: paragraphCheck[
                                            index] ??
                                                '',
                                            defaultTextStyle:
                                            TextStyle(
                                              color: Color.fromARGB(
                                                  255, 0, 3, 44),
                                              fontSize: 14 /
                                                  360 *
                                                  screenWidth,
                                              fontFamily:
                                              "IBMPlexSans",
                                              fontWeight:
                                              FontWeight.w500,
                                            ),
                                          )),
                                    ),
                                  ),
                                ),
                              ]),
                          Positioned(
                            bottom: -0.009 * screenHeight,
                            left: 0.05 * screenWidth,
                            right: 0.05 * screenWidth,
                            child: InkWell(
                              child: Container(
                                height:
                                40 / 720 * screenHeight,
                                decoration: BoxDecoration(
                                    borderRadius:
                                    BorderRadius.all(
                                        Radius.circular(
                                            2)),
                                    gradient: LinearGradient(
                                      // begin: Alignment(1.0199999809265137, 1.0199999809265137),
                                        end: Alignment(
                                            1.0199999809265137,
                                            1.0099999904632568),
                                        colors: [
                                          SemanticColors
                                              .light_purpely,
                                          SemanticColors
                                              .dark_purpely
                                        ])),
                                child: Row(
                                  crossAxisAlignment:
                                  CrossAxisAlignment
                                      .start,
                                  mainAxisAlignment:
                                  MainAxisAlignment
                                      .center,
                                  children: [
                                    Center(
                                      child: Text(
                                        Preparequestions
                                            .qoToQuestion,
                                        style: TextStyle(
                                          color: NeutralColors
                                              .pureWhite,
                                          fontSize: 14 /
                                              360 *
                                              screenWidth,
                                          fontFamily:
                                          "IBMPlexSans",
                                          fontWeight:
                                          FontWeight.w500,
                                        ),
                                        textAlign:
                                        TextAlign.center,
                                      ),
                                    ),
                                    Center(
                                      child: Container(
                                        margin: EdgeInsets.only(
                                            left: 10 /
                                                360 *
                                                screenWidth),
                                        child: Image.asset(
                                          "assets/images/back-white.png",
                                          width: 18 /
                                              360 *
                                              screenWidth,
                                          height: 16 /
                                              720 *
                                              screenHeight,
                                          fit: BoxFit
                                              .scaleDown,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              onTap: () {
                                setState(() {
                                  checking = false;
                                  // AppRoutes.pushWithAnmation(context, PracticeQuestionsScreen(header_status: 3,));
                                });
                              },
                            ),
                          )
                        ],
                      )
                      /// this is for question and answers ///
                          : Stack(
                        alignment: Alignment.bottomCenter,
                        children: <Widget>[
                          Column(
                            mainAxisSize: MainAxisSize.min,
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.stretch,
                            children:[
                              (paragraphCheck[index]!=null)?
                              /// back to paragraph ///
                              Align(
                                alignment: Alignment.topLeft,
                                child:
                                InkWell(

                                  child: Container(
                                    width: (170/360)*screenWidth,
                                    height: (23/720)*screenHeight,
                                    child: Row(
                                      children: [
                                        Container(
                                            margin: EdgeInsets.only(left: (20/360)*screenWidth,right: (8/360)*screenWidth),
                                            child:Icon(
                                              Icons.arrow_back_ios,
                                              color: PrimaryColors.azure_Dark,
                                              size: 15/720*screenHeight,
                                            )
                                        ),
                                        Container(
                                          child:Text(
                                            Preparequestions.back,
                                            style: TextStyle(
                                              color: PrimaryColors.azure_Dark,
                                              fontSize: (14/360)*screenWidth,
                                              fontFamily: "IBMPlexSans",
                                            ),
                                            textAlign: TextAlign.left,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  onTap: (){
                                    setState(() {
                                      checking=true;
                                      //        emaximSoloutionCheck=true;

                                    });
                                  },
                                ),
                              ) : Text(""),
                              /// question and answers ///
                              Expanded(
                                child: DraggableScrollbar(
                                  controller: controller,
                                  heightScrollThumb:
                                  78.0 / 720 * screenHeight,
                                  weightScrollThumb:
                                  3 / 360 * screenWidth,
                                  colorScrollThumb:
                                  NeutralColors.colors_scroll,
                                  marginScrollThumb: EdgeInsets.only(
                                      right: 6 / 360 * screenWidth),
                                  child: LayoutBuilder(
                                    builder: (BuildContext context,
                                        BoxConstraints
                                        viewportConstraints) {
                                      return ListView.builder(
                                        controller: controller,
                                        itemCount: finalTotalOptionsAccordingToQuestions[index].length,
                                        itemBuilder: (context, i) {
                                          return Container(
                                            width: 360 / 360 * screenWidth,
                                            margin: EdgeInsets.only(bottom: (10 / 720) * screenHeight),
                                            child: Column(
                                              children: <Widget>[
                                                /// direction ........///
                                                i == 0
                                                    ? (directionsForQuestion[index]!=""
                                                    || directionsForQuestion[index] != null )?                                                Container(
                                                  // color:Theme.of(context).primaryColor,
                                                    margin: EdgeInsets.only(
                                                        left: 20 / 360 * screenWidth,
                                                        right: 20 / 360 * screenWidth),
                                                    child: Html(blockSpacing: 0.0,
                                                      useRichText: false,
                                                      // backgroundColor:Colors.red ,
                                                        data:
                                                        (directionsForQuestion[index]==null)?"":directionsForQuestion[index],                                                      defaultTextStyle:
                                                      TextStyle(
                                                        color: Color.fromARGB(255, 0, 3, 44),
                                                        fontSize: 14 / 360 * screenWidth,
                                                        fontFamily: "IBMPlexSans",
                                                        fontWeight: FontWeight.w500,
                                                      ),
                                                    )):Text("")
                                                    : Container(),
                                                /// question........... ///
                                                i == 0
                                                    ? (totalQuestions[index]!="")?
                                                Container(
                                                  // color:Theme.of(context).primaryColor,
                                                    margin: EdgeInsets.only(
                                                        left: 20 / 360 * screenWidth,
                                                        right: 20 / 360 * screenWidth),
                                                    child: Html(blockSpacing: 0.0,
                                                      useRichText: false,
                                                      // backgroundColor:Colors.red ,
                                                      data: totalQuestions[index],
                                                      defaultTextStyle:
                                                      TextStyle(
                                                        color: Color.fromARGB(255, 0, 3, 44),
                                                        fontSize: 14 / 360 * screenWidth,
                                                        fontFamily: "IBMPlexSans",
                                                        fontWeight: FontWeight.w500,
                                                      ),
                                                    )):Text("")
                                                    : Container(),
                                                /// question status ... ///
                                                i == 0 ? Align(
                                                  alignment: Alignment.topLeft,
                                                  child: Container(
                                                    decoration: BoxDecoration(
                                                      border: Border.all(
                                                        color:(questionsStatus[index]==true)? PrimaryColors.kelly_green:(questionsStatus[index]==false)? PrimaryColors.dark_coral: Colors.yellow,
                                                        width: 1,
                                                      ),
                                                      borderRadius: BorderRadius.all(Radius.circular(2)),
                                                    ),
                                                    margin:
                                                    EdgeInsets.only(
                                                      left: 20 / 360 * screenWidth, top: 20 / 720 * screenHeight, right: 10 / 360 * screenWidth,),
                                                    child:
                                                    Padding(padding: EdgeInsets.only(
                                                        left: 10 / 720 * screenWidth,
                                                        right: 10 / 720 * screenWidth,
                                                        top: 2 / 720 * screenHeight,
                                                        bottom: 2 / 720 * screenHeight),
                                                      child:
                                                      Text(
                                                        (questionsStatus[index]==true)? Preparequestions.correctStatus:(questionsStatus[index]==false)? Preparequestions.wrongStatusForReview: Preparequestions.skippedStatus,
                                                        style: TextStyle(
                                                            color: (questionsStatus[index]==true)? PrimaryColors.kelly_green:(questionsStatus[index]==false)? PrimaryColors.dark_coral: Colors.yellow,
                                                            fontWeight: FontWeight.w500,
                                                            fontFamily: "IBMPlexSans",
                                                            fontStyle: FontStyle.normal,
                                                            fontSize: 12.0 / 360 * screenWidth),
                                                        textAlign:
                                                        TextAlign.center,
                                                      ),
                                                    ),
                                                  ),
                                                ) : Text(""),
                                                /// answers ...... ///
                                                questionsType[index]=="MCQ"?Container(
                                                  margin: EdgeInsets.only(top: 10 / 720 * screenHeight, bottom: i==(finalTotalOptionsAccordingToQuestions[index].length-1)?40 / 720 *screenHeight:10 / 720 *screenHeight),
                                                  decoration: BoxDecoration(
                                                    gradient: LinearGradient(
                                                        colors:selectedOptionIdList[index]=="-1"?  [
                                                          NeutralColors.pureWhite.withOpacity(0.1),
                                                          SemanticColors.pureWhite.withOpacity(0.1)
                                                        ]:selectedOptionIdList[index]==finalTotalOptionIDAccordingToQuestionsForTrack[index][i].toString()?[
                                                          (questionsStatus[index]==true)? PrimaryColors.kelly_green.withOpacity(0.1):(questionsStatus[index]==false)? PrimaryColors.dark_coral.withOpacity(0.1): Colors.white.withOpacity(0.1),
                                                          (questionsStatus[index]==true)? PrimaryColors.kelly_green.withOpacity(0.1):(questionsStatus[index]==false)? PrimaryColors.dark_coral.withOpacity(0.1): Colors.white.withOpacity(0.1),
                                                        ]:[
                                                          NeutralColors.pureWhite.withOpacity(0.1),
                                                          SemanticColors.pureWhite.withOpacity(0.1)
                                                        ]
                                                      //  colors: [NeutralColors.greycolor.withOpacity(0.1),SemanticColors.dark_purpely.withOpacity(0.1)]
                                                    ),
                                                    border:selectedOptionIdList[index]=="-1"?   Border.all(
                                                      color: Colors.transparent,
                                                      width: 1,
                                                    ):selectedOptionIdList[index]==finalTotalOptionIDAccordingToQuestionsForTrack[index][i].toString()?  Border.all(
                                                      color:(questionsStatus[index]==true)? PrimaryColors.kelly_green:(questionsStatus[index]==false)? PrimaryColors.dark_coral: Colors.white,
                                                      width: 1,
                                                    ): Border.all(
                                                      color: Colors.transparent,
                                                      width: 1,
                                                    ),
                                                    borderRadius: BorderRadius.all(Radius.circular(2)),
                                                  ),
                                                  child: Row(
                                                    mainAxisAlignment: MainAxisAlignment.center,
                                                    crossAxisAlignment: CrossAxisAlignment.center,
                                                    children: <
                                                        Widget>[
                                                      Container(
                                                        height: 15 / 720 * screenHeight,
                                                        width: 15 / 360 * screenWidth,
                                                        decoration:
                                                        BoxDecoration(
                                                          border:
                                                          Border.all(
                                                            color: NeutralColors.scroll,
                                                            width: 1,
                                                          ),
                                                          borderRadius: BorderRadius.all(Radius.circular(2)),),
                                                        margin: EdgeInsets.only(
                                                            left: (20 / 360) * screenWidth),
                                                        child: Center(
                                                          child: Text(
                                                            lettersForQuestions[i],
                                                            style:
                                                            TextStyle(
                                                              color: Color.fromARGB(
                                                                  255, 0, 180, 16),
                                                              fontSize: 10 / 720 * screenHeight,
                                                              fontFamily: "IBMPlexSans",
                                                            ),
                                                            textAlign: TextAlign.center,
                                                          ),
                                                        ),
                                                      ),
                                                      Container(
                                                        //   alignment: Alignment.topRight,
                                                        width: 280 / 360 * screenWidth,
                                                        //  height: 60.0/720*screenHeight,
                                                        color: Colors.transparent,
                                                        margin: EdgeInsets.only(
                                                            left: 8 / 360 * screenWidth,
                                                            right: 20 / 360 * screenWidth),
                                                        child: Html(customTextAlign:
                                                            (elem) {
                                                          return TextAlign
                                                              .start;
                                                        },
                                                            data: finalTotalOptionsAccordingToQuestions[index][i],
                                                            defaultTextStyle:
                                                            TextStyle(
                                                              color: Color.fromARGB(
                                                                  255,
                                                                  0,
                                                                  3,
                                                                  44),
                                                              fontSize: 14 /
                                                                  360 *
                                                                  screenWidth,
                                                              fontFamily:
                                                              "IBMPlexSans",
                                                              fontWeight:
                                                              FontWeight.w500,
                                                            ),
                                                            customTextStyle:
                                                                (node,
                                                                textStyle) {
                                                              return TextStyle(
                                                                color: Color.fromARGB(
                                                                    255,
                                                                    0,
                                                                    3,
                                                                    44),
                                                                fontSize: 14 /
                                                                    360 *
                                                                    screenWidth,
                                                                fontFamily:
                                                                "IBMPlexSans",
                                                                fontWeight:
                                                                FontWeight.w500,
                                                              );
                                                            },
                                                            imageProperties:
                                                            ImageProperties(
                                                              fit: BoxFit
                                                                  .fitHeight,
                                                              matchTextDirection:
                                                              false,
                                                              alignment:
                                                              Alignment.topLeft,
                                                              //centerSlice: new Rect.fromLTRB(1.0, 0.0, 0.0, 0.0),
                                                            )

//                                                  ImageProperties(
//                                                    alignment: Alignment.topRight,
//                                                    matchTextDirection: true,
//                                                    //color: Colors.orange,
//                                                  ),

                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                ):i==0?
                                                Container(
                                                    margin: EdgeInsets.only(top:30/720*screenHeight),
                                                    height:150/720*screenHeight,
                                                    color:Color(0xFFF1F0FA),
                                                    child:Container(
                                                      height:30/720*screenHeight,
                                                      width:320/360*screenWidth,
                                                      margin: EdgeInsets.only(left: 20/360*screenWidth,right: 20/360*screenWidth,top: 40/720*screenHeight,bottom: 40/720*screenHeight),
                                                      decoration: BoxDecoration(
                                                        color: Color(0xFF2F6F8),
                                                        border: Border.all(color: Color(0xFFBEC8CC),width: 1,),
                                                        borderRadius:BorderRadius.all(Radius.circular(10)),
                                                      ),
                                                      child: selectedOptionIdList[index]!="-1"?Container(
                                                        margin: EdgeInsets.only(left: 10/360*screenWidth),
                                                        child: Column(
                                                          mainAxisAlignment: MainAxisAlignment.center,
                                                          crossAxisAlignment: CrossAxisAlignment.start,
                                                          children: <Widget>[
                                                            Text(selectedOptionIdList[index],style: TextStyle(
                                                              color:Colors.black,
                                                            ),
                                                              textAlign: TextAlign.start,
                                                            ),
                                                          ],
                                                        ),
                                                      ):Text(""),
                                                    )
                                                ):Text(""),
                                              ],
                                            ),
                                          );
                                        },
                                      );
                                    },
                                  ),
                                ),
                              ),
                            ], ),
                          /// prev , submit and next buttons ///
                          Positioned(
                              child: Container(
                                height: 40 / 720 * screenHeight,
                                width: 320 / 360 * screenWidth,
                                margin: EdgeInsets.only(
                                    left: (20 / 360) * screenWidth,
                                    right: (20 / 360) * screenWidth),
                                // color: Colors.black,
                                /**LAYOUT FOR PREV,NEXT,SUBMIT BUTTON**/
                                child: Row(
                                  children: <Widget>[
                                    /**LAYOUT FOR PREV BUTTON**/
                                    Container(
                                      decoration: BoxDecoration(
                                          gradient: LinearGradient(
                                            begin: Alignment(-0.056, 0.431),
                                            end: Alignment(1.076, 0.579),
                                            stops: [0, 1,],
                                            colors: practicePositionForQuestion==1?[SemanticColors.iceBlue, NeutralColors.ice_blue,]:[SemanticColors.light_purpely, NeutralColors.box_color,],
                                          )),
                                      width: 105 / 360 * screenWidth,
                                      child: FlatButton(
                                        //color: practicePositionForQuestion==1?SemanticColors.pureWhite:AccentColors.blueGrey,
                                        textColor: Colors.white,
                                        disabledColor:
                                        SemanticColors.iceBlue,
                                        disabledTextColor:
                                        NeutralColors.black,
                                        onPressed: () {
                                          practicePositionForQuestion!=1?setState(() {
                                            (practiceSumbit_button_status == null) ? pageViewController.previousPage(
                                                duration: _kDuration, curve: _kCurve) : null;
                                            practiceOverLayEntryCheck ? practiceOverLayEntryCheck = false : practiceOverLayEntryCheck = true;
                                            hide();
                                          }):null;
                                        },
                                        child: Text(
                                          Preparequestions.prev,
                                          style: TextStyle(
                                            color: practicePositionForQuestion!=1?SemanticColors.pureWhite:AccentColors.blueGrey,
                                            fontSize: 14,
                                            fontFamily: "IBMPlexSans",
                                            fontWeight: FontWeight.w500,
                                          ),
                                          textAlign: TextAlign.center,
                                        ),
                                      ),
                                    ) ,
                                    /**LAYOUT FOR SUBMIT BUTTON**/
                                    Container(
                                      width: 110 / 360 * screenWidth,
                                      decoration: BoxDecoration(
                                          gradient:
                                          LinearGradient(
                                            begin: Alignment(
                                                -0.056, 0.431),
                                            end: Alignment(
                                                1.076, 0.579),
                                            stops: [
                                              0,
                                              1,
                                            ],
                                            colors: [
                                              GradientColors
                                                  .iceBlue,
                                              GradientColors
                                                  .iceBlue,
                                            ],
                                          )),
                                      child: FlatButton(
                                          onPressed: () {

                                          },
                                          child: Text(
                                            Preparequestions.submit,
                                            style: TextStyle(
                                              color: AccentColors.blueGrey,
                                              fontSize: 14,
                                              fontFamily: "IBMPlexSans",
                                              fontWeight: FontWeight.w500,
                                            ),
                                            textAlign: TextAlign.center,
                                          )),
                                    ),
                                    /**LAYOUT FOR NEXT BUTTON**/
                                    Container(
                                      decoration: BoxDecoration(
                                          gradient: LinearGradient(
                                            begin: Alignment(-0.056, 0.431),
                                            end: Alignment(1.076, 0.579),
                                            stops: [0, 1,],
                                            colors: practicePositionForQuestion==totalQuestions.length?[SemanticColors.light_purpely, NeutralColors.box_color,]:[SemanticColors.light_purpely, NeutralColors.box_color,],
                                          )),
                                      width: 105 / 360 * screenWidth,
                                      child: FlatButton(
                                        onPressed: () {
                                          setState(() {
                                            practiceOverLayEntryCheck
                                                ? practiceOverLayEntryCheck =
                                            false
                                                : practiceOverLayEntryCheck =
                                            true;
                                            hide();
                                            if (practicePositionForQuestion == totalQuestions.length) {
                                              Navigator.pop(context,true);
                                            }
                                            (practiceSumbit_button_status ==
                                                null)
                                                ? pageViewController
                                                .nextPage(
                                                duration:
                                                _kDuration,
                                                curve: _kCurve)
                                                : null;
                                            // isSubmitButtonClicked = false;
                                          });
                                        },
                                        // color: GradientColors.iceBlue,
                                        textColor: Colors.white,
                                        disabledColor:
                                        GradientColors.iceBlue,
                                        child: Text(
                                          practicePositionForQuestion == totalQuestions.length?Preparequestions.exit:Preparequestions.next,
                                          style: TextStyle(
                                            color:practicePositionForQuestion!=totalQuestions.length?SemanticColors.pureWhite:SemanticColors.pureWhite,

                                            fontSize: 14,
                                            fontFamily: "IBMPlexSans",
                                            fontWeight: FontWeight.w500,
                                          ),
                                          textAlign: TextAlign.center,
                                        ),
                                      ),
                                    )
                                  ],
                                ),
                              )),
                        ],
                      ),
                    );
                  },
                  itemCount: totalQuestions.length,
                  onPageChanged: (page) {
                    setState(() {
                      answer_status = null;
                      contentHeight = 0;
                      practicePositionForQuestion = page + 1;
                      if (paragraphCheck[practicePositionForQuestion-1]!=null){
                        checking = true;
                      }else{
                        checking = false;
                      }
                      if (practiceBookMarkedQuestionsAfterSharedPref !=
                          null) {
                        if (practiceBookMarkedQuestionsAfterSharedPref
                            .contains((page + 1).toString())) {
                          practiceBookMarkCheck = true;
                        } else {
                          practiceBookMarkCheck = false;
                        }
                      } else {}
                    });
                  },
                ),
              ),
            ],
          ),
        ),
      );
    }
  }
}
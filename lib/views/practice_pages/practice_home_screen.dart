import 'package:connectivity/connectivity.dart';
import 'dart:ui';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:imsindia/api/bloc/services.dart';
import 'package:imsindia/components/bottom_navigation.dart';
import 'package:imsindia/components/custom_expansion_panel.dart';
import 'package:imsindia/components/navigation_bar.dart';
import 'package:imsindia/components/webview_vimeoplayer.dart';
import 'package:imsindia/resources/strings/practice.dart';
import 'package:imsindia/utils/Url.dart';
import 'package:imsindia/utils/colors.dart';
import 'package:imsindia/utils/global.dart' as global;
import 'package:imsindia/utils/svg_images/error_screen_svg_images.dart';
import 'package:imsindia/utils/svg_images/prepare_svg_images.dart';
import 'package:imsindia/views/practice_pages/practice_subjects_screen.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../../routers/routes.dart';

var screenWidthTotal;
var screenHeightTotal;
var _safeAreaHorizontal;
var _safeAreaVertical;
var screenHeight;
var screenWidth;
var courseID;
var menuID;

List titles = [
  'Logical Reasoning',
  'Verbal Ability & Reading Comprehension',
  'Legal Aptitude',
  'General Knowledge',
  'Quantitative Ability',
  'Bookmarked Videos',
];

List insideList = [
  'Line Graph',
  'Pie Chart',
  'Tables',
  'Scatter Plot',
  'Multiple Diagrams',
];
bool _loaded = false;
var img = NetworkImage("http://d1cn01kw45hc6h.cloudfront.net/wp-content/uploads/2019/08/22161210/Quadtratic.png");
class PracticeHomeScreen extends StatefulWidget {
  @override
  PracticeHomeScreenState createState() => PracticeHomeScreenState();
}

class PracticeHomeScreenState extends State<PracticeHomeScreen> {
  int _activeMeterIndex;
  var getIsProductAccessCheck = [];
  var getLearFirstHiraLevelTitles = [];
  var getLearFirstHiraLevelIds = [];
  var getLearSeccondtHiraLevelTitles = [];
  var getLearSeccondtHiraLevelIds = [];
  var finalListForSecondHirarTitles = [];
  var finalListForSecondHirarIds = [];
  var authorizationInvalidMsg;
  Connectivity connectivity = Connectivity();
  final GlobalKey<ScaffoldState> _scaffoldstateemax = new GlobalKey<ScaffoldState>();
  var refreshKeypractice = GlobalKey<RefreshIndicatorState>();

  void getFirstAndSecondhierarchyTitles(Map t) async {
    print("inside api");
    print(URL.GET_LEARN_FIRST_HIRA_NAMES_PRACTICE+ courseID + "&id=" + menuID);
    print(t);

    var connectivityResult = await (Connectivity().checkConnectivity());
    if (connectivityResult.toString() != 'ConnectivityResult.none') {
      ApiService().getAPI(
          URL.GET_LEARN_FIRST_HIRA_NAMES_PRACTICE+ courseID + "&id=" + menuID, t)
          .then((returnValue) {
        setState(() {
          print("after validation");
          print(returnValue);
          if (returnValue[0].toString().toLowerCase() == 'Data fetched successfully'.toLowerCase()) {
            print("API data");
            //       print(menuID);
            var totalData = returnValue[1]['data'];
            
            for (var index = 0; index <
                returnValue[1]['data'].length; index++) {
                  getIsProductAccessCheck.add(returnValue[1]['data'][index]['isProductAccess']);
              if (totalData[index]['parentId'] == menuID) {
                if(!getLearFirstHiraLevelTitles.contains(totalData[index]['component']['title'])){
                  getLearFirstHiraLevelTitles.add(totalData[index]['component']['title']);
                }
                if(!getLearFirstHiraLevelIds.contains(totalData[index]['id'])){
                  getLearFirstHiraLevelIds.add(totalData[index]['id']);
                }
              }
            }
            for (var i = 0; i < getLearFirstHiraLevelIds.length; i++) {
              for (var j = 0; j < totalData.length; j++) {
                if (totalData[j]['parentId'] == getLearFirstHiraLevelIds[i]) {
                  if(!getLearSeccondtHiraLevelTitles.contains(totalData[j]['component']['title'])){
                    getLearSeccondtHiraLevelTitles.add(totalData[j]['component']['title']);
                  }
                  if(!getLearSeccondtHiraLevelIds.contains(totalData[j]['id'])){
                    getLearSeccondtHiraLevelIds.add(totalData[j]['id']);
                  }
                }
              }
              finalListForSecondHirarTitles.add(getLearSeccondtHiraLevelTitles);
              finalListForSecondHirarIds.add(getLearSeccondtHiraLevelIds);
              getLearSeccondtHiraLevelIds = [];
              getLearSeccondtHiraLevelTitles = [];

            }
          } else {
            authorizationInvalidMsg = returnValue[0];
          }
        });
      });
    }else{
      showErrorMessage('No Internet Connection');
    }

  }

  void getCourseId() async {
    print("inside getcourseid");
    SharedPreferences prefs = await SharedPreferences.getInstance();
    courseID = prefs.getString("courseId");
    menuID = prefs.getString("moduleId");
    global.getToken.then((t){
      getFirstAndSecondhierarchyTitles(t);

    });
  }
  void setIdForSubjectLevelTitles(value) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString("idForSubjectLevelTitlesPractice", value);
  }

  void setTitleForSubjectLevelTitles(value) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString("titleForSubjectLevelTitlesPractice", value);
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    checkInternet();
    print('init state called');
    connectivity.onConnectivityChanged.listen((result) {
      if (result.toString() == 'ConnectivityResult.none') {
        print('connection lost');
        noInternetMessage('Internet Connection Lost');
      } else {
        print(_scaffoldstateemax.currentState) ;
        print("_scaffoldstate.currentState");
        _scaffoldstateemax.currentState.removeCurrentSnackBar();
        print('connection exist in notifier');
        getLearFirstHiraLevelTitles = [];
        getLearFirstHiraLevelIds = [];
        getLearSeccondtHiraLevelTitles = [];
        getLearSeccondtHiraLevelIds = [];
        finalListForSecondHirarTitles = [];
        finalListForSecondHirarIds = [];
        getCourseId();
      }
    });
    // img.resolve(ImageConfiguration()).addListener(ImageStreamListener((ImageInfo image, bool synchronousCall) {
    //   if (mounted) {
    //     setState(() => _loaded = true);
    //   }
    // }));
  }
  // on dragging page refresh function will be activated
  Future<Null> refreshList() async {
    refreshKeypractice.currentState?.show(atTop: false);
    await Future.delayed(Duration(seconds: 2));
    setState(() {
      print('Clicked on refresh');
      //Again calling api while refreshing
      getLearFirstHiraLevelTitles = [];
      getLearFirstHiraLevelIds = [];
      getLearSeccondtHiraLevelTitles = [];
      getLearSeccondtHiraLevelIds = [];
      finalListForSecondHirarTitles = [];
      finalListForSecondHirarIds = [];
      getCourseId();
    });

    return null;
  }
  Future<Null> checkInternet() async {
    var connectivityResult = await (Connectivity().checkConnectivity());
    if (connectivityResult.toString() != 'ConnectivityResult.none') {
      setState(() {
        print('Clicked on intenet');
        getLearFirstHiraLevelTitles = [];
        getLearFirstHiraLevelIds = [];
        getLearSeccondtHiraLevelTitles = [];
        getLearSeccondtHiraLevelIds = [];
        finalListForSecondHirarTitles = [];
        finalListForSecondHirarIds = [];
        //Again calling api while refreshing
        getCourseId();
      });
    } else {
      noInternetMessage('Internet Connection Lost');
    }

    return null;
  }
  void noInternetMessage(String value) {
    _scaffoldstateemax.currentState.showSnackBar(new SnackBar(
      content: new Text(value),
      action: SnackBarAction(
        textColor: Colors.blueAccent,
        label: "Dismiss",
        onPressed: () {
          // Some code to undo the change.
        },
      ),
      backgroundColor: Colors.blueGrey,
      duration: Duration(days: 365),
    ));
  }

  
  void showErrorMessage(String value) {
    _scaffoldstateemax.currentState.showSnackBar(new SnackBar(
      content: new Text(value),
      duration: new Duration(seconds: 3),
    ));
  }

  accessCheck(List list){
    int i;
    int j = 0;
    print(list);
    for (i=0;i < list.length ;i++){
      if(list[i] == false){
        j = j+1;
      }
    }
    //print("Check for isProductAccess");
    //print(j);
    if(j == list.length){
      return true;
    }else{
      return false;
    }
  }
  @override
  void didChangeDependencies() {
    // TODO: implement initState
    // super.initState();
    //getCourseId();
  }
  @override
  Widget build(BuildContext context) {


    var _mediaQueryData = MediaQuery.of(context);
    screenWidthTotal = _mediaQueryData.size.width;
    screenHeightTotal = _mediaQueryData.size.height;
    _safeAreaHorizontal =
        _mediaQueryData.padding.left + _mediaQueryData.padding.right;
    _safeAreaVertical =
        _mediaQueryData.padding.top + _mediaQueryData.padding.bottom;

    screenHeight = screenHeightTotal - _safeAreaVertical;
    screenWidth = screenWidthTotal - _safeAreaHorizontal;


    return Scaffold(
        key: _scaffoldstateemax,
        appBar: AppBar(
          brightness: Brightness.light,
          centerTitle: false,
          titleSpacing: 0.0,
          backgroundColor: NeutralColors.pureWhite,
          elevation: 0.0,
          iconTheme: IconThemeData(
            color: NeutralColors.black, //change your color here
          ),
          title: Container(
            width: (302 / 360) * screenWidth,
            child: Text(
              PracticeHomeScreenString.Practice,
              style: TextStyle(
                color: NeutralColors.black,
                fontSize: (16 / 720) * screenHeight,
                fontFamily: "IBMPlexSans",
                fontWeight: FontWeight.w500,
              ),
              textAlign: TextAlign.left,
            ),
          ),
        ),
        drawer: NavigationDrawer(),
//        bottomNavigationBar: BottomNavigation(),
        body: Container(
          color: Colors.white,
          child: Column(
            children: <Widget>[
//
//              Container(
//                decoration:  BoxDecoration(
//                    borderRadius: new BorderRadius.circular(8.0/360*screenWidth)),
//                //child: _getStackpage(false, context), // Hide snd Show widgets
//                child: _getChewiePlayer(false, context),
//              ),
              authorizationInvalidMsg != null
                  ?  Container(
                  margin: EdgeInsets.only(
                      top: (200.0 / 720) * screenHeight),
                  child: Center(
                      child: Text(
                          '${authorizationInvalidMsg} ')))
                  : getLearFirstHiraLevelTitles.length == 0
                  ?  Container(
                  margin: EdgeInsets.only(
                      top: (200.0 / 720) * screenHeight),
                  child:
                  Center(child: CircularProgressIndicator()))
                   : accessCheck(getIsProductAccessCheck) ?
                Center(
                  child: Column(
                    children: [
                      Container(
                margin: EdgeInsets.only(
                        left: 18 / 360 * screenWidth,
                        top: 12.5 / 720 * screenHeight),
                child: SvgPicture.asset(
                      ErrorScreenAssets.errorScreenImg,
                      fit: BoxFit.fill,
                ),
              ),
                      new Container(
                    padding:EdgeInsets.only(top: 10 / 797 * screenHeight,left: 18 / 360 * screenWidth,right: 18 / 360 * screenWidth,),
                    child: Center(
                      child:Text( accessCheck(getIsProductAccessCheck)? "Access to this module will be enabled as per the schedule for your enrolled program. \n OR \n You do not have access to this module as part of your enrolled program.": "No tests available",
                    style: TextStyle(
                          color: NeutralColors.charcoal_grey,
                          fontSize: 14 / 360 * screenWidth,
                          fontFamily: "IBMPlexSans",
                          fontWeight: FontWeight.w500,
                        ),
                        textAlign: TextAlign.center,
                    )
                  )
                    
                )
                    ],
                  ),
                ) :  new Expanded(
                child:RefreshIndicator(
                    displacement: 100/720*screenHeight,
                    color: SemanticColors.blueGrey,
                    backgroundColor: SemanticColors.iceBlue,
                    key: refreshKeypractice,
                    child: new ListView.builder(
                      itemBuilder: (BuildContext context, int i) {
                        return Container(
                          decoration: BoxDecoration(
                            boxShadow: [
                              BoxShadow(
                                offset: Offset(0, 5),
                                blurRadius: 10,
                                color: (_activeMeterIndex == i)
                                    ? Color(0xffeaeaea)
                                    : Colors.transparent,
                                //offset: Offset.lerp(Offset(10.0,-10.0), Offset(10.0,10.0), 1),
                                // offset: Offset(0,10.0),
                                //color: Colors.orange,
                              ),
                            ],
                            borderRadius: new BorderRadius.only(
                                topLeft: new Radius.circular(5),
                                topRight: new Radius.circular(5),
                                bottomLeft: new Radius.circular(5),
                                bottomRight:
                                new Radius.circular(5)),
                            border: Border.all(
                                color: Colors.transparent),
                          ),
                          margin: EdgeInsets.only(
                              top: (10.0 / 720) * screenHeight,
                              left: (20.0 / 360) * screenWidth,
                              right: (20.0 / 360) * screenWidth,
                              bottom: (i==getLearFirstHiraLevelTitles.length-1)?(40 / 720) * screenHeight:0),
                          child: CustomExpansionPanelList(
                            expansionHeaderHeight:
                            (45 / 720) * screenHeight,
                            iconColor: (_activeMeterIndex == i)
                                ? Colors.white
                                : Colors.black,
                            backgroundColor1:
                            (_activeMeterIndex == i)
                                ? NeutralColors.purpleish_blue
                                : SemanticColors.light_purpely
                                .withOpacity(0.1),
                            backgroundColor2:
                            (_activeMeterIndex == i)
                                ? NeutralColors.purpleish_blue
                                : SemanticColors.dark_purpely
                                .withOpacity(0.1),
                            expansionCallback:
                                (int index, bool status) {
                              setState(() {
                                _activeMeterIndex =
                                _activeMeterIndex == i
                                    ? null
                                    : i;
                              });
                            },
                            children: [
                              new ExpansionPanel(
                                canTapOnHeader: true,
                                isExpanded: _activeMeterIndex == i,
                                headerBuilder:
                                    (BuildContext context,
                                    bool isExpanded) =>
                                new Container(
                                  alignment: Alignment.centerLeft,
                                  child: new Padding(
                                    padding: EdgeInsets.only(
                                        left: (15 / 320) *
                                            screenWidth),
                                    child: new Text(
                                        getLearFirstHiraLevelTitles[
                                        i],
                                        style: new TextStyle(
                                            fontSize: (13.6 / 720) *
                                                screenHeight,
                                            fontFamily:
                                            "IBMPlexSans",
                                            fontWeight:
                                            FontWeight.w500,
                                            color: (_activeMeterIndex ==
                                                i)
                                                ? NeutralColors
                                                .pureWhite
                                                : NeutralColors
                                                .charcoal_grey)),
                                  ),
                                ),
                                body: new Container(
                                  color: Colors.white,
                                  // height: (210 / 678) * screenHeight,
                                  child: new ListView.builder(
                                    shrinkWrap: true,
                                    addAutomaticKeepAlives: true,
                                   // addSemanticIndexes: false,
                                    padding: EdgeInsets.only(
                                        bottom: 10.0 / 720 * screenHeight),
//                                    itemExtent:
//                                    (40.0 / 720) * screenHeight,
                                    itemBuilder:
                                        (BuildContext context,
                                        int index) {
                                      return new ListTile(
                                        dense: true,
                                        contentPadding:
                                        EdgeInsets.only(
                                            bottom: 0,
                                            left: (15 / 320) *
                                                screenWidth,
                                            top: 0),
                                        onTap: () {
                                          setIdForSubjectLevelTitles( finalListForSecondHirarIds[i][index]);
                                          setTitleForSubjectLevelTitles(finalListForSecondHirarTitles[i][index]);
                                          global.titleForPrepareSubjectsScreen=finalListForSecondHirarTitles[i][index];
                                          AppRoutes.push(context, PracticeSubjectsScreen());
                                        },
                                        title: new Text(
                                          finalListForSecondHirarTitles[
                                          i][index],
                                          textAlign: TextAlign.left,
                                          style: new TextStyle(
                                              fontSize: (15 / 720) *
                                                  screenHeight,
                                              fontFamily:
                                              "IBMPlexSans",
                                              fontWeight:
                                              FontWeight.w500,
                                              color: NeutralColors
                                                  .blue_grey),
                                        ),
                                      );
                                    },
                                    itemCount:
                                    finalListForSecondHirarTitles[i]==0?0:finalListForSecondHirarTitles[i].length,
                                  ),
                                ),
                              ),
                            ],
                          ),
                        );
                      },
                      itemCount: getLearFirstHiraLevelTitles == null
                          ? 0
                          : getLearFirstHiraLevelTitles.length,
                    ),
                    onRefresh: refreshList
                ),
              )
            ],
          ),
        ));
  }
}

//_getStackpage(bool offStageFlag, BuildContext context) {
//  Offstage offstage = new Offstage(
//      offstage: offStageFlag,
//      child: ChewieListItem(
//        videoPlayerController: VideoPlayerController.network(
//          'https://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4',
//        ),
//      ));
//  return offstage;
//}

_getthumbnail(bool offStageFlag, BuildContext context) {
  Offstage offstage = new Offstage(
    offstage: offStageFlag,
    child: Padding(
      padding: const EdgeInsets.all(10.0),
      child: Container(
        margin: EdgeInsets.only(
            left: (20 / 360) * screenWidth, right: (20 / 360) * screenWidth),
        height: 181 / 720 * screenHeight,
        child: InkWell(
          onTap: () {
            AppRoutes.push(context, WebViewExample(videoId: "246673616",));
          },
          child: Container(

            constraints: new BoxConstraints.expand(
              height: (181 / 720) * screenHeight,

            ),
            alignment: Alignment.bottomLeft,
            padding: new EdgeInsets.only(left: 16.0, bottom: 8.0),
            decoration:  BoxDecoration(
              borderRadius: new BorderRadius.circular(5.0),
              image: DecorationImage(
                  alignment: Alignment(-.2, 0),

                  image: CachedNetworkImageProvider(
                      'http://d1cn01kw45hc6h.cloudfront.net/wp-content/uploads/2019/08/22161210/Quadtratic.png'),
//                  image: NetworkImage(
//                      'http://d1cn01kw45hc6h.cloudfront.net/wp-content/uploads/2019/08/22161210/Quadtratic.png'
//                  ),
                  fit: BoxFit.cover),
            ),
            child: Center(
              child: Container(
                height: (50 / 720) * screenHeight,
                width: (50 / 360) * screenWidth,
                child: SvgPicture.asset(
                  getPrepareSvgImages.playwhite,
                  fit: BoxFit.fitHeight,
                ),
              ),
            ),
          ),
        ),
      ),
    ),
  );
  return offstage;
}

_getChewiePlayer(bool offStageFlag, BuildContext context){
  Offstage offstage = new Offstage(
    offstage: offStageFlag,
    child: Container(
      decoration:  BoxDecoration(
          borderRadius: new BorderRadius.circular(8.0/360*screenWidth)),
      margin: EdgeInsets.only(
          left: (20 / 360) * screenWidth, right: (20 / 360) * screenWidth),
      height: 181 / 720 * screenHeight,
      child: InkWell(
        onTap: () {
          //AppRoutes.push(context,WebViewExample1(null));
        },
        child: Container(

          constraints: new BoxConstraints.expand(
            height: (181 / 720) * screenHeight,

          ),
          alignment: Alignment.bottomLeft,
          padding: new EdgeInsets.only(bottom: 10.0/720*screenHeight),
          decoration:  BoxDecoration(
            borderRadius: new BorderRadius.circular(8.0/360*screenWidth),
            image: DecorationImage(
                alignment: Alignment(-.2, 0),
                image:
                //_loaded ? img : 
                  AssetImage("./assets/images/videoplaceholder.png"),
//                  image: NetworkImage(
//                      'http://d1cn01kw45hc6h.cloudfront.net/wp-content/uploads/2019/08/22161210/Quadtratic.png'
//                  ),
                fit: BoxFit.cover),
          ),
          child: Center(
            child: Container(
              height: (50 / 720) * screenHeight,
              width: (50 / 360) * screenWidth,
              child: SvgPicture.asset(
                getPrepareSvgImages.playwhite,
                fit: BoxFit.fitHeight,
              ),
            ),
          ),
        ),
      ),
    ),
  );
  return offstage;
}

class CustomBoxShadow extends BoxShadow {
  final BlurStyle blurStyle;

  const CustomBoxShadow({
    Color color = const Color(0xFF000000),
    Offset offset = Offset.zero,
    double blurRadius = 0.0,
    this.blurStyle = BlurStyle.normal,
  }) : super(color: color, offset: offset, blurRadius: blurRadius);

  @override
  Paint toPaint() {
    final Paint result = Paint()
      ..color = color
      ..maskFilter = MaskFilter.blur(this.blurStyle, blurSigma);
    assert(() {
      if (debugDisableShadows) result.maskFilter = null;
      return true;
    }());
    return result;
  }
}

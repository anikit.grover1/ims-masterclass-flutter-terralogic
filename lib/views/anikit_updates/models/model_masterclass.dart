import 'package:flutter/material.dart';

class MasterclassModel {
  String masterclassimage;
  String masterclasstypedilr;
  String masterclasstitle;
  String masterclassdatetime;
  String masterclassdescription;
  String masterclasstopicscoveddescription;
  String masterclassshowless;
  Widget masterclasshandout;
  Widget masterclassbookmark;
  Widget masterclassplay;
  Widget masterclassjoin;

  MasterclassModel(
      {@required this.masterclassimage,
      @required this.masterclasstypedilr,
      @required this.masterclasstitle,
      @required this.masterclassdatetime,
       this.masterclassdescription,
       this.masterclasstopicscoveddescription,
       this.masterclassshowless,
       this.masterclasshandout,
       this.masterclassbookmark,
      this.masterclassplay,
      this.masterclassjoin});
}

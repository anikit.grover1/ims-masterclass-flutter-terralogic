

import 'package:flutter/material.dart';

import 'ui/masterclass_screen/masterclass_homepage.dart';
import 'ui/masterclass_screen/masterclass_screen.dart';

class AppRoutes
{
  AppRoutes._();
    static final masterscreen = '/masterscreen';
    static final masterhomepage ='/masterhomepage';
  static final routes = <String, WidgetBuilder>{
  masterscreen:(BuildContext context)=>MasterclassPlayerPage(),
  masterhomepage:(BuildContext context )=>MasterclassHomepage()
  };
  
}
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:imsindia/views/anikit_updates/constants/colors.dart';
import 'package:imsindia/views/anikit_updates/constants/images.dart';

import 'custom_widgets.dart';

Widget commonmastercard(
    Widget displaywidget, String text, Color color, TextStyle customtextstyle,Color dilrcolor ) {
  return GestureDetector(
    onTap: () {
      // Navigator.pushReplacementNamed(AppRoutes.masterscreen);
    },
    child: Container(
      decoration: BoxDecoration(borderRadius: BorderRadius.circular(4.0)),
      // width: 145,
      // height: 220,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Stack(
            children: [
              ClipRRect(
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(4), topRight: Radius.circular(4)),
                clipBehavior: Clip.antiAlias,
                child: Image.network(
                  "https://images.unsplash.com/photo-1561089489-f13d5e730d72?ixid=MnwxMjA3fDB8MHxzZWFyY2h8MXx8bWFzdGVyJTIwY2xhc3N8ZW58MHx8MHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=800&q=60",
                  filterQuality: FilterQuality.high,
                  fit: BoxFit.cover,
                  width: 145,
                  height: 82,
                ),
              ),
              Positioned(
                top: 28,
                left: 60,
                right: 60,
                bottom: 29,
                child: Image(
                  image: AppImagesMasterclass.image_play,
                  //  color: AppColors.dark_navy_blue,
                  width: 25,
                  height: 25,
                ),
              ),
            ],
          ),
          Container(
            width: 148,
            height: 133,
            padding: EdgeInsets.only(left: 10.0, right: 2.0),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(4.0),
              color: AppColors.card_background,
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  padding: EdgeInsets.only(top: 6.0),
                ),
                Positioned(
                    top: 8,
                    left: 15,
                    right: 101,
                    bottom: 110,
                    child: masterclasstypedilr("DI/LR", dilrcolor)),
                SizedBox(
                  height: 6.0,
                ),
                Positioned(
                  top: 30,
                  bottom: 55,
                  left: 10,
                  right: 2,
                  child: masterclasstitle(
                      "Orientation to Quant and DI-LR & Speed Maths: Part 2 (Advanced) E:1",
                      136,
                      50),
                ),
                SizedBox(
                  height: 6.0,
                ),
                time("20 Oct 2020"),
                SizedBox(
                  height: 6.0,
                ),
                Row(
                  //   mainAxisAlignment: MainAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    displaywidget,
                    SizedBox(
                      width: 6.0,
                    ),
                    Text(
                      text,
                      style: customtextstyle,
                    ),
                    SizedBox(
                      width: 33.0,
                    ),
                    Expanded(
                      child: Positioned(
                        top: 106,
                        left: 89,
                        right: 44,
                        child: Image(
                          image: AppImagesMasterclass.icon_bookmark,
                          //  color: AppColors.dark_navy_blue,
                          width: 12,
                          height: 15,
                        ),
                      ),
                    ),
                    SizedBox(
                      width: 20.0,
                    ),
                    Expanded(
                      child: Positioned(
                        left: 121,
                        top: 106,
                        right: 10,
                        child: Image(
                          image: AppImagesMasterclass.mobile_icon,
                          //   color: AppColors.dark_navy_blue,
                          width: 17,
                          height: 17,
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ],
      ),
    ),
  );
}

Widget masterexpansioncard(BuildContext context) {
  return Container(
    padding: EdgeInsets.all(10.0),
    child: Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            displayimage(
                context,
                "https://images.unsplash.com/photo-1561089489-f13d5e730d72?ixid=MnwxMjA3fDB8MHxzZWFyY2h8MXx8bWFzdGVyJTIwY2xhc3N8ZW58MHx8MHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=800&q=60",
                111,
                122,
                5),
            SizedBox(
              height: 20.0,
            ),
            Expanded(
              flex: 2,
              child: Container(
                padding: EdgeInsets.only(left: 15.0, top: 0.0),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Positioned(
                        left: 146,
                        top: 54,
                        right: 173,
                        child: masterclasstypedilr("DI/LR", AppColors.orange)),
                    SizedBox(
                      height: 4.0,
                    ),
                    masterclasstitle(
                        "Orientation to Quant and DI-LR & Speed Maths: Part 2 (Advanced) E:1",
                        309,
                        40),
                    SizedBox(
                      height: 7.0,
                    ),
                    datetime("12 Oct 2020  |  55 Mins"),
                    SizedBox(
                      height: 10.0,
                    ),
                    episodelisthandoutandbookmark(context),
                  ],
                ),
              ),
            )
          ],
        )
      ],
    ),
  );
}

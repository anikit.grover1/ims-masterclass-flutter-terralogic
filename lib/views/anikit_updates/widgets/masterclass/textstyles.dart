// masterclass
import 'package:flutter/material.dart';
import 'package:imsindia/views/anikit_updates/constants/colors.dart';

// TextTheme customtheme()
// {
//   return TextTheme(
//     headline2:masterclasstitles() ,
//     headline3: 
//   );
// }

TextStyle masterclassappbartitle()
{
  return TextStyle(
    color: AppColors.black,
    fontSize: 16,
    fontFamily: "IBMPlexSans",
    letterSpacing: 0,

    
  );
}

TextStyle masterclasstitles()
{
  return TextStyle(
    color: AppColors.dark_navy_blue,
    
    fontSize: 14,
    fontFamily: "IBMPlexSans",
    letterSpacing: 0,
    
  );
}
TextStyle masterclassdescription()
{
  return TextStyle(
    color: AppColors.gunmetal,

    fontSize: 12,
    
    fontFamily: "IBMPlexSans",
    letterSpacing: 0,
    height: 1.5,
  );
}
TextStyle showmore()
{
  return TextStyle(
    color: AppColors.azure,

    fontSize: 12,
    fontFamily: "IBMPlexSans",
    letterSpacing: 0,
    height:2,
  );
}

TextStyle date()

{
 
  return TextStyle(
    color: AppColors.bluey_grey,

    fontSize: 10,
    fontFamily: "IBMPlexSans",
    letterSpacing: 0,
    height: 1.0
    
   );
}

TextStyle dilr()

{
 
  return TextStyle(
    color: AppColors.white,

    fontSize: 12,
    fontFamily: "IBMPlexSans",
    letterSpacing: 0,
    
   );
}
TextStyle episode
()
{
  return TextStyle(
    color: AppColors.dark_navy_blue,
    fontSize: 16,
    fontFamily: "IBMPlexSans-Medium",
    letterSpacing: 0,
     height:2,
  );
}

TextStyle handoutboookmark()
{
  return TextStyle(
    color: AppColors.azure,
    fontSize: 12,
    fontFamily: "IBMPlexSans-Medium",
    letterSpacing: 0,);
}

TextStyle playmasterclass()
{
  return TextStyle(
    color: AppColors.azure,
    fontSize: 12,
    fontFamily: "IBMPlexSans-Medium",
    letterSpacing: 0,);
}

TextStyle joinmasterclass()
{
  return TextStyle(
    color: AppColors.red,
    fontSize: 12,
    fontFamily: "IBMPlexSans-Medium",
    letterSpacing: 0,);
}


TextStyle masterclassorientation()
{
  return TextStyle(
    color: AppColors.dark_navy_blue,

    fontSize: 12,
    fontFamily: "IBMPlexSans",
    letterSpacing: 0,
    height: 2
  );
}


// notify textstyle
TextStyle notifyalert()
{
  return TextStyle(
    color: AppColors.bluey_grey,

    fontSize: 12,
    fontFamily: "IBMPlexSans",
    letterSpacing: 0,
  );
}

TextStyle deletealert()
{
 return TextStyle(
     color: AppColors.bluey_grey,
    
    fontSize: 14,
    fontFamily: "IBMPlexSans",
    letterSpacing: 0.0
   );
}

TextStyle savealert()
{
   return TextStyle(
     color: AppColors.white,
    
    fontSize: 12,
    fontFamily: "IBMPlexSans",
    letterSpacing: 0.0
   );
}

TextStyle radiostyle()
{
  return TextStyle();
}
// 
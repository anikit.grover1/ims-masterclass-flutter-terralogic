import 'package:flutter/material.dart';
import 'package:imsindia/views/anikit_updates/constants/colors.dart';

import 'textstyles.dart';

AppBar custommasterclassappbar(
    BuildContext context,
    String text,
    String routeName,
    IconData searchicon,
    IconData notifications,
    String searchontap,
    String notificationsontap) {
  return AppBar(
    backgroundColor: AppColors.white,
    elevation: 0.0,
    title: Text(text, style: masterclassappbartitle()),
    leading: IconButton(
      icon: Icon(
        Icons.arrow_back,
        size: 20.0,
        color: AppColors.dark_navy_blue,
      ),
      onPressed: () {
        Navigator.pushReplacementNamed(context, routeName);
      },
    ),
  );
}

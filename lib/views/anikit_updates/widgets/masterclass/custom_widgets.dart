import 'package:flutter/material.dart';
import 'package:imsindia/views/anikit_updates/constants/colors.dart';
import 'package:imsindia/views/anikit_updates/constants/images.dart';

import 'textstyles.dart';
// masterclass

Widget displayimage(BuildContext context, String displayimage, double width,
    double height, double radius) {
  return ClipRRect(
    borderRadius: BorderRadius.circular(radius),
    // width:MediaQuery.of(context).size.width,
    child: Image.network(
      displayimage, fit: BoxFit.cover,
      filterQuality: FilterQuality.high,
      width: width,
      height: height,
      //   height: MediaQuery.of(context).size.height/4,
    ),
  );
}

Widget masterclasstitle(String masterclasstitle, double width, double height) {
  return Container(
      width: width,
      height: height,
      child: Text(masterclasstitle,
          //  overflow: TextOverflow.ellipsis,
          style: masterclasstitles()));
}

Widget masterclasstypedilr(String masterclasstypedilr, Color color) {
  return Container(
      padding: EdgeInsets.only(left: 5.0),
      width: 51,
      decoration: BoxDecoration(
          color: color, borderRadius: BorderRadius.all(Radius.circular(2))),
      child: Text(
        masterclasstypedilr,
        style: dilr(),
      ));
}

Widget time(String time) {
  // h3 body interface is missing
  return Container(
      child: Align(
          alignment: Alignment.centerLeft,
          child: Text(time, style: masterclasstitles())));
}

Widget datetime(String datetime) {
  // h3 body interface is missing
  return Container(
    width: 57,
    height: 12,
      child: Align(
          alignment: Alignment.centerLeft,
          child: Text(datetime, style: date())));
}

Widget masterdescriptions(
  String description,
) {
  return Flexible(
    child: Container(
        child: Text(
      description,
      style: masterclassdescription(),
    )),
  );
}

Widget episodelist(String episodelisttitle) {
  return Text(
    episodelisttitle,
    style: episode(),
  );
}

Widget playbuttonmasterclasses() {
  return Positioned(
    top: 104,
    left: 8,
    bottom: 9,
    right: 120,
    child: Image(
      image: AppImagesMasterclass.play,
   //   color: color,
      width: 20,
      height: 20,
    ),
  );
}

Widget joinmasterclasses() {
  return Positioned(
    top: 104,
    left: 8,
    bottom: 9,
    right: 120,
    child: Image(
      image: AppImagesMasterclass.icon_play,
      //color: color,
      width: 20,
      height: 20,
    ),
  );
}

Widget notifymasterclasses() {
  return Positioned(
    top: 104,
    left: 8,
    bottom: 9,
    right: 120,
    child: Image(
      image: AppImagesMasterclass.filter,
     // color: color,
      width: 20,
      height: 20,
    ),
  );
}

Widget masterhandout() {
  return Row(
    mainAxisAlignment: MainAxisAlignment.spaceBetween,
    mainAxisSize: MainAxisSize.min,
    children: [
      Image.asset("assets/anikit_images/mobile_icon_report_2.svg"),
      Image.asset("assets/anikit_images/mobile_icon_report_2.svg"),
    ],
  );
}

Widget episodelisthandoutandbookmark(BuildContext context) {
  return Container(
    child: Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Image(
          image: AppImagesMasterclass.mobile_icon,
          color: AppColors.azure,
          width: 10,
          height: 12,
        ),
        SizedBox(
          width: 5.0,
        ),
        Text(
          "Handout",
          style: handoutboookmark(),
        ),
        SizedBox(
          width: 25.0,
        ),
        Image(
          image: AppImagesMasterclass.icon_bookmark,
          color: AppColors.azure,
          width: 8,
          height: 10,
        ),
        SizedBox(
          width: 5.0,
        ),
        Container(
          child: Text(
            "Bookmark",
            style: handoutboookmark(),
          ),
        ),
      ],
    ),
  );
}

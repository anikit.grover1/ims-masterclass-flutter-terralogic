import 'package:flutter/material.dart';

import 'package:imsindia/views/anikit_updates/constants/colors.dart';

import 'textstyles.dart';

AppBar custommasterclassappbar(BuildContext context,String text,String routeName,IconData searchicon,IconData notifications,String searchontap,String notificationsontap)
{
  return AppBar(
    backgroundColor: AppColors.white,
    elevation: 0.0,
    title: Text(text,style:masterclassappbartitle()),
    actions: [
      Padding(
        padding: const EdgeInsets.all(4.0),
        child: IconButton(icon: Icon(searchicon,size: 30.0,color: AppColors.black,), onPressed: (){
                Navigator.pushReplacementNamed(context, searchontap);
        }),
      ),
      Padding(
        padding: const EdgeInsets.all(4.0),
        child: IconButton(icon: Icon(notifications,size: 30.0,color: AppColors.black,), onPressed: (){
                Navigator.pushReplacementNamed(context, notificationsontap);
        }),
      ),
    
    ],
    leading: IconButton(icon: Icon(Icons.arrow_back,size: 30.0,color: AppColors.dark_navy_blue,),onPressed: (){
      Navigator.pushReplacementNamed(context, routeName);
    },),
    
  );
}
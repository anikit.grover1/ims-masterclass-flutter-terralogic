import 'dart:io';

import 'package:flutter/material.dart';
import 'package:imsindia/routers/routes.dart';
import 'package:imsindia/views/anikit_updates/AllRoutes.dart';
import 'package:imsindia/views/anikit_updates/constants/colors.dart';
import 'package:imsindia/views/anikit_updates/constants/images.dart';
import 'package:imsindia/views/anikit_updates/widgets/masterclass/common_widgets.dart';
import 'package:imsindia/views/anikit_updates/widgets/masterclass/custom_appbar.dart';
import 'package:imsindia/views/anikit_updates/widgets/masterclass/custom_widgets.dart';
import 'package:imsindia/views/anikit_updates/widgets/masterclass/textstyles.dart';

class MasterclassPlayerPage extends StatefulWidget {

  @override
  _MasterclassPlayerPageState createState() => _MasterclassPlayerPageState();
}

class _MasterclassPlayerPageState extends State<MasterclassPlayerPage> {
  bool showmorenable = false;
  void tapped(bool showmorenable) {
    setState(() {
      showmorenable = !showmorenable;
    });
  }

  Widget showmoredata(String showmoretext) {
    return Text(
      showmoretext,
      style: showmore(),
    );
  }

  List<Widget> masterclass = [
    commonmastercard(playbuttonmasterclasses(), "Play",
        AppColors.azure, playmasterclass(),AppColors.orange),
    commonmastercard(playbuttonmasterclasses(  ), "Join",
      AppColors.red      , joinmasterclass(),AppColors.azure),
    commonmastercard(playbuttonmasterclasses(), "Play",
        AppColors.azure, playmasterclass(),AppColors.orange),
    commonmastercard(joinmasterclasses(  ), "Join",
      AppColors.red      , joinmasterclass(),AppColors.azure),
    commonmastercard(playbuttonmasterclasses(), "Play",
        AppColors.azure, playmasterclass(),AppColors.orange),
    commonmastercard(joinmasterclasses(  ), "Join",
    AppColors.red        , joinmasterclass(),AppColors.azure),
    commonmastercard(playbuttonmasterclasses(), "Play",
        AppColors.azure, playmasterclass(),AppColors.orange),
    commonmastercard(joinmasterclasses(  ), "Join",
     AppColors.red       , joinmasterclass(),AppColors.azure),
    commonmastercard(playbuttonmasterclasses(), "Play",
        AppColors.azure, playmasterclass(),AppColors.orange),
    commonmastercard(joinmasterclasses(  ), "Join",
       AppColors.red     , joinmasterclass(),AppColors.azure),
  ];
  List<Widget> buildcards(int count) {
    List<Widget> playcard = List.generate(
      count,
      (index) => masterclass[index],
    );
    return playcard;
  }

  @override
  Widget build(BuildContext context) {
    final ThemeData theme = Theme.of(context);
    return Scaffold(
      backgroundColor: AppColors.white,
      appBar: custommasterclassappbar(context, "Master Class", "", Icons.search,
          Icons.notifications, "", ""),
      body: Container(
        padding: EdgeInsets.symmetric(horizontal:20.0),
        child: ListView(
          
         shrinkWrap: true,
        scrollDirection: Axis.vertical,
        //  physics: ClampingScrollPhysics(),
        //  crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          displayimage(
              context,
              "https://images.unsplash.com/photo-1561089489-f13d5e730d72?ixid=MnwxMjA3fDB8MHxzZWFyY2h8MXx8bWFzdGVyJTIwY2xhc3N8ZW58MHx8MHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=800&q=60",
              320,
              180,
              5),
          SizedBox(
            height: 11.0,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              masterclasstypedilr("DI/LR", AppColors.orange),
               SizedBox(
                width: 233.0,
              ),
              Image(
                image: AppImagesMasterclass.mobile_icon,
                //   color: AppColors.dark_navy_blue,
                width: 14,
                height: 18,
              ),

              SizedBox(
                width: 20.0,
              ),
              Positioned(
                left: 328,
                top: 276,
                child: Image(
                  image: AppImagesMasterclass.icon_bookmark,
                  //   color: AppColors.dark_navy_blue,
                  width: 14,
                  height: 18,
                ),
              ),
            ],
          ),
          SizedBox(
            height: 18.0,
          ),
          masterclasstitle(
              "Orientation to Quant and DI-LR & Speed Maths: Part 2 (Advanced) E:1",
              309,
              34),
          SizedBox(
            height: 11.0,
          ),
          datetime("12 Oct 2020  |  55 Mins"),
          SizedBox(
            height: 12.0,
          ),
          masterdescriptions(
            "Get a headstart into the Quant and DI - LR with these series of videos as well as a simplified introduction to speed maths and time efficiency. Get a headstart into the Quant and DI - LR with these series of videos as well as a simplified introduction to speed maths and time efficiency.",
          ),
          showmorenable == true
              ? Flexible(
                  child: Container(
                      child: Text(
                    "Topics Covered:",
                    style: masterclassdescription(),
                  )),
                )
              : Container(),
          showmorenable == true
              ? Flexible(
                  child: Container(
                      child: Text(
                    "1. Introduction to Quant  00:00 - 03:45",
                    style: masterclassdescription(),
                  )),
                )
              : Container(),
          showmorenable == true
              ? Flexible(
                  child: Container(
                      child: Text(
                    "2. Introduction to Formulas 03:35 - 10:55",
                    style: masterclassdescription(),
                  )),
                )
              : Container(),
          showmorenable == true
              ? Flexible(
                  child: Container(
                      child: Text(
                    "3. Understanding from Examples 10:55 - 16:35",
                    style: masterclassdescription(),
                  )),
                )
              : Container(),
          showmorenable == true
              ? Flexible(
                  child: Container(
                      child: Text(
                    "4. Solving questions using formulas 16:35 - 21:00",
                    style: masterclassdescription(),
                  )),
                )
              : Container(),
          showmorenable == true
              ? Flexible(
                  child: Container(
                      child: Text(
                    "5. Solving questions in given time 21:00 - 24:00",
                    style: masterclassdescription(),
                  )),
                )
              : Container(),
          GestureDetector(
              onTap: () {
                setState(() {
                  showmorenable = !showmorenable;
                });
              },
              child: showmorenable == true
                  ? showmoredata("Show less")
                  : showmoredata("Show More")),
          SizedBox(
            height: 14.0,
          ),
          ExpandableMasterclass(
            padding: false,
          ),
          SizedBox(
            height: 20.0,
          ),
          Text("Master Classes"),
          SizedBox(
            height: 10.0,
          ),
          //        MasterclassListview(),
          Align(
            alignment: Alignment(1428, 38),
            child: Text(
              "See All",
              // textAlign: TextAlign.right,
            ),
          ),
        ],
          ),
      ),
    );
  }
}

class MasterclassListview extends StatefulWidget {
  @override
  _MasterclassListviewState createState() => _MasterclassListviewState();
}

class _MasterclassListviewState extends State<MasterclassListview> {
  
  List<Widget> masterclass = [
    commonmastercard(playbuttonmasterclasses(), "Play",
        AppColors.azure, playmasterclass(),AppColors.orange),
    commonmastercard(playbuttonmasterclasses(  ), "Join",
      AppColors.red      , joinmasterclass(),AppColors.azure),
    commonmastercard(playbuttonmasterclasses(), "Play",
        AppColors.azure, playmasterclass(),AppColors.orange),
    commonmastercard(joinmasterclasses(  ), "Join",
      AppColors.red      , joinmasterclass(),AppColors.azure),
    commonmastercard(playbuttonmasterclasses(), "Play",
        AppColors.azure, playmasterclass(),AppColors.orange),
    commonmastercard(joinmasterclasses(  ), "Join",
    AppColors.red        , joinmasterclass(),AppColors.azure),
    commonmastercard(playbuttonmasterclasses(), "Play",
        AppColors.azure, playmasterclass(),AppColors.orange),
    commonmastercard(joinmasterclasses(  ), "Join",
     AppColors.red       , joinmasterclass(),AppColors.azure),
    commonmastercard(playbuttonmasterclasses(), "Play",
        AppColors.azure, playmasterclass(),AppColors.orange),
    commonmastercard(joinmasterclasses(  ), "Join",
       AppColors.red     , joinmasterclass(),AppColors.azure),
  ];
  List buildcards(int count) {
    List<Widget> playcard = List.generate(
      count,
      (index) => masterclass[index],
    );
    return playcard;
  }

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      shrinkWrap: true,
      scrollDirection: Axis.horizontal,
      physics: ScrollPhysics(),
      itemCount: masterclass.length,
      itemBuilder: (context, int index) {
      return masterclass[index];
    });
    // ListView(
    //     physics: ClampingScrollPhysics(),
    //     shrinkWrap: true,
    //     scrollDirection: Axis.horizontal,
    //     children: buildcards(10));
  }
}

class ExpandableMasterclass extends StatefulWidget {
  final bool padding;
  ExpandableMasterclass({@required this.padding});
  @override
  _ExpandableMasterclassState createState() => _ExpandableMasterclassState();
}

class _ExpandableMasterclassState extends State<ExpandableMasterclass> {
  bool expaned = false;
  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      child: Column(
        children: [
          Container(
              width: MediaQuery.of(context).size.width,
              padding:
                  widget.padding == false ? EdgeInsets.zero : EdgeInsets.zero,
              decoration: BoxDecoration(
                color: AppColors.pale_blue,
              ),
              child: Row(
                // mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    //  padding: EdgeInsets.only(top: 10, right: 262, bottom: 11,),
                    child: Text(
                      "Episode List",
                      style: TextStyle(color: AppColors.black, fontSize: 20.0),
                    ),
                  ),
                  //         SizedBox(
                  //   width: 314.0,
                  // ),
                  GestureDetector(
                    onTap: () {
                      setState(() {
                        expaned = !expaned;
                      });
                    },
                    child: Container(
                      padding: EdgeInsets.only(right: 39),
                      child: Icon(
                        expaned == false
                            ? Icons.expand_more
                            : Icons.expand_less,
                        size: 40.0,
                      ),
                    ),
                  )
                ],
              )),
          expaned != false
              ? ExpandedContainer(
                  expanded: expaned,
                  collapsedheight: 60.0,
                  expandedheight: 490.0,
                  child: ListView(
                    shrinkWrap: true,
                    physics: NeverScrollableScrollPhysics(),
                    children: [
                      masterexpansioncard(context),
                      masterexpansioncard(context),
                      masterexpansioncard(context),
                    ],
                  ),
                )
              : Container(),
        ],
      ),
    );
  }
}

class ExpandedContainer extends StatefulWidget {
  final bool expanded;
  final double collapsedheight;
  final double expandedheight;
  final Widget child;
  ExpandedContainer(
      {this.expanded,
      this.collapsedheight,
      this.expandedheight,
      @required this.child});
  @override
  _ExpandedContainerState createState() => _ExpandedContainerState();
}

class _ExpandedContainerState extends State<ExpandedContainer> {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: AppColors.pale_blue,
      child: widget.child,
      width: MediaQuery.of(context).size.width,
    //  padding: EdgeInsets.zero,
      height: widget.expanded != false
          ? widget.expandedheight
          : widget.collapsedheight,
    );
  }
}

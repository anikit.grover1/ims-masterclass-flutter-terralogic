import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:imsindia/routers/routes.dart';
import 'package:imsindia/views/anikit_updates/constants/colors.dart';
import 'package:imsindia/views/anikit_updates/constants/images.dart';
import 'package:imsindia/views/anikit_updates/widgets/masterclass/common_widgets.dart';
import 'package:imsindia/views/anikit_updates/widgets/masterclass/custom_widgets.dart';
import 'package:imsindia/views/anikit_updates/widgets/masterclass/textstyles.dart';

class MasterclassHomepage extends StatefulWidget {
  @override
  _MasterclassHomepageState createState() => _MasterclassHomepageState();
}

class _MasterclassHomepageState extends State<MasterclassHomepage> {
  int alertindex = 1;
  List<Widget> masterclass = [
    commonmastercard(playbuttonmasterclasses(), "Play", AppColors.azure,
        playmasterclass(), AppColors.orange),
    commonmastercard(joinmasterclasses(), "Join", AppColors.red,
        joinmasterclass(), AppColors.azure),
    commonmastercard(playbuttonmasterclasses(), "Play", AppColors.azure,
        playmasterclass(), AppColors.orange),
    commonmastercard(joinmasterclasses(), "Join", AppColors.red,
        joinmasterclass(), AppColors.azure),
    commonmastercard(playbuttonmasterclasses(), "Play", AppColors.azure,
        playmasterclass(), AppColors.orange),
    commonmastercard(joinmasterclasses(), "Join", AppColors.red,
        joinmasterclass(), AppColors.orange),
    commonmastercard(playbuttonmasterclasses(), "Play", AppColors.azure,
        playmasterclass(), AppColors.orange),
    commonmastercard(joinmasterclasses(), "Join", AppColors.red,
        joinmasterclass(), AppColors.azure),
    commonmastercard(playbuttonmasterclasses(), "Play", AppColors.azure,
        playmasterclass(), AppColors.orange),
    commonmastercard(joinmasterclasses(), "Join", AppColors.red,
        joinmasterclass(), AppColors.azure),
  ];
  List<Widget> buildcards(int count) {
    List<Widget> playcard = List.generate(
      count,
      (index) => masterclass[index],
    );
    return playcard;
  }

  String searchtext;
  TextEditingController searchcontroller = new TextEditingController();
  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    searchcontroller.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.white,
      appBar: AppBar(
        automaticallyImplyLeading: true,
        elevation: 0.0,
        backgroundColor: AppColors.white,
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back,
            size: 20.0,
            color: AppColors.dark_navy_blue,
          ),
          onPressed: () {
            //  Navigator.pushReplacementNamed(context, AppRoutes.masterscreen);
          },
        ), //  titleSpacing: 21.0,
        title: Text(
          "Master Class",
          style: masterclassappbartitle(),
        ),
        actions: [
          //   Padding(
          //     padding: const EdgeInsets.all(8.0),
          //     child: IconButton(
          //         icon: Icon(
          //           searchicon,
          //           size: 20.0,
          //           color: AppColors.black,
          //         ),
          //         onPressed: () {
          //           Navigator.pushReplacementNamed(context, searchontap);
          //         }),
          //   ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: IconButton(
                icon: Icon(
                  Icons.notifications,
                  size: 20.0,
                  color: AppColors.black,
                ),
                onPressed: () {
                  //   Navigator.pushReplacementNamed(context, AppRoutes.masterscreen);
                }),
          ),
        ],
      ),
      body: Container(
        padding: EdgeInsets.symmetric(horizontal: 20.0),
        child: Form(
          child: ListView(
            shrinkWrap: true,
            scrollDirection: Axis.vertical,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Expanded(
                    child: Container(
                      width: 260,
                      height: 40,
                      //    padding: EdgeInsets.only(top: 8.0,bottom: 7.0),
                      decoration: BoxDecoration(
                        border: Border.all(color: AppColors.pale_blue),
                        borderRadius: BorderRadius.circular(5.0),
                      ),
                      child: TextField(
                        controller: searchcontroller,

                        decoration: InputDecoration(
                            hintText: "Search by title or subject",
                            hintStyle: TextStyle(
                                color: AppColors.bluey_grey,
                                fontSize: 12,
                                height: 1.0,
                                fontFamily: "IBMPlexSans",
                                letterSpacing: 0.0),
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(4.0),
                                borderSide: BorderSide(
                                    color: AppColors.azure, width: 1.0)),
                            suffixIcon: Container(
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(5.0),
                                  color: AppColors.azure),
                              child: Container(
                                  width: 40,
                                  height: 40,
                                  child: Icon(Icons.search,
                                      size: 30.0, color: AppColors.white)),
                            )),

                        keyboardType: TextInputType.text,

                        onChanged: (s) {
                          setState(() {
                            searchtext = s;
                          });
                        },

                        //

                        textAlign: TextAlign.start,
                      ),
                    ),
                  ),
                  SizedBox(
                    width: 20.0,
                  ),
                  GestureDetector(
                    onTap: () {
                      notificationalert(context);
                    },
                    child: Image(
                      image: AppImagesMasterclass.filter,
                      width: 40,
                      height: 40,
                      fit: BoxFit.contain,
                      filterQuality: FilterQuality.high,
                      alignment: Alignment.center,
                    ),
                  )
                ],
              ),
              SizedBox(
                height: 30.0,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Container(
                    width: 200,
                    height: 26,
                    decoration: BoxDecoration(
                        border: Border.all(color: AppColors.expandle_color)),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Container(
                            width: 99,
                            height: 26,
                            decoration: BoxDecoration(
                              color: AppColors.white,
                            ),
                            child: Center(
                                child: Text(
                              "Recent",
                              style: recenttextstyle(),
                            ))),
                        Container(
                            width: 99,
                            height: 26,
                            decoration: BoxDecoration(
                              color: AppColors.lightcolor,
                            ),
                            child: Center(
                                child: Text(
                              "Upcoming",
                              style: upcomingtextstyle(),
                            ))),
                      ],
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: 30.0,
              ),
              upcoming(),
            ],
          ),
        ),
      ),
    );
  }

  Widget tabbar() {
    return Container(
        width: 206,
        height: 26,
        child: TabBar(tabs: [
          Tab(
            child: Center(
                child: Text(
              "Recent",
              style: recenttextstyle(),
            )),
          ),
          Tab(
            child: Text(
              "Upcoming",
              style: upcomingtextstyle(),
            ),
          ),
        ]));
  }

  Widget tabarview() {
    return Container(
      child: TabBarView(children: [recent(), upcoming()]),
    );
  }

  Widget recent() {
    return Expanded(
      child: GridView.count(
        shrinkWrap: true,
        crossAxisCount: 2,
        childAspectRatio: 32 / 42,
        crossAxisSpacing: 1.0,
        mainAxisSpacing: 1.0,
        scrollDirection: Axis.vertical,
        children: buildcards(10),
      ),
    );
  }

  TextStyle recenttextstyle() {
    return TextStyle(
        color: AppColors.bluey_grey,
        fontSize: 12,
        height: 1.0,
        fontFamily: "IBMPlexSans",
        letterSpacing: 0.0);
  }

  TextStyle upcomingtextstyle() {
    return TextStyle(
        color: AppColors.purpleish_blue,
        fontSize: 12,
        height: 1.0,
        fontFamily: "IBMPlexSans",
        letterSpacing: 0.0);
  }

  Widget upcoming() {
    return Expanded(
      child: GridView.count(
        shrinkWrap: true,
        crossAxisCount: 2,
        childAspectRatio: 32 / 42,
        crossAxisSpacing: 1.0,
        mainAxisSpacing: 1.0,
        physics: NeverScrollableScrollPhysics(),
        // scrollDirection: Axis.vertical,

        children: buildcards(10),
      ),
    );
  }

// added notification alert  on 25/5/21
  notificationalert(BuildContext context) {
    showModalBottomSheet(
        context: context,
        builder: (context) {
          return Container(
            padding: EdgeInsets.only(right: 29.0),
            width: MediaQuery.of(context).size.width,
            height: 435,
            decoration: BoxDecoration(
                color: AppColors.white,
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(20.0),
                    topRight: Radius.circular(20.0)),
                boxShadow: [
                  BoxShadow(
                    color: AppColors.black.withOpacity(0.31),
                    blurRadius: 15.0,
                    spreadRadius: 2,
                  ),
                ]),
            child: Column(
              children: [
                Positioned(
                    top: 24,
                    left: 44,
                    right: 44,
                    child:
                        Center(child: Text("Notify me", style: notifyalert()))),
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    displayimage(
                        context,
                        "https://images.unsplash.com/photo-1561089489-f13d5e730d72?ixid=MnwxMjA3fDB8MHxzZWFyY2h8MXx8bWFzdGVyJTIwY2xhc3N8ZW58MHx8MHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=800&q=60",
                        90,
                        90,
                        5),
                    SizedBox(
                      height: 20.0,
                    ),
                    Expanded(
                      flex: 2,
                      child: Container(
                        padding: EdgeInsets.only(left: 10.0, top: 8.0),
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Row(
                              children: [
                                Positioned(
                                    left: 146,
                                    top: 54,
                                    right: 173,
                                    child: masterclasstypedilr(
                                        "DI/LR", AppColors.orange)),
                                datetime("12 Oct 2020  |  55 Mins"),
                              ],
                            ),
                            SizedBox(
                              width: 51.0,
                            ),
                            masterclasstitle(
                                "Orientation to Quant and DI-LR & Speed Maths: Part 2 (Advanced) E:1",
                                201,
                                38),
                            SizedBox(
                              height: 10.0,
                            ),
                            Text(
                                "2 line description max of what they are about to expect")
                          ],
                        ),
                      ),
                    )
                  ],
                ),
// adding the tabbar

                // adding the delete button

                Container(
                  padding: EdgeInsets.only(left: 40, right: 40, bottom: 40.0),
                  child: Row(
                    children: [
                      deletealertbutton(alertindex),
                      SizedBox(
                        width: 15.0,
                      ),
                      savealertbutton(alertindex),
                    ],
                  ),
                )
              ],
            ),
          );
        });
  }

  Widget deletealertbutton(int index) {
    return Container(
        width: 130,
        height: 40,
        decoration: BoxDecoration(
            color: AppColors.ice_blue,
            borderRadius: BorderRadius.circular(2.0)),
        child: Positioned(
            top: 11,
            left: 21,
            right: 21,
            bottom: 11,
            child: Text("Delete alert $index", style: deletealert())));
  }

  Widget savealertbutton(int index) {
    return Container(
        width: 130,
        height: 40,
        decoration: BoxDecoration(
            color: AppColors.azure, borderRadius: BorderRadius.circular(2.0)),
        child: Positioned(
            top: 11,
            left: 27,
            right: 27,
            bottom: 11,
            child: Text("Save alert $index", style: deletealert())));
    ;
  }
}

class Customtabbarview extends StatefulWidget {
  @override
  _CustomtabbarviewState createState() => _CustomtabbarviewState();
}

class _CustomtabbarviewState extends State<Customtabbarview> {
  int index = 1;
  String radiobuttonitem = "Minutes";
  int radioselected = 1;

  @override
  Widget build(BuildContext context) {
    return Expanded(
      flex: 1,
      child: Column(
        children: [
          Container(
            child: TabBar(tabs: [
              Tab(
                text: "Alert $index",
              ),
            ]),
          ),
          Row(
            children: [
              Radio(
                value: 1,
                groupValue: index,
                onChanged: (r)
                {
                  setState(() {
                        radiobuttonitem ="Minutes";
                        index =1;              
                                    });
                },
              ),
              Text("Minutes",)
              
            ],
          )
        ],
      ),
    );
  }
}




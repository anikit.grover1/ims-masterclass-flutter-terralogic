import 'package:flutter/cupertino.dart';

class AppImagesMasterclass
{
  AppImagesMasterclass._();
  static const play =AssetImage('assets/anikit_images/group_43.png');
  static const icon_bookmark = AssetImage('assets/anikit_images/icon_bookmark_2.png');
   static const icon_play = AssetImage('assets/anikit_images/icon_play_copy_5.png');
   static const mobile_icon= AssetImage('assets/anikit_images/mobile_icon_report_2.png');
   static const filter = AssetImage('assets/anikit_images/group_5.png');
   static const image_play = AssetImage('assets/anikit_images/icon_play.png');
}
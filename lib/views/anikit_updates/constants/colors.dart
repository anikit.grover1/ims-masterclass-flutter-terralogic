import 'package:flutter/material.dart';

class AppColors
{
  static const ice_blue =Color(0xFFf2f4f4);
  static const dark_navy_blue = Color(0xFF00022c);
  static const black= Color(0xFF000000);
  static const gunmetal =Color(0xFF575d60);
  static const bluey_grey =Color(0xFF999aab);
  static const orange = Color(0xFFf7ae00);
  static const white = Color(0xFFffffff);
  static const azure = Color(0xFF00abfb);
  static const red = Color(0xFFdc4b52);
  static const card_background= Color(0xFFf4fafe);
  static const expandle_color = Color(0xFF3380cc);
  static const pale_blue = Color(0xFFd5e9fa);
  static const cloudy_blue = Color(0xFFbec8cc); 
  static const purpleish_blue = Color(0xFF5647eb);
  static const lightcolor = Color(0xFFe6eef4);
  static const black_shadow_bottomsheet = Color(0xFF50000000);
}
// import 'package:flutter/material.dart';
// import 'package:imsindia/routers/routes.dart';
// import 'package:imsindia/views/Analytics/analytics_home.dart';
// import 'package:imsindia/views/Arun/blogs/Blogs.dart';
// import 'package:imsindia/views/Arun/settings/Settings.dart';
// import 'package:imsindia/views/Emaximiser/emaximizer_home_screen.dart';
// import 'package:imsindia/views/GDPI/Gdpi_HomeScreen.dart';
// import 'package:imsindia/views/GK_Zone/gk_home_screen.dart';
// import 'package:imsindia/views/home_pages/home_widget.dart';
// import 'package:imsindia/views/practice_pages/practice_home_screen.dart';
// import 'package:imsindia/views/prepare_pages/prepare_home_screen.dart';
// import 'package:reorderables/reorderables.dart';

// class UpdatedBottomNavigation extends StatefulWidget {
//   const UpdatedBottomNavigation({Key key}) : super(key: key);

//   @override
//   _UpdatedBottomNavigation createState() => _UpdatedBottomNavigation();
// }

// class _UpdatedBottomNavigation extends State<UpdatedBottomNavigation>
//     with SingleTickerProviderStateMixin {
//   final double _iconSize = 90;
//   List<Widget> _tiles;
//   int _selectedIndex = 0;
//   bool isVisible = false, isEdit = false, isActive = false;
//   double containerHeight = 75.0;
//   AnimationController _animationController;
//   List<Widget> _widgetList = <Widget>[
//     HomeWidget(),
//     EmaximiserHomeScreen(),
//     PrepareHomeScreenHtml(),
//     Blogs(),
//     GKZoneHomePage(),
//     Gdpi_HomeScreen(),
//     PracticeHomeScreen(),
//     AnalyticsHomeScreen(),
//     Settings(true),
//   ];

//   @override
//   void initState() {
//     // TODO: implement initState
//     super.initState();
//     _animationController =
//         AnimationController(vsync: this, duration: Duration(milliseconds: 500));

//     _tiles = <Widget>[
//       Expanded(
//         flex: 1,
//         child: Column(
//           children: <Widget>[
//             IconButton(
//                 iconSize: 30.0,
//                 icon: Icon(Icons.lightbulb_outline_rounded),
//                 color: Colors.blueAccent,
//                 onPressed: () {
//                   AppRoutes.push(context, GKZoneHomePage());
//                 }),
//             Text(
//               "GK ZONE",
//               style: TextStyle(color: Colors.black),
//             )
//           ],
//         ),
//       ),
//       Expanded(
//         flex: 1,
//         child: Column(
//           children: <Widget>[
//             IconButton(
//               iconSize: 30.0,
//               icon: Icon(Icons.group_outlined),
//               color: Colors.blueAccent,
//               onPressed: () {
//                 //        AppRoutes.push(context, Gdpi_HomeScreen());
//               },
//             ),
//             Text(
//               "GDPI",
//               style: TextStyle(color: Colors.black),
//             )
//           ],
//         ),
//       ),
//       Expanded(
//         flex: 1,
//         child: Column(
//           children: <Widget>[
//             IconButton(
//               iconSize: 30.0,
//               icon: Icon(Icons.assignment_outlined),
//               color: Colors.blueAccent,
//               onPressed: () {
//                 AppRoutes.push(context, PracticeHomeScreen());
//               },
//             ),
//             Text(
//               "PRACTICE",
//               style: TextStyle(color: Colors.black),
//             )
//           ],
//         ),
//       ),
//       Expanded(
//         flex: 1,
//         child: Column(
//           children: <Widget>[
//             IconButton(
//               iconSize: 30.0,
//               icon: Icon(Icons.analytics_outlined),
//               color: Colors.blueAccent,
//               onPressed: () {
//                 AppRoutes.push(context, AnalyticsHomeScreen());
//               },
//             ),
//             Text(
//               "ANALYSE",
//               style: TextStyle(color: Colors.black),
//             )
//           ],
//         ),
//       ),
//       Expanded(
//         flex: 1,
//         child: Column(
//           children: <Widget>[
//             IconButton(
//               iconSize: 30.0,
//               icon: Icon(Icons.settings),
//               color: Colors.blueAccent,
//               onPressed: () {
//                 AppRoutes.push(context, Settings(true));
//               },
//             ),
//             Text(
//               "SETTINGS",
//               style: TextStyle(color: Colors.black),
//             )
//           ],
//         ),
//       ),
//       Expanded(
//         flex: 1,
//         child: Column(
//           children: <Widget>[
//             IconButton(
//                 iconSize: 30.0,
//                 icon: Icon(Icons.home_outlined),
//                 color: Colors.blueAccent,
//                 onPressed: () {
//                   AppRoutes.push(context, HomeWidget());
//                 }),
//             Text(
//               "HOME",
//               style: TextStyle(color: Colors.black),
//             )
//           ],
//         ),
//       ),
//       Expanded(
//         flex: 1,
//         child: Column(
//           children: <Widget>[
//             IconButton(
//               iconSize: 30.0,
//               icon: Icon(Icons.apartment_outlined),
//               color: Colors.blueAccent,
//               onPressed: () {
//                 AppRoutes.push(context, EmaximiserHomeScreen());
//               },
//             ),
//             Text(
//               "E-MAX",
//               style: TextStyle(color: Colors.black),
//             )
//           ],
//         ),
//       ),
//       Expanded(
//         flex: 1,
//         child: Column(
//           children: <Widget>[
//             IconButton(
//               iconSize: 30.0,
//               icon: Icon(Icons.playlist_play),
//               color: Colors.blueAccent,
//               onPressed: () {
//                 AppRoutes.push(context, PrepareHomeScreenHtml());
//               },
//             ),
//             Text(
//               "PREPARE",
//               style: TextStyle(color: Colors.black),
//             )
//           ],
//         ),
//       ),
//       Expanded(
//         flex: 1,
//         child: Column(
//           children: <Widget>[
//             IconButton(
//               iconSize: 30.0,
//               icon: Icon(Icons.article_outlined),
//               color: Colors.blueAccent,
//               onPressed: () {
//                 AppRoutes.push(context, Blogs());
//               },
//             ),
//             Text(
//               "BLOGS",
//               style: TextStyle(color: Colors.black),
//             )
//           ],
//         ),
//       ),
//       Expanded(
//         flex: 1,
//         child: Column(
//           children: <Widget>[
//             IconButton(
//               iconSize: 30.0,
//               icon: AnimatedIcon(
//                 icon: AnimatedIcons.menu_close,
//                 progress: _animationController,
//                 color: Colors.blueAccent,
//               ),
//               color: Colors.blueAccent,
//               onPressed: () {
//                 setState(() {
//                   if (containerHeight == 170.0)
//                     containerHeight = 75.0;
//                   else
//                     containerHeight = 170.0;

//                   isActive = !isActive;

//                   isActive
//                       ? _animationController.forward()
//                       : _animationController.reverse();

//                   if (isActive) {
//                     Future.delayed(Duration(milliseconds: 200), () {
//                       setState(() {
//                         isVisible = !isVisible;
//                         isEdit = !isEdit;
//                       });
//                     });
//                   } else {
//                     isVisible = !isVisible;
//                     isEdit = !isEdit;
//                   }
//                 });
//               },
//             ),
//             Text(
//               "MORE",
//               style: TextStyle(color: Colors.black),
//             )
//           ],
//         ),
//       ),
//     ];
//   }

//   @override
//   Widget build(BuildContext context) {
//     // adding the drag and drop features
//     void _onReorder(int oldIndex, int newIndex) {
//       setState(() {
//         Widget row = _tiles.removeAt(oldIndex);
//         _tiles.insert(newIndex, row);
//       });
//     }

//     var wrap = ReorderableWrap(
//         maxMainAxisCount: 5,
//         reorderAnimationDuration: Duration(seconds: 20),
//         spacing: 8.0,
//         runSpacing: 18.0,
//         padding: const EdgeInsets.all(8),
//         children: _tiles,
//         onReorder: _onReorder,
//         onNoReorder: (int index) {
//           //this callback is optional
//           debugPrint(
//               '${DateTime.now().toString().substring(5, 22)} reorder cancelled. index:$index');
//         },
//         onReorderStarted: (int index) {
//           //this callback is optional
//           debugPrint(
//               '${DateTime.now().toString().substring(5, 22)} reorder started: index:$index');
//         });

//     var column = Column(
//       children: <Widget>[
//         Align(
//           alignment: Alignment.centerRight,
//           child: Visibility(
//             child: Padding(
//                 padding: EdgeInsets.fromLTRB(0.0, 15.0, 20.0, 0.0),
//                 child: GestureDetector(
//                   onTap: () {
//                     setState(() {
//                       isEdit = true;
//                     });
//                   },
//                   child: Text(
//                     "Edit",
//                     style: TextStyle(color: Colors.blueAccent, fontSize: 15.0),
//                   ),
//                 )),
//             visible: isEdit,
//           ),
//         ),
//         isEdit == true
//             ? wrap
//             :
//             // Visibility(
//             //   visible: isVisible,
//             //   child: Expanded(
//             //     flex: 1,
//             //     child: Row(
//             //         mainAxisSize: MainAxisSize.max,
//             //         mainAxisAlignment: MainAxisAlignment.spaceEvenly,
//             //         children: <Widget>[
//             //           Expanded(
//             //             flex: 1,
//             //             child: Column(
//             //               children: <Widget>[
//             //                 IconButton(
//             //                     iconSize: 30.0,
//             //                     icon: Icon(Icons.lightbulb_outline_rounded),
//             //                     color: Colors.blueAccent,
//             //                     onPressed: () {
//             //                       //          AppRoutes.push(context, GKZoneHomePage());
//             //                     }),
//             //                 Text(
//             //                   "GK ZONE",
//             //                   style: TextStyle(color: Colors.black),
//             //                 )
//             //               ],
//             //             ),
//             //           ),
//             //           Expanded(
//             //             flex: 1,
//             //             child: Column(
//             //               children: <Widget>[
//             //                 IconButton(
//             //                   iconSize: 30.0,
//             //                   icon: Icon(Icons.group_outlined),
//             //                   color: Colors.blueAccent,
//             //                   onPressed: () {
//             //                     //        AppRoutes.push(context, Gdpi_HomeScreen());
//             //                   },
//             //                 ),
//             //                 Text(
//             //                   "GDPI",
//             //                   style: TextStyle(color: Colors.black),
//             //                 )
//             //               ],
//             //             ),
//             //           ),
//             //           Expanded(
//             //             flex: 1,
//             //             child: Column(
//             //               children: <Widget>[
//             //                 IconButton(
//             //                   iconSize: 30.0,
//             //                   icon: Icon(Icons.assignment_outlined),
//             //                   color: Colors.blueAccent,
//             //                   onPressed: () {
//             //                     //        AppRoutes.push(context, PracticeHomeScreen());
//             //                   },
//             //                 ),
//             //                 Text(
//             //                   "PRACTICE",
//             //                   style: TextStyle(color: Colors.black),
//             //                 )
//             //               ],
//             //             ),
//             //           ),
//             //           Expanded(
//             //             flex: 1,
//             //             child: Column(
//             //               children: <Widget>[
//             //                 IconButton(
//             //                   iconSize: 30.0,
//             //                   icon: Icon(Icons.analytics_outlined),
//             //                   color: Colors.blueAccent,
//             //                   onPressed: () {
//             //                     //       AppRoutes.push(context, AnalyticsHomeScreen());
//             //                   },
//             //                 ),
//             //                 Text(
//             //                   "ANALYSE",
//             //                   style: TextStyle(color: Colors.black),
//             //                 )
//             //               ],
//             //             ),
//             //           ),
//             //           Expanded(
//             //             flex: 1,
//             //             child: Column(
//             //               children: <Widget>[
//             //                 IconButton(
//             //                   iconSize: 30.0,
//             //                   icon: Icon(Icons.settings),
//             //                   color: Colors.blueAccent,
//             //                   onPressed: () {
//             //                     //       AppRoutes.push(context, Settings(true));
//             //                   },
//             //                 ),
//             //                 Text(
//             //                   "SETTINGS",
//             //                   style: TextStyle(color: Colors.black),
//             //                 )
//             //               ],
//             //             ),
//             //           ),
//             //         ]),
//             //   ),
//             // ),
//             Expanded(
//                 flex: 1,
//                 child: Row(
//                     mainAxisSize: MainAxisSize.max,
//                     mainAxisAlignment: MainAxisAlignment.spaceEvenly,
//                     children: <Widget>[
//                       Expanded(
//                         flex: 1,
//                         child: Column(
//                           children: <Widget>[
//                             IconButton(
//                                 iconSize: 30.0,
//                                 icon: Icon(Icons.home_outlined),
//                                 color: Colors.blueAccent,
//                                 onPressed: () {
//                                   //        AppRoutes.push(context, HomeWidget());
//                                 }),
//                             Text(
//                               "HOME",
//                               style: TextStyle(color: Colors.black),
//                             )
//                           ],
//                         ),
//                       ),
//                       Expanded(
//                         flex: 1,
//                         child: Column(
//                           children: <Widget>[
//                             IconButton(
//                               iconSize: 30.0,
//                               icon: Icon(Icons.apartment_outlined),
//                               color: Colors.blueAccent,
//                               onPressed: () {
//                                 //      AppRoutes.push(context, EmaximiserHomeScreen());
//                               },
//                             ),
//                             Text(
//                               "E-MAX",
//                               style: TextStyle(color: Colors.black),
//                             )
//                           ],
//                         ),
//                       ),
//                       Expanded(
//                         flex: 1,
//                         child: Column(
//                           children: <Widget>[
//                             IconButton(
//                               iconSize: 30.0,
//                               icon: Icon(Icons.playlist_play),
//                               color: Colors.blueAccent,
//                               onPressed: () {
//                                 //       AppRoutes.push(context, PrepareHomeScreenHtml());
//                               },
//                             ),
//                             Text(
//                               "PREPARE",
//                               style: TextStyle(color: Colors.black),
//                             )
//                           ],
//                         ),
//                       ),
//                       Expanded(
//                         flex: 1,
//                         child: Column(
//                           children: <Widget>[
//                             IconButton(
//                               iconSize: 30.0,
//                               icon: Icon(Icons.article_outlined),
//                               color: Colors.blueAccent,
//                               onPressed: () {
//                                 //     AppRoutes.push(context, Blogs());
//                               },
//                             ),
//                             Text(
//                               "BLOGS",
//                               style: TextStyle(color: Colors.black),
//                             )
//                           ],
//                         ),
//                       ),
//                       Expanded(
//                         flex: 1,
//                         child: Column(
//                           children: <Widget>[
//                             IconButton(
//                               iconSize: 30.0,
//                               icon: AnimatedIcon(
//                                 icon: AnimatedIcons.menu_close,
//                                 progress: _animationController,
//                                 color: Colors.blueAccent,
//                               ),
//                               color: Colors.blueAccent,
//                               onPressed: () {
//                                 setState(() {
//                                   if (containerHeight == 170.0)
//                                     containerHeight = 75.0;
//                                   else
//                                     containerHeight = 170.0;

//                                   isActive = !isActive;

//                                   isActive
//                                       ? _animationController.forward()
//                                       : _animationController.reverse();

//                                   if (isActive) {
//                                     Future.delayed(Duration(milliseconds: 200),
//                                         () {
//                                       setState(() {
//                                         isVisible = !isVisible;
//                                         isEdit = !isEdit;
//                                       });
//                                     });
//                                   } else {
//                                     isVisible = !isVisible;
//                                     isEdit = !isEdit;
//                                   }
//                                 });
//                               },
//                             ),
//                             Text(
//                               "MORE",
//                               style: TextStyle(color: Colors.black),
//                             )
//                           ],
//                         ),
//                       ),
//                     ]),
//               ),
//       ],
//     );

//     return Scaffold(
//         body: SafeArea(
//       child: Container(
//         child: column,
//       ),
//     ));
//   }
// }

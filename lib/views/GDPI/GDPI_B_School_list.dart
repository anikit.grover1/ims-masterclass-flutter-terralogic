import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/material.dart';
import 'package:flutter_document_picker/flutter_document_picker.dart';
import 'package:flutter_svg/svg.dart';
import 'package:imsindia/components/custom_DropDown_GDPI.dart';

import 'package:imsindia/routers/routes.dart';
import 'package:imsindia/utils/screen_util.dart';
import 'package:imsindia/utils/svg_icons.dart';
import 'package:imsindia/resources/strings/GDPI_labels.dart';
import 'package:imsindia/utils/colors.dart';
import 'package:imsindia/views/GDPI/GDPI_home_scr.dart';
import 'package:imsindia/components/primary_Button.dart';
import 'package:imsindia/components/custom_expansion_panel.dart';
import 'package:sliding_up_panel/sliding_up_panel.dart';
import 'GDPI_PrepareForPI_ScoreBoard_Screen.dart';
import 'package:imsindia/utils/svg_images/prepare_svg_images.dart';





List BschoolList = [
  'FMS - Chennai',
  'FMS - Delhi',
  'IIM - Ahmedabad PGP FABM',
  'IIM C + ISI + IIT K',
  'MICA Ahmedabad',
  'IIM C + ISI + IIT K',
  'MICA Ahmedabad',
];

//List<String> dropDownMonths = [];
class ScoreService {
  static final List<String> score = ["Overall Score", "Bucuresti", "Timisoara", "Brasov", "Constanta"];

  static List<String> getSuggestions(String query) {
    List<String> matches = List();
    matches.addAll(score);

    // matches.retainWhere((s) => s.toLowerCase().contains(query.toLowerCase()));
    return matches;
  }
}
class PercentileService {
  static final List<String> percentile =
  ["Overall Percentile", "Bucuresti", "Timisoara", "Brasov", "Constanta"];

  static List<String> getSuggestions(String query) {
    List<String> matches = List();
    matches.addAll(percentile);

    // matches.retainWhere((s) => s.toLowerCase().contains(query.toLowerCase()));
    return matches;
  }
}



class GDPIBSchool extends StatefulWidget {
  @override
  GDPIBSchoolState createState() => GDPIBSchoolState();
}

class GDPIBSchoolState extends State<GDPIBSchool>{
  int _activeMeterIndex;
  bool _pickFileInProgress = false;
  String _path = '-';
  bool _iosPublicDataUTI = true;
  bool _checkByCustomExtension = false;
  bool _checkByMimeType = false;
  bool uploaded = false;
  final _BSchoolControllerForPopUp = PanelController();
  final TextEditingController _score = new TextEditingController();
  final TextEditingController _percentile = new TextEditingController();
  _pickDocument() async {
    String result;
    try {
      setState(() {
        _path = '-';
        _pickFileInProgress = true;
      });
      final _utiController = TextEditingController(
        text: 'com.sidlatau.example.mwfbak',
      );

      final _extensionController = TextEditingController(
        text: 'mwfbak',
      );

      final _mimeTypeController = TextEditingController(
        text: 'application/pdf image/png',
      );
      FlutterDocumentPickerParams params = FlutterDocumentPickerParams(
        allowedFileExtensions: _checkByCustomExtension
            ? _extensionController.text
            .split(' ')
            .where((x) => x.isNotEmpty)
            .toList()
            : null,
        allowedUtiTypes: _iosPublicDataUTI
            ? null
            : _utiController.text
            .split(' ')
            .where((x) => x.isNotEmpty)
            .toList(),
        allowedMimeTypes: _checkByMimeType
            ? _mimeTypeController.text
            .split(' ')
            .where((x) => x.isNotEmpty)
            .toList()
            : null,
      );

      result = await FlutterDocumentPicker.openDocument(params: params);
    } catch (e) {
      print(e);
      result = 'Error: $e';
    } finally {
      setState(() {
        _pickFileInProgress = false;
      });
    }

    setState(() {
      uploaded = true;
      _path = result;
      print(uploaded);
      print(_path);
      //print(_path.length.toString());
    });
  }




  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }



  @override
  Future<bool> _onWillPop() {
    //back
    AppRoutes.push(context, GDPIHomescreen());
  }
  Widget build(BuildContext context) {
    // TODO: implement build
    final screenHeight = MediaQuery.of(context).size.height;//720dp
    final screenWidth = MediaQuery.of(context).size.width;//360dp
    void _modalBottomSheetMenu() {
      showModalBottomSheet(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.only(topRight: Radius.circular(20),topLeft: Radius.circular(20)),
          ),
          context: context,
          builder: (builder) {
            return new Container(
              decoration: new BoxDecoration(
                boxShadow: [
                  new BoxShadow(
                    color: Colors.white,
                    blurRadius: 10.0,spreadRadius: 0.0,offset: new Offset(0.0, 10.0),
                  )
                ],
              ),
              height:  (430/720)*screenHeight,
              child: Container(
                height: (430/638)*screenHeight,
                width: (360/360)*screenWidth,
                margin: EdgeInsets.only(top: (30/720)*screenHeight),
                child: Column(
                  children: <Widget>[
                    Container(
                      height: (25/720)*screenHeight,
                      width: (272/360)*screenWidth,
                      margin: EdgeInsets.only(left: (44/360)*screenWidth,right: (44/360)*screenWidth),
                      child: Text(
                        GDPIHomeString.B_School_Name,
                        style: TextStyle(
                          color: NeutralColors.black,
                          fontSize: (16/360)*screenWidth,
                          fontFamily: "IBMPlexSans",
                          fontWeight: FontWeight.bold,
                        ),
                        textAlign: TextAlign.center,
                      ),
                    ),
                    Container(
                      height: (45/720)*screenHeight,
                      width: (280/360)*screenWidth,
                      margin: EdgeInsets.only(top: (20/720)*screenHeight),
                      child: DottedBorder(
                          padding: EdgeInsets.all(4),
                          dashPattern: [3],
                          borderType: BorderType.RRect,
                          radius: Radius.circular(5),
                          color: NeutralColors.gunmetal.withOpacity(0.3),
                          child: Row(
                            children: <Widget>[
                              Container(
                                margin: EdgeInsets.only(left: (15/360)*screenWidth),
                                alignment: Alignment.centerLeft,
                                child: uploaded
                                    ? Text('$_path', overflow: TextOverflow.ellipsis)
                                    : Text(
                                        GDPIHomeString.CAT_Scorecard,
                                        style: TextStyle(
                                          color: NeutralColors.charcoal_grey,
                                          fontSize: 14/360*screenWidth,
                                          fontFamily: "IBMPlexSans",
                                          fontWeight: FontWeight.w500,
                                        ),
                                        textAlign: TextAlign.center,
                                    ),
                              ),
                              Container(
                                margin: EdgeInsets.only(left: (100/360)*screenWidth),
                                child: InkWell(
                                  onTap: _pickFileInProgress ? null : _pickDocument,
                                  child: Image.asset(
                                    "assets/images/clip-4.png",
                                    fit: BoxFit.none,
                                    color: Color.fromARGB(255, 0, 171, 251),
                                  ),
                                ),
                              ),
                            ],
                          )
                      ),
                    ),
                    Container(
                      height: (45/720)*screenHeight,
                      width: (280/360)*screenWidth,
                      margin: EdgeInsets.only(top: (20/720)*screenHeight),
                      child: DottedBorder(
                          padding: EdgeInsets.all(4),
                          dashPattern: [3],
                          borderType: BorderType.RRect,
                          radius: Radius.circular(5),
                          color: NeutralColors.gunmetal.withOpacity(0.3),
                          child: Row(
                            children: <Widget>[
                              Container(
                                margin: EdgeInsets.only(left: (15/360)*screenWidth),
                                alignment: Alignment.centerLeft,
                                child: uploaded
                                    ? Text('$_path', overflow: TextOverflow.ellipsis)
                                    :Text(
                                      GDPIHomeString.Call_Letter,
                                      style: TextStyle(
                                        color: NeutralColors.charcoal_grey,
                                        fontSize: 14/360*screenWidth,
                                        fontFamily: "IBMPlexSans",
                                        fontWeight: FontWeight.w500,
                                      ),
                                      textAlign: TextAlign.center,
                                    ),
                              ),
                              Container(
                                margin: EdgeInsets.only(left: (165/360)*screenWidth),
                                child: InkWell(
                                  onTap: _pickFileInProgress ? null : _pickDocument,
                                  child: Image.asset(
                                    "assets/images/clip-4.png",
                                    fit: BoxFit.none,
                                    color: Color.fromARGB(255, 0, 171, 251),
                                  ),
                                ),
                              ),
                            ],
                          )
                      ),
                    ),
                    Container(
                      child: Center(
                        child: InkWell(
                          onTap: (){
                            //_BSchoolControllerForPopUp.close();
                            Navigator.pop(context);
                          },
                          child: Container(
                            height: (40/720) * screenHeight,
                            width: (130/360)*screenWidth,
                            margin: EdgeInsets.only(top: (40/720)*screenHeight,bottom: (97/720)*screenHeight),
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.all(
                                    Radius.circular(2)
                                ),
                                gradient: LinearGradient(
                                    end: Alignment(1.0199999809265137, 1.0099999904632568),
                                    colors: [SemanticColors.light_purpely,SemanticColors.dark_purpely])
                            ),
                            child: Center(
                              child: Text(
                                GDPIHomeString.Submit,
                                style: TextStyle(
                                  color: NeutralColors.pureWhite,
                                  fontSize: 14/360*screenWidth,
                                  fontFamily: "IBMPlexSans ",
                                  fontWeight: FontWeight.w500,
                                ),
                                textAlign: TextAlign.center,
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            );
          }
      );
    }
    return  Scaffold(
      body:  Container(
        margin: EdgeInsets.only(
          top: 0 / Constant.defaultScreenHeight * screenHeight,
        ),
        child: Column(
          children: <Widget>[
            Container(
              margin: EdgeInsets.only(
                top: (42 / 720) * screenHeight,
              ),
              child: Row(
                //crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  InkWell(
                    onTap: () {
                      Navigator.pop(context);
                    },
                    child: Container(
                      width: (58 / 360) * screenWidth,
                      height: (30.0 / 720) * screenHeight,
                      margin: EdgeInsets.only(left: 0.0),
                      child: SvgPicture.asset(
                        getPrepareSvgImages.backIcon,
                        height: (5 / 720) * screenHeight,
                        width: (14 / 360) * screenWidth,
                        fit: BoxFit.none,
                      ),
                    ),
                  ),
                  Container(
                    height: (20 / 720) * screenHeight,
                    // margin: EdgeInsets.only(left: (20 / 360) * screenWidth),
                    child: Text(
                      "GD-PI",
                      style: TextStyle(
                        color: NeutralColors.black,
                        fontSize: (16 / 720) * screenHeight,
                        fontFamily: "IBMPlexSans",
                        fontWeight: FontWeight.w500,
                      ),
                      textAlign: TextAlign.left,
                    ),
                  ),
                ],
              ),
            ),
            Container(
              height: (69/720)*screenHeight,
              width: (320/360)*screenWidth,
              margin: EdgeInsets.only(left: (20/360)*screenWidth,right: (20/360)*screenWidth,top: (30/720)*screenHeight),
              child: Text(
                GDPIHomeString.Select_BSCHOOL,
                style: TextStyle (
                  color: NeutralColors.gunmetal,
                  fontWeight: FontWeight.normal,
                  fontFamily: "IBMPlexSans",
                  fontStyle: FontStyle.normal,
                  fontSize: (14/360)*screenWidth,
                ),
                textAlign: TextAlign.start,
              ),
            ),
            Expanded(
              child: Container(
                margin: EdgeInsets.only(left: 20/360 * screenWidth, right: 20/360 * screenWidth, top: 20/720 * screenHeight),
                child: ListView.builder(
                  shrinkWrap: true,
                  itemCount: BschoolList.length,
                  padding: EdgeInsets.only(bottom: (40)),
                  itemBuilder: (context, int i) {
                    return
                      Container(
                        //margin: EdgeInsets.only(left: (20/360)*screenWidth,right: (20/360)*screenWidth),
                        child:Container(
                          margin: EdgeInsets.only(top: (10/720)*screenHeight),
                          child: CustomExpansionPanelList(
                            expansionHeaderHeight: (45/720)*screenHeight,
                            iconColor: (_activeMeterIndex == i)?Colors.white:Colors.black,
                            backgroundColor1: (_activeMeterIndex == i)?NeutralColors.purpleish_blue:SemanticColors.light_purpely.withOpacity(0.1),
                            backgroundColor2:(_activeMeterIndex == i)? NeutralColors.purpleish_blue:SemanticColors.dark_purpely.withOpacity(0.1),
                            expansionCallback: (int index, bool status) {
                              setState(() {
                                _activeMeterIndex = _activeMeterIndex == i ? null : i;
                              });
                            },
                            children: [
                              new ExpansionPanel(
                                  canTapOnHeader: true,
                                  isExpanded: _activeMeterIndex == i,
                                  headerBuilder: (BuildContext context, bool isExpanded)=>
                                  new Container(
                                    alignment: Alignment.centerLeft,
                                    child: new Padding(
                                      padding: EdgeInsets.only(left: (15/360)*screenWidth),
                                      child: new Text(
                                          BschoolList[i],
                                          style: new TextStyle(
                                              fontSize: (14/720)*screenHeight,
                                              fontFamily: "IBMPlexSans",
                                              fontWeight: FontWeight.w500,
                                              color: (_activeMeterIndex == i) ?NeutralColors.pureWhite:NeutralColors.charcoal_grey)),
                                    ),
                                  ),
                                  body: Container(
                                    decoration: new BoxDecoration(
                                      boxShadow: [
                                        new BoxShadow(
                                          color: Colors.white,
                                          blurRadius: 10.0,spreadRadius: 0.0,offset: new Offset(0.0, 10.0),
                                        )
                                      ],
                                    ),
                                    height:  (151/720)*screenHeight,
                                    child: Container(
                                      height: (151/720)*screenHeight,
                                      width: (320/360)*screenWidth,
                                      margin: EdgeInsets.only(top: (19.2/720)*screenHeight),
                                      child: Column(
                                        children: <Widget>[
                                          Container(
                                            height: (45/720)*screenHeight,
                                            width: (280/360)*screenWidth,
                                            child: DottedBorder(
                                                padding: EdgeInsets.all(4),
                                                dashPattern: [3],
                                                borderType: BorderType.RRect,
                                                radius: Radius.circular(5),
                                                color: NeutralColors.gunmetal.withOpacity(0.3),
                                                child: Row(
                                                  children: <Widget>[
                                                    Container(
                                                      margin: EdgeInsets.only(left: (15/360)*screenWidth),
                                                      alignment: Alignment.centerLeft,
                                                      child: Text(
                                                        GDPIHomeString.CAT_Scorecard,
                                                        style: TextStyle(
                                                          color: NeutralColors.charcoal_grey,
                                                          fontSize: 14/360*screenWidth,
                                                          fontFamily: "IBMPlexSans",
                                                          fontWeight: FontWeight.normal,
                                                        ),
                                                        textAlign: TextAlign.center,
                                                      ),
                                                    ),
                                                    Container(
                                                      margin: EdgeInsets.only(left: (100/360)*screenWidth),
                                                      child: Padding(
                                                        padding: const EdgeInsets.all(5.0),
                                                        child: InkWell(
                                                          onTap: (){
                                                            _modalBottomSheetMenu();
                                                          },
                                                          child:  Image.asset(
                                                            "assets/images/clip-4.png",
                                                            fit: BoxFit.none,
                                                            color: Color.fromARGB(255, 0, 171, 251),
                                                          ),
                                                        ),
                                                      ),
                                                    ),
                                                  ],
                                                )
                                            ),
                                          ),
                                          Container(
                                            height: (45/720)*screenHeight,
                                            width: (280/360)*screenWidth,
                                            margin: EdgeInsets.only(top: (20/720)*screenHeight),
                                            child: DottedBorder(
                                                padding: EdgeInsets.all(4),
                                                dashPattern: [3],
                                                borderType: BorderType.RRect,
                                                radius: Radius.circular(5),
                                                color: NeutralColors.gunmetal.withOpacity(0.3),
                                                child: Row(
                                                  children: <Widget>[
                                                    Container(
                                                      margin: EdgeInsets.only(left: (15/360)*screenWidth),
                                                      alignment: Alignment.centerLeft,
                                                      child: Text(
                                                        GDPIHomeString.Call_Letter,
                                                        style: TextStyle(
                                                          color: NeutralColors.charcoal_grey,
                                                          fontSize: 14/360*screenWidth,
                                                          fontFamily: "IBMPlexSans",
                                                          fontWeight: FontWeight.normal,
                                                        ),
                                                        textAlign: TextAlign.center,
                                                      ),
                                                    ),
                                                    Container(
                                                      margin: EdgeInsets.only(left: (165/360)*screenWidth),
                                                      child: Padding(
                                                        padding: const EdgeInsets.all(5.0),
                                                        child: InkWell(
                                                          onTap: (){
                                                            _modalBottomSheetMenu();
                                                          },
                                                          child: Image.asset(
                                                            "assets/images/clip-4.png",
                                                            fit: BoxFit.none,
                                                            color: Color.fromARGB(255, 0, 171, 251),
                                                          ),
                                                        ),
                                                      ),
                                                    ),
                                                  ],
                                                )
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  )
                              )
                            ],
                          ),
                        ),
                      );
                  },
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}


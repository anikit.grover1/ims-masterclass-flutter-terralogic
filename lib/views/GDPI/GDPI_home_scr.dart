import 'package:flutter/material.dart';
import 'package:imsindia/utils/colors.dart';
import 'package:imsindia/resources/strings/GDPI_labels.dart';
import 'package:imsindia/utils/svg_images/prepare_svg_images.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:imsindia/routers/routes.dart';
import 'package:imsindia/views/GDPI/GDPI_B_School_Processes.dart';
import 'package:imsindia/views/GDPI/GDPI_Lists_B_School.dart';
import 'package:imsindia/views/GDPI/GDPI_PrepareForGD-CASE_WAT_Screen.dart';
import 'package:imsindia/views/GDPI/GDPI_PrepareForPI_Screen.dart';
import 'package:imsindia/components/navigation_bar.dart';
import 'package:imsindia/views/home_pages/home_widget.dart';

class GDPIHomescreen extends StatefulWidget{
  @override
  GDPIHomescreenState createState() => GDPIHomescreenState();
}

class GDPIHomescreenState extends State<GDPIHomescreen>{
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }
  @override
  Future<bool> _onWillPop() {
    //back
    AppRoutes.push(context, HomeWidget());
  }
  Widget build(BuildContext context) {
    // TODO: implement build
    final screenHeight = MediaQuery.of(context).size.height;//720dp
    final screenWidth = MediaQuery.of(context).size.width;//360dp
    return  Scaffold(
        appBar: AppBar(
          brightness: Brightness.light,
          centerTitle: false,
          titleSpacing: 0.0,
          backgroundColor: Colors.transparent,
          elevation: 0.0,
          iconTheme: IconThemeData(
            color: NeutralColors.black, //change your color here
          ),
          title: Container(
            width: (302 / 360) * screenWidth,
            child: Text(
              GDPIHomeString.GDPI_Title,
              style: TextStyle(
                color: NeutralColors.black,
                fontSize: (16 / 720) * screenHeight,
                fontFamily: "IBMPlexSans",
                fontWeight: FontWeight.w500,
              ),
              textAlign: TextAlign.left,
            ),
          ),
        ),
//        drawer: NavigationDrawer(),
        body: Container(
          margin: EdgeInsets.only(top: (7/720)*screenHeight,left: (20/360)*screenWidth),
          child: Column(
            children: <Widget>[
              Container(
                margin: EdgeInsets.only(top: (25/720)*screenHeight),
                child: InkWell(
                  onTap: (){
                    AppRoutes.pushWithAnmation(context, GDPIPrepareForPIScreen());
                  },
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      Container(
                        height: (40/720)*screenHeight,
                        width: (3/360)*screenWidth,
                        color: SemanticColors.dark_purpely,
                      ),
                      Container(
                        height: (18/720)*screenHeight,
                        width: (290/360)*screenWidth,
                        margin: EdgeInsets.only(left: (10/360)*screenWidth),
                        child: Text(
                          GDPIHomeString.First_list,
                        //  overflow: TextOverflow.ellipsis,
                          style: TextStyle (
                            color: NeutralColors.dark_navy_blue,
                            fontWeight: FontWeight.w700,
                            fontFamily: "IBMPlexSans",
                            fontStyle: FontStyle.normal,
                            fontSize: (14/720)*screenHeight,
                          ),
                          textAlign: TextAlign.start,
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(right: (15/360)*screenWidth,bottom: (4/720)*screenHeight),
                        child: Container(
                          child: Padding(
                            padding: const EdgeInsets.all(3.0),
                            child: Align(
                              child: Icon(
                                Icons.arrow_forward_ios,
                                color: NeutralColors.blue_grey,
                                size: 11/720*screenHeight,
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              Container(
                margin: EdgeInsets.only(top: (25/720)*screenHeight),
                child: InkWell(
                  onTap: (){
                    AppRoutes.pushWithAnmation(context, GDPIPrepareForGD_CASE_WATScreen());
                  },
                  child: Row(
                    children: <Widget>[
                      Container(
                        height: (40/720)*screenHeight,
                        width: (3/360)*screenWidth,
                        color: NeutralColors.grapefruit,
                      ),
                      Container(
                        height: (18/720)*screenHeight,
                        width: (290/360)*screenWidth,
                        margin: EdgeInsets.only(left: (10/360)*screenWidth),
                        child: Text(
                          GDPIHomeString.Second_list,
                        //  overflow: TextOverflow.ellipsis,
                          style: TextStyle (
                            color: NeutralColors.dark_navy_blue,
                            fontWeight: FontWeight.w700,
                            fontFamily: "IBMPlexSans",
                            fontStyle: FontStyle.normal,
                            fontSize: (14/720)*screenHeight,
                          ),
                          textAlign: TextAlign.start,
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(right: (15/360)*screenWidth,bottom: (4/720)*screenHeight),
                        child: Container(
                          child: Padding(
                            padding: const EdgeInsets.all(3.0),
                            child: Align(
                              child: Icon(
                                Icons.arrow_forward_ios,
                                color: NeutralColors.blue_grey,
                                size: 11/720*screenHeight,
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              Container(
                margin: EdgeInsets.only(top: (25/720)*screenHeight),
                child: InkWell(
                  onTap: (){
                    AppRoutes.pushWithAnmation(context, GDPIProcess());
                  },
                  child: Row(
                    children: <Widget>[
                      Container(
                        height: (40/720)*screenHeight,
                        width: (3/360)*screenWidth,
                        color: NeutralColors.mango,
                      ),
                      Container(
                        height: (18/720)*screenHeight,
                        width: (290/360)*screenWidth,
                        margin: EdgeInsets.only(left: (10/360)*screenWidth),
                        child: Text(
                          GDPIHomeString.Third_list,
                        //  overflow: TextOverflow.ellipsis,
                          style: TextStyle (
                            color: NeutralColors.dark_navy_blue,
                            fontWeight: FontWeight.w700,
                            fontFamily: "IBMPlexSans",
                            fontStyle: FontStyle.normal,
                            fontSize: (14/720)*screenHeight,
                          ),
                          textAlign: TextAlign.start,
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(right: (15/360)*screenWidth,bottom: (4/720)*screenHeight),
                        child: Container(
                          child: Padding(
                            padding: const EdgeInsets.all(3.0),
                            child: Align(
                              child: Icon(
                                Icons.arrow_forward_ios,
                                color: NeutralColors.blue_grey,
                                size: 11/720*screenHeight,
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      );

  }
}

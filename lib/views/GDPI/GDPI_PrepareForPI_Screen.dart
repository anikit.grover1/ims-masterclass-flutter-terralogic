import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/material.dart';
import 'package:imsindia/routers/routes.dart';
import 'package:imsindia/utils/svg_icons.dart';
import 'package:imsindia/resources/strings/GDPI_labels.dart';
import 'package:imsindia/utils/colors.dart';
import 'package:imsindia/views/GDPI/GDPI_home_scr.dart';
import 'package:imsindia/components/primary_Button.dart';
import 'package:imsindia/components/custom_expansion_panel.dart';
import 'GDPI_PrepareForPI_ScoreBoard_Screen.dart';





List GDPI_TITLE = [
  'CAT',
  'IIFT',
  'XAT',
  'NMAT',
  'SNAP',
  'CAT',
  'IIFT',
  'XAT',
  'NMAT',
];


class GDPIPrepareForPIScreen extends StatefulWidget {
  GDPIPrepareForPIScreen({Key key}) : super(key: key);
  @override
  GDPIPrepareForPIScreenState createState() => GDPIPrepareForPIScreenState();
}

class GDPIPrepareForPIScreenState extends State<GDPIPrepareForPIScreen>{
  int _activeMeterIndex;
  List Score =
  ["Overall Score", "Bucuresti", "Timisoara", "Brasov", "Constanta"];
  List Percentile =
  ["Overall Percentile", "Bucuresti", "Timisoara", "Brasov", "Constanta"];

  List<DropdownMenuItem<String>> _dropDownMenuItems_score;
  List<DropdownMenuItem<String>> _dropDownMenuItems_percentile;
  String score;
  String percentile;


  @override
  void initState() {
    // TODO: implement initState
    _dropDownMenuItems_score = getDropDownMenuItems_scrore();
    _dropDownMenuItems_percentile = getDropDownMenuItems_percentile();
    score = _dropDownMenuItems_score[0].value;
    percentile = _dropDownMenuItems_percentile[0].value;
    super.initState();
  }

  List<DropdownMenuItem<String>> getDropDownMenuItems_scrore() {
    List<DropdownMenuItem<String>> items = new List();
    for (String city in Score) {
      items.add(new DropdownMenuItem(
          value: city,
          child: new Text(city)
      ));
    }
    return items;
  }

  List<DropdownMenuItem<String>> getDropDownMenuItems_percentile() {
    List<DropdownMenuItem<String>> items = new List();
    for (String city in Percentile) {
      items.add(new DropdownMenuItem(
          value: city,
          child: new Text(city)
      ));
    }
    return items;
  }


  @override
  Future<bool> _onWillPop() {
    //back
    AppRoutes.push(context, GDPIHomescreen());
  }
  Widget build(BuildContext context) {
    // TODO: implement build
    final screenHeight = MediaQuery.of(context).size.height;//720dp
    final screenWidth = MediaQuery.of(context).size.width;//360dp
    return  Scaffold(
        body:  Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
              child: Row(
                children: <Widget>[
                  Container(
                    child: InkWell(
                      onTap: (){
                        Navigator.pop(context);
                      },
                      child: Container(
                        child: Padding(
                          padding: const EdgeInsets.all(5.0),
                          child: Align(
                            alignment: Alignment.topLeft,
                            child: Container(
                              width: (17/360)*screenWidth,
                              height: (14/720)*screenHeight,
                              margin: EdgeInsets.only(top: (42/720)*screenHeight,left: (18/360)*screenWidth),
                              child: getSvgIcon.backSvgIcon,
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                  Container(
                    height: (20/720)*screenHeight,
                    // width: (46/360)*screenWidth,
                    margin: EdgeInsets.only(left: (20/360)*screenWidth,top: (42/720)*screenHeight),
                    child: Text(
                      GDPIHomeString.GDPI_Title,
                      style: TextStyle (
                        color: NeutralColors.black,
                        fontWeight: FontWeight.w700,
                        fontFamily: "IBMPlexSans",
                        fontStyle: FontStyle.normal,
                        fontSize: (16/360)*screenWidth,
                      ),
                      textAlign: TextAlign.start,
                    ),
                  ),
                ],
              ),
            ),
            Container(
              height: (27/720)*screenHeight,
              //width: (120/360)*screenWidth,
              margin: EdgeInsets.only(top: (30/720)*screenHeight,left: (20/360)*screenWidth),
              child: Text(
                GDPIHomeString.First_list,
                overflow: TextOverflow.ellipsis,
                style: TextStyle (
                  color: NeutralColors.preparePi_balck,
                  fontWeight: FontWeight.bold,
                  fontFamily: "IBMPlexSans",
                  fontStyle: FontStyle.normal,
                  fontSize: (18/360)*screenWidth,
                ),
                textAlign: TextAlign.start,
              ),
            ),
            Container(
              height: (23/720)*screenHeight,
              width: (225/360)*screenWidth,
              margin: EdgeInsets.only(top: (5/720)*screenHeight,left: (20/360)*screenWidth),
              child: Text(
                GDPIHomeString.Sub_header,
                overflow: TextOverflow.ellipsis,
                style: TextStyle (
                  color: NeutralColors.gunmetal,
                  fontWeight: FontWeight.w400,
                  fontFamily: "IBMPlexSans",
                  fontStyle: FontStyle.normal,
                  fontSize: (14/360)*screenWidth,
                ),
                textAlign: TextAlign.start,
              ),
            ),
            Expanded(
              child: LayoutBuilder(builder: (BuildContext context, BoxConstraints viewportConstraints){
                return SingleChildScrollView(
                  child: ConstrainedBox(constraints: BoxConstraints(minHeight: viewportConstraints.maxHeight),
                    child: Column(
                      children: <Widget>[
                        Container(
                          margin: EdgeInsets.only(left: (20/360)*screenWidth,right: (20/360)*screenWidth,top: (20/720)*screenHeight),
                          child: getList_GDPI_PreparePI(screenHeight,screenWidth),
                        ),
                        Container(
                          child: Center(
                            child: InkWell(
                              onTap: (){
                                AppRoutes.pushWithAnmation(context, GDPIPrepareForPIScoreBoardScreen());
                              },
                              child: Container(
                                height: (40/720) * screenHeight,
                                width: (270/360)*screenWidth,
                                margin: EdgeInsets.only(top: (40/720)*screenHeight,bottom: (97/720)*screenHeight),
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.all(
                                        Radius.circular(2)
                                    ),
                                    gradient: LinearGradient(
                                        end: Alignment(1.0199999809265137, 1.0099999904632568),
                                        colors: [SemanticColors.light_purpely,SemanticColors.dark_purpely])
                                ),
                                child: Center(
                                  child: Text(
                                    GDPIHomeString.Submit,
                                    style: TextStyle(
                                      color: NeutralColors.pureWhite,
                                      fontSize: 14/360*screenWidth,
                                      fontFamily: "IBMPlexSans",
                                      fontWeight: FontWeight.w500,
                                    ),
                                    textAlign: TextAlign.center,
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                );
              }),
            ),
          ],
        ),
      );

  }

  void changedDropDownItem(String selectedCity) {
    setState(() {
      score = selectedCity;
    });
  }
  void changedDropDownItem_percentile(String selectedCity) {
    setState(() {
      percentile = selectedCity;
    });
  }

  Widget getList_GDPI_PreparePI(final screenHeight,final screenWidth){
    List<Widget> list_gdpi = new List<Widget>();
    for(var Count=0; Count < GDPI_TITLE.length; Count++){
      list_gdpi.add(
          new Container(
            child:Container(
              margin: EdgeInsets.only(top: (10/720)*screenHeight),
              child: CustomExpansionPanelList(
                expansionHeaderHeight: (45/720)*screenHeight,
                iconColor: (_activeMeterIndex == Count)?Colors.white:Colors.black,
                backgroundColor1: (_activeMeterIndex == Count)?NeutralColors.purpleish_blue:SemanticColors.light_purpely.withOpacity(0.1),
                backgroundColor2:(_activeMeterIndex == Count)? NeutralColors.purpleish_blue:SemanticColors.dark_purpely.withOpacity(0.1),
                expansionCallback: (int index, bool status) {
                  setState(() {
                    _activeMeterIndex = _activeMeterIndex == Count ? null : Count;
                  });
                },
                children: [
                  new ExpansionPanel(
                      canTapOnHeader: true,
                      isExpanded: _activeMeterIndex == Count,
                      headerBuilder: (BuildContext context, bool isExpanded)=>
                      new Container(
                        alignment: Alignment.centerLeft,
                        child: new Padding(
                          padding: EdgeInsets.only(left: (15/360)*screenWidth),
                          child: new Text(
                              GDPI_TITLE[Count],
                              style: new TextStyle(
                                  fontSize: (13.6/720)*screenHeight,
                                  fontFamily: "IBMPlexSans",
                                  fontWeight: FontWeight.w500,
                                  color: (_activeMeterIndex == Count) ?NeutralColors.pureWhite:NeutralColors.charcoal_grey)),
                        ),
                      ),
                      body: Container(
                        decoration: new BoxDecoration(
                          boxShadow: [
                            new BoxShadow(
                              color: Colors.white,
                              blurRadius: 10.0,spreadRadius: 0.0,offset: new Offset(0.0, 10.0),
                            )
                          ],
                        ),
                        height:  (210/678)*screenHeight,
                        child: Container(
                          height: (235/720)*screenHeight,
                          width: (320/360)*screenWidth,
                          child: Column(
                            children: <Widget>[
                              Container(
                                child: Row(
                                  children: <Widget>[
                                    Container(
                                      child: Center(
                                        child: new Row(
                                          crossAxisAlignment: CrossAxisAlignment.center,
                                          mainAxisAlignment: MainAxisAlignment.center,
                                          children: <Widget>[
                                            new Container(
                                              padding: new EdgeInsets.all(10.0),
                                            ),
                                            Opacity(
                                              opacity: 0.5,
                                              child: new DropdownButton(
                                                icon: Icon(
                                                  Icons.keyboard_arrow_down,
                                                  color: SemanticColors.dusky_blue,
                                                  //size: (11/720)*screenHeight,
                                                ),
                                                value: score,
                                                items: _dropDownMenuItems_score,
                                                onChanged: changedDropDownItem,
                                                style: TextStyle(
                                                    fontSize: 14/360 * screenWidth,
                                                    color: NeutralColors.charcoal_grey,
                                                    fontFamily: "IBMPlexSans",
                                                    fontWeight: FontWeight.normal
                                                ),
                                              ),
                                            )
                                          ],
                                        ),
                                      ),
                                    ),
                                    Container(
                                      child: Center(
                                        child: new Row(
                                          crossAxisAlignment: CrossAxisAlignment.center,
                                          mainAxisAlignment: MainAxisAlignment.center,
                                          children: <Widget>[
                                            new Container(
                                              padding: new EdgeInsets.all(10.0),
                                            ),
                                            Opacity(
                                              opacity: 0.5,
                                              child: new DropdownButton(
                                                icon: Icon(
                                                  Icons.keyboard_arrow_down,
                                                  color: SemanticColors.dusky_blue,
                                                  //size: 11/720 * screenHeight,
                                                ),
                                                value: percentile,
                                                items: _dropDownMenuItems_percentile,
                                                onChanged: changedDropDownItem_percentile,
                                                style: TextStyle(
                                                  fontSize: 14/360 * screenWidth,
                                                  color: NeutralColors.charcoal_grey,
                                                  fontFamily: "IBMPlexSans",
                                                  fontWeight: FontWeight.normal
                                                ),
                                              ),
                                            )
                                          ],
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              Container(
                                height: (45/720)*screenHeight,
                                width: (280/360)*screenWidth,
                                margin: EdgeInsets.only(left: (20/360)*screenWidth,right: (20/360)*screenWidth,top: (20/720)*screenHeight),
                                child: DottedBorder(
                                  padding: EdgeInsets.all(4),
                                  dashPattern: [6],
                                  borderType: BorderType.Rect,
                                  color: NeutralColors.gunmetal.withOpacity(0.3),
                                  child: Container(
                                    child: Row(
                                      children: <Widget>[
                                        Container(
                                          height: (23/720)*screenHeight,
                                          width: (112/360)*screenWidth,
                                          margin: EdgeInsets.only(left: (15/360)*screenWidth,top: (8/720)*screenHeight,bottom: (8/720)*screenHeight),
                                          child: Text(
                                            GDPIHomeString.Upload_Scorecard,
                                            style: TextStyle(
                                              color: PrimaryColors.azure_Dark,
                                              fontSize: 14/360*screenWidth,
                                              fontFamily: "IBMPlexSans",
                                              fontWeight: FontWeight.w500,
                                            ),
                                            textAlign: TextAlign.center,
                                          ),
                                        ),
                                        Container(
                                          margin: EdgeInsets.only(left: (118/360)*screenWidth),
                                          child: InkWell(
                                            onTap: (){},
                                            child: Image.asset(
                                              "assets/images/clip-4.png",
                                              fit: BoxFit.none,
                                              color: Color.fromARGB(255, 0, 171, 251),
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                              Container(
                                child: InkWell(
                                  child:Container(
                                    height: (40/720) * screenHeight,
                                    width: (240/360)*screenWidth,
                                    margin: EdgeInsets.only(left: (40/360)*screenWidth,top: (20/720)*screenHeight,right: (40/360)*screenWidth),
                                    decoration: BoxDecoration(
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(2)
                                        ),
                                        gradient: LinearGradient(
                                            end: Alignment(1.0199999809265137, 1.0099999904632568),
                                            colors: [SemanticColors.light_purpely,SemanticColors.dark_purpely])
                                    ),
                                    child:  Center(
                                      child: Text(
                                        GDPIHomeString.Submit,
                                        style: TextStyle(
                                          color: NeutralColors.pureWhite,
                                          fontSize: 14/360*screenWidth,
                                          fontFamily: "IBMPlexSans",
                                          fontWeight: FontWeight.w500,
                                        ),
                                        textAlign: TextAlign.center,
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      )
                  )
                ],
              ),
            ),
          )
      );
    }
    return new Column(children: list_gdpi);
  }
}


import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/material.dart';
import 'package:flutter_document_picker/flutter_document_picker.dart';
import 'package:flutter_svg/svg.dart';
import 'package:imsindia/components/custom_DropDown_GDPI.dart';

import 'package:imsindia/routers/routes.dart';
import 'package:imsindia/utils/screen_util.dart';
import 'package:imsindia/utils/svg_icons.dart';
import 'package:imsindia/resources/strings/GDPI_labels.dart';
import 'package:imsindia/utils/colors.dart';
import 'package:imsindia/views/Arun/search/ServerSearch.dart' as prefix0;
import 'package:imsindia/views/GDPI/GDPI_PrepareForPI_ScoreBoard.dart';
import 'package:imsindia/views/GDPI/GDPI_home_screen.dart';
import 'package:imsindia/components/primary_Button.dart';
import 'package:imsindia/components/custom_expansion_panel.dart';
import 'GDPI_PrepareForPI_ScoreBoard_Screen.dart';
import 'package:imsindia/utils/svg_images/prepare_svg_images.dart';
import 'package:path/path.dart' as path;




List GDPI_TITLE = [
  'CAT',
  'IIFT',
  'XAT',
  'NMAT',
  'SNAP',
  'CAT',
  'NMAT',
  ''
];
List<dynamic> pathNames=[];
List<TextEditingController> overallScore = <TextEditingController>[];
List<TextEditingController> overallPercentile = <TextEditingController>[];


//List<String> dropDownMonths = [];
class ScoreService {
  static final List<String> score = ["Overall Score", "Bucuresti", "Timisoara", "Brasov", "Constanta"];

  static List<String> getSuggestions(String query) {
    List<String> matches = List();
    matches.addAll(score);

    // matches.retainWhere((s) => s.toLowerCase().contains(query.toLowerCase()));
    return matches;
  }
}
class PercentileService {
  static final List<String> percentile =
  ["Overall Percentile", "Bucuresti", "Timisoara", "Brasov", "Constanta"];

  static List<String> getSuggestions(String query) {
    List<String> matches = List();
    matches.addAll(percentile);

    // matches.retainWhere((s) => s.toLowerCase().contains(query.toLowerCase()));
    return matches;
  }
}



class GDPIPrepareForPI extends StatefulWidget {
  @override
  GDPIPrepareForPIState createState() => GDPIPrepareForPIState();
}

class GDPIPrepareForPIState extends State<GDPIPrepareForPI>{
  List<dynamic> document = [];
  List<dynamic> score = [];
  int _activeMeterIndex;
  bool _pickFileInProgress = false;
  String _path = '-';
  bool _iosPublicDataUTI = true;
  bool _checkByCustomExtension = false;
  bool _checkByMimeType = false;
  bool uploaded = false;
  final TextEditingController _score = new TextEditingController();
  final TextEditingController _percentile = new TextEditingController();
  _pickDocument(int value) async {
    String result;
    try {
      setState(() {
        _path = '-';
        _pickFileInProgress = true;
      });
      final _utiController = TextEditingController(
        text: 'com.sidlatau.example.mwfbak',
      );

      final _extensionController = TextEditingController(
        text: 'mwfbak',
      );

      final _mimeTypeController = TextEditingController(
        text: 'application/pdf image/png',
      );
      FlutterDocumentPickerParams params = FlutterDocumentPickerParams(
        allowedFileExtensions: _checkByCustomExtension
            ? _extensionController.text
            .split(' ')
            .where((x) => x.isNotEmpty)
         
            .toList()
            : null,
        allowedUtiTypes: _iosPublicDataUTI
            ? null
            : _utiController.text
            .split(' ')
            .where((x) => x.isNotEmpty)
            .toList(),
        allowedMimeTypes: _checkByMimeType
            ? _mimeTypeController.text
            .split(' ')
            .where((x) => x.isNotEmpty)
            .toList()
            : null,
      );

      result = await FlutterDocumentPicker.openDocument(params: params);
    } catch (e) {
      print(e);
      result = 'Error: $e';
    } finally {
      setState(() {
        _pickFileInProgress = false;
      });
    }

    setState(() {
      uploaded = true;
      _path = result;     
      pathNames.insert(value,_path);
      print('value'+pathNames[value].toString());    

    });
  }
  void _showDialog() {
    // flutter defined function
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          title: new Text("Alert Dialog title"),
          content: new Text("Alert Dialog body"),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            new FlatButton(
              child: new Text("Close"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }



  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    for(int k=0;k<GDPI_TITLE.length;k++){
        pathNames.add('');
        overallScore.add(new TextEditingController());
        overallPercentile.add(new TextEditingController());
      }
  }



  @override
  Future<bool> _onWillPop() {
    //back
    AppRoutes.push(context, GDPIHomescreen());
  }
  Widget build(BuildContext context) {
    // TODO: implement build
    final screenHeight = MediaQuery.of(context).size.height;//720dp
    final screenWidth = MediaQuery.of(context).size.width;//360dp
    return  Scaffold(
      body:  Container(
        margin: EdgeInsets.only(
          top: 0 / Constant.defaultScreenHeight * screenHeight,
        ),
        child: Column(
          children: <Widget>[
            Container(
              margin: EdgeInsets.only(
                top: (42 / 720) * screenHeight,
              ),
              child: Row(
                //crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  InkWell(
                    onTap: () {
                      Navigator.pop(context);
                    },
                    child: Container(
                      width: (58 / 360) * screenWidth,
                      height: (30.0 / 720) * screenHeight,
                      margin: EdgeInsets.only(left: 0.0),
                      child: SvgPicture.asset(
                        getPrepareSvgImages.backIcon,
                        height: (5 / 720) * screenHeight,
                        width: (14 / 360) * screenWidth,
                        fit: BoxFit.none,
                      ),
                    ),
                  ),
                  Container(
                    height: (20 / 720) * screenHeight,
                    // margin: EdgeInsets.only(left: (20 / 360) * screenWidth),
                    child: Text(
                      "GD-PI",
                      style: TextStyle(
                        color: NeutralColors.black,
                        fontSize: (16 / 720) * screenHeight,
                        fontFamily: "IBMPlexSans",
                        fontWeight: FontWeight.w500,
                      ),
                      textAlign: TextAlign.left,
                    ),
                  ),
                ],
              ),
            ),
            Align(
              alignment: Alignment.topLeft,
              child: Container(
                //height: (27/720)*screenHeight,
                //width: (120/360)*screenWidth,
                margin: EdgeInsets.only(top: (15/720)*screenHeight,left: (20/360)*screenWidth),
                child: Text(
                  GDPIHomeString.First_list,
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle (
                    color: NeutralColors.preparePi_balck,
                    fontWeight: FontWeight.bold,
                    fontFamily: "IBMPlexSans",
                    fontStyle: FontStyle.normal,
                    fontSize: (18/360)*screenWidth,
                  ),
                  textAlign: TextAlign.start,
                ),
              ),
            ),
            Align(
              alignment: Alignment.topLeft,
              child: Container(
                //height: (23/720)*screenHeight,
                //width: (225/360)*screenWidth,
                margin: EdgeInsets.only(top: (5/720)*screenHeight,left: (20/360)*screenWidth),
                child: Text(
                  GDPIHomeString.Sub_header,
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle (
                    color: NeutralColors.gunmetal,
                    fontWeight: FontWeight.w400,
                    fontFamily: "IBMPlexSans",
                    fontStyle: FontStyle.normal,
                    fontSize: (14/360)*screenWidth,
                  ),
                  textAlign: TextAlign.start,
                ),
              ),
            ),
            Expanded(
              child: Container(
                margin: EdgeInsets.only(left: 20/360 * screenWidth, right: 20/360 * screenWidth, top: 20/720 * screenHeight),
                child: ListView.builder(
                  shrinkWrap: true,
                  itemCount: GDPI_TITLE.length,
                  padding: EdgeInsets.only(bottom: (40)),
                  itemBuilder: (context, int i) {
                    return GDPI_TITLE.indexOf(GDPI_TITLE[i]) == GDPI_TITLE.length -1?
                    Container(
                      child: Center(
                        child: InkWell(
                          onTap: (){
                            AppRoutes.pushWithAnmation(context, GDPIPrepareForPIScoreBoard());
                          },
                          child: Container(
                            height: (40/720) * screenHeight,
                            width: (270/360)*screenWidth,
                            margin: EdgeInsets.only(top: (40/720)*screenHeight,bottom: (97/720)*screenHeight),
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.all(
                                    Radius.circular(2)
                                ),
                                gradient: LinearGradient(
                                    end: Alignment(1.0199999809265137, 1.0099999904632568),
                                    colors: [SemanticColors.light_purpely,SemanticColors.dark_purpely])
                            ),
                            child: Center(
                              child: Text(
                                GDPIHomeString.Submit,
                                style: TextStyle(
                                  color: NeutralColors.pureWhite,
                                  fontSize: 14/720*screenHeight,
                                  fontFamily: "IBMPlexSans",
                                  fontWeight: FontWeight.w500,
                                ),
                                textAlign: TextAlign.center,
                              ),
                            ),
                          ),
                        ),
                      ),
                    ):
                      Container(
                        child:Container(
                          margin: EdgeInsets.only(top: (10/720)*screenHeight),
                          child: CustomExpansionPanelList(
                            expansionHeaderHeight: (45/720)*screenHeight,
                            iconColor: (_activeMeterIndex == i)?Colors.white:Colors.black,
                            backgroundColor1: (_activeMeterIndex == i)?NeutralColors.purpleish_blue:SemanticColors.light_purpely.withOpacity(0.1),
                            backgroundColor2:(_activeMeterIndex == i)? NeutralColors.purpleish_blue:SemanticColors.dark_purpely.withOpacity(0.1),
                            expansionCallback: (int index, bool status) {
                              setState(() {
                                _activeMeterIndex = _activeMeterIndex == i ? null : i;
                              });
                            },
                            children: [
                              new ExpansionPanel(
                                  canTapOnHeader: true,
                                  isExpanded: _activeMeterIndex == i,
                                  headerBuilder: (BuildContext context, bool isExpanded)=>
                                  new Container(
                                    alignment: Alignment.centerLeft,
                                    child: new Padding(
                                      padding: EdgeInsets.only(left: (15/360)*screenWidth),
                                      child: new Text(
                                          GDPI_TITLE[i],
                                          style: new TextStyle(
                                              fontSize: (14/720)*screenHeight,
                                              fontFamily: "IBMPlexSans",
                                              fontWeight: FontWeight.w500,
                                              color: (_activeMeterIndex == i) ?NeutralColors.pureWhite:NeutralColors.charcoal_grey)),
                                    ),
                                  ),
                                  body: Container(
                                    decoration: new BoxDecoration(
                                      boxShadow: [
                                        new BoxShadow(
                                          color: Colors.white,
                                          blurRadius: 10.0,spreadRadius: 0.0,offset: new Offset(0.0, 10.0),
                                        )
                                      ],
                                    ),
                                    height:  (210/678)*screenHeight,
                                    child: Container(
                                      height: (235/720)*screenHeight,
                                      width: (320/360)*screenWidth,
                                      child: Column(
                                        children: <Widget>[
                                          Container(
                                            child: Row(
                                              children: <Widget>[
                                                Container(
                                                  child: Center(
                                                    child: new Row(
                                                      crossAxisAlignment: CrossAxisAlignment.center,
                                                      mainAxisAlignment: MainAxisAlignment.center,
                                                      children: <Widget>[
                                                        Container(
                                                          width: 130/360 * screenWidth,
                                                          margin: EdgeInsets.only(left: 20/360 * screenWidth),
                                                          child: DropDownFormField(
                                                              getImmediateSuggestions: true,
                                                              textFieldConfiguration: TextFieldConfiguration(
                                                                label: (overallScore[i].text.isEmpty)?"Overall Score":'empty',
                                                                controller: overallScore[i],
                                                              ),
                                                              suggestionsCallback: (pattern) {
                                                                return ScoreService.getSuggestions(pattern);
                                                              },
                                                              suggestionsBoxDecoration: SuggestionsBoxDecoration(
                                                                  dropDownHeight: 150,
                                                                  borderRadius: new BorderRadius.circular(5.0),
                                                                  color: Colors.white),
                                                              itemBuilder: (context, suggestion) {
                                                                return ListTile(
                                                                  dense: true,
                                                                  contentPadding: EdgeInsets.only(left: 10/360 * screenWidthTotal),
                                                                  title: Text(
                                                                    suggestion,
                                                                    style: TextStyle(
                                                                      fontFamily: "IBMPlexSans",
                                                                      fontWeight: FontWeight.w400,
                                                                      fontSize: (14 / 720) * screenHeight,
                                                                      color: NeutralColors.bluey_grey,
                                                                      textBaseline: TextBaseline.alphabetic,
                                                                      letterSpacing: 0.0,
                                                                      inherit: false,

                                                                    ),
                                                                  ),
                                                                );
                                                              },
                                                              hideOnLoading: true,
                                                              hideOnEmpty: true,
                                                              transitionBuilder: (context, suggestionsBox, controller) {
                                                                return suggestionsBox;
                                                              },
                                                              onSuggestionSelected: (suggestion) {
                                                                overallScore[i].text = suggestion;
                                                                setState(() {
                                                                  //checkTenthDetails();
                                                                });

                                                                // this._typeAheadController.dispose();
                                                              },
                                                            ),
                                                        ),
                                                      ],
                                                    ),
                                                  ),
                                                ),
                                                Container(
                                                  child: Center(
                                                    child: new Row(
                                                      crossAxisAlignment: CrossAxisAlignment.center,
                                                      mainAxisAlignment: MainAxisAlignment.center,
                                                      children: <Widget>[
                                                        Container(
                                                          width: 132/360 * screenWidth,
                                                          margin: EdgeInsets.only(left: 18/360 * screenWidth),
                                                          child: DropDownFormField(
                                                            getImmediateSuggestions: true,
                                                            textFieldConfiguration: TextFieldConfiguration(
                                                              label: "Overall Percentile",
                                                              controller: overallPercentile[i],
                                                            ),
                                                            suggestionsCallback: (pattern) {
                                                              return PercentileService.getSuggestions(pattern);
                                                            },
                                                            hideOnLoading: true,
                                                            suggestionsBoxDecoration: SuggestionsBoxDecoration(
                                                                dropDownHeight: 150,
                                                                borderRadius: new BorderRadius.circular(5.0),
                                                                color: Colors.white),
                                                            itemBuilder: (context, suggestion) {
                                                              return ListTile(
                                                                dense: true,
                                                                contentPadding: EdgeInsets.only(left: 10/360 * screenWidthTotal),
                                                                title: Text(
                                                                  suggestion,
                                                                  style: TextStyle(
                                                                    fontFamily: "IBMPlexSans",
                                                                    fontWeight: FontWeight.w400,
                                                                    fontSize: (14 / 720) * screenHeight,
                                                                    color: NeutralColors.bluey_grey,
                                                                    textBaseline: TextBaseline.alphabetic,
                                                                    letterSpacing: 0.0,
                                                                    inherit: false,

                                                                  ),
                                                                ),
                                                              );
                                                            },
                                                            transitionBuilder: (context, suggestionsBox, controller) {
                                                              return suggestionsBox;
                                                            },
                                                            onSuggestionSelected: (suggestion) {
                                                              overallPercentile[i].text = suggestion;
                                                              setState(() {
                                                                //checkTenthDetails();
                                                              });

                                                              // this._typeAheadController.dispose();
                                                            },
                                                          ),
                                                        ),
                                                      ],
                                                    ),
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                          Container(
                                            height: (45/720)*screenHeight,
                                            width: (280/360)*screenWidth,
                                            margin: EdgeInsets.only(left: (20/360)*screenWidth,right: (20/360)*screenWidth,top: (20/720)*screenHeight),
                                            child: DottedBorder(
                                              padding: EdgeInsets.all(4),
                                              dashPattern: [6],
                                              borderType: BorderType.Rect,
                                              color: NeutralColors.gunmetal.withOpacity(0.3),
                                              child: Container(
                                                child: Row( 
                                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                  children: <Widget>[
                                                    Container(
                                                      //color: Colors.red,
                                                      //height: (23/720)*screenHeight,
                                                      width: (200/360)*screenWidth,
                                                      margin: EdgeInsets.only(left: (15/360)*screenWidth,top: (8/720)*screenHeight,bottom: (8/720)*screenHeight),
                                                      child: (pathNames[i]!=''&& pathNames[i]!=null)
                                                          ? Container(
                                                           
                                                            child: 
                                                            Text(path.basename(pathNames[i]),overflow: pathNames[i].length>20?TextOverflow.ellipsis:TextOverflow.visible,))
                                                          : Text(
                                                              GDPIHomeString.Upload_Scorecard,
                                                              style: TextStyle(
                                                                color: PrimaryColors.azure_Dark,
                                                                fontSize: 14/720*screenHeight,
                                                                fontFamily: "IBMPlexSans",
                                                                fontWeight: FontWeight.w500,
                                                              ),
                                                              textAlign: TextAlign.left,
                                                            ),
                                                    ),
                                                    Container(
                                                      margin: EdgeInsets.only(left: 10/360 * screenWidth,right: (15/360)*screenWidth),
                                                      width: 24/360 * screenWidth,
                                                      child: InkWell(
                                                        onTap: (){
                                                          setState(() {
                                                            if(document.contains(i)){
                                                              document.remove(i);
                                                            }
                                                            else{
                                                              document.add(i);
                                                            }
                                                            //bookmarkSelected = !bookmarkSelected;
                                                          });
                                                          _pickFileInProgress ? null : _pickDocument(i);
                                                        },
                                                        child: Image.asset(
                                                          "assets/images/clip-4.png",
                                                          fit: BoxFit.none,
                                                          color: Color.fromARGB(255, 0, 171, 251),
                                                        ),
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                            ),
                                          ),
                                          Container(
                                            child: InkWell(
                                              child:Container(
                                                height: (40/720) * screenHeight,
                                                width: (240/360)*screenWidth,
                                                margin: EdgeInsets.only(left: (40/360)*screenWidth,top: (20/720)*screenHeight,right: (40/360)*screenWidth),
                                                decoration: BoxDecoration(
                                                    borderRadius: BorderRadius.all(
                                                        Radius.circular(2)
                                                    ),
                                                    gradient: LinearGradient(
                                                        end: Alignment(1.0199999809265137, 1.0099999904632568),
                                                        colors: [SemanticColors.light_purpely,SemanticColors.dark_purpely])
                                                ),
                                                child:  Center(
                                                  child: Text(
                                                    GDPIHomeString.Submit,
                                                    style: TextStyle(
                                                      color: NeutralColors.pureWhite,
                                                      fontSize: 14/720*screenHeight,
                                                      fontFamily: "IBMPlexSans",
                                                      fontWeight: FontWeight.w500,
                                                    ),
                                                    textAlign: TextAlign.center,
                                                  ),
                                                ),
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  )
                              )
                            ],
                          ),
                        ),
                      );
                  },
                ),
              ),
            ),
          ],
        ),
      ),
    );

  }


}


import 'package:connectivity/connectivity.dart';
import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:imsindia/api/bloc/services.dart';
import 'package:imsindia/components/bottom_navigation.dart';
import 'package:imsindia/components/custom_expansion_panel.dart';
import 'package:imsindia/components/navigation_bar.dart';
import 'package:imsindia/utils/Url.dart';
import 'package:imsindia/utils/colors.dart';
import 'package:imsindia/utils/global.dart' as global;
import 'package:shared_preferences/shared_preferences.dart';
import '../../routers/routes.dart';
import 'GDPI_Lists_VideoPdf_Screen.dart';

var screenWidthTotal;
var screenHeightTotal;
var _safeAreaHorizontal;
var _safeAreaVertical;
var screenHeight;
var screenWidth;
var courseID;
var menuID;
var title = [];
var referenceType = [];
var vimeoLink = [];
var vimeoURI = [];
var video_id = [];
var test_id = [];
var isProductAccess = [] ;

bool _loaded = false;
var img = NetworkImage("http://d1cn01kw45hc6h.cloudfront.net/wp-content/uploads/2019/08/22161210/Quadtratic.png");
class Gdpi_HomeScreen extends StatefulWidget {
  @override
  Gdpi_HomeScreenState createState() => Gdpi_HomeScreenState();
}

class Gdpi_HomeScreenState extends State<Gdpi_HomeScreen> {
  int _activeMeterIndex;
  var FirstTitle = [];
  var FirstTitleID = [];
  var SecondTitle = [];
  var SecondTitleID = [];
  var finalListForSecondTitles = [];
  var finalListForSecondTitleIds = [];
  var authorizationInvalidMsg;
  var authToken;

  Connectivity connectivity = Connectivity();
  final GlobalKey<ScaffoldState> _scaffoldstateemax = new GlobalKey<ScaffoldState>();
  var refreshKeyGdpi = GlobalKey<RefreshIndicatorState>();

  void getFirstAndSecondTitles(Map t) async {
    print("inside api");
    print(t);

    var connectivityResult = await (Connectivity().checkConnectivity());
    if (connectivityResult.toString() != 'ConnectivityResult.none') {
      ApiService().getAPI(
          URL.GET_FIRST_SECOND_TITLE_GDPI+ courseID + "&id=" + menuID, t)
          .then((returnValue) {
        setState(() {
          print("after validation");
          print(courseID);
          print(menuID);
          print(returnValue);
          if (returnValue[0].toString().toLowerCase() == 'Data fetched successfully'.toLowerCase()) {
            print("API data");
            //       print(menuID);
            var totalData = returnValue[1]['data'];
            for (var index = 0; index <
                returnValue[1]['data'].length; index++) {
              if (totalData[index]['parentId'] == menuID) {
                if(!FirstTitle.contains(totalData[index]['component']['title'])){
                  FirstTitle.add(totalData[index]['component']['title']);
                }
                if(!FirstTitleID.contains(totalData[index]['id'])){
                  FirstTitleID.add(totalData[index]['id']);
                }
              }
            }
            for (var i = 0; i < FirstTitleID.length; i++) {
              for (var j = 0; j < totalData.length; j++) {
                if (totalData[j]['parentId'] == FirstTitleID[i]) {
                  if(!SecondTitle.contains(totalData[j]['component']['title'])){
                    SecondTitle.add(totalData[j]['component']['title']);
                  }
                  if(!SecondTitleID.contains(totalData[j]['id'])){
                    SecondTitleID.add(totalData[j]['id']);
                  }
                }
              }
              finalListForSecondTitles.add(SecondTitle);
              finalListForSecondTitleIds.add(SecondTitleID);
              SecondTitleID = [];
              SecondTitle = [];

            }
          } else {
            authorizationInvalidMsg = returnValue[0];
          }
          _loaded = true;
        });
      });
    }else{
      showErrorMessage('No Internet Connection');
    }

  }

  void getCourseId() async {
    print("inside getcourseid");
    SharedPreferences prefs = await SharedPreferences.getInstance();
    courseID = prefs.getString("courseId");
    menuID = prefs.getString("moduleId");
    global.getToken.then((t){
      authToken = t;
      getFirstAndSecondTitles(t);
    });
  }
  void setIdForSubjectLevelTitles(value) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString("idForSubjectLevelTitlesGdpi", value);
  }

  void setTitleForSubjectLevelTitles(value) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString("titleForSubjectLevelTitlesPGdpi", value);
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    checkInternet();
    setState((){
      _loaded = false;
    });
    print('init state called');
    connectivity.onConnectivityChanged.listen((result) {
      if (result.toString() == 'ConnectivityResult.none') {
        print('connection lost');
        noInternetMessage('Internet Connection Lost');
      } else {
        print(_scaffoldstateemax.currentState) ;
        print("_scaffoldstate.currentState");
        _scaffoldstateemax.currentState.removeCurrentSnackBar();
        print('connection exist in notifier');
         FirstTitle = [];
         FirstTitleID = [];
         SecondTitle = [];
         SecondTitleID = [];
         finalListForSecondTitles = [];
         finalListForSecondTitleIds = [];
        getCourseId();
      }
    });
    // img.resolve(ImageConfiguration()).addListener(ImageStreamListener((ImageInfo image, bool synchronousCall) {
    //   if (mounted) {
    //     setState(() => _loaded = true);
    //   }
    // }));
  }
  // on dragging page refresh function will be activated
  Future<Null> refreshList() async {
    refreshKeyGdpi.currentState?.show(atTop: false);
    await Future.delayed(Duration(seconds: 2));
    setState(() {
      print('Clicked on refresh');
      //Again calling api while refreshing
       FirstTitle = [];
       FirstTitleID = [];
       SecondTitle = [];
       SecondTitleID = [];
       finalListForSecondTitles = [];
       finalListForSecondTitleIds = [];
      getCourseId();
    });

    return null;
  }
  Future<Null> checkInternet() async {
    var connectivityResult = await (Connectivity().checkConnectivity());
    if (connectivityResult.toString() != 'ConnectivityResult.none') {
      setState(() {
        print('Clicked on intenet');
         FirstTitle = [];
         FirstTitleID = [];
         SecondTitle = [];
         SecondTitleID = [];
         finalListForSecondTitles = [];
         finalListForSecondTitleIds = [];
        //Again calling api while refreshing
        getCourseId();
      });
    } else {
      noInternetMessage('Internet Connection Lost');
    }

    return null;
  }
  void noInternetMessage(String value) {
    _scaffoldstateemax.currentState.showSnackBar(new SnackBar(
      content: new Text(value),
      action: SnackBarAction(
        textColor: Colors.blueAccent,
        label: "Dismiss",
        onPressed: () {
          // Some code to undo the change.
        },
      ),
      backgroundColor: Colors.blueGrey,
      duration: Duration(days: 365),
    ));
  }
  void showErrorMessage(String value) {
    _scaffoldstateemax.currentState.showSnackBar(new SnackBar(
      content: new Text(value),
      duration: new Duration(seconds: 3),
    ));
  }
  @override
  void didChangeDependencies() {
    // TODO: implement initState
    // super.initState();
    //getCourseId();
  }
  @override
  Widget build(BuildContext context) {


    var _mediaQueryData = MediaQuery.of(context);
    screenWidthTotal = _mediaQueryData.size.width;
    screenHeightTotal = _mediaQueryData.size.height;
    _safeAreaHorizontal =
        _mediaQueryData.padding.left + _mediaQueryData.padding.right;
    _safeAreaVertical =
        _mediaQueryData.padding.top + _mediaQueryData.padding.bottom;

    screenHeight = screenHeightTotal - _safeAreaVertical;
    screenWidth = screenWidthTotal - _safeAreaHorizontal;


    return Scaffold(
        key: _scaffoldstateemax,
        appBar: AppBar(
          brightness: Brightness.light,
          centerTitle: false,
          titleSpacing: 0.0,
          backgroundColor: NeutralColors.pureWhite,
          elevation: 0.0,
          iconTheme: IconThemeData(
            color: NeutralColors.black, //change your color here
          ),
          title: Container(
            width: (302 / 360) * screenWidth,
            child: Text(
              "GD-PI",
              style: TextStyle(
                color: NeutralColors.black,
                fontSize: (16 / 720) * screenHeight,
                fontFamily: "IBMPlexSans",
                fontWeight: FontWeight.w500,
              ),
              textAlign: TextAlign.left,
            ),
          ),
        ),
        drawer: NavigationDrawer(),
//        bottomNavigationBar: BottomNavigation(),
        body: Container(
          color: Colors.white,
          child: Column(
            children: <Widget>[
              authorizationInvalidMsg != null
                  ? new Container(
                  margin: EdgeInsets.only(
                      top: (200.0 / 720) * screenHeight),
                  child: Center(
                      child: Text(
                          '${authorizationInvalidMsg} ')))
                  : FirstTitle.length == 0 && _loaded == true
                  ? new Container(
                  margin: EdgeInsets.only(top: (200.0 / 720) * screenHeight),
                  child: Center(
                    child: Text("No data", 
                        style: TextStyle(
                          color: NeutralColors.black,
                          fontSize: (16 / 720) * screenHeight,
                          fontFamily: "IBMPlexSans",
                          fontWeight: FontWeight.w500,
                        ),
                      ),
                    )
                  )
                  : _loaded == false
                  ? new Container(
                  margin: EdgeInsets.only(
                      top: (200.0 / 720) * screenHeight),
                  child:
                  Center(child: CircularProgressIndicator()))
                  : new Expanded(
                child:RefreshIndicator(
                    displacement: 100/720*screenHeight,
                    color: SemanticColors.blueGrey,
                    backgroundColor: SemanticColors.iceBlue,
                    key: refreshKeyGdpi,
                    child: new ListView.builder(
                      itemBuilder: (BuildContext context, int i) {
                        return Container(
                          decoration: BoxDecoration(
                            boxShadow: [
                              BoxShadow(
                                offset: Offset(0, 5),
                                blurRadius: 10,
                                color: (_activeMeterIndex == i)
                                    ? Color(0xffeaeaea)
                                    : Colors.transparent,
                                //offset: Offset.lerp(Offset(10.0,-10.0), Offset(10.0,10.0), 1),
                                // offset: Offset(0,10.0),
                                //color: Colors.orange,
                              ),
                            ],
                            borderRadius: new BorderRadius.only(
                                topLeft: new Radius.circular(5),
                                topRight: new Radius.circular(5),
                                bottomLeft: new Radius.circular(5),
                                bottomRight:
                                new Radius.circular(5)),
                            border: Border.all(
                                color: Colors.transparent),
                          ),
                          margin: EdgeInsets.only(
                              top: (10.0 / 720) * screenHeight,
                              left: (20.0 / 360) * screenWidth,
                              right: (20.0 / 360) * screenWidth,
                              bottom: (i==FirstTitle.length-1)?(40 / 720) * screenHeight:0),
                          child: (finalListForSecondTitles[i].length == 0 && finalListForSecondTitleIds[i].length == 0)
                              ? InkWell(
                              onTap: (){
                                global.getToken.then((t){
                                  authToken=t;
                                  print("888888888888888888888888888888888888888888");
                                  print(FirstTitleID[i]);
                                  getSecondLevelRHSData(t,FirstTitleID[i],FirstTitle[i]);

                                });
                              },
                              child: Container(
                                alignment: Alignment.centerLeft,
                                height: (45 / 720) * screenHeight,
                                decoration: BoxDecoration(
                                  gradient: LinearGradient(
                                      colors:
                                      [SemanticColors.light_purpely.withOpacity(0.1),
                                        NeutralColors.purpleish_blue.withOpacity(0.1),
                                      ]

                                  ),
                                  borderRadius: new BorderRadius.circular(5.0),

                                ),
                                child: new Padding(
                                  padding: EdgeInsets.only(
                                      left: (15 / 320) *
                                          screenWidth),
                                  child: new Text(
                                      FirstTitle[i],
                                      style: new TextStyle(
                                          fontSize: (13.6 / 720) *
                                              screenHeight,
                                          fontFamily:
                                          "IBMPlexSans",
                                          fontWeight:
                                          FontWeight.w500,
                                          color: (_activeMeterIndex == i)
                                              ? NeutralColors
                                              .pureWhite
                                              : NeutralColors
                                              .charcoal_grey)),
                                ),
                              )
                          )
                              :CustomExpansionPanelList(
                            expansionHeaderHeight:
                            (45 / 720) * screenHeight,
                            iconColor: (_activeMeterIndex == i)
                                ? Colors.white
                                : Colors.black,
                            backgroundColor1:
                            (_activeMeterIndex == i)
                                ? NeutralColors.purpleish_blue
                                : SemanticColors.light_purpely
                                .withOpacity(0.1),
                            backgroundColor2:
                            (_activeMeterIndex == i)
                                ? NeutralColors.purpleish_blue
                                : SemanticColors.dark_purpely
                                .withOpacity(0.1),
                            expansionCallback:
                                (int index, bool status) {
                              setState(() {
                                _activeMeterIndex =
                                _activeMeterIndex == i
                                    ? null
                                    : i;
                              });
                            },
                            children: [
                              new ExpansionPanel(
                                canTapOnHeader: true,
                                isExpanded: _activeMeterIndex == i,
                                headerBuilder:
                                    (BuildContext context,
                                    bool isExpanded) =>
                                new Container(
                                  alignment: Alignment.centerLeft,
                                  child: new Padding(
                                    padding: EdgeInsets.only(
                                        left: (15 / 320) *
                                            screenWidth),
                                    child: new Text(
                                        FirstTitle[
                                        i],
                                        style: new TextStyle(
                                            fontSize: (13.6 / 720) *
                                                screenHeight,
                                            fontFamily:
                                            "IBMPlexSans",
                                            fontWeight:
                                            FontWeight.w500,
                                            color: (_activeMeterIndex ==
                                                i)
                                                ? NeutralColors
                                                .pureWhite
                                                : NeutralColors
                                                .charcoal_grey)),
                                  ),
                                ),
                                body: new Container(
                                  color: Colors.white,
                                  // height: (210 / 678) * screenHeight,
                                  child: new ListView.builder(
                                    shrinkWrap: true,
                                    addAutomaticKeepAlives: true,
                                    // addSemanticIndexes: false,
                                    padding: EdgeInsets.only(
                                        bottom: 10.0 / 720 * screenHeight),
                                    itemBuilder:
                                        (BuildContext context,
                                        int index) {
                                      return new ListTile(
                                        dense: true,
                                        contentPadding:
                                        EdgeInsets.only(
                                            bottom: 0,
                                            left: (15 / 320) *
                                                screenWidth,
                                            top: 0),
                                        onTap: () {
                                          setIdForSubjectLevelTitles( finalListForSecondTitleIds[i][index]);
                                          setTitleForSubjectLevelTitles(finalListForSecondTitles[i][index]);
                                          global.titleForPrepareSubjectsScreen=finalListForSecondTitles[i][index];
                                          print("-------------------------id of clicked items");
                                          print(finalListForSecondTitleIds[i][index]);
                                          print(authToken);
                                          global.getToken.then((t){
                                            authToken=t;
                                            getSecondLevelRHSData(t,finalListForSecondTitleIds[i][index],finalListForSecondTitles[i][index]);
                                          });
                                        },
                                        title: new Text(
                                          finalListForSecondTitles[
                                          i][index],
                                          textAlign: TextAlign.left,
                                          style: new TextStyle(
                                              fontSize: (15 / 720) *
                                                  screenHeight,
                                              fontFamily:
                                              "IBMPlexSans",
                                              fontWeight:
                                              FontWeight.w500,
                                              color: NeutralColors
                                                  .blue_grey),
                                        ),
                                      );
                                    },
                                    itemCount:
                                    finalListForSecondTitles[i]==0?0:finalListForSecondTitles[i].length,
                                  ),
                                ),
                              ),
                            ],
                          ),
                        );
                      },
                      itemCount: FirstTitle == null
                          ? 0
                          : FirstTitle.length,
                    ),
                    onRefresh: refreshList
                ),
              ),
            ],
          ),
        ));
  }
  void getSecondLevelRHSData(Map t,id,secondTitle) async {
    print("Authorization url");
    print(URL.GDPI_RHS_DATA + id);
    if (global.headersWithAuthorizationKey['Authorization'] != '') {
      ApiService().getAPI(URL.GDPI_RHS_DATA + id, t)
          .then((returnValue) {
        print(URL.GDPI_RHS_DATA );
        print("ID for api "+id.toString());
        setState(() {
          print("+++++++++++++++++++++++++++++data video");
          print(returnValue);
          title = [];
          referenceType = [];
          vimeoURI = [];
          video_id = [];
          vimeoLink = [];
          test_id = [];
          isProductAccess=[];
          if (returnValue[0].toString().toLowerCase() == 'Fetched Successfully'.toLowerCase()) {
            var totalData_GDPI=returnValue[1]['data'];
            print("Total Data"+totalData_GDPI.toString());
            for (var index = 0; index < totalData_GDPI.length; index++) {
              print("========================testis is_________________");
              print(totalData_GDPI[index]['testId']);
              test_id.add(totalData_GDPI[index]['testId']);
              if(totalData_GDPI[index].containsKey('component')) {
                title.add(totalData_GDPI[index]['component']['title']);
                vimeoLink.add(totalData_GDPI[index]['component']['vimeoLink']);
                vimeoURI.add(totalData_GDPI[index]['component']['vimeoURI']);
                video_id.add(totalData_GDPI[index]['component']['id']);
                referenceType.add(totalData_GDPI[index]['component']['referenceType']);
                isProductAccess.add(totalData_GDPI[index]['isProductAccess']);
              }else{

              }
            }
            print("*************************************data");
            print(referenceType);
            print(vimeoURI);
            AppRoutes.push(context, GDPI_Lists_VideoPdf(secondTitle,title,referenceType,vimeoURI,video_id,test_id,isProductAccess));
          } else {
            authorizationInvalidMsg = returnValue[0];
          }
          print("Authorization Message"+authorizationInvalidMsg.toString());
          print(referenceType);
          print("hiiiiiiiiiiiiiiiiiii11111111111111111111111111111111");
          print(isProductAccess);
        });
      });
    }
  }
}


class CustomBoxShadow extends BoxShadow {
  final BlurStyle blurStyle;

  const CustomBoxShadow({
    Color color = const Color(0xFF000000),
    Offset offset = Offset.zero,
    double blurRadius = 0.0,
    this.blurStyle = BlurStyle.normal,
  }) : super(color: color, offset: offset, blurRadius: blurRadius);

  @override
  Paint toPaint() {
    final Paint result = Paint()
      ..color = color
      ..maskFilter = MaskFilter.blur(this.blurStyle, blurSigma);
    assert(() {
      if (debugDisableShadows) result.maskFilter = null;
      return true;
    }());
    return result;
  }
}

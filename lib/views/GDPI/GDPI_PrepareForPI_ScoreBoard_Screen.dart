import 'package:flutter/material.dart';
import 'package:imsindia/routers/routes.dart';
import 'package:imsindia/views/GDPI/GDPI_Lists_B_School.dart';
import 'package:imsindia/views/GDPI/GDPI_PrepareForPI_Screen.dart';
import 'package:imsindia/utils/svg_icons.dart';
import 'package:imsindia/resources/strings/GDPI_labels.dart';
import 'package:imsindia/utils/colors.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:imsindia/utils/svg_images/GDPI_svg_images.dart';

List GDPI_TITLE_Scoreboard=['PI Masterclass','SOP Masterclass','PI Workbook E-book','50 Important Concepts & Definitions'];
class GDPIPrepareForPIScoreBoardScreen extends StatefulWidget {
  GDPIPrepareForPIScoreBoardScreen({Key key}) : super(key: key);
  @override
  GDPIPrepareForPIScoreBoardScreenState createState() => GDPIPrepareForPIScoreBoardScreenState();
}

class GDPIPrepareForPIScoreBoardScreenState extends State<GDPIPrepareForPIScoreBoardScreen>{
  int _activeMeterIndex;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }
  @override
  Future<bool> _onWillPop() {
    //back
    AppRoutes.push(context, GDPIPrepareForPIScreen());
  }
  Widget build(BuildContext context) {
    final screenHeight = MediaQuery.of(context).size.height;//720dp
    final screenWidth = MediaQuery.of(context).size.width;//360dp
    // TODO: implement build
    return  Scaffold(
        body: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
              child: Row(
                children: <Widget>[
                  Container(
                    child: InkWell(
                      onTap: (){
                        Navigator.pop(context);
                      },
                      child: Container(
                        child: Padding(
                          padding: const EdgeInsets.all(5.0),
                          child: Align(
                            alignment: Alignment.topLeft,
                            child: Container(
                              width: (17/360)*screenWidth,
                              height: (14/720)*screenHeight,
                              margin: EdgeInsets.only(top: (42/720)*screenHeight,left: (18/360)*screenWidth),
                              child: getSvgIcon.backSvgIcon,
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                  Container(
                    height: (20/720)*screenHeight,
                    // width: (46/360)*screenWidth,
                    margin: EdgeInsets.only(left: (20/360)*screenWidth,top: (42/720)*screenHeight),
                    child: Text(
                      GDPIHomeString.GDPI_Title,
                      style: TextStyle (
                        color: NeutralColors.black,
                        fontWeight: FontWeight.w500,
                        fontFamily: "IBMPlexSans",
                        fontStyle: FontStyle.normal,
                        fontSize: (16/360)*screenWidth,
                      ),
                      textAlign: TextAlign.start,
                    ),
                  ),
                ],
              ),
            ),
            Container(
              height: (27/720)*screenHeight,
              //width: (120/360)*screenWidth,
              margin: EdgeInsets.only(top: (30/720)*screenHeight,left: (20/360)*screenWidth),
              child: Text(
                GDPIHomeString.First_list,
                style: TextStyle (
                  color: NeutralColors.preparePi_balck,
                  fontWeight: FontWeight.bold,
                  fontFamily: "IBMPlexSans",
                  fontStyle: FontStyle.normal,
                  fontSize: (18/360)*screenWidth,
                ),
                textAlign: TextAlign.start,
              ),
            ),
            Container(
              child: Column(
                children: <Widget>[
                  Container(
                    height: (45/720)*screenHeight,
                    width: (320/360)*screenWidth,
                    margin: EdgeInsets.only(top: (10/720)*screenHeight,left: (20/360)*screenWidth),
                    decoration: BoxDecoration(
                      border: Border.all(
                        color: NeutralColors.gunmetal.withOpacity(0.10),
                      ),
                      borderRadius: BorderRadius.all(
                          Radius.circular(5)
                      ),
                      //color: NeutralColors.ice_blue,
                    ),
                    child: InkWell(
                      onTap: (){
                        AppRoutes.pushWithAnmation(context, GDPI_BSchoolLists());
                      },
                      child: Row(
                        children: <Widget>[
                          Container(
                            height:(20/720)*screenHeight,
                            width: (20/360)*screenWidth,
                            margin: EdgeInsets.only(top: (13/720)*screenHeight,left: (15/360)*screenWidth,bottom: (12/720)*screenHeight),
                            child: SvgPicture.asset(
                              getGDPISvgImages.play,
                              fit: BoxFit.fitHeight,
                            ),
                          ),
                          Container(
                            height:(23/720)*screenHeight,
                            margin: EdgeInsets.only(top: (11/720)*screenHeight,left: (10/360)*screenWidth,bottom: (11/720)*screenHeight),
                            child: Text(
                              GDPIHomeString.PI_Masterclass,
                              style: TextStyle (
                                color: NeutralColors.dark_navy_blue,
                                fontWeight: FontWeight.w500,
                                fontFamily: "IBMPlexSans",
                                fontStyle: FontStyle.normal,
                                fontSize: (14/360)*screenWidth,
                              ),
                              textAlign: TextAlign.left,
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.only(left: (158/360)*screenWidth),
                            child: InkWell(
                              onTap: (){},
                              child: Icon(
                                Icons.arrow_forward_ios,
                                color: NeutralColors.black,
                                size: 11/720*screenHeight,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  Container(
                    height: (45/720)*screenHeight,
                    width: (320/360)*screenWidth,
                    margin: EdgeInsets.only(top: (10/720)*screenHeight,left: (20/360)*screenWidth),
                    decoration: BoxDecoration(
                      border: Border.all(
                        color: NeutralColors.gunmetal.withOpacity(0.10),

                      ),
                      borderRadius: BorderRadius.all(
                          Radius.circular(5)
                      ),
                      //color: NeutralColors.ice_blue,
                    ),
                    child: InkWell(
                      onTap: (){
                        AppRoutes.pushWithAnmation(context, GDPI_BSchoolLists());
                      },
                      child: Row(
                        children: <Widget>[
                          Container(
                            height:(20/720)*screenHeight,
                            width: (20/360)*screenWidth,
                            margin: EdgeInsets.only(top: (13/720)*screenHeight,left: (15/360)*screenWidth,bottom: (12/720)*screenHeight),
                            child: SvgPicture.asset(
                              getGDPISvgImages.play,
                              fit: BoxFit.fitHeight,
                            ),
                          ),
                          Container(
                            height:(23/720)*screenHeight,
                            margin: EdgeInsets.only(top: (11/720)*screenHeight,left: (10/360)*screenWidth,bottom: (11/720)*screenHeight),
                            child: Text(
                              GDPIHomeString.SOP_Masterclass,
                              style: TextStyle (
                                color: NeutralColors.dark_navy_blue,
                                fontWeight: FontWeight.w500,
                                fontFamily: "IBMPlexSans",
                                fontStyle: FontStyle.normal,
                                fontSize: (14/360)*screenWidth,
                              ),
                              textAlign: TextAlign.left,
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.only(left: (145/360)*screenWidth),
                            child: InkWell(
                              onTap: (){},
                              child: Icon(
                                Icons.arrow_forward_ios,
                                color: NeutralColors.black,
                                size: 11/720*screenHeight,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  Container(
                    height: (45/720)*screenHeight,
                    width: (320/360)*screenWidth,
                    margin: EdgeInsets.only(top: (10/720)*screenHeight,left: (20/360)*screenWidth),
                    decoration: BoxDecoration(
                      border: Border.all(
                        color: NeutralColors.gunmetal.withOpacity(0.10),
                      ),
                      borderRadius: BorderRadius.all(
                          Radius.circular(5)
                      ),
                      //color: NeutralColors.ice_blue,
                    ),
                    child: Row(
                      children: <Widget>[
                        Container(
                          height:(20/720)*screenHeight,
                          width: (20/360)*screenWidth,
                          margin: EdgeInsets.only(top: (13/720)*screenHeight,left: (15/360)*screenWidth,bottom: (12/720)*screenHeight),
                          child: SvgPicture.asset(
                            getGDPISvgImages.practice,
                            fit: BoxFit.fitHeight,
                          ),
                        ),
                        Container(
                          height:(23/720)*screenHeight,
                          margin: EdgeInsets.only(top: (11/720)*screenHeight,left: (10/360)*screenWidth,bottom: (11/720)*screenHeight),
                          child: Text(
                            GDPIHomeString.PI_WorkbookEbook,
                            style: TextStyle (
                              color: NeutralColors.dark_navy_blue,
                              fontWeight: FontWeight.w500,
                              fontFamily: "IBMPlexSans",
                              fontStyle: FontStyle.normal,
                              fontSize: (14/360)*screenWidth,
                            ),
                            textAlign: TextAlign.left,
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(left: (120/360)*screenWidth),
                          child: InkWell(
                            onTap: (){},
                            child: Icon(
                              Icons.arrow_forward_ios,
                              color: NeutralColors.black,
                              size: 11/720*screenHeight,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    height: (45/720)*screenHeight,
                    width: (320/360)*screenWidth,
                    margin: EdgeInsets.only(top: (10/720)*screenHeight,left: (20/360)*screenWidth),
                    decoration: BoxDecoration(
                      border: Border.all(
                        color: NeutralColors.gunmetal.withOpacity(0.10),
                      ),
                      borderRadius: BorderRadius.all(
                          Radius.circular(5)
                      ),
                      //color: NeutralColors.ice_blue,
                    ),
                    child: Row(
                      children: <Widget>[
                        Container(
                          height:(20/720)*screenHeight,
                          width: (20/360)*screenWidth,
                          margin: EdgeInsets.only(top: (13/720)*screenHeight,left: (15/360)*screenWidth,bottom: (12/720)*screenHeight),
                          child: SvgPicture.asset(
                            getGDPISvgImages.practice,
                            fit: BoxFit.fitHeight,
                          ),
                        ),
                        Container(
                          height:(23/720)*screenHeight,
                          margin: EdgeInsets.only(top: (11/720)*screenHeight,left: (10/360)*screenWidth,bottom: (11/720)*screenHeight),
                          child: Text(
                            GDPIHomeString.Number_ImportantConcepts_Definitions,
                            style: TextStyle (
                              color: NeutralColors.dark_navy_blue,
                              fontWeight: FontWeight.w500,
                              fontFamily: "IBMPlexSans",
                              fontStyle: FontStyle.normal,
                              fontSize: (14/360)*screenWidth,
                            ),
                            textAlign: TextAlign.left,
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(left: (17/360)*screenWidth),
                          child: InkWell(
                            onTap: (){},
                            child: Icon(
                              Icons.arrow_forward_ios,
                              color: NeutralColors.black,
                              size: 11/720*screenHeight,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            Container(
              height: (65/720)*screenHeight,
              width: (320/360)*screenWidth,
              margin: EdgeInsets.only(top: (40/720)*screenHeight,left: (20/360)*screenWidth,right: (20/360)*screenWidth),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.all(
                    Radius.circular(5)
                ),
                color: NeutralColors.purpleish_blue,
              ),
              child: Column(
                children: <Widget>[
                  Container(
                    child: Row(
                      children: <Widget>[
                        Container(
                          height: (23/720)*screenHeight,
                          width: (280/360)*screenWidth,
                          margin: EdgeInsets.only(left: (15/360)*screenWidth,top: (10/720)*screenHeight),
                          child: Text(
                            GDPIHomeString.About,
                            style: TextStyle (
                              color: Colors.white,
                              fontWeight: FontWeight.w500,
                              fontFamily: "IBMPlexSans",
                              fontStyle: FontStyle.normal,
                              fontSize: (16/360)*screenWidth,
                            ),
                            textAlign: TextAlign.left,
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(top: (10/720)*screenHeight),
                          child: Icon(
                            Icons.arrow_forward_ios,
                            color: NeutralColors.pureWhite,
                            size: 11/720*screenHeight,
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(left: (15/360)*screenWidth),
                    child: Align(
                      alignment: Alignment.centerLeft,
                      child: Text(
                        GDPIHomeString.AboutScore,
                        style: TextStyle (
                          color: NeutralColors.pureWhite,
                          fontWeight: FontWeight.w400,
                          fontFamily: "IBMPlexSans",
                          fontStyle: FontStyle.normal,
                          fontSize: (12/360)*screenWidth,
                        ),
                        textAlign: TextAlign.left,
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      );

  }
}
import 'package:dotted_border/dotted_border.dart';
import 'package:flutter_document_picker/flutter_document_picker.dart';
import 'package:flutter/material.dart';
import 'package:imsindia/components/custom_expansion_panel.dart';
import 'package:imsindia/resources/strings/GDPI_labels.dart';
import 'package:imsindia/routers/routes.dart';
import 'package:imsindia/utils/colors.dart';
import 'package:imsindia/utils/text_styles.dart';
import 'package:imsindia/views/GDPI/GDPI_home_screen.dart';
import 'package:imsindia/utils/svg_icons.dart';
import 'package:path/path.dart' as path;


var screenWidthTotal;
var screenHeightTotal;
var _safeAreaHorizontal;
var _safeAreaVertical;
var screenHeight;
var screenWidth;
var activeMeterIndex = null;
int likedCommunities = 0;
List<int> numbers = [1, 2, 3, 4, 5, 6];
List<dynamic> joinCommunitiesImages = [
  "assets/images/gdpi-sample.png",
  "assets/images/gdpi-sample-2.png",
  "assets/images/gdpi-sample-3.png",
  "assets/images/mask-7.png",
];
List<dynamic> yourCommunitiesImages = [];
List<dynamic> joinCommunitiesNames = [
  "FMS-CHENNAI",
  "FMS-BENGALURU",
  "FMS-HYDERABAD",
  "FMS-DELHI",
];
List<dynamic> yourCommunitiesNames = [];

class GDPIProcess extends StatefulWidget {
  @override
  GdpiProcessState createState() => GdpiProcessState();
}

class GdpiProcessState extends State<GDPIProcess> {
  bool tenthDetails = false;
  String _path = '';
  bool uploaded = false;
  bool _pickFileInProgress = false;
  bool _iosPublicDataUTI = true;
  bool _checkByCustomExtension = false;
  bool _checkByMimeType = false;
  _pickDocument() async {
    String result;
    try {
      setState(() {
        _path = '';
        _pickFileInProgress = true;
      });
      final _utiController = TextEditingController(
        text: 'com.sidlatau.example.mwfbak',
      );

      final _extensionController = TextEditingController(
        text: 'mwfbak',
      );

      final _mimeTypeController = TextEditingController(
        text: 'application/pdf image/png',
      );
      FlutterDocumentPickerParams params = FlutterDocumentPickerParams(
        allowedFileExtensions: _checkByCustomExtension
            ? _extensionController.text
                .split(' ')
                .where((x) => x.isNotEmpty)
                .toList()
            : null,
        allowedUtiTypes: _iosPublicDataUTI
            ? null
            : _utiController.text
                .split(' ')
                .where((x) => x.isNotEmpty)
                .toList(),
        allowedMimeTypes: _checkByMimeType
            ? _mimeTypeController.text
                .split(' ')
                .where((x) => x.isNotEmpty)
                .toList()
            : null,
      );

      result = await FlutterDocumentPicker.openDocument(params: params);
    } catch (e) {
      print(e);
      result = 'Error: $e';
    } finally {
      setState(() {
        _pickFileInProgress = false;
      });
    }

    setState(() {
      _path = result;
      if(_path!=''&&_path!=null){
        uploaded = true;}
     // print(_path.length.toString());
    });
  }

  @override
  Widget disablePanels(BuildContext context, String name) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(
          Radius.circular(5),
        ),
        color: SemanticColors.iceBlue,
      ),
      margin: EdgeInsets.only(
          top: (20 / 743) * screenHeight, left: 20/360 * screenWidth, right: 20/360 * screenWidth),
      height: (45/743)*screenHeight,
      alignment: Alignment.centerLeft,
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.end,
        children: [
          Container(
            width: (270/360)*screenWidth,
            margin: EdgeInsets.only(left: 15, bottom: 13),
            child: Text(
              name,
              style: TextStyle(
                color: SemanticColors.blueGrey,
                fontSize: (14/720)*screenHeight,
                fontFamily: "IBMPlexSans",
              ),
              textAlign: TextAlign.left,
            ),
          ),
          Align(
            alignment: Alignment.centerLeft,
            child: GestureDetector(
              child: Container(
                width: (32/360)*screenWidth,
                margin: EdgeInsets.only(right: 0),
                child: Icon(
                  Icons.expand_more,
                  color: SemanticColors.blueGrey,
                  size: 23,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    CalculateScreen(context);
    Future<bool> _onWillPop() {
      AppRoutes.push(context, GDPIHomescreen());
    }

    return  Scaffold(
          body: SingleChildScrollView(
        padding: EdgeInsets.only(bottom: 40),
        child: Container(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
                Container(
            child: Row(
              children: <Widget>[
                Container(
                  child: InkWell(
                    onTap: (){
                      Navigator.pop(context);
                    },
                    child: Container(
                      child: Padding(
                        padding: const EdgeInsets.all(5.0),
                        child: Align(
                          alignment: Alignment.topLeft,
                          child: Container(
                            width: (17/360)*screenWidth,
                            height: (14/720)*screenHeight,
                            margin: EdgeInsets.only(top: (60/720)*screenHeight,left: (15/360)*screenWidth),
                            child: getSvgIcon.backSvgIcon,
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
                Container(
                  height: (20/609)*screenHeight,
                  // width: (46/360)*screenWidth,
                  margin: EdgeInsets.only(left: (20/360)*screenWidth,top: (60/720)*screenHeight),
                  child: Text(
                    GDPIHomeString.GDPI_Title,
                    style: TextStyle (
                      color: NeutralColors.black,
                      fontWeight: FontWeight.w500,
                      fontFamily: "IBMPlexSans",
                      fontStyle: FontStyle.normal,
                      fontSize: (16/360)*screenWidth,
                    ),
                    textAlign: TextAlign.start,
                  ),
                ),
              ],
            ),
          ),
              Container(
                  margin: EdgeInsets.only(left: (20/360)*screenWidth, top: (30/720)*screenHeight,right:(29/360)*screenWidth ),
                  child: FittedBox(
                    fit: BoxFit.fitWidth,
                                      child: Text(
                      GDPIHomeString.Third_list,
                      style: TextStyle(
                        color: NeutralColors.black,
                        fontSize: (18/360)*screenWidth,
                        fontFamily: "IBMPlexSans",
                        fontWeight: FontWeight.w700,
                      ),
                      textAlign: TextAlign.left,
                    ),
                  ),
                ),
              
              Container(
                height: 45/720 * screenHeight,
                margin: EdgeInsets.only(left: (20/360)*screenWidth,right: (20/360)*screenWidth,top:(20/720)*screenHeight),
                child: DottedBorder(
                  color:
                      (_path!=''&&_path!=null) ? Colors.blue : Color.fromARGB(77, 87, 93, 96),
                  radius: Radius.circular(5),
                  strokeWidth: 1,
                  dashPattern: [6, 2],
                  borderType: BorderType.RRect,
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: [
                      Container(
                        width: (270/360)*screenWidth,
                        margin: EdgeInsets.only(left:10, bottom: (11/720)*screenHeight,top: (11/720)*screenHeight),
                        child: (_path!=''&&_path!=null)
                            ? Text(path.basename(_path),overflow: _path.length>25?TextOverflow.ellipsis:TextOverflow.visible)
                            : Text(
                                GDPIProcessLabels.uploadDocuments,
                                style: TextStyle(
                                  color: Color.fromARGB(255, 0, 171, 251),
                                  fontSize: (14/360)*screenWidth,
                                  fontFamily: "IBMPlexSans",
                                ),
                                textAlign: TextAlign.left,
                              ),
                      ),
                      Align(

                        alignment: Alignment.centerLeft,
                        child: GestureDetector(
                          child: Container(
                            //height: (15/720)*screenHeight,
                            width: (24/360)*screenWidth,
                            margin: EdgeInsets.only(right: 10/360 * screenWidth),
                            child: Image.asset(
                              "assets/images/clip-4.png",
                              fit: BoxFit.none,
                              color: Color.fromARGB(255, 0, 171, 251),
                            ),
                          ),
                          onTap: _pickFileInProgress ? null : _pickDocument,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              ((_path!=''&&_path!=null))
                  ? Container(
                      margin: EdgeInsets.only(
                          top: (20 / 720) * screenHeight,
                          left: 20.0,
                          right: 20.0),
                      child: CustomExpansionPanelList(
                        expansionHeaderHeight: (45.0 / 720) * screenHeight,
                        iconColor: (activeMeterIndex == 0)
                            ? Colors.white
                            : Colors.black,
                        backgroundColor1: (activeMeterIndex == 0)
                            ? const Color(0xFF6979f8)
                            : (tenthDetails == true)
                                ? NeutralColors.mango.withOpacity(0.05)
                                : const Color(0xFF3380cc).withOpacity(0.10),
                        backgroundColor2: (activeMeterIndex == 0)
                            ? const Color(0xFF6979f8)
                            : (tenthDetails == true)
                                ? NeutralColors.mango.withOpacity(0.05)
                                : const Color(0xFF886dd7).withOpacity(0.10),
                        expansionCallback: (int index, bool status) {
                          setState(() {
                            activeMeterIndex = activeMeterIndex == 0 ? null : 0;
                          });
                        },
                        children: [
                          new ExpansionPanel(
                              canTapOnHeader: true,
                              isExpanded: activeMeterIndex == 0,
                              headerBuilder: (BuildContext context,
                                      bool isExpanded) =>
                                  new Container(
                                    alignment: Alignment.centerLeft,
                                    child: new Padding(
                                      padding: EdgeInsets.only(
                                          left: (15 / 320) * screenWidth),
                                      child: new Text('Process Book',
                                          style: new TextStyle(
                                              fontSize:
                                                  (12.5 / 720) * screenHeight,
                                              fontFamily:
                                                  "IBMPlexSans",
                                              fontWeight: FontWeight.w500,
                                              color: (activeMeterIndex == 0)
                                                  ? Colors.white
                                                  : const Color(0xFF2d3438))),
                                    ),
                                  ),
                              body: Container(
                                margin: EdgeInsets.only(top: 10/720 * screenHeight),
                                child: new Text("Nothing To Display"),
                              )),
                        ],
                      ),
                    )
                  : disablePanels(context, 'Process Book'),
              ((_path!=''&&_path!=null))
                  ? Container(
                      margin: EdgeInsets.only(
                          top: (20 / 720) * screenHeight,
                          left: 20.0,
                          right: 20.0),
                      child: CustomExpansionPanelList(
                        expansionHeaderHeight: (45.0 / 720) * screenHeight,
                        iconColor: (activeMeterIndex == 1)
                            ? Colors.white
                            : Colors.black,
                        backgroundColor1: (activeMeterIndex == 1)
                            ? const Color(0xFF6979f8)
                            : (tenthDetails == true)
                                ? NeutralColors.mango.withOpacity(0.05)
                                : const Color(0xFF3380cc).withOpacity(0.10),
                        backgroundColor2: (activeMeterIndex == 1)
                            ? const Color(0xFF6979f8)
                            : (tenthDetails == true)
                                ? NeutralColors.mango.withOpacity(0.05)
                                : const Color(0xFF886dd7).withOpacity(0.10),
                        expansionCallback: (int index, bool status) {
                          setState(() {
                            activeMeterIndex = activeMeterIndex == 1 ? null : 1;
                          });
                        },
                        children: [
                          new ExpansionPanel(
                              canTapOnHeader: true,
                              isExpanded: activeMeterIndex == 1,
                              headerBuilder: (BuildContext context,
                                      bool isExpanded) =>
                                  new Container(
                                    alignment: Alignment.centerLeft,
                                    child: new Padding(
                                      padding: EdgeInsets.only(
                                          left: (15 / 320) * screenWidth),
                                      child: new Text('Form Filling Guidelines',
                                          style: new TextStyle(
                                              fontSize:
                                                  (12.5 / 720) * screenHeight,
                                              fontFamily:
                                                  "IBMPlexSans",
                                              fontWeight: FontWeight.w500,
                                              color: (activeMeterIndex == 1)
                                                  ? Colors.white
                                                  : const Color(0xFF2d3438))),
                                    ),
                                  ),
                              body: Container(
                                margin: EdgeInsets.only(top: 10/720 * screenHeight),
                                child: new Text("Nothing To Display"),
                              )),
                        ],
                      ),
                    )
                  : disablePanels(context, 'Form Filling Guildelines'),
              (likedCommunities > 0)
                  ? Container(
                      margin: EdgeInsets.only(left: 25, top: 40),
                      child: Align(
                        alignment: Alignment.topLeft,
                        child: Text(
                          GDPIProcessLabels.your_Communities,
                          style: TextStyle(
                            color: Color.fromARGB(255, 0, 3, 44),
                            fontSize: 18,
                            fontFamily: "IBMPlexSans",
                            fontWeight: FontWeight.w700,
                          ),
                          textAlign: TextAlign.left,
                        ),
                      ),
                    )
                  : Text(''),
              (likedCommunities > 0)
                  ? Container(
                      height: 160,
                   // margin: EdgeInsets.only(right: 20),

                      child: ListView.builder(
                          scrollDirection: Axis.horizontal,
                          itemCount: yourCommunitiesImages.length,
                          itemBuilder: (context, index) {
                            return Container(
                              width: 100,
                              margin: EdgeInsets.only(left: 20, top: 20,right:(index==yourCommunitiesImages.length-1)?20:0),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.stretch,
                                children: [
                                  Align(
                                    alignment: Alignment.topLeft,
                                    child: Container(
                                      width: 100,
                                      height: 80,
                                      child: ClipRRect(
                                        borderRadius: BorderRadius.circular(4),
                                        child: Image.asset(
                                          yourCommunitiesImages[index],
                                          fit: BoxFit.none,
                                        ),
                                      ),
                                    ),
                                  ),
                                  Align(
                                    alignment: Alignment.topLeft,
                                    child: Container(
                                      margin: EdgeInsets.only(top: 20),
                                      width: 100,
                                      height: 35,
                                      child: Text(
                                        yourCommunitiesNames[index],
                                        maxLines: 2,
                                        style: TextStyle(
                                          color: Color.fromARGB(255, 0, 3, 44),
                                          fontSize: 14,
                                          fontFamily: "IBMPlexSans",
                                          fontWeight: FontWeight.w500,
                                        ),
                                        textAlign: TextAlign.left,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            );
                          }),
                    )
                  : Text(''),
              Container(
                margin: EdgeInsets.only(left: 20, top: 20),
                child: Align(
                  alignment: Alignment.topLeft,
                  child: Text(
                    GDPIProcessLabels.join_Communities,
                    style: TextStyle(
                      color: Color.fromARGB(255, 0, 3, 44),
                      fontSize: 18,
                      fontFamily: "IBMPlexSans",
                      fontWeight: FontWeight.w700,
                    ),
                    textAlign: TextAlign.left,
                  ),
                ),
              ),
              Container(
                height: 160,
             //   margin: EdgeInsets.only(right: 20),
                child: ListView.builder(
                  shrinkWrap: true,
                    scrollDirection: Axis.horizontal,
                    itemCount: joinCommunitiesImages.length,
                    itemBuilder: (context, index) {
                      return Stack(
                        children: [
                          Container(
                            width: 100,
                            margin: EdgeInsets.only(left: 20, top: 20,right: (index==joinCommunitiesImages.length-1)?20:0),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.stretch,
                              children: [
                                Align(
                                  alignment: Alignment.topLeft,
                                  child: Container(
                                   
                                    width: 100,
                                    height: 80,
                                    child: ClipRRect(
                                      borderRadius: BorderRadius.circular(4),
                                      child: Image.asset(
                                        joinCommunitiesImages[index],
                                        fit: BoxFit.none,
                                      ),
                                    ),
                                  ),
                                ),
                                Align(
                                  alignment: Alignment.topLeft,
                                  child: Container(
                                  
                                    height: 35,
                                    margin: EdgeInsets.only(top: 20),
                                    width: 100,
                                    child: Text(
                                      joinCommunitiesNames[index],
                                      maxLines: 2,
                                      style: TextStyle(
                                        color: Color.fromARGB(255, 0, 3, 44),
                                        fontSize: 14,
                                        fontFamily: "IBMPlexSans",
                                        fontWeight: FontWeight.w500,
                                      ),
                                      textAlign: TextAlign.left,
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Positioned(
                            top: 23,
                            right: (index==joinCommunitiesImages.length-1)?25:4,
                            child: GestureDetector(
                              child: CircleAvatar(
                                radius: 12.0,
                                backgroundColor: Colors.white,
                                child: Icon(
                                  Icons.add,
                                  size: 20.0,
                                ),
                              ),
                              onTap: () {
                           setState(() {
                                  if(yourCommunitiesImages.contains(joinCommunitiesImages[index])){
                                  }else{
                                    likedCommunities = likedCommunities + 1;
                                  yourCommunitiesImages
                                      .add(joinCommunitiesImages[index]);
                                  yourCommunitiesNames
                                      .add(joinCommunitiesNames[index]);
                                }});
                              },
                            ),
                          ),
                        ],
                      );
                    }),
              ),
            ],
          ),
        ),
      ));

  }
}

void CalculateScreen(BuildContext context) {
  var _mediaQueryData = MediaQuery.of(context);
  screenWidthTotal = _mediaQueryData.size.width;
  screenHeightTotal = _mediaQueryData.size.height;
  _safeAreaHorizontal =
      _mediaQueryData.padding.left + _mediaQueryData.padding.right;
  _safeAreaVertical =
      _mediaQueryData.padding.top + _mediaQueryData.padding.bottom;

  screenHeight = screenHeightTotal - _safeAreaVertical;
  screenWidth = screenWidthTotal - _safeAreaHorizontal;
}

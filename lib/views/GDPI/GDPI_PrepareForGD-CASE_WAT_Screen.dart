import 'package:flutter/material.dart';
import 'package:imsindia/routers/routes.dart';
import 'package:imsindia/views/GDPI/GDPI_PrepareForPI_Screen.dart';
import 'package:imsindia/utils/svg_icons.dart';
import 'package:imsindia/resources/strings/GDPI_labels.dart';
import 'package:imsindia/utils/colors.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:imsindia/utils/svg_images/GDPI_svg_images.dart';
import 'package:imsindia/views/GDPI/GDPI_home_scr.dart';

List GDPI__GDCASEWAT=['GD Masterclass','WAT Master Class','GD-WAT Content Bible','Case Study Content Bible'];
class GDPIPrepareForGD_CASE_WATScreen extends StatefulWidget {
  GDPIPrepareForGD_CASE_WATScreen({Key key}) : super(key: key);
  @override
  GDPIPrepareForGD_CASE_WATScreenState createState() => GDPIPrepareForGD_CASE_WATScreenState();
}

class GDPIPrepareForGD_CASE_WATScreenState extends State<GDPIPrepareForGD_CASE_WATScreen>{
  int _activeMeterIndex;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }
  @override
  Future<bool> _onWillPop() {
    //back
    AppRoutes.push(context, GDPIHomescreen());
  }
  Widget build(BuildContext context) {
    final screenHeight = MediaQuery.of(context).size.height;//609dp
    final screenWidth = MediaQuery.of(context).size.width;//360dp
    // TODO: implement build
    return  Scaffold(
        body: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
              child: Row(
                children: <Widget>[
                  Container(
                    child: InkWell(
                      onTap: (){
                        Navigator.pop(context);
                      },
                      child: Container(
                        child: Padding(
                          padding: const EdgeInsets.all(5.0),
                          child: Align(
                            alignment: Alignment.topLeft,
                            child: Container(
                              width: (17/360)*screenWidth,
                              height: (14/609)*screenHeight,
                              margin: EdgeInsets.only(top: (42/609)*screenHeight,left: (18/360)*screenWidth),
                              child: getSvgIcon.backSvgIcon,
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                  Container(
                    height: (20/609)*screenHeight,
                    // width: (46/360)*screenWidth,
                    margin: EdgeInsets.only(left: (20/360)*screenWidth,top: (42/609)*screenHeight),
                    child: Text(
                      GDPIHomeString.GDPI_Title,
                      style: TextStyle (
                        color: NeutralColors.black,
                        fontWeight: FontWeight.w500,
                        fontFamily: "IBMPlexSans",
                        fontStyle: FontStyle.normal,
                        fontSize: (16/360)*screenWidth,
                      ),
                      textAlign: TextAlign.start,
                    ),
                  ),
                ],
              ),
            ),
            Container(
              height: (27/609)*screenHeight,
              width: (230/360)*screenWidth,
              margin: EdgeInsets.only(top: (30/609)*screenHeight,left: (20/360)*screenWidth),
              child: Text(
                GDPIHomeString.Second_list,
                style: TextStyle (
                  color: NeutralColors.preparePi_balck,
                  fontWeight: FontWeight.bold,
                  fontFamily: "IBMPlexSans",
                  fontStyle: FontStyle.normal,
                  fontSize: (18/360)*screenWidth,
                ),
                textAlign: TextAlign.start,
              ),
            ),
            Container(
              child: Column(
                children: <Widget>[
                  Container(
                    height: (45/609)*screenHeight,
                    width: (320/360)*screenWidth,
                    margin: EdgeInsets.only(top: (10/609)*screenHeight,left: (20/360)*screenWidth),
                    decoration: BoxDecoration(
                      border: Border.all(
                        color: NeutralColors.gunmetal.withOpacity(0.1),
                      ),
                      borderRadius: BorderRadius.all(
                          Radius.circular(5)
                      ),
                      //color: NeutralColors.ice_blue,
                    ),
                    child: Row(
                      children: <Widget>[
                        Container(
                          height:(20/609)*screenHeight,
                          width: (20/360)*screenWidth,
                          margin: EdgeInsets.only(top: (13/609)*screenHeight,left: (15/360)*screenWidth,bottom: (12/609)*screenHeight),
                          child: SvgPicture.asset(
                            getGDPISvgImages.play,
                            fit: BoxFit.fitWidth,
                          ),
                        ),
                        Container(
                          height:(23/609)*screenHeight,
                          margin: EdgeInsets.only(top: (11/609)*screenHeight,left: (10/360)*screenWidth,bottom: (11/609)*screenHeight),
                          child: Text(
                            GDPIHomeString.GD_Masterclass,
                            style: TextStyle (
                              color: NeutralColors.dark_navy_blue,
                              fontWeight: FontWeight.w500,
                              fontFamily: "IBMPlexSans",
                              fontStyle: FontStyle.normal,
                              fontSize: (14/360)*screenWidth,
                            ),
                            textAlign: TextAlign.left,
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(left: (148/360)*screenWidth),
                          child: InkWell(
                            onTap: (){},
                            child: Icon(
                              Icons.arrow_forward_ios,
                              color: NeutralColors.black,
                              size: 11/609*screenHeight,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    height: (45/609)*screenHeight,
                    width: (320/360)*screenWidth,
                    margin: EdgeInsets.only(top: (10/609)*screenHeight,left: (20/360)*screenWidth),
                    decoration: BoxDecoration(
                      border: Border.all(
                        color: NeutralColors.gunmetal.withOpacity(0.1),
                      ),
                      borderRadius: BorderRadius.all(
                          Radius.circular(5)
                      ),
                      //color: NeutralColors.ice_blue,
                    ),
                    child: Row(
                      children: <Widget>[
                        Container(
                          height:(20/609)*screenHeight,
                          width: (20/360)*screenWidth,
                          margin: EdgeInsets.only(top: (13/609)*screenHeight,left: (15/360)*screenWidth,bottom: (12/609)*screenHeight),
                          child: SvgPicture.asset(
                            getGDPISvgImages.play,
                            fit: BoxFit.fitWidth,
                          ),
                        ),
                        Container(
                          height:(23/609)*screenHeight,
                          margin: EdgeInsets.only(top: (11/609)*screenHeight,left: (10/360)*screenWidth,bottom: (11/609)*screenHeight),
                          child: Text(
                            GDPIHomeString.WAT_MasterClass,
                            style: TextStyle (
                              color: NeutralColors.dark_navy_blue,
                              fontWeight: FontWeight.w500,
                              fontFamily: "IBMPlexSans",
                              fontStyle: FontStyle.normal,
                              fontSize: (14/360)*screenWidth,
                            ),
                            textAlign: TextAlign.left,
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(left: (137/360)*screenWidth),
                          child: InkWell(
                            onTap: (){},
                            child: Icon(
                              Icons.arrow_forward_ios,
                              color: NeutralColors.black,
                              size: 11/609*screenHeight,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    height: (45/609)*screenHeight,
                    width: (320/360)*screenWidth,
                    margin: EdgeInsets.only(top: (10/609)*screenHeight,left: (20/360)*screenWidth),
                    decoration: BoxDecoration(
                      border: Border.all(
                        color: NeutralColors.gunmetal.withOpacity(0.1),
                      ),
                      borderRadius: BorderRadius.all(
                          Radius.circular(5)
                      ),
                      //color: NeutralColors.ice_blue,
                    ),
                    child: Row(
                      children: <Widget>[
                        Container(
                          height:(20/609)*screenHeight,
                          width: (20/360)*screenWidth,
                          margin: EdgeInsets.only(top: (13/609)*screenHeight,left: (15/360)*screenWidth,bottom: (12/609)*screenHeight),
                          child: SvgPicture.asset(
                            getGDPISvgImages.practice,
                            fit: BoxFit.fitHeight,
                          ),
                        ),
                        Container(
                          height:(23/609)*screenHeight,
                          margin: EdgeInsets.only(top: (11/609)*screenHeight,left: (10/360)*screenWidth,bottom: (11/609)*screenHeight),
                          child: Text(
                            GDPIHomeString.GDWAT_ContentBible,
                            style: TextStyle (
                              color: NeutralColors.dark_navy_blue,
                              fontWeight: FontWeight.w500,
                              fontFamily: "IBMPlexSans",
                              fontStyle: FontStyle.normal,
                              fontSize: (14/360)*screenWidth,
                            ),
                            textAlign: TextAlign.left,
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(left: (107/360)*screenWidth),
                          child: InkWell(
                            onTap: (){},
                            child: Icon(
                              Icons.arrow_forward_ios,
                              color: NeutralColors.black,
                              size: 11/609*screenHeight,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    height: (45/609)*screenHeight,
                    width: (320/360)*screenWidth,
                    margin: EdgeInsets.only(top: (10/609)*screenHeight,left: (20/360)*screenWidth),
                    decoration: BoxDecoration(
                      border: Border.all(
                        color: NeutralColors.gunmetal.withOpacity(0.1),
                      ),
                      borderRadius: BorderRadius.all(
                          Radius.circular(5)
                      ),
                      //color: NeutralColors.ice_blue,
                    ),
                    child: Row(
                      children: <Widget>[
                        Container(
                          height:(20/609)*screenHeight,
                          width: (20/360)*screenWidth,
                          margin: EdgeInsets.only(top: (13/609)*screenHeight,left: (15/360)*screenWidth,bottom: (12/609)*screenHeight),
                          child: SvgPicture.asset(
                            getGDPISvgImages.practice,
                            fit: BoxFit.fitHeight,
                          ),
                        ),
                        Container(
                          height:(23/609)*screenHeight,
                          margin: EdgeInsets.only(top: (11/609)*screenHeight,left: (10/360)*screenWidth,bottom: (11/609)*screenHeight),
                          child: Text(
                            GDPIHomeString.CaseStudy_ContentBible,
                            style: TextStyle (
                              color: NeutralColors.dark_navy_blue,
                              fontWeight: FontWeight.w500,
                              fontFamily: "IBMPlexSans",
                              fontStyle: FontStyle.normal,
                              fontSize: (14/360)*screenWidth,
                            ),
                            textAlign: TextAlign.left,
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(left: (90/360)*screenWidth),
                          child: InkWell(
                            onTap: (){},
                            child: Icon(
                              Icons.arrow_forward_ios,
                              color: NeutralColors.black,
                              size: 11/609*screenHeight,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      );

  }
}
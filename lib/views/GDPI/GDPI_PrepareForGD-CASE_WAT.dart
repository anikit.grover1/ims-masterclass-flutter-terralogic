import 'package:flutter/material.dart';
import 'package:imsindia/utils/colors.dart';
import 'package:imsindia/resources/strings/GDPI_labels.dart';
import 'package:imsindia/utils/screen_util.dart';
import 'package:imsindia/utils/svg_images/prepare_svg_images.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:imsindia/routers/routes.dart';
import 'package:imsindia/views/GDPI/GDPI_B_School_Processes.dart';
import 'package:imsindia/views/GDPI/GDPI_Lists_B_School.dart';
import 'package:imsindia/views/GDPI/GDPI_PrepareForGD-CASE_WAT_Screen.dart';
import 'package:imsindia/views/GDPI/GDPI_PrepareForPI_Screen.dart';
import 'package:imsindia/components/navigation_bar.dart';
import 'package:imsindia/views/home_pages/home_widget.dart';
import 'package:imsindia/utils/svg_images/GDPI_svg_images.dart';
import 'package:imsindia/utils/svg_images/prepare_svg_images.dart';

class GDPIPrepareForGDCASEWAT extends StatefulWidget{
  @override
  GDPIPrepareForGDCASEWATState createState() => GDPIPrepareForGDCASEWATState();
}

class GDPIPrepareForGDCASEWATState extends State<GDPIPrepareForGDCASEWAT>{
  List title=[
    'GD Master Class',
    'WAT Master Class',
    'GD-WAT Content Bible',
    'Case Study Content Bible'];
  int _activeMeterIndex;
  List<dynamic> scorecardImage = [
    getGDPISvgImages.play,
    getGDPISvgImages.practice,
    getGDPISvgImages.practice,
    getGDPISvgImages.practice,
  ];
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }
  @override
  Future<bool> _onWillPop() {
    //back
    AppRoutes.push(context, GDPIPrepareForPIScreen());
  }
  Widget build(BuildContext context) {
    final screenHeight = MediaQuery.of(context).size.height;//720dp
    final screenWidth = MediaQuery.of(context).size.width;//360dp
    // TODO: implement build
    return  Scaffold(
      body: Container(
        margin: EdgeInsets.only(
          top: 0 / Constant.defaultScreenHeight * screenHeight,
        ),
        child: Column(
          children: <Widget>[
            Container(
              margin: EdgeInsets.only(
                top: (42 / 720) * screenHeight,
              ),
              child: Row(
                //crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  InkWell(
                    onTap: () {
                      Navigator.pop(context);
                    },
                    child: Container(
                      width: (58 / 360) * screenWidth,
                      height: (30.0 / 720) * screenHeight,
                      margin: EdgeInsets.only(left: 0.0),
                      child: SvgPicture.asset(
                        getPrepareSvgImages.backIcon,
                        height: (5 / 720) * screenHeight,
                        width: (14 / 360) * screenWidth,
                        fit: BoxFit.none,
                      ),
                    ),
                  ),
                  Container(
                    height: (20 / 720) * screenHeight,
                    // margin: EdgeInsets.only(left: (20 / 360) * screenWidth),
                    child: Text(
                      "GD-PI",
                      style: TextStyle(
                        color: NeutralColors.black,
                        fontSize: (16 / 720) * screenHeight,
                        fontFamily: "IBMPlexSans",
                        fontWeight: FontWeight.w500,
                      ),
                      textAlign: TextAlign.left,
                    ),
                  ),
                ],
              ),
            ),
            Align(
              alignment: Alignment.topLeft,
              child: Container(
                //height: (27/720)*screenHeight,
                //width: (120/360)*screenWidth,
                margin: EdgeInsets.only(top: (15/720)*screenHeight,left: (20/360)*screenWidth),
                child: Text(
                  GDPIHomeString.First_list,
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle (
                    color: NeutralColors.preparePi_balck,
                    fontWeight: FontWeight.bold,
                    fontFamily: "IBMPlexSans",
                    fontStyle: FontStyle.normal,
                    fontSize: (18/360)*screenWidth,
                  ),
                  textAlign: TextAlign.start,
                ),
              ),
            ),
            Expanded(
              child: Container(
                margin: EdgeInsets.only(left: 20/360 * screenWidth, right: 20/360 * screenWidth, top: 20/720 * screenHeight),
                child: ListView.builder(
                  itemCount: title.length,
                  padding: EdgeInsets.only(bottom: (40)),
                  itemBuilder: (context, index) {
                    return Container(
                      height: (45/720)*screenHeight,
                      width: (320/360)*screenWidth,
                      margin: EdgeInsets.only(top: (10/720)*screenHeight),
                      decoration: BoxDecoration(
                        border: Border.all(
                          color: NeutralColors.gunmetal.withOpacity(0.10),
                        ),
                        borderRadius: BorderRadius.all(
                            Radius.circular(5)
                        ),
                        //color: NeutralColors.ice_blue,
                      ),
                      child: InkWell(
                        onTap: (){
                          //AppRoutes.pushWithAnmation(context, GDPI_BSchoolLists());
                        },
                        child: Row(
                          children: <Widget>[
                            Container(
                              height:(20/720)*screenHeight,
                              width: (20/360)*screenWidth,
                              margin: EdgeInsets.only(top: (13/720)*screenHeight,left: (15/360)*screenWidth,bottom: (12/720)*screenHeight),
                              child: SvgPicture.asset(
                                scorecardImage[index],
                                fit: BoxFit.fitHeight,
                              ),
                            ),
                            Container(
                              //height:(23/720)*screenHeight,
                              width: 236/360 * screenWidth,
                              margin: EdgeInsets.only(top: (11/720)*screenHeight,left: (10/360)*screenWidth,bottom: (11/720)*screenHeight),
                              child: Text(
                                title[index],
                                style: TextStyle (
                                  color: NeutralColors.dark_navy_blue,
                                  fontWeight: FontWeight.w500,
                                  fontFamily: "IBMPlexSans",
                                  fontStyle: FontStyle.normal,
                                  fontSize: (14/360)*screenWidth,
                                ),
                                textAlign: TextAlign.left,
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.only(left: (15/360)*screenWidth),
                              child: InkWell(
                                onTap: (){},
                                child: Icon(
                                  Icons.arrow_forward_ios,
                                  color: NeutralColors.black,
                                  size: 11/720*screenHeight,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    );
                  },
                ),
              ),
            ),

          ],
        ),
      ),
    );

  }
}

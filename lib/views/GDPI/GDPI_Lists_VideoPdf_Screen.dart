import 'package:flutter/material.dart';
import 'package:imsindia/utils/colors.dart';
import 'package:imsindia/resources/strings/GDPI_labels.dart';
import 'package:imsindia/utils/screen_util.dart';
import 'package:imsindia/utils/svg_images/prepare_svg_images.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:imsindia/routers/routes.dart';
import 'package:imsindia/views/GDPI/GDPI_B_School_Processes.dart';
import 'package:imsindia/views/GDPI/GDPI_Lists_B_School.dart';
import 'package:imsindia/views/GDPI/GDPI_PDFscreen.dart';
import 'package:imsindia/views/GDPI/GDPI_PrepareForGD-CASE_WAT_Screen.dart';
import 'package:imsindia/views/GDPI/GDPI_PrepareForPI_Screen.dart';
import 'package:imsindia/components/navigation_bar.dart';
import 'package:imsindia/views/home_pages/home_widget.dart';
import 'package:imsindia/utils/svg_images/GDPI_svg_images.dart';
import 'package:imsindia/utils/svg_images/prepare_svg_images.dart';
import 'package:imsindia/utils/global.dart' as global;
import 'package:imsindia/api/bloc/services.dart';
import 'GDPI_VideoPlayer_Screen.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:imsindia/utils/Url.dart';
import 'WebViewExamplePDF_GDPI.dart';


var SignedURL;
class GDPI_Lists_VideoPdf extends StatefulWidget{
  var title = [];
  var referenceType = [];
  var vimeoURI = [];
  var video_id = [];
  var test_id = [];
  var secondTitle;
  var isProductAccess = [];
  GDPI_Lists_VideoPdf(this.secondTitle,this.title,this.referenceType,this.vimeoURI,this.video_id,this.test_id,this.isProductAccess);
  @override
  GDPI_Lists_VideoPdfState createState() => GDPI_Lists_VideoPdfState();
}

class GDPI_Lists_VideoPdfState extends State<GDPI_Lists_VideoPdf>{
  // List title=[
  //   'GD Master Class',
  //   'WAT Master Class',
  //   'GD-WAT Content Bible',
  //   'Case Study Content Bible'];
  var userId;
  int _activeMeterIndex;
  List<dynamic> scorecardImage = [
    getGDPISvgImages.play,
    getGDPISvgImages.practice,
    getGDPISvgImages.practice,
    getGDPISvgImages.practice,
  ];
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    print("==========================ref type");
    print(widget.referenceType);
    getUserID();
  }
  @override
  Future<bool> _onWillPop() {
    //back
    //AppRoutes.push(context, GDPIPrepareForPIScreen());
    Navigator.pop(context,true);
  }
  Widget build(BuildContext context) {
    final screenHeight = MediaQuery.of(context).size.height;//720dp
    final screenWidth = MediaQuery.of(context).size.width;//360dp
    // TODO: implement build
    return WillPopScope(
      onWillPop: _onWillPop,
      child: Scaffold(
      body: Container(
        margin: EdgeInsets.only(
          top: 0 / Constant.defaultScreenHeight * screenHeight,
        ),
        child: Column(
          children: <Widget>[
            Container(
              margin: EdgeInsets.only(
                top: (42 / 720) * screenHeight,
              ),
              child: Row(
                //crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  InkWell(
                    onTap: () {
                      Navigator.pop(context,true);
                    },
                    child: Container(
                      width: (58 / 360) * screenWidth,
                      height: (30.0 / 720) * screenHeight,
                      margin: EdgeInsets.only(left: 0.0),
                      child: SvgPicture.asset(
                        getPrepareSvgImages.backIcon,
                        height: (5 / 720) * screenHeight,
                        width: (14 / 360) * screenWidth,
                        fit: BoxFit.none,
                      ),
                    ),
                  ),
                  Container(
                    height: (20 / 720) * screenHeight,
                    // margin: EdgeInsets.only(left: (20 / 360) * screenWidth),
                    child: Text(
                      widget.secondTitle,
                      style: TextStyle(
                        color: NeutralColors.black,
                        fontSize: (16 / 720) * screenHeight,
                        fontFamily: "IBMPlexSans",
                        fontWeight: FontWeight.w500,
                      ),
                      textAlign: TextAlign.left,
                    ),
                  ),
                ],
              ),
            ),
            // Align(
            //   alignment: Alignment.topLeft,
            //   child: Container(
            //     //height: (27/720)*screenHeight,
            //     //width: (120/360)*screenWidth,
            //     margin: EdgeInsets.only(top: (15/720)*screenHeight,left: (20/360)*screenWidth),
            //     child: Text(
            //       '',
            //       overflow: TextOverflow.ellipsis,
            //       style: TextStyle (
            //         color: NeutralColors.preparePi_balck,
            //         fontWeight: FontWeight.bold,
            //         fontFamily: "IBMPlexSans",
            //         fontStyle: FontStyle.normal,
            //         fontSize: (18/360)*screenWidth,
            //       ),
            //       textAlign: TextAlign.start,
            //     ),
            //   ),
            // ),
            Expanded(
              child: Container(
                margin: EdgeInsets.only(left: 20/360 * screenWidth, right: 20/360 * screenWidth, top: 20/720 * screenHeight),
                child: ListView.builder(
                  itemCount: widget.title.length,
                  padding: EdgeInsets.only(bottom: (40)),
                  itemBuilder: (context, index) {
                    return Container(
                      height: (45/720)*screenHeight,
                      width: (320/360)*screenWidth,
                      margin: EdgeInsets.only(top: (10/720)*screenHeight),
                      decoration: BoxDecoration(
                        border: Border.all(
                          color: NeutralColors.gunmetal.withOpacity(0.10),
                        ),
                        borderRadius: BorderRadius.all(
                            Radius.circular(5)
                        ),
                        //color: NeutralColors.ice_blue,
                      ),
                      child: InkWell(
                        onTap: (){
                          print("====================================selected index");
                          print(widget.vimeoURI);
                          print(index);
                          if(widget.isProductAccess[index]==true && widget.referenceType[index] =="Video"){
                            AppRoutes.pushWithAnmation(context, WebViewExample_GDPI(widget.vimeoURI[index],widget.video_id[index],userId));
                          }else if (widget.isProductAccess[index]==true && widget.referenceType[index]=="Pdf"){
                            global.getToken.then((t){
                              LoadPDFAPI(t,widget.test_id[index]);
                            });
                          }
                          else if(widget.isProductAccess[index]==false)
                          {
                            showModalBottomSheet<dynamic>(
                                backgroundColor: Colors.transparent,
                                isScrollControlled: true,
                                context: context,
                                builder: (BuildContext bc) {
                                  return Wrap(
                                      children: <Widget>[
                                        Container(
                                          child: Container(
                                            decoration: new BoxDecoration(
                                                color:  Colors.white,
                                                borderRadius: new BorderRadius.only(
                                                    topLeft: const Radius.circular(25.0),
                                                    topRight: const Radius.circular(25.0))),
                                            child: _popUpForPauseButton(context),
                                          ),
                                        )
                                      ]
                                  );
                                }
                            );
                          }

                          // widget.referenceType[index] == "Video" ?
                          // (widget.isProductAccess[index]==true?AppRoutes.pushWithAnmation(context, WebViewExample_GDPI(widget.vimeoURI[index],widget.video_id[index],userId))
                          //     :
                          // showModalBottomSheet<dynamic>(
                          //     backgroundColor: Colors.transparent,
                          //     isScrollControlled: true,
                          //     context: context,
                          //     builder: (BuildContext bc) {
                          //       return Wrap(
                          //           children: <Widget>[
                          //             Container(
                          //               child: Container(
                          //                 decoration: new BoxDecoration(
                          //                     color:  Colors.white,
                          //                     borderRadius: new BorderRadius.only(
                          //                         topLeft: const Radius.circular(25.0),
                          //                         topRight: const Radius.circular(25.0))),
                          //                 child: _popUpForPauseButton(context),
                          //               ),
                          //             )
                          //           ]
                          //       );
                          //     }
                          // )
                          // )
                          //     :
                          // //For pdf view have to modify
                          // global.getToken.then((t){
                          //   if(widget.isProductAccess[index] == true){
                          //     LoadPDFAPI(t,widget.test_id[index]);
                          //   }
                          //   else{
                          //     showModalBottomSheet<dynamic>(
                          //         backgroundColor: Colors.transparent,
                          //         isScrollControlled: true,
                          //         context: context,
                          //         builder: (BuildContext bc) {
                          //           return Wrap(
                          //               children: <Widget>[
                          //                 Container(
                          //                   child: Container(
                          //                     decoration: new BoxDecoration(
                          //                         color:  Colors.white,
                          //                         borderRadius: new BorderRadius.only(
                          //                             topLeft: const Radius.circular(25.0),
                          //                             topRight: const Radius.circular(25.0))),
                          //                     child: _popUpForPauseButton(context),
                          //                   ),
                          //                 )
                          //               ]
                          //           );
                          //         }
                          //     );
                          //   }
                          // });

                        },
                        child: Row(
                          children: <Widget>[
                            Container(
                              height:(20/720)*screenHeight,
                              width: (20/360)*screenWidth,
                              margin: EdgeInsets.only(top: (13/720)*screenHeight,left: (15/360)*screenWidth,bottom: (12/720)*screenHeight),
                              child: SvgPicture.asset(
                                widget.referenceType[index] == "Video" ? getGDPISvgImages.play : getGDPISvgImages.practice,
                                fit: BoxFit.fitHeight,
                              ),
                            ),
                            Container(
                              //height:(23/720)*screenHeight,
                              width: 236/360 * screenWidth,
                              margin: EdgeInsets.only(top: (11/720)*screenHeight,left: (10/360)*screenWidth,bottom: (11/720)*screenHeight),
                              child: Text(
                                widget.title[index],
                                style: TextStyle (
                                  color: NeutralColors.dark_navy_blue,
                                  fontWeight: FontWeight.w500,
                                  fontFamily: "IBMPlexSans",
                                  fontStyle: FontStyle.normal,
                                  fontSize: (14/360)*screenWidth,
                                ),
                                textAlign: TextAlign.left,
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.only(left: (15/360)*screenWidth),
                              child: InkWell(
                                onTap: (){},
                                child: Icon(
                                  Icons.arrow_forward_ios,
                                  color: NeutralColors.black,
                                  size: 11/720*screenHeight,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    );
                  },
                ),
              ),
            ),

          ],
        ),
      ),
    ),
    );


  }

  void getUserID() async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    userId = prefs.getString("userId");
  }

  void LoadPDFAPI(Map t,String test_id){
    print("&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&");
    print(test_id);
    if (global.headersWithAuthorizationKey['Authorization'] != '') {
      ApiService().getAPI(URL.GDPI_GET_LEARNINGOBJECT + test_id, t)
          .then((returnValue) {
        print(URL.GDPI_GET_LEARNINGOBJECT );
        setState(() {
          print(returnValue);
          if (returnValue[0].toString().toLowerCase() == 'Fetched Successfully'.toLowerCase()) {
            var totalDataofPDF_GDPI = returnValue[1]['data'];
            print(totalDataofPDF_GDPI['signedURL']);
            //SignedURL = totalDataofPDF_GDPI['signedURL'];
            AppRoutes.pushWithAnmation(context, GPDI_PDFScreen(totalDataofPDF_GDPI['signedURL']));
          }else{

          }
        });
      });
    }
  }
}


Widget _popUpForPauseButton(context) {
  final screenHeight = MediaQuery.of(context).size.height;
  final screenWidth = MediaQuery.of(context).size.width;

  return Container(
    height: screenHeight * 265 / 640,
    margin: EdgeInsets.only(bottom: 154 / 640 * screenHeight),
    child: Column(
      mainAxisAlignment: MainAxisAlignment.end,
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        Align(
          alignment: Alignment.topCenter,
          child: Container(
            width: screenWidth * 140 / 360,
            height: screenHeight * 130 / 720,
            margin: EdgeInsets.only(bottom: 17 / 720 * screenHeight),
            child: SvgPicture.asset(
              getPrepareSvgImages.puseImageForPopUp,
              fit: BoxFit.fill,
            ),
          ),
        ),
        Align(
          alignment: Alignment.topCenter,
          child: Container(
            margin: EdgeInsets.only(bottom: 27 / 720 * screenHeight),
            child: Text(
              "No Access To This Module",
              style: TextStyle(
                color: NeutralColors.black,
                fontSize: screenHeight * 16 / 678,
                fontFamily: "IBMPlexSans",
                fontWeight: FontWeight.w500,
              ),
              textAlign: TextAlign.center,
            ),
          ),
        ),
        Align(
          alignment: Alignment.center,
          child: Container(
            width: screenWidth * 130 / 360,
            height: screenHeight * 40 / 720,
            margin: EdgeInsets.only(top: 20 / 720 * screenHeight),
            child: FlatButton(
              onPressed: () {
                Navigator.pop(context);
              },
              color: Color.fromARGB(255, 0, 171, 251),
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(2)),
              ),
              textColor: NeutralColors.pureWhite,
              padding: EdgeInsets.all(0),
              child: Text(
                "OKAY",
                style: TextStyle(
                  fontSize: screenHeight * 14 / 720,
                  fontFamily: "IBMPlexSans",
                  fontWeight: FontWeight.w500,
                ),
                textAlign: TextAlign.center,
              ),
            ),
          ),
        ),
      ],
    ),
  );
}



import 'package:flutter/material.dart';
import 'package:imsindia/utils/colors.dart';
import 'package:imsindia/resources/strings/GDPI_labels.dart';
import 'package:imsindia/routers/routes.dart';
import 'package:imsindia/utils/svg_icons.dart';
import 'package:imsindia/components/custom_expansion_panel.dart';
import 'package:dotted_border/dotted_border.dart';
import 'package:imsindia/views/GDPI/GDPI_PrepareForPI_ScoreBoard_Screen.dart';
import 'package:imsindia/views/GDPI/GDPI_home_screen.dart';
import 'package:sliding_up_panel/sliding_up_panel.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:imsindia/utils/svg_images/prepare_svg_images.dart';


List BschoolList = [
  'FMS - Chennai',
  'FMS - Delhi',
  'IIM - Ahmedabad PGP FABM',
  'IIM C + ISI + IIT K',
  'MICA Ahmedabad',
  'IIM C + ISI + IIT K',
  'MICA Ahmedabad',
];

class GDPI_BSchoolLists extends StatefulWidget{
  @override
  GDPI_BSchoolListsState createState() => GDPI_BSchoolListsState();
}

class GDPI_BSchoolListsState extends State<GDPI_BSchoolLists>{
  final _BSchoolControllerForPopUp = PanelController();
  double _GDPICpanelHeightClosed = 0.0;
  int _activeMeterIndex;
  bool preparePopUpForResumeAndBackCheck = true;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Future<bool> _onWillPop() {
    //back
    AppRoutes.push(context, GDPIPrepareForPIScoreBoardScreen());
  }
  Widget build(BuildContext context) {
    final screenHeight = MediaQuery.of(context).size.height;//743dp
    final screenWidth = MediaQuery.of(context).size.width;//360dp
    // TODO: implement build
    return  Scaffold(
        body: Container(
          child: SlidingUpPanel(
            maxHeight: (430/720)*screenHeight,
            minHeight: _GDPICpanelHeightClosed,
            parallaxEnabled: false,
            parallaxOffset: 0.5,
            defaultPanelState: PanelState.CLOSED,
            isDraggable: true,
            body: _body(screenHeight,screenWidth),
            panel: _popUpForGDPIsubmit_Button(context),
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(25.0), topRight: Radius.circular(25.0)),
            controller: _BSchoolControllerForPopUp,
          ),
        ),
      );

  }
  Widget  _body(final screenHeight,final screenWidth){
    return Container(
      constraints: BoxConstraints.expand(),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Container(
            child: Row(
              children: <Widget>[
                Container(
                  child: InkWell(
                    onTap: (){
                      Navigator.pop(context);
                    },
                    child: Container(
                      child: Padding(
                        padding: const EdgeInsets.all(5.0),
                        child: Align(
                          alignment: Alignment.topLeft,
                          child: Container(
                            width: (17/360)*screenWidth,
                            height: (15/720)*screenHeight,
                            margin: EdgeInsets.only(top: (42/720)*screenHeight,left: (15/360)*screenWidth),
                            child: getSvgIcon.backSvgIcon,
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
                Container(
                  height: (22/720)*screenHeight,
                  // width: (46/360)*screenWidth,
                  margin: EdgeInsets.only(left: (20/360)*screenWidth,top: (42/720)*screenHeight),
                  child: Text(
                    GDPIHomeString.GDPI_Title,
                    style: TextStyle (
                      color: NeutralColors.black,
                      fontWeight: FontWeight.w500,
                      fontFamily: "IBMPlexSans",
                      fontStyle: FontStyle.normal,
                      fontSize: (16/360)*screenWidth,
                    ),
                    textAlign: TextAlign.start,
                  ),
                ),
              ],
            ),
          ),
          Container(
            height: (69/720)*screenHeight,
            width: (320/360)*screenWidth,
            margin: EdgeInsets.only(left: (20/360)*screenWidth,right: (20/360)*screenWidth,top: (30/720)*screenHeight),
            child: Text(
              GDPIHomeString.Select_BSCHOOL,
              style: TextStyle (
                color: NeutralColors.gunmetal,
                fontWeight: FontWeight.normal,
                fontFamily: "IBMPlexSans",
                fontStyle: FontStyle.normal,
                fontSize: (14/360)*screenWidth,
              ),
              textAlign: TextAlign.start,
            ),
          ),
          Expanded(
            child: LayoutBuilder(builder: (BuildContext context, BoxConstraints viewportConstraints){
              return SingleChildScrollView(
                  child: ConstrainedBox(constraints: BoxConstraints(minHeight: viewportConstraints.maxHeight),
                    child: getList_GDPI_BSchool(screenHeight,screenWidth),
                ),
              );
            }),
          ),
        ],
      ),
    );
  }
  Widget getList_GDPI_BSchool(final screenHeight,final screenWidth){
    List<Widget> list_gdpi = new List<Widget>();
    for(var Count=0; Count < BschoolList.length; Count++){
      list_gdpi.add(
          new Container(
            margin: EdgeInsets.only(left: (20/360)*screenWidth,right: (20/360)*screenWidth),
            child:Container(
              margin: EdgeInsets.only(top: (10/720)*screenHeight),
              child: CustomExpansionPanelList(
                expansionHeaderHeight: (45/720)*screenHeight,
                iconColor: (_activeMeterIndex == Count)?Colors.white:Colors.black,
                backgroundColor1: (_activeMeterIndex == Count)?NeutralColors.purpleish_blue:SemanticColors.light_purpely.withOpacity(0.1),
                backgroundColor2:(_activeMeterIndex == Count)? NeutralColors.purpleish_blue:SemanticColors.dark_purpely.withOpacity(0.1),
                expansionCallback: (int index, bool status) {
                  setState(() {
                    _activeMeterIndex = _activeMeterIndex == Count ? null : Count;
                  });
                },
                children: [
                  new ExpansionPanel(
                      canTapOnHeader: true,
                      isExpanded: _activeMeterIndex == Count,
                      headerBuilder: (BuildContext context, bool isExpanded)=>
                      new Container(
                        alignment: Alignment.centerLeft,
                        child: new Padding(
                          padding: EdgeInsets.only(left: (15/360)*screenWidth),
                          child: new Text(
                              BschoolList[Count],
                              style: new TextStyle(
                                  fontSize: (14/720)*screenHeight,
                                  fontFamily: "IBMPlexSans",
                                  fontWeight: FontWeight.w500,
                                  color: (_activeMeterIndex == Count) ?NeutralColors.pureWhite:NeutralColors.charcoal_grey)),
                        ),
                      ),
                      body: Container(
                        decoration: new BoxDecoration(
                          boxShadow: [
                            new BoxShadow(
                              color: Colors.white,
                              blurRadius: 10.0,spreadRadius: 0.0,offset: new Offset(0.0, 10.0),
                            )
                          ],
                        ),
                        height:  (151/720)*screenHeight,
                        child: Container(
                          height: (151/720)*screenHeight,
                          width: (320/360)*screenWidth,
                          margin: EdgeInsets.only(top: (19.2/720)*screenHeight),
                          child: Column(
                            children: <Widget>[
                              Container(
                                height: (45/720)*screenHeight,
                                width: (280/360)*screenWidth,
                                child: DottedBorder(
                                  padding: EdgeInsets.all(4),
                                  dashPattern: [3],
                                  borderType: BorderType.RRect,
                                  radius: Radius.circular(5),
                                  color: NeutralColors.gunmetal.withOpacity(0.3),
                                  child: Row(
                                   children: <Widget>[
                                     Container(
                                       margin: EdgeInsets.only(left: (15/360)*screenWidth),
                                       alignment: Alignment.centerLeft,
                                       child: Text(
                                         GDPIHomeString.CAT_Scorecard,
                                         style: TextStyle(
                                           color: NeutralColors.charcoal_grey,
                                           fontSize: 14/360*screenWidth,
                                           fontFamily: "IBMPlexSans",
                                           fontWeight: FontWeight.normal,
                                         ),
                                         textAlign: TextAlign.center,
                                       ),
                                     ),
                                     Container(
                                       margin: EdgeInsets.only(left: (100/360)*screenWidth),
                                       child: Padding(
                                         padding: const EdgeInsets.all(5.0),
                                         child: InkWell(
                                           onTap: (){
                                            setState(() {
                                              _BSchoolControllerForPopUp.open();
                                            });
                                           },
                                           child:  Image.asset(
                                             "assets/images/clip-4.png",
                                             fit: BoxFit.none,
                                             color: Color.fromARGB(255, 0, 171, 251),
                                           ),
                                         ),
                                       ),
                                     ),
                                   ],
                                  )
                                ),
                              ),
                              Container(
                                height: (45/720)*screenHeight,
                                width: (280/360)*screenWidth,
                                margin: EdgeInsets.only(top: (20/720)*screenHeight),
                                child: DottedBorder(
                                    padding: EdgeInsets.all(4),
                                    dashPattern: [3],
                                    borderType: BorderType.RRect,
                                    radius: Radius.circular(5),
                                    color: NeutralColors.gunmetal.withOpacity(0.3),
                                    child: Row(
                                      children: <Widget>[
                                        Container(
                                          margin: EdgeInsets.only(left: (15/360)*screenWidth),
                                          alignment: Alignment.centerLeft,
                                          child: Text(
                                            GDPIHomeString.Call_Letter,
                                            style: TextStyle(
                                              color: NeutralColors.charcoal_grey,
                                              fontSize: 14/360*screenWidth,
                                              fontFamily: "IBMPlexSans",
                                              fontWeight: FontWeight.normal,
                                            ),
                                            textAlign: TextAlign.center,
                                          ),
                                        ),
                                        Container(
                                          margin: EdgeInsets.only(left: (165/360)*screenWidth),
                                          child: Padding(
                                            padding: const EdgeInsets.all(5.0),
                                            child: InkWell(
                                              onTap: (){
                                                setState(() {
                                                  _BSchoolControllerForPopUp.open();
                                                });
                                              },
                                              child: Image.asset(
                                                "assets/images/clip-4.png",
                                                fit: BoxFit.none,
                                                color: Color.fromARGB(255, 0, 171, 251),
                                              ),
                                            ),
                                          ),
                                        ),
                                      ],
                                    )
                                ),
                              ),
                            ],
                          ),
                        ),
                      )
                  )
                ],
              ),
            ),
          )
      );
    }
    return new Column(children: list_gdpi);
  }
  Widget _popUpForGDPIsubmit_Button(context) {
    final screenHeight = MediaQuery.of(context).size.height;
    final screenWidth = MediaQuery.of(context).size.width;

    return Container(
      decoration: new BoxDecoration(
        boxShadow: [
          new BoxShadow(
            color: Colors.white,
            blurRadius: 10.0,spreadRadius: 0.0,offset: new Offset(0.0, 10.0),
          )
        ],
      ),
      height:  (430/720)*screenHeight,
      child: Container(
        height: (430/638)*screenHeight,
        width: (360/360)*screenWidth,
        margin: EdgeInsets.only(top: (30/720)*screenHeight),
        child: Column(
          children: <Widget>[
            Container(
              height: (25/720)*screenHeight,
              width: (272/360)*screenWidth,
              margin: EdgeInsets.only(left: (44/360)*screenWidth,right: (44/360)*screenWidth),
              child: Text(
                GDPIHomeString.B_School_Name,
                style: TextStyle(
                  color: NeutralColors.black,
                  fontSize: (16/360)*screenWidth,
                  fontFamily: "IBMPlexSans",
                  fontWeight: FontWeight.bold,
                ),
                textAlign: TextAlign.center,
              ),
            ),
            Container(
              height: (45/720)*screenHeight,
              width: (280/360)*screenWidth,
              margin: EdgeInsets.only(top: (20/720)*screenHeight),
              child: DottedBorder(
                  padding: EdgeInsets.all(4),
                  dashPattern: [3],
                  borderType: BorderType.RRect,
                  radius: Radius.circular(5),
                  color: NeutralColors.gunmetal.withOpacity(0.3),
                  child: Row(
                    children: <Widget>[
                      Container(
                        margin: EdgeInsets.only(left: (15/360)*screenWidth),
                        alignment: Alignment.centerLeft,
                        child: Text(
                          GDPIHomeString.CAT_Scorecard,
                          style: TextStyle(
                            color: NeutralColors.charcoal_grey,
                            fontSize: 14/360*screenWidth,
                            fontFamily: "IBMPlexSans",
                            fontWeight: FontWeight.normal,
                          ),
                          textAlign: TextAlign.center,
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(left: (100/360)*screenWidth),
                        child: InkWell(
                          onTap: (){
                            setState(() {
                              _BSchoolControllerForPopUp.open();
                            });
                          },
                          child: Image.asset(
                            "assets/images/clip-4.png",
                            fit: BoxFit.none,
                            color: Color.fromARGB(255, 0, 171, 251),
                          ),
                        ),
                      ),
                    ],
                  )
              ),
            ),
            Container(
              height: (45/720)*screenHeight,
              width: (280/360)*screenWidth,
              margin: EdgeInsets.only(top: (20/720)*screenHeight),
              child: DottedBorder(
                  padding: EdgeInsets.all(4),
                  dashPattern: [3],
                  borderType: BorderType.RRect,
                  radius: Radius.circular(5),
                  color: NeutralColors.gunmetal.withOpacity(0.3),
                  child: Row(
                    children: <Widget>[
                      Container(
                        margin: EdgeInsets.only(left: (15/360)*screenWidth),
                        alignment: Alignment.centerLeft,
                        child: Text(
                          GDPIHomeString.Call_Letter,
                          style: TextStyle(
                            color: NeutralColors.charcoal_grey,
                            fontSize: 14/360*screenWidth,
                            fontFamily: "IBMPlexSans",
                            fontWeight: FontWeight.normal,
                          ),
                          textAlign: TextAlign.center,
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(left: (165/360)*screenWidth),
                        child: InkWell(
                          onTap: (){},
                          child: Image.asset(
                            "assets/images/clip-4.png",
                            fit: BoxFit.none,
                            color: Color.fromARGB(255, 0, 171, 251),
                          ),
                        ),
                      ),
                    ],
                  )
              ),
            ),
            Container(
              child: Center(
                child: InkWell(
                  onTap: (){
                    _BSchoolControllerForPopUp.close();
                  },
                  child: Container(
                    height: (40/720) * screenHeight,
                    width: (130/360)*screenWidth,
                    margin: EdgeInsets.only(top: (40/720)*screenHeight,bottom: (97/720)*screenHeight),
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(
                            Radius.circular(2)
                        ),
                        gradient: LinearGradient(
                            end: Alignment(1.0199999809265137, 1.0099999904632568),
                            colors: [SemanticColors.light_purpely,SemanticColors.dark_purpely])
                    ),
                    child: Center(
                      child: Text(
                        GDPIHomeString.Submit,
                        style: TextStyle(
                          color: NeutralColors.pureWhite,
                          fontSize: 14/360*screenWidth,
                          fontFamily: "IBMPlexSans",
                          fontWeight: FontWeight.w500,
                        ),
                        textAlign: TextAlign.center,
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
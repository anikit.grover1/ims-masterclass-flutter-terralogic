

/**WEBVIEW PLAYER**/

// Copyright 2018 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart';
import 'package:imsindia/utils/Url.dart';
import 'package:imsindia/views/Arun/IMSSharedPref.dart';
import 'package:path/path.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:webview_flutter/webview_flutter.dart';
import 'package:imsindia/utils/emaximiser_svg_icons.dart';
import 'package:http/http.dart' as http;
import 'package:connectivity/connectivity.dart';


//void main() => runApp(
//      MaterialApp(
//        home: WebViewExample(),
//        theme: ThemeData(
//          primarySwatch: Colors.red,
//        ),
//      ),
//    );
String userID;
bool blimitCellularDataUsage = false;
class EmaxWebViewAndroid extends StatefulWidget {
  String videoid;
  String userid;
  String showVimeoId;
  EmaxWebViewAndroid(this.videoid,this.userid,this.showVimeoId);

  @override
  _EmaxWebViewExampleState createState() => _EmaxWebViewExampleState();
}

class _EmaxWebViewExampleState extends State<EmaxWebViewAndroid>  with WidgetsBindingObserver {
  TextEditingController _controllerr = TextEditingController();
  String inputString = "";
  final Completer<InAppWebViewController> _controller =
  Completer<InAppWebViewController>();
  InAppWebViewController webViewController;
  String id;

  String get player {
    String _player = '''
    <!DOCTYPE html>
    <html>
    <head>
        <style>
            html,
            body {
                height: 100%;
                width: 100%;
                margin: 0;
                padding: 0;
                background-color: #ffffff;
                overflow: hidden;
                position: fixed;
            }
        </style>
        <meta name='viewport' content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no'>
    </head>
    <body>
     <div style="padding:56.25% 0 0 0;position:relative;">
     <iframe src="https://player.vimeo.com/video/366417284" style="position:absolute;top:height / 2;left:0;width:100%;height:100%;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
     </div><script src="https://player.vimeo.com/api/player.js"></script>
    <!- Your Vimeo SDK player script goes here ->
  </script>
    </body>
    </html>
    ''';
    return 'data:text/html;base64,${base64Encode(const Utf8Encoder().convert(_player))}';
  }

  @override
  void initState() {

    SystemChrome.setPreferredOrientations([
      DeviceOrientation.landscapeRight,
      DeviceOrientation.landscapeLeft,
      DeviceOrientation.portraitUp,
    ]);
    // TODO: implement initState
    print("***************************************123");
    print(widget.showVimeoId);
    print(widget.videoid);
    GetMobileDataStatus_pref();
    super.initState();
    WidgetsBinding.instance.addObserver(this);
  }

  @override
  void dispose() {
    //To reset the screen orientation
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
    ]);
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
    _controller.isCompleted;
  }

  @override
  void deactivate() {
    //To reset the screen orientation
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
    ]);
    super.deactivate();
    webViewController?.reload();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    if (state == AppLifecycleState.paused) {
      webViewController?.reload();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        brightness: Brightness.light,
        elevation: 0.0,
        centerTitle: false,
        titleSpacing: 0.0,
        backgroundColor: Colors.white,
        title: Text(
          '',
          style: const TextStyle(
              color: Colors.black,
              fontWeight: FontWeight.w500,
              fontFamily: "IBMPlexSans",
              fontStyle: FontStyle.normal,
              fontSize: 16.0),
        ),
        leading: IconButton(
          icon: getSvgIcon.backSvgIcon,
          onPressed: () => Navigator.pop(context, false),
        ),
      ),
      body:  InAppWebView(
        initialUrlRequest: URLRequest(url: Uri.parse(widget.showVimeoId!=null ?"https://myims.imsindia.com/test-video-player/?videoId="+widget.showVimeoId :
        "https://ims.quizky.com/vimeo-player/"+widget.videoid+"/ims/"+widget.userid,)),
        
        
        onWebViewCreated: (webViewController) {
          _controller.complete(webViewController);
        },
//        initialOptions: InAppWebViewWidgetOptions(   
//            androidInAppWebViewOptions: AndroidInAppWebViewOptions(
//                hardwareAcceleration: true
//            ),
//            inAppWebViewOptions: InAppWebViewOptions(
//              debuggingEnabled: true,
//              preferredContentMode: InAppWebViewUserPreferredContentMode.MOBILE,
//            )
//        ),
      ),
    );
  }
  // Future<void> function() async {
  //   var res = await http.get("https://player.vimeo.com/video/366417284",headers: {"Authorization":"bearer pl2014268313.1851351634"});
  //   var response = res.body;
  //   //print(response);
  // }

  void GetMobileDataStatus_pref() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    if(prefs.getBool("limitCellularDataUsage") != null){
      blimitCellularDataUsage = prefs.getBool("limitCellularDataUsage");
    }
    print("+++++++++++++++++");
    print(blimitCellularDataUsage);
  }
  Widget LoadDialogifmibiledataactive() {
    return AlertDialog(
      title: Text(''),
      content: const Text(
          'You are about to use mobile data to see the Video'),
      actions: <Widget>[
        FlatButton(
          child: const Text('GO BACK'),
          onPressed: () {
            //Navigator.pop(context);
          },
        ),
        FlatButton(
          child: const Text('ALLOW'),
          onPressed: () {
          },
        )
      ],
    );
  }
}
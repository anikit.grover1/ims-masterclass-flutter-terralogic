import 'package:flutter_tex/flutter_tex.dart';
import 'dart:convert';
import 'package:flutter_html/image_properties.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:imsindia/components/progress_hub.dart';
import 'package:imsindia/resources/strings/prepare.dart';
import 'package:imsindia/routers/routes.dart';
import 'package:imsindia/utils/colors.dart';
import 'package:imsindia/utils/svg_images/prepare_svg_images.dart';
import 'package:imsindia/components/stop_watch_timer.dart';
import 'package:imsindia/views/Emaximiser/emaximizer_introduction.dart';
import 'package:imsindia/views/Emaximiser/emaximizer_statistics_screen.dart';
import 'package:imsindia/views/Emaximiser/emaximizer_show_answer_screen.dart';
import 'package:imsindia/views/Emaximiser/emaximizer_summary_screen.dart';
import 'package:imsindia/views/channel/channel_comments.dart';
import 'dart:async';
import 'package:sliding_up_panel/sliding_up_panel.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:imsindia/utils/Url.dart';
import 'package:imsindia/components/custom_scrollbar_component.dart';
import 'package:imsindia/utils/global.dart' as global;
import 'package:imsindia/api/bloc/services.dart';
import 'package:html/parser.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:imsindia/views/Emaximiser/emaximizer_track_screen.dart';
import 'package:wakelock/wakelock.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_tex/flutter_tex.dart';
import 'package:webview_flutter/webview_flutter.dart';
import 'package:webview_flutter_plus/webview_flutter_plus.dart';
import 'package:f_logs/model/flog/flog.dart';

var screenWidthTotal;
var screenHeightTotal;
var _safeAreaHorizontal;
var _safeAreaVertical;
var screenHeight;
var screenWidth;
String get answerEntered => _answer.text;
var timeIntotalTimeTakenPerQuestion;
final TextEditingController _answer = new TextEditingController();
FocusNode _focusNode_entertext = new FocusNode();
int questionNumberInsideQuestionDropDown = 0;
bool isQuestionDropDownClickchecked = false;
bool checking;
SigObject myObj = new SigObject();
int tracknumberForTrack = 0;
var authToken;
var selectedOptionidaftersubmit;

class EmaximserQuestionScreen  extends StatefulWidget {
  final int emax_header_status;
  final bool emax_sumbit_status;
  final bool timer_status;
  bool emaxStatisticsCheck;
  Stopwatch stopwatch;
  final int backtoques_num;
  final int backtoquesnum_bargraph;
  var image_value;
  var image_value_for_option;
  var pos;
  var statusFromCatch;
  List listOfTopic;
  List listOfId;
  List listOfStatus;
  List listOfObjectType;
  var backgroundColor;
  var positionForTest;
  EmaximserQuestionScreen ({
    this.emax_header_status,
    this.timer_status,
    this.stopwatch,
    this.emax_sumbit_status,
    this.backtoques_num,
    this.backtoquesnum_bargraph,
    this.image_value,
    this.image_value_for_option,
    this.emaxStatisticsCheck,
    this.listOfObjectType,
    this.listOfStatus,
    this.listOfId,
    this.listOfTopic,
    this.statusFromCatch,
    this.pos,
    this.backgroundColor,
    this.positionForTest,
  });

  @override
  EmaximserQuestionScreenState createState() =>
      EmaximserQuestionScreenState();
}

class EmaximserQuestionScreenState
    extends State<EmaximserQuestionScreen > with WidgetsBindingObserver {
  var latestTrackNumber=0;
  var questionIndexInHighestTrack=0;
  var latestTrackNumberForTrack;
  FocusNode _focusNode = new FocusNode();
  var beforeAssign;
  var afterAssign;
  final threeSecond = const Duration(seconds: 3);
  var sync_list = [];
  // Track
  List<String> areaNameForTrack = [];
  bool attemptForTrack;
  String companyCodeForTrack;
  bool slidingPanelTimer;
  List<int> difficultyIdForTrack = [];
  List<String> difficultyNameForTrack = [];
  List<String> enteredTextForTrack = [];
  var idForTrack;
  List<bool> isCorrectForTrack = [];
  var isSectionCompletedForTrack;
  List<int> itemIDForTrack = [];
  List correctAnswerList =[];
  List<String> itemtypeForTrack = [];
  List<String> accessTokenList = [];
  List<String> finalAcessTokenList=[];
  //testIdsList
  List<bool> isMarkedForTrack = [];
  // bool markedForTrack;
  List<dynamic> negaticepointsForTrack = [];
  List<dynamic> pointsForTrack = [];
  int questionindexForTrack;
  var TrackselectedOptionid;
  /// clarify
  var sectionidForTrack;
  /// clarify
  var sectionindexForTrack;
  String sectionNameForTrack;
  var selectedoptionidForTrack = [];
  var finalTotalOptionIDAccordingToQuestionsForTrack = [];
  var studentidForTrack;
  var userNameForLogInfo;
  List<int> subjectIdForTrack = [];
  List<String> subjectNameForTrack = [];
  var testidForTrack;
  int timeRemainingForTrack;

  String timestampForTrack;
  var timetakenForTrack = 0;
  List<int> topicIDForTrack = [];
  List<String> topicnameForTrack = [];
  var savedforTrack;
  var areaIdforTrack;
  //track variable declaration end

  bool timeresumeonprev;
  String hoursStr;
  String minutesStr;
  String secondsStr;
  Stopwatch watch = new Stopwatch();
  Timer timer;
  String elapsedTime = '';
  var obj;
  bool EnterResponse = false;

  bool stackToView = false;



  WebViewController controllerForWebView;
  bool showWebView = false;
  bool loading = true;
  String get colorStr {
    var color;
    print("clor");
    print(widget.backgroundColor);
    if(widget.backgroundColor==null){
      color = Colors.white;
      return '#${color.red.toRadixString(16).padLeft(2, '0')}${color.green.toRadixString(16).padLeft(2, '0')}${color.blue.toRadixString(16).padLeft(2, '0')}';
    }else{
      color = widget.backgroundColor;
      return '#${color.red.toRadixString(16).padLeft(2, '0')}${color.green.toRadixString(16).padLeft(2, '0')}${color.blue.toRadixString(16).padLeft(2, '0')}${color.alpha.toRadixString(16).padLeft(2, '0')}';
    }
  }

  WebViewPlusController webViewPlusController;
  WebViewPlusController webViewPlusController1;
  WebViewPlusController webViewPlusController11;
  WebViewPlusController webViewPlusController2;
  WebViewPlusController webViewPlusControllerShow;
  WebViewPlusController webViewPlusControllerrr;
  double _heightShow = 1;
  double _height = 1;
  double _heightbeforequestion = 1;
  double _heightbeforequestionDirection = 1;
  double _heightbeforequestionOption = 1;
  double _heightbeforequestionParagraph1 = 1;
  double _heightbeforequestionDirection1 = 1;
  double _heightbeforequestionParagraph = 1;
  startWatch() {
    watch.start();
//    print(updateTime);
//    print("updateTimeupdateTimeupdateTime");
    timer = new Timer.periodic(new Duration(milliseconds: 100), updateTime);
  }

  stopWatch() {
    watch.stop();
    setTime();
  }

  resetWatch() {
    watch.reset();
    setTime();
  }

  setTime() {
    var timeSoFar = watch.elapsedMilliseconds;
//    print("before====================");
//    print(elapsedTime);
//    print(totalTimeTakenPerQuestions[emaxPositionForQuestion-1]);
//    print("before====================");
    setState(() {
      //elapsedTime = transformMilliSeconds(timeSoFar);
      if(totalTimeTakenPerQuestions[emaxPositionForQuestion-1].runtimeType==int){
        elapsedTime = transformMilliSeconds(timeSoFar);
      }else{
        var s=totalTimeTakenPerQuestions[emaxPositionForQuestion-1].split(":");
        //elapsedTime = transformMilliSeconds((_getDuration(int.parse(s[0]),int.parse(s[1]),int.parse(s[2])))*1000);
        if(slidingPanelTimer==true){
          elapsedTime=elapsedTime;
        }else{
          elapsedTime = transformMilliSeconds((_getDuration(int.parse(s[0]),int.parse(s[1]),int.parse(s[2])))*1000);
        }
      }
    });
//    print('setting time');
//    print("---+++++++++_____-"+elapsedTime);
  }

  updateTime(Timer timer) {
    if (watch.isRunning) {
      if (this.mounted) {
        setState(() {
          //elapsedTime = transformMilliSeconds(watch.elapsedMilliseconds);
          if (totalTimeTakenPerQuestions[emaxPositionForQuestion - 1]
              .runtimeType == int) {
            elapsedTime = transformMilliSeconds(watch.elapsedMilliseconds +
                totalTimeTakenPerQuestions[emaxPositionForQuestion - 1]);
          } else {
            var s = totalTimeTakenPerQuestions[emaxPositionForQuestion - 1]
                .split(":");
            //elapsedTime = transformMilliSeconds((_getDuration(int.parse(s[0]),int.parse(s[1]),int.parse(s[2])))*1000);
            elapsedTime = transformMilliSeconds((watch.elapsedMilliseconds) +
                (_getDuration(
                    int.parse(s[0]), int.parse(s[1]), int.parse(s[2]))) * 1000);
          }
        });
      }
      // print("----------"+elapsedTime);
    }
  }

  transformMilliSeconds(int milliseconds) {
    int seconds = (milliseconds / 1000).truncate();
    int minutes = (seconds / 60).truncate();
    int hours = (minutes / 60).truncate();
    int days = (hours / 24).truncate();
    minutesStr = (minutes % 60).toString().padLeft(2, '0');
    secondsStr = (seconds % 60).toString().padLeft(2, '0');
    hoursStr = (hours % 60).toString().padLeft(2, '0');
    String daysStr = (days % 24).toString().padLeft(2, '0');
    return "$hoursStr:$minutesStr:$secondsStr";
  }

  /// check box in popup for see answer ///
  checkbox() {
    setState(() {
      if (checkBoxState) {
        Preparequestions.dontShowMsgInSolutionCheckPopup = "Don't show this again";

        checkBoxState = !checkBoxState;
      } else {
        Preparequestions.dontShowMsgInSolutionCheckPopup = " Don't show this again";
        checkBoxState = !checkBoxState;
      }
    });
  }
  /// set flags for answer popup
  setFlagsForAnswerPopUp(value) async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setStringList("testIdForEmax",value);
  }

  /// getting flags for answer popup ///
  getFlagsForAnswerPopUp() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    var sharedValue=prefs.getStringList("testIdForEmax") ?? [];
    print(sharedValue);
    print(sharedValue);
    print("shared value");

    if(sharedValue==null ||sharedValue.toString()=="null"||sharedValue.toString()=="Null"){
      print("going to if conditionnnnnnnnnnnnnnnn");
      accessTokenList=[];
      accessTokenList.add(sharedValue.toString());
    }else if(sharedValue.length==0){
      accessTokenList=[];
      accessTokenList.add(null);
    } else{
      print("going to else condition");
      accessTokenList = sharedValue;
    }

  }


  bool SUB_FLAG = false;
  /// OPTION_ACTIVE : This flag indicate that options of a question is checked or not ///
  bool OPTION_ACTIVE = true;
  /// SUBMIT_ACTIVE : This flag indicate that SUBMIT button of a question is checked or   not ///
  bool SUBMIT_ACTIVE = false;
  /// ANSWER_SUBMITTED : This flag indicate that answer of a question is already
  /// submitted or not if submitted  make ANSWER_SUBMITTED as TRUE ///
  bool ANSWER_SUBMITTED = false;

  bool INDICATED_CARD = false;


  bool isOptionsSelected;
  String answer_status;
  String finalanswer_status;
  double contentHeight = 0;
  double contentHeight1 = 0;
  final Dependencies dependencies = new Dependencies();
  SharedPreferences emaxPrefs;
  bool emaxPopUpForResumeButton = false;
  bool emaxPopUpForExitButton = false;
  bool emaxPopUpForBackButton = false;
  bool emaxPopUpForSolutionIcon = false;
  /// related to show answer popup
  bool checkBoxState = false;
  bool dontshowagain = false;
  bool checkAnswerPopUpOpenFlag;
  bool okay_flag = false;
  var AECorrectAnswer;
  var isCorrectForNext;
  var selectedOptionIdForNext;
  var attemptForNext;
  /// end related to show answer popup


  List emaxBookMarkedQuestionsList = List<String>();
  List emaxSolutionCheckQuestionsList = List<String>();
  List emaxStastisticsCheckQuestionsList = List<String>();
  List<dynamic> emaxQuestionNoTimeList = [];
  var emaxBookMarkedQuestionsAfterSharedPref;
  var emaxSolutionCheckQuestionsAfterSharedPref;
  var emaxStatisticsCheckQuestionsAfterSharedPref;
  int emaxHeader_status_question;
  bool emaxOverLayEntryCheck = false;
  final _emaxControllerForPopUp = PanelController();
  double _emaxCpanelHeightClosed = 0.0;
  bool emaxBookMarkCheck = false;
  bool emaxSoloutionCheck = false;
  bool emaxStatisticsCheck = false;
  int emaxPositionForQuestion = 1, emaxtotalpage;
  static OverlayEntry entry = null;
  bool get isshow => entry != null;
  void show(context) => addOverlayEntry(context);
  void hide() => removeOverlay();
  bool isChecked = false;
  static const _kDuration = const Duration(milliseconds: 300);
  static const _kCurve = Curves.easeIn;
  int currentPage = 1;
  int totalpage;
  int index = 0;
  final ScrollController controller = ScrollController();
  bool iscardchecked = false;
  //final pageViewController = new PageController();
  PageController pageViewController;
  bool questionArrowCheck = false;
  var accessToken;
  var testStatus;
  String nameOfTheTopic;
  var testIdOfTheTopic;
  var totalnumberOfQuestions = 0;
  bool isMigrated;
  var totalQuestions = [];
  var  directionsForQuestion = [];
  var totalOptions = [];
  var answerList = [];
  var finalTotalOptionsAccordingToQuestions = [];
  var finalTotalAnswerListAccordingToQuestion = [];
  var correctAnswerAccordingToQues;
  bool _isLoadingPage;
  var heightOfTheTitleWidget;
  GlobalKey _keyRed = GlobalKey();
  int _selectedIndexForOption;
  int selectedIndexForQues;
  List paragraphCheck = [];
  List paragraphDirection = [];
  Timer _timer;

  /// variables related to track data from prod/test launch api .............///
  List itemIdsInTracks = [];
  List itemIdsForData = [];
  int totalTimeTakenPerQuestion = 0;
  List totalTimeTakenPerQuestions = [];
  List totalNumberOfTracksPerQuestion = [];
  List trackNumbersPerAllQuestions=[];
  List totalTracksForItemIds = [];
  var questionsStatus = [];
  var bookmarkorUnmarkList = [];
  var attemptList = [];
  List selectedOptionIdList = [];
  bool totalTimeCheck;
  /// variables related to track data from prod/test launch api .............///

  List showAnswerExplanation = [];
  List showAnswerVideo = [];
  List areaNameForTrackApi = [];
  List itemIds = [];
  List questionsType = [];
  String filePathForImg =
      'https://imsclick2cert.blob.core.windows.net/imsitemimages/';
  _onSelectedOption(int indexforoption, int indexforques) {

    setState(() {
      _selectedIndexForOption = indexforoption;
      selectedIndexForQues = indexforques;
    });
  }

  var selectedId;
  var selectedStatus;
  var selectedObjectType;
  var selectedvalue;
  var instruction;
  var desc;
  double sizedBoxHeight=0;

  List lettersForQuestions = [
    'A',
    'B',
    'C',
    'D',
    'E',
    'F',
    'G',
    'H',
    'I',
    'J',
    'K',
    'L',
    'M',
    'N'
  ];

  List heightlist = [1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0];

  bool directionBeforeAttemptForWebViewLoader=true;
  bool questionBeforeAttemptForWebViewLoader=true;
  bool optionsBeforeAttemptForWebViewLoader=true;
  bool directionForParagraphForWebViewLoader = true;
  bool paragraphForForWebViewLoader = true;


  void getNextTopicUnlock() {
    print("get next topic unlock");
    print(selectedId.toString());
    if (global.headersWithAuthorizationKey[
    'Authorization'] !=
        '') {
      Map postdata = {
        "testId": selectedId.toString()
      };
      var totalData;
      ApiService()
          .postAPI(
          URL.TEST_LAUNCH_FOR_YES_PREPARE,
          postdata,
          authToken)
          .then((result) {
        print(result);
        print("resultresultresultresultresultresultresultresult");
        // setState(() {
        //    if (result[0] == 'successfully resume') {
        totalData = result[1]['data'];
        var tokenForTestLaunchApi = totalData['token'];

//          } else {
//            print("else part");
//          }
        Map postdata_token = {
          "token": tokenForTestLaunchApi,
        };
        //    widget.statusFromCatch = "In-Progress";


        if (selectedObjectType == "Lesson") {
          selectedObjectType = false;
          Navigator.pop(context,true);
          Navigator.pushReplacement(
            context,
            MaterialPageRoute(
                builder: (context) =>
                    EmaxIntroduction(
                      screen : true,
                      instruction: instruction,
                      selectedStatus: selectedStatus,
                      selectedId: selectedId,
                      selectedvalue: selectedvalue,
                      pos: widget.pos,
                      desc: desc,
                      selectedObjectTypeQ: selectedObjectType,
                      positionForTest: widget.positionForTest + 1,
                    )),

          );
        }
        else {
          print("in else part");
          print(((selectedStatus == "In-Progress") || (selectedStatus == "Start"))
              ?
          URL.GET_QUESTIONS_DATA_FOR_PREPARE
              : URL.EMAX_PROD_REVISE);
          selectedObjectType = true;
          ApiService().postAPI(
              ((selectedStatus == "In-Progress") || (selectedStatus == "Start"))
                  ?
              URL.GET_QUESTIONS_DATA_FOR_PREPARE
                  : URL.EMAX_PROD_REVISE,
              postdata_token,
              authToken)
              .then((result) {
            //  setState(() {
            if (result[0].toString().toLowerCase()== 'success'.toLowerCase()) {
              var totalData = result[1]['data'];
              instruction = totalData['testJson']['Instruction'];
              desc = totalData['testJson']['Description'];
              print("in else part");
              Navigator.pop(context,true);
              Navigator.pushReplacement(
                context,
                MaterialPageRoute(
                    builder: (context) =>
                        EmaxIntroduction(
                          screen : true,
                          instruction: instruction,
                          selectedStatus: selectedStatus,
                          selectedId: selectedId,
                          selectedvalue: selectedvalue,
                          pos: widget.pos,
                          desc: desc,
                          selectedObjectTypeQ: selectedObjectType,
                          positionForTest: widget.positionForTest + 1,
                        )),

              );
            } else {}
            //  });
          });
        }
      });
      // });
    }

    //  dispose();
  }
  List<Widget> _getTiles() {
    final List<Widget> tiles = <Widget>[];

    for (int iteration = 0; iteration < totalnumberOfQuestions; iteration++) {
      tiles.add(new GridTile(
          child: new InkResponse(
              enableFeedback: true,
              onTap: () {
                OPTION_ACTIVE = true;
                SUB_FLAG = false;
                ANSWER_SUBMITTED = false;
                setState(() {
                  emaxQuestionNoTimeList.add([
                    iteration + 1,
                    dependencies.stopwatch.elapsedMilliseconds
                  ]);
                  dependencies.stopwatch.reset();
                  beforeAssign=emaxPositionForQuestion-1;
                  afterAssign=iteration;
                  if(attemptList[beforeAssign]==false){
                    if(totalTimeTakenPerQuestions[emaxPositionForQuestion-1].toString().contains(":")){
                      timeIntotalTimeTakenPerQuestion=totalTimeTakenPerQuestions[emaxPositionForQuestion-1].toString().split(":");
                    }
                    if(attemptList[emaxPositionForQuestion-1]!=true){
                      latestTrackNumber=latestTrackNumber+1;
                    }
                    FLog.info(
                      dataLogType: "Debug",
                      text: latestTrackNumber.toString()+"&&&&&&&&&&&&"+(emaxPositionForQuestion-1).toString()+"&&&&&&&&&"+(subjectNameForTrack[emaxPositionForQuestion-1]).toString()+"&&&&&&&"+(topicnameForTrack[emaxPositionForQuestion-1]).toString()+"&&&&&&&"+userNameForLogInfo.toString()+"&&&&&&"+studentidForTrack.toString()+"======================================================TRACK NUMBER && QUESTION INDEX && SUBJECT NAME && AREA/TOPIC NAME && USER NAME && STUDENT USER ID  WHEN CLICK ON QUESTIONS DROPDOWN @@@@@@@@@ EMAX",
                    );
                    myObj.AddtoSist(Track.Comment(areaNameForTrack[emaxPositionForQuestion-1].toString(),
                        //    attemptForTrack,
                        false,
                        companyCodeForTrack,
                        difficultyIdForTrack[emaxPositionForQuestion-1],
                        difficultyNameForTrack[emaxPositionForQuestion-1],
                        null,
                        companyCodeForTrack+"_"+studentidForTrack+"_"+testidForTrack+"_"+sectionidForTrack.toString()+"_"+
                            itemIDForTrack[emaxPositionForQuestion-1].toString()+"_"+latestTrackNumber.toString(),
                        null,
                        isSectionCompletedForTrack,
                        itemIDForTrack[emaxPositionForQuestion-1],
                        itemtypeForTrack[emaxPositionForQuestion-1],
                        //  isMarkedForTrack[emaximPositionForQuestionForTrack],
                        bookmarkorUnmarkList[emaxPositionForQuestion-1]?true:false, //marked,   //marked
                        negaticepointsForTrack[emaxPositionForQuestion-1],
                        pointsForTrack[emaxPositionForQuestion-1],
                        emaxPositionForQuestion-1,
                        sectionidForTrack,
                        sectionindexForTrack,
                        sectionNameForTrack,
                        null,
                        studentidForTrack,
                        subjectIdForTrack[emaxPositionForQuestion-1],
                        subjectNameForTrack[emaxPositionForQuestion-1],
                        testidForTrack.toString(),
                        timeRemainingForTrack,
                        systemtime,
                        totalTimeTakenPerQuestions[emaxPositionForQuestion-1].toString().contains(":")==true?_getDuration(int.parse(hoursStr),int.parse(minutesStr),int.parse(secondsStr))- _getDuration(int.parse(timeIntotalTimeTakenPerQuestion[0]),int.parse(timeIntotalTimeTakenPerQuestion[1]),int.parse(timeIntotalTimeTakenPerQuestion[2])):_getDuration(int.parse(hoursStr),int.parse(minutesStr),int.parse(secondsStr)),
                        topicIDForTrack[emaxPositionForQuestion-1],
                        topicnameForTrack[emaxPositionForQuestion-1],
                        latestTrackNumber,
                        0,
                        0));
                    myObj.SyncTrackToApi();
                    FLog.info(
                      dataLogType: "Debug",
                      text:  "myObj.SyncTrackToApi()======================== @@@@@@@@ EMAX"+myObj.SyncTrackToApi().toString(),
                    );
                    FLog.info(
                      dataLogType: "Debug",
                      text:  "TRACK GOT SYNC WHEN CLICK ON QUESTIONS DROPDOWN @@@@@@@@@@ EMAX",
                    );
                    bookmarkorUnmarkList[emaxPositionForQuestion-1]=bookmarkorUnmarkList[emaxPositionForQuestion-1]?true:false;
                    if(totalTimeTakenPerQuestions[emaxPositionForQuestion-1].toString().contains(":")){
                      var timeInList=totalTimeTakenPerQuestions[emaxPositionForQuestion-1].toString().split(":");
                      // totalTimeTakenPerQuestions[emaxPositionForQuestion-1]= (durationToSec(((_getDuration(int.parse(hoursStr),int.parse(minutesStr),int.parse(secondsStr))))+(_getDuration(int.parse(timeInList[0]),int.parse(timeInList[1]),int.parse(timeInList[2])))));
                      totalTimeTakenPerQuestions[emaxPositionForQuestion-1]= (durationToSec(((_getDuration(int.parse(hoursStr),int.parse(minutesStr),int.parse(secondsStr))))));
                    }else{
                      totalTimeTakenPerQuestions[emaxPositionForQuestion-1]= (durationToSec((_getDuration(int.parse(hoursStr),int.parse(minutesStr),int.parse(secondsStr)))+totalTimeTakenPerQuestions[emaxPositionForQuestion-1]));
                    }
                  }
                  stopWatch();
                  emaxPositionForQuestion = iteration + 1;

                  questionNumberInsideQuestionDropDown =
                      emaxPositionForQuestion;
                  isQuestionDropDownClickchecked = true;
                  if(emaxHeader_status_question!=1&&emaxHeader_status_question!=2){
                    pageViewController.jumpToPage(emaxPositionForQuestion - 1);
                  }

                  emaxOverLayEntryCheck
                      ? emaxOverLayEntryCheck = false
                      : emaxOverLayEntryCheck = true;
                  hide();

                });
              },
              child: Stack(
                children: <Widget>[
                  Container(
                    decoration: BoxDecoration(
                      border: Border.all(
                          color: (questionsStatus[iteration] == true)
                              ? PrimaryColors.kelly_green
                              : (questionsStatus[iteration] == false)
                              ? PrimaryColors.dark_coral:
                          (questionsStatus[iteration] == null&&attemptList[iteration]==true)
                              ?Colors.yellow
                              : Colors.grey,
                          width: 0.5),
                      boxShadow: [
                        BoxShadow(color: Colors.white
                          // color:Colors.blue,
                        ),
                      ],
                      //  borderSide: const BorderSide(color: Colors.grey, width: 0.0),
                      borderRadius: BorderRadius.all(Radius.circular(6)),
                    ),
                    child: Align(
                      alignment: Alignment.center,
                      child: Text(
                        (iteration + 1).toString(),
                        style: TextStyle(
                          color: Color.fromARGB(255, 0, 3, 44),
                          fontSize: 14 / 360 * screenWidth,
                          fontFamily: "IBMPlexSans",
                          fontWeight: FontWeight.w500,
                        ),
                        textAlign: TextAlign.center,
                      ),
                    ),
                  ),
                  Positioned(
                    top: 0.0,
                    right: 3.0 / 360 * screenWidth,
                    child: Align(
                      alignment: Alignment.topRight,
                      child: Container(
                          margin: EdgeInsets.only(
                              top: (2 / 720) * screenHeight,
                              left: (25 / 360) * screenWidth),
                          child: Align(
                              alignment: Alignment.topRight,
                              child: (bookmarkorUnmarkList[iteration] == true)
                                  ? Image.asset(
                                "assets/images/bookmark-selected.png",
                                height: (11.3 / 720) * screenHeight,
                                width: (7 / 360) * screenWidth,
                                fit: BoxFit.fitHeight,
                                color:
                                (questionsStatus[iteration] == true)
                                    ? PrimaryColors.kelly_green
                                    : (questionsStatus[iteration] ==
                                    false)
                                    ? PrimaryColors.dark_coral
                                    : Colors.yellow,
                              )
                                  : Text(""))),
                    ),
                  ),
                ],
              ))));
    }
    return tiles;
  }

  _getSizes() {
    final RenderBox renderBoxRed = _keyRed.currentContext.findRenderObject();
    heightOfTheTitleWidget = renderBoxRed.size.height;
  }

  addOverlayEntry(context) {
    _getSizes();

    questionArrowCheck = true;
    if (entry != null) return;
    entry = new OverlayEntry(builder: (BuildContext context) {
      return LayoutBuilder(builder: (_, BoxConstraints constraints) {
        return Stack(
          children: <Widget>[
            Positioned(
              top: heightOfTheTitleWidget < 53.87
                  ? 150 / 720 * screenHeight
                  : 173.7 / 720 * screenHeight,
              right: 20 / 360 * screenWidth,
              left: 19 / 360 * screenWidth,
              child: Material(
                color: Colors.white,
                child: Container(
                  width: 320 / 360 * screenWidth,
                  height: totalnumberOfQuestions > 30
                      ? 240 / 720 * screenHeight
                      : null,
                  margin: EdgeInsets.only(top: 8 / 720 * screenHeight),
                  // color: Colors.white,
                  decoration: BoxDecoration(
                    color: Colors.white,
                    //border: Border.all(color: NeutralColors.pureWhite , width: 0.5),
                    boxShadow: [
                      BoxShadow(
                        offset: Offset(0, 5),
                        blurRadius: 10,
                        color: Color(0xffeaeaea),
                        //offset: Offset.lerp(Offset(10.0,-10.0), Offset(10.0,10.0), 1),
                        // offset: Offset(0,10.0),
                        //color: Colors.orange,
                      ),
                    ],
                    borderRadius: BorderRadius.all(Radius.circular(6)),
                  ),
//                  height: 35/720*screenHeight,
//                  width: 35/360*screenWidth,
                  child: Column(
                    children: <Widget>[
                      totalnumberOfQuestions > 30
                          ? Expanded(
                        child: new GridView.count(
                          crossAxisCount: 6,
                          childAspectRatio: (screenHeight / 600),
                          controller: new ScrollController(
                              keepScrollOffset: false),
                          shrinkWrap: true,
                          scrollDirection: Axis.vertical,
                          padding: const EdgeInsets.all(20.0),
                          mainAxisSpacing: 10.0,
                          crossAxisSpacing: 10.0,
                          children: _getTiles(),
                        ),
                      )
                          : new GridView.count(
                        crossAxisCount: 6,
                        childAspectRatio: (screenHeight / 600),
                        controller:
                        new ScrollController(keepScrollOffset: false),
                        shrinkWrap: true,
                        scrollDirection: Axis.vertical,
                        padding: const EdgeInsets.all(20.0),
                        mainAxisSpacing: 10.0,
                        crossAxisSpacing: 10.0,
                        children: _getTiles(),
                      ),
                    ],
                  ),
                ),
              ),
            )
          ],
        );
      });
    });

    addoverlay(entry, context);
  }

  static addoverlay(OverlayEntry entry, context) async {
    Overlay.of(context).insert(entry);
  }

  removeOverlay() {
    questionArrowCheck = false;
    entry?.remove();
    entry = null;
  }

  /// validation for input type text fielld ///
//  String validateEmailOrPin(String value) {
//    Pattern pattern = r'^[0-9]{1,7}$';
//    RegExp regex = new RegExp(pattern);
//    if(regex.hasMatch(value)){
//      OPTION_ACTIVE = true;
//      SUB_FLAG = true;
//      EnterResponse = true;
//    }else{
//      OPTION_ACTIVE = false;
//      SUB_FLAG = false;
//      EnterResponse = false;
//    }
//    return null;
//  }
  String validateInputForQuestions(String value) {
    Pattern pattern;
    if(questionsType[emaxPositionForQuestion-1].toString().toLowerCase()=="NE".toLowerCase()){
      pattern = r'^[0-9-.]+$';

    }else if(questionsType[emaxPositionForQuestion-1].toString().toLowerCase()=="AN".toLowerCase()){
      pattern = r'^[a-zA-Z0-9]+$';

    }else if(questionsType[emaxPositionForQuestion-1].toString().toLowerCase()=="BN".toLowerCase()){
      pattern = r'^[A-Za-z]+$';

    }else{
      pattern = r'^[a-zA-Z0-9-.]{1,50}$';
    }
    ///  ^[a-zA-Z0-9]+$ ---- AN
    /// ^[A-Za-z]+$ ----BN
    /// ^[0-9-.]+$ ------ NE
    RegExp regex = new RegExp(pattern);
    if(regex.hasMatch(value)){
      OPTION_ACTIVE = true;
      EnterResponse = true;
      SUB_FLAG = true;
    }else{
      OPTION_ACTIVE = true;
      SUB_FLAG = false;
      EnterResponse = false;
    }
    return null;
  }


  void _onScroll() {
    isOptionsSelected = false;
  }

  int _getDuration(int hoursStr,int minutesStr,int secondsStr){
    Duration fastestMarathon = new Duration(hours:hoursStr, minutes:minutesStr, seconds:secondsStr);
    //print("=========================================");
    return fastestMarathon.inSeconds;
  }
  String _parseHtmlString(var htmlString) {
    var document = parse(htmlString);

    String parsedString = parse(document.body.text).documentElement.text;

    return parsedString;
  }

  /// getting access token from shared pref ///
  void getAccessTokenForProdTestLaunch() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    accessToken = prefs.getString("accessTokenEmax");
    getAllDataForSolveOrResumeSection();
  }

  /// getting test status for test launch/revise api call ///
  getTestStatusToCallTestLaunchOrReviseApi() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    testStatus = prefs.getString("testStatus");
  }

  /// converting duration to sec
  String durationToSec(int time) {
    Duration duration = new Duration(seconds: time);
    String twoDigits(int n) {
      if (n >= 10) return "$n";
      return "0$n";
    }

    String twoDigitMinutes = twoDigits(duration.inMinutes.remainder(60));
    String twoDigitSeconds = twoDigits(duration.inSeconds.remainder(60));
    return "${twoDigits(duration.inHours)}:$twoDigitMinutes:$twoDigitSeconds";
  }

  /// Prod Test launch Api ////
  getAllDataForSolveOrResumeSection() async {
    if (global.headersWithAuthorizationKey['Authorization'] != '') {
      Map postdata = {"token": accessToken.toString()};
      print(accessToken.toString());
      print("acessss tokennnnnnnnnnnnnnnnnnnnnnnnnnnnnnnn");
      ApiService().postAPI(URL.GET_QUESTIONS_DATA_FOR_PREPARE, postdata, authToken).then((result) {
        setState(() {
          if (result[0].toString().toLowerCase() == 'success'.toLowerCase()) {
            var totalData = result[1]['data'];

            nameOfTheTopic = totalData['testJson']['Name'];
            testIdOfTheTopic = totalData['studentTestData']['testId'];
            totalnumberOfQuestions =
            totalData['testJson']['SectionResponse'][0]['TotalQuestions'];
            if (totalData['studentTestData'].containsKey('isMigrated')) {
              isMigrated = true;
            }

            /// related to test .................................
            for (var i = 0; i < (totalData['testJson']['SectionResponse'][0]['ItemResponse']).length; i++) {
              areaNameForTrackApi.add(totalData['testJson']['SectionResponse'][0]['ItemResponse'][i]['AreaName']);
              itemIds.add(totalData['testJson']['SectionResponse'][0]['ItemResponse'][i]['ItemID']);
              questionsType.add(totalData['testJson']['SectionResponse'][0]['ItemResponse'][i]['ItemType']);
              // Track api data
              savedforTrack = 0;
              areaIdforTrack = 0;
              areaNameForTrack.add(totalData['testJson']['SectionResponse'][0]['ItemResponse'][i]['AreaName']);
              companyCodeForTrack = totalData['studentTestData']['companyCode'];
              attemptForTrack = true;
              difficultyIdForTrack.add(totalData['testJson']['SectionResponse'][0]['ItemResponse'][i]['DifficultyLevel']);
              difficultyNameForTrack.add(totalData['testJson']['SectionResponse'][0]['ItemResponse'][i]['Difficulty'].toString());
              //  print(totalData['testJson']['SectionResponse'][0]['ItemResponse'][i]['EnteredResponse']);
              enteredTextForTrack.add(totalData['testJson']['SectionResponse'][0]['ItemResponse'][i]['EnteredResponse'].toString());
              idForTrack = totalData['studentTestData']['id'];
              itemIDForTrack.add(totalData['testJson']['SectionResponse'][0]['ItemResponse'][i]['ItemID']);
              itemtypeForTrack.add(totalData['testJson']['SectionResponse'][0]['ItemResponse'][i]['ItemType']);
              isMarkedForTrack.add(totalData['testJson']['SectionResponse'][0]['ItemResponse'][i]['IsMarked']);
              negaticepointsForTrack.add(totalData['testJson']['SectionResponse'][0]['ItemResponse'][i]['NegativePoints']);
              pointsForTrack.add(totalData['testJson']['SectionResponse'][0]['ItemResponse'][i]['Points']);
              questionindexForTrack = 0;
              sectionidForTrack = totalData['testJson']['SectionResponse'][0]['QuestionPaperSectionID'];
              sectionindexForTrack = 0;
              sectionNameForTrack = totalData['testJson']['SectionResponse'][0]['SectionTitle'].toString();
              subjectIdForTrack.add(totalData['testJson']['SectionResponse'][0]['ItemResponse'][i]['SubjectID']);
              subjectNameForTrack.add(totalData['testJson']['SectionResponse'][0]['ItemResponse'][i]['SubjectName']);
              testidForTrack = totalData['studentTestData']['testId'];
              studentidForTrack = totalData['studentTestData']['userId'];
              userNameForLogInfo=totalData['userData']['studentName'];
              /**timeRemaining, timeStamp**/
              timeRemainingForTrack = 0;
              timestampForTrack = systemtime;
//              timetakenForTrack.add(
//                  totalData['testJson']['SectionResponse'][0]['ItemResponse'][i]['timetaken']);
              topicIDForTrack.add(totalData['testJson']['SectionResponse'][0]['ItemResponse'][i]['TopicID']);
              topicnameForTrack.add(totalData['testJson']['SectionResponse'][0]['ItemResponse'][i]['TopicName']);
              // end of track api data
              String question = _parseHtmlString(totalData['testJson']['SectionResponse'][0]['ItemResponse'][i]['Items']);
              if(totalData['testJson']['SectionResponse'][0]['ItemResponse'][i]['Direction']!=null&&totalData['testJson']['SectionResponse'][0]['ItemResponse'][i]['Direction']!=" "){

                if (totalData['testJson']['SectionResponse'][0]['ItemResponse'][i]['Direction'].contains('src')) {
                  String img = totalData['testJson']['SectionResponse'][0]['ItemResponse'][i]['Direction'].replaceAll('QUIZKYIMAGEREPO/', filePathForImg);
                  paragraphDirection.add(img);
                } else {
                  paragraphDirection.add(totalData['testJson']['SectionResponse'][0]['ItemResponse'][i]['Direction']);
                }}else{
                paragraphDirection.add(totalData['testJson']['SectionResponse'][0]['ItemResponse'][i]['Direction']);

              }

              if(totalData['testJson']['SectionResponse'][0]['ItemResponse'][i]['Passage']!=null&&totalData['testJson']['SectionResponse'][0]['ItemResponse'][i]['Passage']!=" "){
                if (totalData['testJson']['SectionResponse'][0]['ItemResponse'][i]['Passage'].contains('src')) {
                  String img = totalData['testJson']['SectionResponse'][0]['ItemResponse'][i]['Passage'].replaceAll('QUIZKYIMAGEREPO/', filePathForImg);
                  paragraphCheck.add(img);
                } else {
                  paragraphCheck.add(totalData['testJson']['SectionResponse'][0]['ItemResponse'][i]['Passage']);
                }
              }else{
                paragraphCheck.add(totalData['testJson']['SectionResponse'][0]['ItemResponse'][i]['Passage']);
              }

              if (totalData['testJson']['SectionResponse'][0]['ItemResponse'][i]['Explanation'].contains('src')) {
                if(totalData['testJson']['SectionResponse'][0]['ItemResponse'][i]['Explanation'].contains("@@@~")) {
                  String img = (totalData['testJson']['SectionResponse'][0]['ItemResponse'][i]['Explanation'].split("@@@~"))[0].replaceAll('QUIZKYIMAGEREPO/', filePathForImg);
                  showAnswerExplanation.add(img);
                }
                else{
                  String img = totalData['testJson']['SectionResponse'][0]['ItemResponse'][i]['Explanation'].replaceAll('QUIZKYIMAGEREPO/', filePathForImg);
                  showAnswerExplanation.add(img);
                }
              }
              else {
                if(totalData['testJson']['SectionResponse'][0]['ItemResponse'][i]['Explanation'].contains("@@@~")){
                  showAnswerExplanation.add((totalData['testJson']['SectionResponse'][0]['ItemResponse'][i]['Explanation'].split("@@@~"))[0]);
                }else{
                  showAnswerExplanation.add(totalData['testJson']['SectionResponse'][0]['ItemResponse'][i]['Explanation']);
                }
              }

              if((totalData['testJson']['SectionResponse'][0]['ItemResponse'][i]['VimeoURI'])!= null){
                showAnswerVideo.add(totalData['testJson']['SectionResponse'][0]['ItemResponse'][i]['VimeoURI']);
                //print((totalData['testJson']['SectionResponse'][0]['ItemResponse'][i]['VimeoLink'].split("/"))[1]);
              }
              else
              {
                showAnswerVideo.add("");
              }


              if (totalData['testJson']['SectionResponse'][0]['ItemResponse'][i]['Items'].contains('src')) {
                String img = totalData['testJson']['SectionResponse'][0]['ItemResponse'][i]['Items'].replaceAll('QUIZKYIMAGEREPO/', filePathForImg);
//                if(img.contains("Enter your response")){
//                  var question;
//                  question=(img.split("Enter your response"))[0];
//                  if(question.contains("Rs[quizky-text]")){
//                    totalQuestions.add((question.replaceAll("Rs[quizky-text]","")));
//                  }else if(question.contains("[quizky-text]")){
//                    totalQuestions.add((question.replaceAll("[quizky-text]","")));
//                  }else{
//                    totalQuestions.add((img.split("Enter your response"))[0]);
//                  }
//                }
                if(img.contains("Rs[quizky-text]")){
                  var question=(img.split("Rs[quizky-text]"))[0];
//                  if(question.contains("Enter your response")){
//                    totalQuestions.add((question.split("Enter your response"))[0]);
//                  }else{
                  totalQuestions.add((img.replaceAll("Rs[quizky-text]","")));
//                  }
                }
                else if(img.contains("[quizky-text]")){
                  var question=(img.split("[quizky-text]"))[0];
//                  if(question.contains("Enter your response")){
//                    totalQuestions.add((question.split("Enter your response"))[0]);
//                  }else{
                  totalQuestions.add((img.replaceAll("[quizky-text]","")));
                  //}
                }
                else{
                  totalQuestions.add(img);
                }
              } else {
//                if(totalData['testJson']['SectionResponse'][0]['ItemResponse'][i]['Items'].contains("Enter your response")){
//                  var question;
//                  question=(totalData['testJson']['SectionResponse'][0]['ItemResponse'][i]['Items'].split("Enter your response"))[0];
//                  if(question.contains("Rs[quizky-text]")){
//                    totalQuestions.add((question.replaceAll("Rs[quizky-text]","")));
//                  }
//                  else if(question.contains("[quizky-text]")){
//                    totalQuestions.add((question.replaceAll("[quizky-text]","")));
//                  }
//                  else{
//                    totalQuestions.add((totalData['testJson']['SectionResponse'][0]['ItemResponse'][i]['Items'].split("Enter your response"))[0]);
//                  }
//                }
                if(totalData['testJson']['SectionResponse'][0]['ItemResponse'][i]['Items'].contains("Rs[quizky-text]")){
                  var question=(totalData['testJson']['SectionResponse'][0]['ItemResponse'][i]['Items'].split("Rs[quizky-text]"))[0];
//                  if(question.contains("Enter your response")){
//                    totalQuestions.add((question.split("Enter your response"))[0]);
//                  }else{
                  totalQuestions.add((totalData['testJson']['SectionResponse'][0]['ItemResponse'][i]['Items'].replaceAll("Rs[quizky-text]","")));
//                  }
                }
                else if(totalData['testJson']['SectionResponse'][0]['ItemResponse'][i]['Items'].contains("[quizky-text]")){
                  var question=(totalData['testJson']['SectionResponse'][0]['ItemResponse'][i]['Items'].replaceAll("[quizky-text]",""));
//                  if(question.contains("Enter your response")){
//                    totalQuestions.add((question.split("Enter your response"))[0]);
//                  }else{
                  totalQuestions.add((totalData['testJson']['SectionResponse'][0]['ItemResponse'][i]['Items'].replaceAll("[quizky-text]","")));
//                  }
                }
                else{
                  totalQuestions.add(totalData['testJson']['SectionResponse'][0]['ItemResponse'][i]['Items']);
                }
              }
              if(totalData['testJson']['SectionResponse'][0]['ItemResponse'][i]['Direction']!=null&&totalData['testJson']['SectionResponse'][0]['ItemResponse'][i]['Direction']!=" "&&totalData['testJson']['SectionResponse'][0]['ItemResponse'][i]['Direction']!=""){

                if (totalData['testJson']['SectionResponse'][0]['ItemResponse'][i]['Direction'].contains('src')) {
                  String img = totalData['testJson']['SectionResponse'][0]['ItemResponse'][i]['Direction'].replaceAll('QUIZKYIMAGEREPO/', filePathForImg);
                  directionsForQuestion.add(img);
                } else {
                  directionsForQuestion.add(totalData['testJson']['SectionResponse'][0]['ItemResponse'][i]['Direction']);
                }}
              else{
                directionsForQuestion.add(totalData['testJson']['SectionResponse'][0]['ItemResponse'][i]['Direction']);

              }
              for (var j = 0; j < totalData['testJson']['SectionResponse'][0]['ItemResponse'][i]['ItemOptionResponse'].length; j++) {
                if (totalData['testJson']['SectionResponse'][0]['ItemResponse'][i]['ItemOptionResponse'][j]['Options']
                    .contains('src')) {
                  String img = totalData['testJson']['SectionResponse'][0]['ItemResponse'][i]['ItemOptionResponse'][j]['Options'].replaceAll('QUIZKYIMAGEREPO/', filePathForImg);
                  totalOptions.add(img);
                } else {
                  totalOptions.add(totalData['testJson']['SectionResponse'][0]['ItemResponse'][i]['ItemOptionResponse'][j]['Options']);
                }
                selectedoptionidForTrack.add(totalData['testJson']['SectionResponse'][0]['ItemResponse'][i]['ItemOptionResponse'][j]['ItemOptionID']);
                // for track
                isCorrectForTrack.add(totalData['testJson']['SectionResponse'][0]['ItemResponse'][i]['ItemOptionResponse'][j]['IsCorrect']);
                isSectionCompletedForTrack = null;
                // track end
                answerList.add(totalData['testJson']['SectionResponse'][0]['ItemResponse'][i]['ItemOptionResponse'][j]['IsCorrect']);
              }
              finalTotalOptionIDAccordingToQuestionsForTrack.add(selectedoptionidForTrack);
              finalTotalOptionsAccordingToQuestions.add(totalOptions);
              finalTotalAnswerListAccordingToQuestion.add(answerList);
              selectedoptionidForTrack = [];
              totalOptions = [];
              answerList = [];
            }
            /// to get the correct answer ///
            for(var i=0 ; i<questionsType.length;i++){
              for(var j=0; j < totalData['testJson']['SectionResponse'][0]['ItemResponse'][i]['ItemOptionResponse'].length; j++){
                if(totalData['testJson']['SectionResponse'][0]['ItemResponse'][i]['ItemOptionResponse'][j]['IsCorrect']==true){
                  if (totalData['testJson']['SectionResponse'][0]['ItemResponse'][i]['ItemOptionResponse'][j]['Options'].contains('src')) {
                    String img = totalData['testJson']['SectionResponse'][0]['ItemResponse'][i]['ItemOptionResponse'][j]['Options'].replaceAll('QUIZKYIMAGEREPO/', filePathForImg);
                    correctAnswerList.add(img);
                  } else {
                    correctAnswerList.add(totalData['testJson']['SectionResponse'][0]['ItemResponse'][i]['ItemOptionResponse'][j]['Options']);
                  }
                }
              }
            }
            if(paragraphCheck[emaxPositionForQuestion-1]!=null){
              checking =null;
            }else{
              checking =false;
            }
            /// --------------------------------- related to resume thing in prod test launch api ---------------------------------- ///
            var totalTracks = totalData["tracks"];
            if (totalData.containsKey("tracks")) {
              for (var i = 0; i < totalTracks.length; i++) {
                if (!itemIdsInTracks.contains(totalTracks[i]['itemId'])) {
                  itemIdsInTracks.add(totalTracks[i]['itemId']);
                }
              }
              for (var j = 0; j < itemIds.length; j++) {
                if (itemIdsInTracks.contains(itemIds[j].toString())) {
                  itemIdsForData.add(itemIds[j]);
                } else {
                  itemIdsForData.add(null);
                }
              }
              for (var tracksAttempt = 0; tracksAttempt < itemIdsForData.length; tracksAttempt++) {
                if (itemIdsForData[tracksAttempt] != null) {
                  for (var j = 0; j < totalTracks.length; j++) {
                    if (totalData['testJson']['SectionResponse'][0]['ItemResponse'][tracksAttempt]['ItemID'].toString() == totalTracks[j]['itemId']) {
                      totalTimeTakenPerQuestion = totalTimeTakenPerQuestion + totalTracks[j]["timeTaken"];
                      totalTimeCheck = true;
                      totalNumberOfTracksPerQuestion.add(totalTracks[j]['trackNumber']);
                      totalNumberOfTracksPerQuestion.sort();
                    }
                  }
                  /// to get the latest track number ///
                  trackNumbersPerAllQuestions.add(totalNumberOfTracksPerQuestion);
                  for (var track = 0; track < totalNumberOfTracksPerQuestion.length; track++) {
                    if (track == totalNumberOfTracksPerQuestion.length - 1) {
                      for (var i = 0; i < totalTracks.length; i++) {
                        if (totalNumberOfTracksPerQuestion[totalNumberOfTracksPerQuestion.length - 1] == totalTracks[i]['trackNumber'] && totalTracks[i]['itemId'] == itemIdsForData[tracksAttempt].toString()) {
                          if(latestTrackNumber<totalNumberOfTracksPerQuestion[totalNumberOfTracksPerQuestion.length - 1]){
                            latestTrackNumber=totalNumberOfTracksPerQuestion[totalNumberOfTracksPerQuestion.length - 1];
                            questionIndexInHighestTrack = totalTracks[i]['questionIndex'];
                          }else{
                            latestTrackNumber=latestTrackNumber;
                          }
                          questionsStatus.add(totalTracks[i]["isCorrect"]);
                          bookmarkorUnmarkList.add(totalTracks[i]["marked"]);
                          attemptList.add(totalTracks[i]["attempt"]);
                          // totalTimeTakenPerQuestions.add(durationToSec(totalTracks[i]["timeTaken"]));
                          if (totalTracks[i]["isCorrect"] == null && (totalTracks[i]["selectedOptionId"]==0||totalTracks[i]["selectedOptionId"]==null||totalTracks[i]["selectedOptionId"].toString()=='0') ) {
                            selectedOptionIdList.add("-1");
                          } else if (totalTracks[i]["itemType"] != "MCQ" && totalTracks[i]["itemType"] != "mcq"&& totalTracks[i]["itemType"] != null) {
                            selectedOptionIdList.add(totalTracks[i]["enteredText"]);
                          } else if((totalTracks[i]["enteredText"]!="null"||totalTracks[i]["enteredText"]!=null)&&(totalTracks[i]["selectedOptionId"]!='0'||totalTracks[i]["selectedOptionId"]!=null)){
                            selectedOptionIdList.add(totalTracks[i]["selectedOptionId"]);
                          }else {
                            for (var k = 0; k < totalData['testJson']['SectionResponse'][0]['ItemResponse'][tracksAttempt]['ItemOptionResponse'].length; k++) {
                              if (totalTracks[i]["selectedOptionId"] == (totalData['testJson']['SectionResponse'][0]['ItemResponse'][tracksAttempt]['ItemOptionResponse'][k]['ItemOptionID']).toString()) {
                                selectedOptionIdList.add(totalTracks[i]["selectedOptionId"]);
                              }
                              else{
                                //selectedOptionIdList.add(totalTracks[i]["selectedOptionId"]);
                              }

                            }
                          }
                          break;
                        }
                        else {}
                      }
                    }
                  }
                  totalNumberOfTracksPerQuestion = [];
                  if (totalTimeCheck) {
                    totalTimeTakenPerQuestions.add(durationToSec(totalTimeTakenPerQuestion));
                    totalTimeTakenPerQuestion = 0;
                  }
                  totalTimeCheck = false;
                }
                else {
                  attemptList.add(false);
                  questionsStatus.add("-1");
                  bookmarkorUnmarkList.add(false);
                  selectedOptionIdList.add("-2");
                  totalTimeTakenPerQuestions.add(0);
                }
              }

            }
            /// loading page for questions ////
            if(isMigrated==true){
              pageViewController = PageController(
                  keepPage: true,
                  initialPage: 0)
                ..addListener(_onScroll);
            }else{
              pageViewController = PageController(
                  keepPage: true,
                  initialPage: questionIndexInHighestTrack)
                ..addListener(_onScroll);
            }
            setState(() {
              if(isMigrated==true){
                emaxPositionForQuestion= 1;
              }else{
                emaxPositionForQuestion= questionIndexInHighestTrack+1;
              }
              /// end loading page for questions ////
            });
            // print("printinggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggg");
            //  print(finalTotalOptionsAccordingToQuestions[9]);
            //            print(selectedOptionIdList);
//            print(selectedOptionIdList.length);
//            print(attemptList);
//            print(attemptList.length);F
//            print(questionsStatus);
//            print(questionsStatus.length);
//            print(bookmarkorUnmarkList);
//            print(bookmarkorUnmarkList.length);
//            print(totalnumberOfQuestions);
//            print(totalTimeTakenPerQuestions);
//            print(totalTimeTakenPerQuestions.length);
//            print(latestTrackNumber);
//            print("selectedOptionIdListselectedOptionIdListselectedOptionIdList");
            startWatch();
            // print("printinggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggg");
          } else {}
          //print("===============================ADDTRACKWHILELOADING"+tracknumberForTrack.toString() + latestTrackNumber.toString() );
          // latestTrackNumber = latestTrackNumber++;
          // print("++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
//          print(latestTrackNumber);
//          print(latestTrackNumber++);
          //   print("latestTrackNumberlatestTrackNumberlatestTrackNumber");
          if(attemptList[emaxPositionForQuestion-1]!=true){
            latestTrackNumber=latestTrackNumber+1;
            FLog.info(
              dataLogType: "Debug",
              text: latestTrackNumber.toString()+"&&&&&&&&&&&&"+(emaxPositionForQuestion-1).toString()+"&&&&&&&&&"+(subjectNameForTrack[emaxPositionForQuestion-1]).toString()+"&&&&&&&"+(topicnameForTrack[emaxPositionForQuestion-1]).toString()+"&&&&&&&"+userNameForLogInfo.toString()+"&&&&&&"+studentidForTrack.toString()+"======================================================TRACK NUMBER && QUESTION INDEX && SUBJECT NAME && AREA/TOPIC NAME && USER NAME && STUDENT USER ID  WHILE LOADING FIRST TIME/INIT STATE @@@@@@ EMAX",
            );
            myObj.AddtoSist(Track.Comment(areaNameForTrack[emaxPositionForQuestion-1].toString(),
                //    attemptForTrack,
                false,
                companyCodeForTrack,
                difficultyIdForTrack[emaxPositionForQuestion-1],
                difficultyNameForTrack[emaxPositionForQuestion-1],
                null,
                companyCodeForTrack+"_"+studentidForTrack+"_"+testidForTrack+"_"+sectionidForTrack.toString()+"_"+
                    itemIDForTrack[emaxPositionForQuestion-1].toString()+"_"+latestTrackNumber.toString(),
                null,
                isSectionCompletedForTrack,
                itemIDForTrack[emaxPositionForQuestion-1],
                itemtypeForTrack[emaxPositionForQuestion-1],
                //  isMarkedForTrack[emaximPositionForQuestionForTrack],
                false,
                negaticepointsForTrack[emaxPositionForQuestion-1],
                pointsForTrack[emaxPositionForQuestion-1],
                emaxPositionForQuestion-1,
                sectionidForTrack,
                sectionindexForTrack,
                sectionNameForTrack,
                null,
                studentidForTrack,
                subjectIdForTrack[emaxPositionForQuestion-1],
                subjectNameForTrack[emaxPositionForQuestion-1],
                testidForTrack.toString(),
                timeRemainingForTrack,
                timestampForTrack,
                timetakenForTrack,
                topicIDForTrack[emaxPositionForQuestion-1],
                topicnameForTrack[emaxPositionForQuestion-1],
                latestTrackNumber,
                0,
                0));
            myObj.SyncTrackToApi();
            FLog.info(
              dataLogType: "Debug",
              text:  "myObj.SyncTrackToApi()======================== @@@@@@@@ EMAX"+myObj.SyncTrackToApi().toString(),
            );
            FLog.info(
              dataLogType: "Debug",
              text:  "TRACK GOT SYNC WHILE LOADING FIRST TIME/INIT STATE @@@@@@@@ EMAX",
            );
          }



          new Timer.periodic(threeSecond, (Timer t) async {
//            print("========================updating");
//            print("==============stage1");
//            print(myObj.result);
            sync_list = myObj.result;
//            print(sync_list);
//            print(sync_list.length);
            if(sync_list!=null){
              FLog.info(
                  dataLogType: "Debug",
                  text:sync_list.length.toString()+"============================================================================SYNC LIST LENGTH IN TIMER FUNCTION @@@@@@@@@ EMAX "
              );
              for(int i=0;i<sync_list.length;i++){
                FLog.info(
                    dataLogType: "Debug",
                    text:i.toString()+"&&&&&&"+sync_list[i].id.toString()+"============================================================================I VALUE AND ID IN SYNC LIST IN TIMER @@@@@@@@@@@ EMAX"
                );

              }

              syncData();
            }


          });
        });
      });
    }
  }
  /// end test Api ///
  endTestApi() async{
    if (global.headersWithAuthorizationKey['Authorization'] != '') {
      Map postdata = {"token": accessToken.toString()};
      ApiService().postAPI(URL.END_TEST_API_FOR_PREPARE, postdata, authToken).then((result) {
        if (this.mounted) {
          setState(() {
            // print(result[0]);
            if (result[0].toString().toLowerCase() == 'Successfully submitted'.toLowerCase()) {
              getFlagsForAnswerPopUp().then((_){
                finalAcessTokenList= accessTokenList;
                if(finalAcessTokenList.contains(accessToken.toString())){
                  finalAcessTokenList.remove(accessToken.toString());
                }
                for(var i=0;i<finalAcessTokenList.length;i++){
                  if(finalAcessTokenList[i]==null){
                    finalAcessTokenList[i]="";
                  }
                }
                setFlagsForAnswerPopUp(finalAcessTokenList) ;
              });
              for (int z = 0; z < widget.listOfTopic.length - 1; z++) {
                if (nameOfTheTopic == widget.listOfTopic[z]&&widget.listOfId[z]==testIdOfTheTopic) {
                  selectedvalue = widget.listOfTopic[z + 1];
                  selectedId = widget.listOfId[z + 1];
                  selectedStatus = widget.listOfStatus[z + 1];
                  selectedObjectType = widget.listOfObjectType[z + 1];
                }
              }
              getNextTopicUnlock();
            } else {}
          });
        }
      });
    }
  }

  var systemtime;
  @override
  void initState() {
    _focusNode_entertext.addListener(_ensureVisible);
    WidgetsBinding.instance.addObserver(this);
    _isLoadingPage = true;
    _answer.clear();
    widget.emaxStatisticsCheck = false;
    /// Enable Screen Lock Off
    Wakelock.enable();

    // TODO: implement initState
    emaxHeader_status_question = widget.emax_header_status;
    super.initState();
    global.getToken.then((t){
      authToken=t;
      getAccessTokenForProdTestLaunch();

    });

    /// listener for text form field ///
    _answer.addListener(() {
      setState(() {
        validateInputForQuestions(_answer.text);
      });
    });

    getTimeStamp();
  }

  @override
  void dispose(){
    _focusNode_entertext.removeListener(_ensureVisible);
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
    _timer.cancel();
  }
  @override
  void didChangeMetrics(){
    if (_focusNode_entertext.hasFocus){
      _focusNodeListener();
    }
  }
  Future<Null> _focusNodeListener() async {
    if (_focusNode_entertext.hasFocus){
      print('TextField got the focus');
    } else {
      print('TextField lost the focus');
    }
  }

  Future<Null> _keyboardToggled() async {
    if (mounted){
      EdgeInsets edgeInsets = MediaQuery.of(context).viewInsets;
      while (mounted && MediaQuery.of(context).viewInsets == edgeInsets) {
        await new Future.delayed(const Duration(milliseconds: 10));
      }
    }

    return;
  }

  Future<Null> _ensureVisible() async {
    // Wait for the keyboard to come into view
    await Future.any([new Future.delayed(const Duration(milliseconds: 300)), _keyboardToggled()]);

    // No need to go any further if the node has not the focus
    if (!_focusNode_entertext.hasFocus){
      return;
    }

    // Find the object which has the focus
    final RenderObject object = context.findRenderObject();
    final RenderAbstractViewport viewport = RenderAbstractViewport.of(object);

    // If we are not working in a Scrollable, skip this routine
    if (viewport == null) {
      return;
    }

    // Get the Scrollable state (in order to retrieve its offset)
    ScrollableState scrollableState = Scrollable.of(context);
    assert(scrollableState != null);

    // Get its offset
    ScrollPosition position = scrollableState.position;
    double alignment;

    if (position.pixels > viewport.getOffsetToReveal(object, 0.0).offset) {
      // Move down to the top of the viewport
      alignment = 0.0;
    } else if (position.pixels < viewport.getOffsetToReveal(object, 1.0).offset){
      // Move up to the bottom of the viewport
      alignment = 1.0;
    } else {
      // No scrolling is necessary to reveal the child
      return;
    }

    position.ensureVisible(
      object,
      alignment: alignment,
      //duration: widget.duration,
      //curve: widget.curve,
    );
  }


  void getTimeStamp() {
    var moonLanding = DateTime.now();
    String systemDate = moonLanding.toUtc().toString().split(" ")[0];
    String systemTime = moonLanding.toUtc().toString().split(" ")[1].toString().split(".")[0];
    String lastZstring = moonLanding.toUtc().toString().split(" ")[1].toString().split(".")[1];
    String thridDigit = lastZstring.substring(0,3)+"Z";
    // print("=============="+systemDate+"================"+systemTime+"==========="+lastZstring+"=========="+thridDigit);
    systemtime = systemDate+"T"+systemTime+"."+thridDigit;
    // print("***********************************123");
    // print(emaxPositionForQuestion);
    //print(systemtime);
  }

  panelSlide() {
    if (widget.stopwatch == null) {
      dependencies.stopwatch.start();
    } else if (widget.stopwatch != null) {
      dependencies.stopwatch = widget.stopwatch;
      dependencies.stopwatch.stop();
    }
  }

  Future<void> executeAfterBuild() async {
    if (widget.stopwatch == null) {
      dependencies.stopwatch.start();
    } else if (widget.stopwatch != null) {
      dependencies.stopwatch = widget.stopwatch;
      dependencies.stopwatch.stop();
    }
    // this code will get executed after the build method
    // because of the way async functions are scheduled
  }

  Widget build(BuildContext context) {
    var _mediaQueryData = MediaQuery.of(context);
    screenWidthTotal = _mediaQueryData.size.width;
    screenHeightTotal = _mediaQueryData.size.height;
    _safeAreaHorizontal =
        _mediaQueryData.padding.left + _mediaQueryData.padding.right;
    _safeAreaVertical =
        _mediaQueryData.padding.top + _mediaQueryData.padding.bottom;
    screenHeight = screenHeightTotal - _safeAreaVertical;
    screenWidth = screenWidthTotal - _safeAreaHorizontal;
    Future<bool> _onWillPop() {
      setState(() {
        // popUpCheck = false;
        emaxPopUpForBackButton = true;
        emaxOverLayEntryCheck
            ? emaxOverLayEntryCheck = false
            : emaxOverLayEntryCheck = true;
        hide();
        FocusScope.of(context).requestFocus(new FocusNode());

      });
      if (_emaxControllerForPopUp.isPanelClosed) {
        _emaxControllerForPopUp.open();
      } else {
        _emaxControllerForPopUp.close();
      }
    }

    return WillPopScope(
      onWillPop: _onWillPop,
      child: Scaffold(
        body: new SafeArea(
          top: false,
          bottom: false,
          child: SingleChildScrollView(
            //color: Colors.white,
            child: SlidingUpPanel(
              onPanelClosed: () => setState(() {
                if( emaxPopUpForResumeButton==true){
                  slidingPanelTimer=false;
                  startWatch();
                }
                emaxPopUpForExitButton=false;
                emaxPopUpForBackButton=false;
                emaxPopUpForResumeButton=false;
                emaxPopUpForSolutionIcon=false;
              }),
              onPanelSlide: (double pos) => setState(() {}),
              onPanelOpened: () => setState(() {
                if( emaxPopUpForResumeButton==true){
                  slidingPanelTimer = true;
                  // totalTimeTakenPerQuestions[preparePositionForQuestion-1]=elapsedTime;
                  stopWatch();
                }
              }),
              maxHeight: emaxPopUpForSolutionIcon==true?screenHeight * 0.45:screenHeight * .75,
              minHeight: _emaxCpanelHeightClosed,
              parallaxEnabled: false,
              parallaxOffset: 0.5,
              //defaultPanelState: PanelState.CLOSED,
              backdropEnabled: true,
              backdropTapClosesPanel: true,
              isDraggable: true,
              body: _body(context),
              panel:  emaxPopUpForBackButton == true
                  ? (_popUpForBackButton(context))
                  :  emaxPopUpForResumeButton == true
                  ? (_popUpForPauseButton(context))
                  : emaxPopUpForExitButton==true?(_popUpForExitButton(context)): emaxPopUpForSolutionIcon == true ?
              (_popUpForAnswerButton(context))  :null,
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(25.0),
                  topRight: Radius.circular(25.0)),
              controller: _emaxControllerForPopUp,
            ),
          ),
        ),
      ),
    );
  }

  Widget _body(context) {
    return Container(
      color: Colors.white,
      constraints: BoxConstraints.expand(),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          totalQuestions.length == 0
              ? Container()

          /// title  and timer ..................... ////
              : GestureDetector(
            onTap: () {
              setState(() {
                emaxOverLayEntryCheck
                    ? emaxOverLayEntryCheck = false
                    : emaxOverLayEntryCheck = true;
                hide();
                FocusScope.of(context).requestFocus(new FocusNode());

              });
            },
            child: Container(
              //  height: 54 / 720 * screenHeight,
              width: double.infinity,
              margin: EdgeInsets.only(top: 38 / 720 * screenHeight),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  /// back arrow .............. ///
                  InkWell(
                    onTap: () {
                      setState(() {
                        //popUpCheck = false;

                        emaxPopUpForBackButton = true;
                        emaxOverLayEntryCheck
                            ? emaxOverLayEntryCheck = false
                            : emaxOverLayEntryCheck = true;
                        hide();
                        FocusScope.of(context).requestFocus(new FocusNode());

                      });
                      if (_emaxControllerForPopUp.isPanelClosed) {
                        _emaxControllerForPopUp.open();
                      } else {
                        _emaxControllerForPopUp.close();
                      }
                    },
                    child: Container(
                      height: (14 / 678) * screenHeight,
                      width: (16 / 360) * screenWidth,
                      margin: EdgeInsets.only(
                          left: 18 / 360 * screenWidth,
                          top: 12.5 / 720 * screenHeight),
                      child: SvgPicture.asset(
                        getPrepareSvgImages.backIcon,
                        fit: BoxFit.fill,
                      ),
                    ),
                  ),

                  /// title ........... ///
                  Container(
                    key: _keyRed,
                    width: 200 / 360 * screenWidth,
                    margin: EdgeInsets.only(
                        left: 12 / 360 * screenWidth,
                        top: 10 / 720 * screenHeight),
                    child: Text(
                      nameOfTheTopic == null
                          ? "Emaximizer"
                          : nameOfTheTopic.toString(),
                      maxLines: 3,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(
                        color: NeutralColors.black,
                        fontSize: 18 / 360 * screenWidth,
                        fontFamily: "IBMPlexSans",
                        fontWeight: FontWeight.w700,
                      ),
                      textAlign: TextAlign.left,
                    ),
                  ),
                  (optionsBeforeAttemptForWebViewLoader==false||directionBeforeAttemptForWebViewLoader==false||questionBeforeAttemptForWebViewLoader==false)?
                  /// timer and resume button ........ ///
                  attemptList[ emaxPositionForQuestion - 1] == true
                  /// after question attempts
                      ? Container(
                    width: 73 / 360 * screenWidth,
                    margin: EdgeInsets.only(
                      top: 10 / 720 * screenHeight,
                      left: 20 / 360 * screenWidth,
                    ),
                    child: Column(
                      crossAxisAlignment:
                      CrossAxisAlignment.end,
                      children: <Widget>[
                        Container(
                          child: Text(
                            "Time Taken",
                            style: TextStyle(
                              color: NeutralColors.dark_navy_blue,
                              fontSize: 12 / 360 * screenWidth,
                              fontFamily: "IBMPlexSans",
                              fontWeight: FontWeight.w500,
                            ),
                            textAlign: TextAlign.left,
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(
                              left: 10 / 360 * screenWidth,
                              bottom: 0.0),
                          child: Center(
                            child: Text(
                              totalTimeTakenPerQuestions[emaxPositionForQuestion - 1].toString(),
                              style: TextStyle(
                                color: NeutralColors.dark_navy_blue,
                                fontSize: 12 / 360 * screenWidth,
                                fontFamily: "IBMPlexSans",
                                fontWeight: FontWeight.w500,
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  )
                  /// before question attempts
                      : Container(
                    width: 73 / 360 * screenWidth,
                    height: 30.0 / 720 * screenHeight,
                    margin: EdgeInsets.only(
                      top: 10 / 720 * screenHeight,
                      left: 20 / 360 * screenWidth,
                    ),
                    child: Row(
                      crossAxisAlignment:
                      CrossAxisAlignment.stretch,
                      children: <Widget>[
                        GestureDetector(
                          child: Container(
                            child: Image.asset(
                              "assets/images/ic-pause-24px.png",
                              height: 0.02 * screenHeight,
                              width: 0.02 * screenWidth,
                              fit: BoxFit.scaleDown,
                            ),
                          ),
                          onTap: () {
                            setState(() {
                              emaxPopUpForResumeButton = true;
                              dependencies.stopwatch.stop();
                              hide();
                              FocusScope.of(context).requestFocus(new FocusNode());
                              emaxOverLayEntryCheck
                                  ? emaxOverLayEntryCheck = false
                                  : emaxOverLayEntryCheck = true;
                              //questionsScreen.buttonstatus=false;
                            });
                            _emaxControllerForPopUp.open();
                          },
                        ),
                        Container(
                          margin: EdgeInsets.only(
                              left: 10 / 360 * screenWidth,
                              bottom: 0.0),
                          child: Center(
                            child: Text(
                              //     (timeresumeonprev==true)?totalTimeTakenPerQuestions[emaxPositionForQuestion-1]+elapsedTime:

                              elapsedTime,
                              style: TextStyle(
                                color: NeutralColors.dark_navy_blue,
                                fontSize: 12 / 360 * screenWidth,
                                fontFamily: "IBMPlexSans",
                                fontWeight: FontWeight.w500,
                              ),
                            ),

//                                  child: TimerText(
//                                    dependencies: dependencies,
//                                  ),
                          ),
                        ),
                      ],
                    ),
                  )
                      :Text(""),
                ],
              ),
            ),
          ),
          totalQuestions.length == 0
              ? Container()

          /// question dropdown and hint symbols ........... ///
              : GestureDetector(
            onTap: () {
              setState(() {
                emaxOverLayEntryCheck
                    ? emaxOverLayEntryCheck = false
                    : emaxOverLayEntryCheck = true;
                hide();
                FocusScope.of(context).requestFocus(new FocusNode());

              });
            },
            child: Container(
              height: (30 / 640) * screenHeight,
              width: (320 / 360) * screenWidth,
              margin: EdgeInsets.only(
                  top: (30 / 640) * screenHeight,
                  left: (20 / 360) * screenWidth,
                  right: (20 / 360) * screenWidth),
              child: attemptList[emaxPositionForQuestion - 1 ]== true
              /// after question attempts
                  ? Row(
                children: <Widget>[
                  /// question dropdown ............ ///
                  Container(
                    decoration: BoxDecoration(
                      color: NeutralColors.purpleish_blue,
                      borderRadius:
                      BorderRadius.all(Radius.circular(2)),
                    ),
                    height: (30 / 640) * screenHeight,
                    width: (170 / 360) * screenWidth,
                    margin: EdgeInsets.only(
                        right: (55 / 360) * screenWidth, left: 0.0),
                    child: InkWell(
                      child: Row(
                        crossAxisAlignment:
                        CrossAxisAlignment.stretch,
                        mainAxisAlignment:
                        MainAxisAlignment.spaceBetween,
                        children: [
                          Container(
                            height: (18 / 640) * screenHeight,
                            margin: EdgeInsets.only(
                                left: (15 / 360) * screenWidth,
                                top: (6 / 640) * screenHeight,
                                bottom: (6 / 640) * screenHeight),
                            child: Text(
                              PrepareParagraphScreenStrings
                                  .question +
                                  "${emaxPositionForQuestion}/${totalnumberOfQuestions.toString()}",
                              style: TextStyle(
                                color: NeutralColors.pureWhite,
                                fontSize: 14 / 720 * screenHeight,
                                fontFamily: "IBMPlexSans",
                                fontWeight: FontWeight.w500,
                              ),
                              textAlign: TextAlign.left,
                            ),
                          ),
                          Container(
                              width: (11 / 360) * screenWidth,
                              margin: EdgeInsets.only(
                                right: (15 / 360) * screenWidth,
                              ),
                              child: Center(
                                  child: Icon(
                                    (emaxHeader_status_question!=1&&emaxHeader_status_question!=2)? questionArrowCheck
                                        ? Icons.keyboard_arrow_up
                                        : Icons.keyboard_arrow_down:null,
                                    color: NeutralColors.pureWhite,
                                    size: 22.0 / 360 * screenWidth,
                                  )))
                        ],
                      ),
                      onTap: () {
                        setState(() {
                          emaxOverLayEntryCheck
                              ? emaxOverLayEntryCheck = false
                              : emaxOverLayEntryCheck = true;
                          (emaxHeader_status_question!=1&&emaxHeader_status_question!=2)?
                          emaxOverLayEntryCheck
                              ? show(context)
                              : hide():null;
                          FocusScope.of(context).requestFocus(new FocusNode());

                        });
                      },
                    ),
                  ),
                  Container(
                    width: 95 / 360 * screenWidth,
                    child: Row(
                      mainAxisAlignment:
                      MainAxisAlignment.spaceBetween,
                      crossAxisAlignment:
                      CrossAxisAlignment.stretch,
                      children: <Widget>[
                        /// solution .......... ///
                        InkWell(
                          onTap: () {
                            if((optionsBeforeAttemptForWebViewLoader==false||directionBeforeAttemptForWebViewLoader==false||questionBeforeAttemptForWebViewLoader==false)){
                              checking != null?checking != true
                                  ? setState(() {
                                dependencies.stopwatch.stop();
                                emaxHeader_status_question = 1;
                                if(emaxHeader_status_question==1){
                                  widget.emaxStatisticsCheck = false;
                                }
                                emaxSoloutionCheck = true;
                                emaxOverLayEntryCheck
                                    ? emaxOverLayEntryCheck =
                                false
                                    : emaxOverLayEntryCheck =
                                true;
                                hide();
                                FocusScope.of(context).requestFocus(new FocusNode());

                              })
                                  : null:null;
                            }

                          },
                          child: Container(
                            child: Padding(
                              padding: const EdgeInsets.all(5.0),
                              child: Container(
                                child: SvgPicture.asset(
                                  (optionsBeforeAttemptForWebViewLoader==false||directionBeforeAttemptForWebViewLoader==false||questionBeforeAttemptForWebViewLoader==false)? (questionsStatus[emaxPositionForQuestion-1]==null&&attemptList[emaxPositionForQuestion-1]?
                                  getPrepareSvgImages
                                      .solutionActive
                                      : getPrepareSvgImages
                                      .solutionInactive):getPrepareSvgImages
                                      .solutionInactive,
                                  fit: BoxFit.fill,
                                  height:
                                  (19.3 / 640) * screenHeight,
                                  width: (14.4 / 360) * screenWidth,
                                  color:(optionsBeforeAttemptForWebViewLoader==false||directionBeforeAttemptForWebViewLoader==false||questionBeforeAttemptForWebViewLoader==false)?( checking != null?checking == true
                                      ? Colors.grey
                                      : null:Colors.grey):Colors.grey,
                                ),
                              ),
                            ),
                          ),
                        ),
                        //  Spacer(),
                        /// statistics ..........  after submit ///
                        InkWell(
                          onTap: () {
                            if((optionsBeforeAttemptForWebViewLoader==false||directionBeforeAttemptForWebViewLoader==false||questionBeforeAttemptForWebViewLoader==false)) {
                              checking != null ? checking != true
                                  ? setState(() {
                                if (emaxHeader_status_question != 2) {
                                  widget.emaxStatisticsCheck = true;
                                }
                                emaxHeader_status_question = 2;
                                dependencies.stopwatch.stop();

                                emaxOverLayEntryCheck
                                    ? emaxOverLayEntryCheck =
                                false
                                    : emaxOverLayEntryCheck =
                                true;
                                hide();
                                FocusScope.of(context).requestFocus(
                                    new FocusNode());
                              })
                                  : null : null;
                            }
                          },
                          child: Padding(
                            padding: const EdgeInsets.all(5.0),
                            child: Container(
                                height: (18 / 640) * screenHeight,
                                width: (16 / 360) * screenWidth,
                                child: InkWell(
                                  child: SvgPicture.asset(
                                    (optionsBeforeAttemptForWebViewLoader==false||directionBeforeAttemptForWebViewLoader==false||questionBeforeAttemptForWebViewLoader==false)? (widget.emaxStatisticsCheck
                                        ? getPrepareSvgImages
                                        .barGraphActive
                                        : getPrepareSvgImages
                                        .barGraphInactive):getPrepareSvgImages
                                        .barGraphInactive,
                                    fit: BoxFit.fill,
                                    color:(optionsBeforeAttemptForWebViewLoader==false||directionBeforeAttemptForWebViewLoader==false||questionBeforeAttemptForWebViewLoader==false)?(checking != null?checking == true
                                        ? Colors.grey
                                        : null:Colors.grey):Colors.grey,
                                  ),
                                )),
                          ),
                        ),
                        // Spacer(),
                        /// bookmark .............///
                        InkWell(
                          onTap: () {
                            if((optionsBeforeAttemptForWebViewLoader==false||directionBeforeAttemptForWebViewLoader==false||questionBeforeAttemptForWebViewLoader==false)) {
                              checking != null ? checking != true
                                  ? setState(() {
                                emaxBookMarkCheck == true ?
                                emaxBookMarkCheck = false : emaxBookMarkCheck =
                                true;
                                emaxOverLayEntryCheck
                                    ? emaxOverLayEntryCheck = false
                                    : emaxOverLayEntryCheck = true;
                                hide();
                                getTimeStamp();
                                latestTrackNumber = latestTrackNumber + 1;

                                FLog.info(
                                  dataLogType: "Debug",
                                  text: latestTrackNumber.toString() +
                                      "&&&&&&&&&&&&" +
                                      (emaxPositionForQuestion - 1).toString() +
                                      "&&&&&&&&&" +
                                      (subjectNameForTrack[emaxPositionForQuestion -
                                          1]).toString() + "&&&&&&&" +
                                      (topicnameForTrack[emaxPositionForQuestion -
                                          1]).toString() + "&&&&&&&" +
                                      userNameForLogInfo.toString() + "&&&&&&" +
                                      studentidForTrack.toString() +
                                      "======================================================TRACK NUMBER && QUESTION INDEX && SUBJECT NAME && AREA/TOPIC NAME && USER NAME && STUDENT USER ID WHEN CLICK ON BOOK MARK AFTER SUBMIT QUESTION @@@@@EMAX",
                                );
                                myObj.AddtoSist(Track.Comment(
                                    areaNameForTrack[emaxPositionForQuestion -
                                        1].toString(),
                                    //    attemptForTrack,
                                    true,
                                    companyCodeForTrack,
                                    difficultyIdForTrack[emaxPositionForQuestion] -
                                        1,
                                    difficultyNameForTrack[emaxPositionForQuestion -
                                        1],
                                    questionsType[emaxPositionForQuestion - 1]
                                        .toString()
                                        .toLowerCase() == "MCQ".toLowerCase()
                                        ? null
                                        : selectedOptionIdList[emaxPositionForQuestion -
                                        1],
                                    companyCodeForTrack + "_" +
                                        studentidForTrack + "_" +
                                        testidForTrack + "_" +
                                        sectionidForTrack.toString() + "_" +
                                        itemIDForTrack[emaxPositionForQuestion -
                                            1].toString() + "_" +
                                        latestTrackNumber.toString(),
                                    questionsStatus[emaxPositionForQuestion -
                                        1],
                                    isSectionCompletedForTrack,
                                    itemIDForTrack[emaxPositionForQuestion - 1],
                                    itemtypeForTrack[emaxPositionForQuestion -
                                        1],
                                    //  isMarkedForTrack[emaximPositionForQuestionForTrack],
                                    (emaxBookMarkCheck &&
                                        (checking == false || checking == null))
                                        ? true
                                        : false,
                                    //marked
                                    negaticepointsForTrack[emaxPositionForQuestion -
                                        1],
                                    pointsForTrack[emaxPositionForQuestion - 1],
                                    emaxPositionForQuestion - 1,
                                    sectionidForTrack,
                                    sectionindexForTrack,
                                    sectionNameForTrack,
                                    questionsType[emaxPositionForQuestion - 1]
                                        .toString()
                                        .toLowerCase() == "MCQ".toLowerCase()
                                        ? selectedOptionIdList[emaxPositionForQuestion -
                                        1].toString() != "-1"
                                        ? selectedOptionIdList[emaxPositionForQuestion -
                                        1]
                                        : null
                                        : null,
                                    studentidForTrack,
                                    subjectIdForTrack[emaxPositionForQuestion -
                                        1],
                                    subjectNameForTrack[emaxPositionForQuestion -
                                        1],
                                    testidForTrack.toString(),
                                    timeRemainingForTrack,
                                    systemtime,
                                    timetakenForTrack,
                                    topicIDForTrack[emaxPositionForQuestion -
                                        1],
                                    topicnameForTrack[emaxPositionForQuestion -
                                        1],
                                    latestTrackNumber,
                                    0,
                                    0));
                                myObj.SyncTrackToApi();
                                FLog.info(
                                  dataLogType: "Debug",
                                  text: "myObj.SyncTrackToApi()======================== @@@@@@@EMAX" +
                                      myObj.SyncTrackToApi().toString(),
                                );
                                FLog.info(
                                  dataLogType: "Debug",
                                  text: "TRACK GOT SYNC WHEN CLICK ON BOOK MARK AFTER SUBMIT QUESTION @@@@@@EMAX",
                                );
                                bookmarkorUnmarkList[emaxPositionForQuestion -
                                    1] = (emaxBookMarkCheck &&
                                    (checking == false || checking == null))
                                    ? true
                                    : false; //marked,   //marked; //marked,   //marked;

                              })
                                  : null : null;
                            }
                          },
                          child: Padding(
                            padding: const EdgeInsets.all(5.0),
                            child: Container(
                              height: (15 / 640) * screenHeight,
                              width: (11.7 / 360) * screenWidth,
                              // margin: EdgeInsets.only(right: 25.6),
                              child: SvgPicture.asset(
                                (optionsBeforeAttemptForWebViewLoader==false||directionBeforeAttemptForWebViewLoader==false||questionBeforeAttemptForWebViewLoader==false)? ( bookmarkorUnmarkList[emaxPositionForQuestion-1]==true ? ((checking == false || checking == null) ? getPrepareSvgImages
                                    .bookmarkActive: getPrepareSvgImages
                                    .bookmarkInactive) : getPrepareSvgImages
                                    .bookmarkInactive):getPrepareSvgImages.bookmarkInactive,
                                fit: BoxFit.contain,
                                color:(optionsBeforeAttemptForWebViewLoader==false||directionBeforeAttemptForWebViewLoader==false||questionBeforeAttemptForWebViewLoader==false)? ( checking != null?checking == true
                                    ? Colors.grey
                                    : null:Colors.grey):Colors.grey,
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              )
              /// before question attempts
                  : Row(
                children: <Widget>[
                  /// questions dropdown ........... ///
                  Container(
                    decoration: BoxDecoration(
                      color: NeutralColors.purpleish_blue,
                      borderRadius:
                      BorderRadius.all(Radius.circular(2)),
                    ),
                    height: (30 / 640) * screenHeight,
                    width: (170 / 360) * screenWidth,
                    margin: EdgeInsets.only(
                        right: (55 / 360) * screenWidth, left: 0.0),
                    child: InkWell(
                      child: Row(
                        crossAxisAlignment:
                        CrossAxisAlignment.stretch,
                        mainAxisAlignment:
                        MainAxisAlignment.spaceBetween,
                        children: [
                          Container(
                            height: (18 / 640) * screenHeight,
                            margin: EdgeInsets.only(
                                left: (15 / 360) * screenWidth,
                                top: (6 / 640) * screenHeight,
                                bottom: (6 / 640) * screenHeight),
                            child: Text(
                              PrepareParagraphScreenStrings
                                  .question +
                                  "${emaxPositionForQuestion}/${totalnumberOfQuestions.toString()}",
                              style: TextStyle(
                                color: NeutralColors.pureWhite,
                                fontSize: 14 / 720 * screenHeight,
                                fontFamily: "IBMPlexSans",
                                fontWeight: FontWeight.w500,
                              ),
                              textAlign: TextAlign.left,
                            ),
                          ),
                          Container(
                              width: (11 / 360) * screenWidth,
                              margin: EdgeInsets.only(
                                right: (15 / 360) * screenWidth,
                              ),
                              child: Center(
                                  child: Icon(
                                    (emaxHeader_status_question!=1&&emaxHeader_status_question!=2)?questionArrowCheck
                                        ? Icons.keyboard_arrow_up
                                        : Icons.keyboard_arrow_down:null,
                                    color: NeutralColors.pureWhite,
                                    size: 22.0 / 360 * screenWidth,
                                  )))
                        ],
                      ),
                      onTap: () {
                        setState(() {
                          emaxOverLayEntryCheck
                              ? emaxOverLayEntryCheck = false
                              : emaxOverLayEntryCheck = true;
                          (emaxHeader_status_question!=1&&emaxHeader_status_question!=2)?
                          emaxOverLayEntryCheck
                              ? show(context)
                              : hide():null;
                          FocusScope.of(context).requestFocus(new FocusNode());

                        });
                      },
                    ),
                  ),

                  /// hint,statistics and book mark symbols ...... ///
                  Container(
                    width: 95 / 360 * screenWidth,
                    child: Row(
                      mainAxisAlignment:
                      MainAxisAlignment.spaceBetween,
                      crossAxisAlignment:
                      CrossAxisAlignment.stretch,
                      children: <Widget>[
                        /// solution ........ ///

                        InkWell(
                          onTap: () {
                            if((optionsBeforeAttemptForWebViewLoader==false||directionBeforeAttemptForWebViewLoader==false||questionBeforeAttemptForWebViewLoader==false)) {
                              checking != null ? checking != true
                                  ? setState(() {
                                //popUpCheck = null;
                                dependencies.stopwatch.stop();
                                emaxHeader_status_question = 0;

                                emaxSoloutionCheck = true;
                                emaxOverLayEntryCheck
                                    ? emaxOverLayEntryCheck =
                                false
                                    : emaxOverLayEntryCheck =
                                true;
                                hide();
                                FocusScope.of(context).requestFocus(new FocusNode());

                                getFlagsForAnswerPopUp().then((_) {
                                  if (accessTokenList[0] != "null" || accessTokenList[0] != "Null" ||
                                      accessTokenList[0].toString() == "null" ||
                                      accessTokenList[0].toString() == "null") {
                                    if (accessTokenList.contains(accessToken.toString())) {
                                      print(
                                          "===============================ADDTRACKWHILESEEANSWR WITHOUT POPUP");
                                      if (totalTimeTakenPerQuestions[emaxPositionForQuestion - 1]
                                          .toString()
                                          .contains(":")) {
                                        timeIntotalTimeTakenPerQuestion =
                                            totalTimeTakenPerQuestions[emaxPositionForQuestion - 1]
                                                .toString()
                                                .split(":");
                                      }
                                      if (attemptList[emaxPositionForQuestion - 1] != true) {
                                        latestTrackNumber = latestTrackNumber + 1;
                                        FLog.info(
                                          dataLogType: "Debug",
                                          text: latestTrackNumber.toString() + "&&&&&&&&&&&&" +
                                              (emaxPositionForQuestion - 1).toString() + "&&&&&&&&&" +
                                              (subjectNameForTrack[emaxPositionForQuestion - 1])
                                                  .toString() + "&&&&&&&" +
                                              (topicnameForTrack[emaxPositionForQuestion - 1])
                                                  .toString() + "&&&&&&&" +
                                              userNameForLogInfo.toString() + "&&&&&&" +
                                              studentidForTrack.toString() +
                                              "======================================================TRACK NUMBER && QUESTION INDEX && SUBJECT NAME && AREA/TOPIC NAME && USER NAME && STUDENT USER ID WHEN CLICK ON SEE SOLUTION WITHOUT POPUP @@@@@@@ EMAX",
                                        );
                                        myObj.AddtoSist(Track.Comment(
                                            areaNameForTrack[emaxPositionForQuestion - 1].toString(),
                                            //    attemptForTrack,
                                            true,
                                            companyCodeForTrack,
                                            difficultyIdForTrack[emaxPositionForQuestion - 1],
                                            difficultyNameForTrack[emaxPositionForQuestion - 1],
                                            null,
                                            companyCodeForTrack + "_" + studentidForTrack + "_" +
                                                testidForTrack + "_" + sectionidForTrack.toString() +
                                                "_" +
                                                itemIDForTrack[emaxPositionForQuestion - 1].toString() +
                                                "_" + latestTrackNumber.toString(),
                                            null,
                                            //isCorrect
                                            isSectionCompletedForTrack,
                                            itemIDForTrack[emaxPositionForQuestion - 1],
                                            itemtypeForTrack[emaxPositionForQuestion - 1],
                                            //  isMarkedForTrack[emaximPositionForQuestionForTrack],
                                            bookmarkorUnmarkList[emaxPositionForQuestion - 1]
                                                ? true
                                                : false,
                                            negaticepointsForTrack[emaxPositionForQuestion - 1],
                                            pointsForTrack[emaxPositionForQuestion - 1],
                                            emaxPositionForQuestion - 1,
                                            sectionidForTrack,
                                            sectionindexForTrack,
                                            sectionNameForTrack,
                                            null,
                                            studentidForTrack,
                                            subjectIdForTrack[emaxPositionForQuestion - 1],
                                            subjectNameForTrack[emaxPositionForQuestion - 1],
                                            testidForTrack.toString(),
                                            timeRemainingForTrack,
                                            systemtime,
                                            totalTimeTakenPerQuestions[emaxPositionForQuestion - 1]
                                                .toString()
                                                .contains(":") == true
                                                ? _getDuration(
                                                int.parse(hoursStr), int.parse(minutesStr),
                                                int.parse(secondsStr)) - _getDuration(
                                                int.parse(timeIntotalTimeTakenPerQuestion[0]),
                                                int.parse(timeIntotalTimeTakenPerQuestion[1]),
                                                int.parse(timeIntotalTimeTakenPerQuestion[2]))
                                                : _getDuration(
                                                int.parse(hoursStr), int.parse(minutesStr),
                                                int.parse(secondsStr)),
                                            topicIDForTrack[emaxPositionForQuestion - 1],
                                            topicnameForTrack[emaxPositionForQuestion - 1],
                                            latestTrackNumber,
                                            0,
                                            0));
                                        myObj.SyncTrackToApi();
                                        syncData();
                                        FLog.info(
                                          dataLogType: "Debug",
                                          text: "myObj.SyncTrackToApi()======================== @@@@@@@ EMAX" +
                                              myObj.SyncTrackToApi().toString(),
                                        );
                                        FLog.info(
                                          dataLogType: "Debug",
                                          text: "TRACK GOT SYNC WHEN CLICK ON SEE SOLUTION WITHOUT POPUP @@@@@@ EMAX",
                                        );
                                      }

                                      attemptList[emaxPositionForQuestion - 1] = true;
                                      questionsStatus[emaxPositionForQuestion - 1] = null;
                                      bookmarkorUnmarkList[emaxPositionForQuestion - 1] =
                                      bookmarkorUnmarkList[emaxPositionForQuestion - 1] ? true : false;
                                      selectedOptionIdList[emaxPositionForQuestion - 1] = "-1";
                                      if (totalTimeTakenPerQuestions[emaxPositionForQuestion - 1]
                                          .toString()
                                          .contains(":")) {
                                        var timeInList = totalTimeTakenPerQuestions[emaxPositionForQuestion -
                                            1].toString().split(":");
                                        totalTimeTakenPerQuestions[emaxPositionForQuestion - 1] =
                                        (durationToSec((_getDuration(
                                            int.parse(hoursStr), int.parse(minutesStr),
                                            int.parse(secondsStr)))));
                                      } else {
                                        totalTimeTakenPerQuestions[emaxPositionForQuestion - 1] =
                                        (durationToSec((_getDuration(
                                            int.parse(hoursStr), int.parse(minutesStr),
                                            int.parse(secondsStr))) +
                                            totalTimeTakenPerQuestions[emaxPositionForQuestion - 1]));
                                      }


                                      emaxHeader_status_question = 1;
                                      if (emaxHeader_status_question == 1) {
                                        widget.emaxStatisticsCheck = false;
                                      }
                                    }
                                    else {
                                      emaxPopUpForSolutionIcon = true;
                                      _emaxControllerForPopUp.open();
                                    }
                                    getTimeStamp();
                                  } else {
                                    emaxPopUpForSolutionIcon = true;
                                    _emaxControllerForPopUp.open();
                                  }
                                });

                                //latestTrackNumber = latestTrackNumber++;
                              })
                                  : null : null;
                            }
                          },
                          child: Container(
                            child: Padding(
                              padding: const EdgeInsets.all(5.0),
                              child: Container(
//
                                child: SvgPicture.asset(
                                  (optionsBeforeAttemptForWebViewLoader==false||directionBeforeAttemptForWebViewLoader==false||questionBeforeAttemptForWebViewLoader==false)?
                                  ( (questionsStatus[emaxPositionForQuestion-1]==null&&attemptList[emaxPositionForQuestion-1] &&
                                      ( checking == false ||  checking == null))
                                      ? getPrepareSvgImages
                                      .solutionActive
                                      : getPrepareSvgImages
                                      .solutionInactive):getPrepareSvgImages.solutionInactive,
                                  fit: BoxFit.fill,
                                  height:
                                  (19.3 / 640) * screenHeight,
                                  width: (14.4 / 360) * screenWidth,
                                  color: (optionsBeforeAttemptForWebViewLoader==false||directionBeforeAttemptForWebViewLoader==false||questionBeforeAttemptForWebViewLoader==false)?(checking != null?checking == true
                                      ? Colors.grey
                                      : null:Colors.grey):Colors.grey,
                                ),
                              ),
                            ),
                          ),
                        ),
                        //  Spacer(),
                        /// statistics ............. before submit///
                        InkWell(
                          onTap: () {
                            if((optionsBeforeAttemptForWebViewLoader==false||directionBeforeAttemptForWebViewLoader==false||questionBeforeAttemptForWebViewLoader==false)) {
                              if ((SUB_FLAG) &&
                                  (OPTION_ACTIVE == false)) {
                                checking != null ? checking != true


                                    ? setState(() {
                                  OPTION_ACTIVE = false;
                                  SUBMIT_ACTIVE = true;
                                  ANSWER_SUBMITTED = true;
                                  SUB_FLAG = true;
                                  if (emaxHeader_status_question != 1 &&
                                      emaxHeader_status_question != 2) {
                                    widget.emaxStatisticsCheck = true;
                                  }
                                  emaxHeader_status_question = 2;
                                  dependencies.stopwatch.stop();
                                  emaxStatisticsCheck = true;
                                  emaxOverLayEntryCheck
                                      ? emaxOverLayEntryCheck =
                                  false
                                      : emaxOverLayEntryCheck =
                                  true;
                                  hide();
                                  FocusScope.of(context).requestFocus(new FocusNode());
                                })
                                    : null : null;
                              }
                            }
                          },
                          child: Padding(
                            padding: const EdgeInsets.all(5.0),
                            child: Container(
                                height: (18 / 640) * screenHeight,
                                width: (16 / 360) * screenWidth,
                                child: InkWell(
                                  child: SvgPicture.asset(
                                    (optionsBeforeAttemptForWebViewLoader==false||directionBeforeAttemptForWebViewLoader==false||questionBeforeAttemptForWebViewLoader==false)?
                                    (( widget.emaxStatisticsCheck &&
                                        checking == false)
                                        ? getPrepareSvgImages
                                        .barGraphActive
                                        : getPrepareSvgImages
                                        .barGraphInactive):getPrepareSvgImages.barGraphInactive,
                                    fit: BoxFit.fill,
                                    color:     (optionsBeforeAttemptForWebViewLoader==false||directionBeforeAttemptForWebViewLoader==false||questionBeforeAttemptForWebViewLoader==false)?
                                    (((SUB_FLAG) &&
                                        (OPTION_ACTIVE == false))?
                                    checking != null?checking == true
                                        ? Colors.grey
                                        : null:Colors.grey:Colors.grey):Colors.grey,
                                  ),
                                )),
                          ),
                        ),
                        // Spacer(),
                        /// bookmark .................. ///
                        InkWell(
                          onTap: () {
                            if((optionsBeforeAttemptForWebViewLoader==false||directionBeforeAttemptForWebViewLoader==false||questionBeforeAttemptForWebViewLoader==false)) {
                              checking != null ? checking != true
                                  ? setState(() {
                                emaxBookMarkCheck == true
                                    ? emaxBookMarkCheck = false
                                    : emaxBookMarkCheck = true;
                                emaxOverLayEntryCheck
                                    ? emaxOverLayEntryCheck =
                                false
                                    : emaxOverLayEntryCheck =
                                true;
                                hide();
                                FocusScope.of(context).requestFocus(new FocusNode());

                                getTimeStamp();

                                print("===============================ADDTRACKWHILEMARKED");

                                /// without submit   ///
                                /// need to remove sub flag
                                latestTrackNumber = latestTrackNumber + 1;
                                FLog.info(
                                  dataLogType: "Debug",
                                  text: latestTrackNumber.toString() + "&&&&&&&&&&&&" +
                                      (emaxPositionForQuestion - 1).toString() + "&&&&&&&&&" +
                                      (subjectNameForTrack[emaxPositionForQuestion - 1]).toString() +
                                      "&&&&&&&" +
                                      (topicnameForTrack[emaxPositionForQuestion - 1]).toString() +
                                      "&&&&&&&" + userNameForLogInfo.toString() + "&&&&&&" +
                                      studentidForTrack.toString() +
                                      "======================================================TRACK NUMBER && QUESTION INDEX && SUBJECT NAME && AREA/TOPIC NAME && USER NAME && STUDENT USER ID  WHEN CLICK ON BOOK MARK BEFORE SUMBIT QUESTION @@@@@@@ EMAX",
                                );
                                myObj.AddtoSist(Track.Comment(
                                    areaNameForTrack[emaxPositionForQuestion - 1].toString(),
                                    //    attemptForTrack,
                                    false,
                                    companyCodeForTrack,
                                    difficultyIdForTrack[emaxPositionForQuestion - 1],
                                    difficultyNameForTrack[emaxPositionForQuestion - 1],
                                    null,
                                    companyCodeForTrack + "_" + studentidForTrack + "_" +
                                        testidForTrack + "_" + sectionidForTrack.toString() + "_" +
                                        itemIDForTrack[emaxPositionForQuestion - 1].toString() + "_" +
                                        latestTrackNumber.toString(),
                                    null,
                                    isSectionCompletedForTrack,
                                    itemIDForTrack[emaxPositionForQuestion - 1],
                                    itemtypeForTrack[emaxPositionForQuestion - 1],
                                    //  isMarkedForTrack[emaximPositionForQuestionForTrack],
                                    (emaxBookMarkCheck && (checking == false || checking == null))
                                        ? true
                                        : false,
                                    //marked,   //marked
                                    negaticepointsForTrack[emaxPositionForQuestion - 1],
                                    pointsForTrack[emaxPositionForQuestion - 1],
                                    emaxPositionForQuestion - 1,
                                    sectionidForTrack,
                                    sectionindexForTrack,
                                    sectionNameForTrack,
                                    null,
                                    studentidForTrack,
                                    subjectIdForTrack[emaxPositionForQuestion - 1],
                                    subjectNameForTrack[emaxPositionForQuestion - 1],
                                    testidForTrack.toString(),
                                    timeRemainingForTrack,
                                    systemtime,
                                    // _getDuration(int.parse(hoursStr),int.parse(minutesStr),int.parse(secondsStr)),
                                    timetakenForTrack,
                                    topicIDForTrack[emaxPositionForQuestion - 1],
                                    topicnameForTrack[emaxPositionForQuestion - 1],
                                    latestTrackNumber,
                                    0,
                                    0));
                                myObj.SyncTrackToApi();
                                FLog.info(
                                  dataLogType: "Debug",
                                  text: "myObj.SyncTrackToApi()======================== @@@@@@ EMAX" +
                                      myObj.SyncTrackToApi().toString(),
                                );
                                FLog.info(
                                  dataLogType: "Debug",
                                  text: "TRACK GOT SYNC WHEN CLICK ON BOOK MARK BEFORE SUMBIT QUESTION @@@@@@@@ EMAX",
                                );

                                //attemptList[emaxPositionForQuestion-1]=false;
                                //questionsStatus[emaxPositionForQuestion-1]= "-1";
                                bookmarkorUnmarkList[emaxPositionForQuestion - 1] =
                                (emaxBookMarkCheck && (checking == false || checking == null))
                                    ? true
                                    : false; //marked,   //marked; //marked,   //marked;
                                //selectedOptionIdList[emaxPositionForQuestion-1]="-1";
//                                if(totalTimeTakenPerQuestions[emaxPositionForQuestion-1].toString().contains(":")){
//                                  var timeInList=totalTimeTakenPerQuestions[emaxPositionForQuestion-1].toString().split(":");
//                                  totalTimeTakenPerQuestions[emaxPositionForQuestion-1]= (durationToSec((_getDuration(int.parse(hoursStr),int.parse(minutesStr),int.parse(secondsStr)))+(_getDuration(int.parse(timeInList[0]),int.parse(timeInList[1]),int.parse(timeInList[2])))));
//                                }else{
//                                  totalTimeTakenPerQuestions[emaxPositionForQuestion-1]= (durationToSec((_getDuration(int.parse(hoursStr),int.parse(minutesStr),int.parse(secondsStr)))+totalTimeTakenPerQuestions[emaxPositionForQuestion-1]));
//                                }

                              })
                                  : null : null;
                            }
                          },
                          child: Padding(
                            padding: const EdgeInsets.all(5.0),
                            child: Container(
                              height: (15 / 640) * screenHeight,
                              width: (11.7 / 360) * screenWidth,
                              // margin: EdgeInsets.only(right: 25.6),
                              child: SvgPicture.asset(
                                (optionsBeforeAttemptForWebViewLoader==false||directionBeforeAttemptForWebViewLoader==false||questionBeforeAttemptForWebViewLoader==false)?
                                (bookmarkorUnmarkList[emaxPositionForQuestion-1]==true ? ((checking == false || checking == null) ? getPrepareSvgImages
                                    .bookmarkActive: getPrepareSvgImages
                                    .bookmarkInactive) : getPrepareSvgImages
                                    .bookmarkInactive):getPrepareSvgImages.bookmarkInactive,
                                fit: BoxFit.contain,
                                color: (optionsBeforeAttemptForWebViewLoader==false||directionBeforeAttemptForWebViewLoader==false||questionBeforeAttemptForWebViewLoader==false)?(checking != null?checking == true
                                    ? Colors.grey
                                    : null:Colors.grey):Colors.grey,
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),

          /// status ................ ///
          GestureDetector(
            onTap: () {
              setState(() {
                emaxOverLayEntryCheck
                    ? emaxOverLayEntryCheck = false
                    : emaxOverLayEntryCheck = true;
                hide();
                FocusScope.of(context).requestFocus(new FocusNode());

              });
            },
            child: Align(
              alignment: Alignment.topCenter,
              child: Container(
                // color: Colors.red,
                margin: EdgeInsets.only(bottom: 0),
                //height: 520/720 * screenHeight,
                //margin: EdgeInsets.only(top: 40/720 * screenHeight,bottom:23/720* screenHeight),
                child: _status(),
              ),
            ),
          ),
        ],
      ),
    );
  }

  /// popup for pause button .... ///
  Widget _popUpForPauseButton(context) {
    final screenHeight = MediaQuery.of(context).size.height;
    final screenWidth = MediaQuery.of(context).size.width;

    return Container(
      height: screenHeight * 265 / 640,
      margin: EdgeInsets.only(bottom: 154 / 640 * screenHeight),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.end,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Align(
            alignment: Alignment.topCenter,
            child: Container(
              width: screenWidth * 140 / 360,
              height: screenHeight * 130 / 720,
              margin: EdgeInsets.only(bottom: 17 / 720 * screenHeight),
              child: SvgPicture.asset(
                getPrepareSvgImages.puseImageForPopUp,
                fit: BoxFit.fill,
              ),
            ),
          ),
          Align(
            alignment: Alignment.topCenter,
            child: Container(
              margin: EdgeInsets.only(bottom: 27 / 720 * screenHeight),
              child: Text(
                PrepareParagraphScreenStrings.sessionPaused,
                style: TextStyle(
                  color: NeutralColors.black,
                  fontSize: screenHeight * 16 / 678,
                  fontFamily: "IBMPlexSans",
                  fontWeight: FontWeight.w500,
                ),
                textAlign: TextAlign.center,
              ),
            ),
          ),
          Align(
            alignment: Alignment.center,
            child: Container(
              width: screenWidth * 130 / 360,
              height: screenHeight * 40 / 720,
              margin: EdgeInsets.only(top: 20 / 720 * screenHeight),
              child: FlatButton(
                onPressed: () {
                  _emaxControllerForPopUp.close();
                  //dependencies.stopwatch.start();
                  stopWatch();
                },
                color: Color.fromARGB(255, 0, 171, 251),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(2)),
                ),
                textColor: NeutralColors.pureWhite,
                padding: EdgeInsets.all(0),
                child: Text(
                  PrepareParagraphScreenStrings.resume,
                  style: TextStyle(
                    fontSize: screenHeight * 14 / 720,
                    fontFamily: "IBMPlexSans",
                    fontWeight: FontWeight.w500,
                  ),
                  textAlign: TextAlign.center,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  /// popup for back button ......... ///
  Widget _popUpForBackButton(context) {
    final screenHeight = MediaQuery.of(context).size.height;
    final screenWidth = MediaQuery.of(context).size.width;
    return Container(
      height: screenHeight * 275 / 640,
      margin: EdgeInsets.only(bottom: 154 / 640 * screenHeight),
      //color: Colors.black,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.end,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Align(
            alignment: Alignment.topCenter,
            child: Container(
              width: screenWidth * 140 / 360,
              height: screenHeight * 130 / 720,
              margin: EdgeInsets.only(bottom: 15 / 720 * screenHeight),
              child: SvgPicture.asset(
                getPrepareSvgImages.exitImageForPopUp,
                fit: BoxFit.fill,
              ),
            ),
          ),
          Align(
            alignment: Alignment.topCenter,
            child: Container(
              margin: EdgeInsets.only(bottom: 40 / 720 * screenHeight),
              child: Text(
                PrepareParagraphScreenStrings.popUpForExit,
                style: TextStyle(
                  color: NeutralColors.black,
                  fontSize: screenHeight * 16 / 720,
                  fontFamily: "IBMPlexSans",
                  fontWeight: FontWeight.w500,
                ),
                textAlign: TextAlign.center,
              ),
            ),
          ),
          Container(
            height: screenHeight * 47.5 / 720,
            padding: EdgeInsets.only(
                left: 40.0 / 360 * screenWidth,
                right: 40.0 / 360 * screenWidth),
            child: Row(
              children: [
                Container(
                  width: screenWidth * 130 / 360,
                  height: screenHeight * 40 / 720,
                  margin: EdgeInsets.only(top: 1 / 720 * screenHeight),
                  child: FlatButton(
                    onPressed: () {
                      stopWatch();
                      Wakelock.disable();
                      if(attemptList[emaxPositionForQuestion-1]==false){
                        if(totalTimeTakenPerQuestions[emaxPositionForQuestion-1].toString().contains(":")){
                          timeIntotalTimeTakenPerQuestion=totalTimeTakenPerQuestions[emaxPositionForQuestion-1].toString().split(":");
                        }
                        if(attemptList[emaxPositionForQuestion-1]!=true){
                          latestTrackNumber=latestTrackNumber+1;
                        }
                        FLog.info(
                          dataLogType: "Debug",
                          text: latestTrackNumber.toString()+"&&&&&&&&&&&&"+(emaxPositionForQuestion-1).toString()+"&&&&&&&&&"+(subjectNameForTrack[emaxPositionForQuestion-1]).toString()+"&&&&&&&"+(topicnameForTrack[emaxPositionForQuestion-1]).toString()+"&&&&&&&"+userNameForLogInfo.toString()+"&&&&&&"+studentidForTrack.toString()+"======================================================TRACK NUMBER && QUESTION INDEX && SUBJECT NAME && AREA/TOPIC NAME && USER NAME && STUDENT USER ID  WHEN ClICK ON YES BUTTON IN POPUP @@@@@@ EMAX",
                        );
                        myObj.AddtoSist(Track.Comment(areaNameForTrack[emaxPositionForQuestion-1].toString(),
                            //    attemptForTrack,
                            false,
                            companyCodeForTrack,
                            difficultyIdForTrack[emaxPositionForQuestion-1],
                            difficultyNameForTrack[emaxPositionForQuestion-1],
                            null,
                            companyCodeForTrack+"_"+studentidForTrack+"_"+testidForTrack+"_"+sectionidForTrack.toString()+"_"+
                                itemIDForTrack[emaxPositionForQuestion-1].toString()+"_"+latestTrackNumber.toString(),
                            null,
                            isSectionCompletedForTrack,
                            itemIDForTrack[emaxPositionForQuestion-1],
                            itemtypeForTrack[emaxPositionForQuestion-1],
                            //  isMarkedForTrack[emaximPositionForQuestionForTrack],
                            bookmarkorUnmarkList[emaxPositionForQuestion-1]?true:false, //marked,   //marked
                            negaticepointsForTrack[emaxPositionForQuestion-1],
                            pointsForTrack[emaxPositionForQuestion-1],
                            emaxPositionForQuestion-1,
                            sectionidForTrack,
                            sectionindexForTrack,
                            sectionNameForTrack,
                            null,
                            studentidForTrack,
                            subjectIdForTrack[emaxPositionForQuestion-1],
                            subjectNameForTrack[emaxPositionForQuestion-1],
                            testidForTrack.toString(),
                            timeRemainingForTrack,
                            systemtime,
                            totalTimeTakenPerQuestions[emaxPositionForQuestion-1].toString().contains(":")==true?_getDuration(int.parse(hoursStr),int.parse(minutesStr),int.parse(secondsStr))- _getDuration(int.parse(timeIntotalTimeTakenPerQuestion[0]),int.parse(timeIntotalTimeTakenPerQuestion[1]),int.parse(timeIntotalTimeTakenPerQuestion[2])):_getDuration(int.parse(hoursStr),int.parse(minutesStr),int.parse(secondsStr)),
                            topicIDForTrack[emaxPositionForQuestion-1],
                            topicnameForTrack[emaxPositionForQuestion-1],
                            latestTrackNumber,
                            0,
                            0));
                        myObj.SyncTrackToApi();
                        FLog.info(
                          dataLogType: "Debug",
                          text:  "myObj.SyncTrackToApi()======================== @@@@@@@@ EMAX"+myObj.SyncTrackToApi().toString(),
                        );
                        FLog.info(
                          dataLogType: "Debug",
                          text:  "TRACK GOT SYNC WHEN ClICK ON YES BUTTON IN POPUP @@@@@@ EMAX",
                        );
                        bookmarkorUnmarkList[emaxPositionForQuestion-1]=bookmarkorUnmarkList[emaxPositionForQuestion-1]?true:false;
                        if(totalTimeTakenPerQuestions[emaxPositionForQuestion-1].toString().contains(":")){
                          //var timeInList=totalTimeTakenPerQuestions[emaxPositionForQuestion-1].toString().split(":");
                          totalTimeTakenPerQuestions[emaxPositionForQuestion-1]= (durationToSec(((_getDuration(int.parse(hoursStr),int.parse(minutesStr),int.parse(secondsStr))))));
                        }else{
                          totalTimeTakenPerQuestions[emaxPositionForQuestion-1]= (durationToSec((_getDuration(int.parse(hoursStr),int.parse(minutesStr),int.parse(secondsStr)))+totalTimeTakenPerQuestions[emaxPositionForQuestion-1]));
                        }

                      }
                      Navigator.pop(context, true);


                      // AppRoutes.push(context, PrepareSubjectsScreenHtml);

                    },
                    color: NeutralColors.ice_blue,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(Radius.circular(2)),
                    ),
                    textColor: NeutralColors.blue_grey,
                    padding: EdgeInsets.all(0),
                    child: Text(
                      PrepareParagraphScreenStrings.yesForPopUpExit,
                      style: TextStyle(
                        fontSize: screenHeight * 14 / 678,
                        fontFamily: "IBMPlexSans",
                        fontWeight: FontWeight.w500,
                      ),
                      textAlign: TextAlign.center,
                    ),
                  ),
                ),
                Spacer(),
                Container(
                  width: screenWidth * 130 / 360,
                  height: screenHeight * 40 / 720,
                  // margin: EdgeInsets.only(top: 1/720*screenHeight),
                  child: FlatButton(
                    onPressed: () => _emaxControllerForPopUp.close(),
                    color: Color.fromARGB(255, 0, 171, 251),
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(Radius.circular(3)),
                    ),
                    textColor: NeutralColors.pureWhite,
                    padding: EdgeInsets.all(0.0),
                    child: Text(
                      PrepareParagraphScreenStrings.noForPopUpExit,
                      style: TextStyle(
                        fontSize: screenHeight * 14 / 678,
                        fontFamily: "IBMPlexSans",
                        fontWeight: FontWeight.w500,
                      ),
                      textAlign: TextAlign.center,
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  ///popup for answer button solutionActive
  Widget _popUpForAnswerButton(context) {
    final screenHeight = MediaQuery.of(context).size.height;
    final screenWidth = MediaQuery.of(context).size.width;
    return Container(
      // height: screenHeight * 275 / 640,
      margin: EdgeInsets.only(
          top: 10 / 720 * screenHeight, bottom: 10 / 720 * screenHeight),
      //color: Colors.black,
      child: Column(
        children: [
          Container(
            // color:Colors.red,
            // height: 50/720*screenHeight,
            margin: EdgeInsets.only(left: 22 / 360 * screenHeight,
                right: 20 / 360 * screenHeight,
                top: 60 / 720 * screenHeight),
            child: Text(
              Preparequestions.messageInSolutionCheckPopup,
              maxLines: 2,
              style: TextStyle(
                color: Colors.black,
                fontSize: screenHeight * 16 / 720,
                fontFamily: "IBM Plex Sans Medium",
                fontWeight: FontWeight.w500,
              ),
              textAlign: TextAlign.center,
            ),
          ),

          Container(
            color: Colors.transparent,
            margin: EdgeInsets.only(left: 42 / 360 * screenWidth,
                right: 42 / 360 * screenWidth,
                top: 22 / 720 * screenHeight),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,

              children: <Widget>[
                Container(
                  color: Colors.transparent,
                  margin: EdgeInsets.only(left: 0),
                  child: new Checkbox(
                      onChanged: (bool e) => {
                        checkbox(),
                      },
                      value: checkBoxState),
                ),
                new Text(Preparequestions.dontShowMsgInSolutionCheckPopup,
                  style: TextStyle(
                    color: NeutralColors.bluey_grey,
                    fontSize: screenHeight * 13 / 720,
                    fontFamily: "IBM Plex Sans Medium",
                    fontWeight: FontWeight.w500,
                  ),
                ),
              ],
            ),
          ),
          Container(
            // height: screenHeight * 40 / 720,
            margin: EdgeInsets.only(top: 25 / 720,
                left: 42 / 360 * screenWidth,
                right: 42 / 360 * screenWidth),
            child: Row(
              children: [
                Container(
                  width: screenWidth * 130 / 360,
                  height: screenHeight * 40 / 720,
                  margin: EdgeInsets.only(top: 10 / 720 * screenHeight),
                  child: FlatButton(
                    onPressed: () {
                      _emaxControllerForPopUp.close();
                      if(checkBoxState==true){
                        checkBoxState=false;
                      }else{
                        checkBoxState=false;
                      }
                    },
                    color: Color.fromARGB(255, 242, 244, 244),
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(Radius.circular(2)),
                    ),
                    textColor: Color.fromARGB(255, 126, 145, 154),
                    padding: EdgeInsets.all(0),
                    child: Text(
                      Preparequestions.cancelInSolutionCheckPopup,
                      style: TextStyle(
                        fontSize: screenHeight * 14 / 678,
                        fontFamily: "IBM Plex Sans Medium",
                        fontWeight: FontWeight.w500,
                      ),
                      textAlign: TextAlign.center,
                    ),
                  ),
                ),
                Spacer(),
                Container(
                  width: screenWidth * 130 / 360,
                  height: screenHeight * 40 / 720,
                  margin: EdgeInsets.only(top: 10 / 720 * screenHeight),

                  // margin: EdgeInsets.only(left: 15/360*screenWidth),
                  child: FlatButton(
                    onPressed: () {
                      for(int i=0;i<finalTotalAnswerListAccordingToQuestion[emaxPositionForQuestion-1].length;i++){
                        if(finalTotalAnswerListAccordingToQuestion[emaxPositionForQuestion-1][i]== true){
                          AECorrectAnswer = finalTotalOptionsAccordingToQuestions[emaxPositionForQuestion-1][i];
                        }
                      }
                      _emaxControllerForPopUp.close();
                      setState(() {
                        okay_flag = true;
                        OPTION_ACTIVE = true;
                        SUB_FLAG = false;
                        if(checkBoxState==true){
                          getFlagsForAnswerPopUp().then((_){

                            if(accessTokenList[0]=="null"||accessTokenList[0]=="Null"||accessTokenList[0].toString()=="null"||accessTokenList[0].toString()=="Null"){
                              finalAcessTokenList=finalAcessTokenList;
                            }else{
                              finalAcessTokenList= accessTokenList;
                            }

                            finalAcessTokenList.add(accessToken.toString());

                            setFlagsForAnswerPopUp(finalAcessTokenList) ;
                          });
                        }
                      });


                      getTimeStamp();
                      selectedOptionIdForNext = null;
                      attemptForNext = true;
                      isCorrectForNext = null;
                      if(totalTimeTakenPerQuestions[emaxPositionForQuestion-1].toString().contains(":")){
                        timeIntotalTimeTakenPerQuestion=totalTimeTakenPerQuestions[emaxPositionForQuestion-1].toString().split(":");
                      }
                      if(attemptList[emaxPositionForQuestion-1]!=true){
                        latestTrackNumber=latestTrackNumber+1;
                        FLog.info(
                          dataLogType: "Debug",
                          text: latestTrackNumber.toString()+"&&&&&&&&&&&&"+(emaxPositionForQuestion-1).toString()+"&&&&&&&&&"+(subjectNameForTrack[emaxPositionForQuestion-1]).toString()+"&&&&&&&"+(topicnameForTrack[emaxPositionForQuestion-1]).toString()+"&&&&&&&"+userNameForLogInfo.toString()+"&&&&&&"+studentidForTrack.toString()+"======================================================TRACK NUMBER && QUESTION INDEX && SUBJECT NAME && AREA/TOPIC NAME && USER NAME && STUDENT USER ID  WHEN CLICK ON SEE SOLUTION WITH POPUP @@@@@@@@ EMAX",
                        );
                        myObj.AddtoSist(Track.Comment(areaNameForTrack[emaxPositionForQuestion-1].toString(),
                            //    attemptForTrack,
                            true,
                            companyCodeForTrack,
                            difficultyIdForTrack[emaxPositionForQuestion-1],
                            difficultyNameForTrack[emaxPositionForQuestion-1],
                            null,
                            companyCodeForTrack+"_"+studentidForTrack+"_"+testidForTrack+"_"+sectionidForTrack.toString()+"_"+
                                itemIDForTrack[emaxPositionForQuestion-1].toString()+"_"+latestTrackNumber.toString(),
                            null,       //isCorrect
                            isSectionCompletedForTrack,
                            itemIDForTrack[emaxPositionForQuestion-1],
                            itemtypeForTrack[emaxPositionForQuestion-1],
                            //  isMarkedForTrack[emaximPositionForQuestionForTrack],
                            bookmarkorUnmarkList[emaxPositionForQuestion-1]?true:false,
                            negaticepointsForTrack[emaxPositionForQuestion-1],
                            pointsForTrack[emaxPositionForQuestion-1],
                            emaxPositionForQuestion-1,
                            sectionidForTrack,
                            sectionindexForTrack,
                            sectionNameForTrack,
                            null,
                            studentidForTrack,
                            subjectIdForTrack[emaxPositionForQuestion-1],
                            subjectNameForTrack[emaxPositionForQuestion-1],
                            testidForTrack.toString(),
                            timeRemainingForTrack,
                            systemtime,
                            totalTimeTakenPerQuestions[emaxPositionForQuestion-1].toString().contains(":")==true?_getDuration(int.parse(hoursStr),int.parse(minutesStr),int.parse(secondsStr))- _getDuration(int.parse(timeIntotalTimeTakenPerQuestion[0]),int.parse(timeIntotalTimeTakenPerQuestion[1]),int.parse(timeIntotalTimeTakenPerQuestion[2])):_getDuration(int.parse(hoursStr),int.parse(minutesStr),int.parse(secondsStr)),                          topicIDForTrack[emaxPositionForQuestion-1],
                            topicnameForTrack[emaxPositionForQuestion-1],
                            latestTrackNumber,
                            0,
                            0));
                        myObj.SyncTrackToApi();
                        syncData();
                        FLog.info(
                          dataLogType: "Debug",
                          text:  "myObj.SyncTrackToApi()======================== @@@@@@@@ EMAX"+myObj.SyncTrackToApi().toString(),
                        );
                        FLog.info(
                          dataLogType: "Debug",
                          text:  "TRACK GOT SYNC WHEN CLICK ON SEE SOLUTION WITH POPUP @@@@@@@@@ EMAX",
                        );
                      }

                      attemptList[emaxPositionForQuestion-1]=true;
                      questionsStatus[emaxPositionForQuestion-1]= null;
                      bookmarkorUnmarkList[emaxPositionForQuestion-1]=bookmarkorUnmarkList[emaxPositionForQuestion-1]?true:false;
                      selectedOptionIdList[emaxPositionForQuestion-1]="-1";
                      if(totalTimeTakenPerQuestions[emaxPositionForQuestion-1].toString().contains(":")){
                        // var timeInList=totalTimeTakenPerQuestions[emaxPositionForQuestion-1].toString().split(":");
                        totalTimeTakenPerQuestions[emaxPositionForQuestion-1]= (durationToSec((_getDuration(int.parse(hoursStr),int.parse(minutesStr),int.parse(secondsStr)))));
                      }else{
                        totalTimeTakenPerQuestions[emaxPositionForQuestion-1]= (durationToSec((_getDuration(int.parse(hoursStr),int.parse(minutesStr),int.parse(secondsStr)))+totalTimeTakenPerQuestions[emaxPositionForQuestion-1]));
                      }
                      emaxHeader_status_question = 1;
                      stopWatch();

                    },
                    color: Color.fromARGB(255, 0, 171, 251),
                    shape:
                    RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(Radius.circular(3)),
                    ),
                    textColor: Color.fromARGB(255, 255, 255, 255),
                    padding: EdgeInsets.all(0.0),
                    child: Text(
                      Preparequestions.okayInSolutionCheckPopup,
                      style: TextStyle(
                        fontSize: screenHeight * 14 / 678,
                        fontFamily: "IBM Plex Sans Medium",
                        fontWeight: FontWeight.w500,
                      ),
                      textAlign: TextAlign.center,
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),);
  }

  /// popup for exit button ......... ///
  Widget _popUpForExitButton(context) {
    print("coming inside exit button");
    final screenHeight = MediaQuery.of(context).size.height;
    final screenWidth = MediaQuery.of(context).size.width;
    var unattepmtedquestionsCoun=0;
    for(var i=0; i < attemptList.length;i++){
      if(attemptList[i]==false){
        unattepmtedquestionsCoun=unattepmtedquestionsCoun+1;
      }else{
        unattepmtedquestionsCoun=unattepmtedquestionsCoun;
      }
    }
    return Container(
      height: screenHeight * 275 / 640,
      margin: EdgeInsets.only(bottom: 154 / 640 * screenHeight),
      //color: Colors.black,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.end,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Align(
            alignment: Alignment.topCenter,
            child: Container(
              margin: EdgeInsets.only(bottom: 40 / 720 * screenHeight,
                  left: 10.0 / 360 * screenWidth,
                  right: 10.0 / 360 * screenWidth
              ),
              child: Text(
                (PrepareParagraphScreenStrings.popUpForMsgInExitButton1)+(unattepmtedquestionsCoun.toString()+"/"+totalQuestions.length.toString())+(PrepareParagraphScreenStrings.popUpForMsgInExitButton2),
                style: TextStyle(
                  color: NeutralColors.black,
                  fontSize: screenHeight * 16 / 720,
                  fontFamily: "IBMPlexSans",
                  fontWeight: FontWeight.w500,
                ),
                textAlign: TextAlign.center,
              ),
            ),
          ),
          Align(
            alignment: Alignment.topCenter,
            child: Container(
              margin: EdgeInsets.only(bottom: 40 / 720 * screenHeight,left: 10.0 / 360 * screenWidth,
                  right: 10.0 / 360 * screenWidth),
              child: Text(
                PrepareParagraphScreenStrings.popUpForExitButton,
                style: TextStyle(
                  color: NeutralColors.black,
                  fontSize: screenHeight * 16 / 720,
                  fontFamily: "IBMPlexSans",
                  fontWeight: FontWeight.w500,
                ),
                textAlign: TextAlign.center,
              ),
            ),
          ),
          Container(
            height: screenHeight * 47.5 / 720,
            padding: EdgeInsets.only(
                left: 40.0 / 360 * screenWidth,
                right: 40.0 / 360 * screenWidth),
            child: Row(
              children: [
                /// yes button in exit popup
                Container(
                  width: screenWidth * 130 / 360,
                  height: screenHeight * 40 / 720,
                  margin: EdgeInsets.only(top: 1 / 720 * screenHeight),
                  child: FlatButton(
                    onPressed: () {
                      Navigator.pop(context, true);
                      //AppRoutes.push(context, EmaximizerSummaryScreen(questionsStatus,totalQuestions));
                    },
                    color: NeutralColors.ice_blue,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(Radius.circular(2)),
                    ),
                    textColor: NeutralColors.blue_grey,
                    padding: EdgeInsets.all(0),
                    child: Text(
                      PrepareParagraphScreenStrings.yesForPopUpExit,
                      style: TextStyle(
                        fontSize: screenHeight * 14 / 678,
                        fontFamily: "IBMPlexSans",
                        fontWeight: FontWeight.w500,
                      ),
                      textAlign: TextAlign.center,
                    ),
                  ),
                ),
                Spacer(),
                /// no button in exit popup
                Container(
                  width: screenWidth * 130 / 360,
                  height: screenHeight * 40 / 720,
                  // margin: EdgeInsets.only(top: 1/720*screenHeight),
                  child: FlatButton(
                    onPressed: () => _emaxControllerForPopUp.close(),
                    color: Color.fromARGB(255, 0, 171, 251),
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(Radius.circular(3)),
                    ),
                    textColor: NeutralColors.pureWhite,
                    padding: EdgeInsets.all(0.0),
                    child: Text(
                      PrepareParagraphScreenStrings.noForPopUpExit,
                      style: TextStyle(
                        fontSize: screenHeight * 14 / 678,
                        fontFamily: "IBMPlexSans",
                        fontWeight: FontWeight.w500,
                      ),
                      textAlign: TextAlign.center,
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
//webViewForOptions
  String  player(String value) {
    String _player = '''
   
    <html>
   <head>
               <meta name='viewport' content='width=device-width,initial-scale=1,maximum-scale=1'>
          </head>     
    <link href='https://fonts.googleapis.com/css?family=IBM Plex Sans' rel='stylesheet'>
    <body>
   
   
    <div id ="PSGQuestion1" style="font-family: 'IBM Plex Sans',sans-serif;">$value</div>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.7/latest.js?config=TeX-MML-AM_CHTML"></script>
    <script>
    var mathpsg1ques = document.getElementById("PSGQuestion1");
   
    MathJax.Hub.Queue(["Typeset",MathJax.Hub,mathpsg1ques]);


    </script>
    </body>
    </html>
   
    ''';
    return _player;
    //return 'data:text/html;base64,${base64Encode(const Utf8Encoder().convert(_player))}';
  }
  String  webViewForOptions(String value,Color color) {
    var cc ;
    cc = '#${color.red.toRadixString(16).padLeft(2, '0')}${color.green.toRadixString(16).padLeft(2, '0')}${color.blue.toRadixString(16).padLeft(2, '0')}${color.alpha.toRadixString(16).padLeft(2, '0')}';

    String _player = '''
    <html>
    <head>
               <meta name='viewport' content='width=device-width,initial-scale=1,maximum-scale=1'>
          </head>     
          <link href='https://fonts.googleapis.com/css?family=IBM Plex Sans' rel='stylesheet'>
           <body style="background-color: $cc;">
  
    <div id ="PSGQuestion1" style="font-family: 'IBM Plex Sans',sans-serif;">$value</div>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.7/latest.js?config=TeX-MML-AM_CHTML"></script>
    <script>
    var mathpsg1ques = document.getElementById("PSGQuestion1");
   
    MathJax.Hub.Queue(["Typeset",MathJax.Hub,mathpsg1ques]);


    </script>
    </body>
    </html>
   
    ''';
    return _player;
    //return 'data:text/html;base64,${base64Encode(const Utf8Encoder().convert(_player))}';
  }

  colorLogic(int index,int i){
    if(attemptList[index]==true) {
      if (selectedOptionIdList[index] == finalTotalOptionIDAccordingToQuestionsForTrack[index][i].toString()) {
        print("options selected");
        print(questionsStatus[index]);
        if (questionsStatus[index] == true) {
          return  widget.backgroundColor = PrimaryColors.kelly_green.withOpacity(0.1);
        }
        else
        if (questionsStatus[index] == false) {
          print("here");
          return  widget.backgroundColor = PrimaryColors.dark_coral.withOpacity(0.1);
        }
        else {
          return  widget.backgroundColor = Colors.transparent;
        }
      } else {
        return widget.backgroundColor =Colors.transparent;
      }
    }else{
      return widget.backgroundColor = Colors.transparent;
    }
  }

  /// status ........... ///
  Widget _status() {
    var _mediaQueryData = MediaQuery.of(context);
    screenWidthTotal = _mediaQueryData.size.width;
    screenHeightTotal = _mediaQueryData.size.height;
    _safeAreaHorizontal =
        _mediaQueryData.padding.left + _mediaQueryData.padding.right;
    _safeAreaVertical =
        _mediaQueryData.padding.top + _mediaQueryData.padding.bottom;

    screenHeight = screenHeightTotal - _safeAreaVertical;
    screenWidth = screenWidthTotal - _safeAreaHorizontal;

    if (emaxHeader_status_question == 1) {
      return Container(
        height: 505 / 720 * screenHeight,
        margin: EdgeInsets.only(
            top: 40 / 720 * screenHeight, bottom: 23 / 720 * screenHeight),
        child: ShowAnswersScreenEmax(
          value: currentPage,
          stopwatch: widget.stopwatch,
          backtoques_num: emaxPositionForQuestion,
          showAnswer: showAnswerExplanation[emaxPositionForQuestion - 1],
          showVideo: showAnswerVideo[emaxPositionForQuestion - 1],
          EmaximserQuestionsScreen: this,
          statusForQuestion: questionsStatus[emaxPositionForQuestion - 1].toString(),
          correctAnswer: correctAnswerList[emaxPositionForQuestion-1],
        ),
      );
      //return ShowAnswerScreen(value:currentPage,);
    }
    else if (emaxHeader_status_question == 2) {
      return Container(
        height: 505 / 720 * screenHeight,
        margin: EdgeInsets.only(
            top: 40 / 720 * screenHeight, bottom: 23 / 720 * screenHeight),
        child: EmaximiserStatisticScreen(
          stopwatch: widget.stopwatch,
          bargraphActiveStatus: widget.emaxStatisticsCheck,
          backtoquesnum_bargraph: emaxPositionForQuestion,
          EmaximserQuestionScreen: this,
          timeTakenForQuestion: totalTimeTakenPerQuestions[emaxPositionForQuestion - 1],
          statusForQuestion: questionsStatus[emaxPositionForQuestion - 1].toString(),
        ),
      );
      //return PrepareStatisticScreen();
    }
    else {
      return totalQuestions.length == 0
          ? Container(
        // margin: EdgeInsets.only(top: 40/720 * screenHeight,bottom:23/720* screenHeight),
          height: 720 / 720 * screenHeight,
          child: Center(child: CircularProgressIndicator()))
          : GestureDetector(
        onTap: () {
          setState(() {
            emaxOverLayEntryCheck
                ? emaxOverLayEntryCheck = false
                : emaxOverLayEntryCheck = true;
            hide();
            FocusScope.of(context).requestFocus(new FocusNode());

          });
        },
        child: Container(
          height: 505 / 720 * screenHeight,
          margin: EdgeInsets.only(
              top: 40 / 720 * screenHeight,
              bottom: 23 / 720 * screenHeight),
          child: Column(
            children: <Widget>[
              Container(
                height: (505 / 720) * screenHeight,
                width: (360 / 360) * screenWidth,
                child: PageView.builder(
                  controller: pageViewController,
                  physics: NeverScrollableScrollPhysics(),
                  itemBuilder: (BuildContext context, int index) {
                    return
                      /// LAYOUT FOR QUESTION & ANSWER
                      Container(
                        // height:(450/720)*screenHeight,
                        child: (paragraphCheck[index] != null &&
                            checking != false)
                        /// paragraph part .............. ////
                            ? Stack(
                          alignment: Alignment.bottomCenter,
                          children: <Widget>[
                            ProgressHUD(
                              color: Colors.white,
                              child: Column(
                                  mainAxisSize: MainAxisSize.min,
                                  mainAxisAlignment:
                                  MainAxisAlignment.start,
                                  crossAxisAlignment:
                                  CrossAxisAlignment.stretch,
                                  children: <Widget>[
                                    (directionsForQuestion[index]!="")?
                                    Align(
                                      alignment: Alignment.topLeft,
                                      child: Container(
                                        // color:Theme.of(context).primaryColor,
                                          height: _heightbeforequestionDirection1,
                                          margin: EdgeInsets.only(
                                              left: 20 / 360 * screenWidth,
                                              right: 20 / 360 * screenWidth,bottom:20/720*screenHeight),
                                          child:
                                          // Html(
                                          //   blockSpacing:
                                          //   0.0,
                                          //   useRichText:
                                          //   false,
                                          //   // backgroundColor:Colors.red ,
                                          //   data:
                                          //   directionsForQuestion[index],
                                          //   defaultTextStyle:
                                          //   TextStyle(
                                          //     color: Color.fromARGB(255, 0, 3, 44),
                                          //     fontSize: 14 / 360 * screenWidth,
                                          //     fontFamily: "IBMPlexSans",
                                          //     fontWeight: FontWeight.w500,
                                          //   ),
                                          // )
                                          WebViewPlus(

                                            //initialUrl: player(directionsForQuestion[index]),
                                            onWebViewCreated: (controller) {
                                              this.webViewPlusController = controller;
                                              controller.loadString(player(directionsForQuestion[index]));
                                              setState((){
                                                _heightbeforequestionDirection1 = 1;
                                                directionForParagraphForWebViewLoader=true;
                                              });

                                            },
                                            onPageFinished: (url) {
                                              webViewPlusController.getHeight().then((double height) {
                                                print("Height:  " + height.toString());
                                                setState(() {
                                                  _heightbeforequestionDirection1 = height;
                                                  _timer = new Timer(const Duration(milliseconds: 600), () {
                                                  setState(() {
                                                    directionForParagraphForWebViewLoader=false;
                                                  });
                                                });
                                                });
                                                
                                              });
                                            },
                                            javascriptMode: JavascriptMode.unrestricted,
                                          )
                                      ),
                                    ):Text(""),
                                    Expanded(
                                      child: Container(
                                        height: 505 / 720 * screenHeight,
                                        margin: EdgeInsets.only(
                                            top: 0 / 720 * screenHeight,
                                            bottom:
                                            60 / 720 * screenHeight,
                                            left: 20 / 360 * screenWidth,
                                            right:
                                            20 / 360 * screenWidth),
                                        child: Container(
                                          height: _heightbeforequestionParagraph,
                                          child: SingleChildScrollView(
                                            //
                                              scrollDirection:
                                              Axis.vertical,
                                              child:
                                              // Html(
                                              //   blockSpacing: 0.0,
                                              //   useRichText: false,
                                              //   // backgroundColor:Colors.red ,
                                              //   data: paragraphCheck[
                                              //   index] ??
                                              //       '',
                                              //   defaultTextStyle:
                                              //   TextStyle(
                                              //     color: Color.fromARGB(
                                              //         255, 0, 3, 44),
                                              //     fontSize: 14 /
                                              //         360 *
                                              //         screenWidth,
                                              //     fontFamily:
                                              //     "IBMPlexSans",
                                              //     fontWeight:
                                              //     FontWeight.w500,
                                              //   ),
                                              // )
                                              WebViewPlus(

                                                //initialUrl: player(paragraphCheck[index]),
                                                onWebViewCreated: (controller) {
                                                  this.webViewPlusControllerrr = controller;
                                                  controller.loadString(player(paragraphCheck[index]));
                                                  setState((){
                                                    _heightbeforequestionParagraph = 1;
                                                    paragraphForForWebViewLoader=true;
                                                  });

                                                },

                                                onPageFinished: (url) {
                                                  webViewPlusControllerrr.getHeight().then((double height) {
                                                    print("Height:  " + height.toString());
                                                    setState(() {
                                                      _heightbeforequestionParagraph = height;
                                                       _timer = new Timer(const Duration(milliseconds: 600), () {
                                                    setState(() {
                                                      paragraphForForWebViewLoader=false;
                                                    });
                                                  });
                                                    });
                                                  });
                                                },
                                                javascriptMode: JavascriptMode.unrestricted,
                                              )
                                          ),
                                        ),
                                      ),
                                    ),
                                  ]),
                               inAsyncCall: (directionsForQuestion[index]!="")?(directionForParagraphForWebViewLoader==false&&paragraphForForWebViewLoader==false)?false:true
                          :(paragraphForForWebViewLoader==false)?false:true,
                            ),
                            Positioned(
                              //bottom: -0.009 * screenHeight,
                              left: 0.05 * screenWidth,
                              right: 0.05 * screenWidth,
                              child: InkWell(
                                child: Container(
                                  height:
                                  40 / 720 * screenHeight,
                                  decoration: BoxDecoration(
                                      borderRadius:
                                      BorderRadius.all(
                                          Radius.circular(
                                              2)),
                                      gradient: LinearGradient(
                                        // begin: Alignment(1.0199999809265137, 1.0199999809265137),
                                          end: Alignment(
                                              1.0199999809265137,
                                              1.0099999904632568),
                                          colors: [
                                            SemanticColors
                                                .light_purpely,
                                            SemanticColors
                                                .dark_purpely
                                          ])),
                                  child: Row(
                                    crossAxisAlignment:
                                    CrossAxisAlignment
                                        .center,
                                    mainAxisAlignment:
                                    MainAxisAlignment
                                        .center,
                                    children: [
                                      Center(
                                        child: Text(
                                          Preparequestions
                                              .qoToQuestion,
                                          style: TextStyle(
                                            color: NeutralColors
                                                .pureWhite,
                                            fontSize: 14 /
                                                360 *
                                                screenWidth,
                                            fontFamily:
                                            "IBMPlexSans",
                                            fontWeight:
                                            FontWeight.w500,
                                          ),
                                          textAlign:
                                          TextAlign.center,
                                        ),
                                      ),
                                      Center(
                                        child: Container(
                                          margin: EdgeInsets.only(
                                              left: 10 /
                                                  360 *
                                                  screenWidth),
                                          child: Image.asset(
                                            "assets/images/back-white.png",
                                            width: 18 /
                                                360 *
                                                screenWidth,
                                            height: 16 /
                                                720 *
                                                screenHeight,
                                            fit: BoxFit
                                                .scaleDown,
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                onTap: () {
                                  setState(() {
                                    checking = false;
                                    // AppRoutes.pushWithAnmation(context, PracticeQuestionsScreen(header_status: 3,));
                                  });
                                },
                              ),
                            )
                          ],
                        )

                        /// questions and answers part .............. ///
                            :
                        Stack(
                          alignment: Alignment.bottomCenter,
                          children: <Widget>[
                            ProgressHUD(
                              color:Colors.white,
                              child: Column(
                                mainAxisSize: MainAxisSize.min,
                                mainAxisAlignment:
                                MainAxisAlignment.start,
                                crossAxisAlignment:
                                CrossAxisAlignment.stretch,
                                children: [
                                  /// back to paragraph ............///
                                  (paragraphCheck[index] != null)
                                      ? Align(
                                    alignment:
                                    Alignment.topLeft,
                                    child: InkWell(
                                      child: Container(
                                        width: (170 / 360) *
                                            screenWidth,
                                        height: (23 / 720) *
                                            screenHeight,
                                        child: Row(
                                          children: [
                                            Container(
                                                margin: EdgeInsets.only(
                                                    left: (20 /
                                                        360) *
                                                        screenWidth,
                                                    right: (8 /
                                                        360) *
                                                        screenWidth),
                                                child: Icon(
                                                  Icons
                                                      .arrow_back_ios,
                                                  color: PrimaryColors
                                                      .azure_Dark,
                                                  size: 15 /
                                                      720 *
                                                      screenHeight,
                                                )),
                                            Container(
                                              child: Text(
                                                Preparequestions
                                                    .back,
                                                style:
                                                TextStyle(
                                                  color: PrimaryColors
                                                      .azure_Dark,
                                                  fontSize: (14 /
                                                      360) *
                                                      screenWidth,
                                                  fontFamily:
                                                  "IBMPlexSans",
                                                ),
                                                textAlign:
                                                TextAlign
                                                    .left,
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                      onTap: () {
                                        setState(() {
                                          checking = true;
                                          //        emaximSoloutionCheck=true;
                                        });
                                      },
                                    ),
                                  )
                                      : Text(""),
                                  /// QUESTION AND ANSWER BEFORE SUBMIT
                                  Expanded(
                                    child: DraggableScrollbar(
                                      controller: controller,
                                      heightScrollThumb: 78.0 / 720 *
                                          screenHeight,
                                      weightScrollThumb:
                                      3 / 360 * screenWidth,
                                      colorScrollThumb:
                                      NeutralColors
                                          .colors_scroll,
                                      marginScrollThumb:
                                      EdgeInsets.only(
                                          right: 6 /
                                              360 *
                                              screenWidth),
                                      child: LayoutBuilder(
                                        builder: (BuildContext
                                        context,
                                            BoxConstraints
                                            viewportConstraints) {
                                              

                                          return ListView.builder(
                                            padding: EdgeInsets.only(top:21/720*screenHeight),

                                            controller:
                                            controller,
                                            itemCount:
                                            finalTotalOptionsAccordingToQuestions[index].length,
                                            itemBuilder:
                                                (context, i) {
                                                  // var heightlist = [];
                                                  // var valuelistt = [];
                                                  // for(int x = 0; x < finalTotalOptionsAccordingToQuestions[index].length;x++)
                                                  // {
                                                    
                                                  //   for(int y= 0;y< finalTotalOptionsAccordingToQuestions[index][i].length;y++){
                                                  //     valuelistt.add(1.0);
                                                  //     break;
                                                  //   }
                                                  //   heightlist.add(valuelistt);
                                                    
                                                  // }
                                                  // print("list of the list"+heightlist.toString());

                                              return Container(
                                                width: 360 / 360 * screenWidth,
                                                margin: EdgeInsets.only(
                                                    bottom: (10 / 720) * screenHeight),
                                                child: Column(
                                                  children: <
                                                      Widget>[
                                                    i == 0
                                                    /// direction .......... ///
                                                        ? (directionsForQuestion[index]!="")?
                                                    Container(
                                                        height: _heightbeforequestionDirection,
                                                        // color:Theme.of(context).primaryColor,
                                                        margin: EdgeInsets.only(
                                                            left: 20 / 360 * screenWidth,
                                                            right: 20 / 360 * screenWidth),
                                                        child:
                                                        // Html(

                                                        //   blockSpacing:
                                                        //   0.0,
                                                        //   useRichText:
                                                        //   false,
                                                        //   // backgroundColor:Colors.red ,
                                                        //   data:player
                                                        //   (directionsForQuestion[index]),
                                                        //   defaultTextStyle:
                                                        //   TextStyle(
                                                        //     color: Color.fromARGB(255, 0, 3, 44),
                                                        //     fontSize: 14 / 360 * screenWidth,
                                                        //     fontFamily: "IBMPlexSans",
                                                        //     fontWeight: FontWeight.w500,
                                                        //   ),
                                                        // )
                                                        // WebView(

                                                        //   initialUrl: player(directionsForQuestion[index]),
                                                        //   javascriptMode: JavascriptMode.unrestricted,
                                                        // )
                                                        WebViewPlus(
                                                          //initialUrl: (player(directionsForQuestion[index])),
                                                          //gestureRecognizers: null,
                                                            onWebViewCreated: (controller) {
                                                              this.webViewPlusController1 = controller;
                                                              controller.loadString(player(directionsForQuestion[index]));
                                                              setState((){
                                                                _heightbeforequestionDirection = 1;
                                                                directionBeforeAttemptForWebViewLoader=true;
                                                              });

                                                            },
                                                            onPageStarted: (url){
                                                              setState(() {
                                                                directionBeforeAttemptForWebViewLoader=true;
                                                              });
                                                            },
                                                            onPageFinished: (url) {
                                                              webViewPlusController1.getHeight().then((double height) {
                                                                print("Height:  " + height.toString());
                                                                setState(() {
                                                                  _heightbeforequestionDirection = height;
                                                                  _timer = new Timer(const Duration(milliseconds: 600), () {
                                                                    setState(() {
                                                                      directionBeforeAttemptForWebViewLoader=false;
                                                                    });
                                                                  });
                                                                  //handleLoad(true);
                                                                });
                                                              });
                                                            },
                                                            javascriptMode: JavascriptMode.unrestricted)
                                                    ):Text("")
                                                        : Container(),
                                                    i == 0
                                                    /// question .......... ///
                                                        ? (totalQuestions[index]!="")?
                                                    Container(
                                                        height : _heightbeforequestion,
                                                        margin: EdgeInsets.only(
                                                          left: 20 / 360 * screenWidth,
                                                          right: 20 / 360 * screenWidth,
                                                        ),
                                                        //padding: EdgeInsets.only(bottom: 20/ 720 * screenHeight),
                                                        child:
                                                        // Html(
                                                        //   blockSpacing:
                                                        //   0.0,
                                                        //   useRichText:
                                                        //   false,
                                                        //   // bakgroundColor:Colors.red ,
                                                        //   data:
                                                        //   player(totalQuestions[index]),
                                                        //   //totalQuestions[index],
                                                        //   defaultTextStyle:
                                                        //   TextStyle(
                                                        //     color: Color.fromARGB(255, 0, 3, 44),
                                                        //     fontSize: 14 / 360 * screenWidth,
                                                        //     fontFamily: "IBMPlexSans",
                                                        //     fontWeight: FontWeight.w500,
                                                        //   ),
                                                        // )
                                                        WebViewPlus(
                                                          //initialUrl: player(totalQuestions[index]),
                                                          
                                                          onWebViewCreated: (controller) {
                                                            this.webViewPlusController = controller;
                                                            controller.loadString(player(totalQuestions[index]));
                                                            setState((){
                                                              _heightbeforequestion = 1;
                                                              questionBeforeAttemptForWebViewLoader=true;
                                                            });

                                                          },
                                                          onPageStarted:(url){
                                                            setState(() {
                                                              questionBeforeAttemptForWebViewLoader=true;
                                                            });
                                                          },
                                                          onPageFinished: (url) {

                                                            webViewPlusController.getHeight().then((double height) {
                                                              print("Height:  " + height.toString());
                                                              setState(() {
                                                                _heightbeforequestion = height;
                                                                _timer = new Timer(const Duration(milliseconds: 600), () {
                                                                  setState(() {
                                                                    questionBeforeAttemptForWebViewLoader=false;
                                                                  });
                                                                });
                                                                //handleLoad(true);
                                                              });
                                                            });
                                                          },
                                                          javascriptMode: JavascriptMode.unrestricted,
                                                        )
                                                      //TeXView(child: TeXViewDocument(player(totalQuestions[index])))
                                                    ):Text("")
                                                        : Container(),
                                                    /// question status .........
                                                    attemptList[index]==true? i == 0 ? Align(
                                                      alignment:
                                                      Alignment.topLeft,
                                                      child:
                                                      Container(
                                                        decoration:
                                                        BoxDecoration(
                                                          border: Border.all(
                                                            color: (questionsStatus[index] == true) ? PrimaryColors.kelly_green : (questionsStatus[index] == false) ? PrimaryColors.dark_coral : Colors.yellow,
                                                            width: 1,
                                                          ),
                                                          borderRadius: BorderRadius.all(Radius.circular(2)),
                                                        ),
                                                        margin:
                                                        EdgeInsets.only(
                                                          left: 20 / 360 * screenWidth,
                                                          top: 20 / 720 * screenHeight,
                                                          right: 10 / 360 * screenWidth,
                                                        ),
                                                        child:
                                                        Padding(
                                                          padding: EdgeInsets.only(left: 10 / 720 * screenWidth, right: 10 / 720 * screenWidth, top: 2 / 720 * screenHeight, bottom: 2 / 720 * screenHeight),
                                                          child: Text(
                                                            (questionsStatus[index] == true) ? Preparequestions.correctStatus : (questionsStatus[index] == false) ? Preparequestions.wrongStatusForReview : Preparequestions.skippedStatus,
                                                            style: TextStyle(color: (questionsStatus[index] == true) ? PrimaryColors.kelly_green : (questionsStatus[index] == false) ? PrimaryColors.dark_coral : Colors.yellow, fontWeight: FontWeight.w500, fontFamily: "IBMPlexSans", fontStyle: FontStyle.normal, fontSize: 12.0 / 360 * screenWidth),
                                                            textAlign: TextAlign.center,
                                                          ),
                                                        ),
                                                      ),
                                                    ) : Container(color:Colors.red) :Text(""),
                                                    /// options ..............
                                                    questionsType[index].toString().toLowerCase() == "MCQ".toLowerCase() ?
                                                    /// mcq type questions layout start ///
                                                    GestureDetector(
                                                      onTap:() {
                                                        if(attemptList[index]==true){

                                                        }
                                                        else{
                                                          setState(() {
                                                            if (OPTION_ACTIVE == true) {
                                                              SUB_FLAG = true;
                                                            }
                                                          });
                                                          answer_status = null;
                                                          isOptionsSelected = true;
                                                          _onSelectedOption(i,index);
                                                          setState(() {
                                                            correctAnswerAccordingToQues = finalTotalAnswerListAccordingToQuestion[index][i];
                                                            if (OPTION_ACTIVE == false) {
                                                              if (correctAnswerAccordingToQues.toString() == "true") {
                                                                answer_status = "CORRECT";
                                                                if (emaxPositionForQuestion == totalQuestions.length) {
                                                                  //AppRoutes.pop(context);
                                                                }
                                                              } else {
                                                                answer_status = "WRONG";
                                                                if (emaxPositionForQuestion == totalQuestions.length) {
//                                            if(isOptionsSelected == true){
//                                              AppRoutes.push(context,PrepareCalculationSummary());
//                                            }
                                                                }
                                                              }
                                                            }

                                                            emaxOverLayEntryCheck ? emaxOverLayEntryCheck = false : emaxOverLayEntryCheck = true;
                                                            hide();
                                                            FocusScope.of(context).requestFocus(new FocusNode());

                                                          });
                                                        }

                                                      },
                                                      child:
                                                      Row(
                                                        children: [
                                                          Container(
                                                            height: _heightbeforequestionOption,
                                                            width: (3/360)*screenWidth,
                                                            color: attemptList[index]?
                                                            (selectedOptionIdList[index] == finalTotalOptionIDAccordingToQuestionsForTrack[index][i].toString())
                                                                ? (questionsStatus[index] == true) ? PrimaryColors.kelly_green : (questionsStatus[index] == false) ? PrimaryColors.dark_coral : Colors.white
                                                                :NeutralColors.pureWhite
                                                             :(_selectedIndexForOption != null && _selectedIndexForOption == i && selectedIndexForQues == index)
                                                                 ? answer_status == null ? NeutralColors.purpel_box : (answer_status == "CORRECT" ? NeutralColors.scroll : SemanticColors.dark_coral)
                                                                 : NeutralColors.pureWhite
                                                          ),
                                                          Container(
                                                            margin:
                                                            EdgeInsets.only(top: 10 / 720 * screenHeight, bottom: i == (finalTotalOptionsAccordingToQuestions[index].length - 1) ? 40 / 720 * screenHeight : 10 / 720 * screenHeight),
                                                            decoration:
                                                            attemptList[index]==true?
                                                            BoxDecoration(
                                                              gradient: LinearGradient(
                                                                colors : [NeutralColors.pureWhite.withOpacity(0.1), SemanticColors.pureWhite.withOpacity(0.1)],
                                                                  // colors: selectedOptionIdList[index] == "-1"
                                                                  //     ? [
                                                                  //   NeutralColors.pureWhite.withOpacity(0.1),
                                                                  //   SemanticColors.pureWhite.withOpacity(0.1)
                                                                  // ]
                                                                  //     : selectedOptionIdList[index] == finalTotalOptionIDAccordingToQuestionsForTrack[index][i].toString()
                                                                  //     ? [
                                                                  //   (questionsStatus[index] == true) ? PrimaryColors.kelly_green.withOpacity(0.1) : (questionsStatus[index] == false) ? PrimaryColors.dark_coral.withOpacity(0.1) : Colors.white.withOpacity(0.1),
                                                                  //   (questionsStatus[index] == true) ? PrimaryColors.kelly_green.withOpacity(0.1) : (questionsStatus[index] == false) ? PrimaryColors.dark_coral.withOpacity(0.1) : Colors.white.withOpacity(0.1),
                                                                  // ]
                                                                  //     : [
                                                                  //   NeutralColors.pureWhite.withOpacity(0.1),
                                                                  //   SemanticColors.pureWhite.withOpacity(0.1)
                                                                  // ]
                                                                //  colors: [NeutralColors.greycolor.withOpacity(0.1),SemanticColors.dark_purpely.withOpacity(0.1)]
                                                              ),
                                                              border: selectedOptionIdList[index] == "-1"
                                                                  ? Border.all(
                                                                color: Colors.transparent,
                                                                width: 1,
                                                              )
                                                                  : selectedOptionIdList[index] == finalTotalOptionIDAccordingToQuestionsForTrack[index][i].toString()
                                                                  ? Border.all(
                                                                //color: (questionsStatus[index] == true) ? PrimaryColors.kelly_green : (questionsStatus[index] == false) ? PrimaryColors.dark_coral : Colors.white,
                                                                color: Colors.white,
                                                                width: 1,
                                                              )
                                                                  : Border.all(
                                                                color: Colors.transparent,
                                                                width: 1,
                                                              ),
                                                              borderRadius:
                                                              BorderRadius.all(Radius.circular(2)),
                                                            ):
                                                            BoxDecoration(
                                                              gradient: LinearGradient(
                                                                  colors: [NeutralColors.pureWhite.withOpacity(0.1), SemanticColors.pureWhite.withOpacity(0.1)],
                                                                  // colors: _selectedIndexForOption != null && _selectedIndexForOption == i && selectedIndexForQues == index
                                                                  //     ? answer_status == null
                                                                  //     ? [
                                                                  //   NeutralColors.greycolor.withOpacity(0.1),
                                                                  //   SemanticColors.dark_purpely.withOpacity(0.1)
                                                                  // ]
                                                                  //     : (answer_status == "CORRECT"
                                                                  //     ? [NeutralColors.scroll.withOpacity(0.1), NeutralColors.scroll.withOpacity(0.1)]
                                                                  //     : [
                                                                  //   SemanticColors.dark_coral.withOpacity(0.1),
                                                                  //   SemanticColors.dark_coral.withOpacity(0.1)
                                                                  // ])
                                                                  //     : [
                                                                  //   NeutralColors.pureWhite.withOpacity(0.1),
                                                                  //   SemanticColors.pureWhite.withOpacity(0.1)
                                                                  // ]
                                                                //  colors: [NeutralColors.greycolor.withOpacity(0.1),SemanticColors.dark_purpely.withOpacity(0.1)]
                                                              ),
                                                              border: Border.all(
                                                                color: Colors.transparent,
                                                                width: 1,
                                                              ),
                                                              borderRadius: BorderRadius.all(Radius.circular(2)),
                                                            ),
                                                            child:
                                                            Row(
                                                              mainAxisAlignment: MainAxisAlignment.center,
                                                              crossAxisAlignment: CrossAxisAlignment.center,
                                                              children: <Widget>[
                                                                /// option letters ////
                                                                Container(
                                                                  height: 15 / 720 * screenHeight,
                                                                  width: 15 / 360 * screenWidth,
                                                                  decoration:attemptList[index]?
                                                                  BoxDecoration(
                                                                  border: Border.all(
                                                                  //color: NeutralColors.scroll,
                                                                   color: (selectedOptionIdList[index] == finalTotalOptionIDAccordingToQuestionsForTrack[index][i].toString())
                                                                       ? (questionsStatus[index] == true) ? PrimaryColors.kelly_green : (questionsStatus[index] == false) ? PrimaryColors.dark_coral : NeutralColors.blue_grey
                                                                       :NeutralColors.blue_grey,
                                                                    width: 1,
                                                                   ),
                                                                 borderRadius: BorderRadius.all(Radius.circular(2)),
                                                                 color:
                                                                     (selectedOptionIdList[index] == finalTotalOptionIDAccordingToQuestionsForTrack[index][i].toString())
                                                                     ? (questionsStatus[index] == true) ? PrimaryColors.kelly_green : (questionsStatus[index] == false) ? PrimaryColors.dark_coral : NeutralColors.pureWhite
                                                                     :NeutralColors.pureWhite,
                                                                   )
                                                                  :BoxDecoration(
                                                                    border: Border.all(
                                                                      //color: NeutralColors.scroll,
                                                                      color: _selectedIndexForOption != null && _selectedIndexForOption == i && selectedIndexForQues == index
                                                                          ? answer_status == null ? NeutralColors.purpel_box : (answer_status == "CORRECT" ? NeutralColors.scroll : SemanticColors.dark_coral)
                                                                          :NeutralColors.blue_grey,
                                                                      width: 1,
                                                                    ),
                                                                    borderRadius: BorderRadius.all(Radius.circular(2)),
                                                                    color:
                                                                    _selectedIndexForOption != null && _selectedIndexForOption == i && selectedIndexForQues == index
                                                                        ? answer_status == null ? NeutralColors.purpel_box : (answer_status == "CORRECT" ? NeutralColors.scroll : SemanticColors.dark_coral)
                                                                        :NeutralColors.pureWhite,
                                                                  ),
                                                                  margin: EdgeInsets.only(left: (20 / 360) * screenWidth),
                                                                  child: Center(
                                                                    child: Text(
                                                                      lettersForQuestions[i],
                                                                      style: TextStyle(
                                                                       // color: Color.fromARGB(255, 0, 180, 16),
                                                                        color:attemptList[index]?
                                                                        (selectedOptionIdList[index] == finalTotalOptionIDAccordingToQuestionsForTrack[index][i].toString())
                                                                            ? (questionsStatus[index]==true)? NeutralColors.pureWhite:(questionsStatus[index]==false)? NeutralColors.pureWhite: NeutralColors.bluey_grey:NeutralColors.bluey_grey
                                                                            :(_selectedIndexForOption != null && _selectedIndexForOption == i && selectedIndexForQues == index)
                                                                            ? answer_status == null ? NeutralColors.pureWhite : (answer_status == "CORRECT" ? NeutralColors.pureWhite : SemanticColors.pureWhite)
                                                                            : NeutralColors.blue_grey,
                                                                        fontSize: 10 / 720 * screenHeight,
                                                                        fontFamily: "IBMPlexSans",
                                                                      ),
                                                                      textAlign: TextAlign.center,
                                                                    ),
                                                                  ),
                                                                ),

                                                                /// options ............. ///
                                                                Container(
                                                                  //   alignment: Alignment.topRight,
                                                                    width: 280 / 360 * screenWidth,
                                                                    height: heightlist[i],
                                                                    color: Colors.transparent,
                                                                    margin: EdgeInsets.only(left: 8 / 360 * screenWidth, right: 20 / 360 * screenWidth),
                                                                    child:
//                                                            Html(
//                                                                customTextAlign: (elem) {
//                                                                  return TextAlign.start;
//                                                                },
//                                                                data: finalTotalOptionsAccordingToQuestions[index][i],
//                                                                defaultTextStyle: TextStyle(
//                                                                  color: Color.fromARGB(255, 0, 3, 44),
//                                                                  fontSize: 14 / 360 * screenWidth,
//                                                                  fontFamily: "IBMPlexSans",
//                                                                  fontWeight: FontWeight.w500,
//                                                                ),
//                                                                customTextStyle: (node, textStyle) {
//                                                                  return TextStyle(
//                                                                    color: Color.fromARGB(255, 0, 3, 44),
//                                                                    fontSize: 14 / 360 * screenWidth,
//                                                                    fontFamily: "IBMPlexSans",
//                                                                    fontWeight: FontWeight.w500,
//                                                                  );
//                                                                },
//                                                                onImageTap: (src) {
//                                                                  setState(() {
//                                                                    if (OPTION_ACTIVE == true) {
//                                                                      SUB_FLAG = true;
//                                                                    }
//                                                                  });
//                                                                  answer_status = null;
//                                                                  isOptionsSelected = true;
//                                                                  _onSelectedOption(i,index);
//                                                                  setState(() {
//                                                                    correctAnswerAccordingToQues = finalTotalAnswerListAccordingToQuestion[index][i];
//                                                                    if (OPTION_ACTIVE == false) {
//                                                                      if (correctAnswerAccordingToQues.toString() == "true") {
//                                                                        answer_status = "CORRECT";
//                                                                        if (emaxPositionForQuestion == totalQuestions.length) {
//                                                                          //AppRoutes.pop(context);
//                                                                        }
//                                                                      } else {
//                                                                        answer_status = "WRONG";
//                                                                        if (emaxPositionForQuestion == totalQuestions.length) {
////                                            if(isOptionsSelected == true){
////                                              AppRoutes.push(context,PrepareCalculationSummary());
////                                            }
//                                                                        }
//                                                                      }
//                                                                    }
//
//                                                                    emaxOverLayEntryCheck ? emaxOverLayEntryCheck = false : emaxOverLayEntryCheck = true;
//                                                                    hide();
//                                                                    FocusScope.of(context).requestFocus(new FocusNode());
//
//                                                                  });
//                                                                  // Display the image in large form.
//                                                                },
//                                                                imageProperties: ImageProperties(
//                                                                  fit: BoxFit.fitHeight,
//                                                                  matchTextDirection: false,
//                                                                  alignment: Alignment.topLeft,
//
//                                                                  //centerSlice: new Rect.fromLTRB(1.0, 0.0, 0.0, 0.0),
//                                                                )
//
////                                                  ImageProperties(
////                                                    alignment: Alignment.topRight,
////                                                    matchTextDirection: true,
////                                                    //color: Colors.orange,
////                                                  ),
//
//                                                            ),
                                                                    WebViewPlus(
                                                                      //initialUrl: (player(directionsForQuestion[index])),
                                                                        onWebViewCreated: (controller) {
                                                                          this.webViewPlusController11 = controller;
                                                                          controller.loadString(player(finalTotalOptionsAccordingToQuestions[index][i]));
                                                                          setState((){
                                                                            _heightbeforequestionOption = 1;
                                                                            heightlist[i] = 1.0;
                                                                            optionsBeforeAttemptForWebViewLoader=true;
                                                                          });
                                                                        },
                                                                        onPageStarted: (url){
                                                                          setState(() {
                                                                            _timer = new Timer(const Duration(milliseconds: 400), () {
                                                                              setState(() {
                                                                                optionsBeforeAttemptForWebViewLoader=true;
                                                                              });
                                                                            });
                                                                          });
                                                                        },
                                                                        onPageFinished: (url) {
                                                                          webViewPlusController11.getHeight().then((double height) {
                                                                            print("Height:  " + height.toString());
                                                                            setState(() {
                                                                              heightlist[i] = height;
                                                                              _heightbeforequestionOption = height;
                                                                              _timer = new Timer(const Duration(milliseconds: 1000), ()
                                                                              {
                                                                                setState(() {
                                                                                  optionsBeforeAttemptForWebViewLoader=false;
                                                                                });
                                                                              });
                                                                              //handleLoad(true);
                                                                            });
                                                                          });
                                                                        },
                                                                        javascriptMode: JavascriptMode.unrestricted)
                                                                )
                                                              ],
                                                            ),
                                                          ),
                                                        ],
                                                      ),
                                                    )
                                                    /// mcq type questions layout end ///
                                                        : i == 0
                                                        ? attemptList[index]==true?Container(
                                                        margin: EdgeInsets.only(top: 30 / 720 * screenHeight),
                                                        height: 150 / 720 * screenHeight,
                                                        color: Color(0xFFF1F0FA),
                                                        child: Container(
                                                          height: 30 / 720 * screenHeight,
                                                          width: 320 / 360 * screenWidth,
                                                          margin: EdgeInsets.only(left: 20 / 360 * screenWidth, right: 20 / 360 * screenWidth, top: 40 / 720 * screenHeight, bottom: 40 / 720 * screenHeight),
                                                          decoration: BoxDecoration(
                                                            color: Color(0xFF2F6F8),
                                                            border: Border.all(
                                                              color: Color(0xFFBEC8CC),
                                                              width: 1,
                                                            ),
                                                            borderRadius: BorderRadius.all(Radius.circular(10)),
                                                          ),
                                                          child: selectedOptionIdList[index] != "-1"
                                                              ? Container(
                                                            margin: EdgeInsets.only(left: 10 / 360 * screenWidth),
                                                            child: Column(
                                                              mainAxisAlignment: MainAxisAlignment.center,
                                                              crossAxisAlignment: CrossAxisAlignment.start,
                                                              children: <Widget>[
                                                                Text(
                                                                  selectedOptionIdList[index],
                                                                  style: TextStyle(
                                                                    color: Colors.black,
                                                                  ),
                                                                  textAlign: TextAlign.start,
                                                                ),
                                                              ],
                                                            ),
                                                          )
                                                              : Text(""),
                                                        )):
                                                    Container(
                                                        margin: EdgeInsets.only(top: 30 / 720 * screenHeight),
                                                        height: 140 / 720 * screenHeight,
                                                        color: Color(0xFFF1F0FA),
                                                        child: Column(
                                                          children: <Widget>[
//                                                        Container(
//                                                            margin: EdgeInsets.only(left: 20 / 360 * screenWidth, right: 20 / 360 * screenWidth, top: 40 / 720 * screenHeight),
//                                                            child: Text(
//                                                              Preparequestions.textForNeTypeQuestion,
//                                                              style: TextStyle(color: Colors.black, fontSize: 14 / 360 * screenWidth),
//                                                            )),
                                                            Container(
                                                              height: 60 / 720 * screenHeight,
                                                              width: 320 / 360 * screenWidth,
                                                              margin: EdgeInsets.only(left: 20 / 360 * screenWidth, right: 20 / 360 * screenWidth, top: 40 / 720 * screenHeight, bottom: 36 / 720 * screenHeight),
                                                              child: TextField(
                                                                focusNode: _focusNode_entertext,
                                                                controller: _answer,
                                                                onSubmitted: (value) {
                                                                },
                                                                keyboardType: questionsType[index].toString().toLowerCase() == "NE".toLowerCase() ? TextInputType.numberWithOptions(signed: true,decimal: true) : questionsType[index].toString().toLowerCase() == "AN".toLowerCase()  ? TextInputType.text : questionsType[index].toString().toLowerCase() == "bn".toLowerCase()?TextInputType.text: TextInputType.text ,
                                                                decoration: InputDecoration(
                                                                  //Add th Hint text here.
                                                                  contentPadding: EdgeInsets.all(10.0),
                                                                  hintText: Preparequestions.hintTextForNaTypeQuestions,
                                                                  border: OutlineInputBorder(
                                                                    borderRadius: BorderRadius.circular(10.0),
                                                                  ),
                                                                ),
                                                              ),
                                                            ),
                                                          ],
                                                        ))
                                                        : Text(""),
                                                  ],
                                                ),
                                              );
                                            },
                                          );
                                        },
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                              opacity:1,
                              inAsyncCall:questionsType[index].toString().toLowerCase() == "MCQ".toLowerCase()?
                              (directionsForQuestion[index]!=""&&totalQuestions[index]!="")?
                              (questionBeforeAttemptForWebViewLoader==false&&optionsBeforeAttemptForWebViewLoader==false&&directionBeforeAttemptForWebViewLoader==false)?false:true
                              :((questionBeforeAttemptForWebViewLoader==false||directionBeforeAttemptForWebViewLoader==false)&&optionsBeforeAttemptForWebViewLoader==false)?false:true:
                              (directionsForQuestion[index]!=""&&totalQuestions[index]!="")?
                              (questionBeforeAttemptForWebViewLoader==false&&directionBeforeAttemptForWebViewLoader==false)?false:true
                              :(questionBeforeAttemptForWebViewLoader==false||directionBeforeAttemptForWebViewLoader==false)?false:true
                              ,
                            ),
                            attemptList[index]==true?
                            /// pre,next,submit button after attempt.......... ///
                            Positioned(
                                child: Container(
                                  height: 40 / 720 * screenHeight,
                                  width: 320 / 360 * screenWidth,
                                  margin: EdgeInsets.only(
                                      left:
                                      (20 / 360) * screenWidth,
                                      right:
                                      (20 / 360) * screenWidth),
                                  // color: Colors.black,
                                  /**LAYOUT FOR PREV,NEXT,SUBMIT BUTTON  AFTER ATTEMPT**/
                                  child: Row(
                                    children: <Widget>[
                                      /**LAYOUT FOR PREV BUTTON**/
                                      Container(
                                        decoration: BoxDecoration(gradient:
                                        LinearGradient(
                                          begin: Alignment(-0.056, 0.431),
                                          end: Alignment(1.076, 0.579),
                                          stops: [0, 1,],
                                          colors: emaxPositionForQuestion == 1
                                              ? [SemanticColors.iceBlue,
                                            NeutralColors.ice_blue,
                                          ]
                                              : [SemanticColors.light_purpely,
                                            NeutralColors.box_color,
                                          ],
                                        )),
                                        width:
                                        105 / 360 * screenWidth,
                                        child: FlatButton(
                                          //color: emaxPositionForQuestion==1?SemanticColors.pureWhite:AccentColors.blueGrey,
                                          textColor: Colors.white,
                                          disabledColor:
                                          SemanticColors
                                              .iceBlue,
                                          disabledTextColor:
                                          NeutralColors.black,
                                          onPressed: () {
                                            emaxPositionForQuestion !=
                                                1
                                                ? setState(() {
                                              pageViewController.previousPage(
                                                  duration:
                                                  _kDuration,
                                                  curve:
                                                  _kCurve);
                                              emaxOverLayEntryCheck
                                                  ? emaxOverLayEntryCheck =
                                              false
                                                  : emaxOverLayEntryCheck =
                                              true;
                                              hide();
                                              FocusScope.of(context).requestFocus(new FocusNode());

                                            })
                                                : null;

                                          },
                                          child: Text(
                                            Preparequestions.prev,
                                            style: TextStyle(
                                              color:
                                              emaxPositionForQuestion !=
                                                  1
                                                  ? SemanticColors
                                                  .pureWhite
                                                  : AccentColors
                                                  .blueGrey,
                                              fontSize: 14,
                                              fontFamily:
                                              "IBMPlexSans",
                                              fontWeight:
                                              FontWeight.w500,
                                            ),
                                            textAlign:
                                            TextAlign.center,
                                          ),
                                        ),
                                      ),
                                      /**LAYOUT FOR SUBMIT BUTTON**/
                                      Container(
                                        width:
                                        110 / 360 * screenWidth,
                                        decoration: BoxDecoration(
                                            gradient:
                                            LinearGradient(
                                              begin: Alignment(
                                                  -0.056, 0.431),
                                              end: Alignment(
                                                  1.076, 0.579),
                                              stops: [
                                                0,
                                                1,
                                              ],
                                              colors: [
                                                GradientColors.iceBlue,
                                                GradientColors.iceBlue,
                                              ],
                                            )),
                                        child: FlatButton(
                                            onPressed: () {
                                            },
                                            child: Text(
                                              Preparequestions
                                                  .submit,
                                              style: TextStyle(
                                                color: AccentColors
                                                    .blueGrey,
                                                fontSize: 14,
                                                fontFamily:
                                                "IBMPlexSans",
                                                fontWeight:
                                                FontWeight.w500,
                                              ),
                                              textAlign:
                                              TextAlign.center,
                                            )),
                                      ),
                                      /**LAYOUT FOR NEXT BUTTON**/
                                      Container(
                                        decoration: BoxDecoration(
                                            gradient:
                                            LinearGradient(
                                              begin: Alignment(
                                                  -0.056, 0.431),
                                              end: Alignment(
                                                  1.076, 0.579),
                                              stops: [
                                                0,
                                                1,
                                              ],
                                              colors:
                                              (attemptList.contains(false)&&emaxPositionForQuestion==totalQuestions.length)
                                                  ? [GradientColors.iceBlue,
                                                GradientColors.iceBlue]
                                                  : [SemanticColors.light_purpely,
                                                NeutralColors.box_color,],
                                            )),
                                        width:
                                        105 / 360 * screenWidth,
                                        child: FlatButton(
                                          onPressed: () {
                                            setState(() {
                                              emaxOverLayEntryCheck
                                                  ? emaxOverLayEntryCheck =
                                              false
                                                  : emaxOverLayEntryCheck =
                                              true;
                                              hide();
                                              FocusScope.of(context).requestFocus(new FocusNode());
                                              getTimeStamp();
                                              //latestTrackNumber = latestTrackNumber++;
                                              if(totalTimeTakenPerQuestions[emaxPositionForQuestion-1].toString().contains(":")){
                                                timeIntotalTimeTakenPerQuestion=totalTimeTakenPerQuestions[emaxPositionForQuestion-1].toString().split(":");
                                              }

                                              if(attemptList[emaxPositionForQuestion-1]==true){

                                                latestTrackNumber=latestTrackNumber+1;
                                                FLog.info(
                                                  dataLogType: "Debug",
                                                  text: latestTrackNumber.toString()+"&&&&&&&&&&&&"+(emaxPositionForQuestion-1).toString()+"&&&&&&&&&"+(subjectNameForTrack[emaxPositionForQuestion-1]).toString()+"&&&&&&&"+(topicnameForTrack[emaxPositionForQuestion-1]).toString()+"&&&&&&&"+userNameForLogInfo.toString()+"&&&&&&"+studentidForTrack.toString()+"======================================================TRACK NUMBER && QUESTION INDEX && SUBJECT NAME && AREA/TOPIC NAME && USER NAME && STUDENT USER ID  WHEN CLICK ON NEXT BUTTON @@@@@@ EMAX",
                                                );
                                                myObj.AddtoSist(Track.Comment(areaNameForTrack[emaxPositionForQuestion-1].toString(),
                                                    //    attemptForTrack,
                                                    true,
                                                    companyCodeForTrack,
                                                    difficultyIdForTrack[emaxPositionForQuestion-1],
                                                    difficultyNameForTrack[emaxPositionForQuestion-1],
                                                    questionsType[emaxPositionForQuestion-1].toString().toLowerCase()=="MCQ".toLowerCase()?null:selectedOptionIdList[emaxPositionForQuestion-1],
                                                    companyCodeForTrack+"_"+studentidForTrack+"_"+testidForTrack+"_"+sectionidForTrack.toString()+"_"+
                                                        itemIDForTrack[emaxPositionForQuestion-1].toString()+"_"+latestTrackNumber.toString(),
                                                    questionsStatus[emaxPositionForQuestion-1],
                                                    isSectionCompletedForTrack,
                                                    itemIDForTrack[emaxPositionForQuestion-1],
                                                    itemtypeForTrack[emaxPositionForQuestion-1],
                                                    //  isMarkedForTrack[emaximPositionForQuestionForTrack],
                                                    bookmarkorUnmarkList[emaxPositionForQuestion-1]?true:false,//marked
                                                    negaticepointsForTrack[emaxPositionForQuestion-1],
                                                    pointsForTrack[emaxPositionForQuestion-1],
                                                    emaxPositionForQuestion-1,
                                                    sectionidForTrack,
                                                    sectionindexForTrack,
                                                    sectionNameForTrack,
                                                    questionsType[emaxPositionForQuestion-1].toString().toLowerCase()=="MCQ".toLowerCase()?selectedOptionIdList[emaxPositionForQuestion-1].toString()!="-1"?selectedOptionIdList[emaxPositionForQuestion-1]:null:null,
                                                    studentidForTrack,
                                                    subjectIdForTrack[emaxPositionForQuestion-1],
                                                    subjectNameForTrack[emaxPositionForQuestion-1],
                                                    testidForTrack.toString(),
                                                    timeRemainingForTrack,
                                                    systemtime,
                                                    timetakenForTrack,
                                                    topicIDForTrack[emaxPositionForQuestion-1],
                                                    topicnameForTrack[emaxPositionForQuestion-1],
                                                    latestTrackNumber,
                                                    0,
                                                    0));
                                                myObj.SyncTrackToApi();
                                                FLog.info(
                                                  dataLogType: "Debug",
                                                  text:  "myObj.SyncTrackToApi()======================== @@@@@@ EMAX"+myObj.SyncTrackToApi().toString(),
                                                );
                                                FLog.info(
                                                  dataLogType: "Debug",
                                                  text:  "TRACK GOT SYNC WHEN CLICK ON NEXT BUTTON  @@@@@@@@ EMAX",
                                                );
                                              }



                                              bookmarkorUnmarkList[emaxPositionForQuestion-1]=bookmarkorUnmarkList[emaxPositionForQuestion-1]?true:false;
                                              if (emaxPositionForQuestion == totalQuestions.length && !attemptList.contains(false)) {
                                                //endTestApi();
                                                AppRoutes.push(context, EmaximizerSummaryScreen(nameOfTheTopic,testIdOfTheTopic,widget.listOfTopic,widget.listOfId,widget.listOfObjectType,widget.listOfStatus,widget.pos,selectedId,selectedStatus,selectedObjectType,selectedvalue,instruction,questionsStatus,totalQuestions,widget.positionForTest));
                                              } else if(emaxPositionForQuestion == totalQuestions.length &&attemptList.contains(false)){
                                                /// pop up for exit button
                                                emaxPopUpForExitButton=true;
                                                _emaxControllerForPopUp.open();
                                              }
                                              pageViewController.nextPage(duration: _kDuration,
                                                  curve: _kCurve);
                                              // isSubmitButtonClicked = false;
                                            });

                                          },
                                          // color: GradientColors.iceBlue,
                                          textColor: Colors.white,
                                          disabledColor: GradientColors.iceBlue,
                                          child: Text(
                                            emaxPositionForQuestion == totalQuestions.length
                                                ? Preparequestions.emaxSubmitForLastQuestion
                                                : Preparequestions.next,
                                            style: TextStyle(
                                              color: (attemptList.contains(false)&&emaxPositionForQuestion==totalQuestions.length)? AccentColors.blueGrey:Colors.white,
                                              fontSize: 14,
                                              fontFamily: "IBMPlexSans",
                                              fontWeight: FontWeight.w500,
                                            ),
                                            textAlign: TextAlign.center,
                                          ),
                                        ),
                                      )
                                    ],
                                  ),
                                )):
                            /// pre,next,submit button before attempt.......... ///
                            Positioned(
                                child: Container(
                                  height: 40 / 720 * screenHeight,
                                  width: 320 / 360 * screenWidth,
                                  margin: EdgeInsets.only(
                                      left:
                                      (20 / 360) * screenWidth,
                                      right:
                                      (20 / 360) * screenWidth),
                                  // color: Colors.black,
                                  /**LAYOUT FOR PREV,NEXT,SUBMIT BUTTON BEFORE ATTEMPT**/
                                  child: Row(
                                    children: <Widget>[
                                      /**LAYOUT FOR PREV BUTTON**/
                                      Container(
                                        decoration: BoxDecoration(
                                            gradient:
                                            LinearGradient(
                                              begin: Alignment(-0.056, 0.431),
                                              end: Alignment(1.076, 0.579),
                                              stops: [0, 1,],
                                              colors: emaxPositionForQuestion == 1
                                                  ? [SemanticColors.iceBlue,
                                                NeutralColors.ice_blue,]
                                                  : [SemanticColors.light_purpely,
                                                NeutralColors.box_color,
                                              ],
                                            )),
                                        width:
                                        105 / 360 * screenWidth,
                                        child: FlatButton(
                                          //color: emaxPositionForQuestion==1?SemanticColors.pureWhite:AccentColors.blueGrey,
                                          textColor: Colors.white,
                                          disabledColor:
                                          SemanticColors
                                              .iceBlue,
                                          disabledTextColor:
                                          NeutralColors.black,
                                          onPressed: () {
                                            emaxPositionForQuestion != 1
                                                ? setState(() {
                                              //stopWatch();
                                              pageViewController.previousPage(
                                                  duration: _kDuration,
                                                  curve: _kCurve);
                                              emaxOverLayEntryCheck
                                                  ? emaxOverLayEntryCheck =
                                              false
                                                  : emaxOverLayEntryCheck =
                                              true;
                                              hide();
                                              FocusScope.of(context).requestFocus(new FocusNode());

                                              /// tack for previous button before submitting ................
                                              //stopWatch();
                                              if(attemptList[emaxPositionForQuestion-1]==false){
                                                if(totalTimeTakenPerQuestions[emaxPositionForQuestion-1].toString().contains(":")){
                                                  timeIntotalTimeTakenPerQuestion=totalTimeTakenPerQuestions[emaxPositionForQuestion-1].toString().split(":");
                                                }
                                                if(attemptList[emaxPositionForQuestion-1]!=true){
                                                  latestTrackNumber=latestTrackNumber+1;
                                                }

                                                FLog.info(
                                                  dataLogType: "Debug",
                                                  text: latestTrackNumber.toString()+"&&&&&&&&&&&&"+(emaxPositionForQuestion-1).toString()+"&&&&&&&&&"+(subjectNameForTrack[emaxPositionForQuestion-1]).toString()+"&&&&&&&"+(topicnameForTrack[emaxPositionForQuestion-1]).toString()+"&&&&&&&"+userNameForLogInfo.toString()+"&&&&&&"+studentidForTrack.toString()+"======================================================TRACK NUMBER && QUESTION INDEX && SUBJECT NAME && AREA/TOPIC NAME && USER NAME && STUDENT USER ID  WHEN CLICK ON PREV BUTTON BEFORE SUBMIT @@@@@@@@@@@ EMAX",
                                                );
                                                myObj.AddtoSist(Track.Comment(areaNameForTrack[emaxPositionForQuestion-1].toString(),
                                                    //    attemptForTrack,
                                                    false,
                                                    companyCodeForTrack,
                                                    difficultyIdForTrack[emaxPositionForQuestion-1],
                                                    difficultyNameForTrack[emaxPositionForQuestion-1],
                                                    null,
                                                    companyCodeForTrack+"_"+studentidForTrack+"_"+testidForTrack+"_"+sectionidForTrack.toString()+"_"+
                                                        itemIDForTrack[emaxPositionForQuestion-1].toString()+"_"+latestTrackNumber.toString(),
                                                    null,
                                                    isSectionCompletedForTrack,
                                                    itemIDForTrack[emaxPositionForQuestion-1],
                                                    itemtypeForTrack[emaxPositionForQuestion-1],
                                                    //  isMarkedForTrack[emaximPositionForQuestionForTrack],
                                                    bookmarkorUnmarkList[emaxPositionForQuestion-1]?true:false, //marked,   //marked
                                                    negaticepointsForTrack[emaxPositionForQuestion-1],
                                                    pointsForTrack[emaxPositionForQuestion-1],
                                                    emaxPositionForQuestion-1,
                                                    sectionidForTrack,
                                                    sectionindexForTrack,
                                                    sectionNameForTrack,
                                                    null,
                                                    studentidForTrack,
                                                    subjectIdForTrack[emaxPositionForQuestion-1],
                                                    subjectNameForTrack[emaxPositionForQuestion-1],
                                                    testidForTrack.toString(),
                                                    timeRemainingForTrack,
                                                    systemtime,
                                                    totalTimeTakenPerQuestions[emaxPositionForQuestion-1].toString().contains(":")==true?_getDuration(int.parse(hoursStr),int.parse(minutesStr),int.parse(secondsStr))- _getDuration(int.parse(timeIntotalTimeTakenPerQuestion[0]),int.parse(timeIntotalTimeTakenPerQuestion[1]),int.parse(timeIntotalTimeTakenPerQuestion[2])):_getDuration(int.parse(hoursStr),int.parse(minutesStr),int.parse(secondsStr)),
                                                    topicIDForTrack[emaxPositionForQuestion-1],
                                                    topicnameForTrack[emaxPositionForQuestion-1],
                                                    latestTrackNumber,
                                                    0,
                                                    0));
                                                myObj.SyncTrackToApi();
                                                FLog.info(
                                                  dataLogType: "Debug",
                                                  text:  "myObj.SyncTrackToApi()======================== @@@@@@ EMAX"+myObj.SyncTrackToApi().toString(),
                                                );
                                                FLog.info(
                                                  dataLogType: "Debug",
                                                  text:  "TRACK GOT SYNC WHEN CLICK ON PREV BUTTON BEFORE SUBMIT @@@@@@@@@@ EMAX",
                                                );

                                                bookmarkorUnmarkList[emaxPositionForQuestion-1]=bookmarkorUnmarkList[emaxPositionForQuestion-1]?true:false;
                                                if(totalTimeTakenPerQuestions[emaxPositionForQuestion-1].toString().contains(":")){
                                                  var timeInList=totalTimeTakenPerQuestions[emaxPositionForQuestion-1].toString().split(":");
                                                  // totalTimeTakenPerQuestions[emaxPositionForQuestion-1]= (durationToSec(((_getDuration(int.parse(hoursStr),int.parse(minutesStr),int.parse(secondsStr))))+(_getDuration(int.parse(timeInList[0]),int.parse(timeInList[1]),int.parse(timeInList[2])))));
                                                  totalTimeTakenPerQuestions[emaxPositionForQuestion-1]= (durationToSec(((_getDuration(int.parse(hoursStr),int.parse(minutesStr),int.parse(secondsStr))))));
                                                }else{
                                                  totalTimeTakenPerQuestions[emaxPositionForQuestion-1]= (durationToSec((_getDuration(int.parse(hoursStr),int.parse(minutesStr),int.parse(secondsStr)))+totalTimeTakenPerQuestions[emaxPositionForQuestion-1]));
                                                }
                                              }
                                              stopWatch();
                                            })
                                                : null;


                                          },
                                          child: Text(
                                            Preparequestions.prev,
                                            style: TextStyle(
                                              color:
                                              emaxPositionForQuestion !=
                                                  1
                                                  ? SemanticColors
                                                  .pureWhite
                                                  : AccentColors
                                                  .blueGrey,
                                              fontSize: 14,
                                              fontFamily:
                                              "IBMPlexSans",
                                              fontWeight:
                                              FontWeight.w500,
                                            ),
                                            textAlign:
                                            TextAlign.center,
                                          ),
                                        ),
                                      ),
                                      /**LAYOUT FOR SUBMIT BUTTON**/
                                      OPTION_ACTIVE
                                          ? (SUB_FLAG
                                          ? Container(
                                        width: 110 /
                                            360 *
                                            screenWidth,
                                        decoration:
                                        BoxDecoration(
                                            gradient:
                                            LinearGradient(
                                              begin:
                                              Alignment(
                                                  -0.056,
                                                  0.431),
                                              end: Alignment(
                                                  1.076,
                                                  0.579),
                                              stops: [
                                                0,
                                                1,
                                              ],
                                              colors: [
                                                SemanticColors
                                                    .light_purpely,
                                                NeutralColors
                                                    .box_color,
                                              ],
                                            )),
                                        child: FlatButton(
                                            onPressed:
                                                () {
                                              setState(
                                                      () {

                                                    OPTION_ACTIVE = false;
                                                    SUBMIT_ACTIVE = true;
                                                    ANSWER_SUBMITTED = true;

                                                  });

                                              if(questionsType[emaxPositionForQuestion-1] == "MCQ"){
                                                (isOptionsSelected ==
                                                    true)
                                                    ? setState(
                                                        () {
                                                      if (correctAnswerAccordingToQues.toString() ==
                                                          "true") {
                                                        answer_status = "CORRECT";
                                                        INDICATED_CARD = true;
                                                      } else {
                                                        answer_status = "WRONG";
                                                        INDICATED_CARD = false;

                                                      }
//
                                                    })
                                                    : Text(
                                                    "");
//
                                                finalanswer_status = answer_status;
                                              }
                                              else{

                                                List data1 = [];
                                                data1 = finalTotalOptionsAccordingToQuestions[index];
                                                for(int i=0;i<data1.length;i++){
                                                  if(_parseHtmlString(data1[i]) == answerEntered){
                                                    if(finalTotalAnswerListAccordingToQuestion[emaxPositionForQuestion-1][i].toString() == "true"){
                                                      setState(() {
                                                        answer_status = "CORRECT";
                                                      });

                                                    }else{
                                                      setState(() {
                                                        answer_status = "WRONG";
                                                      });

                                                    }
                                                    break;
                                                  }else{
                                                    setState(() {
                                                      answer_status = "WRONG";
                                                    });

                                                  }
                                                }
                                                finalanswer_status = answer_status;
                                              }
                                              emaxOverLayEntryCheck
                                                  ? emaxOverLayEntryCheck =
                                              false
                                                  : emaxOverLayEntryCheck =
                                              true;
                                              hide();
                                              FocusScope.of(context).requestFocus(new FocusNode());

                                              selectedOptionidaftersubmit = _selectedIndexForOption;
                                              getTimeStamp();
                                              if(totalTimeTakenPerQuestions[emaxPositionForQuestion-1].toString().contains(":")){
                                                timeIntotalTimeTakenPerQuestion=totalTimeTakenPerQuestions[emaxPositionForQuestion-1].toString().split(":");
                                              }
                                              if(attemptList[emaxPositionForQuestion-1]!=true){
                                                latestTrackNumber=latestTrackNumber+1;
                                                FLog.info(
                                                  dataLogType: "Debug",
                                                  text: latestTrackNumber.toString()+"&&&&&&&&&&&&"+(emaxPositionForQuestion-1).toString()+"&&&&&&&&&"+(subjectNameForTrack[emaxPositionForQuestion-1]).toString()+"&&&&&&&"+(topicnameForTrack[emaxPositionForQuestion-1]).toString()+"&&&&&&&"+userNameForLogInfo.toString()+"&&&&&&"+studentidForTrack.toString()+"======================================================TRACK NUMBER && QUESTION INDEX && SUBJECT NAME && AREA/TOPIC NAME && USER NAME && STUDENT USER ID  WHEN CLICK ON SUBMIT BUTTON BEFORE SUBMIT @@@@@@@@ EMAX",
                                                );
                                                myObj.AddtoSist(Track.Comment(areaNameForTrack[emaxPositionForQuestion-1].toString(),
                                                    //    attemptForTrack,
                                                    true,
                                                    companyCodeForTrack,
                                                    difficultyIdForTrack[emaxPositionForQuestion-1],
                                                    difficultyNameForTrack[emaxPositionForQuestion-1],
                                                    questionsType[emaxPositionForQuestion-1].toString().toLowerCase()=="MCQ".toLowerCase()?null:EnterResponse ? answerEntered :enteredTextForTrack[emaxPositionForQuestion-1],
                                                    companyCodeForTrack+"_"+studentidForTrack+"_"+testidForTrack+"_"+sectionidForTrack.toString()+"_"+
                                                        itemIDForTrack[emaxPositionForQuestion-1].toString()+"_"+latestTrackNumber.toString(),
                                                    EnterResponse ? ((answer_status == "CORRECT")?true:false) : ((answer_status == "CORRECT")?true:false),//isCorrect
                                                    isSectionCompletedForTrack,
                                                    itemIDForTrack[emaxPositionForQuestion-1],
                                                    itemtypeForTrack[emaxPositionForQuestion-1],
                                                    //  isMarkedForTrack[emaximPositionForQuestionForTrack],
                                                    bookmarkorUnmarkList[emaxPositionForQuestion-1]?true:false, //marked,   //marked
                                                    negaticepointsForTrack[emaxPositionForQuestion-1],
                                                    pointsForTrack[emaxPositionForQuestion-1],
                                                    emaxPositionForQuestion-1,
                                                    sectionidForTrack,
                                                    sectionindexForTrack,
                                                    sectionNameForTrack,
                                                    questionsType[emaxPositionForQuestion-1].toString().toLowerCase()=="MCQ".toLowerCase()?EnterResponse ? null :finalTotalOptionIDAccordingToQuestionsForTrack[emaxPositionForQuestion-1][_selectedIndexForOption].toString():null,
                                                    studentidForTrack,
                                                    subjectIdForTrack[emaxPositionForQuestion-1],
                                                    subjectNameForTrack[emaxPositionForQuestion-1],
                                                    testidForTrack.toString(),
                                                    timeRemainingForTrack,
                                                    systemtime,
                                                    totalTimeTakenPerQuestions[emaxPositionForQuestion-1].toString().contains(":")==true?_getDuration(int.parse(hoursStr),int.parse(minutesStr),int.parse(secondsStr))- _getDuration(int.parse(timeIntotalTimeTakenPerQuestion[0]),int.parse(timeIntotalTimeTakenPerQuestion[1]),int.parse(timeIntotalTimeTakenPerQuestion[2])):_getDuration(int.parse(hoursStr),int.parse(minutesStr),int.parse(secondsStr)),                                                topicIDForTrack[emaxPositionForQuestion-1],
                                                    topicnameForTrack[emaxPositionForQuestion-1],
                                                    latestTrackNumber,
                                                    0,
                                                    0));
                                                myObj.SyncTrackToApi();
                                                syncData();
                                                FLog.info(
                                                  dataLogType: "Debug",
                                                  text:  "myObj.SyncTrackToApi()======================== @@@@@@@@ EMAX"+myObj.SyncTrackToApi().toString(),
                                                );
                                                FLog.info(
                                                  dataLogType: "Debug",
                                                  text:  "TRACK GOT SYNC WHEN CLICK ON SUBMIT BUTTON BEFORE SUBMIT @@@@@@@@ EMAX",
                                                );
                                              }

                                              attemptList[emaxPositionForQuestion-1]=true;
                                              questionsStatus[emaxPositionForQuestion-1]= EnterResponse ? ((answer_status == "CORRECT")?true:false) : ((answer_status == "CORRECT")?true:false);//isCorrect;
                                              bookmarkorUnmarkList[emaxPositionForQuestion-1]=bookmarkorUnmarkList[emaxPositionForQuestion-1]?true:false;
                                              selectedOptionIdList[emaxPositionForQuestion-1]=EnterResponse ? answerEntered :finalTotalOptionIDAccordingToQuestionsForTrack[emaxPositionForQuestion-1][_selectedIndexForOption].toString();
                                              if(totalTimeTakenPerQuestions[emaxPositionForQuestion-1].toString().contains(":")){
                                                var timeInList=totalTimeTakenPerQuestions[emaxPositionForQuestion-1].toString().split(":");
                                                totalTimeTakenPerQuestions[emaxPositionForQuestion-1]= (durationToSec((_getDuration(int.parse(hoursStr),int.parse(minutesStr),int.parse(secondsStr)))));
                                              }else{
                                                totalTimeTakenPerQuestions[emaxPositionForQuestion-1]= (durationToSec((_getDuration(int.parse(hoursStr),int.parse(minutesStr),int.parse(secondsStr)))+totalTimeTakenPerQuestions[emaxPositionForQuestion-1]));
                                              }
                                              stopWatch();


                                            },
                                            child: Text(
                                              Preparequestions
                                                  .submit,
                                              style:
                                              TextStyle(
                                                color: NeutralColors
                                                    .pureWhite,
                                                fontSize:
                                                14,
                                                fontFamily:
                                                "IBMPlexSans",
                                                fontWeight:
                                                FontWeight
                                                    .w500,
                                              ),
                                              textAlign:
                                              TextAlign
                                                  .center,
                                            )),
                                      )
                                          : Container(
                                        width: 110 /
                                            360 *
                                            screenWidth,
                                        decoration:
                                        BoxDecoration(
                                            gradient:
                                            LinearGradient(
                                              begin:
                                              Alignment(
                                                  -0.056,
                                                  0.431),
                                              end: Alignment(
                                                  1.076,
                                                  0.579),
                                              stops: [
                                                0,
                                                1,
                                              ],
                                              colors: [
                                                GradientColors
                                                    .iceBlue,
                                                GradientColors
                                                    .iceBlue,
                                              ],
                                            )),
                                        child: FlatButton(
                                            onPressed:
                                                () {
                                              setState(
                                                      () {

                                                  });
                                            },
                                            child: Text(
                                              Preparequestions
                                                  .submit,
                                              style:
                                              TextStyle(
                                                color: AccentColors
                                                    .blueGrey,
                                                fontSize:
                                                14,
                                                fontFamily:
                                                "IBMPlexSans",
                                                fontWeight:
                                                FontWeight
                                                    .w500,
                                              ),
                                              textAlign:
                                              TextAlign
                                                  .center,
                                            )),
                                      ))
                                          : Container(
                                        width: 110 /
                                            360 *
                                            screenWidth,
                                        decoration:
                                        BoxDecoration(
                                            gradient:
                                            LinearGradient(
                                              begin: Alignment(
                                                  -0.056, 0.431),
                                              end: Alignment(
                                                  1.076, 0.579),
                                              stops: [
                                                0,
                                                1,
                                              ],
                                              colors: [
                                                GradientColors
                                                    .iceBlue,
                                                GradientColors
                                                    .iceBlue,
                                              ],
                                            )),
                                        child: FlatButton(
                                            onPressed: () {
                                              setState(() {

                                              });
                                            },
                                            child: Text(
                                              Preparequestions
                                                  .submit,
                                              style:
                                              TextStyle(
                                                color: AccentColors
                                                    .blueGrey,
                                                fontSize: 14,
                                                fontFamily:
                                                "IBMPlexSans",
                                                fontWeight:
                                                FontWeight
                                                    .w500,
                                              ),
                                              textAlign:
                                              TextAlign
                                                  .center,
                                            )),
                                      ),
                                      /**LAYOUT FOR NEXT BUTTON**/
                                      Container(
                                        decoration:  BoxDecoration(
                                            gradient: LinearGradient(
                                              begin: Alignment(-0.056, 0.431),
                                              end: Alignment(1.076, 0.579),
                                              stops: [0, 1,],
                                              colors:(attemptList.contains(false)&&emaxPositionForQuestion==totalQuestions.length)? [
                                                GradientColors.iceBlue,
                                                GradientColors.iceBlue,
                                              ] : [
                                                SemanticColors.light_purpely,
                                                NeutralColors.box_color,
                                              ],
                                            )
                                        ),
                                        width: 105 /360 * screenWidth,
                                        child: FlatButton(
                                          onPressed: () {
                                            OPTION_ACTIVE = true;
                                            SUB_FLAG = false;
                                            ANSWER_SUBMITTED = false;
                                            iscardchecked = false;
                                            // answer_status = null;
                                            isOptionsSelected = false;
                                            setState(() {
                                              emaxQuestionNoTimeList.add([
                                                emaxPositionForQuestion,
                                                dependencies.stopwatch.elapsedMilliseconds
                                              ]);

                                              pageViewController.nextPage(
                                                  duration: _kDuration,
                                                  curve: _kCurve);
                                              iscardchecked == false;
                                              emaxOverLayEntryCheck
                                                  ? emaxOverLayEntryCheck =
                                              false
                                                  : emaxOverLayEntryCheck =
                                              true;
                                              hide();
                                              FocusScope.of(context).requestFocus(new FocusNode());
                                              /// tack for next button before submitting ................
                                              //stopWatch();
                                              if(attemptList[emaxPositionForQuestion-1]==false){
                                                if(totalTimeTakenPerQuestions[emaxPositionForQuestion-1].toString().contains(":")){
                                                  timeIntotalTimeTakenPerQuestion=totalTimeTakenPerQuestions[emaxPositionForQuestion-1].toString().split(":");
                                                }
                                                if(attemptList[emaxPositionForQuestion-1]!=true){
                                                  latestTrackNumber=latestTrackNumber+1;
                                                }
                                                FLog.info(
                                                  dataLogType: "Debug",
                                                  text: latestTrackNumber.toString()+"&&&&&&&&&&&&"+(emaxPositionForQuestion-1).toString()+"&&&&&&&&&"+(subjectNameForTrack[emaxPositionForQuestion-1]).toString()+"&&&&&&&"+(topicnameForTrack[emaxPositionForQuestion-1]).toString()+"&&&&&&&"+userNameForLogInfo.toString()+"&&&&&&"+studentidForTrack.toString()+"======================================================TRACK NUMBER && QUESTION INDEX && SUBJECT NAME && AREA/TOPIC NAME && USER NAME && STUDENT USER ID  WHEN CLICK ON NEXT BUTTON BEFORE SUBMIT",
                                                );
                                                myObj.AddtoSist(Track.Comment(areaNameForTrack[emaxPositionForQuestion-1].toString(),
                                                    //    attemptForTrack,
                                                    false,
                                                    companyCodeForTrack,
                                                    difficultyIdForTrack[emaxPositionForQuestion-1],
                                                    difficultyNameForTrack[emaxPositionForQuestion-1],
                                                    null,
                                                    companyCodeForTrack+"_"+studentidForTrack+"_"+testidForTrack+"_"+sectionidForTrack.toString()+"_"+
                                                        itemIDForTrack[emaxPositionForQuestion-1].toString()+"_"+latestTrackNumber.toString(),
                                                    null,
                                                    isSectionCompletedForTrack,
                                                    itemIDForTrack[emaxPositionForQuestion-1],
                                                    itemtypeForTrack[emaxPositionForQuestion-1],
                                                    //  isMarkedForTrack[emaximPositionForQuestionForTrack],
                                                    bookmarkorUnmarkList[emaxPositionForQuestion-1]?true:false, //marked,   //marked
                                                    negaticepointsForTrack[emaxPositionForQuestion-1],
                                                    pointsForTrack[emaxPositionForQuestion-1],
                                                    emaxPositionForQuestion-1,
                                                    sectionidForTrack,
                                                    sectionindexForTrack,
                                                    sectionNameForTrack,
                                                    null,
                                                    studentidForTrack,
                                                    subjectIdForTrack[emaxPositionForQuestion-1],
                                                    subjectNameForTrack[emaxPositionForQuestion-1],
                                                    testidForTrack.toString(),
                                                    timeRemainingForTrack,
                                                    systemtime,
                                                    totalTimeTakenPerQuestions[emaxPositionForQuestion-1].toString().contains(":")==true?_getDuration(int.parse(hoursStr),int.parse(minutesStr),int.parse(secondsStr))- _getDuration(int.parse(timeIntotalTimeTakenPerQuestion[0]),int.parse(timeIntotalTimeTakenPerQuestion[1]),int.parse(timeIntotalTimeTakenPerQuestion[2])):_getDuration(int.parse(hoursStr),int.parse(minutesStr),int.parse(secondsStr)),
                                                    topicIDForTrack[emaxPositionForQuestion-1],
                                                    topicnameForTrack[emaxPositionForQuestion-1],
                                                    latestTrackNumber,
                                                    0,
                                                    0));
                                                myObj.SyncTrackToApi();
                                                FLog.info(
                                                  dataLogType: "Debug",
                                                  text:  "myObj.SyncTrackToApi()========================"+myObj.SyncTrackToApi().toString(),
                                                );
                                                FLog.info(
                                                  dataLogType: "Debug",
                                                  text:  "TRACK GOT SYNC WHEN CLICK ON NEXT BUTTON BEFORE SUBMIT",
                                                );
                                                bookmarkorUnmarkList[emaxPositionForQuestion-1]=bookmarkorUnmarkList[emaxPositionForQuestion-1]?true:false;
                                                if(totalTimeTakenPerQuestions[emaxPositionForQuestion-1].toString().contains(":")){
                                                  var timeInList=totalTimeTakenPerQuestions[emaxPositionForQuestion-1].toString().split(":");
                                                  // totalTimeTakenPerQuestions[emaxPositionForQuestion-1]= (durationToSec(((_getDuration(int.parse(hoursStr),int.parse(minutesStr),int.parse(secondsStr))))+(_getDuration(int.parse(timeInList[0]),int.parse(timeInList[1]),int.parse(timeInList[2])))));
                                                  totalTimeTakenPerQuestions[emaxPositionForQuestion-1]= (durationToSec(((_getDuration(int.parse(hoursStr),int.parse(minutesStr),int.parse(secondsStr))))));
                                                }else{
                                                  totalTimeTakenPerQuestions[emaxPositionForQuestion-1]= (durationToSec((_getDuration(int.parse(hoursStr),int.parse(minutesStr),int.parse(secondsStr)))+totalTimeTakenPerQuestions[emaxPositionForQuestion-1]));
                                                }
                                              }
                                              stopWatch();
                                            });

                                            if (emaxPositionForQuestion == totalQuestions.length && !attemptList.contains(false)) {
                                              //endTestApi();
                                              AppRoutes.push(context, EmaximizerSummaryScreen(nameOfTheTopic,testIdOfTheTopic,widget.listOfTopic,widget.listOfId,widget.listOfObjectType,widget.listOfStatus,widget.pos,selectedId,selectedStatus,selectedObjectType,selectedvalue,instruction,questionsStatus,totalQuestions,widget.positionForTest));
                                            } else if(emaxPositionForQuestion == totalQuestions.length &&attemptList.contains(false)){
                                              /// pop up for exit button
                                              emaxPopUpForExitButton=true;
                                              _emaxControllerForPopUp.open();
                                            }
                                          },
//                                                                color: GradientColors.iceBlue,
//                                                                textColor: Colors.white,
//                                                                disabledColor: GradientColors.iceBlue,
                                          child: Text(
                                            (emaxPositionForQuestion == totalQuestions.length)?Preparequestions.emaxSubmitForLastQuestion:Preparequestions.next,
                                            style:
                                            TextStyle(
                                              color:(attemptList.contains(false)&&emaxPositionForQuestion==totalQuestions.length)? AccentColors.blueGrey:Colors.white,
                                              fontSize: 14,
                                              fontFamily: "IBMPlexSans",
                                              fontWeight: FontWeight.w500,
                                            ),
                                            textAlign: TextAlign.center,
                                          ),
                                        ),
                                      )
                                    ],
                                  ),
                                )),
                          ],
                        ),
                      );
                  },
                  itemCount: totalQuestions.length,
                  onPageChanged: (page) {
                    setState(() {
                      directionBeforeAttemptForWebViewLoader=true;
                      questionBeforeAttemptForWebViewLoader=true;
                      optionsBeforeAttemptForWebViewLoader=true;
                      emaxPositionForQuestion = page + 1;
                      _answer.clear();
                      _answer.text="";
                      EnterResponse = false;
                      emaxBookMarkCheck = false;
                      emaxSoloutionCheck = false;
                      /// for click option ( without submit this flag will be true after submit it will be false)
                      OPTION_ACTIVE = true;
                      /// it will control all flags in every action
                      SUB_FLAG = false;
                      /// first time it will false after submit answer it will become true
                      ANSWER_SUBMITTED = false;
                      /// first time button will be disable once we click on option then it will enable
                      SUBMIT_ACTIVE = false;
                      answer_status = null;
                      contentHeight = 0;

                      if(attemptList[emaxPositionForQuestion-1]!=true){
                        resetWatch();
                        startWatch();
                      }

//                        updateTimeForPrev(totalTimeTakenPerQuestions[emaxPositionForQuestion - 1]);
//                        startWatchForPrevButton();

//                      updateTimeForPrev(totalTimeTakenPerQuestions[emaxPositionForQuestion - 1]);
//                       startWatchForPrevButton();
                      if (paragraphCheck[emaxPositionForQuestion-1] != null) {
                        checking = true;
                      } else {
                        checking = false;
                      }
                      getTimeStamp();
                      // markedForTrack = false;
                      getTimeStamp();
                      if(attemptList[emaxPositionForQuestion-1]!=true){
                        latestTrackNumber=latestTrackNumber+1;
                        FLog.info(
                          dataLogType: "Debug",
                          text: latestTrackNumber.toString()+"&&&&&&&&&&&&"+(emaxPositionForQuestion-1).toString()+"&&&&&&&&&"+(subjectNameForTrack[emaxPositionForQuestion-1]).toString()+"&&&&&&&"+(topicnameForTrack[emaxPositionForQuestion-1]).toString()+"&&&&&&&"+userNameForLogInfo.toString()+"&&&&&&"+studentidForTrack.toString()+"======================================================TRACK NUMBER && QUESTION INDEX && SUBJECT NAME && AREA/TOPIC NAME && USER NAME && STUDENT USER ID  WHILE ON PAGE CHANGE @@@@@@@@@@ EMAX",
                        );
                        myObj.AddtoSist(Track.Comment(areaNameForTrack[emaxPositionForQuestion-1].toString(),
                            //   attemptForTrack,
                            false,
                            companyCodeForTrack,
                            difficultyIdForTrack[emaxPositionForQuestion-1],
                            difficultyNameForTrack[emaxPositionForQuestion-1],
                            null,
                            companyCodeForTrack+"_"+studentidForTrack+"_"+testidForTrack+"_"+sectionidForTrack.toString()+"_"+
                                itemIDForTrack[emaxPositionForQuestion-1].toString()+"_"+latestTrackNumber.toString(),
                            null,
                            isSectionCompletedForTrack,
                            itemIDForTrack[emaxPositionForQuestion-1],
                            itemtypeForTrack[emaxPositionForQuestion-1],
                            //        isMarkedForTrack[emaximPositionForQuestionForTrack],
                            false,
                            negaticepointsForTrack[emaxPositionForQuestion-1],
                            pointsForTrack[emaxPositionForQuestion-1],
                            emaxPositionForQuestion-1,
                            sectionidForTrack,
                            sectionindexForTrack,
                            sectionNameForTrack,
                            null,
                            studentidForTrack,
                            subjectIdForTrack[emaxPositionForQuestion-1],
                            subjectNameForTrack[emaxPositionForQuestion-1],
                            testidForTrack.toString(),
                            timeRemainingForTrack,
                            systemtime,
                            timetakenForTrack,
                            topicIDForTrack[emaxPositionForQuestion-1],
                            topicnameForTrack[emaxPositionForQuestion-1],
                            latestTrackNumber,
                            0,
                            0));
                        myObj.SyncTrackToApi();
                        FLog.info(
                          dataLogType: "Debug",
                          text:  "myObj.SyncTrackToApi()======================== @@@@@@@@@ EMAX"+myObj.SyncTrackToApi().toString(),
                        );
                        FLog.info(
                          dataLogType: "Debug",
                          text:  "TRACK GOT SYNC WHILE ON PAGE CHANGE @@@@@@@@@ EMAX",
                        );
                      }



                      timeresumeonprev = false;

//                            attemptList[emaxPositionForQuestion-1]=false;
//                            questionsStatus[emaxPositionForQuestion-1]= null;
//                            bookmarkorUnmarkList[emaxPositionForQuestion-1]=false; //marked,   //marked;
//                            selectedOptionIdList[emaxPositionForQuestion-1]="-1";
//                            if(totalTimeTakenPerQuestions[emaxPositionForQuestion-1].toString().contains(":")){
//                              var timeInList=totalTimeTakenPerQuestions[emaxPositionForQuestion-1].toString().split(":");
//                              totalTimeTakenPerQuestions[emaxPositionForQuestion-1]= (durationToSec((_getDuration(int.parse(hoursStr),int.parse(minutesStr),int.parse(secondsStr)))+(_getDuration(int.parse(timeInList[0]),int.parse(timeInList[1]),int.parse(timeInList[2])))));
//                            }else{
//                              totalTimeTakenPerQuestions[emaxPositionForQuestion-1]= (durationToSec(timetakenForTrack+totalTimeTakenPerQuestions[emaxPositionForQuestion-1]));
//                            }

                    });
                  },
                ),
              ),
            ],
          ),
        ),
      );
    }
  }

  void syncData() {
    Map<String, dynamic> _finalData;
    Map<String, dynamic> postdata;
    List track = [];
    if (sync_list.length > 0) {
      for (int i = 0; i < sync_list.length; i++) {
        print(myObj.result[i].id);
        postdata = {
          "id": sync_list[i].id,
          "testId": sync_list[i].testId,
          "studentId": sync_list[i].studentId,
          "itemId": sync_list[i].itemId,
          "sectionId": sync_list[i].sectionId,
          "sectionIndex": sync_list[i].sectionIndex,
          "sectionName": sync_list[i].sectionName,
          "questionIndex": sync_list[i].questionIndex,
          "subjectId": sync_list[i].subjectId,
          "subjectName": sync_list[i].subjectName,
          "areaId": sync_list[i].areaid,
          "areaName": sync_list[i].areaName,
          "topicId": sync_list[i].topicId,
          "topicName": sync_list[i].topicName,
          "difficultyId": sync_list[i].difficultyId,
          "difficultyName": sync_list[i].difficultyName,
          "timeRemaining": sync_list[i].timeRemaining,
          "isSectionCompleted": sync_list[i].isSectionCompleted,
          "selectedOptionId": sync_list[i].selectedOptionId,
          "attempt": sync_list[i].attempt,
          "itemType": sync_list[i].itemType,
          "marked": sync_list[i].marked,
          "timeTaken": sync_list[i].timeTaken,
          "timeStamp": sync_list[i].timeStamp,
          "companyCode": sync_list[i].companyCode,
          "points": sync_list[i].points,
          "negativePoints": sync_list[i].negativePoints,
          "isCorrect": sync_list[i].isCorrect,
          "enteredText": sync_list[i].enteredText,
          "trackNumber": sync_list[i].trackNumber,
          "saved": sync_list[i].saved,
        };
        if (sync_list[i].saved == 0) {
          FLog.info(
            dataLogType: "Debug",
            text: i.toString() + "======================" +
                sync_list[i].id.toString() + "&&&&&&&&&&&&&&" +
                (sync_list[i].saved.toString() + "&&&&&&&&&&&&&&" +
                    "======================================================I VALUE AND ID AND SAVED VALUE IN POST DATA @@@@@@ EMAX"),
          );
          track.add(postdata);
          FLog.info(
            dataLogType: "Debug",
            text: i.toString() + "======================" +
                sync_list[i].id.toString() + "&&&&&&&&&&&&&&" +
                (sync_list[i].saved.toString() + "&&&&&&&&&&&&&&" +
                    "======================================================I VALUE AND ID AND SAVED VALUE AFTER ADDING  POST DATA VALUE TO TRACK @@@@@ EMAX"),
          );
        }
      }

      _finalData = {"track": track};
      String user = jsonEncode(_finalData);
      if (authToken != '') {
        ApiService().postAPI(
            URL.SYNC_API, _finalData, authToken)
            .then((result) {
          FLog.info(
            dataLogType: "Debug",
            text: _finalData.toString() + "&&&&&&&&&&&&&&" +
                "======================================================FINAL DATA TO BE SYNCED IN SYNC TRACK API @@@@@@@ EMAX",
          );
          FLog.info(
            dataLogType: "Debug",
            text: result.toString() + "&&&&&&&&&&&&&&" +
                "======================================================SUCCESS MESSAGE @@@@@@@ EMAX",
          );

          if (result.toString() == "Success") {
            FLog.info(
              dataLogType: "Debug",
              text: "ALL TRACKS GOT SYNC IN SYNC TRACK API @@@@@@@ EMAX",
            );
            for (int z = 0; z < myObj.result.length; z++) {
              FLog.info(
                dataLogType: "Debug",
                text: z.toString() + "&&&&&&" +
                    myObj.result[z].saved.toString() +
                    "===============AFTER SYNC I &&&&&& SAVED VALUE IN myObj.result @@@@@@@ EMAX",
              );
              myObj.result[z].saved = 1;
              FLog.info(
                dataLogType: "Debug",
                text: z.toString() + "&&&&&&" +
                    myObj.result[z].saved.toString() +
                    "===============AFTER SYNC I &&&&&& SAVED VALUE IN myObj.result AFTER MAKING SAVE EQUALS TO ONE @@@@@@@ EMAX",
              );
            }
            myObj.result = [];
            sync_list = [];
            myObj.list_name_sync = [];
            postdata = {};
            track = [];
            myObj.SyncTrackToApi();
          }
        });


        // });
      }
    }
  }


}
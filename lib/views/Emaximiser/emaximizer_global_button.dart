import 'package:flutter/material.dart';



var screenWidthTotal;
var screenHeightTotal;

var _safeAreaHorizontal;

var _safeAreaVertical;

var screenHeight;

var screenWidth;

class GlobalButtons extends StatefulWidget {


  bool isButtonDisabled = true;
  GlobalButtons({Key key, this.isButtonDisabled}): super(key: key);


  @override
  _GlobalButtonsState createState() => _GlobalButtonsState();
}

class _GlobalButtonsState extends State<GlobalButtons> {
  @override
  Widget build(BuildContext context) {
    var _mediaQueryData = MediaQuery.of(context);
    screenWidthTotal = _mediaQueryData.size.width;
    screenHeightTotal = _mediaQueryData.size.height;
    _safeAreaHorizontal =
        _mediaQueryData.padding.left + _mediaQueryData.padding.right;
    _safeAreaVertical =
        _mediaQueryData.padding.top + _mediaQueryData.padding.bottom;

    screenHeight = screenHeightTotal - _safeAreaVertical;
    screenWidth = screenWidthTotal - _safeAreaHorizontal;

    return  Container(
      height:40/720*screenHeight ,
      width:320/360*screenWidth ,
      margin: EdgeInsets.only(left:20/360*screenWidth ,right:20/360*screenWidth ),
      // color: Colors.black,
      child: Row(
        children: <Widget>[
          Container(
            decoration: BoxDecoration(
                color: Color(0xFFf2f4f4)
            ),
            width:105/360*screenWidth ,
            child:
            FlatButton( color: Color(0xFFf2f4f4),
              textColor: Colors.white,
              disabledColor: Color(0xFFf2f4f4),
              disabledTextColor: Colors.black,
              onPressed: (){

              },
              child: Text(
                "PREV",
                style: TextStyle(
                  color: Color(0xFF7e919a),
                  fontSize: 14,
                  fontFamily: "IBMPlexSans",
                  fontWeight: FontWeight.w500,
                ),
                textAlign: TextAlign.center,
              ),),

          ),
          Container(
            width:110/360*screenWidth  ,
            decoration: BoxDecoration(
              gradient: widget.isButtonDisabled?LinearGradient(
                begin: Alignment(-0.056, 0.431),
                end: Alignment(1.076, 0.579),
                stops: [
                  0,
                  1,
                ],
                colors: [
                  Color.fromARGB(255, 51, 128, 204),
                  Color.fromARGB(255, 137, 110, 216),
                ],
              ):LinearGradient(
                begin: Alignment(-0.056, 0.431),
                end: Alignment(1.076, 0.579),
                stops: [
                  0,
                  1,
                ],
                colors: [
                  Color(0xFFf2f4f4),
                  Color(0xFFf2f4f4),
                ],
              ),
            ),
            child:FlatButton(
                onPressed: (){

                },
                child: Text(
                  "SUBMIT",
                  style:  widget.isButtonDisabled?TextStyle(
                    color: Color.fromARGB(255, 255, 255, 255),
                    fontSize: 14,
                    fontFamily: "IBMPlexSans",
                    fontWeight: FontWeight.w500,
                  ):TextStyle(
                    color: Color(0xFF7e919a),
                    fontSize: 14,
                    fontFamily: "IBMPlexSans",
                    fontWeight: FontWeight.w500,
                  ),
                  textAlign: TextAlign.center,
                )),),
          Container(
            decoration: BoxDecoration(
                color: Color(0xFFf2f4f4)
            ),
            width: 105/360*screenWidth ,
            child:FlatButton(
              onPressed: (){

              },
              color: Color(0xFFf2f4f4),
              textColor: Colors.white,
              disabledColor: Color(0xFFf2f4f4),   child: Text(
              "NEXT",
              style: TextStyle(
                color: Color(0xFF7e919a),
                fontSize: 14,
                fontFamily: "IBMPlexSans",
                fontWeight: FontWeight.w500,
              ),
              textAlign: TextAlign.center,
            ),),),
        ],
      ),
    );
  }
}
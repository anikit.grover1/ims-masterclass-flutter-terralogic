import 'package:flutter/material.dart';
import 'dart:async';

class ElapsedTime {
  final int hundreds;
  final int seconds;
  final int minutes;
  final hours;
  ElapsedTime({
    this.hundreds,
    this.seconds,
    this.minutes,
    this.hours,
  });
}

class Dependency {

  final List<ValueChanged<ElapsedTime>> timerListeners = <ValueChanged<ElapsedTime>>[];
  final TextStyle textStyle = const TextStyle(
    color: Color.fromARGB(255, 0, 3, 44),
    fontSize: 12,
    fontFamily: "IBMPlexSans",
    fontWeight: FontWeight.w500,
  );
  Stopwatch stopwatch = new Stopwatch();
  final int timerMillisecondsRefreshRate = 30;
}



class TimerText extends StatefulWidget {
  TimerText({this.dependency});
  final Dependency dependency;

  TimerTextState createState() => new TimerTextState(dependency: dependency);
}

class TimerTextState extends State<TimerText> {
  TimerTextState({this.dependency});
  final Dependency dependency;
  Timer timer;
  int milliseconds;

  @override
  void initState() {
    timer = new Timer.periodic(new Duration(milliseconds: dependency.timerMillisecondsRefreshRate), callback);
    super.initState();
  }

  @override
  void dispose() {
    timer?.cancel();
    timer = null;
    super.dispose();
  }

  void callback(Timer timer) {
    if (milliseconds != dependency.stopwatch.elapsedMilliseconds) {
      milliseconds = dependency.stopwatch.elapsedMilliseconds;
      final int hundreds = (milliseconds / 10).truncate();
      final int seconds = (hundreds / 100).truncate();
      final int minutes = (seconds / 60).truncate();
      final int hours = (minutes / 60).truncate();


      final ElapsedTime elapsedTime = new ElapsedTime(
          hundreds: hundreds,
          seconds: seconds,
          minutes: minutes,
          hours:hours
      );
      for (final listener in dependency.timerListeners) {
        listener(elapsedTime);
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: new  Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          new RepaintBoundary(
            child:new MinutesAndSeconds(dependency: dependency),

          ),
          new RepaintBoundary(
            child: new Hundreds(dependencies: dependency),

          ),
        ],
      ),
    );
  }
}

class MinutesAndSeconds extends StatefulWidget {
  MinutesAndSeconds({this.dependency});
  final Dependency dependency;

  MinutesAndSecondsState createState() => new MinutesAndSecondsState(dependency: dependency);
}

class MinutesAndSecondsState extends State<MinutesAndSeconds> {
  MinutesAndSecondsState({this.dependency});
  final Dependency dependency;

  int minutes = 0;
  int seconds = 0;

  @override
  void initState() {
    dependency.timerListeners.add(onTick);
    super.initState();
  }

  void onTick(ElapsedTime elapsed) {
    if (elapsed.minutes != minutes || elapsed.seconds != seconds) {
      setState(() {
        minutes = elapsed.minutes;
        seconds = elapsed.seconds;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    String minutesStr = (minutes % 60).toString().padLeft(2, '0');
    String secondsStr = (seconds % 60).toString().padLeft(2, '0');
    return new Text('$minutesStr:$secondsStr.', style: dependency.textStyle);
  }
}

class Hundreds extends StatefulWidget {
  Hundreds({this.dependencies});
  final Dependency dependencies;

  HundredsState createState() => new HundredsState(dependencies: dependencies);
}

class HundredsState extends State<Hundreds> {
  HundredsState({this.dependencies});
  final Dependency dependencies;

  int hundreds = 0;

  @override
  void initState() {
    dependencies.timerListeners.add(onTick);
    super.initState();
  }

  void onTick(ElapsedTime elapsed) {
    if (elapsed.hundreds != hundreds) {
      setState(() {
        hundreds = elapsed.hundreds;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    String hundredsStr = (hundreds % 100).toString().padLeft(2, '0');
    return new Text(hundredsStr, style: dependencies.textStyle);
  }
}
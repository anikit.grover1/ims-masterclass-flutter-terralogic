import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:flutter_svg/svg.dart';
import 'package:imsindia/components/primary_button_gradient.dart';
import 'package:imsindia/routers/routes.dart';
import 'package:imsindia/utils/colors.dart';
import 'package:imsindia/utils/emaximiser_svg_icons.dart';
import 'package:imsindia/utils/Url.dart';
import 'package:imsindia/api/bloc/services.dart';
import 'package:imsindia/utils/global.dart' as global;
import 'package:imsindia/views/Emaximiser/emaximiser_question_review_screen.dart';
import 'package:imsindia/views/Emaximiser/emaximizer_home_screen.dart';
import 'package:imsindia/views/Emaximiser/emaximizer_question_screen.dart';
import 'package:imsindia/views/Emaximiser/emaximizer_videoplayer.dart';
import 'package:imsindia/views/Emaximiser/emaximizer_videoplayer_ios.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../../routers/routes.dart';
import 'package:connectivity/connectivity.dart';
import 'dart:io' as Platform;

String userid;
var screenWidthTotal;
var screenHeightTotal;
var _safeAreaHorizontal;
var _safeAreaVertical;
var screenHeight;
var screenWidth;
var totalQuestions = [];
var questionsAttempt = [];
var authToken;
var indexForIntoandCon = 0;
var courseName;
class EmaxIntroduction extends StatefulWidget {
  var pos;
  var desc;
  // bool qscreen;
  bool screen;
  var selectedvalue;
  var selectedId;
  var selectedStatus;
  var selectedObjectTypeQ;
  var instruction;
  var positionForTest;
  EmaxIntroduction({this.screen,this.instruction,this.selectedStatus,this.selectedId,this.selectedvalue,this.pos,this.desc,this.selectedObjectTypeQ,this.positionForTest});

  @override
  EmaxIntroductionState createState() => EmaxIntroductionState();
}

class EmaxIntroductionState extends State<EmaxIntroduction> {

  Connectivity connectivity = Connectivity();
  final GlobalKey<ScaffoldState> _scaffoldstate = new GlobalKey<ScaffoldState>();

  /// set access token to prod test launch/reveiw screen...............
  void setAccessTokenForProdTestLaunch(value) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString("accessTokenEmax", value);
  }

  final LayerLink _layerLink = LayerLink();
  var testIdselected;
  var getLearFirstHiraLevelTitles = [];
  var objectTypeList = [];
  var first = '';
  var testIdfirst = '';
  var testStatus = [];
  var testId = [];
  var testStatusForCatch;
  var getIdForSubjectLevelId;
  var instruction;
  var desc;
  var selectedIndex;
  var selectedvalue;
  var systemtime;
  var testIdInitial;
  var indexForQAT=0;
  var jumpToValueFromSubScreenValue;

  var postdataforcomplete;
  var tokenforendtestlesson;
  PageController controller = PageController();


  void getSubjectLevelTitlesAndCardLevelData() async {
   // if (global.headersWithOtherAuthorizationKey['Authorization'] != '') {
      print("APIII");
      print(URL.EMAX_URL_DROP+getIdForSubjectLevelId);
      ApiService()
          .getAPI(URL.EMAX_URL_DROP+getIdForSubjectLevelId,authToken)
          .then((returnValue) {
        setState(() {
          if (returnValue[0] == 'Fetched Successfully') {
            print("Data Fetched.....................");
            print(returnValue[1]['data'].length);
            if(widget.screen == true){
            var resumeFlag = false;
            
            for (var index = 0; index < returnValue[1]['data'].length; index++) {
              
              if(returnValue[1]['data'][index]['testStatus'].toString() == "In-Progress"){
                print("Entered in progress"+selectedvalue.toString());
                resumeFlag = true;
                selectedId = returnValue[1]['data'][index]['testId'];
                selectedvalue = returnValue[1]['data'][index]['component']['title'];
                testStatusForCatch = returnValue[1]['data'][index]['testStatus'];
                selectedObjectType = returnValue[1]['data'][index]['component']['objectType'];
                indexForIntoandCon=index;
                if(selectedObjectType == "Lesson"){
                                selectedObjectType = false;
                              }
                              else{
                                selectedObjectType = true;
                              }
                if(selectedObjectType == true) {  
                                  widget.selectedObjectTypeQ = true;           // true = "Test"

                                  global.getToken.then((t) {
                                    apiForTokenAndData(t);
                                  });

                                  //questionsScreen.buttonstatus=false;
                                }else{
                                  widget.selectedObjectTypeQ = false;
                                }
              }

              //testIdselected = testId[index];
              
            }
            if(resumeFlag == false){
              print("Entered in start"+selectedvalue.toString());
              selectedId = returnValue[1]['data'][0]['testId'];
                selectedvalue = returnValue[1]['data'][0]['component']['title'];
                testStatusForCatch = returnValue[1]['data'][0]['testStatus'];
                selectedObjectType = returnValue[1]['data'][0]['component']['objectType'];
                indexForIntoandCon=0;
                if(selectedObjectType == "Lesson"){
                                selectedObjectType = false;
                              }
                              else{
                                selectedObjectType = true;
                              }
                if(selectedObjectType == true) {  
                                  widget.selectedObjectTypeQ = true;           // true = "Test"

                                  global.getToken.then((t) {
                                    apiForTokenAndData(t);
                                  });

                                  //questionsScreen.buttonstatus=false;
                                }else{
                                  widget.selectedObjectTypeQ = false;
                                }
            }
            }

            //selectedId = testId[0];
            //testIdfirst = testId[1];
            //   first = getLearFirstHiraLevelTitles[0];
            getNextTopicUnlock();

          } else {
            print("return value");
            print(returnValue[0]);
            //      authorizationInvalidMsg = returnValue[0];
          }
        });
      });
  //  }
  }
  Future<void> getNextTopicUnlock() async {
    print("shwetaselected"+selectedId.toString());
    print(testIdInitial);
   // if (global.headersWithAuthorizationKey['Authorization'] != '') {
      Map postdata = {"testId":await selectedId.toString()};
      var totalData;
      ApiService().postAPI(URL.TEST_LAUNCH_FOR_YES_PREPARE,postdata,authToken)
          .then((result) {
        totalData = result[1]['data'];
        tokenforendtestlesson = totalData['token'];
        print(totalData['token']);
        // getSubjectLevelTitlesAndCardLevelData();
        ApiService()
            .getAPI(URL.EMAX_URL_DROP+getIdForSubjectLevelId, authToken)
            .then((returnValue) {
          setState(() {
            if (returnValue[0] == 'Fetched Successfully') {
              print("Data Fetched.....................");
              for (var index = 0;index < returnValue[1]['data'].length;index++) {

                getLearFirstHiraLevelTitles.add(returnValue[1]['data'][index]['component']['title']);
                objectTypeList.add(returnValue[1]['data'][index]['component']['objectType']);


                testId.add(returnValue[1]['data'][index]['testId']);


                testStatus.add(returnValue[1]['data'][index]['testStatus']);
                totalQuestions.add(returnValue[1]['data'][index]['component']['totalQuestions']);
                totalQuestions.removeWhere((value) => value == null);

                questionsAttempt.add(returnValue[1]['data'][index]['questionAttempt']);


                //  testStatusValue =  true;
              }
              first = getLearFirstHiraLevelTitles[0];
              print("titles");
              print(getLearFirstHiraLevelTitles);
            }
          });
        });
        //    widget.statusFromCatch = "In-Progress";
      });
      // });
   // }
    //  dispose();
  }

  setSharePref(selValue) async {
    print("selectedValue ${selValue.toString()}");
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString(selValue.toString(),"topicNameEmax");

  }

  void getTimeStamp() {
    var moonLanding = DateTime.now();
    print("Current UTC Timing");
    print(moonLanding.toUtc());
    String systemDate = moonLanding.toUtc().toString().split(" ")[0];
    String systemTime = moonLanding.toUtc().toString().split(" ")[1].toString().split(".")[0];
    String lastZstring = moonLanding.toUtc().toString().split(" ")[1].toString().split(".")[1];
    String thridDigit = lastZstring.substring(0,3)+"Z";
    print("=============="+systemDate+"================"+systemTime+"==========="+lastZstring+"=========="+thridDigit);
    systemtime = systemDate+"T"+systemTime+"."+thridDigit;
    print(systemtime);
  }
  @override
  void initState() {
    totalQuestions = [];
    questionsAttempt = [];

    print("INIT STATE IN EMAX");
    print(widget.selectedvalue);
    print(widget.selectedId);
    print(widget.selectedStatus);
    print(widget.selectedObjectTypeQ);
    print(widget.instruction);
    print(selectedvalue);
    print(widget.pos);
    print(widget.desc);
    print("Positions---------");
    print(widget.positionForTest);
    print(indexForIntoandCon);

    print("END INIT STATE IN EMAX");


    // TODO: implement initState


    if(widget.instruction!=null){
      instruction=widget.instruction;
      print("Shweta Instruction"+instruction);
    }
    if (widget.selectedvalue!=null){
      selectedvalue = widget.selectedvalue;
    }
    if (widget.selectedId!=null){
      selectedId = widget.selectedId;
    }
    if (widget.selectedStatus!=null){
      testStatusForCatch = widget.selectedStatus;
    }
    if (widget.selectedObjectTypeQ!=null){
      selectedObjectType = widget.selectedObjectTypeQ;
    }
    if (widget.desc!=null){
      desc = widget.desc;
    }
    if(widget.positionForTest != null){
      indexForIntoandCon = widget.positionForTest;
    }else{
      indexForIntoandCon = 0;
    }


    //*****internet connectivity//
    super.initState();
    //checkInternet();
    print('init state called');
    connectivity.onConnectivityChanged.listen((result) {
      if (result.toString() == 'ConnectivityResult.none') {
        print('connection lost');
        noInternetMessage('Internet Connection Lost');
      } else {
        print(_scaffoldstate.currentState) ;
        print("_scaffoldstate.currentState");
        _scaffoldstate.currentState.removeCurrentSnackBar();
        print('connection exist in notifier');
        getLearFirstHiraLevelTitles = [];
        objectTypeList = [];
        questionsAttempt = [];
        totalQuestions = [];
        testStatus = [];
        testId = [];
        getIdAndTitleForSubjectLevel() .then((returnValue) {
          global.getToken.then((t){
            authToken=t;
            getSubjectLevelTitlesAndCardLevelData();
          });
        });
      }
    });

    getCourseId();
//    getIdAndTitleForSubjectLevel().then((returnValue) {
//      getSubjectLevelTitlesAndCardLevelData();
//    });
  }


  Future<Null> checkInternet() async {
    var connectivityResult = await (Connectivity().checkConnectivity());
    if (connectivityResult.toString() != 'ConnectivityResult.none') {
      setState(() {
        getLearFirstHiraLevelTitles = [];
        objectTypeList = [];
        questionsAttempt = [];
        totalQuestions = [];
        testStatus = [];
        testId = [];
        getIdAndTitleForSubjectLevel() .then((returnValue) {
          global.getToken.then((t){
            authToken=t;
            getSubjectLevelTitlesAndCardLevelData();
          });
        });

      });
    } else {
      noInternetMessage('Internet Connection Lost');
    }

    return null;
  }
  void noInternetMessage(String value) {
    _scaffoldstate.currentState.showSnackBar(new SnackBar(
      content: new Text(value),
      action: SnackBarAction(
        textColor: Colors.blueAccent,
        label: "Dismiss",
        onPressed: () {
          // Some code to undo the change.
        },
      ),
      backgroundColor: Colors.blueGrey,
      duration: Duration(days: 365),
    ));
  }
  void showErrorMessage(String value) {
    _scaffoldstate.currentState.showSnackBar(new SnackBar(
      content: new Text(value),
      duration: new Duration(seconds: 3),
    ));
  }

  void getCourseId() async {

    SharedPreferences prefs = await SharedPreferences.getInstance();
    courseName = prefs.getString('courseName');
    global.getToken.then((t){
      getIdAndTitleForSubjectLevel().then((returnValue) {
        authToken=t;
        getSubjectLevelTitlesAndCardLevelData();
      });
    });

    //getCourseIdData();
  }

  Future getendtest(Map t) async {
    //if (global.headersWithOtherAuthorizationKey['Authorization'] != '') {
      Map postdataendtest = {
        "token": "ims-$selectedId-$userid"
      };

      print("TOKENCH" + postdataendtest.toString());
      ApiService().postAPI(URL.EMAX_END_TEST, postdataendtest,
          t)
          .then((result) {
        setState(() {
          print(result);
          if (result[0] == 'Successfully submitted'  || result[0]=="TESTSUBMITTED") {
            var totalData = result[1]['data'];
            print(result[0]);
            print("result end test issssss");
            print(totalData);
            //  instruction=totalData['testJson']['Instruction'];
            //print("INSCH"+instruction);
            ((selectedvalue)==(getLearFirstHiraLevelTitles[getLearFirstHiraLevelTitles.length-1]))?
            Navigator.pop(context, true)
                :print("Not exiting");
          } else {}
        });
      });
   // }
  }

  getIdAndTitleForSubjectLevel() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    getIdForSubjectLevelId = prefs.getString("idForSubjectLevelIdEmax");
    userid = prefs.getString("userId");
    print("shwetauserid");
    print(getIdForSubjectLevelId);


  }
//  void onGroup3Pressed(inst,testStatusToCatch,selvalue,selectedVAlue,selId,testIdFirstt, BuildContext context) {
//    print("selectedValuepass"+selvalue);
//    print("INST"+inst);
//    print(testStatusToCatch);
//    print(selId);
//    (selectedVAlue==getLearFirstHiraLevelTitles[getLearFirstHiraLevelTitles.length-1] && selectedObjectType=="Lesson")?
//
//    Navigator.pop(
//        context)        :
//    Navigator.push(
//        context, MaterialPageRoute(builder: (context) =>
//        Emaximizercatchup(inst,testStatusToCatch,selvalue.toString(),selId.toString(),testIdFirstt.toString(),widget.pos)));
//
//  }
  bool OverLayEntryCheck = false;
  bool ArrowUpDown = false;
  bool overLayEntryCheck = false;
  static OverlayEntry entry = null;
  bool get isshow => entry != null;
  void show(context) => addOverlayEntry(context);
  void hide() => removeOverlay();
  // var selectedValueOfNextTopic;
  var selectedvaluenext;

  var selectedId;
  var selectedObjectType;
  var courseId;

  ///
  Future completeLessonTypeEndTest(Map t) async{
    print("inside completeLessonTypeEndTest");
    print(tokenforendtestlesson);
    print(testId[0]);
    postdataforcomplete = (((selectedvalue)==(getLearFirstHiraLevelTitles[getLearFirstHiraLevelTitles.length-1]))
        && (selectedObjectType==false))?selectedId:testIdInitial;
    print(postdataforcomplete);
    Map postdataendtest = {
      "token": "ims-$selectedId-$userid"
    };

    print(postdataendtest);

    return ApiService().postAPI(URL.EMAX_END_TEST, postdataendtest,
        t)
        .then((result) {
          print(result.toString());
          print(result[0].toString());
      //setState(() {
        if (result[0] == 'Successfully submitted' || result[0]=="TESTSUBMITTED") {
          setState(() {
            //if(result[0]!="TESTSUBMITTED"){
              getLearFirstHiraLevelTitles = [];
            objectTypeList = [];
            questionsAttempt = [];
            totalQuestions = [];
            testStatus = [];
            testId = [];
            //}
            
          });
          var totalData = result[1]['data'];
          
          print("Entered");
          //  instruction=totalData['testJson']['Instruction'];
          //  print("INSCH"+instruction);
           return ApiService()
              .getAPI(URL.EMAX_URL_DROP+getIdForSubjectLevelId, t)
              .then((returnValue) {
            //setState(() {
              if (returnValue[0] == 'Fetched Successfully') {
                print("Data Fetched..................... for dropdown");
                for (var index = 0;
                index < returnValue[1]['data'].length;
                index++) {
                  getLearFirstHiraLevelTitles.add(returnValue[1]['data'][index]['component']['title']);
                  objectTypeList.add(returnValue[1]['data'][index]['component']['objectType']);
                  testId.add(returnValue[1]['data'][index]['testId']);
                  testStatus.add(returnValue[1]['data'][index]['testStatus']);
                  totalQuestions.add(returnValue[1]['data'][index]['component']['totalQuestions']);
                  totalQuestions.removeWhere((value) => value == null);
                  questionsAttempt.add(returnValue[1]['data'][index]['questionAttempt']);
                  //  testStatusValue =  true;
                }

               

              }
         //   });
         print("all first level titlesssssssssssssssssssssss");
          print(getLearFirstHiraLevelTitles);
          print(testStatus);
          print(selectedId);
          print(testId[testId.length-1]);
          if(((selectedId)==(testId[testId.length-1]))){
            Navigator.pop(context,true);
          }
          //  ((selectedId)==(testId[testId.length-1]))?
          //   Navigator.pop(context, true)
          //       :print("Not exiting");
          });
          
        } else {}
     // });
    });
  }

  Future apiForTokenAndData(Map t) async{
    print(selectedId);
    print(testStatusForCatch);
    
      Map postdata = {
        "testId": selectedId
      };
      ApiService()
          .postAPI(
          URL.TEST_LAUNCH_FOR_YES_PREPARE,
          postdata,
          t)
          .then((result) {
        setState(() {
          overLayEntryCheck
              ? overLayEntryCheck = false
              : overLayEntryCheck = true;
          hide();
          if (result[0] == 'successfully resume' || result[0] == 'successfully created') {
            var totalData = result[1]['data'];
            global.tokenForTestLaunchApi = totalData['token'];
            setAccessTokenForProdTestLaunch(totalData['token']);
            print("selected id for next topic");
            print("..");
            Map postdata_token = {
              "token": global.tokenForTestLaunchApi.toString()
            };
            print("TOKENCH for next" + global.tokenForTestLaunchApi.toString());
            print(testStatusForCatch);



            ApiService().postAPI((testStatusForCatch=='Completed')?URL.EMAX_PROD_REVISE:URL.GET_QUESTIONS_DATA_FOR_PREPARE, postdata_token,
                t)
                .then((result) {
              print("result");
              print(result);
              setState(() {
                if (result[0].toString().toLowerCase() == 'success'.toLowerCase()) {
                  var totalData = result[1]['data'];
                  print("resulit issssss");
                  print(totalData);
                  print("instructions loading...");
                  instruction = totalData['testJson']['Instruction'];
                  desc = totalData['testJson']['Description'];
                  courseId = totalData['studentTestData']['courseId'];

                  //   instructionStatus = true;
                  //   widget.instructionFromIntroduction = null;
                  //print("INSCH" + instruction);
                  print("description  loading..." + desc.toString());
//
//                  ApiService().postAPI(URL.EMAX_END_TEST, postdataendtest,
//                      t)
//                      .then((result) {
//                    setState(() {
//                      if (result[0] == 'Successfully submitted') {
//                        var totalData = result[1]['data'];
//
//                        //  instruction=totalData['testJson']['Instruction'];
//                        //  print("INSCH"+instruction);
//
//                      } else {}
//                      global.getToken.then((t) {
//                        getNextTopicUnlock(t);
//                      });
//                    });
//                  });

                } else {}
              });
            });
          } else {}
        });
      });
    

  }
  ///* post API call for  a emaximizer *///
  void userAction(Map t) async{
    getTimeStamp();
    Map postdata = {
      "courseId": courseId??1,
      "parentId": getIdForSubjectLevelId,
      "referenceId": selectedId,
      "timeStamp": systemtime,
    };
    print("Postdataof action");
    print(postdata);
    ApiService().postAPI(URL.EMAX_USER_ACTION ,postdata,t,)
        .then((result) {
      setState(()
      {
        print("returnValue"+ result[0].toString());
      });
    });
  }

  addOverlayEntry(context) {
    ArrowUpDown = true;
    if (entry != null) return;
    entry = new OverlayEntry(builder: (BuildContext context) {
      return LayoutBuilder(builder: (context, BoxConstraints constraints) {
        return Stack(
          children: <Widget>[
            Positioned(
              top: 175 / 720 * screenHeight,
              right: 20 / 360 * screenWidth,
              left: 20 / 360 * screenWidth,
              child:  Container(
                height: 280 / 720 * screenHeight,
                width: 320 / 360 * screenWidth,
                decoration: BoxDecoration(
                  color: Colors.white,
                  boxShadow: [
                    BoxShadow(
                      blurRadius: 10,
                      color: Color(0xffeaeaea),
                      //offset: Offset.lerp(Offset(10.0,-10.0), Offset(10.0,10.0), 1),
                      // offset: Offset(0,10.0),
                      //color: Colors.orange,
                    ),
                  ],
//                  borderRadius: BorderRadius.circular(5.0),
//                  border: Border.all(
//                    color: Colors.transparent,
//                  ),
                ),
                margin: EdgeInsets.all(0),
                alignment: Alignment.topCenter,
                child:  Material(
                  color: Colors.transparent,
                  elevation: 0.0,
                  //   borderRadius: BorderRadius.circular(5.0),
                  child: Container(
                    // padding: EdgeInsets.all(0),
//                        decoration: BoxDecoration(
//                          color: Colors.black,
//                          borderRadius: BorderRadius.circular(5.0),
////                          border: Border.all(
////                            color: Colors.black,
////                          ),
//                        ),
                    child: ListView.builder(
                      padding: EdgeInsets.all(0),
                      itemCount: getLearFirstHiraLevelTitles.length,
                      itemBuilder: (context, index) {
                        //  final item = getLearFirstHiraLevelTitles[index];
                        return InkWell(
                          onTap: () {
                            if(testStatus[index] != 'Start'){
                              selectedvalue = getLearFirstHiraLevelTitles[index];
                              //    selectedValueOfNextTopic =  getLearFirstHiraLevelTitles[index+1];
                              instruction = null;
                              desc = null;
                              testStatusForCatch = testStatus[index];
                              selectedId = testId[index];
                              selectedObjectType = objectTypeList[index];
                              indexForIntoandCon=index;
                              if(selectedObjectType == "Lesson"){
                                selectedObjectType = false;
                              }
                              else{
                                selectedObjectType = true;
                              }
                              print("Tap tap");
                              print(selectedObjectType);
                              indexForQAT = index;
                              hide();
                              OverLayEntryCheck
                                  ? OverLayEntryCheck = false
                                  : OverLayEntryCheck = true;
                              setState(() {

                                print("Shweta"+(index+1).toString()+getLearFirstHiraLevelTitles.length.toString());
                                if(selectedObjectType == true) {  
                                  widget.selectedObjectTypeQ = true;           // true = "Test"

                                  global.getToken.then((t) {
                                    apiForTokenAndData(t);
                                  });

                                  //questionsScreen.buttonstatus=false;
                                }else{
                                  widget.selectedObjectTypeQ = false;
                                }

                              });

                              global.getToken.then((t) {
                                userAction(t);
                              });
                            }
                          },
                          child: Container(

                            color: (testStatus[index] == 'In-Progress')
                                ? NeutralColors.ice_blue
                                : Colors.white,
                            height: 40 / 720 * screenHeight,
                            margin: EdgeInsets.only(top: 0),
                            child: Row(
                              children: [
                                Container(
                                  width: 250/360 * screenWidth,
                                  margin: EdgeInsets.only(left: 15),
                                  child: Text(
                                    getLearFirstHiraLevelTitles[index],
                                    overflow: TextOverflow.ellipsis,
                                    style: TextStyle(
                                      color: (testStatus[index] == 'In-Progress')
                                          ? NeutralColors.purpleish_blue
                                          : (testStatus[index] == 'Start')
                                          ? NeutralColors.blue_grey
                                          : Color.fromARGB(255, 0, 3, 44),
                                      fontSize: 16 / 720 * screenHeight,
                                      fontFamily: "IBM Plex Sans",
                                      fontWeight: FontWeight.w500,
                                      
                                      ),
                                    textAlign: TextAlign.left,
                                    
                                  ),
                                ),
                                Spacer(),
                                Align(
                                  alignment: Alignment.center,
                                  child: Container(
                                    width: 19 / 360 * screenWidth,
                                    height: 19 / 720 * screenHeight,
                                    margin: EdgeInsets.only(right: 15),
                                    child: (testStatus[index] == 'In-Progress')
                                        ? new SvgPicture.asset(
                                      'assets/images/selected.svg',
                                      fit: BoxFit.scaleDown,
                                      //color: Colors.red,
                                    )
                                        : (testStatus[index] == 'Start')
                                        ? new SvgPicture.asset(
                                      'assets/images/emax_lock.svg',
                                      fit: BoxFit.scaleDown,
                                      //color: Colors.red,
                                    )
                                        : (testStatus[index] == 'Completed')
                                        ? new SvgPicture.asset(
                                      'assets/images/tick.svg',
                                      fit: BoxFit.scaleDown,
                                      //color: Colors.red,
                                    )
                                        : null,
                                  ),
                                ),
                              ],
                            ),
                          ),
                        );
                      },
                    ),
                  ),
                ),
              ),
            )
          ],
        );
      });
    });

    addoverlay(entry, context);
  }

  static addoverlay(OverlayEntry entry, context) async {
    Overlay.of(context).insert(entry);
  }

  removeOverlay() {
    ArrowUpDown = false;
    entry?.remove();
    entry = null;
  }

  @override
  Future<bool> _onWillPop() {
    Navigator.pop(context, true);
    setState(() {
      print("Overlay closed");
      hide();
      overLayEntryCheck
          ? overLayEntryCheck = false
          : overLayEntryCheck = true;
      //questionsScreen.buttonstatus=false;

    });

  }

  @override
  Widget build(BuildContext context) {
    var _mediaQueryData = MediaQuery.of(context);
    screenWidthTotal = _mediaQueryData.size.width;
    screenHeightTotal = _mediaQueryData.size.height;
    _safeAreaHorizontal =
        _mediaQueryData.padding.left + _mediaQueryData.padding.right;
    _safeAreaVertical =
        _mediaQueryData.padding.top + _mediaQueryData.padding.bottom;
    screenHeight = screenHeightTotal - _safeAreaVertical;
    screenWidth = screenWidthTotal - _safeAreaHorizontal;

    return WillPopScope(
      onWillPop: _onWillPop,
      child: GestureDetector(
        onTap: (){
          setState(() {
            hide();
            overLayEntryCheck
                ? overLayEntryCheck = false
                : overLayEntryCheck = true;
          });
        },
        child: Scaffold(
          key: _scaffoldstate,
          backgroundColor: Colors.white,
          appBar: new AppBar(
            brightness: Brightness.light,
            elevation: 0.0,
            centerTitle: false,
            iconTheme: IconThemeData(
              color: Colors.black, //change your color here
            ),
            title: GestureDetector(
              onTap: () {
                //Navigator.pop(context);
                // Navigator.push(
                //   context,
                //   MaterialPageRoute(
                //       builder: (context) => EmaxIntroduction(widget.pos)),
                // );
                setState(() {
                  hide();
                  overLayEntryCheck
                      ? overLayEntryCheck = false
                      : overLayEntryCheck = true;
                  //questionsScreen.buttonstatus=false;
                });
              },
              child: Text(
                widget.pos.toString(),
                style: TextStyle(
                  color: Color.fromARGB(255, 0, 0, 0),
                  fontSize: 16 / 720 * screenHeight,
                  fontFamily: "IBMPlexSans",
                  fontWeight: FontWeight.w500,
                ),
              ),
            ),
            titleSpacing: 0.0,
            actions: <Widget>[],
            backgroundColor: Color.fromARGB(255, 255, 255, 255),
            leading: GestureDetector(
              onTap: () {
                int count = 0;
                //   Navigator.pop(context);
                Navigator.pop(context, true);

                // Navigator.push(
                //   context,
                //   MaterialPageRoute(
                //       bui000000000older: (context) => EmaxIntroduction(widget.pos)),
                // );
                setState(() {
                  hide();
                  overLayEntryCheck
                      ? overLayEntryCheck = false
                      : overLayEntryCheck = true;
                  //questionsScreen.buttonstatus=false;
                });
              },
              child: Container(
                // color: Colors.white,
                width: (28 / 360) * screenWidth,
                height: (24.0 / 678) * screenHeight,
                margin: EdgeInsets.only(left: 0.0),
                child: SvgPicture.asset(
                  getSvgIcon.backbutton,
                  height: (5 / 678) * screenHeight,
                  width: (14 / 360) * screenWidth,
                  fit: BoxFit.none,
                ),
              ),
            ),
          ),
          body: GestureDetector(
            onTap: () {
              setState(() {
                hide();
                overLayEntryCheck
                    ? overLayEntryCheck = false
                    : overLayEntryCheck = true;
                print("sfsfdsfsf");
              });
            },
            child:

            Container(
              //    color: Colors.red,


              child: SingleChildScrollView(
                physics: ArrowUpDown == true
                    ? NeverScrollableScrollPhysics()
                    : AlwaysScrollableScrollPhysics(),
                child: Column(
                  children:[
                    (getLearFirstHiraLevelTitles.length==0)?
                    Padding(
                        padding: EdgeInsets.only(top:screenHeight/2.5),
                        child: Center(child: CircularProgressIndicator()))
                        :
                    /// TEST NAMES DROPDOWN
                    GestureDetector(

                      onTap: () {
                        setState(() {
                          overLayEntryCheck
                              ? overLayEntryCheck = false
                              : overLayEntryCheck = true;
                          overLayEntryCheck ? show(context) : hide();
                        });
                      },
                      child:
                      Container(
                        height: 45 / 720 * screenHeight,
                        margin: EdgeInsets.only(
                            left: 20 / 360 * screenWidth,
                            top: 30 / 720 * screenHeight,
                            right: 20 / 360 * (screenWidth)),
                        decoration: BoxDecoration(

                          color: Color.fromARGB(255, 255, 255, 255),
                          border: Border.all(
                            color: Color.fromARGB(255, 242, 244, 244),
                            width: 1,
                          ),
                          borderRadius: BorderRadius.all(Radius.circular(5)),
                        ),
                        child: Row(
                          children: [
                            /// name of test names to be attempted
                            Container(
                              margin: EdgeInsets.only(left: 15 / 360 * screenWidth),
                              child: Text(
                                //         (widget.selectedvalue!=null)?widget.selectedvalue:
                                (selectedvalue.toString()=='null')?first:
                                selectedvalue.toString(),
                                style: TextStyle(
                                  color: Color.fromARGB(255, 0, 3, 44),
                                  fontSize: 14 / 720 * screenHeight,
                                  fontFamily: "IBMPlexSans",
                                  fontWeight: FontWeight.w500,
                                ),
                                textAlign: TextAlign.left,
                              ),
                            ),
                            Spacer(),
                            /// arrow up and down in dropdown
                            Container(
                              width: 7 / 306 * screenWidth,
                              height: 11 / 720 * screenHeight,
                              margin: EdgeInsets.only(right:19/ 360 * screenWidth,bottom: 17/720*screenHeight),
                              child: FlatButton(
                                //onPressed: () => this.onShapeCopy3Pressed(context),
                                  color: Colors.transparent,
                                  textColor: Color.fromARGB(255, 0, 0, 0),
                                  padding: EdgeInsets.all(0),
                                  child: Icon(
                                    ArrowUpDown
                                        ? Icons.keyboard_arrow_up
                                        : Icons.keyboard_arrow_down,
                                    color: NeutralColors.black,
                                    size: 22.0 / 360 * screenWidth,
                                  )),
                            ),
                          ],
                        ),
                      ),
                    ),
                    (getLearFirstHiraLevelTitles.length==0)?
                    Text('')
                        :
                    (((widget.selectedObjectTypeQ != null)?
                    widget.selectedObjectTypeQ:selectedObjectType)==true)?
                    /// layout for test other than introduction and conclusion ...............
                    Column(
                      children:[
                        /// instructions .........................
                        InkWell(
                          child: Container(
                            //  color:Colors.red,
                            child: Column(
                              children: <Widget>[
                                (instruction==null && desc == null)?
                                /// circular indicator if instructions is null.
                                Container(
                                  padding: EdgeInsets.only(top:20*720/screenHeight,bottom:
                                  20*720/screenHeight),
                                  child: CircularProgressIndicator(),
                                )
                                    :
                                /// instructions .......
                                desc == ""?
                                    Container():
                                InkWell(
                                  child: Container(
                                    //  width: 320/360*screenWidth,
                                    // height: 179/720*screenHeight,

                                    margin: EdgeInsets.only(
                                      top: 0 / 720 * screenHeight,
                                      left: 20 / 360 * screenWidth,
                                      right: 20 / 360 * screenWidth
                                    ),
                                    child: getChewiePlayerVimeoLink(false,OverLayEntryCheck,desc, testId,context),
                                  ),
                                  onTap: () {
                                    setState(() {
                                      hide();
                                      OverLayEntryCheck
                                          ? OverLayEntryCheck = false
                                          : OverLayEntryCheck = true;
                                      //questionsScreen.buttonstatus=false;
                                    });
                                  },
                                ),
                                instruction==""|| instruction == null?Container():
                                Container(
                                  //color: Colors.green,
                                    width: 320 / 360 * screenWidth,
                                    //height: 100,
                                    margin: EdgeInsets.only(
                                        left: 20 / 360 * screenWidth,
                                        top: 30 / 720 * screenHeight,
                                        right: 20 / 360 * screenWidth),


                                    child:Html(
                                      blockSpacing: 0.0,
                                      useRichText: true,
                                      data:instruction,
                                      defaultTextStyle: TextStyle(
                                        color: Color.fromARGB(255, 0, 3, 44),
                                        fontSize: 16 / 720 * screenHeight,
                                        fontFamily: "IBMPlexSans",
                                      ),
                                    )
                                ),
                              ],
                            ),
                          ),
                          onTap: () {
                            setState(() {
                              hide();
                              overLayEntryCheck
                                  ? overLayEntryCheck = false
                                  : overLayEntryCheck = true;
                            });
                          },
                        ),
                        /// button for review or start
                        instruction == null && courseName != "PGDBA"? Container() :
                        Container(
                          color: Colors.yellow,
                          height: 40 / 720 * screenHeight,
                          width: 270 / 360 * screenWidth,
                          margin: EdgeInsets.only(
                            left: 45 / 360 * screenWidth,
                            top: 30 / 720 * screenHeight,
                            bottom: 35 / 720 * screenHeight,
                            right: 45 / 360 * screenWidth,
                          ),


                          child:
                          (questionsAttempt==null)?CircularProgressIndicator():


                          PrimaryButtonGradient(
                            onTap: () {
//                              for(int z=0 ;z<getLearFirstHiraLevelTitles.length; z++){
//                                if(selectedvalue==getLearFirstHiraLevelTitles[z])
//                                  {
//                                    selectedValueOfNextTopic=getLearFirstHiraLevelTitles[z+1];
//                                  }
//                              }
                              //      ( widget.testStatusForCatch==null)?(widget.testStatusForCatch = testStatus[1]):widget.testStatusForCatch;

                              print("getTestIds[1]");
                              print("start invoked");
                              print(selectedId);
                              global.getToken.then((t) {
                                if (global.headersWithAuthorizationKey[
                                'Authorization'] !=
                                    '') {
                                  Map postdata = {
                                    "testId": selectedId
                                  };
                                  print("POSTMAN");
                                  print(postdata);

                                  ApiService()
                                      .postAPI(
                                      URL.TEST_LAUNCH_FOR_YES_PREPARE,
                                      postdata,
                                      t)
                                      .then((result) {
                                    setState(() {
                                      overLayEntryCheck
                                          ? overLayEntryCheck = false
                                          : overLayEntryCheck = true;
                                      hide();
                                      if (result[0] == 'successfully resume') {
                                        var totalData = result[1]['data'];
                                        global.tokenForTestLaunchApi = totalData['token'];
                                        setAccessTokenForProdTestLaunch(totalData['token']);
                                        print("start invoked");
                                        print(totalData['token']);
                                        print(testStatusForCatch);

                                        for(i=0;i<getLearFirstHiraLevelTitles.length;i++){
                                          if(getLearFirstHiraLevelTitles[i]==selectedvalue){
                                            indexForQAT=i;
                                          }
                                        }
                                        //       print("selectedvalue"+selectedvalue);
                                        //  selectedValueOfNextTopic =  getLearFirstHiraLevelTitles[index+1];
                                        //   print(selectedValueOfNextTopic);
                                        if(testStatusForCatch == "Completed"){
                                          AppRoutes.push(context, EmaximiserQuestionReviewScreenUsingHtml(
                                            testStatus: testStatusForCatch,
                                            listOfObjectType: objectTypeList,
                                            listOfId : testId,
                                            listOfTopic : getLearFirstHiraLevelTitles,
                                            listOfStatus : testStatus,
                                            pos: widget.pos,
                                            positionForTest : indexForIntoandCon,
                                          ));
                                        }else{
                                          print("value of i for question screen");
                                          print(questionsAttempt);
                                          print(indexForQAT);
                                          print(totalQuestions);
//                                            print(questionsAttempt[indexForQAT]);
//                                            print(totalQuestions[indexForQAT-1]);
                                          Navigator.of(context).push(new MaterialPageRoute(builder: (_)=>EmaximserQuestionScreen(
                                            listOfObjectType: objectTypeList,
                                            listOfId : testId,
                                            listOfTopic : getLearFirstHiraLevelTitles,
                                            listOfStatus : testStatus,
                                            pos: widget.pos,
                                            statusFromCatch:testStatusForCatch,
                                            positionForTest: indexForIntoandCon,
                                          )),).then((value) {
                                            if (value == true) {
                                              setState(() {
                                                print("inside set state");
                                                //       totalQuestions=[];
                                                questionsAttempt=[];
//                                                  indexForQAT=0;
//                                                  testId=[];
//                                                  objectTypeList=[];
                                                getLearFirstHiraLevelTitles =[];
                                                testStatus = [];
                                                getIdAndTitleForSubjectLevel() .then((returnValue) {
                                                  global.getToken.then((t){
                                                    authToken=t;
                                                    ApiService()
                                                        .getAPI(URL.EMAX_URL_DROP+getIdForSubjectLevelId, t)
                                                        .then((returnValue) {
                                                      setState(() {
                                                        if (returnValue[0] == 'Fetched Successfully') {
                                                          print("Data Fetched.....................");
                                                          for (var index = 0;
                                                          index < returnValue[1]['data'].length;
                                                          index++) {
                                                            //print("Indexxxx"+index.toString()+"="+widget.pos.toString()+"positionnn");
                                                            getLearFirstHiraLevelTitles
                                                                .add(returnValue[1]['data'][index]['component']['title']);
//                                                              objectTypeList
//                                                                  .add(returnValue[1]['data'][index]['component']['objectType']);
//
//
//                                                              testId.add(returnValue[1]['data'][index]['testId']);
//
//
//                                                              totalQuestions.add(returnValue[1]['data'][index]['component']['totalQuestions']);
//                                                              totalQuestions.removeWhere((value) => value == null);
                                                            testStatus.add(returnValue[1]['data'][index]['testStatus']);
                                                            questionsAttempt.add(returnValue[1]['data'][index]['questionAttempt']);
                                                            if(returnValue[1]['data'][index]['testId'] == selectedId){
                                                              print("Indexxxx"+returnValue[1]['data'][index]['testId'].toString()+"="+selectedId.toString()+"positionnn");
                                                              testStatusForCatch = returnValue[1]['data'][index]['testStatus'];
                                                            }

                                                            //  testStatusValue =  true;
                                                          }
                                                        }
                                                      });
                                                    });
                                                  });
                                                });
                                                // refresh page 1 here, you may want to reload your SharedPreferences here according to your needs
                                              });
                                            }
                                            else{
                                            }
                                          });

                                          questionsAttempt[indexForQAT] = 0;
                                        }
//                                        AppRoutes.push(
//                                            context,
//                                            EmaximserQuestionScreen(
//                                              listOfObjectType: objectTypeList,
//                                              listOfId : testId,
//                                              listOfTopic : getLearFirstHiraLevelTitles,
//                                              listOfStatus : testStatus,
//                                              pos: widget.pos,
//                                              statusFromCatch:testStatusForCatch,
//                                            ));
                                        print("..");
                                      } else {}
                                    });
                                  });
                                }
                              });
                              indexForIntoandCon=indexForIntoandCon+1;
                            },
                            text:
                            (testStatusForCatch == 'Start') ? "START":(testStatusForCatch == 'In-Progress') ? "RESUME":(testStatusForCatch == 'Completed')?"REVIEW":"",
                          ),
                        ),],
                    )
                        :
                    /// layout for introduction and conclusion ...............
                    GestureDetector(
                      child: Container(
                         // color:Colors.red,
                        //  height: 553 / 720 * screenHeight,
                        width: 320 / 360 * screenWidth,
                        margin: EdgeInsets.only(
                            left: 20 / 360 * screenWidth,
                            // top: 20 / 720 * screenHeight,
                            right: 20 / 360 * screenWidth),
                        child: Column(
                          children: <Widget>[
                            /// des in introduction ..........
//                             Container(
//                               // height: 102 / 720 * screenHeight,
//                               // width: 320 / 360 * screenWidth,
//                               margin: EdgeInsets.only(top: 30 / 720 * screenHeight),
//                               child:  Html(
//                                 blockSpacing: 0.0,
//                                 useRichText: false,
//                                 // backgroundColor:Colors.red ,
//                                 data:widget.desc,
//                                 defaultTextStyle:   TextStyle(
//                                   color: Color.fromARGB(255, 0, 3, 44),
//                                   fontSize: 16.0 / 720 * screenHeight,
//                                   fontFamily: "IBMPlexSans",
//                                   //   fontWeight: FontWeight.w500,
//
//                                 ),
// //backgroundColor: Colors.transparent,
//                               ),
//                             ),
                                /// what should know ............
                                Container(
                                  height: 52 / 720 * screenHeight,
                                  width: 320 / 360 * screenWidth,
                                  margin: EdgeInsets.only(top: 25 / 720 * screenHeight),
                                  child: Text(
                                    "What you should know before attempting \nthis session:",
                                    style: TextStyle(
                                      color: Color.fromARGB(255, 71, 89, 147),
                                      fontSize: 16.0 / 720 * screenHeight,
                                      fontFamily: "IBMPlexSans",
                                      fontWeight: FontWeight.w500,
                                    ),
                                    textAlign: TextAlign.left,
                                  ),
                                ),
                                /// video player ...........
                                InkWell(
                                  child: Container(
                                    //  width: 320/360*screenWidth,
                                    // height: 179/720*screenHeight,

                                    margin: EdgeInsets.only(
                                      top: 0 / 720 * screenHeight,
                                    ),
                                    child: getChewiePlayer(false,OverLayEntryCheck,selectedId, testId,context),
                                  ),
                                  onTap: () {
                                    setState(() {
                                      hide();
                                      OverLayEntryCheck
                                          ? OverLayEntryCheck = false
                                          : OverLayEntryCheck = true;
                                      //questionsScreen.buttonstatus=false;
                                    });
                                  },
                                ),
                                /// Next/Exit button
                                Container(
                                  width: 270 / 360 * screenWidth,
                                  height: 40 / 720 * screenHeight,
                                  margin: EdgeInsets.only(top: 30 / 720 * screenHeight,bottom: 100/720*screenHeight),
                                  child: PrimaryButtonGradient(
                                    onTap: () {
                                      global.getToken.then((t) {
                                        completeLessonTypeEndTest(t).then((value) {

                                          if  ((((selectedvalue)==(getLearFirstHiraLevelTitles[getLearFirstHiraLevelTitles.length-1])) && (selectedObjectType==false))
                                      )
                                      {
                                        overLayEntryCheck
                                            ? overLayEntryCheck = false
                                            : overLayEntryCheck = true;
                                        hide();
                                        // global.getToken.then((t) {
                                        //   getendtest(t);
                                        // });
                                        //Navigator.pop(context,true);
                                      }
                                      else{
                                        print("first here");
                                        print(indexForIntoandCon);
                                        print(selectedvalue);

                                        selectedvalue = getLearFirstHiraLevelTitles[indexForIntoandCon+1];
                                        selectedId = testId[indexForIntoandCon + 1 ];
                                        testStatusForCatch = testStatus[indexForIntoandCon + 1];
                                        instruction = null;
                                        desc = null;
                                        selectedObjectType = objectTypeList[indexForIntoandCon + 1];
                                        if(selectedObjectType == "Lesson"){
                                          selectedObjectType = false;
                                        }
                                        else{
                                          selectedObjectType = true;
                                        }
                                        if(selectedObjectType == true) {
                                          print("coming here");
                                          widget.selectedObjectTypeQ = true;
                                          global.getToken.then((t) {
                                            apiForTokenAndData(t);
                                          });
                                        }else{
                                          widget.selectedObjectTypeQ = false;
                                        }
                                        //
//                                  (getLearFirstHiraLevelTitles.length == 0)?
//                                  null
//                                      :
//                                  this.onGroup3Pressed(instruction,testStatusForCatch,(selectedObjectType=="Lesson")?selectedvaluenext.toString()??"Lesson":selectedvalue.toString(),selectedvalue.toString(),selectedId.toString(),testIdfirst.toString(),context);
//                            Navigator.push(
//                              context,
//                              MaterialPageRoute(
//                                  builder: (context) => onGroup3Pressed(context),
//                              ),  );
                                        indexForIntoandCon=indexForIntoandCon+1;
                                        setState(() {
                                          hide();
                                          OverLayEntryCheck
                                              ? OverLayEntryCheck = false
                                              : OverLayEntryCheck = true;
                                          //questionsScreen.buttonstatus=false;
                                        }
                                        );

                                      }
                                        });

                                        
                                      });
                                      
                                    },

                                    text: (((selectedvalue)==(getLearFirstHiraLevelTitles[getLearFirstHiraLevelTitles.length-1])) && (selectedObjectType==false))?"EXIT":"NEXT",
                                  ) ,
                                ),
                              ],
                            ),
                          ),
                      onTap: () {
                        setState(() {
                          hide();
                          OverLayEntryCheck
                              ? OverLayEntryCheck = false
                              : OverLayEntryCheck = true;
                          //questionsScreen.buttonstatus=false;
                        });
                      },
                    )

                  ],),
              ),
            ),

          ),
        ),
      ),
    );
  }

}

getChewiePlayer(bool offStageFlag, OverLayEntryCheck,selectedId,testId,BuildContext context){
  Offstage offstage = new Offstage(
    offstage: offStageFlag,
    child: Padding(
      padding: const EdgeInsets.only(top:10.0,bottom: 10.00,left: 1,right: 1),
      child: Container(
        margin: EdgeInsets.only(
        ),
        height: 185 / 720 * screenHeight,
        child: InkWell(
          onTap: () {
            OverLayEntryCheck
                ? OverLayEntryCheck = false
                : OverLayEntryCheck = true;
            if(Platform.Platform.isAndroid)
            {
              AppRoutes.push(context,selectedId!=null ? EmaxWebViewAndroid(selectedId,userid,null) : EmaxWebViewAndroid(testId[0],userid,null));
            }
            if(Platform.Platform.isIOS)
            {
              AppRoutes.push(context,selectedId!=null ? EmaxWebViewiOS(selectedId,userid,null) : EmaxWebViewiOS(testId[0],userid,null));
            }


          },
          child: Container(

            constraints: new BoxConstraints.expand(
              height: (181 / 720) * screenHeight,

            ),
            //alignment: Alignment.bottomLeft,
            //padding: new EdgeInsets.only(left: 16.0, bottom: 8.0),
            decoration:  BoxDecoration(
              borderRadius: new BorderRadius.circular(5.0),
              image: DecorationImage(
                //alignment: Alignment(-.2, 0),
                  image: AssetImage("assets/images/thumbnail.png"),
                  //CachedNetworkImageProvider(
                  //   'http://d1cn01kw45hc6h.cloudfront.net/wp-content/uploads/2019/08/22161210/Quadtratic.png'),
//                  image: NetworkImage(
//                      'http://d1cn01kw45hc6h.cloudfront.net/wp-content/uploads/2019/08/22161210/Quadtratic.png'
//                  ),
                  fit: BoxFit.cover),
            ),
            child: Center(
              child: Container(
                height: (40 / 720) * screenHeight,
                width: (40 / 360) * screenWidth,
                child: SvgPicture.asset(
                  "assets/images/playwhite.svg",
                  fit: BoxFit.fitHeight,
                ),
              ),
            ),
          ),
        ),
      ),
    ),
  );
  return offstage;
}

getChewiePlayerVimeoLink(bool offStageFlag, OverLayEntryCheck,selectedId,testId,BuildContext context){
  Offstage offstage = new Offstage(
    offstage: offStageFlag,
    child: Padding(
      padding: const EdgeInsets.only(top:10.0,bottom: 10.00,left: 1,right: 1),
      child: Container(
        margin: EdgeInsets.only(
        ),
        height: 185 / 720 * screenHeight,
        child: InkWell(
          onTap: () {
            OverLayEntryCheck
                ? OverLayEntryCheck = false
                : OverLayEntryCheck = true;
            if(Platform.Platform.isAndroid)
            {
              AppRoutes.push(context,selectedId!=null ? EmaxWebViewAndroid(null,null,selectedId) : null);
            }
            if(Platform.Platform.isIOS)
            {
              AppRoutes.push(context,selectedId!=null ? EmaxWebViewiOS(null,null,selectedId) : null);
            }
          },
          child: Container(

            constraints: new BoxConstraints.expand(
              height: (181 / 720) * screenHeight,

            ),
            //alignment: Alignment.bottomLeft,
            //padding: new EdgeInsets.only(left: 16.0, bottom: 8.0),
            decoration:  BoxDecoration(
              borderRadius: new BorderRadius.circular(5.0),
              image: DecorationImage(
                //alignment: Alignment(-.2, 0),
                  image: AssetImage("assets/images/thumbnail.png"),
                  //CachedNetworkImageProvider(
                  //   'http://d1cn01kw45hc6h.cloudfront.net/wp-content/uploads/2019/08/22161210/Quadtratic.png'),
//                  image: NetworkImage(
//                      'http://d1cn01kw45hc6h.cloudfront.net/wp-content/uploads/2019/08/22161210/Quadtratic.png'
//                  ),
                  fit: BoxFit.cover),
            ),
            child: Center(
              child: Container(
                height: (40 / 720) * screenHeight,
                width: (40 / 360) * screenWidth,
                child: SvgPicture.asset(
                  "assets/images/playwhite.svg",
                  fit: BoxFit.fitHeight,
                ),
              ),
            ),
          ),
        ),
      ),
    ),
  );
  return offstage;
}

class CustomBoxShadow extends BoxShadow {
  final BlurStyle blurStyle;

  const CustomBoxShadow({
    Color color = const Color(0xFF000000),
    Offset offset = Offset.zero,
    double blurRadius = 0.0,
    this.blurStyle = BlurStyle.normal,
  }) : super(color: color, offset: offset, blurRadius: blurRadius);

  @override
  Paint toPaint() {
    final Paint result = Paint()
      ..color = color
      ..maskFilter = MaskFilter.blur(this.blurStyle, blurSigma);
    assert(() {
      if (debugDisableShadows) result.maskFilter = null;
      return true;
    }());
    return result;
  }
}
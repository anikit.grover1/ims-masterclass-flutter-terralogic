import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_svg/svg.dart';
import 'package:imsindia/api/bloc/services.dart';
import 'package:imsindia/resources/strings/prepare.dart';
import 'package:imsindia/resources/strings/emaximiser.dart';
import 'package:imsindia/utils/Url.dart';
import 'package:imsindia/utils/colors.dart';
import 'package:imsindia/utils/svg_images/prepare_svg_images.dart';
import 'package:imsindia/views/Emaximiser/emaximizer_introduction.dart';
import 'package:imsindia/utils/global.dart' as global;
import 'package:shared_preferences/shared_preferences.dart';

class EmaximizerSummaryScreen extends StatefulWidget {
  var questionStatus = [];
  var totalQuestion = [];
  var selectedId;
  var selectedStatus;
  var selectedObjectType;
  var selectedvalue;
  var instruction;
  var pos;
  List listOfTopic;
  List listOfId;
  List listOfStatus;
  List listOfObjectType;
  var nameOfTheTopic;
  var testIdOfTheTopic;
  var positionForTest;
  @override
  EmaximizerSummaryScreen(this.nameOfTheTopic,this.testIdOfTheTopic,this.listOfTopic,this.listOfId,this.listOfObjectType,this.listOfStatus,this.pos,this.selectedId,this.selectedStatus,this.selectedObjectType,this.selectedvalue,this.instruction,this.questionStatus,this.totalQuestion,this.positionForTest);
  EmaximizerSummaryScreenState createState() => EmaximizerSummaryScreenState();
}

class EmaximizerSummaryScreenState extends State<EmaximizerSummaryScreen> {
  var questionCorrect = [];
  var questionIncorrect = [];
  var questionSkipped = [];
  var accessToken;
  var authToken;
  List<String> accessTokenList = [];
  List<String> finalAcessTokenList=[];

  /// end test Api ///
  endTestApi() async{
    if (global.headersWithAuthorizationKey['Authorization'] != '') {
      Map postdata = {"token": accessToken.toString()};
      ApiService().postAPI(URL.END_TEST_API_FOR_PREPARE, postdata, authToken).then((result) {
        if (this.mounted) {
          setState(() {
            // print(result[0]);
            if (result[0].toString().toLowerCase() == 'Successfully submitted'.toLowerCase()) {
              getFlagsForAnswerPopUp().then((_){
                finalAcessTokenList= accessTokenList;
                if(finalAcessTokenList.contains(accessToken.toString())){
                  finalAcessTokenList.remove(accessToken.toString());
                }
                for(var i=0;i<finalAcessTokenList.length;i++){
                  if(finalAcessTokenList[i]==null){
                    finalAcessTokenList[i]="";
                  }
                }
                setFlagsForAnswerPopUp(finalAcessTokenList) ;
              });
              int lastCheck;
              for (int z = 0; z < widget.listOfTopic.length - 1; z++) {
                if (widget.nameOfTheTopic == widget.listOfTopic[z] && widget.listOfId[z]==widget.testIdOfTheTopic) {
                  lastCheck = z+1;
                  widget.selectedvalue = widget.listOfTopic[z + 1];
                  widget.selectedId = widget.listOfId[z + 1];
                  widget.selectedStatus = widget.listOfStatus[z + 1];
                  widget.selectedObjectType = widget.listOfObjectType[z + 1];
                }
              }
              print(lastCheck);
              print(widget.listOfTopic.length);
              if(lastCheck != null){
                getNextTopicUnlock();
              }else{
                var count = 0;
                Navigator.popUntil(context, (route) {
                    
                    return count++ == 2;
                  });
                  Navigator.pop(context,true);
              }
              
            } else {}
          });
        }
      });
    }
  }

  void getNextTopicUnlock() {
    print("get next topic unlock");
    print(widget.selectedId.toString());
    if (global.headersWithAuthorizationKey[
    'Authorization'] !=
        '') {
      Map postdata = {
        "testId": widget.selectedId.toString()
      };
      var totalData;
      ApiService()
          .postAPI(
          URL.TEST_LAUNCH_FOR_YES_PREPARE,
          postdata,
          authToken)
          .then((result) {
        print(result);
        print("resultresultresultresultresultresultresultresult");
        // setState(() {
        //    if (result[0] == 'successfully resume') {
        totalData = result[1]['data'];
         var tokenForTestLaunchApi = totalData['token'];

//          } else {
//            print("else part");
//          }
        Map postdata_token = {
          "token": tokenForTestLaunchApi,
        };
        //    widget.statusFromCatch = "In-Progress";


        if (widget.selectedObjectType == "Lesson") {
          widget.selectedObjectType = false;
          var count = 0;
              Navigator.popUntil(context, (route) {
                  return count++ == 2;
                });
          //Navigator.pop(context);
          Navigator.pushReplacement(
            context,
            MaterialPageRoute(
                builder: (context) =>
                    EmaxIntroduction(
                      instruction: widget.instruction,
                      selectedStatus: widget.selectedStatus,
                      selectedId: widget.selectedId,
                      selectedvalue: widget.selectedvalue,
                      pos: widget.pos,
                      desc: '',
                      selectedObjectTypeQ: widget.selectedObjectType,
                        positionForTest:widget.positionForTest+1
                    )),

          );
        }
        else {
          print("in else part");
          print(((widget.selectedStatus == "In-Progress") || (widget.selectedStatus == "Start"))
              ?
          URL.GET_QUESTIONS_DATA_FOR_PREPARE
              : URL.EMAX_PROD_REVISE);
          widget.selectedObjectType = true;
          ApiService().postAPI(
              ((widget.selectedStatus == "In-Progress") || (widget.selectedStatus == "Start"))
                  ?
              URL.GET_QUESTIONS_DATA_FOR_PREPARE
                  : URL.EMAX_PROD_REVISE,
              postdata_token,
              authToken)
              .then((result) {
            //  setState(() {
            if (result[0].toString().toLowerCase()== 'success'.toLowerCase()) {
              var totalData = result[1]['data'];
              widget.instruction = totalData['testJson']['Instruction'];
              print("in else part");
              var count = 0;
              Navigator.popUntil(context, (route) {
                  return count++ == 2;
                });
              Navigator.pushReplacement(
                context,
                MaterialPageRoute(
                    builder: (context) =>
                        EmaxIntroduction(
                          instruction: widget.instruction,
                          selectedStatus: widget.selectedStatus,
                          selectedId: widget.selectedId,
                          selectedvalue: widget.selectedvalue,
                          pos: widget.pos,
                          desc: '',
                          selectedObjectTypeQ: widget.selectedObjectType,
                            positionForTest:widget.positionForTest+1
                        )),

              );
            } else {}
            //  });
          });
        }
      });
      // });
    }

    //  dispose();
  }
  /// set flags for answer popup
  setFlagsForAnswerPopUp(value) async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setStringList("testIdForEmax",value);
  }

  getFlagsForAnswerPopUp() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    var sharedValue=prefs.getStringList("testIdForEmax") ?? [];
    print(sharedValue);
    print(sharedValue);
    print("shared value");

    if(sharedValue==null ||sharedValue.toString()=="null"||sharedValue.toString()=="Null"){
      print("going to if conditionnnnnnnnnnnnnnnn");
      accessTokenList=[];
      accessTokenList.add(sharedValue.toString());
    }else if(sharedValue.length==0){
      accessTokenList=[];
      accessTokenList.add(null);
    } else{
      print("going to else condition");
      accessTokenList = sharedValue;
    }

  }

  /// To get no. of correct, incorrect and skipped question ///
  void getQuestionStatus() {
    print("Question Status "+widget.questionStatus.toString());
    for (var i = 0; i < widget.questionStatus.length; i++){
      if (widget.questionStatus[i] == true){
        questionCorrect.add("1");
      }else if (widget.questionStatus[i] == false){
        questionIncorrect.add("1");
      }else if(widget.questionStatus[i] == null){
        questionSkipped.add("1");
      }
    }
  }

  /// getting access token from shared pref ///
  void getAccessTokenForProdTestLaunch() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    accessToken = prefs.getString("accessTokenEmax");
  }

  @override
  void initState() {

    // TODO: implement initState
    super.initState();
    global.getToken.then((t){
      authToken=t;
      getAccessTokenForProdTestLaunch();
    });
    getQuestionStatus();
  }

  Widget build(BuildContext context) {
    final screenHeight = MediaQuery.of(context).size.height;
    final screenWidth = MediaQuery.of(context).size.width;
    Future<bool> _onWillPop() {
      int count = 0;
      Navigator.popUntil(context, (route) {
        return count++ == 2;
      });
    }
    return WillPopScope(
      onWillPop: _onWillPop,
      child: Scaffold(
        body: Container(
          constraints: BoxConstraints.expand(),
          decoration: BoxDecoration(
            color:NeutralColors.pureWhite,
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              /// back arrow and title
              Container(
                width: double.infinity,
                margin: EdgeInsets.only(top: 50 / 720 * screenHeight),
                child: Row(
                  children: [
                    /// back arrow .............. ///
//                    InkWell(
//                      onTap: () {
//                        setState(() {
//                        },);
//                      },
//                      child: Container(
//                        height: (14 / 678) * screenHeight,
//                        width: (16 / 360) * screenWidth,
//                        margin: EdgeInsets.only(
//                            left: 18 / 360 * screenWidth,
//                            top: 12.5 / 720 * screenHeight),
//                        child: SvgPicture.asset(
//                          getPrepareSvgImages.backIcon,
//                          fit: BoxFit.fill,
//                        ),
//                      ),
//                    ),

                    /// title ........... ///
                    Container(
                      width: 200 / 360 * screenWidth,
                      margin: EdgeInsets.only(
                          left: 12 / 360 * screenWidth,
                          top: 10 / 720 * screenHeight),
                      child: Text(
                        widget.nameOfTheTopic + " Summary",
                        maxLines: 3,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(
                          color: NeutralColors.black,
                          fontSize: 18 / 360 * screenWidth,
                          fontFamily: "IBMPlexSans",
                          fontWeight: FontWeight.w700,
                        ),
                        textAlign: TextAlign.left,
                      ),
                    ),
                  ],
                ),
              ),
              /// data regarding the test
              Align(
                alignment: Alignment.topCenter,
                child: Container(
                  width: 300/360*screenWidth,
//                height: 257,
                  margin: EdgeInsets.only(top: 50/720*screenHeight),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      /// total questions .......
                      Container(
                        height: 45/720*screenHeight,
                        child: Stack(
                          alignment: Alignment.center,
                          children: [
                            Positioned(
                              left: 0,
                              top: 0,
                              right: 0,
                              child: Opacity(
                                opacity: 0.05,
                                child: Container(
                                  height: 45/720*screenHeight,
                                  decoration: BoxDecoration(
                                    gradient: LinearGradient(
                                      begin: Alignment(-0.011, 0.507),
                                      end: Alignment(1.031, 0.483),
                                      stops: [
                                        0,
                                        1,
                                      ],
                                      colors: [
                                        Color.fromARGB(255, 168, 174, 35),
                                        Color.fromARGB(255, 201, 110, 216),
                                      ],
                                    ),
                                    borderRadius: BorderRadius.all(Radius.circular(5)),
                                  ),
                                  child: Container(),
                                ),
                              ),
                            ),
                            Positioned(
                              left: 25/360*screenWidth,
                              top: 6/720*screenHeight,
                              right: 28/360*screenWidth,
                              child: Container(
                                height: 45/720*screenHeight,
                                child: Row(
                                  crossAxisAlignment: CrossAxisAlignment.stretch,
                                  children: [
                                    Align(
                                      alignment: Alignment.topLeft,
                                      child: Container(
                                        margin: EdgeInsets.only(top: 8/720*screenHeight),
                                        child: Text(
                                          SummaryScreenStrings.Total,
                                          style: TextStyle(
                                            color: Color.fromARGB(255, 0, 3, 44),
                                            fontSize: 14/678*screenHeight,
                                            fontFamily: "IBMPlexSans",
                                          ),
                                          textAlign: TextAlign.left,
                                        ),
                                      ),
                                    ),
                                    Spacer(),
                                    Align(
                                      alignment: Alignment.topLeft,
                                      child: Container(
                                        margin: EdgeInsets.only(top: 8/720*screenHeight),
                                        child: Text(
                                          widget.totalQuestion.length.toString(),
                                          style: TextStyle(
                                            color: Color.fromARGB(255, 0, 3, 44),
                                            fontSize: 14/678*screenHeight,
                                            fontFamily: "IBMPlexSans",
                                            fontWeight: FontWeight.w500,
                                          ),
                                          textAlign: TextAlign.left,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      /// correct
                      Container(
                        height: 45/720*screenHeight,
                        margin: EdgeInsets.only(top: 8/720*screenHeight),
                        child: Stack(
                          alignment: Alignment.center,
                          children: [
                            Positioned(
                              left: 0,
                              top: 0,
                              right: 0,
                              child: Opacity(
                                opacity: 0.05,
                                child: Container(
                                  height: 45/720*screenHeight,
                                  decoration: BoxDecoration(
                                    gradient: LinearGradient(
                                      begin: Alignment(-0.011, 0.507),
                                      end: Alignment(1.031, 0.483),
                                      stops: [
                                        0,
                                        1,
                                      ],
                                      colors: [
                                        Color.fromARGB(255, 168, 174, 35),
                                        Color.fromARGB(255, 201, 110, 216),
                                      ],
                                    ),
                                    borderRadius: BorderRadius.all(Radius.circular(5)),
                                  ),
                                  child: Container(),
                                ),
                              ),
                            ),
                            Positioned(
                              left: 25/360*screenWidth,
                              top: 6/720*screenHeight,
                              right: 28/360*screenWidth,
                              child: Container(
                                height: 45/720*screenHeight,
                                child: Row(
                                  crossAxisAlignment: CrossAxisAlignment.stretch,
                                  children: [
                                    Align(
                                      alignment: Alignment.topLeft,
                                      child: Container(
                                        margin: EdgeInsets.only(top: 8/720*screenHeight),
                                        child: Text(
                                          SummaryScreenStrings.Correct,
                                          style: TextStyle(
                                            color: Color.fromARGB(255, 0, 3, 44),
                                            fontSize: 14/678*screenHeight,
                                            fontFamily: "IBMPlexSans",
                                          ),
                                          textAlign: TextAlign.left,
                                        ),
                                      ),
                                    ),
                                    Spacer(),
                                    Align(
                                      alignment: Alignment.topLeft,
                                      child: Container(
                                        margin: EdgeInsets.only(top: 8/720*screenHeight),
                                        child: Text(
                                          questionCorrect.length.toString(),
                                          style: TextStyle(
                                            color: Color.fromARGB(255, 0, 3, 44),
                                            fontSize: 14/678*screenHeight,
                                            fontFamily: "IBMPlexSans",
                                            fontWeight: FontWeight.w500,
                                          ),
                                          textAlign: TextAlign.left,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      /// incorrect
                      Container(
                        height: 45/720*screenHeight,
                        margin: EdgeInsets.only(top: 8/720*screenHeight),
                        child: Stack(
                          alignment: Alignment.center,
                          children: [
                            Positioned(
                              left: 0,
                              top: 0,
                              right: 0,
                              child: Opacity(
                                opacity: 0.05,
                                child: Container(
                                  height: 45/720*screenHeight,
                                  decoration: BoxDecoration(
                                    gradient: LinearGradient(
                                      begin: Alignment(-0.011, 0.507),
                                      end: Alignment(1.031, 0.483),
                                      stops: [
                                        0,
                                        1,
                                      ],
                                      colors: [
                                        Color.fromARGB(255, 168, 174, 35),
                                        Color.fromARGB(255, 201, 110, 216),
                                      ],
                                    ),
                                    borderRadius: BorderRadius.all(Radius.circular(5)),
                                  ),
                                  child: Container(),
                                ),
                              ),
                            ),
                            Positioned(
                              left: 25/360*screenWidth,
                              top: 6/720*screenHeight,
                              right: 28/360*screenWidth,
                              child: Container(
                                height: 45/720*screenHeight,
                                child: Row(
                                  crossAxisAlignment: CrossAxisAlignment.stretch,
                                  children: [
                                    Align(
                                      alignment: Alignment.topLeft,
                                      child: Container(
                                        margin: EdgeInsets.only(top: 8/720*screenHeight),
                                        child: Text(
                                          SummaryScreenStrings.InCorrect,
                                          style: TextStyle(
                                            color: Color.fromARGB(255, 0, 3, 44),
                                            fontSize: 14/678*screenHeight,
                                            fontFamily: "IBMPlexSans",
                                          ),
                                          textAlign: TextAlign.left,
                                        ),
                                      ),
                                    ),
                                    Spacer(),
                                    Align(
                                      alignment: Alignment.topLeft,
                                      child: Container(
                                        margin: EdgeInsets.only(top: 8/720*screenHeight),
                                        child: Text(
                                          questionIncorrect.length.toString(),
                                          style: TextStyle(
                                            color: Color.fromARGB(255, 0, 3, 44),
                                            fontSize: 14/678*screenHeight,
                                            fontFamily: "IBMPlexSans",
                                            fontWeight: FontWeight.w500,
                                          ),
                                          textAlign: TextAlign.left,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      /// skipped
                      Container(
                        height: 45/720*screenHeight,
                        margin: EdgeInsets.only(top: 8/720*screenHeight),
                        child: Stack(
                          alignment: Alignment.center,
                          children: [
                            Positioned(
                              left: 0,
                              top: 0,
                              right: 0,
                              child: Opacity(
                                opacity: 0.05,
                                child: Container(
                                  height: 45/720*screenHeight,
                                  decoration: BoxDecoration(
                                    gradient: LinearGradient(
                                      begin: Alignment(-0.011, 0.507),
                                      end: Alignment(1.031, 0.483),
                                      stops: [
                                        0,
                                        1,
                                      ],
                                      colors: [
                                        Color.fromARGB(255, 168, 174, 35),
                                        Color.fromARGB(255, 201, 110, 216),
                                      ],
                                    ),
                                    borderRadius: BorderRadius.all(Radius.circular(5)),
                                  ),
                                  child: Container(),
                                ),
                              ),
                            ),
                            Positioned(
                              left: 25/360*screenWidth,
                              top: 6/720*screenHeight,
                              right: 28/360*screenWidth,
                              child: Container(
                                height: 45/720*screenHeight,
                                child: Row(
                                  crossAxisAlignment: CrossAxisAlignment.stretch,
                                  children: [
                                    Align(
                                      alignment: Alignment.topLeft,
                                      child: Container(
                                        margin: EdgeInsets.only(top: 8/720*screenHeight),
                                        child: Text(
                                          SummaryScreenStrings.Skipped,
                                          style: TextStyle(
                                            color: Color.fromARGB(255, 0, 3, 44),
                                            fontSize: 14/678*screenHeight,
                                            fontFamily: "IBMPlexSans",
                                          ),
                                          textAlign: TextAlign.left,
                                        ),
                                      ),
                                    ),
                                    Spacer(),
                                    Align(
                                      alignment: Alignment.topLeft,
                                      child: Container(
                                        margin: EdgeInsets.only(top: 8/720*screenHeight),
                                        child: Text(
                                          questionSkipped.length.toString(),
                                          style: TextStyle(
                                            color: Color.fromARGB(255, 0, 3, 44),
                                            fontSize: 14/720*screenHeight,
                                            fontFamily: "IBMPlexSans",
                                            fontWeight: FontWeight.w500,
                                          ),
                                          textAlign: TextAlign.left,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              /// submit confirmation text ..........
              Align(
                alignment: Alignment.topCenter,
                child: Container(
                  margin: EdgeInsets.only(bottom: 20 / 720 * screenHeight,left: 10.0 / 360 * screenWidth,
                      right: 10.0 / 360 * screenWidth,top: 50/720*screenHeight),
                  child: Text(
                    SummaryScreenStrings.submitConfirmInSummary,
                    style: TextStyle(
                      color: NeutralColors.black,
                      fontSize: screenHeight * 16 / 720,
                      fontFamily: "IBMPlexSans",
                      fontWeight: FontWeight.w500,
                    ),
                    textAlign: TextAlign.center,
                  ),
                ),
              ),
              /// yes or no buttons ....................
              Container(
                height: screenHeight * 47.5 / 720,
                padding: EdgeInsets.only(
                    left: 40.0 / 360 * screenWidth,
                    right: 40.0 / 360 * screenWidth),
                child: Row(
                  children: [
                    /// yes button in summary
                    Container(
                      width: screenWidth * 130 / 360,
                      height: screenHeight * 40 / 720,
                      margin: EdgeInsets.only(top: 1 / 720 * screenHeight),
                      child: FlatButton(
                        onPressed: () {
                          //Navigator.pop(context, true);
                          endTestApi();
                        },
                        color: NeutralColors.ice_blue,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.all(Radius.circular(2)),
                        ),
                        textColor: NeutralColors.blue_grey,
                        padding: EdgeInsets.all(0),
                        child: Text(
                          PrepareParagraphScreenStrings.yesForPopUpExit,
                          style: TextStyle(
                            fontSize: screenHeight * 14 / 678,
                            fontFamily: "IBMPlexSans",
                            fontWeight: FontWeight.w500,
                          ),
                          textAlign: TextAlign.center,
                        ),
                      ),
                    ),
                    Spacer(),
                    /// no button in summary
                    Container(
                      width: screenWidth * 130 / 360,
                      height: screenHeight * 40 / 720,
                      // margin: EdgeInsets.only(top: 1/720*screenHeight),
                      child: FlatButton(
                        onPressed: () {
                          Navigator.pop(context, true);
                        },
                        color: Color.fromARGB(255, 0, 171, 251),
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.all(Radius.circular(3)),
                        ),
                        textColor: NeutralColors.pureWhite,
                        padding: EdgeInsets.all(0.0),
                        child: Text(
                          PrepareParagraphScreenStrings.noForPopUpExit,
                          style: TextStyle(
                            fontSize: screenHeight * 14 / 678,
                            fontFamily: "IBMPlexSans",
                            fontWeight: FontWeight.w500,
                          ),
                          textAlign: TextAlign.center,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),




        ),

      ),
    );
  }
}












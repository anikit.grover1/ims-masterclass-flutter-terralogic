
//import 'package:advance_pdf_viewer_fork/advance_pdf_viewer_fork.dart';
import 'package:flutter/material.dart';
//import 'package:advance_pdf_viewer/advance_pdf_viewer.dart';
import 'package:pdf_viewer_jk/pdf_viewer_jk.dart';


class Emaximiser_PDFScreen extends StatefulWidget {
  var pdflink;

  @override
  Emaximiser_PDFScreenState createState() => Emaximiser_PDFScreenState();

  Emaximiser_PDFScreen(this.pdflink);
}

class Emaximiser_PDFScreenState extends State<Emaximiser_PDFScreen> {

  bool _isLoading = true;
  PDFDocument document;

  loadDocument(String url) async {
    document = await PDFDocument.fromURL(url);

    setState(() => _isLoading = false);
  }


  void initState(){
    super.initState();
    print(widget.pdflink.toString());

    loadDocument(widget.pdflink.toString());

  }

  @override
  Widget build(BuildContext context) {
    return  Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.white,
          brightness: Brightness.light,
          iconTheme: IconThemeData(color: Colors.black),
          elevation: 0.0,
          centerTitle: false,
          titleSpacing: 0.0,
          title: Text(
            "",
            style: const TextStyle(
                color: Colors.black,
                fontWeight: FontWeight.w500,
                fontFamily: "IBMPlexSans",
                fontStyle: FontStyle.normal,
                fontSize: 16.0),
          ),
          // leading: IconButton(
          //   icon: getPrepareSvgImages.backIcon,
          //   onPressed: () => Navigator.pop(context, false),
          // ),
        ),
        body: Center(
          child: _isLoading
              ? Center(child: CircularProgressIndicator())
              : PDFViewer(
            document: document,

            zoomSteps: 1,

            //uncomment below line to preload all pages
            lazyLoad: false,
            // uncomment below line to scroll vertically
            // scrollDirection: Axis.vertical,
          ),
        ),

    );
  }
}


import 'package:flutter/material.dart';

class QuestionOption{
  String qid;
  String question;
  String answer;
  String paragraph;
  var options;
  PageController controller;
  int pos;
  var imageURL;
  List<Map<String, dynamic>> option;

  QuestionOption({
    this.qid,
    this.question,
    this.paragraph,
    this.options,
    this.controller,
    this.imageURL,
    this.answer,
    this.option
  });
  QuestionOption.pos(this.pos);

}

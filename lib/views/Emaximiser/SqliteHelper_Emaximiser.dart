import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:imsindia/views/Emaximiser/EmaximiserContent.dart';
import 'package:imsindia/utils/Url.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

class SqliteHelper_Emaximiser {
  String dbName = '';
  Future<Database> database;

  SqliteHelper_Emaximiser(String dbName) {
    this.dbName = dbName;
  }

  createDB() async {
    debugPrint(
      'Emaximiser table definition ' +
          "CREATE TABLE ${URL.TBL_EmaximiserContent}(areaName TEXT,attempt BOOLEAN,companyCode TEXT,difficultyId INT,difficultyName TEXT,enteredText TEXT,id VARCHAR,isCorrect BOOLEAN,isSectionCompleted TEXT,itemId TEXT,itemType TEXT,marked BOOLEAN,negativePoints TEXT,points TEXT, questionIndex TEXT,sectionId TEXT,sectionIndex TEXT, sectionName TEXT,selectedOptionId TEXT,studentId VARCHAR,subjectId INT,subjectName TEXT,testId VARCHAR,timeRemaining TEXT,timeStamp VARCHAR,timeTaken TEXT, topicId TEXT,topicName TEXT,trackNumber TEXT,saved INT,areaid INT)",
    );
    database = openDatabase(
      // Set the path to the database. Note: Using the `join` function from the
      // `path` package is best practice to ensure the path is correctly
      // constructed for each platform.
      join(await getDatabasesPath(), dbName),
      // When the database is first created, create a table to store dogs.
      onCreate: (db, version) {
        return db.execute(
          "CREATE TABLE ${URL.TBL_EmaximiserContent}(areaName TEXT,attempt BOOLEAN,companyCode TEXT,difficultyId INT,difficultyName TEXT,enteredText TEXT,id TEXT,isCorrect TEXT,isSectionCompleted TEXT,itemId INT,itemType TEXT,marked TEXT,negativePoints INT,points INT, questionIndex INT,sectionId INT,sectionIndex INT, sectionName TEXT,selectedOptionId TEXT,studentId TEXT,subjectId INT,subjectName TEXT,testId TEXT,timeRemaining INT,timeStamp TEXT,timeTaken INT, topicId INT,topicName TEXT,trackNumber INT,saved INT,areaid INT)",
        );
      },
      // Set the version. This executes the onCreate function and provides a
      // path to perform database upgrades and downgrades.
      version: 1,
    );

    debugPrint('Emaximiser db created successfully');
  }

  Future<void> insert(EmaximiserContent data) async {
    // Get a reference to the database.
    Database db = await database;

    // Insert the BlogContent into the correct table. Also specify the
    // `conflictAlgorithm`. In this case, if the same BlogContent is inserted
    // multiple times, it replaces the previous data.
    if (db == null) {
      db = await openDatabase(
        // Set the path to the database. Note: Using the `join` function from the
        // `path` package is best practice to ensure the path is correctly
        // constructed for each platform.
        join(await getDatabasesPath(), 'imsAppModule.db'),
      );
    }
    debugPrint('Emax map of data : ${data.toMap()}');
    debugPrint('Emax String of data : ${data.toString()}');
    await db.insert(
      '${URL.TBL_EmaximiserContent}',
      data.toMap(),
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
  }


  Future<List<EmaximiserContent>> getData() async {
    // Get a reference to the database.
    Database db = await database;

    if (db == null) {
      db = await openDatabase(
        // Set the path to the database. Note: Using the `join` function from the
        // `path` package is best practice to ensure the path is correctly
        // constructed for each platform.
        join(await getDatabasesPath(), dbName),
      );
    }
    // Query the table for all The BlogContent.
    final List<Map<String, dynamic>> maps = await db.query('emaximisercontent');

    // Convert the List<Map<String, dynamic> into a List<Dog>.
    print("MAPS ${maps}");
    return List.generate(maps.length, (i) {
      print( maps[i]['timeRemaining']);
      return EmaximiserContent(
          areaName: maps[i]['areaName'],
          attempt: (maps[i]['attempt']==1)?true:false,
          companyCode: maps[i]['companyCode'],
          difficultyId: maps[i]['difficultyId'],
        difficultyName: maps[i]['difficultyName'],
        enteredText: maps[i]['enteredText'],
        id: maps[i]['id'],
        isCorrect: (maps[i]['isCorrect']==1)?true:false,
        isSectionCompleted: maps[i]['isSectionCompleted'],
        itemId: maps[i]['itemId'],
        itemType: maps[i]['itemType'],
        marked: (maps[i]['marked']==1)?true:false,
        negativePoints: maps[i]['negativePoints'],
        points: maps[i]['points'],
        questionIndex: maps[i]['questionIndex'],
        sectionId: maps[i]['sectionId'],
        sectionIndex: maps[i]['sectionIndex'],
        sectionName: maps[i]['sectionName'],
        selectedOptionId: maps[i]['selectedOptionId'],
        studentId: maps[i]['studentId'],
        subjectId: maps[i]['subjectId'],
        subjectName: maps[i]['subjectName'],
        testId: maps[i]['testId'],
        timeRemaining: maps[i]['timeRemaining'],
        timeStamp: maps[i]['timeStamp'],
        timeTaken: maps[i]['timeTaken'],
        topicId: maps[i]['topicId'],
        topicName: maps[i]['topicName'],
        trackNumber: maps[i]['trackNumber'],
        saved: maps[i]['saved'],
        areaid: maps[i]['areaid']

      );


//      return Dog(
//        id: maps[i]['id'],
//        name: maps[i]['name'],
//        age: maps[i]['age'],
//      );
    });
  }

  Future<void> update(EmaximiserContent data) async {
    // Get a reference to the database.
    Database db = await database;
    if (db == null) {
      db = await openDatabase(
        // Set the path to the database. Note: Using the `join` function from the
        // `path` package is best practice to ensure the path is correctly
        // constructed for each platform.
        join(await getDatabasesPath(), dbName),
      );
    }
    print("======================updatetrack");
    print(data.trackNumber);
    print(data.saved);
    // Update the given BlogContent.
    await db.update(
      '${URL.TBL_EmaximiserContent}',
      data.toMap(),

      where: "trackNumber=?",
      // Pass the BlogContent's id as a whereArg to prevent SQL injection.
      whereArgs: [data.trackNumber],
    );
  }

  Future<void> delete() async {
    // Get a reference to the database.
    Database db = await database;

    if (db == null) {
      db = await openDatabase(
        // Set the path to the database. Note: Using the `join` function from the
        // `path` package is best practice to ensure the path is correctly
        // constructed for each platform.
        join(await getDatabasesPath(), dbName),
      );
    }
    // Remove the BlogContent from the database.
    await db.delete(
      '${URL.TBL_EmaximiserContent}',
      // Use a `where` clause to delete a specific BlogContent.
      where: "content = ?",
      // Pass the BlogContent's id as a whereArg to prevent SQL injection.
      whereArgs: [],
    );
  }

  Future<void> deleteAll() async {
    // Get a reference to the database.
    Database db = await database;
    debugPrint('In Emaximiser deleteAll delete from ${URL.TBL_EmaximiserContent}');
    if (db == null) {
      db = await openDatabase(
        // Set the path to the database. Note: Using the `join` function from the
        // `path` package is best practice to ensure the path is correctly
        // constructed for each platform.
        join(await getDatabasesPath(), dbName),
      );
    }
    // Remove the BlogContent from the database.
    await db.rawDelete(
      'delete from ${URL.TBL_EmaximiserContent}',
      // Use a `where` clause to delete a specific BlogContent.
    );
    debugPrint('Emaximiser db deleted successfully');

  }

  Future<void> deleteTable() async {
    // Get a reference to the database.
    Database db = await database;
    debugPrint('In Emaximiser deleteAll drop table ${URL.TBL_EmaximiserContent}');
    if (db == null) {
      db = await openDatabase(
        // Set the path to the database. Note: Using the `join` function from the
        // `path` package is best practice to ensure the path is correctly
        // constructed for each platform.
        join(await getDatabasesPath(), dbName),
      );
    }
    // Remove the BlogContent from the database.
    await db.rawDelete(
      'drop table ${URL.TBL_EmaximiserContent}',
      // Use a `where` clause to delete a specific BlogContent.
    );
    debugPrint('Emaximiser table deleted successfully');

  }
  Future <void> closeConnection() async{
    Database db = await database;
    db.close();
  }

}

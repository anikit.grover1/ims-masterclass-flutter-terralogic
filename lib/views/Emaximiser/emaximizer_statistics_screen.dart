
import 'package:flutter/material.dart';
import '../../routers/routes.dart';
import 'package:imsindia/components/stop_watch_timer.dart';
import 'package:imsindia/components/custom_scrollbar_component.dart';
import 'package:imsindia/resources/strings/prepare.dart';
import 'package:imsindia/utils/colors.dart';
import 'package:imsindia/views/Emaximiser/emaximizer_question_screen.dart';
import 'package:imsindia/views/Emaximiser/emaximiser_question_review_screen.dart';

var screenWidthTotal;
var screenHeightTotal;

var _safeAreaHorizontal;

var _safeAreaVertical;

var screenHeight;

var screenWidth;

class EmaximiserStatisticScreen extends StatefulWidget {
  final Stopwatch stopwatch;
  bool bargraphActiveStatus;
  final int backtoquesnum_bargraph;
  String timeTakenForQuestion;
  String statusForQuestion;
  EmaximserQuestionScreenState EmaximserQuestionScreen;
  EmaximiserQuestionReviewScreenUsingHtmlState EmaximiserQuestionsReviewScreen;

  EmaximiserStatisticScreen({this.stopwatch,this.bargraphActiveStatus,this.backtoquesnum_bargraph,this.EmaximserQuestionScreen,this.EmaximiserQuestionsReviewScreen,this.timeTakenForQuestion, this. statusForQuestion});

  @override
  _EmaximiserStatisticScreenHtmlState createState() => _EmaximiserStatisticScreenHtmlState();
}

class _EmaximiserStatisticScreenHtmlState extends State<EmaximiserStatisticScreen> {
  final Dependencies dependencies = new Dependencies();
  final ScrollController controller = ScrollController();

  @override
  bool pressed = false;
  bool pressAttention = true;
  Widget build(BuildContext context) {
    var _mediaQueryData = MediaQuery.of(context);
    screenWidthTotal = _mediaQueryData.size.width;
    screenHeightTotal = _mediaQueryData.size.height;
    _safeAreaHorizontal =
        _mediaQueryData.padding.left + _mediaQueryData.padding.right;
    _safeAreaVertical =
        _mediaQueryData.padding.top + _mediaQueryData.padding.bottom;

    screenHeight = screenHeightTotal - _safeAreaVertical;
    screenWidth = screenWidthTotal - _safeAreaHorizontal;

    return Container(
      height: (476/720)*screenHeight,
      margin: EdgeInsets.only(left: (20/360)*screenWidth),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Align(
            alignment: Alignment.topLeft,
            child:
            InkWell(child:Container(
              width: (150/360)*screenWidth,
              height: (23/720)*screenHeight,
              // margin: EdgeInsets.only(top:(39/720)*screenHeight),
              child: Row(
                // mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                      child:Icon(
                        Icons.arrow_back_ios,
                        color: PrimaryColors.azure_Dark,
                        size: 15/720*screenHeight,
                      )
                  ),
                  Container(
                    margin: EdgeInsets.only(left: (8/360)*screenWidth),
                    child:Text(
                      StatisticsScreenStrings.Text_BackToQuestion,
                      style: TextStyle(
                        color: Color.fromARGB(255, 0, 171, 251),
                        fontSize: (14/360)*screenWidth,
                        fontFamily: "IBMPlexSans",
                      ),
                      textAlign: TextAlign.left,
                    ),



                  ),
                ],
              ),
            ),
              onTap: (){
                setState(() {
                  dependencies.stopwatch = widget.stopwatch;
                  if(widget.EmaximserQuestionScreen != null && widget.EmaximiserQuestionsReviewScreen == null){
                    widget.EmaximserQuestionScreen.setState(() {
                      widget.EmaximserQuestionScreen.emaxHeader_status_question=3;
                      widget.EmaximserQuestionScreen.pageViewController=PageController(initialPage: widget.backtoquesnum_bargraph-1);
                      widget.EmaximserQuestionScreen.widget.stopwatch=widget.stopwatch;
                      widget.EmaximserQuestionScreen.panelSlide();
                      widget.EmaximserQuestionScreen.widget.emaxStatisticsCheck=false;

                    });
                  }
                  if(widget.EmaximiserQuestionsReviewScreen != null && widget.EmaximserQuestionScreen == null){
                    widget.EmaximiserQuestionsReviewScreen.setState(() {
                      widget.EmaximiserQuestionsReviewScreen.emaximHeader_status_question=3;
                      widget.EmaximiserQuestionsReviewScreen.emaximSumbit_button_status=null;
                      widget.EmaximiserQuestionsReviewScreen.pageViewController=PageController(initialPage: widget.backtoquesnum_bargraph-1);
                      widget.EmaximiserQuestionsReviewScreen.widget.stopwatch=widget.stopwatch;
                      widget.EmaximiserQuestionsReviewScreen.panelSlide();
                      widget.EmaximiserQuestionsReviewScreen.widget.emaximStatisticsCheck=false;
                    });
                  }
                });
              },
            ),
          ),
          Align(
            alignment: Alignment.topLeft,

            child:Container(
              height:(22/720)*screenHeight ,
              //width: (81/360)*screenWidth,
              margin: EdgeInsets.only(top:(21/720)*screenHeight),
              child:Text(
                StatisticsScreenStrings.Text_QuestionStats,
                style: TextStyle(
                  color: Color.fromARGB(255, 0, 3, 44),
                  fontSize: (16/360)*screenWidth,
                  fontFamily: "IBMPlexSans",
                  fontWeight: FontWeight.w500,
                ),
                textAlign: TextAlign.left,
              ) ,
            ),),
          Align(
            alignment: Alignment.topLeft,
            child:Container(
              width: (107/360)*screenWidth,
              height: (20/720)*screenHeight,
              margin: EdgeInsets.only(top:(10/720)*screenHeight),
              decoration: BoxDecoration(
                // color:  (widget.statusForQuestion=="true")? PrimaryColors.kelly_green:(widget.statusForQuestion=="false")? PrimaryColors.dark_coral: Colors.yellow,
                border: Border.all(
                  color:  (widget.statusForQuestion=="true")? PrimaryColors.kelly_green:(widget.statusForQuestion=="false")? PrimaryColors.dark_coral: Colors.yellow,

                  width: 1,
                ),
                borderRadius: BorderRadius.all(Radius.circular(2)),
              ),
              child:  Center(child:Text(
                (widget.statusForQuestion=="true")? Preparequestions.correctStatus:(widget.statusForQuestion=="false")? Preparequestions.wrongStatusForReview: Preparequestions.skippedStatus,
                style: TextStyle(
                  color:(widget.statusForQuestion=="true")? PrimaryColors.kelly_green:(widget.statusForQuestion=="false")? PrimaryColors.dark_coral: Colors.yellow,
                  fontSize: (12/720)*screenHeight,
                  fontFamily: "IBMPlexSans",
                  fontWeight: FontWeight.w500,
                ),
                textAlign: TextAlign.center,
              ),),
            ),),
          DraggableScrollbar(
            controller: controller,
            heightScrollThumb: 78.0/720*screenHeight,
            weightScrollThumb: 3/360*screenWidth,
            colorScrollThumb: Color(0xffcecece),
            marginScrollThumb: EdgeInsets.only(right: 6/360*screenWidth,top:100/720*screenHeight),

            child:Container(
              width: 320/360*screenWidth,
              decoration: BoxDecoration(
                color: Color.fromARGB(255, 242, 246, 248),
                borderRadius: BorderRadius.all(Radius.circular(5)),
              ),
              margin: EdgeInsets.only(top: 32/720*screenHeight),
              child: Column(
                children: [
                  Container(
                    height: 43/720*screenHeight,
                    margin: EdgeInsets.only(left: 20/360*screenWidth, top: 20/720*screenHeight, right: 99/360*screenWidth),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: [
                        Align(
                          alignment: Alignment.topLeft,
                          child: Container(
                            width: 88/360*screenWidth,
                            height: 43/720*screenHeight,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  StatisticsScreenStrings.Text_Timetaken,
                                  style: TextStyle(
                                    color: Color.fromARGB(255, 153, 154, 171),
                                    fontSize: 12/360*screenWidth,
                                    fontFamily: "IBMPlexSans",
                                    fontWeight: FontWeight.w500,
                                  ),
                                  textAlign: TextAlign.left,
                                ),
                                Container(
                                  margin: EdgeInsets.only(top: 1/720*screenHeight),
                                  child: Text(
                                    widget.timeTakenForQuestion,
                                    style: TextStyle(
                                      color: Color.fromARGB(255, 0, 3, 44),
                                      fontSize: 14/720*screenHeight,
                                      fontFamily: "IBMPlexSans",
                                      fontWeight: FontWeight.w500,
                                    ),
                                    textAlign: TextAlign.left,
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                        Spacer(),
//                      Align(
//                        alignment: Alignment.topLeft,
//                        child: Container(
//                          width: 90/360*screenWidth,
//                          height: 44/720*screenHeight,
//                          child: Column(
//                            crossAxisAlignment: CrossAxisAlignment.start,
//                            children: [
//                              Text(
//                                StatisticsScreenStrings.Text_AvgTimetaken,
//                                style: TextStyle(
//                                  color: Color.fromARGB(255, 153, 154, 171),
//                                  fontSize: 12/360*screenWidth,
//                                  fontFamily: "IBMPlexSans",
//                                  fontWeight: FontWeight.w500,
//                                ),
//                                textAlign: TextAlign.left,
//                              ),
//                              Container(
//                                margin: EdgeInsets.only(top: 1/720*screenHeight),
//                                child: Text(
//                                  StatisticsScreenStrings.Text_ValueAvgTimetaken,
//                                  style: TextStyle(
//                                    color: Color.fromARGB(255, 0, 3, 44),
//                                    fontSize: 14/720*screenHeight,
//                                    fontFamily: "IBMPlexSans",
//                                    fontWeight: FontWeight.w500,
//                                  ),
//                                  textAlign: TextAlign.left,
//                                ),
//                              ),
//                            ],
//                          ),
//                        ),
//                      ),
                      ],
                    ),
                  ),
//                Container(
//                  height: 43/720*screenHeight,
//                  margin: EdgeInsets.only(left: 20/360*screenWidth, top: 30/720*screenHeight, right: 99/360*screenWidth),
//
//                  child: Row(
//                    crossAxisAlignment: CrossAxisAlignment.stretch,
//                    children: [
//                      Align(
//                        alignment: Alignment.topLeft,
//                        child: Container(
//                          width: 88/360*screenWidth,
//                          height: 43/720*screenHeight,
//                          child: Column(
//                            crossAxisAlignment: CrossAxisAlignment.start,
//                            children: [
//                              Text(
//                                CalculationScreenStrings.Text_Accuracy,
//                                style: TextStyle(
//                                  color: Color.fromARGB(255, 153, 154, 171),
//                                  fontSize: 12/360*screenWidth,
//                                  fontFamily: "IBMPlexSans",
//                                  fontWeight: FontWeight.w500,
//                                ),
//                                textAlign: TextAlign.left,
//                              ),
//                              Container(
//                                margin: EdgeInsets.only(top: 1/720*screenHeight),
//                                child: Text(
//                                  StatisticsScreenStrings.Text_ValueAccurcy,
//                                  style: TextStyle(
//                                    color: Color.fromARGB(255, 0, 3, 44),
//                                    fontSize: 14/720*screenHeight,
//                                    fontFamily: "IBMPlexSans",
//                                    fontWeight: FontWeight.w500,
//                                  ),
//                                  textAlign: TextAlign.left,
//                                ),
//                              ),
//                            ],
//                          ),
//                        ),
//                      ),
//                      Spacer(),
//                      Align(
//                        alignment: Alignment.topLeft,
//                        child: Container(
//                          width: 88/360*screenWidth,
//                          height: 43/720*screenHeight,
//                          child: Column(
//                            crossAxisAlignment: CrossAxisAlignment.start,
//                            children: [
//                              Text(
//                                StatisticsScreenStrings.Text_AttemptedBy,
//                                style: TextStyle(
//                                  color: Color.fromARGB(255, 153, 154, 171),
//                                  fontSize: 12/360*screenWidth,
//                                  fontFamily: "IBMPlexSans",
//                                  fontWeight: FontWeight.w500,
//                                ),
//                                textAlign: TextAlign.left,
//                              ),
//                              Container(
//                                margin: EdgeInsets.only(top: 1/720*screenHeight),
//                                child: Text(
//                                  StatisticsScreenStrings.Text_ValueAttemptedBy,
//                                  style: TextStyle(
//                                    color: Color.fromARGB(255, 0, 3, 44),
//                                    fontSize: 14/720*screenHeight,
//                                    fontFamily: "IBMPlexSans",
//                                    fontWeight: FontWeight.w500,
//                                  ),
//                                  textAlign: TextAlign.left,
//                                ),
//                              ),
//                            ],
//                          ),
//                        ),
//                      ),
//                    ],
//                  ),
//                ),
//                Container(
//                  height: 43/720*screenHeight,
//                  margin: EdgeInsets.only(left: 20/360*screenWidth, top: 30/720*screenHeight, right: 99/360*screenWidth,bottom: 14/720*screenHeight),
//                  child: Row(
//                    crossAxisAlignment: CrossAxisAlignment.stretch,
//                    children: [
//                      Align(
//                        alignment: Alignment.topLeft,
//                        child: Container(
//                          width: 88/360*screenWidth,
//                          height: 43/720*screenHeight,
//                          child: Column(
//                            crossAxisAlignment: CrossAxisAlignment.start,
//                            children: [
//                              Text(
//                                StatisticsScreenStrings.Text_PValue,
//                                style: TextStyle(
//                                  color: Color.fromARGB(255, 153, 154, 171),
//                                  fontSize: 12/360*screenWidth,
//                                  fontFamily: "IBMPlexSans",
//                                  fontWeight: FontWeight.w500,
//                                ),
//                                textAlign: TextAlign.left,
//                              ),
//                              Container(
//                                margin: EdgeInsets.only(top: 1/720*screenHeight),
//                                child: Text(
//                                  StatisticsScreenStrings.Text_ValuePValue,
//                                  style: TextStyle(
//                                    color: Color.fromARGB(255, 0, 3, 44),
//                                    fontSize: 14/720*screenHeight,
//                                    fontFamily: "IBMPlexSans",
//                                    fontWeight: FontWeight.w500,
//                                  ),
//                                  textAlign: TextAlign.left,
//                                ),
//                              ),
//                            ],
//                          ),
//                        ),
//                      ),
//                      Spacer(),
//                      Align(
//                        alignment: Alignment.topLeft,
//                        child: Container(
//                          width: 88/360*screenWidth,
//                          height: 43/720*screenHeight,
//                          child: Column(
//                            crossAxisAlignment: CrossAxisAlignment.start,
//                            children: [
//                              Text(
//                                StatisticsScreenStrings.Text_ABCApproach,
//                                style: TextStyle(
//                                  color: Color.fromARGB(255, 153, 154, 171),
//                                  fontSize: 12/360*screenWidth,
//                                  fontFamily: "IBMPlexSans",
//                                  fontWeight: FontWeight.w500,
//                                ),
//                                textAlign: TextAlign.left,
//                              ),
//                              Container(
//                                margin: EdgeInsets.only(top: 1/720*screenHeight),
//                                child: Text(
//                                  StatisticsScreenStrings.Text_ValueABCApproach,
//                                  style: TextStyle(
//                                    color: Color.fromARGB(255, 0, 3, 44),
//                                    fontSize: 14/720*screenHeight,
//                                    fontFamily: "IBMPlexSans",
//                                    fontWeight: FontWeight.w500,
//                                  ),
//                                  textAlign: TextAlign.left,
//                                ),
//                              ),
//                            ],
//                          ),
//                        ),
//                      ),
//                    ],
//                  ),
//                ),
                ],
              ),
            ),),

        ],

      ),


    );
  }
}
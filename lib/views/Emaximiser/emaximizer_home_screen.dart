import 'dart:io' as Platform;

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:html/parser.dart';
import 'package:imsindia/api/bloc/services.dart';
import 'package:imsindia/components/bottom_navigation.dart';
import 'package:imsindia/components/custom_expansion_panel.dart';
import 'package:imsindia/components/navigation_bar.dart';
import 'package:imsindia/utils/Url.dart';
import 'package:imsindia/utils/colors.dart';
import 'package:imsindia/utils/global.dart' as global;
import 'package:imsindia/utils/svg_images/error_screen_svg_images.dart';
import 'package:imsindia/views/Arun/ims_utility.dart';
import 'package:imsindia/views/Emaximiser/emaximiser_pdfscreen.dart';
import 'package:imsindia/views/Emaximiser/emaximizer_introduction.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:imsindia/views/Emaximiser/emaximizer_videoplayer_ios.dart';
import '../../routers/routes.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'emaximizer_videoplayer.dart';
import 'package:connectivity/connectivity.dart';
import 'package:imsindia/resources/strings/emaximiser.dart';

String userid;
String moduleId;
String courseName;
String moduleDesc;
String introVideoId;
var screenWidthTotal;
var screenHeightTotal;

var _safeAreaHorizontal;

var _safeAreaVertical;

var screenHeight;

var screenWidth;
var i = 1;

List<Color> colors = [
  Color(0xff886dd7),
  Color(0xffff5757),
  Color(0xffff982a),
  Color(0xff00abfb),
  Color(0xff5647eb),
];




class EmaximiserHomeScreen extends StatefulWidget {
  @override
  EmaximiserHomeScreenState createState() => EmaximiserHomeScreenState();
}

class EmaximiserHomeScreenState extends State<EmaximiserHomeScreen> {

  Connectivity connectivity = Connectivity();
  final GlobalKey<ScaffoldState> _emaxhomescaffoldstate = new GlobalKey<ScaffoldState>();
  var authorizationInvalidMsg;
  var noDes = "NO DATA";
  var getIsProductAccessCheck = [];
  var getLearFirstHiraLevelTitles = [];
  var getLengthOfCourse = [];
  var getSecondLevelTitles = [];
  var getSecondLevelOrderNo = [];
  var getSecondLevelId = [];
  var getSecondLevelDescription = [];
  var finalListForSecondHirarTitles =[];
  var finalListForSecondOrderNo = [];
  var finalListForSecondHirarId =[];
  var finalListForgetTotalTest =[];
  var finalListForgetCompletedTest =[];
  var finalListForgetWorkshopType = [];
  var finalListForSecondHirarDescription =[];
  var getListoflist = [];
  var getlistofOrderNo = [];
  var getListofId = [];
  var getTotalTest = [];
  var getCompletedTest = [];
  var getWorkshopType = [];
  var split_list = [];
  var videoId = [];
  var first = '';
  var groupColorIndex = 0;
  var authToken;
  @override
  void initState() {
    print("INIT STATE");

    // TODO: implement initState
    super.initState();

    checkInternet();

    // connectivity.onConnectivityChanged.listen((result) {
    //   if (result.toString() == 'ConnectivityResult.none') {
    //     print('connection lost');
    //     noInternetMessage('Internet Connection Lost');
    //   } else {
    //     print(_emaxhomescaffoldstate.currentState) ;
    //     print("_emaxhomescaffoldstate.currentState");
    //     _emaxhomescaffoldstate.currentState.removeCurrentSnackBar();
    //     print('connection exist in notifier');
    //     getLearFirstHiraLevelTitles = [];
    //     questionsAttempt = [];
    //     totalQuestions = [];
    //      getCourseId();
    //   }
    // });
    //getCourseId();

  }

  Future<Null> checkInternet() async {
    var connectivityResult = await (Connectivity().checkConnectivity());
    if (connectivityResult.toString() != 'ConnectivityResult.none') {
      setState(() {
        getLearFirstHiraLevelTitles = [];
        questionsAttempt = [];
        totalQuestions = [];
        getCourseId();
      });
    } else {
      noInternetMessage('Internet Connection Lost');
    }

    return null;
  }
  void noInternetMessage(String value) {
    _emaxhomescaffoldstate.currentState.showSnackBar(new SnackBar(
      content: new Text(value),
      action: SnackBarAction(
        textColor: Colors.blueAccent,
        label: "Dismiss",
        onPressed: () {
          // Some code to undo the change.
        },
      ),
      backgroundColor: Colors.blueGrey,
      duration: Duration(days: 365),
    ));
  }
  void showErrorMessage(String value) {
    _emaxhomescaffoldstate.currentState.showSnackBar(new SnackBar(
      content: new Text(value),
      duration: new Duration(seconds: 3),
    ));
  }


  getCourseId() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    userid = prefs.getString("userId");
    moduleId = prefs.getString('moduleId');
    courseName = prefs.getString('courseName');
    moduleDesc = prefs.getString('moduleDesc');
    global.getToken.then((t) {
      print("Authentication token");
      print(t);
      authToken=t;
      APIcall();
    });
  }
  void didChangeDependencies(){
    super.didChangeDependencies();
  }

//  void listupdate1() async {
//    split_list = [];
//    getListoflist = [];
//    for(int i = 0; i< finalListForSecondHirarTitles.length;i++){
//      print("finalselection"+selection.toString());
//      for(int j = 0; j < finalListForSecondHirarTitles[i].length;j++){
//        //     for(int j = 0; j < finalListForSecondHirarTitles.length;j++){
//        print("value of i"+i.toString());
//        //   //print(getSecondLevelTitles[i]);
//        //   //print(getSecondLevelTitles[i].toString());
//        //   //print(getSecondLevelTitles[i].toString().split('.')[1]);
//        if(selection == j){
//          split_list.add(finalListForSecondHirarTitles[i][j].toString().split('.')[1]);
//          getListoflist.add(finalListForSecondHirarDescription[i][j].toString());
//          print("selected");
//        }
//        else{
//          split_list.add(finalListForSecondHirarTitles[i][j].toString().split('.')[1]);
//          getListoflist.add(finalListForSecondHirarDescription[i][j].toString());
//          print("unselected");
//        }
//      }
//    }
//  }

  void listupdate() async{
    print("inside list update");
    print(selection);
    print(split_list);
    print(split_list.length);
    //On selection of ALL
    if (selection < 0){
      split_list = [];
      getListofId = [];
      getlistofOrderNo = [];
      getListoflist = [];
      getTotalTest = [];
      getCompletedTest = [];
      getWorkshopType = [];
      var split_listt = [];
      var getListoflistt = [];
      var getlistofOrderNot = [];
      var getListofIdt = [];
      var getTotalTestt = [];
      var getCompletedTestt = [];
      var getWorkshopTypet = [];
      for(int i = 0; i< finalListForSecondHirarTitles.length;i++){

        for(int j = 0; j < finalListForSecondHirarTitles[i].length;j++){
          //     for(int j = 0; j < finalListForSecondHirarTitles.length;j++){
          print("value of i"+i.toString());
          //   //print(getSecondLevelTitles[i]);
          //   //print(getSecondLevelTitles[i].toString());
          //   //print(getSecondLevelTitles[i].toString().split('.')[1]);
          if(selection == j){
            if(courseName == "PGDBA"){
              split_listt.add(finalListForSecondHirarTitles[i][i].toString());
            }else{
              split_listt.add(finalListForSecondHirarTitles[i][i].toString().split('.')[1]);
            }
            //split_listt.add(finalListForSecondHirarTitles[i][j].toString().split('.')[1]);
            getlistofOrderNot.add(finalListForSecondOrderNo[i][j]);
            getListoflistt.add(finalListForSecondHirarDescription[i][j].toString());
            getListofIdt.add(finalListForSecondHirarId[i][j].toString());
            getTotalTestt.add(finalListForgetTotalTest[i][j]);
            getCompletedTestt.add(finalListForgetCompletedTest[i][j]);
            getWorkshopTypet.add(finalListForgetWorkshopType[i][j]);
            print("selected");
          }
          else{
            if(courseName == "PGDBA"){
              split_listt.add(finalListForSecondHirarTitles[i][i].toString());
            }else{
              split_listt.add(finalListForSecondHirarTitles[i][i].toString().split('.')[1]);
            }
            //split_listt.add(finalListForSecondHirarTitles[i][j].toString().split('.')[1]);
            getlistofOrderNot.add(finalListForSecondOrderNo[i][j]);
            getListoflistt.add(finalListForSecondHirarDescription[i][j].toString());
            getListofIdt.add(finalListForSecondHirarId[i][j].toString());
            getTotalTestt.add(finalListForgetTotalTest[i][j]);
            getCompletedTestt.add(finalListForgetCompletedTest[i][j]);
            getWorkshopTypet.add(finalListForgetWorkshopType[i][j]);
            print("unselected");
          }
        }
      }

      //var sortedList = [];
          //sortedList = getlistofOrderNo..sort((a, b) => a.compareTo(b));
         // sortedList = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15];
          for (int x = 1 ; x < getlistofOrderNot.length +1; x++){
            print("Entered the for loop");
            for (int y = 0; y < getlistofOrderNot.length; y++ ){
              if ( x == getlistofOrderNot [y]){
                split_list.add(
                    split_listt[y].toString());
                getListoflist.add(
                    getListoflistt[y].toString());
                getlistofOrderNo.add(getlistofOrderNot[y]);
                getListofId.add(getListofIdt[y].toString());
                getTotalTest.add(getTotalTestt[y]);
                getCompletedTest.add(getCompletedTestt[y]);
                getWorkshopType.add(getWorkshopTypet[y]);
              }
            }
          }
    }
    //On selection of any value in the dropdown
    else {
      split_list = [];
      getListofId = [];
      getListoflist = [];
      getlistofOrderNo = [];
      getTotalTest = [];
      getCompletedTest = [];
      getWorkshopType = [];
      for (int i = 0; i < finalListForSecondHirarTitles[selection].length; i++) {
        if(courseName == "PGDBA"){
          split_list.add(finalListForSecondHirarTitles[selection][i].toString());
        }else{
          split_list.add(finalListForSecondHirarTitles[selection][i].toString().split('.')[1]);
        }
        
        getlistofOrderNo.add(finalListForSecondOrderNo[selection][i]);
        getListoflist.add(finalListForSecondHirarDescription[selection][i].toString());
        getListofId.add(finalListForSecondHirarId[selection][i].toString());
        getTotalTest.add(finalListForgetTotalTest[selection][i]);
        getCompletedTest.add(finalListForgetCompletedTest[selection][i]);
        getWorkshopType.add(finalListForgetWorkshopType[selection][i]);
        groupColorIndex = selection+1;
        print ("selected : "
            +selection.toString());
        print(finalListForSecondHirarTitles[selection]);
        print(getListofId);
        print(getTotalTest);
      }
    }
  }
  void setIdForSubjectLevelId(value) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString("idForSubjectLevelIdEmax", value);
  }

  void APIcall() async {
    String _parseHtmlString(String htmlString) {
      var document = parse(htmlString);

      String parsedString = parse(document.body.text).documentElement.text;

      return parsedString;
    }
    print("Authentication token");
    print(authToken);

      ApiService()
          .getAPI(URL.EMAX_URL+moduleId, authToken)
          .then((returnValue) {
        if (this.mounted){
      setState(() {
        if (returnValue[0].toString().toLowerCase() ==
            'Fetched Successfully'.toLowerCase()) {
          print("Data Fetched....................." + moduleId.toString() +
              authToken.toString());
          getLearFirstHiraLevelTitles.add("All");


          for (var index = 0; index < returnValue[1]['data'].length; index++) {

            // if (returnValue[1]['data'][index]['sublearnModule'].length != 0) {
            //  // print("new print.....................$returnValue............................");

            //     getLearFirstHiraLevelTitles.add(
            //         returnValue[1]['data'][index]['component']['title']);

            // }
            getIsProductAccessCheck.add(returnValue[1]['data'][index]['isProductAccess']);
            getLengthOfCourse.add(
                returnValue[1]['data'][index]['sublearnModule'].length);
            if (returnValue[1]['data'][index]['sublearnModule'].length == 0){
               videoId.add(returnValue[1]['data'][index]['id']);
            }
           
            first = getLearFirstHiraLevelTitles[0];
            if (returnValue[1]['data'][index]['sublearnModule'].length != 0) {
              for (var subindex = 0; subindex <
                  returnValue[1]['data'][index]['sublearnModule']
                      .length; subindex++) {
                if( returnValue[1]['data'][index]['sublearnModule'][subindex]['isActive']==1){
                  if (returnValue[1]['data'][index]['sublearnModule'][subindex]
                  ['component']['description'] != null) {
                    String des = _parseHtmlString(returnValue[1]['data'][index]
                    ['sublearnModule'][subindex]['component']['description']);
                    print(des);
                    // if(returnValue[1]['data'][index]['isActive']==1 ){
                    getSecondLevelDescription.add((returnValue[1]
                    ['data'][index]['sublearnModule'][subindex]
                    ['component']['description']));
                    //}

                  } else {
                    getSecondLevelDescription.add("");
                  }
                  getSecondLevelTitles.add(
                      returnValue[1]['data'][index]['sublearnModule'][subindex]['component']['title']);
                  getSecondLevelOrderNo.add(
                      returnValue[1]['data'][index]['sublearnModule'][subindex]['orderNo']);
                  getSecondLevelId.add(
                      returnValue[1]['data'][index]['sublearnModule'][subindex]['testId']);
                  getTotalTest.add(
                      returnValue[1]['data'][index]['sublearnModule'][subindex]['totalTests']);
                  getCompletedTest.add(
                      returnValue[1]['data'][index]['sublearnModule'][subindex]['testCompleted']);
                  getWorkshopType.add(returnValue[1]['data'][index]['component']['title']);
                }

                //            print(_parseHtmlString(returnValue[1]['data'][index]['sublearnModule'][subindex]['component']['description']));
                //              String des=_parseHtmlString('<p><b>In this workshop, the students will:</b></p><p>1. Build on their Conceptual Clarity and Application skills for the following topics: Comparisons on the Number Line, Divisibility Tests, Cyclicity Rule, Algebraic Formulae, Binomial theorem, Sequence and Series.</p><p>2.Assess their preparedness for the CAT on various parameters.</p>');
                //              getSecondLevelDescription.add((returnValue[1]['data'][index]['sublearnModule'][subindex]['component']['description']));

                //              getSecondLevelDescription.add(
                //                  returnValue[1]['data'][index]['sublearnModule'][subindex]['component']['description']);
                //if(returnValue[1]['data'][index]['isActive'] == 1){

                //}

                // index++;
              }
            }
            if (returnValue[1]['data'][index]['sublearnModule'].length != 0) {
              finalListForSecondHirarDescription.add(
                  getSecondLevelDescription);
              finalListForSecondHirarTitles.add(getSecondLevelTitles);
              finalListForSecondOrderNo.add(getSecondLevelOrderNo);
              finalListForSecondHirarId.add(getSecondLevelId);
              finalListForgetTotalTest.add(getTotalTest);
              finalListForgetCompletedTest.add(getCompletedTest);
              finalListForgetWorkshopType.add(getWorkshopType);
            }
            getSecondLevelTitles = [];
            getSecondLevelOrderNo = [];
            getSecondLevelId = [];
            getSecondLevelDescription = [];
            getTotalTest = [];
            getCompletedTest = [];
            getWorkshopType = [];
          }
          print(finalListForSecondHirarTitles);
          print(
              "finalListForSecondHirarTitlesfinalListForSecondHirarTitlesfinalListForSecondHirarTitles");
          var split_listt = [];
          var getListoflistt = [];
          var getlistofOrderNot = [];
          var getListofIdt = [];
          var getTotalTestt = [];
          var getCompletedTestt = [];
          var getWorkshopTypet = [];
          for (int i = 0; i < finalListForSecondHirarTitles.length; i++) {
            for (int j = 0; j < finalListForSecondHirarTitles[i].length; j++) {
              //     for(int j = 0; j < finalListForSecondHirarTitles.length;j++){
              print("value of i" + i.toString());
              //   //print(getSecondLevelTitles[i]);
              //   //print(getSecondLevelTitles[i].toString());
              //   //print(getSecondLevelTitles[i].toString().split('.')[1]);
              if (selection == j) {

                if(courseName == "PGDBA"){
                  split_listt.add(
                    finalListForSecondHirarTitles[i][j].toString());
                }else{
                split_listt.add(
                    finalListForSecondHirarTitles[i][j].toString().split(
                        '.')[1]);
                }
                getlistofOrderNot.add(finalListForSecondOrderNo[i][j]);
                getListoflistt.add(
                    finalListForSecondHirarDescription[i][j].toString());
                getListofIdt.add(finalListForSecondHirarId[i][j].toString());
                getTotalTestt.add(finalListForgetTotalTest[i][j]);
                getCompletedTestt.add(finalListForgetCompletedTest[i][j]);
                getWorkshopTypet.add(finalListForgetWorkshopType[i][j]);
                print("selected");
              }
              else {
                if(courseName == "PGDBA"){
                  split_listt.add(
                    finalListForSecondHirarTitles[i][j].toString());
                }else{
                split_listt.add(
                    finalListForSecondHirarTitles[i][j].toString().split(
                        '.')[1]);
                }
                getListoflistt.add(
                    finalListForSecondHirarDescription[i][j].toString());
                getlistofOrderNot.add(finalListForSecondOrderNo[i][j]);

                getListofIdt.add(finalListForSecondHirarId[i][j].toString());
                getTotalTestt.add(finalListForgetTotalTest[i][j]);
                getCompletedTestt.add(finalListForgetCompletedTest[i][j]);
                getWorkshopTypet.add(finalListForgetWorkshopType[i][j]);
                print("unselected");
              }
            }
          }
          
          //var sortedList = [];
          //sortedList = getlistofOrderNo..sort((a, b) => a.compareTo(b));
          //sortedList = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15];
          for (int x = 1 ; x < getlistofOrderNot.length + 1; x++){
            print("Entered the for loop");
            for (int y = 0; y < getlistofOrderNot.length; y++ ){
              if (x == getlistofOrderNot [y]){
                split_list.add(
                    split_listt[y].toString());
                getListoflist.add(
                    getListoflistt[y].toString());
                getlistofOrderNo.add(getlistofOrderNot[y]);
                getListofId.add(getListofIdt[y].toString());
                getTotalTest.add(getTotalTestt[y]);
                getCompletedTest.add(getCompletedTestt[y]);
                getWorkshopType.add(getWorkshopTypet[y]);
              }
            }
          }

          for(int k = 0; k< getWorkshopType.toSet().toList().length;k++){
            getLearFirstHiraLevelTitles.add(getWorkshopType.toSet().toList()[k]);
          }

          print("Printing new lists");
          print(split_listt);
          print(getlistofOrderNo);
          print(getlistofOrderNot);
          print(getListofIdt);
          print(getCompletedTestt);
          print(getWorkshopType);
          print(getWorkshopTypet);

          //  for(int i = 0; i< finalListForSecondHirarDescription.length;i++){
          //   for(int j = 0; j < finalListForSecondHirarDescription[i].length;j++){
          //    print("value of i"+i.toString());
          //    getListoflist.add(finalListForSecondHirarDescription[i][j].toString());
          //   }
          // }
          print("---" + getLengthOfCourse.toString());
          print(getLearFirstHiraLevelTitles);
          print(getSecondLevelTitles);
          print(getSecondLevelId);
          print(getSecondLevelTitles.length);
          print(finalListForSecondHirarDescription);
          print(finalListForSecondHirarTitles);
          print(finalListForSecondHirarId);
          print(finalListForSecondHirarDescription.length);
          print(getSecondLevelDescription);
          print(finalListForSecondHirarTitles.length);
          print("000000" + split_list.toString());
          print("getTotalTest");
          print(getTotalTest);
          print(getCompletedTest);
          print(getlistofOrderNo);

          print(split_list.length);
        } else {
          authorizationInvalidMsg = returnValue[0];
          print("return value");
          print(returnValue[0]);
          //      authorizationInvalidMsg = returnValue[0];
        }
      });
    }
      });
    }

  var value = 'All';

  accessCheck(List list){
    int i;
    int j = 0;
    print(list);
    for (i=0;i < list.length ;i++){
      if(list[i] == false){
        j = j+1;
      }
    }
    //print("Check for isProductAccess");
    //print(j);
    if(j == list.length){
      return true;
    }else{
      return false;
    }
  }


  void onPressed(BuildContext context,split,desc) => Navigator.push(
      context, MaterialPageRoute(builder: (context) =>
      EmaxIntroduction(pos: split,desc: desc,screen: true,)
  )).then((valuee) {
  if (valuee == true) {
    setState(() {
      print("coming inside it");
      getLearFirstHiraLevelTitles = [];
      getLengthOfCourse = [];
      getSecondLevelTitles = [];
      getSecondLevelId = [];
      getSecondLevelDescription = [];
      finalListForSecondHirarTitles = [];
      finalListForSecondHirarId = [];
      finalListForgetTotalTest = [];
      finalListForgetCompletedTest = [];
      finalListForSecondHirarDescription = [];
      getListoflist = [];
      getlistofOrderNo = [];
      getListofId = [];
      getTotalTest = [];
      getCompletedTest = [];
      split_list = [];
      videoId = [];
      value = 'All';
      selection = -1;
      getCourseId();
    }
    );
  }
  });

  int _activeMeterIndex = -1;
  
  var selection = -1;
  var listttttt = [];

  colorSelect(String dropDownValue,bool withOpacity){
    if(value == "All" || value == ""){
      listttttt = getWorkshopType.toSet().toList();
      for(int i = 0 ; i < getWorkshopType.toSet().toList().length;i++){
        //print(i>4?i%5:i.toString()+"length"+getWorkshopType.toSet().toList().toString());
      if(dropDownValue.toString() == getWorkshopType.toSet().toList()[i].toString()){
          if(withOpacity == true){
            //print(i>4?i%5:i.toString()+"length"+getWorkshopType.toSet().toList().length.toString());
            return colors[i>4?i%5:i].withOpacity(0.1);
          }else{
            return colors[i>4?i%5:i];
          }
      }
     }
    }
    else{
      for(int j = 0 ; j < listttttt.length;j++){
      if(dropDownValue.toString() == listttttt[j].toString()){
        //print(colors[j>4?j%5:j].toString());
        if(withOpacity == true){
          return colors[j>4?j%5:j].withOpacity(0.1);
        }else{
          return colors[j>4?j%5:j];
        }
        
      }
    }
  }
    
    
  }



  @override
  Widget build(BuildContext context) {
    var _mediaQueryData = MediaQuery.of(context);
    screenWidthTotal = _mediaQueryData.size.width;
    screenHeightTotal = _mediaQueryData.size.height;
    _safeAreaHorizontal =
        _mediaQueryData.padding.left + _mediaQueryData.padding.right;
    _safeAreaVertical =
        _mediaQueryData.padding.top + _mediaQueryData.padding.bottom;

    screenHeight = screenHeightTotal - _safeAreaVertical;
    screenWidth = screenWidthTotal - _safeAreaHorizontal;

    return

      Scaffold(
        key: _emaxhomescaffoldstate,
        appBar: AppBar(
          centerTitle: false,
          titleSpacing: 0.0,
          backgroundColor: Colors.white,
          elevation: 0.0,
          brightness: Brightness.light,
          iconTheme: IconThemeData(
            color: Colors.black, //change your color here
          ),
          /// Title of the page ///
          title: Container(
            width: (302 / 360) * screenWidth,
            child: Text(
              courseName == "PGDBA" ? "PGDBA Workshops" : EmaximiserHomeScreenString.Emaximiser,
              style: TextStyle(
                color: Color.fromARGB(255, 0, 0, 0),
                fontSize: (16 / 720) * screenHeight,
                fontFamily: "IBMPlexSans",
                fontWeight: FontWeight.w500,
              ),
              textAlign: TextAlign.left,
            ),
          ),
        ),
        drawer: NavigationDrawer(),
//        bottomNavigationBar: BottomNavigation(),
        body: SingleChildScrollView(
                  child: Container(
            color: Colors.white,
            child: Column(
              children: <Widget>[
                authorizationInvalidMsg != null 
                    /// Auth message if something went wrong ... ///
                    ? new Container(
                    margin: EdgeInsets.only(
                        top: (200.0 / 720) * screenHeight),
                    child: Center(
                        child: Text(
                          '${authorizationInvalidMsg}' )))
                    :   (split_list.length == 0  && videoId.length == 0 && selection<0)?
                /// Circular indicator if data is not present ///
                new Container(
                  // margin: EdgeInsets.only(top: 50/ 720 * screenHeight),
                  height: screenHeight,
                  child:
                 Center(child: CircularProgressIndicator()),
                )
            :
                /// Area Level titles or Titles in Dropdown ...... ///
                accessCheck(getIsProductAccessCheck) ? 
                Center(
                  child: Container(
                margin: EdgeInsets.only(
                    left: 18 / 360 * screenWidth,
                    top: 12.5 / 720 * screenHeight),
                child: SvgPicture.asset(
                  ErrorScreenAssets.errorScreenImg,
                  fit: BoxFit.fill,
                ),
              ),
                ):
                Column(
                  children: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        moduleDesc == '' ? Container() : Container(
                                // height: 102 / 720 * screenHeight,
                                // width: 320 / 360 * screenWidth,
                                padding: EdgeInsets.symmetric(horizontal: 20 / 360 * screenWidth),
                                margin: EdgeInsets.only(top: 10 / 797 * screenHeight),
                                child:  Html(
                                  blockSpacing: 0.0,
                                  useRichText: false,
                                  // backgroundColor:Colors.red ,
                                  data:moduleDesc,
                                  defaultTextStyle:   TextStyle(
                                    color: Color.fromARGB(255, 0, 3, 44),
                                    fontSize: 16.0 / 720 * screenHeight,
                                    fontFamily: "IBMPlexSans",
                                    fontWeight: FontWeight.w500,


                                  ),

//backgroundColor: Colors.transparent,
                                ),
                              ),
                              /// what should know ............
                              GestureDetector(
                                onTap: (){
                                   if(Platform.Platform.isAndroid)
                                {
                                AppRoutes.push(context,EmaxWebViewAndroid(videoId[0],userid,null) );
                                }
                                if(Platform.Platform.isIOS)
                                {
                                AppRoutes.push(context, EmaxWebViewiOS(videoId[0],userid,null) );
                                }
                                  
                                },
                                  child: Padding(
                                  padding:EdgeInsets.only(top: 10 / 797 * screenHeight,right: 20 / 360 * screenWidth,left: 20 / 360 * screenWidth),
                                  child: Text(
                                    "Watch introductionary video >",
                                    style: TextStyle(
                                      color: Color.fromARGB(255, 71, 89, 147),
                                      fontSize: 16.0 / 720 * screenHeight,
                                      fontFamily: "IBMPlexSans",
                                      fontWeight: FontWeight.w500,
                                      
                                    ),
                                    textAlign: TextAlign.right,
                                  ),
                                ),
                              ),
                      ],
                    ),
                    getLearFirstHiraLevelTitles.length == 2 ? Container() :
                    Container(
                      decoration: BoxDecoration(

                        boxShadow: [BoxShadow(
                          offset: Offset(0, 5),
                          blurRadius: 10,
                          color: (_activeMeterIndex == i && split_list.length != 0)?Color(0xffeaeaea): Colors.transparent,
                          //offset: Offset.lerp(Offset(10.0,-10.0), Offset(10.0,10.0), 1),
                          // offset: Offset(0,10.0),
                          //color: Colors.orange,
                        ),],
                        borderRadius: new BorderRadius.only(topLeft:new Radius.circular(5) ,topRight:new Radius.circular(5),bottomLeft:new Radius.circular(5) ,bottomRight:new Radius.circular(5) ),
                        border: Border.all(color: Colors.transparent),
                      ),
                      margin: EdgeInsets.only(
                          top: (15.0 / 797) * screenHeight,
                          left: (20.0 / 360) * screenWidth,
                          right: (20.0 / 360) * screenWidth),
                      child: CustomExpansionPanelList(
                        expansionHeaderHeight: (45 / 797) * screenHeight,
                        iconColor: (_activeMeterIndex == i)
                            ? Colors.white
                            : NeutralColors.dark_navy_blue,
                        backgroundColor1: (_activeMeterIndex == i)
                            ? NeutralColors.purpleish_blue
                            : SemanticColors.light_purpely.withOpacity(0.1),
                        backgroundColor2: (_activeMeterIndex == i)
                            ? NeutralColors.purpleish_blue
                            : SemanticColors.dark_purpely.withOpacity(0.1),
                        expansionCallback: (int index, bool status) {
                          setState(() {
                            _activeMeterIndex = _activeMeterIndex == i ? null : i;
                            //i == null ? i = 1 : i = null;
                          });
                          print("click");
                        },
                        children: [
                          new ExpansionPanel(
                            canTapOnHeader: true,
                            isExpanded: _activeMeterIndex == i,
                            headerBuilder: (BuildContext context, bool isExpanded) =>
                            new Container(
                              alignment: Alignment.centerLeft,
                              child: new Padding(
                                padding:
                                EdgeInsets.only(left: (15 / 320) * screenWidth),
                                child: new Text(value.toString(),
                                    style: new TextStyle(
                                        fontSize: (13.6 / 797) * screenHeight,
                                        fontFamily: "IBMPlexSans",
                                        fontWeight: FontWeight.w500,
                                        color: (_activeMeterIndex == i)
                                            ? Colors.white
                                            : const Color(0xFF2d3438))),
                              ),
                            ),
                            body: new Container(
                                color: Colors.white,
                                height: (205 / 678) * screenHeight,
                                child: new ListView.builder(
                                  //physics: NeverScrollableScrollPhysics(),
                                  shrinkWrap: true,

                                  itemExtent: (60.0 / 797) * screenHeight,
                                  itemBuilder: (BuildContext context, int index) {
                                    return new Container(

                                      //width: (320/360)*screenWidth,
                                      decoration: new BoxDecoration(
                                          border: new Border.all(color: Colors.white),
                                          color: Colors.white),
                                      child: new ListTile(
                                        //dense: true,
                                        contentPadding: EdgeInsets.only(
                                            bottom: 0,
                                            left: (15 / 320) * screenWidth,
                                            top: 0),
                                        onTap: () {
                                          print("Gesture");
                                          setState(() {
                                            i == null ? i = 1 : i = null;
                                            value = getLearFirstHiraLevelTitles[index];
                                            selection = index-1;
                                            print("i"+i.toString());
                                            print("index"+index.toString());
                                            print("selection"+selection.toString());
                                          });
                                          listupdate();
                                        },
                                        title: new Text(
                                          getLearFirstHiraLevelTitles[index],

                                          textAlign: TextAlign.left,
                                          style: new TextStyle(
                                              fontSize: (15 / 797) * screenHeight,
                                              fontFamily: "IBMPlexSans",
                                              fontWeight: FontWeight.w500,
                                              color: const Color(0xFF999aab)),
                                        ),

                                      ),
                                    );
                                  },
                                  itemCount: getLearFirstHiraLevelTitles.length,
                                )),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
                //         (selection>0)?Text("Empty list"):
                /// subject related Data according to dropdown ....///
                (split_list.length == 0 && videoId.length != 0 && selection<0) || (split_list.length == 0 && videoId.length != 0 && selection>0) || accessCheck(getIsProductAccessCheck)?
                new Container(
                    padding:EdgeInsets.only(top: 10 / 797 * screenHeight,left: 18 / 360 * screenWidth,right: 18 / 360 * screenWidth,),
                    child: Center(
                      child:Text( accessCheck(getIsProductAccessCheck)? "Access to this module will be enabled as per the schedule for your enrolled program. \n OR \n You do not have access to this module as part of your enrolled program.": "No tests available",
                    style: TextStyle(
                          color: NeutralColors.charcoal_grey,
                          fontSize: 14 / 360 * screenWidth,
                          fontFamily: "IBMPlexSans",
                          fontWeight: FontWeight.w500,
                        ),
                        textAlign: TextAlign.center,
                    )
                  )
                    
                ):
                /// Body .......... //
                new Container(
                  child: new ListView.builder(
                    shrinkWrap: true,
                    physics: NeverScrollableScrollPhysics(),
                    itemCount:split_list.length,
                    itemBuilder: (context, int index) {
                      // final item = courses[index];

                      //  final color = colors[index];
                      return GestureDetector(
                        onTap: () {
                          print("Pressed");
                          print(getListofId[index]);
                          setIdForSubjectLevelId( getListofId[index]);
                          this.onPressed(context,split_list[index],getListoflist[index]);
                          //   print(color);
                        },
                        /// Entire subject level data ....... ///
                        child: Container(

                          //color:Colors.red,
                          //  height: 144.0 / 797 * screenHeight,
                            width: 320 / 360 * screenWidth,
                            margin: EdgeInsets.only(
                                left: 20.0 / 360 * screenWidth,
                                top: 7.5 / 797 * screenHeight,
                                right: 20.0 / 360 * screenWidth,
                                bottom: 7.5 / 797 * screenHeight),
                            decoration: BoxDecoration(
                              color: Color.fromARGB(255, 255, 255, 255),
                              border: Border.all(
                                color: NeutralColors.ice_blue,
                                width: 1,
                              ),
                              borderRadius: BorderRadius.all(Radius.circular(5)),
                            ),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.end,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                /// Subject Level Titles ///
                                Container(
                                  height: 23 / 797 * screenHeight,
                                  width: 290 / 360 * screenWidth,
                                  margin: EdgeInsets.only(
                                      left: 15 / 360 * screenWidth,
                                      right: 15 / 360 * screenWidth,
                                      bottom: 10 / 797 * screenHeight,
                                      top: 19 / 797 * screenHeight),
                                  child: Text(

                                    split_list[index].toString().trim(),
                                    style: TextStyle(
                                      color: NeutralColors.charcoal_grey,
                                      fontSize: 14 / 360 * screenWidth,
                                      fontFamily: "IBMPlexSans",
                                      fontWeight: FontWeight.w700,
                                    ),
                                    textAlign: TextAlign.left,
                                  ),
                                ),
                                /// Description for subject level Titles ///
                                Container(
                                  //   height: 75 / 797 * screenHeight,
                                  width: 290 / 360 * screenWidth,
                                  margin: EdgeInsets.only(
                                      left: 15 / 360 * screenWidth,
                                      right: 15 / 360 * screenWidth,
                                      bottom: 20 / 797 * screenHeight),
                                  child:
                                  (getListoflist[index] == null)?
                                  Text(
                                    noDes,
                                    style: TextStyle(
                                      color: NeutralColors.bluey_grey,
                                      fontSize: (14/720)*screenHeight,
                                      fontFamily: "IBMPlexSans",
                                      fontWeight: FontWeight.w500,
                                    ),
                                    //backgroundColor: Colors.transparent,
                                  ):
                                  Html(
                                    blockSpacing: 0.0,
                                    useRichText: false,
                                    // backgroundColor:Colors.red ,

                                    onLinkTap: (url){
                                      //If the url is a link to be opened in browser
                                        // setState(() {
                                        //   print("launching");
                                        //       try {
                                        //         launchURL(url);
                                        //       } catch (e) {
                                        //         print(e.toString());
                                        //       }
                                        //     });

                                        //If the url is a PDF
                                        AppRoutes.push(context,Emaximiser_PDFScreen(url) );
                                    },
                                    data:getListoflist[index],
                                    defaultTextStyle:  TextStyle(
                                      color: NeutralColors.bluey_grey,
                                      fontSize: (14/720)*screenHeight,
                                      fontFamily: "IBMPlexSans",
                                      fontWeight: FontWeight.w500,
                                    ),
//backgroundColor: Colors.transparent,
                                  ),
                                ),
                                /// Progress Indicator For subject ///
                                Container(
                                  height: 5 / 797 * screenHeight,
                                  child: ClipRRect(
                                    borderRadius: BorderRadius.only(
                                        bottomLeft: Radius.circular(5),
                                        bottomRight: Radius.circular(5)),
                                    child: LinearProgressIndicator(
                                      backgroundColor:
                                      //colors[(groupColorIndex)>4?groupColorIndex%5:groupColorIndex].withOpacity(.10),
                                      colorSelect(getWorkshopType[index],true),
                                      value: (getCompletedTest[index]==null)?0:getCompletedTest[index]/getTotalTest[index],
                                      valueColor: AlwaysStoppedAnimation<Color>(
                                        //colors[(groupColorIndex)>4?groupColorIndex%5:groupColorIndex],
                                        colorSelect(getWorkshopType[index],false)
                                      ),
                                    ),
                                  ),
                                )
                              ],
                            )),
                      );
                    },
                  ),
                ),
              ],
            ),
          ),
        ));
  }
}



getChewiePlayer(bool offStageFlag,videoId ,BuildContext context){
  Offstage offstage = new Offstage(
    offstage: offStageFlag,
    child: Padding(
      padding: const EdgeInsets.all(10.0),
      child: Container(
        margin: EdgeInsets.only(
            left: (10 / 360) * screenWidth, right: (10 / 360) * screenWidth),
        height: 185 / 720 * screenHeight,
        child: InkWell(
            onTap: () {
              print("useridforvideo");
              print(userid.toString());
              print(videoId);

              AppRoutes.push(context,videoId[1]!=null ? EmaxWebViewAndroid(videoId[1],userid,null) : EmaxWebViewAndroid(videoId[1],userid,null));


            },
          child: Container(

            constraints: new BoxConstraints.expand(
              height: (181 / 720) * screenHeight,

            ),
            alignment: Alignment.bottomLeft,
            padding: new EdgeInsets.only(left: 16.0, bottom: 8.0),
            decoration:  BoxDecoration(
              borderRadius: new BorderRadius.circular(5.0),
//               image: DecorationImage(
//                   alignment: Alignment(-.2, 0),
//                   image: CachedNetworkImageProvider(
//                       'http://d1cn01kw45hc6h.cloudfront.net/wp-content/uploads/2019/08/22161210/Quadtratic.png'),
// //                  image: NetworkImage(
// //                      'http://d1cn01kw45hc6h.cloudfront.net/wp-content/uploads/2019/08/22161210/Quadtratic.png'
// //                  ),
//                   fit: BoxFit.cover),
            ),
            child: Center(
              child: Container(
                height: (50 / 720) * screenHeight,
                width: (50 / 360) * screenWidth,
                child: SvgPicture.asset(
                  "assets/images/playwhite.svg",
                  fit: BoxFit.fitHeight,
                ),
              ),
            ),
          ),
        ),
      ),
    ),
  );
  return offstage;
}

class CustomBoxShadow extends BoxShadow {
  final BlurStyle blurStyle;

  const CustomBoxShadow({
    Color color = const Color(0xFF000000),
    Offset offset = Offset.zero,
    double blurRadius = 0.0,
    this.blurStyle = BlurStyle.normal,
  }) : super(color: color, offset: offset, blurRadius: blurRadius);

  @override
  Paint toPaint() {
    final Paint result = Paint()
      ..color = color
      ..maskFilter = MaskFilter.blur(this.blurStyle, blurSigma);
    assert(() {
      if (debugDisableShadows) result.maskFilter = null;
      return true;
    }());
    return result;
  }
}
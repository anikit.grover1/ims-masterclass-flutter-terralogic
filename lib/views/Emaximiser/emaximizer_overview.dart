import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:imsindia/utils/colors.dart';
import 'package:imsindia/utils/emaximiser_svg_icons.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:imsindia/utils/Url.dart';
import 'package:imsindia/api/bloc/services.dart';
import 'package:imsindia/utils/global.dart' as global;
class TestData {
  String title;
  List<String> marks;
  TestData({
    this.title,
    this.marks,
  });
}

class EmaximizerOverview extends StatefulWidget {
  var pos;
  EmaximizerOverview(this.pos);

  @override
  EmaximizerOverviewState createState() => EmaximizerOverviewState();
}

class EmaximizerOverviewState extends State<EmaximizerOverview> {
  var getIdForSubjectLevelId;
  var getLearFirstHiraLevelTitles = [''];

  void initState() {
    super.initState();
    getIdAndTitleForSubjectLevel().then((returnValue) {
      getSubjectLevelTitlesAndCardLevelData();
    });
  }

  void getSubjectLevelTitlesAndCardLevelData() async {
    if (global.headersWithOtherAuthorizationKey['Authorization'] != '') {
      print("APIII");
      print(URL.EMAX_URL_DROP+getIdForSubjectLevelId);
      ApiService()
          .getAPI(URL.EMAX_URL_DROP+getIdForSubjectLevelId, global.headersWithOtherAuthorizationKey)
          .then((returnValue) {
        setState(() {
          if (returnValue[0] == 'Fetched Successfully') {
            print("Data Fetched.....................");
            for (var index = 0;
            index < returnValue[1]['data'].length;
            index++) {
              getLearFirstHiraLevelTitles
                  .add(returnValue[1]['data'][index]['component']['title']);
            }
            print("shweta");
            String header= "CORRECT";
            print(getLearFirstHiraLevelTitles);
          } else {
            print("return value");
            print(returnValue[0]);
            //      authorizationInvalidMsg = returnValue[0];
          }
        });
      });
    }
  }

  getIdAndTitleForSubjectLevel() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    getIdForSubjectLevelId = prefs.getString("idForSubjectLevelIdEmax");
  }

  @override
  Widget build(BuildContext context) {

    final screenHeight = MediaQuery.of(context).size.height;
    final screenWidth = MediaQuery.of(context).size.width;
    TrackingScrollController _scrollController;
    @override
    void initState() {
      super.initState();
      _scrollController = new TrackingScrollController();
    }


    @override
    Future<bool> _onWillPop() {
      // AppRoutes.push(context, EmaximserQuestionScreen());
      int count = 0;
      Navigator.popUntil(context, (route) {
        return count++ == 4;
      }
      );
      setState(() {
//      print("Overlay closed");
//      hide();
//      overLayEntryCheck
//          ? overLayEntryCheck = false
//          : overLayEntryCheck = true;
//      //questionsScreen.buttonstatus=false;

      });

    }
    final test = TestData(
      title: "CAT Files 1 Set 1",
      marks: [
        "17/36",
        "19/36",
      ],
    );

    final heading = TestData(
        title: "",
        marks: [
          "CORRECT",
          "SKIPPED",
        ]);


    final List<TestData> tests = [
      heading,
      test,
      test,
      test,
      test,
      test,
      test,
      test,
    ];

    return WillPopScope(
      onWillPop: _onWillPop,
      child: Scaffold(
        body:
        (getLearFirstHiraLevelTitles.length==1)?
        Center(child: CircularProgressIndicator())
            :
        Container(
          // color: Colors.white,
          decoration: BoxDecoration(
            color: Color.fromARGB(255, 255, 255, 255),
            boxShadow: [
              BoxShadow(
                color: Color(0x0d676767),
                offset: Offset(8, 0),
                spreadRadius: 2,
                blurRadius: 16,
              ),
            ],
          ),
          constraints: BoxConstraints.expand(),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              Container(
                // color: Colors.white,

                margin: EdgeInsets.only(
                  top: (42 / 678) * screenHeight,
                ),
                child: Row(
                  //crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    InkWell(
                      onTap: () {
                        // Navigator.push(
                        //   context,
                        //   MaterialPageRoute(
                        //       builder: (context) => EmaximserQuestionScreen()),
                        int count = 0;
                        Navigator.popUntil(context, (route) {
                          return count++ == 4;
                        }
                        );
                      },
                      child: Container(
                        // color: Colors.white,
                        width: (58 / 360) * screenWidth,
                        height: (30.0 / 678) * screenHeight,
                        margin: EdgeInsets.only(left: 0.0),
                        child: SvgPicture.asset(
                          getSvgIcon.backbutton,
                          height: (5 / 678) * screenHeight,
                          width: (14 / 360) * screenWidth,
                          fit: BoxFit.none,
                        ),
                      ),
                    ),
                    Container(
                      height: (20 / 678) * screenHeight,
                      // margin: EdgeInsets.only(left: (20 / 360) * screenWidth),
                      child: Text(
                        widget.pos.toString(),
                        style: TextStyle(
                          color: NeutralColors.black,
                          fontSize: (16 / 678) * screenHeight,
                          fontFamily: "IBMPlexSans",
                          fontWeight: FontWeight.w700,
                        ),
                        textAlign: TextAlign.left,
                      ),
                    ),
                  ],
                ),
              ),

              Container(

                margin: EdgeInsets.only(left: 0, top: 0),
                height: 65 / 720 * screenHeight,
                width: screenWidth,
                // color: Colors.red,
                decoration: BoxDecoration(		            // color: Colors.red,
                  color: Color.fromARGB(255, 255, 255, 255),
                  boxShadow: [
                    BoxShadow(                      color: Color(0x0d676767),
                      offset: Offset(0, 8),
                      spreadRadius: 2,
                      blurRadius: 16,
                    ),
                  ],
                ),
                child: Container(
                  padding: EdgeInsets.only(
                      left: 20 / 360 * screenWidth, top: 20 / 720 * screenHeight),
                  child: Text(
                    "Overview",
                    style: TextStyle(
                      color: Color.fromARGB(255, 0, 0, 0),
                      fontSize: 18 / 360 * screenWidth,
                      fontFamily: "IBMPlexSans",
                      fontWeight: FontWeight.w700,
                    ),
                    textAlign: TextAlign.left,
                  ),
                ),
              ),
              //     Padding(padding: EdgeInsets.only(bottom: 10/720*screenHeight),),
              Expanded(
                //height: 590 / 720 * screenHeight,
                child: SingleChildScrollView(
                  child: Container(
                    height: 62 / 720 * screenHeight * (getLearFirstHiraLevelTitles.length),

                    child: Row(
                      children: <Widget>[
                        Container(

                          //  color: Colors.red,
                          decoration: BoxDecoration(
                            gradient: LinearGradient(
                              // Where the linear gradient begins and ends
                              begin: Alignment.topCenter,
                              end: Alignment.bottomCenter,
                              // Add one stop for each color. Stops should increase from 0 to 1
                              stops: [0, 0.05],
                              colors: [
                                // Colors are easy thanks to Flutter's Colors class.
                                NeutralColors.ice_blue,
                                Colors.white,
                              ],
                            ),
                            //     color: Color.fromARGB(255, 255, 255, 255),
                            boxShadow: [
                              BoxShadow(
                                color: NeutralColors.ice_blue,
                                offset: Offset(0, 7),
                                blurRadius: 3,
                              ),
                            ],
                          ),
                          // padding: EdgeInsets.only(top:50.00),

                          // margin: EdgeInsets.only(right: screenWidth/2, top:2),
                          // height:screenHeight ,
                          width: screenWidth / 2,
                          child: ListView.builder(
                              padding: EdgeInsets.all(0),
                              physics: NeverScrollableScrollPhysics(),
                              controller: _scrollController,
                              itemCount: getLearFirstHiraLevelTitles.length,
                              itemBuilder: (context, index) {
                                final item = getLearFirstHiraLevelTitles[index];
                                return Container(
                                  // color:Colors.red,

                                  child: Column(
                                    children: <Widget>[
                                      Container(
                                        height: 60 / 720 * screenHeight,
                                        //width: 100,
                                        //margin: EdgeInsets.only(top: 15 / 720 * screenHeight),
                                        child: Center(
                                          child: Text(
                                            (getLearFirstHiraLevelTitles[index].length>20)?
                                            getLearFirstHiraLevelTitles[index].substring(0,20)+"..."
                                                :
                                            getLearFirstHiraLevelTitles[index],
                                            style: TextStyle(
                                              color: Color.fromARGB(255, 71, 89, 147),
                                              fontSize: 14 / 360 * screenWidth,
                                              fontFamily: "IBMPlexSans",
                                              fontWeight: FontWeight.w500,
                                            ),
                                            textAlign: TextAlign.left,
                                          ),
                                        ),
                                      ),
                                      Container(
                                        height: 1 / 720 * screenHeight,
                                        margin: EdgeInsets.only(
                                            bottom: 1 / 720 * screenHeight),
                                        decoration: BoxDecoration(
                                          color: Color.fromARGB(255, 244, 244, 244),
                                        ),
                                        child: Container(),
                                      ),
                                    ],
                                  ),
                                );
                              }),
                        ),
                        Container(
                          //height: 590 / 720 * screenHeight,
                          decoration: BoxDecoration(
                            gradient: LinearGradient(
                              // Where the linear gradient begins and ends
                              begin: Alignment.topCenter,
                              end: Alignment.bottomCenter,
                              // Add one stop for each color. Stops should increase from 0 to 1
                              stops: [0, 0.05],
                              colors: [
                                // Colors are easy thanks to Flutter's Colors class.
                                NeutralColors.ice_blue,
                                Colors.white,
                              ],
                            ),
                            //     color: Color.fromARGB(255, 255, 255, 255),
                            boxShadow: [
                              BoxShadow(
                                color: NeutralColors.ice_blue,
                                offset: Offset(0.5, 7),
                                blurRadius: 3,
                              ),
                            ],
                          ),
                          width: screenWidth / 2,
                          child: Scrollbar(
                            //ListView
                            child: SingleChildScrollView(
                              scrollDirection: Axis.horizontal,
                              child: Container(
                                decoration: BoxDecoration(
                                  gradient: LinearGradient(
                                    // Where the linear gradient begins and ends
                                    begin: Alignment.centerLeft,
                                    end: Alignment.centerRight,
                                    // Add one stop for each color. Stops should increase from 0 to 1
                                    stops: [0, 0.05],
                                    colors: [
                                      // Colors are easy thanks to Flutter's Colors class.
                                      NeutralColors.ice_blue,
                                      Colors.white,
                                    ],
                                  ),
                                  //     color: Color.fromARGB(255, 255, 255, 255),
                                  boxShadow: [
                                    BoxShadow(
                                      color: NeutralColors.ice_blue,
                                      offset: Offset(0, 7),
                                      blurRadius: 3,
                                    ),
                                  ],
                                ),
                                //color: Colors.transparent,
                                //  margin: EdgeInsets.only(bottom:140),
                                width: 320 / 360 * screenWidth,
                                child: ListView.builder(
                                  padding: EdgeInsets.all(0),
                                  physics: NeverScrollableScrollPhysics(),
                                  controller: _scrollController,
                                  scrollDirection: Axis.vertical,
                                  itemCount: getLearFirstHiraLevelTitles.length,
                                  itemBuilder: (context, index) {
                                    final item = getLearFirstHiraLevelTitles[index];
                                    return Container(
                                      //     color: Colors.red,

                                      child: Column(
                                        children: <Widget>[
                                          Container(
                                            //    color: Colors.red,
                                            height: 60 / 720 * screenHeight,
                                            //width: 100,
                                            //margin: EdgeInsets.only(top: 15 / 720 * screenHeight),
                                            child: Row(
                                              crossAxisAlignment: CrossAxisAlignment.center,
                                              mainAxisAlignment: MainAxisAlignment.start,
                                              children: <Widget>[
                                                Container(

                                                  width: 90 / 360 * screenWidth,
                                                  child: Center(
                                                    child:
                                                    Text(
                                                      (index==0)?"CORRECT":
                                                      "17/36",
                                                      style: TextStyle(
                                                        color: NeutralColors.gunmetal,
                                                        fontSize: index == 0
                                                            ? 10 / 360 * screenWidth
                                                            : 12 / 360 * screenWidth,
                                                        fontFamily: "IBMPlexSans",
                                                        fontWeight: FontWeight.w500,
                                                      ),
                                                      textAlign: TextAlign.left,
                                                    ),
                                                  ),
                                                ),
                                                Container(
                                                  width: 80 / 360 * screenWidth,
                                                  child: Center(
                                                    child: Text(
                                                      (index==0)?"SKIPPED":
                                                      "19/36",
                                                      style: TextStyle(
                                                        color: NeutralColors.gunmetal,
                                                        fontSize: index == 0
                                                            ? 10 / 360 * screenWidth
                                                            : 12 / 360 * screenWidth,
                                                        fontFamily: "IBMPlexSans",
                                                        fontWeight: FontWeight.w500,
                                                      ),
                                                      textAlign: TextAlign.left,
                                                    ),
                                                  ),
                                                ),
                                                Container(
                                                  //height: 40/720*screenHeight,
                                                  width: 120 / 360 * screenWidth,
                                                  child: Center(
                                                    child: Container(
                                                      padding: EdgeInsets.symmetric(horizontal: 10/360 * screenWidth,vertical: 15 / 720 * screenHeight),
                                                      child: index == 0
                                                          ? null
                                                          : FlatButton(
                                                        //onPressed: null,
                                                        color: Colors.white,
                                                        shape:
                                                        RoundedRectangleBorder(
                                                          borderRadius:
                                                          BorderRadius.all(
                                                              Radius.circular(
                                                                  5)),
                                                          side: BorderSide(
                                                            width: 1,
                                                            color: NeutralColors
                                                                .gunmetal
                                                                .withOpacity(.1),
                                                            style:
                                                            BorderStyle.solid,
                                                          ),
                                                        ),
                                                        child: Center(
                                                          child: Text(
                                                            "Review",
                                                            style: TextStyle(
                                                              fontSize: 14 /
                                                                  360 *
                                                                  screenWidth,
                                                              fontFamily:
                                                              "IBMPlexSans",
                                                              fontWeight:
                                                              FontWeight.w500,
                                                              color: PrimaryColors
                                                                  .azure_Dark,
                                                            ),
                                                          ),
                                                        ),
                                                      ),
                                                    ),
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                          Container(
                                            height: 1 / 720 * screenHeight,
                                            margin: EdgeInsets.only(
                                                bottom: 1 / 720 * screenHeight),
                                            decoration: BoxDecoration(
                                              color: Color.fromARGB(255, 244, 244, 244),
                                            ),
                                            child: Container(),
                                          ),
                                        ],
                                      ),
                                    );
                                  },
                                ),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

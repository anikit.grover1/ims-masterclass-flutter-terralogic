import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:imsindia/components/progress_hub.dart';
import 'package:imsindia/views/Emaximiser/emaximiser_question_review_screen.dart';
import 'package:imsindia/views/Emaximiser/emaximizer_question_screen.dart';
import 'package:webview_flutter_plus/webview_flutter_plus.dart';
import '../../routers/routes.dart';
import 'package:imsindia/components/stop_watch_timer.dart';
import 'package:imsindia/components/custom_scrollbar_component.dart';
import 'package:imsindia/resources/strings/prepare.dart';
import 'package:imsindia/utils/colors.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:flutter_tex/flutter_tex.dart';
import 'dart:io' as Platform;


import 'emaximizer_videoplayer.dart';
import 'emaximizer_videoplayer_ios.dart';

var screenWidthTotal;
var screenHeightTotal;

var _safeAreaHorizontal;

var _safeAreaVertical;

var screenHeight;

var screenWidth;
var showAnsForSolution;
var statusForQuestion;
var correctAnswerForMCQTypeQuestions;
class ShowAnswersScreenEmax  extends StatefulWidget {
  final int value;
  final Stopwatch stopwatch;
  final int backtoques_num;
  String showAnswer;
  String showVideo;
  String statusForQuestion;
  String correctAnswer;
  EmaximserQuestionScreenState EmaximserQuestionsScreen;
  EmaximiserQuestionReviewScreenUsingHtmlState EmaximiserQuestionsReviewScreen;

  ShowAnswersScreenEmax ({this.value,this.stopwatch,this.backtoques_num,this.showAnswer,this.EmaximserQuestionsScreen,this.EmaximiserQuestionsReviewScreen,this.statusForQuestion,this.correctAnswer,this.showVideo});
  @override
  _ShowAnswersScreenEmaxState createState() => _ShowAnswersScreenEmaxState();
}

class _ShowAnswersScreenEmaxState extends State<ShowAnswersScreenEmax > {
  final Dependencies dependencies = new Dependencies();

  final ScrollController controller = ScrollController();
  WebViewPlusController webViewPlusControllerShowAnswer;
  WebViewPlusController webViewPlusControllerAnswer;
  double _heightShowAnswer = 1;
  double _heightAnswer = 1;
  Timer _timer;
  bool exaplanationForWebViewLoader=true;

  String  player(String value) {
    String _player = '''
   <html>
    <head>
               <meta name='viewport' content='width=device-width,initial-scale=1,maximum-scale=1'>
          </head> 
     
   <link href='https://fonts.googleapis.com/css?family=IBM Plex Sans' rel='stylesheet'>
   <body>
    <div id ="PSGQuestion1" style="font-family: 'IBM Plex Sans',sans-serif;">$value</div>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.7/latest.js?config=TeX-MML-AM_CHTML"></script>
    <script>
    var mathpsg1ques = document.getElementById("PSGQuestion1");
   
    MathJax.Hub.Queue(["Typeset",MathJax.Hub,mathpsg1ques]);


    </script>
    </body>
    </html>
   
    ''';
    return _player;
    //return 'data:text/html;base64,${base64Encode(const Utf8Encoder().convert(_player))}';
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    showAnsForSolution=widget.showAnswer;
    statusForQuestion=widget.statusForQuestion;
  }
  @override
  void dispose(){
    super.dispose();
    _timer.cancel();
  }
  @override
  bool pressed = false;
  bool pressAttention = true;

  Widget build(BuildContext context) {
    var _mediaQueryData = MediaQuery.of(context);
    screenWidthTotal = _mediaQueryData.size.width;
    screenHeightTotal = _mediaQueryData.size.height;
    _safeAreaHorizontal =
        _mediaQueryData.padding.left + _mediaQueryData.padding.right;
    _safeAreaVertical =
        _mediaQueryData.padding.top + _mediaQueryData.padding.bottom;

    screenHeight = screenHeightTotal - _safeAreaVertical;
    screenWidth = screenWidthTotal - _safeAreaHorizontal;

    return  Container(
      height: (476/720)*screenHeight,
      margin: EdgeInsets.only(left: (20/360)*screenWidth),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Align(
            alignment: Alignment.topLeft,
            child:
            InkWell(
              child: Container(
                width: (150/360)*screenWidth,
                height: (23/720)*screenHeight,
                child: Row(
                  children: [
                    Container(
                        child:Icon(
                          Icons.arrow_back_ios,
                          color: PrimaryColors.azure_Dark,
                          size: 15/720*screenHeight,
                        )
                    ),
                    Container(
                      margin: EdgeInsets.only(left: (8/360)*screenWidth),
                      child:Text(
                        StatisticsScreenStrings.Text_BackToQuestion,
                        style: TextStyle(
                          color: Color.fromARGB(255, 0, 171, 251),
                          fontSize: (14/360)*screenWidth,
                          fontFamily: "IBMPlexSans",
                        ),
                        textAlign: TextAlign.left,


                      ),
                    ),
                  ],
                ),
              ),
              onTap: (){
                setState(() {
                  dependencies.stopwatch = widget.stopwatch;
                  if(widget.EmaximserQuestionsScreen != null && widget.EmaximiserQuestionsReviewScreen == null){
                    widget.EmaximserQuestionsScreen.setState(() {
                      widget.EmaximserQuestionsScreen.emaxHeader_status_question=3;
                      widget.EmaximserQuestionsScreen.pageViewController=PageController(initialPage: widget.backtoques_num-1);
                      widget.EmaximserQuestionsScreen.widget.stopwatch=widget.stopwatch;
                      widget.EmaximserQuestionsScreen.panelSlide();
                      widget.EmaximserQuestionsScreen.widget.emaxStatisticsCheck=false;

                    });
                  }
                  if(widget.EmaximiserQuestionsReviewScreen != null && widget.EmaximserQuestionsScreen == null){
                    widget.EmaximiserQuestionsReviewScreen.setState(() {
                      widget.EmaximiserQuestionsReviewScreen.emaximHeader_status_question=3;
                      widget.EmaximiserQuestionsReviewScreen.emaximSumbit_button_status=null;
                      widget.EmaximiserQuestionsReviewScreen.pageViewController=PageController(initialPage: widget.backtoques_num-1);
                      widget.EmaximiserQuestionsReviewScreen.widget.stopwatch=widget.stopwatch;
                      widget.EmaximiserQuestionsReviewScreen.panelSlide();
                      widget.EmaximiserQuestionsReviewScreen.widget.emaximStatisticsCheck=false;

                    });
                  }

                });

              },
            ),

          ),
          Align(
            alignment: Alignment.topLeft,
            child:widget.correctAnswer==null?Container(
              height:(22/720)*screenHeight ,
              //width: (81/360)*screenWidth,
              margin: EdgeInsets.only(top:(21/720)*screenHeight),
              child:Text(
                ShowAnswerScreenStrings.Text_Answer+"A",
                style: TextStyle(
                  color: Color.fromARGB(255, 0, 3, 44),
                  fontSize: (16/360)*screenWidth,
                  fontFamily: "IBMPlexSans",
                  fontWeight: FontWeight.w500,
                ),
                textAlign: TextAlign.left,
              ) ,
            ):Container(
              child:
              Container(
                //height:(22/720)*screenHeight ,
                //width: (81/360)*screenWidth,
                margin: EdgeInsets.only(top:(21/720)*screenHeight),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      ShowAnswerScreenStrings.Text_Answer,
                      style: TextStyle(
                        color: Color.fromARGB(255, 0, 3, 44),
                        fontSize: (16/360)*screenWidth,
                        fontFamily: "IBMPlexSans",
                        fontWeight: FontWeight.w500,
                      ),
                      textAlign: TextAlign.left,
                    ),
                    Container(
                      height: _heightAnswer,
                      margin: EdgeInsets.only(right: 20/360*screenWidth,left: 20/360*screenWidth),
                      width: 180/360*screenWidth,
                      child:
//                         TeXView(
                        
//                           child: TeXViewInkWell(
//                               id: "id_0",
//                               child: TeXViewDocument(
//                                   widget.correctAnswer??'',
//                                   style: TeXViewStyle(textAlign: TeXViewTextAlign.Left))

//                           ),

//                           style: TeXViewStyle(
//                             //// elevation: 10,
//                             //  borderRadius: TeXViewBorderRadius.all(25),
// //                                                        border: TeXViewBorder.all(TeXViewBorderDecoration(
// //                                                            borderColor: Colors.blue,
// //                                                            borderStyle: TeXViewBorderStyle.Solid,
// //                                                            borderWidth: 5)),
//                               backgroundColor: Colors.transparent,
//                               textAlign:TeXViewTextAlign.Right
//                           ),

                                  WebViewPlus(
                                    onWebViewCreated: (controller) {
                                      this.webViewPlusControllerAnswer = controller;
                                      controller.loadString(player(widget.correctAnswer??''));
                                      setState((){
                                        _heightAnswer = 1;
                                        //exaplanationForWebViewLoader=true;
                                      });

                                    },
                                    onPageFinished: (url) {
                                      webViewPlusControllerAnswer.getHeight().then((double height) {
                                        print("Height:  " + height.toString());
                                        setState(() {
                                          _heightAnswer = height;
                                          _timer = new Timer(const Duration(milliseconds: 200), () {
                                            setState(() {
                                              //exaplanationForWebViewLoader=false;
                                            });
                                          });
                                        });
                                      });
                                    },
                                    javascriptMode: JavascriptMode.unrestricted,
                                
//                                        loadingWidgetBuilder: (context) => Center(
//                                          child: Column(
//                                            crossAxisAlignment: CrossAxisAlignment.center,
//                                            mainAxisSize: MainAxisSize.min,
//                                            mainAxisAlignment: MainAxisAlignment.center,
//                                            children: <Widget>[
//                                              CircularProgressIndicator(),
//                                              Text("Rendering...")
//                                            ],
//                                          ),
//                                        ),
                      ),                      ),
                  ],
                ),
              ) ,
            ),
          ),
          Align(
            alignment: Alignment.topLeft,
            child:Container(
              width: (107/360)*screenWidth,
              height: (20/720)*screenHeight,
              margin: EdgeInsets.only(top:(10/720)*screenHeight),
              decoration: BoxDecoration(
                // color:  (widget.statusForQuestion=="true")? PrimaryColors.kelly_green:(widget.statusForQuestion=="false")? PrimaryColors.dark_coral: Colors.yellow,
                border: Border.all(
                  color: (widget.statusForQuestion=="true")? PrimaryColors.kelly_green:(widget.statusForQuestion=="false")? PrimaryColors.dark_coral: Colors.yellow,

                  width: 1,
                ),
                borderRadius: BorderRadius.all(Radius.circular(2)),
              ),
              child:  Center(child:Text(
                (widget.statusForQuestion=="true")? Preparequestions.correctStatus:(widget.statusForQuestion=="false")? Preparequestions.wrongStatusForReview: Preparequestions.skippedStatus,
                style: TextStyle(
                  color:(widget.statusForQuestion=="true")? PrimaryColors.kelly_green:(widget.statusForQuestion=="false")? PrimaryColors.dark_coral: Colors.yellow,
                  fontSize: (12/720)*screenHeight,
                  fontFamily: "IBMPlexSans",
                  fontWeight: FontWeight.w500,
                ),
                textAlign: TextAlign.center,
              ),),
            ),),
          widget.showVideo != null ? GestureDetector(
            onTap: (){
              print("this is the vimeo id"+widget.showVideo.toString());
              if(Platform.Platform.isAndroid)
              {
                AppRoutes.push(context,EmaxWebViewAndroid("","",widget.showVideo.toString()) );
              }
              if(Platform.Platform.isIOS)
              {
                AppRoutes.push(context,EmaxWebViewiOS("","",widget.showVideo.toString()) );
              }
            },
            child: Container(
              margin: EdgeInsets.only(top: (20/720)*screenHeight,bottom:(20/720)*screenHeight),
              height: (27/720)*screenHeight,
              child: Text("Video Solution",
                style: TextStyle(
                  color: Color.fromARGB(255, 0, 171, 251),
                  fontSize: (14/360)*screenWidth,
                  fontFamily: "IBMPlexSans",
                ),
              ),
            ),
          ):Container(),

          Expanded(
            //alignment: Alignment.topLeft,
            child:Container(
              // width: (52/360)*screenWidth,
              height: (pressed==false)?(27/720)*screenHeight:(310/720)*screenHeight,
              margin: EdgeInsets.only(right:(0/360)*screenWidth),
              child: InkWell(
                  onTap: (){

                    setState(() {
                      pressed = true;
                    });
                  },
                  child:(pressed==false)? Text(
                    ShowAnswerScreenStrings.Text_Solution,
                    style: TextStyle(
                      color: Color.fromARGB(255, 0, 171, 251),
                      fontSize: (14/360)*screenWidth,
                      fontFamily: "IBMPlexSans",
                    ),
                    textAlign: TextAlign.justify,
                  ):
                  ProgressHUD(
                      color:Colors.white,
                      child:DraggableScrollbar(
                    controller: controller,
                    heightScrollThumb: 78.0/720*screenHeight,
                    weightScrollThumb: 3/360*screenWidth,
                    colorScrollThumb: Color(0xffcecece),
                    marginScrollThumb: EdgeInsets.only(right: 6/360*screenWidth,top:30/720*screenHeight),

                    child:Column(
                      children: <Widget>[
                      Expanded(
                            //height: (336/720)*screenHeight,
                            //width: (360)*screenHeight,
                            //margin: EdgeInsets.only(top:(20/720)*screenHeight ),
                            child: ListView(
                              controller: controller,
                              //shrinkWrap: true,
                              padding: EdgeInsets.only(top:0/720*screenHeight),
                              children:[
                                Container(
                                  height: _heightShowAnswer,
                                  margin: EdgeInsets.only(right: 20/360*screenWidth),
                                  child:
//                 TeXView(

//                   child: TeXViewInkWell(
//                       id: "id_0",
//                       child: TeXViewDocument(
//                           showAnsForSolution,
//                           style: TeXViewStyle(textAlign: TeXViewTextAlign.Left))

//                   ),

//                   style: TeXViewStyle(
//                     //// elevation: 10,
//                     //  borderRadius: TeXViewBorderRadius.all(25),
// //                                                        border: TeXViewBorder.all(TeXViewBorderDecoration(
// //                                                            borderColor: Colors.blue,
// //                                                            borderStyle: TeXViewBorderStyle.Solid,
// //                                                            borderWidth: 5)),
//                       backgroundColor: Colors.transparent,
//                       textAlign:TeXViewTextAlign.Right
//                   ),
                                  WebViewPlus(
                                    onWebViewCreated: (controller) {
                                      this.webViewPlusControllerShowAnswer = controller;
                                      controller.loadString(player(showAnsForSolution));
                                      setState((){
                                        _heightShowAnswer = 1;
                                        exaplanationForWebViewLoader=true;
                                      });

                                    },
                                    onPageFinished: (url) {
                                      webViewPlusControllerShowAnswer.getHeight().then((double height) {
                                        print("Height:  " + height.toString());
                                        setState(() {
                                          _heightShowAnswer = height;
                                          _timer = new Timer(const Duration(milliseconds: 200), () {
                                            setState(() {
                                              exaplanationForWebViewLoader=false;
                                            });
                                          });
                                        });
                                      });
                                    },
                                    javascriptMode: JavascriptMode.unrestricted,

//                                        loadingWidgetBuilder: (context) => Center(
//                                          child: Column(
//                                            crossAxisAlignment: CrossAxisAlignment.center,
//                                            mainAxisSize: MainAxisSize.min,
//                                            mainAxisAlignment: MainAxisAlignment.center,
//                                            children: <Widget>[
//                                              CircularProgressIndicator(),
//                                              Text("Rendering...")
//                                            ],
//                                          ),
//                                        ),
                                  ),
                                ),],
                            ),),

                      ],
                    ),

                  ),
                      inAsyncCall:(exaplanationForWebViewLoader==false)?false:true)
              ),
            ),),
        ],
      ),
    );
  }
}

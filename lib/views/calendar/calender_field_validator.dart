import 'package:imsindia/resources/strings/login.dart';

//validation for Text Input Fields
class FieldValidatorCalender {

  static bool college = false;
  static String validateCollege(String value){
    if (value.isEmpty) {
      college=false;
      return null;
    }
    else if(value.length<5){
      college=false;
      return "title should be atleast 5 characters";
    }
    Pattern pattern = r'^[0-9]*$';
    RegExp regex = new RegExp(pattern);
    if (regex.hasMatch(value.trim())) {
      college=false;
      return "invalid title";
    }
    if (!regex.hasMatch(value.trim())) {
      college=true;

      return null;
    }
  }

}
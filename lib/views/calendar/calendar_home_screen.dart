import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:imsindia/components/bottom_navigation.dart';
import 'package:imsindia/components/custom_DropDown_Calendar.dart';
import 'package:imsindia/components/navigation_bar.dart';
import 'package:imsindia/components/video_player.dart' as prefix0;
import 'package:imsindia/utils/colors.dart';
import 'package:imsindia/utils/svg_images/calendar_svg_images.dart';
import 'package:imsindia/views/calendar/calender_create_event.dart';
import 'package:imsindia/views/home_pages/home_widget.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../../routers/routes.dart';
import 'package:sliding_up_panel/sliding_up_panel.dart';
import 'package:intl/intl.dart';
import 'package:flutter_calendar_carousel/flutter_calendar_carousel.dart'
    show CalendarCarousel, WeekdayFormat;
import 'package:flutter_calendar_carousel/classes/event.dart';
import 'package:flutter_calendar_carousel/classes/event_list.dart';
import 'package:imsindia/utils/colors.dart';
import 'package:queries/collections.dart';
var screenWidthTotal;
var screenHeightTotal;

var _safeAreaHorizontal;

var _safeAreaVertical;

var screenHeight;

var screenWidth;
var finalListForEvents=[
  ["Announcement of simCAT 103 results","Mentor Visit with Jhon Doe","Announcement of CAT 2018 results","Mentor Visit with Jhon Doe\n 10:30 AM - 11:00 PM"],
  ["Announcement of simCAT 103 results","Mentor Visit with Jhon Doe"],
  ["Announcement of simCAT 103 results","Mentor Visit with Jhon Doe","Mentor Visit with Jhon Doe\n 10:30 AM - 11:00 PM"]
];
List<Color> ListOfcolors =[
  NeutralColors.mango.withOpacity(0.1),
  NeutralColors.sun_yellow.withOpacity(0.1),
  PrimaryColors.azure_Dark.withOpacity(0.1),
  NeutralColors.purply.withOpacity(0.1)
];
List dateOfEvent=[
  "23","12","28"
];
List dayOfEvent=[
  "FRI","MON","WED"
];
List<String> getMonths = [];
List<String> dropDownMonths = [];




class EventData {
  String category;
  String title;
  String time;
  String time1;


  EventData({

    this.time,
    this.time1,
    this.title,
    this.category,


  });
}



class CalendarHomeScreen extends StatefulWidget {
  @override
  _CalendarHomeScreenState createState() => _CalendarHomeScreenState();
}

class _CalendarHomeScreenState extends State<CalendarHomeScreen> {
  String selectedMonth;
  bool matched = false;
  bool markedweekenddate = false;
  String category;
  String title;
  String time;
  String time1;

  String finalDateToCompare;
  int _selectedIndexDate;
  int _selectedIndexSlot;
  bool _enabledVenue = false;
  bool _enabledDate = false;
  bool _enabledSlot = false;


  _onSelectedDate(int suggestion) {
    setState(() => _selectedIndexDate = suggestion);
  }

  final TextEditingController _sample = new TextEditingController();
  final _controller = PanelController();
  double _panelHeightClosed = 0.0;
  int calMonthValue = 0;
  String calYearValue = '';
  bool popupflag;

//static int pos =11;
  bool calendarActive = false;
  DateTime _choosemonth = new DateTime(2019, 12 ,13);
  DateTime _currentDate = DateTime.now();
  DateTime _month ;
  String _currentMonth = '';
  String currentYear = '';

  String lastTwodigitofyear = '';
  var monthInint = 2;

  @override
  void initState() {
    getData();
    popupState();
    print(DateTime.now());
    _currentMonth = DateFormat("MMMM").format(DateTime.now());
    currentYear = DateFormat("yyyy").format(DateTime.now());
    lastTwodigitofyear = currentYear.substring(currentYear.length - 2);
    var listnov  = _currentMonth+"\'"+lastTwodigitofyear;
    var today = new DateTime.now();
    var today1 = new  DateTime(2019, monthInint, 13);
    for(int month=1;month<13;month++){
      var nextmonthFromNow = today.add(new Duration(days: 29));
      today = nextmonthFromNow;
      String monthfromnow = DateFormat("MMMM").format(today);
      String yearfromnow = DateFormat("yyyy").format(today);
      print("==========="+monthfromnow.toString() +"=========="+ DateFormat("MMMM").format(today));
      var lasttwodigit_year = yearfromnow.substring(yearfromnow.length - 2);
      getMonths.add(listnov);
      getMonths.add(monthfromnow +"'"+lasttwodigit_year);
      var result = new Collection(getMonths).distinct();
      dropDownMonths=result.toList();
      print("listnov"+getMonths.toString());
      //print(result.toList());

    }


    DateTime now = DateTime.now();
    DateFormat dateFormat = DateFormat("EEE d MMMM yyyy");
    String formattedDate = dateFormat.format(DateTime.now());
    var newDate = DateFormat("EEE d MM yyyy").format(DateTime.now());
    print(newDate);

    print("=========Month"+_currentMonth+"============="+currentYear+"==============="+formattedDate+"==========="+DateFormat("MM").format(today1));
    //_month = DateTime.parse(DateFormat("MMMM").format(today1)) ;
    print("monthssssssssssssssss"+_month.toString());


    super.initState();
  }





  @override
  void didChangeDependencies() {
    getData();
    popupState();
    super.didChangeDependencies();
  }


  void popupState(){
    if(popupflag == true && calendarActive==true){
      _controller.open();
    }
  }



  void getData() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    if( preferences.getString("dropDownVal")!=null){
      setState(() {
        Eventcontent.category = preferences.getString("dropDownVal");
        print("dropdownallllll");
      });
    }else{
      setState(() {
        Eventcontent.category = "Topic";
        print("nulllllllll");
      });
    }
    if( preferences.getString("Calendartitle")!=null){
      setState(() {
        Eventcontent.title = preferences.getString("Calendartitle");
        print("*************Calendartitle"+Eventcontent.title);
      });
    }else{
      setState(() {
        Eventcontent.title = "Calculus";
      });
    }
    if( preferences.getString("Time")!=null){
      setState(() {
        Eventcontent.time = preferences.getString("Time");
      });
    }else{
      setState(() {
        Eventcontent.time = "4:00 AM";
      });
    }
    if( preferences.getString("Time1")!=null){
      setState(() {
        Eventcontent.time1 = preferences.getString("Time1");
      });
    }else{
      setState(() {
        Eventcontent.time1 = "4:30 AM";
      });
    }
    if( preferences.getBool("popupflag")!=null){
      setState(() {
        popupflag = preferences.getBool("popupflag");
      });
    }else{
      setState(() {
        popupflag = false;
      });



    }
  }
//
//
//  @override
//  Future<bool> _onWillPop() {
//    (calendarActive==true)?
//    AppRoutes.push(context, (CalendarHomeScreen()))
//        :
//    AppRoutes.push(context, HomeWidget());
//    setState(() {
////      print("Overlay closed");
////      hide();
////      overLayEntryCheck
////          ? overLayEntryCheck = false
////          : overLayEntryCheck = true;
////      //questionsScreen.buttonstatus=false;
//
//    });
//
//  }

  @override
  Widget build(BuildContext context) {

    var _mediaQueryData = MediaQuery.of(context);
    screenWidthTotal = _mediaQueryData.size.width;
    screenHeightTotal = _mediaQueryData.size.height;
    _safeAreaHorizontal =
        _mediaQueryData.padding.left + _mediaQueryData.padding.right;
    _safeAreaVertical =
        _mediaQueryData.padding.top + _mediaQueryData.padding.bottom;

    screenHeight = screenHeightTotal - _safeAreaVertical;
    screenWidth = screenWidthTotal - _safeAreaHorizontal;

    return  Scaffold(
      appBar:AppBar(
        brightness: Brightness.light,
        centerTitle: false,
        titleSpacing: 0.0,
        backgroundColor: Colors.white,
        elevation: 0.0,
        iconTheme: IconThemeData(
          color: NeutralColors.black, //change your color here
        ),
        title: Container(
          // width: (302 / 360) * screenWidth,
          child: Text(
            "Calendar",
            style: TextStyle(
              color:NeutralColors.textblack,
              fontSize: (18 / 720) * screenHeight,
              fontFamily: "IBMPlexSans",
              fontWeight: FontWeight.w500,
            ),
            textAlign: TextAlign.left,
          ),
        ),
      ),
      drawer: NavigationDrawer(),
//      bottomNavigationBar: BottomNavigation(),
      body:  Container(

        child: SlidingUpPanel(
          maxHeight: screenHeight / 2.6,
          minHeight: _panelHeightClosed,
          parallaxEnabled: false,
          parallaxOffset: .5,
          //defaultPanelState: PanelState.OPEN,
          isDraggable: true,
          body: _body(),
          panel: _panel(finalDateToCompare,_currentMonth,currentYear),
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(25.0), topRight: Radius.circular(25.0)),
          //onPanelSlide: (double pos) => setState((){
          //_fabHeight =  _initFabHeight/2;
          //}),
          controller: _controller,
        ),
      ),

    );
  }

  Widget _body() {
    final screenHeight = MediaQuery.of(context).size.height;
    final screenWidth = MediaQuery.of(context).size.width;
    final defaultScreenHeight = 720;
    final defaultScreenWidth = 360;
    return Container(
      // height: 720/720*screenHeight,
      //constraints: BoxConstraints.expand(),
      decoration: BoxDecoration(
        color: Color.fromARGB(255, 255, 255, 255),
      ),
      child:      Stack(
        children: <Widget>[
          Positioned(child: GestureDetector(
            onTap: (){
              _controller.close();
            },
            child: Container(
              color: NeutralColors.pureWhite,
              child: Column(
                children: <Widget>[
                  Container(
                    margin: EdgeInsets.only(left: 20/360*screenWidth,right: 20/360*screenWidth,top: 13/720*screenHeight),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Container(
                          //   width:178 /360*screenWidth ,
                          height: 40/720*screenHeight,
                          decoration: BoxDecoration(
                            borderRadius: new BorderRadius.circular(5.0),
                            border: Border.all(color: NeutralColors.gunmetal.withOpacity(0.15)),
                          ),
                          child: DropDownFormField(
                            selected_month: selectedMonth,
                            getImmediateSuggestions: true,
                            // autoFlipDirection: true,
                            textFieldConfiguration: TextFieldConfiguration(
                              controller: _sample,
                              label: _currentMonth,
                              label1: " ' " + lastTwodigitofyear,

                            ),
                            suggestionsBoxDecoration: SuggestionsBoxDecoration(

                                borderRadius: new BorderRadius.circular(5.0),
                                shadowColor: Colors.black.withOpacity(.5),
                                color: Colors.white),
                            suggestionsCallback: (pattern) {
                              return SampleService.getSuggestions();
                            },
                            itemBuilder: (context, suggestion) {
                              print("suggestinmon"+selectedMonth.toString());
                              return Container(



                                color: selectedMonth != null &&
                                    selectedMonth == suggestion
                                    ? NeutralColors.ice_blue
                                    : NeutralColors.pureWhite,
                                child: Padding(
                                  padding:  EdgeInsets.only(top: (20 / 760) * screenHeight,
                                      bottom: (17 /760) * screenHeight,
                                      left:(15 / 360) * screenWidth ),
                                  child: Text(
                                    suggestion,
                                    style: TextStyle(
                                      fontFamily: "IBMPlexSans",
                                      fontWeight: FontWeight.w400,
                                      fontSize:
                                      (14 / 360) *
                                          screenWidth,
                                      color: selectedMonth != null &&
                                          selectedMonth == suggestion
                                          ? NeutralColors.purpleish_blue
                                          : NeutralColors.bluey_grey,
                                      textBaseline: TextBaseline.alphabetic,
                                      letterSpacing: 0.0,
                                      inherit: false,
                                    ),
                                  ),
                                ),
                              );
//                              ListTile(
//                                title: Text(
//                                  suggestion,
//                                  style: TextStyle(
//                                    fontFamily: "IBMPlexSans",
//                                    fontWeight: FontWeight.w500,
//                                    fontSize: 14 /360*screenWidth,
//                                    color: selectedMonth != null &&
//                                        selectedMonth == suggestion
//                                        ? NeutralColors.purpleish_blue
//                                        : NeutralColors.bluey_grey,
//                                    textBaseline: TextBaseline.alphabetic,
//                                   // letterSpacing: 0.0,
//                                    inherit: false,
//                                  ),
//                                ),
//                              );
                            },
                            hideOnLoading: true,
                            debounceDuration: Duration(milliseconds: 100),
                            transitionBuilder: (context, suggestionsBox, controller) {
                              return suggestionsBox;
                            },
                            onSuggestionSelected: (suggestion) {
                              for (int i = 0; i < dropDownMonths.length; i++) {
                                print("Cli");
                                if (dropDownMonths[i] == suggestion) {
                                  print(dropDownMonths[i]);
                                  print(suggestion);
                                  print("Clicked");
                                }
                              }
                              print(suggestion);
                              //  print(suggestion.toString().split('\'')[0]);
                              _sample.text = suggestion;
                              selectedMonth = suggestion;
                              setState(() {
                                _currentMonth = selectedMonth.split('\'')[0];
                                currentYear = selectedMonth.split('\'')[1];
                              });

                              print("=aftersplit"+currentYear+"========="+_currentMonth);
                              print(calMonthValue);
                              calMonthValue = 0;
                              print(currentYear);
                              if(_currentMonth == 'January')
                              {
                                calMonthValue = 1;
                                calYearValue =currentYear;
                                _currentDate = new DateTime(2019, calMonthValue ,13);
                                print('Jan');
                                print(calMonthValue);

                              }
                              else if(_currentMonth=='February'){
                                calMonthValue = 2;
                                print(calMonthValue);
                                _currentDate = new DateTime(2019, calMonthValue ,13);
                                print(currentYear);
                                print('Feb');
                              }
                              else if(_currentMonth=='March'){
                                calMonthValue = 3;
                                print(calMonthValue);
                                _currentDate = new DateTime(2019, calMonthValue ,13);
                                print('Feb');
                              }else if(_currentMonth=='April'){
                                calMonthValue = 4;
                                print(calMonthValue);
                                _currentDate = new DateTime(2019, calMonthValue ,13);
                                print('Feb');
                              }else if(_currentMonth=='May'){
                                calMonthValue = 5;
                                print(calMonthValue);
                                _currentDate = new DateTime(2019, calMonthValue ,13);
                                print('Feb');
                              }else if(_currentMonth=='June'){
                                calMonthValue = 6;
                                print(calMonthValue);
                                _currentDate = new DateTime(2019, calMonthValue ,13);
                                print('Feb');
                              }else if(_currentMonth=='July'){
                                calMonthValue = 7;
                                print(calMonthValue);
                                _currentDate = new DateTime(2019, calMonthValue ,13);
                                print('Feb');
                              }else if(_currentMonth=='August'){
                                calMonthValue = 8;
                                print(calMonthValue);
                                _currentDate = new DateTime(2019, calMonthValue ,13);
                                print('Feb');
                              }else if(_currentMonth=='September'){
                                calMonthValue = 9;
                                print(calMonthValue);
                                _currentDate = new DateTime(2019, calMonthValue ,13);
                                print('Feb');
                              }else if(_currentMonth=='October'){
                                calMonthValue = 10;
                                print(calMonthValue);
                                _currentDate = new DateTime(2019, calMonthValue ,13);
                                print('Feb');
                              }else if(_currentMonth=='November'){
                                calMonthValue = 11;
                                print(calMonthValue);
                                _currentDate = new DateTime(2019, calMonthValue ,13);
                                print('Feb');
                              }else if(_currentMonth=='December'){
                                calMonthValue = 12;
                                print(calMonthValue);
                                _currentDate = new DateTime(2019, calMonthValue ,13);
                                print('Feb');
                                print(currentYear);
                              }
                            },
                            //   keepSuggestionsOnSuggestionSelected: true,
                            onSaved: (value) => {print("something")},
                          ),

                        ),
                        Container(
                            width: 20/360*screenWidth,
                            height: 20/720*screenHeight,
                            // child: SvgPicture.asset(CalendarAssets.calendarBlackIcon,),
                            child: InkWell(
                              onTap: (){
                                print(_currentMonth);
                                setState(() {
                                  calendarActive = !calendarActive;
                                });
                              },
                              child: calendarActive ?  SvgPicture.asset(
                                CalendarAssets.calendarBlueIcon,
                                fit: BoxFit.fill,
                              ):  SvgPicture.asset(
                                CalendarAssets.calendarBlackIcon,
                                fit: BoxFit.fill,
                              ),
                            )
                        )
                      ],
                    ),
                  ),
                  calendarActive == true ?
                  Container(
                    margin: EdgeInsets.all(16.0),
                    color: Colors.white,
                    child: CarouselCalendar(),
                  ) : Container(),
                  (calendarActive == true )?
                  InkWell(
                    onTap: (){

                      for(int i=0;i<dateOfEvent.length;i++) {
                        if (_currentDate== dateOfEvent[i]) {
                          print("date of event...............");
                          print(dateOfEvent[i]);
                        }
                      }},
                    child:Container(
                      height: 280/720*screenHeight,
                      //  color: Colors.red,
                      child: Center(
                          child:
                          (matched!=true)?
                          Column(
                            children:[

                              Padding(
                                padding:  EdgeInsets.only(top:10/360*screenHeight),
                                child: Image.asset(
                                  "assets/images/no_events.png",
                                  // height: (11.3 / 720) * screenHeight,
                                  // width: (7 / 360) * screenWidth,
                                  fit: BoxFit.fitHeight,
                                ),
                              ),
                              Expanded(
                                //width: screenWidth/3,
                                //color: Colors.red,
                                  child: Text('No  events  today!')),

                            ],
                          ):


                          new ListView.builder(
                            itemBuilder: (BuildContext context, int i) {
                              return Container(
                                margin: EdgeInsets.only(left: 20/360*screenWidth,right: 20/360*screenWidth,top:0/720*screenHeight ),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[

                                    Container(
                                      //    color:Colors.blue,
                                      child:Column(
                                        crossAxisAlignment: CrossAxisAlignment.center,
                                        mainAxisAlignment: MainAxisAlignment.center,
                                        children: <Widget>[
                                          getEventsByDates(finalListForEvents[0],ListOfcolors,screenHeight,screenWidth)
                                        ],
                                      ) ,
                                    )
                                  ],
                                ),
                              );

                            },
                            itemCount:1,
                          )

                      ),
                    ),):  Expanded(
                      child: new ListView.builder(
                        itemBuilder: (BuildContext context, int i) {
                          return Container(
                            margin: EdgeInsets.only(left: 20/360*screenWidth,right: 20/360*screenWidth,top:33/720*screenHeight ),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Container(
                                    margin: EdgeInsets.only(top: 5/720*screenHeight),
                                    child:Column(
                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                      children: <Widget>[
                                        Text(dayOfEvent[i].toString(), style: TextStyle(
                                          color: NeutralColors.blue_grey,
                                          fontSize: 12 / 720 * screenHeight,
                                          fontFamily: "IBMPlexSans",
                                          fontWeight: FontWeight.w500,
                                        ),),
                                        Text(dateOfEvent[i].toString(),
                                          style: TextStyle(
                                            color: NeutralColors.purpleish_blue,
                                            fontSize: 26 / 720 * screenHeight,
                                            fontFamily: "IBMPlexSans",
                                            fontWeight: FontWeight.w400,
                                          ),)
                                      ],
                                    )),
                                Container(
                                  //    color:Colors.blue,
                                  child:Column(
                                    crossAxisAlignment: CrossAxisAlignment.center,
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: <Widget>[
                                      getEventsByDates(finalListForEvents[i],ListOfcolors,screenHeight,screenWidth)
                                    ],
                                  ) ,
                                )
                              ],
                            ),
                          );

                        },
                        itemCount:finalListForEvents.length,
                      )
                  ),
                ],
              ),
            ),
          )),
          Positioned(
              bottom: 120/720*screenHeight,
              right:20/360*screenWidth ,
              child: GestureDetector(
                onTap: (){
                  AppRoutes.push(context, CalenderCreateEvent());
                },

                child: Container(
                  height: 50/720*screenHeight,
                  width: 50/360*screenWidth,
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    gradient:  LinearGradient(
//                    begin: Alignment(0, 0),
//                    end: Alignment(1.0199999809265137, 1.0099999904632568),
                        colors: [SemanticColors.light_purpely, SemanticColors.dark_purpely]),

                  ),
                  child: Center(child: Icon(Icons.add,size: 27/360*screenWidth,color: Colors.white,),),
                ),
              ))
        ],
      ),


    );
  }

  Widget CarouselCalendar(){
    return Column(
      children:[
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 8.0, vertical: 0.0),
          child: SingleChildScrollView(
            child: CalendarCarousel<Event>(
              onDayPressed: (DateTime date, List<Event> events) {
                print("Day pressed");
                setState(() {
                  _currentDate = date;
                  print("+++++++++currewnt Date");
                  finalDateToCompare = DateFormat("dd").format(_currentDate);
                  print(finalDateToCompare);
                  print(dateOfEvent.length);
                  for(int i =0;i<dateOfEvent.length;i++) {
                    //  print(dateOfEvent[i]);
                    matched = false;
                    if (finalDateToCompare == dateOfEvent[i]) {
                      matched= true;
                      print(i);
                      print('matched');
                      break;
                    }
                    else {
                      print("Not Matched");
                    }
                  }
                });
              },
              thisMonthDayBorderColor: Colors.transparent,
              selectedDayButtonColor: NeutralColors.mango,
              selectedDayBorderColor: NeutralColors.mango,
              markedDateShowIcon: true,
              markedDateCustomTextStyle: TextStyle(
                // fontSize: 18,
                color: NeutralColors.mango,
              ),
              selectedDayTextStyle: TextStyle(color: Colors.white),
              weekendTextStyle: TextStyle(color: Colors.black),
              // daysTextStyle: TextStyle(color: Colors.black),
              nextDaysTextStyle: TextStyle(color: Colors.grey),
              prevDaysTextStyle: TextStyle(color: Colors.grey),
              // inactiveDaysTextStyle: TextStyle(color: Colors.red),
              weekdayTextStyle: TextStyle(color: Colors.black),
              weekDayFormat: WeekdayFormat.narrow,
              //  firstDayOfWeek: 0,
              showHeader: false,
              isScrollable: false,
              // showOnlyCurrentMonthDate: true,
              // weekDayMargin: EdgeInsets.only(bottom: 10),
              weekFormat: false,
              height: 338/720*screenHeight,
              selectedDateTime: _currentDate,
              daysHaveCircularBorder: true,
              //  showOnlyCurrentMonthDate: false,
              customGridViewPhysics: NeverScrollableScrollPhysics(),
              markedDatesMap: _getCarouselMarkedDates(),
              staticSixWeekFormat: false,

//        markedDateWidget: Container(
//          height: 3,
//          width: 20,
//          decoration: new BoxDecoration(
//            color: NeutralColors.mango,
//            shape: BoxShape.rectangle,
//           // borderRadius: BorderRadius.all(Radius.circular(8.0)),
//          ),
//        ),
            ),
          ),
        ),
        Padding(
          padding:  EdgeInsets.symmetric(horizontal: 10/360*screenWidth),
          child: Divider(),
        ),
      ],);
  }


  EventList<Event> _getCarouselMarkedDates1() {
    return EventList<Event>(
      events: {
        DateTime.now(): [
          new Event(
            date: DateTime.now(),
            title: 'Event 1',
          ),
        ],
        new DateTime(2019, 11, 5): [
          new Event(
            date: new DateTime(2019, 11, 5),
            title: 'Event 1',
          ),
        ],
        new DateTime(2019, 11, 22): [
          new Event(
            date: new DateTime(2019, 11, 22),
            title: 'Event 1',
          ),
        ],
        new DateTime(2019, 11, 24): [
          new Event(
            date: new DateTime(2019, 11, 24),
            title: 'Event 1',
          ),
        ],
        new DateTime(2019, 11, 26): [
          new Event(
            date: new DateTime(2019, 11, 26),
            title: 'Event 1',
          ),
        ],
      },
    );
  }

  EventList<Event> _getCarouselMarkedDates() {
    return EventList<Event>(
      events: {

        new DateTime(2019, 11, 12): [
          new Event(
            date: new DateTime(2019, 11, 12),
            title: 'Event 1',
          ),
        ],
        new DateTime(2019, 11, 23): [
          new Event(
            date: new DateTime(2019, 11, 23),
            title: 'Event 1',
          ),
        ],
        new DateTime(2019, 11, 28): [
          new Event(
            date: new DateTime(2019, 11, 28),
            title: 'Event 1',
          ),
        ],
        new DateTime(2019, 11, 28): [
          new Event(
            date: new DateTime(2019, 11, 28),
            title: 'Event 1',
          ),
        ],
      },
    );
  }
}




String category="";
String nameKey = "_key_name";
String time="";
String time1="";
String title="";


final Eventcontent = EventData(
  category: "Classes",
  title: "Calculus",
  time: "04:00 PM",
  time1:"04:30 PM",
);

Widget _panel(curdate,selmonth,curyear) {
  final defaultScreenHeight = 720;
  final defaultScreenWidth = 360;
  return new Container(

    color: Colors.transparent,

    child: new Container(

      decoration: new BoxDecoration(
          color: Colors.white,
          borderRadius: new BorderRadius.only(
              topLeft: const Radius.circular(20.0),
              topRight: const Radius.circular(20.0))),
      child: new Wrap(
        children: <Widget>[
          Container(

            margin: EdgeInsets.only(left: 20/defaultScreenWidth*screenWidth, top: 20/defaultScreenHeight*screenHeight, right: 20/defaultScreenWidth*screenWidth),
            child: Container(


              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  Container(
                    height: screenHeight * 41 / 720,
                    margin: EdgeInsets.only(left: 15/defaultScreenWidth*screenWidth, top: 17.5/defaultScreenHeight*screenHeight),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: [
                        Align(
                          alignment: Alignment.topLeft,
                          child: Text(
                            Eventcontent.title,
                            style: TextStyle(
                              color:Colors.black,
                              fontSize: screenHeight * 18 / 720,
                              fontFamily: "IBMPlexSans",
                              fontWeight: FontWeight.w500,
                            ),
                            textAlign: TextAlign.left,
                          ),
                        ),

                      ],
                    ),
                  ),
                  Container(
                    height: screenHeight * 41 / 720,
                    margin: EdgeInsets.only(left: 15/defaultScreenWidth*screenWidth, top: 20/defaultScreenHeight*screenHeight),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: [
                        Align(
                          alignment: Alignment.topLeft,
                          child: Text(
                            "Date",
                            style: TextStyle(
                              color: Color.fromARGB(255, 153, 154, 171),
                              fontSize: screenHeight * 12 / 720,
                              fontFamily: "IBMPlexSans",
                              fontWeight: FontWeight.w500,
                            ),
                            textAlign: TextAlign.left,
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(top: 4/defaultScreenHeight*screenHeight),
                          child: Text(
                            curdate.toString()+ ' ' +selmonth.toString()+" "+curyear,
                            style: TextStyle(
                              color: Color.fromARGB(255, 0, 3, 44),
                              fontSize: screenHeight * 13 / 720,
                              fontFamily: "IBMPlexSans",
                              fontWeight: FontWeight.w500,
                            ),
                            textAlign: TextAlign.left,
                          ),
                        ),
                      ],
                    ),
                  ),

                  Container(
                    height: screenHeight * 41 / 720,
                    margin: EdgeInsets.only(left: 15/defaultScreenWidth*screenWidth, top: 20 / defaultScreenHeight * screenHeight),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: [
                        Align(
                          alignment: Alignment.topLeft,
                          child: Container(
                            width: screenWidth * 130 / 360,
                            height: screenHeight * 41 / 720,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.stretch,
                              children: [
                                Align(
                                  alignment: Alignment.topLeft,
                                  child: Text(
                                    "From",
                                    style: TextStyle(
                                      color:
                                      Color.fromARGB(255, 153, 154, 171),
                                      fontSize: screenHeight * 12 / 720,
                                      fontFamily: "IBMPlexSans",
                                      fontWeight: FontWeight.w500,
                                    ),
                                    textAlign: TextAlign.left,
                                  ),
                                ),
                                Container(
                                  margin: EdgeInsets.only(top: 4/defaultScreenHeight*screenHeight),
                                  child: Text(
                                    Eventcontent.time,
                                    style: TextStyle(
                                      color: Color.fromARGB(255, 0, 3, 44),
                                      fontSize: screenHeight * 13 / 720,
                                      fontFamily: "IBMPlexSans",
                                      fontWeight: FontWeight.w500,
                                    ),
                                    textAlign: TextAlign.left,
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                        Spacer(),
                        Align(
                          alignment: Alignment.topLeft,
                          child: Container(
                            width: screenWidth * 150 / 360,
                            height: screenHeight * 41 / 720,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.stretch,
                              children: [
                                Align(
                                  alignment: Alignment.topLeft,
                                  child: Text(
                                    "To",
                                    style: TextStyle(
                                      color:
                                      Color.fromARGB(255, 153, 154, 171),
                                      fontSize: screenHeight * 12 / 720,
                                      fontFamily: "IBMPlexSans",
                                      fontWeight: FontWeight.w500,
                                    ),
                                    textAlign: TextAlign.left,
                                  ),
                                ),
                                Container(
                                  margin: EdgeInsets.only(top: 4/defaultScreenHeight*screenHeight),
                                  child: Text(
                                    Eventcontent.time1,
                                    style: TextStyle(
                                      color: Color.fromARGB(255, 0, 3, 44),
                                      fontSize: screenHeight * 13 / 720,
                                      fontFamily: "IBMPlexSans",
                                      fontWeight: FontWeight.w500,
                                    ),
                                    textAlign: TextAlign.left,
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    height: screenHeight * 41 / 720,
                    margin: EdgeInsets.only(left: 15/defaultScreenWidth*screenWidth, top: 20 / defaultScreenHeight * screenHeight),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: [
                        Align(
                          alignment: Alignment.topLeft,
                          child: Container(
                            width: screenWidth * 130 / 360,
                            height: screenHeight * 41 / 720,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.stretch,
                              children: [
                                Align(
                                  alignment: Alignment.topLeft,
                                  child: Text(
                                    "Topics",
                                    style: TextStyle(
                                      color:
                                      Color.fromARGB(255, 153, 154, 171),
                                      fontSize: screenHeight * 12 / 720,
                                      fontFamily: "IBMPlexSans",
                                      fontWeight: FontWeight.w500,
                                    ),
                                    textAlign: TextAlign.left,
                                  ),
                                ),
                                Container(
                                  margin: EdgeInsets.only(top: 4/defaultScreenHeight*screenHeight),
                                  child: Text(
                                    Eventcontent.category,
                                    style: TextStyle(
                                      color: Color.fromARGB(255, 0, 3, 44),
                                      fontSize: screenHeight * 13 / 720,
                                      fontFamily: "IBMPlexSans",
                                      fontWeight: FontWeight.w500,
                                    ),
                                    textAlign: TextAlign.left,
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                        Spacer(),
                        Align(
                          alignment: Alignment.topLeft,
                          child: Container(
                            width: screenWidth * 150 / 360,
                            height: screenHeight * 41 / 720,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.stretch,
                              children: [
                                Align(
                                  alignment: Alignment.topLeft,
                                  child: Text(
                                    "Created by",
                                    style: TextStyle(
                                      color:
                                      Color.fromARGB(255, 153, 154, 171),
                                      fontSize: screenHeight * 12 / 720,
                                      fontFamily: "IBMPlexSans",
                                      fontWeight: FontWeight.w500,
                                    ),
                                    textAlign: TextAlign.left,
                                  ),
                                ),
                                Container(
                                  margin: EdgeInsets.only(top: 4/defaultScreenHeight*screenHeight),
                                  child: Text(
                                    "Harsh Thacker",
                                    style: TextStyle(
                                      color: Color.fromARGB(255, 0, 3, 44),
                                      fontSize: screenHeight * 13 / 720,
                                      fontFamily: "IBMPlexSans",
                                      fontWeight: FontWeight.w500,
                                    ),
                                    textAlign: TextAlign.left,
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),

                ],
              ),
            ),
          ),
        ],),),
  );
}

Widget getEventsByDates(List strings,List<Color> ListOfcolors,screenHeight,screenWidth)
{
  List<Widget> list = new List<Widget>();
  for(var i=0; i < strings.length; i++){
    var split = strings[i].toString().split('\n').map((i) {
      if (i == "") {
        return Divider();
      } else {
        return i;
      }
    }).toList();
    list.add(
      Container(

        width: 270/360*screenWidth,
        decoration: BoxDecoration(
          color: ListOfcolors[i],
          borderRadius: new BorderRadius.circular(5.0),

        ),
        margin: EdgeInsets.only(top: 5/720*screenHeight,left: 13/360*screenWidth,right: 0.0),
        child: Container(
          margin: EdgeInsets.only(top:8/720*screenHeight,bottom: 8/720*screenHeight,right: 12/360*screenWidth),
          child: Container(
            child: Container(
              child: Container(
                child: split.length > 1?

                Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Container(
                      margin: EdgeInsets.only(left: 12/360*screenWidth),
                      child: Text(split[0].toString(), style: TextStyle(
                          color: NeutralColors.dark_navy_blue,
                          fontSize: 14 / 360 * screenWidth,
                          fontFamily: "IBMPlexSans",
                          fontWeight: FontWeight.w500,
                          height: 1.5
                      ),),
                    ),
                    Container(
                      margin: EdgeInsets.only(left: 8/360*screenWidth),

                      child: Text( split[1].toString(),

                        style: TextStyle(
                            color: NeutralColors.blue_grey,
                            fontSize: 14 / 360 * screenWidth,
                            fontFamily: "IBMPlexSans",
                            fontWeight: FontWeight.w500,
                            height: 1.5
                        ),),
                    )
                  ],
                )
                    :  Container(
                  margin: EdgeInsets.only(left: 12/360*screenWidth),

                  child:Text(split[0].toString(),  style: TextStyle(
                      color: NeutralColors.dark_navy_blue,
                      fontSize: 14 / 360 * screenWidth,
                      fontFamily: "IBMPlexSans",
                      fontWeight: FontWeight.w500,
                      height: 1.5
                  ),),
                ),
              ),
            ),
          ),),),
    );
  }
  return new Column(children: list);

}
class SampleService {
  static final List<String> degrees = [
    '',
  ];

  static List<String> getSuggestions() {
    List<String> matches = List();
    matches.addAll(dropDownMonths);

    //  matches.retainWhere((s) => s.toLowerCase().contains(query.toLowerCase()));
    return matches;
  }
}
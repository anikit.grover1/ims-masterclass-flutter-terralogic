import 'package:flutter/material.dart';
import 'package:imsindia/components/primary_Button.dart';
import 'package:imsindia/utils/colors.dart';
import 'package:imsindia/views/Arun/notification/Notification.dart';
import 'package:imsindia/views/Arun/search/search_widget.dart';
import 'package:imsindia/components/Input_forms.dart';
import 'package:imsindia/components/custom_DropDown.dart';
import 'package:imsindia/utils/home_svg_icons.dart';
import 'package:flutter_svg/svg.dart';
import 'package:shared_preferences/shared_preferences.dart';


class StatesService {
  static final List<String> venues = [
    'Tests',
    'Topic',
    'Mentor Visits',
    'Classes'
  ];
  List titles = ['am', 'pm'];

  static List<String> getSuggestions(String query) {
    List<String> matches = List();
    matches.addAll(venues);

    // matches.retainWhere((s) => s.toLowerCase().contains(query.toLowerCase()));
    return matches;
  }
}

GlobalKey _keyRed = GlobalKey();

class CalenderCreateEvent extends StatefulWidget {
  @override
  _CalenderCreateEventState createState() => _CalenderCreateEventState();
}

class _CalenderCreateEventState extends State<CalenderCreateEvent> {
  String category;
 // String title;
  bool titleChecked = false;
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  String time;
  String time1;
  String name;
  final TextEditingController _title = new TextEditingController();
  String get title => _title.text;


  TimeOfDay _fromTime =  TimeOfDay.now();
  TimeOfDay _fromTime1 =  TimeOfDay.fromDateTime(DateTime.now().add(new Duration(hours: 1)));

  bool _enabledCategory = false;


  final focus = FocusNode();
  String get email => _email.text;

  final TextEditingController _stateOfVenues = new TextEditingController();


  final TextEditingController _email = TextEditingController();


  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
  }

  @override
  void initState() {
    super.initState();
    _title.addListener(() {
      setState(() {
        validateCollege(title);
      });
    });
  }

  String validateCollege(String value) {
    print("****************************************titlw"+title);
    if (title.isEmpty) {
      titleChecked = false;
      return null;
    } else if (title.length < 5) {
      titleChecked = false;
      return "title should be atleast 5 characters";
    }
    else if (title.length > 4) {
      print("****************************************length achhghukhjkjl"+title);
      titleChecked = true;
      return null;
    }


  }


  final _formKey = GlobalKey<FormState>();

  @override
  Future<bool> _onWillPop() {
    Navigator.pop(context);
    setState(() {});
  }

  String dropDownValue;
  bool dateFlag;
  void setData() async {
    print("*******Coming setData");
    SharedPreferences preferences = await SharedPreferences.getInstance();
  print("%%%%%%%%%%%%%%%%%name" +title);
       preferences.setString("Calendartitle", title);
       await preferences.setBool("popupflag", dateFlag);
    await preferences.setString("Time", _fromTime.format(context));
    await preferences.setString("Time1", _fromTime1.format(context));
    await preferences.setString("dropDownVal", dropDownValue);
  }



  @override
  Widget build(BuildContext context) {
    var _mediaQueryData = MediaQuery.of(context);
    double screenWidth = _mediaQueryData.size.width;
    double screenHeight = _mediaQueryData.size.height;
    return WillPopScope(
      onWillPop: _onWillPop,
      child: Scaffold(
        backgroundColor: Colors.white,
        // resizeToAvoidBottomPadding:false,
         key: _scaffoldKey,
        appBar: new AppBar(
          brightness: Brightness.light,
          elevation: 0.0,
          centerTitle: false,
          iconTheme: IconThemeData(
            color: Colors.black, //change your color here
          ),
          title: Text(
            "Create Event",
            style: TextStyle(
              color: Color.fromARGB(255, 0, 0, 0),
              fontSize: 16 / 360 * screenWidth,
              fontFamily: "IBMPlexSans",
              fontWeight: FontWeight.w500,
            ),
          ),
          titleSpacing: 0.0,
          actions: <Widget>[
             Container(
                // width:0.130*screenWidth,
                margin: EdgeInsets.only(right: 20 / 360  * screenWidth),
                child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Container(
                        child: InkWell(
                          onTap: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(builder: (context) => Search()),
                            );
                          },
                          child: Padding(
                            padding: const EdgeInsets.all(5.0),
                            child: Container(
                              height: 20 / 720 * screenHeight,
                              width: 20 / 360 * screenWidth,
                              child: new SvgPicture.asset(
                                getSvgIcon.search,
                                fit: BoxFit.contain,
                                //color: Colors.red,
                              ),
                            ),
                          ),
                        ),
                      ),

                      Container(

                        child: Stack(
                          alignment: Alignment.bottomRight,
                          children: [

                            Positioned(
                              child:   Container(
                               // padding: EdgeInsets.only(left: 18 / 360 * screenWidth),
                                child: InkWell(
                                  onTap: () {
                                    Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) => Notifications()),
                                    );
                                  },
                                  child: Padding(
                                    padding: const EdgeInsets.all(5.0),
                                    child: Container(
                                      height: 20 / 720 * screenHeight,
                                      width: 20 / 360 * screenWidth,
                                      child: new SvgPicture.asset(
                                        getSvgIcon.notification,
                                        fit: BoxFit.contain,
                                        //color: Colors.red,
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            ),

                            Positioned(
                              right: 9,
                              bottom:14,
                            child:Container(
                              height:5,
                              width: 5,
                              decoration: new BoxDecoration(
                                shape: BoxShape.circle,
                                color: NeutralColors.grapefruit,
                              ),

                            ),


                            ),
                          ],
                        ),



                      ),
                    ])),
          ],
          backgroundColor: Color.fromARGB(255, 255, 255, 255),
          leading: GestureDetector(
            onTap: () {
              Navigator.pop(context);
            },
            child: Container(
              // color: Colors.white,
              width: (28 / 360) * screenWidth,
              height: (24.0 / 678) * screenHeight,
              margin: EdgeInsets.only(left: 0.0),
              child: SvgPicture.asset(
                "assets/images/back.svg",
                height: (5 / 678) * screenHeight,
                width: (14 / 360) * screenWidth,
                fit: BoxFit.none,
              ),
            ),
          ),
        ),

        body: GestureDetector(
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());

            print("++++++++++++++++++++++++++++++++++++++");
            print(_fromTime);
          },
          child: SingleChildScrollView(
            // color: Colors.white,
            child: Column(
              children: [
                Padding(
                    padding: EdgeInsets.only(
                        left: screenWidth * 20 / 360,
                        top: screenHeight * 25 / 720,
                        right: screenWidth * 20 / 360),
                    child: DropDownFormField(
                      getImmediateSuggestions: true,
                      textFieldConfiguration: TextFieldConfiguration(
                          controller: _stateOfVenues,
                          label: "Category"

                      ),
                      suggestionsBoxDecoration: SuggestionsBoxDecoration(
                          dropDownHeight: 180,
                          borderRadius: new BorderRadius.circular(5.0),
                          color: Colors.white),
                      suggestionsCallback: (pattern) {
                        return StatesService.getSuggestions(toString());
                      },
                      itemBuilder: (context, suggestion) {
                        return ListTile(
                          dense: true,
                          contentPadding: EdgeInsets.only(left: 10),
                          title: Text(
                            suggestion,
                            style: TextStyle(
                              fontFamily: "IBMPlexSans",
                              fontWeight: FontWeight.w400,
                              fontSize: (13 / 720) * screenHeight,
                              color: NeutralColors.bluey_grey,
                              textBaseline: TextBaseline.alphabetic,
                              letterSpacing: 0.0,
                              inherit: false,
                            ),
                          ),
                        );
                      },
                      hideOnLoading: true,
                      debounceDuration: Duration(milliseconds: 100),
                      transitionBuilder: (context, suggestionsBox, controller) {
                        return suggestionsBox;
                      },
                      onSuggestionSelected: (suggestion) {
                        setState(() {
                          _enabledCategory = true;
                          dropDownValue = suggestion;
                        });
                        for (int i = 0; i < StatesService.venues.length; i++) {
                          if (StatesService.venues[i] == suggestion) {
                            //  courseId =  StatesService.venues[i];
                          }
                        }
                        print(suggestion);
                        // _courses.text = suggestion;
                        _stateOfVenues.text = suggestion;
                        //setState(() {});
                      },
                      onSaved: (value) => {print("something")},
                    )),
                Form(
                  key: _formKey,
                  child: Container(
                    width: double.infinity,
                    margin: EdgeInsets.only(
                        left: screenWidth * 20 / 360,
                        top: screenHeight * 25 / 720,
                        right: screenWidth * 20 / 360),
                    child: TextInputFormField(
                      controller: _title,
                      //  focusNode: focus,
                      keyboardType: TextInputType.text,
                      textInputAction: TextInputAction.done,
                      validator: validateCollege,
                      border: title.isEmpty
                          ? UnderlineInputBorder(
                              borderSide: BorderSide(
                                  color:
                                      SemanticColors.blueGrey.withOpacity(0.3),
                                  width: 1.5))
                          : UnderlineInputBorder(
                              borderSide: BorderSide(
                                  color: SemanticColors.dark_purpely,
                                  width: 1.5)),
                      autovalidate: true,
                      onSaved: (value) {
                        print("*******************onSaved");

                        setState(() {
                          name = value;
                        });

                      },
                      lableText: "Title",
                      lableStyle: TextStyle(
                        fontFamily: "IBMPlexSans",
                        fontWeight: FontWeight.w400,
                        fontSize: (12 / 720) * screenHeight,
                        color: NeutralColors.bluey_grey,
                        textBaseline: TextBaseline.alphabetic,
                        letterSpacing: 0.0,
                        inherit: false,
                      ),

                      onFieldSubmitted: (val) {
                        print("*******************onFieldSubmitted");
                        setState(() {
                          name = val;
                        });
                        FocusScope.of(context).requestFocus(focus);
                      },

                      onEditingComplete: () {
                        print("*******************onEditingComplete");

                      },
                    ),
                  ),

                ),
                Container(
                  //  height: (79/720)*screenHeight,
                  margin: EdgeInsets.only(right: 20 / 360 * screenWidth),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Container(
                        margin: EdgeInsets.only(
                            left: 20 / 360 * screenWidth,
                            top: 20 / 720 * screenHeight),
                        child: Text(
                          "Time",
                          style: TextStyle(
                            fontFamily: "IBMPlexSans",
                            fontWeight: FontWeight.w400,
                            fontSize: (12.5 / 720) * screenHeight,
                            color: NeutralColors.bluey_grey,
                            inherit: false,
                          ),
                        ),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Expanded(
                              child: Container(
                            //  color: Colors.red,
                            margin: EdgeInsets.only(left: 22, top: 0),
                            child: _DateTimePicker(
                              labelText: "hh:mm",

                              selectedTime: _fromTime,
                              selectTime: (TimeOfDay time) {
                                setState(() {
                                  _fromTime = time;

                                });
                              },
                            ),
                          )),
                          Container(
                            width: screenWidth / 9,
                            //  color: Colors.red,
                            margin: EdgeInsets.only(left: 13.00, top: 20),
                            child: Center(child: Text('TO')),
                          ),
                          Expanded(
                              child: Container(
                            margin: EdgeInsets.only(left: 22, top: 0),
                            child: _DateTimePicker(
                              labelText: "hh:mm",
                              selectedTime: _fromTime1,
                              selectTime: (TimeOfDay time) {
                                setState(() {
                                  _fromTime1 = time;
                                });
                              },
                            ),
                          )),
                        ],
                      ),
                    ],
                  ),
                ),
                Container(
                    height: 40 / 720 * screenHeight,
                    margin: EdgeInsets.only(
                        left: 45 / 360 * screenWidth,
                        top: 45 / 720 * screenHeight,
                        right: 45 / 360 * screenWidth),
                    child: PrimaryButton(
                      gradient:
                      (_enabledCategory == true && titleChecked == true)
                          ? true
                          : false,
                      onTap: () {

                      DateTime fromTimeValue =   DateTime(DateTime.now().year, DateTime.now().month, DateTime.now().day, _fromTime.hour, _fromTime.minute);
                      DateTime ToTimeValue =   DateTime(DateTime.now().year, DateTime.now().month, DateTime.now().day, _fromTime1.hour, _fromTime1.minute);

                      print(fromTimeValue);
                      print(ToTimeValue);
                      print((ToTimeValue.isAfter(fromTimeValue)));


                      if (_enabledCategory == true && titleChecked == true) {
                        if ((ToTimeValue.isAfter(fromTimeValue))==false){
                          _scaffoldKey.currentState.showSnackBar(
                              SnackBar(
                                content: new Text('To Time should be greater than from Time'),
                                duration: new Duration(seconds: 10),
                              )
                          );

    setState(() {
    dateFlag = false;
    });
                        }
    if ((ToTimeValue.isAfter(fromTimeValue))==true){

    setState(() {
                            dateFlag = true;
                          });
                          setData();

                          Navigator.pop(context);      }                  }
                      },
                      text: "SAVE EVENT",
                    ) //
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  DateTime selectedDate = DateTime.now();
  Future<Null> _selectDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: selectedDate,
        firstDate: DateTime(2015, 8),
        lastDate: DateTime(2101));
    if (picked != null && picked != selectedDate)
      setState(() {
        selectedDate = picked;
      });
  }
}

_getPositions() {
  final RenderBox renderBoxRed = _keyRed.currentContext.findRenderObject();
  final positionRed = renderBoxRed.localToGlobal(Offset.zero);
  print("POSITION of Red: $positionRed ");
  return positionRed;
}

_getSizes() {
  final RenderBox renderBoxRed = _keyRed.currentContext.findRenderObject();
  final sizeRed = renderBoxRed.size;

  print("SIZE of Red: $sizeRed");
  return sizeRed.height;
}

class _DateTimePicker extends StatelessWidget {
  const _DateTimePicker({
    Key key,
    this.labelText,
    this.selectedTime,
    this.selectTime,
  }) : super(key: key);

  final String labelText;
  final TimeOfDay selectedTime;
  final ValueChanged<TimeOfDay> selectTime;

  Future<void> _selectTime(BuildContext context) async {
    final TimeOfDay picked = await showTimePicker(
      context: context,
      builder: (BuildContext context, Widget child) {
        return MediaQuery(
          data: MediaQuery.of(context).copyWith(alwaysUse24HourFormat: false),
          child: child,
        );
      },
      initialTime: selectedTime,
    );
    if (picked != null && picked != selectedTime) selectTime(picked);
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.end,
      children: <Widget>[
        Expanded(
          // flex: 3,

          child: _InputDropdown(
            labelText: "hh:mm",
            valueText: selectedTime.format(context),
            valueStyle: TextStyle(
              color: Color.fromARGB(255, 0, 0, 0),
              fontSize: 16 ,
              fontFamily: "IBMPlexSans",
              fontWeight: FontWeight.w500,
            ) ,
            //   valueStyle: valueStyle,
            onPressed: () {
              _selectTime(context);
              print("gsdfgsdjfhsdklfsd;f.........");
            },
          ),
        ),
      ],
    );
  }
}

class _InputDropdown extends StatelessWidget {
  const _InputDropdown({
    Key key,
    this.child,
    this.labelText,
    this.valueText,
    this.valueStyle,
    this.onPressed,
  }) : super(key: key);

  final String labelText;
  final String valueText;
  final TextStyle valueStyle;
  final VoidCallback onPressed;
  final Widget child;
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onPressed,
      child: InputDecorator(
        decoration: InputDecoration(
            enabledBorder: UnderlineInputBorder(
              borderSide: BorderSide(color: NeutralColors.purpley,
                  width: 1.0, style: BorderStyle.solid ),
            ),
            focusedBorder: UnderlineInputBorder(borderSide: BorderSide(color:Colors.red))
        ),



//          ? UnderlineInputBorder(
//      borderSide: BorderSide(
//      color:
//      SemanticColors.blueGrey.withOpacity(0.3),
//        width: 1.5))
//        : UnderlineInputBorder(
//    borderSide: BorderSide(
//    color: SemanticColors.dark_purpely,
//    width: 1.5)),

          child: (valueText.length>6)?
            Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Text(
              valueText.split(' ')[0],
              style: TextStyle(
                color: Color.fromARGB(255, 0, 0, 0),
                fontSize: 15 ,
                fontFamily: "IBMPlexSans",
                fontWeight: FontWeight.w400,
              ),
            ),
            Text(
              valueText.split(' ')[1].toLowerCase(),
              style:TextStyle(
                color: Colors.blue,
                fontSize: 15 ,
                fontFamily: "IBMPlexSans",
                fontWeight: FontWeight.w400,
              ),
            ),
          ],
        ):
            Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Text(
              valueText,
              style: TextStyle(
                color: Color.fromARGB(255, 0, 0, 0),
                fontSize: 15 ,
                fontFamily: "IBMPlexSans",
                fontWeight: FontWeight.w400,
              ),
            ),

          ],
        )
      ),
    );
  }
}

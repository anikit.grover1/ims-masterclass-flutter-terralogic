import 'package:flutter/material.dart';
import 'package:imsindia/utils/colors.dart';
import 'package:imsindia/utils/screen_util.dart';
class GKZonePageController extends StatefulWidget {
  @override
  _GKZonePageControllerState createState() => _GKZonePageControllerState();
}

class _GKZonePageControllerState extends State<GKZonePageController> {
  @override
  Widget build(BuildContext context) {
    var _mediaQueryData = MediaQuery.of(context);
    double screenWidth = _mediaQueryData.size.width;
    double screenHeight = _mediaQueryData.size.height;
    return Scaffold(
      body: Container(
        color: Colors.white,
        margin: EdgeInsets.only(left:(20/Constant.defaultScreenWidth)*screenWidth,right: ((20/Constant.defaultScreenWidth)*screenWidth)),
        child: Column(
          children: <Widget>[
            Container(

              child: Text(
                "Prose is to Poetry, said Valéry, as walking is to dancing. That poets regularly produce prose, while prose writers rarely write poetry, is not, as Brodsky argues, evidence of poetry’s superiority.",
                style: TextStyle(
                  fontSize: (16/Constant.defaultScreenWidth)*screenWidth,
                  color: NeutralColors.dark_navy_blue,
                  fontWeight: FontWeight.w500,
                //  height: 1.5,
                  fontFamily: "IBMPlexSans",
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.only(top:(30/Constant.defaultScreenHeight)*screenHeight),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  GestureDetector(
                    child: Container(
                      margin: EdgeInsets.only(right:(15/Constant.defaultScreenWidth)*screenWidth),
                      height: (35/Constant.defaultScreenHeight)*screenHeight,
                      width: (35/Constant.defaultScreenWidth) *screenWidth,
                      decoration: BoxDecoration(
                        border: Border.all(
                          color: NeutralColors.dark_navy_blue,
                          // color: Colors.cyan,
                          width: (2 / Constant.defaultScreenWidth) * screenWidth,
                        ),
                        borderRadius: BorderRadius.all(Radius.circular(3)),
                      ),
                      child: Align(
                        alignment: Alignment.center,
                        child: Text(
                          "1",
                          style: TextStyle(
                            fontSize: (14/Constant.defaultScreenWidth)*screenWidth,
                            color: NeutralColors.dark_navy_blue,
                            fontWeight: FontWeight.w400,
                            //  height: 1.5,
                            fontFamily: "IBMPlexSans",
                          ),
                          textAlign: TextAlign.center,
                        ),
                      ),
                    ),
                  ),
                  GestureDetector(
                    child: Container(
                      margin: EdgeInsets.only(right:(15/Constant.defaultScreenWidth)*screenWidth),
                      height: (35/Constant.defaultScreenHeight)*screenHeight,
                      width: (35/Constant.defaultScreenWidth) *screenWidth,
                      decoration: BoxDecoration(
                        border: Border.all(
                          color: NeutralColors.blue_grey,
                          // color: Colors.cyan,
                          width: (2 / Constant.defaultScreenWidth) * screenWidth,
                        ),
                        borderRadius: BorderRadius.all(Radius.circular(3)),
                      ),
                      child: Align(
                        alignment: Alignment.center,
                        child: Text(
                          "2",
                          style: TextStyle(
                            fontSize: (14/Constant.defaultScreenWidth)*screenWidth,
                            color: NeutralColors.dark_navy_blue,
                            fontWeight: FontWeight.w400,
                            //  height: 1.5,
                            fontFamily: "IBMPlexSans",
                          ),
                          textAlign: TextAlign.center,
                        ),
                      ),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(right:(15/Constant.defaultScreenWidth)*screenWidth),
                    height: (35/Constant.defaultScreenHeight)*screenHeight,
                    width: (35/Constant.defaultScreenWidth) *screenWidth,
                    decoration: BoxDecoration(
                      border: Border.all(
                        color: NeutralColors.blue_grey,
                        // color: Colors.cyan,
                        width: (2 / Constant.defaultScreenWidth) * screenWidth,
                      ),
                      borderRadius: BorderRadius.all(Radius.circular(3)),
                    ),
                    child: Align(
                      alignment: Alignment.center,
                      child: Text(
                        "3",
                        style: TextStyle(
                          fontSize: (14/Constant.defaultScreenWidth)*screenWidth,
                          color: NeutralColors.dark_navy_blue,
                          fontWeight: FontWeight.w400,
                          //  height: 1.5,
                          fontFamily: "IBMPlexSans",
                        ),
                        textAlign: TextAlign.center,
                      ),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(right:(15/Constant.defaultScreenWidth)*screenWidth),
                    height: (35/Constant.defaultScreenHeight)*screenHeight,
                    width: (35/Constant.defaultScreenWidth) *screenWidth,
                    decoration: BoxDecoration(
                      border: Border.all(
                        color: NeutralColors.blue_grey,
                        // color: Colors.cyan,
                        width: (2 / Constant.defaultScreenWidth) * screenWidth,
                      ),
                      borderRadius: BorderRadius.all(Radius.circular(3)),
                    ),
                    child: Align(
                      alignment: Alignment.center,
                      child: Text(
                        "4",
                        style: TextStyle(
                          fontSize: (14/Constant.defaultScreenWidth)*screenWidth,
                          color: NeutralColors.dark_navy_blue,
                          fontWeight: FontWeight.w400,
                          //  height: 1.5,
                          fontFamily: "IBMPlexSans",
                        ),
                        textAlign: TextAlign.center,
                      ),
                    ),
                  )
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.only(top:(20/Constant.defaultScreenHeight)*screenHeight),
                child: Divider()),
            Container(
              margin: EdgeInsets.only(top:(20/Constant.defaultScreenHeight)*screenHeight),
              child: Text(
                "Prose is to Poetry, said Valéry, as walking is to dancing. That poets regularly produce prose, while prose writers rarely write poetry, is not, as Brodsky argues, evidence of poetry’s superiority.",
                style: TextStyle(
                  fontSize: (16/Constant.defaultScreenWidth)*screenWidth,
                  color: NeutralColors.dark_navy_blue,
                  fontWeight: FontWeight.w400,
                  //  height: 1.5,
                  fontFamily: "IBMPlexSans",
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

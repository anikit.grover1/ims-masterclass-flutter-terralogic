import 'package:flutter/material.dart';
import 'package:imsindia/api/bloc/services.dart';
import 'package:imsindia/components/navigation_bar.dart';
import 'package:imsindia/resources/strings/channel_labels.dart';
import 'package:imsindia/resources/strings/gk_zone.dart';
import 'package:imsindia/routers/routes.dart';
import 'package:imsindia/utils/Url.dart';
import 'package:imsindia/utils/colors.dart';
import 'package:imsindia/utils/screen_util.dart';
import 'package:imsindia/views/GK_Zone/gk_zone_current_affairs_tab.dart';
import 'package:imsindia/views/GK_Zone/gk_zone_exam_wise_tab.dart';
import 'package:imsindia/views/GK_Zone/gk_zone_gkzone_screen.dart';
import 'package:imsindia/views/GK_Zone/gk_zone_sample.dart';
import 'package:imsindia/views/GK_Zone/gk_zone_week5_sept.dart';
import 'package:imsindia/views/leaderboard/leaderboard_screen.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'gk_zone_current_affairs_tab.dart';
import 'gk_zone_current_affairs_tab.dart';
import 'gk_zone_gk_tab.dart';
import 'package:imsindia/utils/global.dart' as globals;

class GKZoneHomePage extends StatefulWidget {
  @override
  _GKZoneHomePageState createState() => _GKZoneHomePageState();
}

class _GKZoneHomePageState extends State<GKZoneHomePage>
    with TickerProviderStateMixin {
  //GKZoneCurrentAffairsTab entry = new GKZoneCurrentAffairsTab();
 int _currentIndex;
  List<String> tab = [];
  var tabId;
  var subcategory;
  List<Tab> tabList = [];
  TabController _tabController;
  int _index;
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  _handleTabSelection() {
    setState(() {
      _currentIndex = _tabController.index;
      print("index"+_currentIndex.toString());
    });
   // entry.overlayEntry.remove();

  }
  var userId;
  var authorization;
  var courseID;
  var generalKnowledgeSubCategory;
  var examWiseSubcategory;
  var generalKnowledgeTitle;
  var generalKnowledgeId;
  var examWiseTitle;
  var examWiseId;
  var generalKnowledgeRead;
  var generalKnowledgeTotal;
  var generalKnowledgeTestStatus;
  var generalKnowledgeTestID;

  void getCategoryList() async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    userId = prefs.getString("userId");
    authorization = prefs.getString("loginAccessToken");
    courseID = prefs.getString("courseId");
    Map postdata = {
      "userId": userId,
      "Authorization": authorization,
      "courseId":courseID
    };
    print("Post Data"+postdata.toString());
    ApiService().postAPIStatusCodeFromResponse(URL.GKZone_CategoryList, postdata, globals.headers).then((result) {
      print("Result"+result.toString());
      setState(() {
        if (result[0]['success']==true) {
          tab = [];
          tabId = [];
          tabList = [];
          // generalKnowledgeTitle = [];
          // generalKnowledgeId = [];
          // generalKnowledgeRead = [];
          // generalKnowledgeTotal = [];
          examWiseTitle = [];
          examWiseId = [];
          // generalKnowledgeTestID=[];
          // generalKnowledgeTestStatus=[];
          //print("Result value"+result[0]['data'].toString());
          for (var index = 0; index < result[0]['data'].length; index++){
            if(result[0]['data'][index]['categoryTitle'] != "SNAP"){
              tab.add(result[0]['data'][index]['categoryTitle']);
              tabId.add(result[0]['data'][index]['id']);
              //subcategory.add(result[0]['data'][index]['subCategory']);
              // if(result[0]['data'][index]['categoryTitle'] == "General Knowledge"){
              //   generalKnowledgeSubCategory = result[0]['data'][index]['subCategory'];
              // }
              if(result[0]['data'][index]['categoryTitle'] == "Exam-Wise"){
                examWiseSubcategory = result[0]['data'][index]['subCategory'];
              }
            }
          }
          for (var index = 0; index < tab.length; index++){
            tabList.add(Tab(
              child: Container(
                child: Text(
                  tab[index],
                ),
              ),
            ));
          }
          // for (var index = 0; index < generalKnowledgeSubCategory.length; index++){
          //   generalKnowledgeTitle.add(generalKnowledgeSubCategory[index]['title']);
          //   generalKnowledgeId.add(generalKnowledgeSubCategory[index]['id']);
          //   generalKnowledgeRead.add(generalKnowledgeSubCategory[index]['read']);
          //   generalKnowledgeTotal.add(generalKnowledgeSubCategory[index]['total']);
          //   generalKnowledgeTestID.add(generalKnowledgeSubCategory[index]['testId']);
          //   generalKnowledgeTestStatus.add(generalKnowledgeSubCategory[index]['testStatus']);

          // }
          for (var index = 0; index < examWiseSubcategory.length; index++){
            examWiseTitle.add(examWiseSubcategory[index]['title']);
            examWiseId.add(examWiseSubcategory[index]['id']);




          }

          print("exam wise"+examWiseId.toString());
          // print("GK read"+generalKnowledgeRead.toString());
          // print("GK total"+generalKnowledgeTotal.toString());

          print("exam wise Title"+examWiseTitle.toString());
          _tabController = new TabController(vsync: this, length: tabList.length);
          _tabController.addListener(_handleTabSelection);

        } else {

        }
      });
    });
  }
  @override
  void initState() {
    print("**************initStateHome");
//    getUserId();

    super.initState();
    //_tabController = new TabController(vsync: this, length: tabList.length);
   // _index = 0;
    setState(() {


      //this._index = _index;


      print(_index);
    });
    print("initttttt");
    getCategoryList();
  }

  Widget build(BuildContext context) {
    var _mediaQueryData = MediaQuery.of(context);
    double screenWidth = _mediaQueryData.size.width;
    double screenHeight = _mediaQueryData.size.height;
    return Scaffold(
      // key: _scaffoldKey,
//      drawer: NavigationDrawer(),
      appBar: AppBar(
        backgroundColor: Colors.white,
        elevation: 0.0,
        titleSpacing: 0.0,
        brightness: Brightness.light,
        centerTitle: false,
        iconTheme: IconThemeData(
          color: Colors.black,
        ),
        title: Text(
          GKZoneStrings.appBarLabel,
          style: TextStyle(
            fontSize: (16 / Constant.defaultScreenWidth) * screenWidth,
            fontFamily: "IBMPlexSans",
            fontWeight: FontWeight.w500,
            color: Colors.black,
          ),
          textAlign: TextAlign.left,
        ),
      ),
      body: Container(
        color: Colors.white,
        // height: 300.0 / 720 * screenHeight,
        margin: EdgeInsets.only(
            top: (0 / Constant.defaultScreenWidth) * screenWidth),
        child: Column(
          children: <Widget>[
    
            /// GK Zone , GK Runs Scores and Weekly Rank UI
            
            // Container(
            //   height: 80/720 * screenHeight,
            //   margin: EdgeInsets.only(left: 20/360 * screenWidth, top: 10/720 * screenHeight, right: 20/360 * screenWidth),
            //   decoration: BoxDecoration(
            //     gradient: LinearGradient(
            //       begin: Alignment(0, 0),
            //       end: Alignment(1.049, 1.008),
            //       stops: [
            //         0,
            //         1,
            //       ],
            //       colors: [
            //         Color.fromARGB(255, 168, 174, 35).withOpacity(0.1),
            //         Color.fromARGB(255, 201, 110, 216).withOpacity(0.1),
            //       ],
            //     ),
            //     borderRadius: BorderRadius.all(Radius.circular(5)),
            //   ),
            //   child: Row(
            //     crossAxisAlignment: CrossAxisAlignment.start,
            //     children: [
            //       Container(
            //        // width: 39/360 * screenWidth,
            //        // height: 44/720 * screenHeight,
            //         margin: EdgeInsets.only(left: 15/360 * screenHeight, top: 18/720 * screenHeight),
            //         child: Column(
            //           crossAxisAlignment: CrossAxisAlignment.start,
            //           children: [
            //             Container(
            //               margin: EdgeInsets.only(left: 14/360 * screenWidth),
            //               child: Text(
            //                 "15",
            //                 style: TextStyle(
            //                   color: NeutralColors.dark_navy_blue,
            //                   fontSize: (20 / Constant.defaultScreenWidth) * screenWidth,
            //                   fontFamily: "IBMPlexSans",
            //                   fontWeight: FontWeight.w700,
            //                 ),
            //                 textAlign: TextAlign.center,
            //               ),
            //             ),
            //             Align(
            //               alignment: Alignment.topCenter,
            //               child: Container(
            //                 margin: EdgeInsets.only( top: 4/720 * screenHeight),
            //                 child: Text(
            //                   GKZoneStrings.appBarLabel.toUpperCase(),
            //                   style: TextStyle(
            //                     color: NeutralColors.gunmetal,
            //                     fontSize: (12 / Constant.defaultScreenWidth) * screenWidth,
            //                     fontFamily: "IBMPlexSans",
            //                     fontWeight: FontWeight.w500,
            //                   ),
            //                   textAlign: TextAlign.center,
            //                 ),
            //               ),
            //             ),
            //           ],
            //         ),
            //       ),
            //       Container(
            //         margin: EdgeInsets.only(left: 10/360 * screenHeight, top: 17/720 * screenHeight),
            //         child: Opacity(
            //           opacity: 0.15,
            //           child: Container(
            //             width: 1,
            //             height: 48/720 * screenHeight,
            //             decoration: BoxDecoration(
            //               color: Color.fromARGB(255, 122, 113, 213),
            //             ),
            //             child: Container(),
            //           ),
            //         ),
            //       ),
            //       Container(
            //         width: 66/360 * screenWidth,
            //         height: showSearch == false? 44/720 * screenHeight:56/720 * screenHeight,
            //         margin: EdgeInsets.only(left: 20/360 * screenWidth, top: 18/720 * screenHeight),
            //         child: Column(
            //           children: [
            //             Text(
            //               "12",
            //               style: TextStyle(
            //                 color: NeutralColors.dark_navy_blue,
            //                 fontSize: (20 / Constant.defaultScreenWidth) * screenWidth,
            //                 fontFamily: "IBMPlexSans",
            //                 fontWeight: FontWeight.w700,
            //               ),
            //               textAlign: TextAlign.center,
            //             ),
            //             Align(
            //               alignment: Alignment.topCenter,
            //               child: Container(
            //                 margin: EdgeInsets.only(top: 4/720 * screenHeight),
            //                 child: Text(
            //                   GKZoneStrings.gk_runs.toUpperCase(),
            //                   style: TextStyle(
            //                     color: NeutralColors.gunmetal,
            //                     fontSize: (12 / Constant.defaultScreenWidth) * screenWidth,
            //                     fontFamily: "IBMPlexSans",
            //                     fontWeight: FontWeight.w500,
            //                   ),
            //                   textAlign: TextAlign.center,
            //                 ),
            //               ),
            //             ),
            //           ],
            //         ),
            //       ),
            //       Container(
            //         margin: EdgeInsets.only(left: 15/360 * screenWidth, top: 17/720 * screenHeight),
            //         child: Opacity(
            //           opacity: 0.15,
            //           child: Container(
            //             width: 1,
            //             height: 48/720 * screenHeight,
            //             decoration: BoxDecoration(
            //               color: Color.fromARGB(255, 122, 113, 213),
            //             ),
            //             child: Container(),
            //           ),
            //         ),
            //       ),
            //       Container(
            //         width: 85/360 * screenWidth,
            //         height: showSearch == false? 44/720 * screenHeight:56/720 * screenHeight,
            //         margin: EdgeInsets.only(left: 5/360 * screenWidth, top: 18/720 * screenHeight),
            //         child: Column(
            //           crossAxisAlignment: CrossAxisAlignment.center,
            //           children: [
            //             Text(
            //               "03",
            //               style: TextStyle(
            //                 color: NeutralColors.dark_navy_blue,
            //                 fontSize: (20 / Constant.defaultScreenWidth) * screenWidth,
            //                 fontFamily: "IBMPlexSans",
            //                 fontWeight: FontWeight.w700,
            //               ),
            //               textAlign: TextAlign.center,
            //             ),
            //             Align(
            //               alignment: Alignment.topCenter,
            //               child: Container(
            //                 margin: EdgeInsets.only(left: 3/360 * screenWidth, top: 4/720 * screenHeight),
            //                 child: Text(
            //                   GKZoneStrings.weekly_rank.toUpperCase(),
            //                   style: TextStyle(
            //                     color: NeutralColors.gunmetal,
            //                     fontSize: (11 / Constant.defaultScreenWidth) * screenWidth,
            //                     fontFamily: "IBM PlexSans",
            //                     fontWeight: FontWeight.w500,
            //                   ),
            //                   textAlign: TextAlign.center,
            //                 ),
            //               ),
            //             ),
            //           ],
            //         ),
            //       ),
            //     ],
            //   ),
            // ),
            
            /// Weekly Status Button and GK leaderboard Button UI
            
//             Align(
//              // alignment: Alignment.center,
//               child: Container(
//                 width: 320/360 * screenWidth,
//                 height: 40/720 * screenHeight,
//                // color: Colors.cyan,
//                 margin: EdgeInsets.only(left: 0/360 * screenWidth, top: 10/720 * screenHeight),
//                 child: Row(
//                   crossAxisAlignment: CrossAxisAlignment.start,
//                   children: [
//                     Expanded(
//                       flex:1,
//                       child: GestureDetector(
//                         onTap: (){
//                           AppRoutes.push(context, GKZoneWeeklyStats());
//                         },
//                         child: Container(
//                           width: 0/360 * screenWidth,
//                           height: 40/720 * screenHeight,
//                           decoration: BoxDecoration(
//                             border: Border.all(
//                               color: NeutralColors.gunmetal.withOpacity(0.1),
//                               width: 1,
//                             ),
//                             borderRadius: BorderRadius.all(Radius.circular(5)),
//                           ),
//                           child: Row(
//                             crossAxisAlignment: CrossAxisAlignment.center,
//                             mainAxisAlignment: MainAxisAlignment.center,
//                             children: [
//                               Container(
//                                // margin: EdgeInsets.only(left: 34/360 * screenWidth),
//                                 child: Text(
//                                   GKZoneStrings.weekly_Stats,
//                                   style: TextStyle(
//                                     color: NeutralColors.dark_navy_blue,
//                                     fontSize: (14 / Constant.defaultScreenWidth) * screenWidth,
//                                     fontFamily: "IBMPlexSans",
//                                     fontWeight: FontWeight.w500,
//                                   ),
//                                   textAlign: TextAlign.center,
//                                 ),
//                               ),
//                               Container(
//                                 width: 7/360 * screenWidth,
//                                 height: 11/720 * screenHeight,
//                                 margin: EdgeInsets.only(left: 11/360 * screenWidth),
//                                 child: Image.asset(
//                                   "assets/images/shape-copy-3-5.png",
//                                   color: Colors.black,
//                                   fit: BoxFit.contain,
//                                 ),
//                               ),
// //                            Align(
// //                              alignment: Alignment.topCenter,
// //                              child: Container(
// //                                width: 7/360 * screenWidth,
// //                                height: 11/720 * screenHeight,
// //                                margin: EdgeInsets.only(left: 5/360 * screenWidth,top: 4/720*screenHeight),
// //                                child: Container(
// //                                  margin:EdgeInsets.only(right:(0 / Constant.defaultScreenWidth) * screenWidth,
// //                                      top: (3 / Constant.defaultScreenHeight) * screenHeight),
// //                                  child: Image.asset(
// //                                    "assets/images/shape-copy-3-5.png",
// //                                    color: Colors.black,
// //                                    fit: BoxFit.contain,
// //                                  ),
// //                                ),
// //                              ),
// //                            ),
//                             ],
//                           ),
//                         ),
//                       ),
//                     ),
//                     Expanded(
//                       flex:1,
//                       child: GestureDetector(
//                         onTap: (){
//                           AppRoutes.push(context, LeaderboardScreen());
//                         },
//                         child: Container(
//                           width: 0/360 * screenWidth,
//                           height: 40/720 * screenHeight,
//                           margin: EdgeInsets.only(left: 10/360 * screenWidth),
//                           decoration: BoxDecoration(
//                             border: Border.all(
//                               color: NeutralColors.gunmetal.withOpacity(0.1),
//                               width: 1,
//                             ),
//                             borderRadius: BorderRadius.all(Radius.circular(5)),
//                           ),
//                           child: Row(
//                             crossAxisAlignment: CrossAxisAlignment.center,
//                             mainAxisAlignment: MainAxisAlignment.center,
//                             children: [
//                               Container(
//                                 margin: EdgeInsets.only(left: 0/360 * screenHeight),
//                                 child: Text(
//                                   GKZoneStrings.GK_Leaderboard,
//                                   style: TextStyle(
//                                     color: NeutralColors.dark_navy_blue,
//                                     fontSize: (14 / Constant.defaultScreenWidth) * screenWidth,
//                                     fontFamily: "IBMPlexSans",
//                                     fontWeight: FontWeight.w500,
//                                   ),
//                                   //textAlign: TextAlign.left,
//                                 ),
//                               ),
//                               Container(
//                                 width: 7/360 * screenWidth,
//                                 height: 11/720 * screenHeight,
//                                 margin: EdgeInsets.only(left: 11/360 * screenWidth),
//                                 child: Image.asset(
//                                   "assets/images/shape-copy-3-5.png",
//                                   color: Colors.black,
//                                   fit: BoxFit.contain,
//                                 ),
//                               ),
// //                            Align(
// //                              alignment: Alignment.topRight,
// //                              child: Container(
// //                                width: 7/360 * screenWidth,
// //                                height: 11/720 * screenHeight,
// //                                margin: EdgeInsets.only(left: 5/360 * screenWidth,top: 4/720*screenHeight),
// //                                child: Container(
// //                                  margin:EdgeInsets.only(right:(0 / Constant.defaultScreenWidth) * screenWidth,
// //                                      top: (3 / Constant.defaultScreenHeight) * screenHeight),
// //                                  child: Icon(
// //                                    Icons.keyboard_arrow_right,
// //                                  ),
// //                                ),
// //                              ),
// //                            ),
//                             ],
//                           ),
//                         ),
//                       ),
//                     ),
//                   ],
//                 ),
//               ),
//             ),

            tabList.length == 0
                ? new Container(
                margin: EdgeInsets.only(
                    top: (200.0 / 720) * screenHeight),
                child:
                Center(child: CircularProgressIndicator())):
            Expanded(
              child: Container(
                color: Colors.white,
                child: Column(
                  children: <Widget>[
                    Container(
                     height: 42/720*screenHeight,
              //color: Colors.white,
              margin: EdgeInsets.only(
                  left: 20 / 360 * screenWidth,
                  right: 20 / 360 * screenWidth,

                  top: (20 / Constant.defaultScreenHeight) * screenHeight),
                  decoration: BoxDecoration(
                  color: Colors.white,
                          boxShadow: [
                       BoxShadow(
                        //color: Colors.red,
                        color :Color(0x0d979797),
                        offset: Offset(0, 8),
                        spreadRadius: 2,
                        blurRadius: 16,
                      ),
                    ],
                  ),
              child: TabBar(
                //Opacity: 1.0,
                controller: _tabController,
                indicatorColor: NeutralColors.purpleish_blue,
                labelColor: NeutralColors.purpleish_blue,
                unselectedLabelColor: NeutralColors.blue_grey,
                indicatorSize: TabBarIndicatorSize.tab,
                tabs: tabList,
                indicatorPadding: EdgeInsets.only(
                    left: 0.0 / 360 * screenWidth,
                    right: 0.0 / 360 * screenWidth
                  ),
                isScrollable: true,
                unselectedLabelStyle: TextStyle(
                  fontSize: 14 / 360 * screenWidth,
                  fontFamily: "IBMPlexSans",
                  fontWeight: FontWeight.w400,
                ),
                labelStyle: TextStyle(
                  fontSize: 14 / 360 * screenWidth,
                  fontFamily: "IBMPlexSans",
                  fontWeight: FontWeight.w500,
                ),
              ),
            ),
                    Expanded(
                        child: Container(
                          child: TabBarView(
                        controller: _tabController,
                        children: [
                          GKZoneGKTab(),
                          GKZoneCurrentAffairsTab(tabId[1]),
                          GKZoneExamWiseTab(examWiseTitle,examWiseId)
                        ],
                      ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

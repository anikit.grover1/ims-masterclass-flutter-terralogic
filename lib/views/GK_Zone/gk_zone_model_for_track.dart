class GkzoneModelClass {
//  final int id;
//  final String name;
//  final int age;
//
//  Dog({this.id, this.name, this.age});

  final String areaName;
  final bool attempt;
  final String companyCode;
  final int difficultyId;
  final String difficultyName;
  final enteredText;
  final String id;
  final bool isCorrect;
  final String isSectionCompleted;
  final int itemId;
  final String itemType;
  final bool marked;
  final int negativePoints;
  final int points;
  final int questionIndex;
  final bool saved;
  final int sectionId;
  final int sectionIndex;
  final String sectionName;
  final int selectedOptionId;
  final String studentId;
  final int subjectId;
  final String subjectName;
  final String testId;
  final int timeRemaining;
  final String timeStamp;
  final int timeTaken;
  final int topicId;
  final String topicName;
  final int trackNumber;

  GkzoneModelClass({this.areaName, this.attempt, this.companyCode, this.difficultyId,this.difficultyName,
    this.enteredText,this.id,this.isCorrect,this.isSectionCompleted,this.itemId,this.itemType,this.marked,
    this.negativePoints,this.points,this.questionIndex,this.saved,this.sectionId
    ,this.sectionIndex,this.sectionName,this.selectedOptionId,this.studentId,this.subjectId,this.subjectName,
    this.testId,this.timeRemaining,this.timeStamp,this.timeTaken,this.topicId,this.topicName
    ,this.trackNumber});

  Map<String, dynamic> toMap() {
    return {
      "areaName": areaName,
      "attempt": attempt,
      "companyCode": companyCode,
      "difficultyId": difficultyId,
      "difficultyName": difficultyName,
      "enteredText": enteredText,
      "id": id,
      "isCorrect": isCorrect,
      "isSectionCompleted": isSectionCompleted,
      "itemId": itemId,
      "itemType": itemType,
      "marked": marked,
      "negativePoints": negativePoints,
      "points": points,
      "questionIndex": questionIndex,
      "saved":saved,
      "sectionId": sectionId,
      "sectionIndex": sectionIndex,
      "sectionName": sectionName,
      "selectedOptionId": selectedOptionId,
      "studentId": studentId,
      "subjectId": subjectId,
      "subjectName": subjectName,
      "testId": testId,
      "timeRemaining": timeRemaining,
      "timeStamp": timeStamp,
      "timeTaken": timeTaken,
      "topicId": topicId,
      "topicName": topicName,
      "trackNumber": trackNumber
    };
  }

  // Implement toString to make it easier to see information about
  // each dog when using the print statement.
  @override
  String toString() {
    return 'GkzoneModelClass data { areaName: $areaName, attempt: $attempt,companyCode: $companyCode,'
        'difficultyId: $difficultyId,difficultyName: $difficultyName,enteredText: $enteredText,id: $id,'
        'isCorrect: $isCorrect,isSectionCompleted: $isSectionCompleted,itemId: $itemId,itemType: $itemType,'
        'marked: $marked,negativePoints: $negativePoints,points: $points,questionIndex: $questionIndex,saved:$saved'
        'sectionId: $sectionId,sectionIndex: $sectionIndex,sectionName: $sectionName,selectedOptionId: $selectedOptionId,'
        'studentId: $studentId,subjectId: $subjectId,subjectName: $subjectName,testId: $testId,'
        'timeRemaining: $timeRemaining,timeStamp: $timeStamp,timeTaken: $timeTaken,'
        'topicId: $topicId,topicName: $topicName,trackNumber: $trackNumber,}';
  }
}

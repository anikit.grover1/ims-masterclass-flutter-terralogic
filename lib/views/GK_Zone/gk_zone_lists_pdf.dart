import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:imsindia/components/error_screen.dart';
import 'package:imsindia/utils/colors.dart';
import 'package:imsindia/utils/screen_util.dart';
import 'package:imsindia/utils/svg_images/gk_zone_svg_images.dart';
import 'package:imsindia/utils/svg_images/prepare_svg_images.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:imsindia/routers/routes.dart';
import 'package:imsindia/views/GDPI/GDPI_PDFscreen.dart';
import 'package:imsindia/utils/svg_images/GDPI_svg_images.dart';
import 'package:imsindia/utils/global.dart' as global;
import 'package:imsindia/api/bloc/services.dart';
import 'package:imsindia/utils/Url.dart';
import 'package:imsindia/views/GK_Zone/gk_zone_PdfScreen.dart';
import 'package:shared_preferences/shared_preferences.dart';


const actualHeight = 768;
const actualWidth = 360;


class SavePdfRecords {
  int catId;
  int current;
  int postId;
  int parentId;
  int status;

  SavePdfRecords(this.catId, this.current, this.postId, this.parentId, this.status);

  SavePdfRecords.fromJson(Map<String, dynamic> json)
      : catId = json['catId'],
        current = json['current'],
        postId = json['postId'],
        parentId = json['parentId'],
        status = json['status'];

  Map<String, dynamic> toJson() {
    return {
      'catId': catId,
      'current': current,
      'postId': postId,
      'parentId': parentId,
      'status': status,
    };
  }
//  @override
//  String toString() {
//    return 'Student: {postId: ${postId}, current: ${current}}';
//  }
}

class GKZoneListOfPdf extends StatefulWidget{
  var CategoryId;
  GKZoneListOfPdf(this.CategoryId);

  @override
  GKZoneListOfPdfState createState() => GKZoneListOfPdfState();
}

class GKZoneListOfPdfState extends State<GKZoneListOfPdf>{
  int _activeMeterIndex;
  List<dynamic> scorecardImage = [
    getGDPISvgImages.play,
    getGDPISvgImages.practice,
    getGDPISvgImages.practice,
    getGDPISvgImages.practice,
  ];



  Future _future;
  var totalDataForReadPdf;
  var totalDataForSavePdf;
  List savePdfRecords = new List();
  var userId;
  var authorization;
  var courseID;





  Future readPdfData() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    userId = prefs.getString("userId");
    authorization = prefs.getString("loginAccessToken");
    courseID = prefs.getString("courseId");
    Map<String, dynamic> postData;
    postData = {
      "userId":userId,
      "categoryId":widget.CategoryId,
      "courseId":courseID,
      "Authorization":authorization
    };
print("+zzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz");
    print(postData);
    final result = await ApiService().postAPIStatusCodeFromResponse(URL.GKZONE_READPDF, postData, global.headers);
    if (result[0]['success']==true){
      setState(() {
        print(totalDataForReadPdf.runtimeType);
        totalDataForReadPdf=result[0]['data'];
        for (var i=0;i<totalDataForReadPdf.length;i++){
          savePdfRecords.add(
              json.decode(jsonEncode(SavePdfRecords(totalDataForReadPdf[i]['catId'],totalDataForReadPdf[i]['current'],totalDataForReadPdf[i]['postId'],totalDataForReadPdf[i]['parentId'],totalDataForReadPdf[i]['status'])))
          );
        }
        print("total records");
        print(savePdfRecords);
      });

      return totalDataForReadPdf;
    }
    else{
      // totalDataForBookings = result[0]['message'];
      if(result[0].containsKey("message")){
        if(result[0]['message']=='No Record found'){
          totalDataForReadPdf = result[0]['message'];
        }else{
          totalDataForReadPdf=result[0]['message'];
        }
      }else{
        totalDataForReadPdf=result[0];
      }

      return totalDataForReadPdf;
    }

  }
  void showMessage(String inputValue) {
    Fluttertoast.showToast(
      msg: inputValue,
      toastLength: Toast.LENGTH_SHORT,
      gravity: ToastGravity.BOTTOM,
      timeInSecForIosWeb: 1,
      backgroundColor: PrimaryColors.azure_Dark,
      textColor: Colors.white,
      fontSize: 14.0,
    );
  }

   savePdfRecord() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    userId = prefs.getString("userId");
    authorization = prefs.getString("loginAccessToken");
    courseID = prefs.getString("courseId");
    Map<String, dynamic> postData;
    List finalSavePdfRecords = new List();
    print(finalSavePdfRecords);
    print("finalSavePdfRecords beore");
    for(var i=0;i<savePdfRecords.length;i++){
      if(savePdfRecords[i]['status']==1){
        print("coming here");
        finalSavePdfRecords.add(savePdfRecords[i]);
      }else{

      }
    }
    print(finalSavePdfRecords);
    print("finalSavePdfRecords after");
    postData = {
      "userId":userId,
      "courseId":courseID,
      "Authorization":authorization,
      "pdfRecords":finalSavePdfRecords
    };
    print("+zzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz");
    print(postData);
    final result = await ApiService().postAPIStatusCodeFromResponse(URL.SAVE_PDF_RECORDS, postData, global.headers);
    if (result[0]['success']==true){
      if (this.mounted) {
        setState(() {
          print("comingf here");
          totalDataForSavePdf = result[0]['message'];
          print("after comingf here");
          Navigator.pop(context,true);
        });
      }
      return totalDataForSavePdf;
    }
    else{
      // totalDataForBookings = result[0]['message'];
      if(result[0].containsKey("message")){
        if(result[0]['message']=='No Record found'){
          totalDataForSavePdf = result[0]['message'];
        }else{
          totalDataForSavePdf=result[0]['message'];
        }
      }else{
        totalDataForSavePdf=result[0];
      }
    //  showMessage(totalDataForSavePdf.toString());
      Navigator.pop(context,true);
      return totalDataForSavePdf;
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    print("jhhhhhhhhhhhhhhhhhhhhhhhhhhh");
   // ReadPdfData();
    _future = readPdfData();

  }

  Widget build(BuildContext context) {
    final screenHeight = MediaQuery.of(context).size.height;//720dp
    final screenWidth = MediaQuery.of(context).size.width;//360dp
    // TODO: implement build
    Future<bool> _onWillPop() {
      setState(() {
        savePdfRecord();
      });
    }
    return  WillPopScope(
      onWillPop: _onWillPop,
      child: Scaffold(
        body: Container(
          margin: EdgeInsets.only(
            top: 0 / Constant.defaultScreenHeight * screenHeight,
          ),
          child: Column(
            children: <Widget>[
              Container(
                margin: EdgeInsets.only(
                  top: (42 / 720) * screenHeight,
                ),
                child: Row(
                  //crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    InkWell(
                      onTap: () {
                        savePdfRecord();
                      },
                      child: Container(
                        width: (58 / 360) * screenWidth,
                        height: (30.0 / 720) * screenHeight,
                        margin: EdgeInsets.only(left: 0.0),
                        child: SvgPicture.asset(
                          getPrepareSvgImages.backIcon,
                          height: (5 / 720) * screenHeight,
                          width: (14 / 360) * screenWidth,
                          fit: BoxFit.none,
                        ),
                      ),
                    ),
                    Container(
                      height: (20 / 720) * screenHeight,
                      // margin: EdgeInsets.only(left: (20 / 360) * screenWidth),
                      child: Text(
                        "GK Zone",
                        style: TextStyle(
                          color: NeutralColors.black,
                          fontSize: (16 / 720) * screenHeight,
                          fontFamily: "IBMPlexSans",
                          fontWeight: FontWeight.w500,
                        ),
                        textAlign: TextAlign.left,
                      ),
                    ),
                  ],
                ),
              ),
              FutureBuilder(
              builder: (context, snapshot) {
                  if (snapshot.hasError) {
                      return Container(); // widget to be shown on any error
                      }
                  return snapshot.hasData
                     ?
                  totalDataForReadPdf.length>0?Expanded(
                  child: Container(
                    margin: EdgeInsets.only(left: 20/360 * screenWidth, right: 20/360 * screenWidth, top: 20/720 * screenHeight),
                    child: ListView.builder(
                      itemCount: totalDataForReadPdf.length,
                      shrinkWrap: true,
                      physics: NeverScrollableScrollPhysics(),
                      padding: EdgeInsets.only(bottom: (40)),
                      itemBuilder: (context, index) {
                        final dataForReadPdf = totalDataForReadPdf[index];

                        return Container(
                          height: (45/720)*screenHeight,
                          width: (320/360)*screenWidth,
                          margin: EdgeInsets.only(top: (10/720)*screenHeight),
                          decoration: BoxDecoration(
                            border: Border.all(
                              color: NeutralColors.gunmetal.withOpacity(0.10),
                            ),
                            borderRadius: BorderRadius.all(
                                Radius.circular(5)
                            ),
                            //color: NeutralColors.ice_blue,
                          ),
                          child: InkWell(
                            onTap: (){
                              print("====================================selected index");
                              print(index);
                              print(dataForReadPdf['URL']);
                              print("gggggggggggggggggggggggggggggggggggg");
                              Navigator.of(context).push(new MaterialPageRoute(builder: (_) => GKZonePdfScreen(dataForReadPdf['URL']))).then((value) {
                                if (value == true) {
                                  setState(() {
                                   for(var i=0;i<savePdfRecords.length;i++){
                                     if(savePdfRecords[i]['postId']==dataForReadPdf['postId']){
                                       savePdfRecords[i]['current']=1;
                                       savePdfRecords[i]['status']=1;
                                     }else{
                                       savePdfRecords[i]['current']=0;
                                     }
                                   }
                                   print(savePdfRecords);
                                   print("save pfd recores");
                                  });
                                }
                              });
                              // widget.referenceType[index] == "Video" ?
                              //     AppRoutes.pushWithAnmation(context, WebViewExample_GDPI(widget.vimeoURI[index],widget.video_id[index],userId)) :
                              //For pdf view have to modify
                              // global.getToken.then((t){
                              //   ReadPdfData(ID[index]);
                              // });

                            },
                            child: Row(
                              children: <Widget>[
                                Container(
                                  height:(20/720)*screenHeight,
                                  width: (20/360)*screenWidth,
                                  margin: EdgeInsets.only(top: (13/720)*screenHeight,left: (15/360)*screenWidth,bottom: (12/720)*screenHeight),
                                  child: (savePdfRecords[index]['current'] ==1)
                                      ? new SvgPicture.asset(
                                    GKZoneAssets.selectedSvg,
                                    fit: BoxFit.scaleDown,
                                    color: PrimaryColors.azure_Dark,
                                  ) : (savePdfRecords[index]['status'] ==1)
                                      ? new SvgPicture.asset(
                                    GKZoneAssets.insideTickSvg,
                                    fit: BoxFit.scaleDown,
                                    color: PrimaryColors.azure_Dark,
                                  ) :  new SvgPicture.asset(
                                    GKZoneAssets.notCompleted,
                                    fit: BoxFit.scaleDown,
                                    color: PrimaryColors.azure_Dark,
                                  ),
                                ),
                                Container(
                                  //height:(23/720)*screenHeight,
                                  width: 236/360 * screenWidth,
                                  margin: EdgeInsets.only(top: (11/720)*screenHeight,left: (10/360)*screenWidth,bottom: (11/720)*screenHeight),
                                  child: Text(
                                    dataForReadPdf['pdfTitle'].toString(),
                                    // pdfTitle[index],
                                    // widget.title[index],
                                    style: TextStyle (
                                      color: NeutralColors.dark_navy_blue,
                                      fontWeight: FontWeight.w500,
                                      fontFamily: "IBMPlexSans",
                                      fontStyle: FontStyle.normal,
                                      fontSize: (14/360)*screenWidth,
                                    ),
                                    textAlign: TextAlign.left,
                                  ),
                                ),
                                Container(
                                  margin: EdgeInsets.only(left: (15/360)*screenWidth),
                                  child: InkWell(
                                    onTap: (){},
                                    child: Icon(
                                      Icons.arrow_forward_ios,
                                      color: NeutralColors.black,
                                      size: 11/720*screenHeight,
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        );
                      },
                    ),
                  ),
                ):
                  Container(
                    margin: EdgeInsets.only(
                      bottom: 40 / actualHeight * screenHeight,
                    ),
                    child: Column(
                      children: [
                        Container(
                          margin: EdgeInsets.only(
                            top: 40/actualHeight*screenWidth,
                            bottom: 20.0/actualHeight*screenHeight,
                          ),
                          child: Text(
                            "GK Zone",
                            style: TextStyle(color:NeutralColors.black,
                              fontFamily: "IBMPlexSans",
                              fontStyle: FontStyle.normal,
                              fontSize: 18.0/actualWidth*screenWidth,
                              fontWeight: FontWeight.w700,),
                          ),
                        ),
                        ErrorScreen(),
                      ],
                    ),
                  )
                : Container(
                    margin: EdgeInsets.only(top: 300/actualHeight*screenHeight),
                    child: Center(child: CircularProgressIndicator(),
                    ),); // widget to be shown while data is being loaded from api call method
                },
               future: _future,
                )
            ],
          ),
        ),
      ),
    );
  }
}




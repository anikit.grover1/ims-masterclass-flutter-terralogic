import 'dart:core';

import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:imsindia/utils/colors.dart';
import 'package:imsindia/utils/screen_util.dart';
import 'package:imsindia/utils/emaximiser_svg_icons.dart';
import 'package:imsindia/views/login_page/login_work_experience.dart';



class TestData {
  String heading;
  String status;

  TestData({
    this.heading,
    this.status,
   
  });
}

class Week5Steps extends StatefulWidget{
  @override
  Week5StepsState createState() => Week5StepsState();

}
class Week5StepsState extends State<Week5Steps>{
  @override
  Widget build(BuildContext context) {
    var _mediaQueryData = MediaQuery.of(context);
    screenWidth = _mediaQueryData.size.width;
    screenHeight = _mediaQueryData.size.height;

     final intro = TestData(
        heading: "Important Dates",
         status: "completed",

    );

     final catchup = TestData(
        heading: "In the News",
       status: "in-progress",

    );

     final test1 = TestData(
        heading: "Awards and Honours",
       status: "start",

    );
    final test2 = TestData(
        heading: "in-progress",
      status: "start",

    );
    final test3 = TestData(
        heading: "Sports",
      status: "start",

    );
    final test4 = TestData(
        heading: "Business and Economy",
      status: "start",

    );
    final test5 = TestData(
        heading: "National Affairs",
      status: "start",

    );






    final List<TestData> testdata = [
      intro,
      catchup,
      test1,
      test2,
      test3,
      test4,
      test5,
    ];



    return Scaffold(
      appBar: new AppBar(
        bottomOpacity: 0.0,
        brightness: Brightness.light,
        elevation: 0.0,
        centerTitle: false,
        titleSpacing: 0.0,
        iconTheme: IconThemeData(
          color: Colors.black, //change your color here
        ),
        backgroundColor: Color.fromARGB(255, 255, 255, 255),

        leading: IconButton(
          icon: getSvgIcon.backSvgIcon,
          onPressed: () => Navigator.pop(context, false),
        ),
        title: new Text(
          "Week 5 - Sept",
          style: TextStyle(
            color: Color.fromARGB(255, 0, 0, 0),
            fontSize: screenHeight * 16 / 720,
            fontFamily: "IBMPlexSans",
            fontWeight: FontWeight.w500,
          ),
          textAlign: TextAlign.left,
        ),),
      body: SingleChildScrollView(
        child: Container(

         // constraints: BoxConstraints.expand(),
          decoration: BoxDecoration(
            color: Color.fromARGB(255, 255, 255, 255),
          ),

          child: Column(
            children: <Widget>[

              Container(
                //color: Colors.blue,

                margin: EdgeInsets.only(top: 15/720*screenHeight,bottom: 00/720*screenHeight),
                height:300/720*screenHeight,
//                width: 360/360*screenWidth,

                child: ListView.builder(
                  //physics: NeverScrollableScrollPhysics(),
                  padding: EdgeInsets.all(0),
                  itemCount: testdata.length,
                  itemBuilder: (context,index){
                    final item = testdata[index];
                    return Container(
//                      decoration: item.status== 'completed'? BoxDecoration(
//                          boxShadow: [new BoxShadow(
//                            //color: NeutralColors.blue_grey,
//                            blurRadius: 20.0,
//                          ),]
//
//                      ):'',
                      color: item.status== 'in-progress'? NeutralColors.ice_blue
                          :item.status== 'completed'? NeutralColors.ice_blue.withOpacity(0.40) :Colors.transparent,
                      height: 40/720*screenHeight,
//                      margin: EdgeInsets.only( top: 18/720*screenHeight,bottom: 18/720*screenHeight),
                      child: Row(
                        children: [
                          Container(

                            margin: EdgeInsets.only(left: 20/360*screenWidth,right: 20/360*screenWidth),
                            child: Text(
                              item.heading,
                              style: TextStyle(
                                color: item.status=='completed'?Colors.black
                                    :item.status=='in-progress'?NeutralColors.purpleish_blue
                                :item.status=='start'?AccentColors.blueGrey:Colors.white,
                                fontSize: 16/720*screenHeight,
                                fontFamily: "IBMPlexSans",
                                fontWeight: FontWeight.w500,
                              ),
                              textAlign: TextAlign.left,
                            ),
                          ),
                          Spacer(),
                          Align(
                            alignment: Alignment.center,
                            child: Container(
//                              width: 19/360*screenWidth,
//                              height: 19/720 *screenHeight,
                              margin: EdgeInsets.only(right : 15),
                              child: new SvgPicture.asset(
                                item.status== 'completed'?"assets/images/tick.svg"
                                    :item.status=='in-progress'?"assets/images/selected.svg"
                                    :item.status=='start'?"assets/images/primary_lock.svg":"",
                                fit: BoxFit.scaleDown,
                                //color: Colors.red,
                              ),
                            ),
                          ),
                        ],
                      ),
                    );
                  },
                ),


              ),
              Container(
                //color: Colors.blue,
                child: Divider(),
                margin: EdgeInsets.only( bottom: 24/5/720*screenHeight),

              ),
              Container(
                //color: Colors.red,
//                height: 151/720*screenHeight,
//                width: 360/360*screenWidth,
                margin: EdgeInsets.only(left: 20/360*screenWidth,right: 20/360*screenWidth),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Container(
//                      height: 23/720*screenHeight,
//                      width: 290/360*screenWidth,
                      margin: EdgeInsets.only(top: 24/720*screenHeight),
                      child: Text (
                        "Abbreviations in News",
                        style: TextStyle(
                          color: NeutralColors.dark_navy_blue,
                          fontSize: 18/360*screenWidth,
                          fontFamily: "IBMPlexSans",
                          letterSpacing: 0,
                          fontWeight: FontWeight.w700,

                        ),
                      ),
                    ),
                    Container(
//                      height: 52/720*screenHeight,
//                      width: 320/360*screenWidth,
                      margin: EdgeInsets.only(top: 12/720*screenHeight),
                      child: Text (
                        "NARI: National Repository of Information for Women",
                        style: TextStyle(
                          color: NeutralColors.dark_navy_blue,
                          fontSize: 14/360*screenWidth,
                          fontFamily: "IBMPlexSans",
                          letterSpacing: 0,
                          fontWeight: FontWeight.w500,

                        ),
                      ),
                    ),
                    Container(
//                      height: 52/720*screenHeight,
//                      width: 320/360*screenWidth,
                      margin: EdgeInsets.only(top: 10/720*screenHeight),
                      child: Text (
                        "ICET: International Center for Entrepreneurship and Technology",
                        style: TextStyle(
                          color: NeutralColors.dark_navy_blue,
                          fontSize: 14/360*screenWidth,
                          fontFamily: "IBMPlexSans",
                          letterSpacing: 0,
                          fontWeight: FontWeight.w500,

                        ),
                      ),
                    ),

                    Container(
//                      height: 23/720*screenHeight,
//                      width: 290/360*screenWidth,
                      margin: EdgeInsets.only(top: 38/720*screenHeight),
                      child: Text (
                        "Books in News",
                        style: TextStyle(
                          color: NeutralColors.dark_navy_blue,
                          fontSize: 18/360*screenWidth,
                          letterSpacing: 0,
                          fontFamily: "IBMPlexSans",
                          fontWeight: FontWeight.w800,

                        ),
                      ),
                    ),
                    Container(
                      child: Row(
                        children: <Widget>[
                          Container(
                            //margin: EdgeInsets.only(left: 20/360*screenWidth),
                            //color: Colors.red,
                            height: 156/720*screenHeight,
                            width: 210/360*screenWidth,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Container(
                                  height:70/720*screenHeight,
                                  margin: EdgeInsets.only(top: 12/720*screenHeight),
                                  child: Text(
                                    "Skills in mathematics Play with Graphs for JEE Main and Advanced",
                                    style: TextStyle(
                                      color: NeutralColors.dark_navy_blue,
                                      fontSize: 14/360*screenWidth,
                                      fontFamily: "IBMPlexSans",
                                      letterSpacing: 0,
                                      fontWeight: FontWeight.w500,

                                    ),
                                  ),

                                ),
                                Container(
                                  //margin: EdgeInsets.only(right: 25/360*screenWidth),
                                  child: Text(
                                    "by Arihant Datt | 3 June 2019",
                                    style: TextStyle(
                                      color: NeutralColors.bluey_grey,
                                      fontSize: 14/360*screenWidth,
                                      letterSpacing: 0,
                                      fontFamily: "IBMPlexSans",
                                      fontWeight: FontWeight.w500,

                                    ),
                                  ),
                                )
                              ],
                            ),
                          ),

                          Container(
                            margin: EdgeInsets.only(left: 10/360*screenWidth,bottom: 20/720*screenHeight),
                            //color: Colors.yellow,
                            height: 133/720*screenHeight,
                            width: 100/360*screenWidth,
                            child: Image.asset(
                              "assets/images/bitmap.png",
                              fit: BoxFit.scaleDown,
                            ),
                          ),
                        ],
                      ),
                    ),




                  ],

                ),
              ),
//              Container(
//               // color: Colors.blue,
//                //height: 180/720*screenHeight,
//                margin: EdgeInsets.only(left: 20/360*screenWidth),
//                child: Column(
//                  crossAxisAlignment: CrossAxisAlignment.start,
//                  children: <Widget>[
//
//
//
//                  ],
//                ),
//              ),
              Container(
               // height: 50/720*screenHeight,
               // color: Colors.red,
                margin: EdgeInsets.only(top: 17/720*screenHeight,bottom: 38/720*screenHeight),
                child: Row(
                  children: <Widget>[
                    GestureDetector(
//                      onTap: () { Navigator.of(context).pop();
//                      AppRoutes.push(context, Channel_quicktips());
//
//                      },
                      onTap: () => Navigator.pop(context, false),
                      child: Container(
                        margin: EdgeInsets.only(left: 43/360*screenWidth),
                        height:(40/Constant.defaultScreenHeight)*screenHeight,
                        width: (130/Constant.defaultScreenWidth)*screenWidth,
                        decoration: BoxDecoration(
                          color: NeutralColors.ice_blue,
                          borderRadius: BorderRadius.all(Radius.circular(2)),
                        ),
                        child: Center(
                          child: Text(
                            "Back",
                            style: TextStyle(
                              fontWeight:FontWeight.w500,
                              fontFamily: "IBMPlexSans",
                              color: NeutralColors.blue_grey,
                              fontSize: 14/720*screenHeight,

                            ),
                          ),
                        ),
                      ),
                    ),

                    GestureDetector(
//                      onTap: () {
//                        Navigator.of(context).pop();
//                        AppRoutes.push(context, ChannelReplies());
//
//                      },
                      child: Container(
                        margin: EdgeInsets.only(left: (15/Constant.defaultScreenWidth)*screenWidth,right: 42/360*screenWidth),
                        height:(40/Constant.defaultScreenHeight)*screenHeight,
                        width: (130/Constant.defaultScreenWidth)*screenWidth,
                        decoration: BoxDecoration(
                          color: PrimaryColors.azure_Dark,
                          borderRadius: BorderRadius.all(Radius.circular(2)),
                        ),
                        child: Center(
                          child: Text(
                            "Next",
                            style: TextStyle(
                              fontWeight:FontWeight.w500,
                              fontFamily: "IBMPlexSans",
                              color: Colors.white,
                              fontSize: 14/720*screenHeight,

                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),

              )
            ],

          ),
        ),
      ),
    );
  }

}
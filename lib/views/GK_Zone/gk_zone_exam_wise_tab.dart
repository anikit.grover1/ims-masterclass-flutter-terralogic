import 'package:flutter/material.dart';
import 'package:imsindia/api/bloc/services.dart';
import 'package:imsindia/components/custom_dropdown_gk_zone.dart';
import 'package:imsindia/resources/strings/gk_zone.dart';
import 'package:imsindia/routers/routes.dart';
import 'package:imsindia/utils/Url.dart';
import 'package:imsindia/utils/colors.dart';
import 'package:imsindia/utils/screen_util.dart';
import 'package:imsindia/utils/svg_images/gk_zone_svg_images.dart';
import 'package:imsindia/views/GK_Zone/gk_zone_quiz_screen.dart';
import 'package:imsindia/views/GK_Zone/gk_zone_week5_sept.dart';
import 'package:percent_indicator/circular_percent_indicator.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:imsindia/utils/global.dart' as global;

import 'gk_zone_lists_pdf.dart';
import 'gk_zone_question_review_screen.dart';
import 'gk_zone_question_screen.dart';

final titles = [
  'Quiz 1',
  'Quiz 2',
  'Quiz 3',
];
bool value = false;
GlobalKey _keyRed = GlobalKey();
GlobalKey _keyRed1 = GlobalKey();

List dropdowntitles = [
  'CAT',
  'TISS',
  'CIT',
  'IEART',
];

final icons = [
  GKZoneAssets.scienceSvgIcon,
  GKZoneAssets.geographySvgIcon,
  GKZoneAssets.historySvgIcon,
];
var selectedExam;
var selectedId;
bool buttonClickOnlyOnce = false;
bool loadForReviewAndResumeButoon = false;
var authToken;

class GKZoneExamWiseTab extends StatefulWidget {
  var titles;
  var id;


  GKZoneExamWiseTab(this.titles,this.id);
  @override
  _GKZoneExamWiseTabState createState() => _GKZoneExamWiseTabState();
}

class _GKZoneExamWiseTabState extends State<GKZoneExamWiseTab> {


  final TextEditingController _sample = new TextEditingController();
  getValuesSF() async {
    print('****getValuesSF');

    SharedPreferences prefs = await SharedPreferences.getInstance();

    if (prefs.getString('selectedVal') != null) {
      print('****prefs');

      String gotValue = prefs.getString('selectedVal');
      setState(() {
        _labelValue = gotValue;
      });
    } else {
      print('****else');

      setState(() {
        _labelValue = "Select Course";
      });
    }

  }
  var userId;
  var authorization;
  var courseID;
  var subCategoryTitle;
  var subCategoryId;
  var showReadButtonCheck;
  var read;
  var total;
  var isLoading = true;
  var totaldata;
  var examwiseID = [];
  var pdfid;
  var testId;
  var testStatus;

  void getExamWiseList(int selectedId) async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    userId = prefs.getString("userId");
    authorization = prefs.getString("loginAccessToken");
    courseID = prefs.getString("courseId");
    Map postdata = {
      "userId": userId,
      "categoryId":selectedId,
      "Authorization": authorization,
      "courseId":courseID
    };
    print("Post Data"+postdata.toString());
    ApiService().postAPIStatusCodeFromResponse(URL.GKZone_Examwise, postdata, global.headers).then((result) {
      print("Result"+result.toString());
      setState(() {
        if (result[0]['success']==true) {
          isLoading = true;
          subCategoryTitle = [];
          subCategoryId = [];
          read = [];
          total = [];
          testId=[];
          testStatus=[];
          showReadButtonCheck = [];
          totaldata = result[0]['data'];
          for (var index = 0; index < result[0]['data'].length; index++){
            if(result[0]['data'][index]['categoryTitle'] != "SNAP"){
              subCategoryTitle.add(result[0]['data'][index]['subCategory']);
              subCategoryId.add(result[0]['data'][index]['id']);
              read.add(result[0]['data'][index]['read']);
              total.add(result[0]['data'][index]['total']);
              testId.add(result[0]['data'][index]['testId']);
              testStatus.add(result[0]['data'][index]['testStatus']);
              showReadButtonCheck.add(result[0]['data'][index]['showReadButton']);
            }
            examwiseID.add(result[0]['data'][index]['id']);
            pdfid =(result[0]['data'][index]['id']);
          }
          isLoading = false;
          print(subCategoryTitle);
          print(subCategoryId);
          print(read);
          print(total);
          print(testId);
          print(testStatus);


        } else {

        }
      });
    });
  }
  /// set access token to prod test launch/reveiw screen...............
  void setAccessTokenForProdTestLaunch(value) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString("accessTokenGkzone", value);
  }

  void setTestStatusToCallTestLaunchOrReviseApi(value) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString("testStatusgkzone", value);
    print(value + "valueeeeeeeeeee");
    print(prefs.setString("testStatusgkzone", value));
  }

  @override
  void initState() {
    if(selectedExam!=null) {
      print("**************selectedExam" + selectedExam.toString());
    }else{
      getExamWiseList(widget.id[0]);
    }

    super.initState();
  }
  String _labelValue = 'Select Course';
  bool isClicked = false;
  @override
  Widget build(BuildContext context) {
    var _mediaQueryData = MediaQuery.of(context);
    double screenWidth = _mediaQueryData.size.width;
    double screenHeight = _mediaQueryData.size.height;
    return GestureDetector(
      onTap: () {},
      child: Scaffold(
        backgroundColor: Colors.transparent,
        body: Container(
          //color: Colors.white,
          child: Column(
            children: <Widget>[
              Container(
                  margin: EdgeInsets.only(
                      left: (21 / Constant.defaultScreenWidth) * screenWidth,
                      top: (29 / Constant.defaultScreenHeight) * screenHeight,
                      bottom:
                          (8 / Constant.defaultScreenHeight) * screenHeight),
                  child: Row(
                    children: <Widget>[
                      Container(
                        margin: EdgeInsets.only(
                            right: (15 / Constant.defaultScreenWidth) *
                                screenWidth),
                        child: Text(
                          GKZoneStrings.select_Exam,
                          style: TextStyle(
                            fontWeight: FontWeight.w500,
                            fontSize: (14 / Constant.defaultScreenWidth) *
                                screenWidth,
                            fontFamily: "IBMPlexSans",
                            color: NeutralColors.dark_navy_blue,
                          ),
                        ),
                      ),
                      Container(
                        width: 160 / 360 * screenWidth,
                        height: 40 / 720 * screenHeight,
                        decoration: BoxDecoration(
                          borderRadius: new BorderRadius.circular(5.0),
                          border: Border.all(
                              color: NeutralColors.gunmetal.withOpacity(0.15)),
                        ),
                        child: DropDownFormField(
                         // selected_month: selectedExam,
                          getImmediateSuggestions: true,
                          // autoFlipDirection: true,
                          textFieldConfiguration: TextFieldConfiguration(
                            controller: _sample,
                            label: selectedExam!=null ? selectedExam : widget.titles[0] ,
                          ),
                          // suggestionsBoxController:sboxcontroller,
                          suggestionsBoxDecoration: SuggestionsBoxDecoration(
                              dropDownHeight: 140/720*screenHeight,
                              borderRadius: new BorderRadius.circular(5.0),
                              color: Colors.white),
                          suggestionsCallback: (pattern) {
                            return widget.titles;
                          },
                          itemBuilder: (context, suggestion) {
                            print("suggestinmon" + selectedExam.toString());
                            return Container(
                              color: selectedExam != null &&
                                      selectedExam == suggestion
                                  ? NeutralColors.ice_blue
                                  : NeutralColors.pureWhite,
                              child: Padding(
                                padding: EdgeInsets.only(
                                    top: (10 / Constant.defaultScreenHeight) *
                                        screenHeight,
                                    bottom:
                                        (11 / Constant.defaultScreenHeight) *
                                            screenHeight,
                                    left: (15 / Constant.defaultScreenWidth) *
                                        screenWidth),
                                child: Text(
                                  suggestion,
                                  style: TextStyle(
                                    fontFamily: "IBMPlexSans",
                                    fontWeight: FontWeight.w400,
                                    fontSize: 13,
                                    color: selectedExam != null &&
                                            selectedExam == suggestion
                                        ? NeutralColors.purpleish_blue
                                        : NeutralColors.bluey_grey,
                                    textBaseline: TextBaseline.alphabetic,
                                    letterSpacing: 0.0,
                                    inherit: false,
                                  ),
                                ),
                              ),
                            );
                          },
                          hideOnLoading: true,
                          debounceDuration: Duration(milliseconds: 100),
                          transitionBuilder:
                              (context, suggestionsBox, controller) {
                            return suggestionsBox;
                          },
                          onSuggestionSelected: (suggestion) {
                            print(suggestion);
                            //  print(suggestion.toString().split('\'')[0]);
                            _sample.text = suggestion;
                            setState(() {
                              selectedExam = suggestion;
                              for(int i = 0;i<widget.titles.length;i++){
                                if(widget.titles[i]==suggestion){
                                  selectedId = widget.id[i];
                                }
                              }
                            });
                            print("SelectedId"+selectedId.toString());

                            print("=aftersplit" + selectedExam + "=========" + _sample.text);
                            subCategoryTitle = [];
                            getExamWiseList(selectedId);
                            // setState(() {});
                          },
                          onSaved: (value) => {print("something")},
                        ),
                      ),
                    ],
                  )),
              isLoading
                  ? new Container(
                  margin: EdgeInsets.only(
                      top: (200.0 / 720) * screenHeight),
                  child:
                  Center(child: CircularProgressIndicator())):
              Expanded(
                child: ListView.builder(
                  itemCount: subCategoryTitle.length,
                  itemBuilder: (context, index) {
                    return Container(
                      // height: (140/Constant.defaultScreenHeight)*screenHeight,
                      margin: EdgeInsets.only(
                        left: (20 / Constant.defaultScreenWidth) * screenWidth,
                        right: (20 / Constant.defaultScreenWidth) * screenWidth,
                        top: (10 / Constant.defaultScreenHeight) * screenHeight,
                        bottom:
                            (10 / Constant.defaultScreenHeight) * screenHeight,
                      ),
                      decoration: BoxDecoration(
                        border: Border.all(
                          color: NeutralColors.gunmetal.withOpacity(0.10),
                          width:
                              (1 / Constant.defaultScreenWidth) * screenWidth,
                        ),
                        borderRadius: BorderRadius.all(Radius.circular(5)),
                      ),
                      child: Column(
                        children: <Widget>[
                          Container(
                            margin: EdgeInsets.only(
                                top: (20 / Constant.defaultScreenHeight) *
                                    screenHeight,
                                left: (23 / Constant.defaultScreenWidth) *
                                    screenWidth),
                            child: Row(
                              children: <Widget>[
                                Container(
                                  child: read[index]/total[index] != 1.0 ?
                                  CircularPercentIndicator(
                                    radius: 35.0,
                                    lineWidth: 4.0,
                                    percent: read[index]/total[index],
                                    // center: percentiles[index]==1.0 ? GKZoneAssets.tickSvgIcon : Text(""),
                                    // progressColor: NeutralColors.kelly_green,
                                    backgroundColor:
                                    const Color(0xffeeeeee),
                                    progressColor:
                                    NeutralColors.kelly_green,
                                  )
                                      : GKZoneAssets.tickSvgIcon,
                                ),
                                Container(
                                  margin: EdgeInsets.only(
                                      left: (18 / Constant.defaultScreenWidth) *
                                          screenWidth),
                                  child: Text(
                                    subCategoryTitle[index],
                                    style: TextStyle(
                                        fontFamily: "IBMPlexSans",
                                        color: const Color(0xff1f252b),
                                        fontWeight: FontWeight.w500,
                                        fontSize:
                                            (16 / Constant.defaultScreenWidth) *
                                                screenWidth),
                                  ),
                                )
                              ],
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.only(
                                top: (20 / Constant.defaultScreenHeight) *
                                    screenHeight,
                                bottom: (20 / Constant.defaultScreenHeight) *
                                    screenHeight),
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                
                                (showReadButtonCheck[index] == "Yes") ?
                                GestureDetector(
                                  onTap: () {
                                    AppRoutes.push(context, GKZoneListOfPdf(pdfid)).then((value)
                                                  {
                                                    if(value==true)
                                                      {
                                                        subCategoryTitle = [];
                                                        getExamWiseList(selectedId);
                                                      }print("qwertyuu");
                                                    print(value);
                                                  });
                                  },
                                  child: Container(
                                    height:
                                        (40 / Constant.defaultScreenHeight) *
                                            screenHeight,
                                    width: (130 / Constant.defaultScreenWidth) *
                                        screenWidth,
                                    decoration: BoxDecoration(
                                      gradient: LinearGradient(
                                        begin: Alignment(-0.029, 0.472),
                                        end: Alignment(1.049, 0.538),
                                        stops: [
                                          0,
                                          1,
                                        ],
                                        colors: [
                                          SemanticColors.light_purpely,
                                          SemanticColors.dark_purpely
                                        ],
                                      ),
                                      borderRadius:
                                          BorderRadius.all(Radius.circular(2)),
                                    ),
                                    child: Align(
                                      alignment: Alignment.center,
                                      child: Text(
                                        GKZoneStrings.read_label.toUpperCase(),
                                        style: TextStyle(
                                          fontSize: (14 /
                                                  Constant.defaultScreenWidth) *
                                              screenWidth,
                                          fontWeight: FontWeight.w500,
                                          fontFamily: "IBMPlexSans",
                                          color: Colors.white,
                                        ),
                                        textAlign: TextAlign.center,
                                      ),
                                    ),
                                  ),
                                ) : Container(),

                                (testId[index] == "" || testId[index] == null) ?
                                Container () :
                                GestureDetector(
                                  onTap: ()
                                  {

                                    setTestStatusToCallTestLaunchOrReviseApi(testStatus[index]);
                                      global.getToken.then((t) {
                                        
                                          Map postdata = {
                                            "testId": testId[index]
                                          };
                                          print("POSTMAN");
                                          print(postdata);

                                          ApiService()
                                              .postAPI(
                                              URL.TEST_LAUNCH_FOR_YES_PREPARE,
                                              postdata,
                                              t)
                                              .then((result) {
                                            setState(() {
                                              if (result[0].toString().toLowerCase() == 'successfully resume'.toLowerCase() || result[0].toString().toLowerCase() == 'successfully created'.toLowerCase()) {
                                                var totalData = result[1]['data'];
                                                global.tokenForTestLaunchApi = totalData['token'];
                                                print("global");
                                                print(global.tokenForTestLaunchApi);
                                                setAccessTokenForProdTestLaunch(totalData['token']);
                                                buttonClickOnlyOnce = false;
                                                print("start invoked");
                                                print(totalData['token']);
                                                if (testStatus[index] == "Completed") {
                                                  print(testStatus[index]);
                                                  AppRoutes.push(context, GkZoneQuestionReviewScreenUsingHtml());
                                                }
                                                else {
                                                  print(
                                                      "value of i for question screen");
                                                  Navigator.of(context).push(
                                                    new MaterialPageRoute(
                                                        builder: (_) =>
                                                            GkZoneQuestionScreenUsingHtml()),).then((value)
                                                  {
                                                    if(value==true)
                                                      {
                                                        subCategoryTitle = [];
                                                        getExamWiseList(selectedId);
                                                      }print("qwertyuu");
                                                    print(value);
                                                  });
                                                }
//
                                                print("outsideifelse");
                                              } else {}
                                            });
                                          });
                                        
                                      });

                                  },
                                  child: Container(
                                    height:
                                        (40 / Constant.defaultScreenHeight) *
                                            screenHeight,
                                    width: (130 / Constant.defaultScreenWidth) *
                                        screenWidth,
                                    margin: EdgeInsets.only(
                                      left: (15 / Constant.defaultScreenWidth) *
                                          screenWidth,
                                    ),
                                    decoration: BoxDecoration(
                                      gradient: LinearGradient(
                                        begin: Alignment(-0.029, 0.472),
                                        end: Alignment(1.049, 0.538),
                                        stops: [
                                          0,
                                          1,
                                        ],
                                        colors: [
                                          SemanticColors.light_purpely,
                                          SemanticColors.dark_purpely
                                        ],
                                      ),
                                      borderRadius:
                                          BorderRadius.all(Radius.circular(2)),
                                    ),
                                    child: Align(
                                      alignment: Alignment.center,
                                      child: Text(
                                        loadForReviewAndResumeButoon == true ?
                                        GKZoneStrings.waitString :
                                        (testStatus[index] == 'Start') ? GKZoneStrings.sloveString :
                                        (testStatus[index] == 'In-Progress') ? GKZoneStrings.ResumeString :
                                        ((testStatus[index] == 'Completed')) ? GKZoneStrings.ReviewString : "abc",                                      style: TextStyle(
                                        fontSize: (14/Constant.defaultScreenWidth)*screenWidth,
                                        fontWeight: FontWeight.w500,
                                        color: Colors.white,
                                        fontFamily: "IBMPlexSans",
                                      ),
                                        textAlign: TextAlign.center,
                                      ),
                                    ),

                                  ),
                                ),
                              ],
                            ),
                          )
                        ],
                      ),
                    );
                  },
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

_getPositions() {
  final RenderBox renderBoxRed = _keyRed.currentContext.findRenderObject();
  final positionRed = renderBoxRed.localToGlobal(Offset.zero);
  print("POSITION of Red: $positionRed ");
  return positionRed;
}

_getSizes() {
  final RenderBox renderBoxRed = _keyRed.currentContext.findRenderObject();
  final sizeRed = renderBoxRed.size;

  print("SIZE of Red: $sizeRed");
  return sizeRed.height;
}

class SampleService {
  static final List<String> degrees = [
    '',
    'Anusha',
    'Vetri',
    'Kavya',
  ];

  static List<String> getSuggestions(String query) {
    List<String> matches = List();
    matches.add("CAT");
    matches.add("CIT");
    matches.add("IEART");
    matches.add("TISS");

    //  matches.retainWhere((s) => s.toLowerCase().contains(query.toLowerCase()));
    return matches;
  }
}

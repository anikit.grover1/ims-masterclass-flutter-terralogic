import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:imsindia/components/stop_watch_timer.dart';
import 'package:imsindia/resources/strings/prepare.dart';
import 'package:imsindia/utils/colors.dart';
import 'package:imsindia/utils/screen_util.dart';
import 'package:imsindia/utils/svg_images/gk_zone_svg_images.dart';
import 'package:imsindia/utils/svg_images/prepare_svg_images.dart';
import 'package:imsindia/views/GK_Zone/gk_zone_questions_options.dart';
import 'package:imsindia/views/practice_pages/practice_QuestionsOptions.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sliding_up_panel/sliding_up_panel.dart';
List<String> listOfQuestion = ["Prose is to Poetry, said Valéry, as walking is to dancing. That poets regularly produce prose, while prose writers rarely write poetry, is not, as Brodsky argues, evidence of poetry’s superiority.",
                               "The hash code is compatible with equality. It returns the same value for an int and a double with the same numerical value, and therefore the same value for the doubles zero and minus zero.",
                                "Flutter is Google’s mobile UI framework for crafting high-quality native interfaces on iOS and Android in record time. Flutter works with existing code, is used by developers and organizations around the world, and is free and open source.",
                               "The features defined in this library are the lowest-level utility classes and functions used by all the other layers of the Flutter framework."];
List<int> mostPopularAnswer = [2,3,4,1];
List<int> listOfOptions = [1,2,3,4];

List<List> fiftyFityOption = [[1,3],[2,4],[1,4],[2,3]];
List<List> options = [[1,2,3,4],[1,2,3,4],[1,2,3,4],[1,2,3,4]];

class GKZoneQuizScreen extends StatefulWidget {
   Stopwatch stopwatch;
    int prepare_header_status;

  @override
  _GKZoneQuizScreenState createState() => _GKZoneQuizScreenState();

}

final Dependencies dependencies = new Dependencies();

@override

class _GKZoneQuizScreenState extends State<GKZoneQuizScreen> {
  bool iscardClicked = false;
  int preparePositionForQuestion = 1, preparetotalpage;
  int questionNumberInsideQuestionDropDown = 0;
  PageController pageViewController;
  bool isQuestionDropDownClickchecked = false;
  int optionIndex = 1;
  bool isFiftyFiftyOptionEnabled = false;
  bool isMostPopularOptionEnabled = false;
  bool isBookMarkOptionEnabled = false;
  bool prepareBookMarkCheck = false;
  bool mostPopularOptionCheck = false;
  bool fiftyFiftyOptionCheck = false;
  //added for grid answers
  int selectedOptionIndex ;
  List<dynamic> selectedOption = [];
   bool isClicked = false;
  var prepareBookMarkedQuestionsAfterSharedPref;
  var gkZoneMostPopularAnswersAfterSharedPref ;
  var gkZoneFiftyFiftyAnswersAfterSharedPref ;

  bool prepareOverLayEntryCheck = false;
  final _prepareControllerForPopUp = PanelController();
  List prepareBookMarkedQuestionsList= List<String>();
  List GKZoneMostPopularAnswersQuestionsList= List<String>();
  List GKZoneFiftyFiftyOptionsList= List<String>();


  static const _kDuration = const Duration(milliseconds: 300);
  static const _kCurve = Curves.easeIn;
  double _prepareCpanelHeightClosed = 0.0;
  bool questionArrowCheck=false;
  static OverlayEntry entry = null;
  bool get isshow => entry != null;
  void show(context) => addOverlayEntry(context);
  void hide() => removeOverlay();
  void _onScroll() {
   // isOptionsSelected = false;
  }

  bool showMostPopularIcon = false;
  static var question1 = GKZoneQuestionsOptions(
    questionID : 1,
    question: "Prose is to Poetry, said Valéry, as walking is to dancing. That poets regularly produce prose, while prose writers rarely write poetry, is not, as Brodsky argues, evidence of poetry’s superiority.",
    option:
    [{"optionID":1},
      {"optionID":2},
      {"optionID":3},
      {"optionID":4}],
    anotherParagraph: "Poetry is not superior to prose since it was once considered a normal skill expected of any cultivated individual, unlike today, when both poetry and its audience have been marginalized as being difficult and intimidating",
    mostPopularAnswer : 4,
    fiftyFiftyOptions: [1,2],
  );
  static var question2 = GKZoneQuestionsOptions(
    questionID : 2,
    question: "The features defined in this library are the lowest-level utility classes and functions used by all the other layers of the Flutter framework.",
    option:
    [{"optionID":1},
      {"optionID":2},
      ],
    anotherParagraph: "Poetry is not superior to prose since it was once considered a normal skill expected of any cultivated individual, unlike today, when both poetry and its audience have been marginalized as being difficult and intimidating",
    mostPopularAnswer : 1,
    fiftyFiftyOptions: [1],
  );
  static var question3 = GKZoneQuestionsOptions(
    questionID : 3,
    question: "The hash code is compatible with equality. It returns the same value for an int and a double with the same numerical value, and therefore the same value for the doubles zero and minus zero.",
    option:
    [{"optionID":1},
      {"optionID":2},
      {"optionID":3}],
    anotherParagraph: "Poetry is not superior to prose since it was once considered a normal skill expected of any cultivated individual, unlike today, when both poetry and its audience have been marginalized as being difficult and intimidating",
    mostPopularAnswer : 2,
    fiftyFiftyOptions: [2,1],
  );
  static var question4 = GKZoneQuestionsOptions(
    questionID : 4,
    question: "Flutter is Google’s mobile UI framework for crafting high-quality native interfaces on iOS and Android in record time. Flutter works with existing code, is used by developers and organizations around the world, and is free and open source.",
    option:
    [{"optionID":1},
      {"optionID":2},
      {"optionID":3},
      {"optionID":4}],
    anotherParagraph: "Poetry is not superior to prose since it was once considered a normal skill expected of any cultivated individual, unlike today, when both poetry and its audience have been marginalized as being difficult and intimidating",
    mostPopularAnswer : 4,
    fiftyFiftyOptions: [3,1],
  );
  final List<GKZoneQuestionsOptions> listForQuestionptions = [question1, question2, question3, question4,];

  addOverlayEntry(context) {
    questionArrowCheck = true;
    if (entry != null) return;
    entry = new OverlayEntry(builder: (BuildContext context) {
      return LayoutBuilder(builder: (_, BoxConstraints constraints) {
        return  Stack(
          children: <Widget>[
            Positioned(
              top: 120 / 720 * screenHeight,
              right: 20 / 360 * screenWidth,
              left: 20 / 360 * screenWidth,
              child: Material(
                color: Colors.white,
                child: Container(
                  width: 320 / 360 * screenWidth,
                  decoration: BoxDecoration(
                    color: Colors.white,
                    //border: Border.all(color: NeutralColors.pureWhite , width: 0.5),
                    boxShadow: [
                      BoxShadow(
                        offset: Offset(0, 5),
                        blurRadius: 10,
                        color:
                        Color(0xffeaeaea),
                        //offset: Offset.lerp(Offset(10.0,-10.0), Offset(10.0,10.0), 1),
                        // offset: Offset(0,10.0),
                        //color: Colors.orange,
                      ),
                    ],
                  ),
                  height: listOfQuestion.length>30?240 / 720 * screenHeight:null,
                  margin: EdgeInsets.only(top: 8 / 720 * screenHeight),
                  child: Column(
                    children: <Widget>[
                      listOfQuestion.length>30?Expanded(child:  new GridView.count(
                        crossAxisCount: 6,
                        childAspectRatio: (screenHeight / 600),
                        controller:
                        new ScrollController(keepScrollOffset: false),
                        shrinkWrap: true,
                        scrollDirection: Axis.vertical,
                        padding: const EdgeInsets.all(20.0),
                        mainAxisSpacing: 10.0,
                        crossAxisSpacing: 10.0,
                        children: _getTiles(),
                      ),):
                      new GridView.count(
                        crossAxisCount: 6,
                        childAspectRatio: (screenHeight / 600),
                        controller:
                        new ScrollController(keepScrollOffset: false),
                        shrinkWrap: true,
                        scrollDirection: Axis.vertical,
                        padding: const EdgeInsets.all(20.0),
                        mainAxisSpacing: 10.0,
                        crossAxisSpacing: 10.0,
                        children: _getTiles(),
                      ),
                    ],
                  ),
                ),
              ),

            )
          ],

        );
      });
    });

    addoverlay(entry, context);
  }
  List<Widget> _getOptionTiles(int position) {
    final List<Widget> tiles = <Widget>[];

    for (int iteration = 0; iteration < listForQuestionptions[position].option.length; iteration++) {
      tiles.add(new GridTile(
          child: new InkResponse(
              enableFeedback: true,
              onTap: () {
                setState(() {
                  selectedOptionIndex = iteration ;
                  print("******************selectedOptionIndex"+position.toString());

                  if(gkZoneMostPopularAnswersAfterSharedPref==null){
                   // print("******************selectedOptionIndex"+position.toString());

                  }
                  if(gkZoneMostPopularAnswersAfterSharedPref.contains(iteration)){
                    print("******************selectedOptionIndex"+gkZoneMostPopularAnswersAfterSharedPref.toString());

                  }
                 // print("******************selectedOptionIndex"+gkZoneMostPopularAnswersAfterSharedPref.toString());
                  if(selectedOption.contains(iteration)){
                    selectedOption.remove(iteration);
                  }
                  else{
                    selectedOption.add(iteration);
                  }                  prepareOverLayEntryCheck
                      ? prepareOverLayEntryCheck = false
                      : prepareOverLayEntryCheck = true;
                  hide();
                });
              },
              child: Stack(
                children: <Widget>[
                  Positioned(
                    child: Container(
                      decoration: BoxDecoration(
                        border:  Border.all(color:
                        selectedOptionIndex != null &&
                            selectedOptionIndex == iteration
                             ? NeutralColors.dark_navy_blue :
                        isFiftyFiftyOptionEnabled &&
                            listForQuestionptions[position].fiftyFiftyOptions.contains(listForQuestionptions[position].option[iteration]['optionID'])?
                        NeutralColors.ice_blue :  NeutralColors.blue_grey, width: 2),
                        boxShadow: [
                          BoxShadow(color: Colors.white
                            // color:Colors.blue,
                          ),
                        ],
                        //  borderSide: const BorderSide(color: Colors.grey, width: 0.0),
                        borderRadius: BorderRadius.all(Radius.circular(3)),
                      ),
                      child: Align(
                        alignment: Alignment.center,
                        child: Text(
                          //listOfOptions[iteration].toString(),\
                          listForQuestionptions[position].option[iteration]['optionID'].toString(),
                          style: TextStyle(
                            color: NeutralColors.dark_navy_blue,
                            fontSize: 14 / 360 * screenWidth,
                            fontFamily: "IBMPlexSans",
                            fontWeight: FontWeight.w400,


                          ),
                          textAlign: TextAlign.center,
                        ),
                      ),
                    ),
                  ),
//                  gkZoneMostPopularAnswersAfterSharedPref != null? (isMostPopularOptionEnabled && listForQuestionptions[preparePositionForQuestion].option[iteration]['optionID'] == listForQuestionptions[preparePositionForQuestion].mostPopularAnswer?
//                  Container(
//                  margin: EdgeInsets.only(left: (25/Constant.defaultScreenWidth)*screenWidth,
//                      top:(0/Constant.defaultScreenHeight)*screenHeight),
//                    child: SvgPicture.asset(
//                      GKZoneAssets.RedPopularFilledSvg ,
//                      fit: BoxFit.none,
//                      //  alignment: Alignment.topRight,
//                      //  alignment: Alignment.topLeft,
//                    ),
//                  ) :Container()):Text(""),
                  (gkZoneMostPopularAnswersAfterSharedPref != null)
                  ? ( gkZoneMostPopularAnswersAfterSharedPref.contains(position) && listForQuestionptions[position].option[iteration]['optionID'] == listForQuestionptions[position].mostPopularAnswer
                  ? Container(
                  margin: EdgeInsets.only(left: (25/Constant.defaultScreenWidth)*screenWidth,
                      top:(0/Constant.defaultScreenHeight)*screenHeight),
                    child: SvgPicture.asset(
                      GKZoneAssets.RedPopularFilledSvg ,
                      fit: BoxFit.none,
                    ))
                      : Text(""))
        : Text(""),
                ],
              ))));
    }
    return tiles;
  }
//
  List<Widget> _getTiles() {
    final List<Widget> tiles = <Widget>[];

    for (int iteration = 0; iteration < listOfQuestion.length; iteration++) {
      tiles.add(new GridTile(
          child: new InkResponse(
              enableFeedback: true,
              onTap: () {
                setState(() {
                  preparePositionForQuestion = iteration + 1;
                  questionNumberInsideQuestionDropDown = preparePositionForQuestion;
                  isQuestionDropDownClickchecked = true;

                  pageViewController.jumpToPage(preparePositionForQuestion-1);

                  prepareOverLayEntryCheck
                      ? prepareOverLayEntryCheck = false
                      : prepareOverLayEntryCheck = true;
                  hide();
                });
              },
              child: Stack(
                children: <Widget>[
                  Container(
                    decoration: BoxDecoration(
                      border: Border.all(color: Colors.grey, width: 0.5),
                      boxShadow: [
                        BoxShadow(color: Colors.white
                          // color:Colors.blue,
                        ),
                      ],
                      //  borderSide: const BorderSide(color: Colors.grey, width: 0.0),
                      borderRadius: BorderRadius.all(Radius.circular(6)),
                    ),
                    child: Align(
                      alignment: Alignment.center,
                      child: Text(
                        (iteration + 1).toString(),
                        style: TextStyle(
                          color: Color.fromARGB(255, 0, 3, 44),
                          fontSize: 14 / 360 * screenWidth,
                          fontFamily: "IBMPlexSans",
                          fontWeight: FontWeight.w500,
                        ),
                        textAlign: TextAlign.center,
                      ),
                    ),
                  ),
                  Positioned(
                    top: 0.0,
                    right: 3.0 / 360 * screenWidth,
                    child: Align(
                      alignment: Alignment.topRight,
                      child: Container(
                        margin: EdgeInsets.only(
                            top: (2 / 720) * screenHeight,
                            left: (25 / 360) * screenWidth),
                        child: Align(
                            alignment: Alignment.topRight,
                            child: (prepareBookMarkedQuestionsAfterSharedPref != null)
                                ? (prepareBookMarkedQuestionsAfterSharedPref
                                .contains((iteration + 1).toString())
                                ? Image.asset(
                              "assets/images/bookmark-selected.png",
                              height: (11.3 / 720) * screenHeight,
                              width: (7 / 360) * screenWidth,
                              fit: BoxFit.fitHeight,
                            )
                                : Text(""))
                                : Text("")),
                      ),
                    ),
                  ),
                ],
              ))));
    }
    return tiles;
  }
  static addoverlay(OverlayEntry entry, context) async {
    Overlay.of(context).insert(entry);
  }
  removeOverlay() {
    questionArrowCheck=false;
    entry?.remove();
    entry = null;

  }


  void bookMarkCheck(){
    if(prepareBookMarkedQuestionsAfterSharedPref.contains((preparePositionForQuestion).toString())){
      prepareBookMarkCheck = true;
    }else{
      prepareBookMarkCheck = false;
    }
  }

  void setBookMarkedquestionsForPrepare() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setStringList("bookMarkedQuestionsForGKZone", prepareBookMarkedQuestionsList);
  }

  getBookMarkedquestionsForPrepare() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prepareBookMarkedQuestionsAfterSharedPref = prefs.getStringList("bookMarkedQuestionsForGKZone");
    this.setState(() {
      prepareBookMarkedQuestionsAfterSharedPref = prefs.getStringList("bookMarkedQuestionsForGKZone");
      if(prefs.getStringList("bookMarkedQuestionsForGKZone")==null){
        prepareBookMarkedQuestionsList=prepareBookMarkedQuestionsList;
        print(prepareBookMarkedQuestionsList);
      }else{
        prepareBookMarkedQuestionsList=prefs.getStringList("bookMarkedQuestionsForGKZone");
        bookMarkCheck();
      }
    });
  }
  void fiftyFiftyCheck(){
    if(gkZoneFiftyFiftyAnswersAfterSharedPref.contains((preparePositionForQuestion).toString())){
      fiftyFiftyOptionCheck = true;
    }else{
      fiftyFiftyOptionCheck = false;
    }
  }
  void setFiftyFiftyOptionsForGKZone() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setStringList("fiftyFityOptionsGKZone", GKZoneFiftyFiftyOptionsList);
    print("***************************setFiftyFityAnswersforGkZone"+GKZoneFiftyFiftyOptionsList.toString());
  }
  getFiftyFiftyOptionsForGKZone() async {

    SharedPreferences prefs = await SharedPreferences.getInstance();
    gkZoneFiftyFiftyAnswersAfterSharedPref = prefs.getStringList("fiftyFityOptionsGKZone");
    this.setState(() {
      gkZoneFiftyFiftyAnswersAfterSharedPref = prefs.getStringList("fiftyFityOptionsGKZone");
      if(prefs.getStringList("fiftyFityOptionsGKZone")==null){
        GKZoneFiftyFiftyOptionsList=GKZoneFiftyFiftyOptionsList;
        print("IFFFFFFFFFF****getFiftyFiftyOptionsForGKZone"+GKZoneFiftyFiftyOptionsList.toString());
      }else{
        GKZoneFiftyFiftyOptionsList=prefs.getStringList("fiftyFityOptionsGKZone");
        print("ELSEEEEEEEEEEE****getFiftyFiftyOptionsForGKZone"+GKZoneFiftyFiftyOptionsList.toString());
        fiftyFiftyCheck();
      }
    });
  }
  void mostPopularCheck(){
    if(gkZoneMostPopularAnswersAfterSharedPref.contains((preparePositionForQuestion).toString())){
      mostPopularOptionCheck = true;
    }else{
      mostPopularOptionCheck = false;
    }
  }
  void setMostPopularAnswersforGkZone() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setStringList("mostPopularAnswersForGKZone", GKZoneMostPopularAnswersQuestionsList);
    print("***************************setMostPopularAnswersforGkZone"+GKZoneMostPopularAnswersQuestionsList.toString());
  }
  getMostPopularOptionsForGKZone() async {

    SharedPreferences prefs = await SharedPreferences.getInstance();
    gkZoneMostPopularAnswersAfterSharedPref = prefs.getStringList("mostPopularAnswersForGKZone");
    this.setState(() {
      gkZoneMostPopularAnswersAfterSharedPref = prefs.getStringList("mostPopularAnswersForGKZone");
      if(prefs.getStringList("mostPopularAnswersForGKZone")==null){
        GKZoneMostPopularAnswersQuestionsList=GKZoneMostPopularAnswersQuestionsList;
        print("IFFFFFFFFFF****getMostPopularOptionsForGKZone"+GKZoneMostPopularAnswersQuestionsList.toString());
      }else{
        GKZoneMostPopularAnswersQuestionsList=prefs.getStringList("mostPopularAnswersForGKZone");
        print("ELSEEEEEEEEEEE****getMostPopularOptionsForGKZone"+GKZoneMostPopularAnswersQuestionsList.toString());
        mostPopularCheck();
      }
    });
  }
  void initState() {
     pageViewController = PageController(
    keepPage: true
  )..addListener(_onScroll);
    // TODO: implement initState
    // prepareHeader_status_question = widget.prepare_header_status;
    // prepareSumbit_button_status = widget.prepare_sumbit_status;
    super.initState();
//    setState(() {
//      if(prepareSoloutionCheck == true){
//        index = widget.backtoques_num;
//      }
//      if (widget.stopwatch == null) {
        dependencies.stopwatch.start();
//      } else if (widget.stopwatch != null) {
//        dependencies.stopwatch = widget.stopwatch;
//        dependencies.stopwatch.stop();
//      }
//    });
    getMostPopularOptionsForGKZone();
    getBookMarkedquestionsForPrepare();
    getFiftyFiftyOptionsForGKZone();
//    UpdateViewForSolutionActive_back();
//    UpdateViewForBargraphActive_back();
  }
  Future<bool> _onWillPop() {
    setState(() {
      prepareOverLayEntryCheck
          ? prepareOverLayEntryCheck = false
          : prepareOverLayEntryCheck = true;
      hide();

    });
    Navigator.pop(context, false);
  }
  @override
  Widget build(BuildContext context) {
    var _mediaQueryData = MediaQuery.of(context);
    double screenWidth = _mediaQueryData.size.width;
    double screenHeight = _mediaQueryData.size.height;
    return WillPopScope(
      onWillPop: _onWillPop,
      child: GestureDetector(
        onTap: (){
          if(prepareOverLayEntryCheck){
            setState(() {
              hide();
           //   questionArrowCheck = false;
            });
          }
          },
        child: Scaffold(
          appBar: AppBar(
            elevation: 0.0,
            leading: IconButton(
              icon: SvgPicture.asset(
                getPrepareSvgImages.backIcon,
                fit: BoxFit.fill,
              ),
              onPressed: () =>{
              setState(() {
              prepareOverLayEntryCheck
              ? prepareOverLayEntryCheck = false
                  : prepareOverLayEntryCheck = true;
              hide();

              }),
                Navigator.pop(context, false),
              }
            ),
            titleSpacing: 0.0,
            brightness: Brightness.light,
            centerTitle: false,
            automaticallyImplyLeading: true,
            actions: <Widget>[
              InkWell(
                onTap: (){
                },
                child: Container(
                  margin: EdgeInsets.only(
                    // top: 33 / 720 * screenHeight,
                    right: 20 / 360 * screenWidth,
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      GestureDetector(
                        child: Container(
                          width: 15.0/360*screenWidth,
                          child:
                          Image.asset(
                            "assets/images/ic-pause-24px.png",
                            height: 0.02 * screenHeight,
                            width: 0.02 * screenWidth,
                            fit: BoxFit.scaleDown,
                          ),
                        ),
                        onTap: () {
//                            setState(() {
//                              preparePopUpForResumeAndBackCheck = true;
//                              dependencies.stopwatch.stop();
//                              hide();
//                              prepareOverLayEntryCheck
//                                  ? prepareOverLayEntryCheck = false
//                                  : prepareOverLayEntryCheck = true;
//                              //questionsScreen.buttonstatus=false;
//                            });
                          //_prepareControllerForPopUp.open();
                        },
                      ),
                      Container(
                        margin: EdgeInsets.only(left: (10/Constant.defaultScreenWidth)*screenWidth),
                        child: TimerText(
                          dependencies: dependencies,
                        ),
                      ),
                    ],
                  ),
                ),
              )],
            backgroundColor: Colors.white,
            iconTheme: IconThemeData(
              color: Colors.black,
            ),
            title:Text(
                "Week 5 - Sept",
                style: TextStyle(
                  color: NeutralColors.black,
                  fontSize: 18 / 720 * screenHeight,
                  fontFamily: "IBMPlexSans",
                  fontWeight: FontWeight.w700,
                ),
                ),
          ),
          body: Container(
            color: Colors.white,
            child: Column(
              children: <Widget>[
                Container(
                  height: (30 / 640) * screenHeight,
                  width: (320 / 360) * screenWidth,
                  margin: EdgeInsets.only(
                      top: (10 / 640) * screenHeight,
                      left: (20 / 360) * screenWidth,
                      right: (20 / 360) * screenWidth),
                  child: Row(
                    children: <Widget>[
                      Container(
                        decoration: BoxDecoration(
                          color: NeutralColors.purpleish_blue,
                          borderRadius: BorderRadius.all(Radius.circular(2)),
                        ),
                        height: (30 / 640) * screenHeight,
                        width: (170 / 360) * screenWidth,
                        margin: EdgeInsets.only(
                            right: (55 / 360) * screenWidth, left: 0.0),
                        child: InkWell(
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.stretch,
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Container(
                                height: (18 / 640) * screenHeight,
                                margin: EdgeInsets.only(
                                    left: (15 / 360) * screenWidth,
                                    top: (6 / 640) * screenHeight,
                                    bottom: (6 / 640) * screenHeight),
                                child: Text(
                                  PrepareParagraphScreenStrings.question  +"${preparePositionForQuestion}/${listOfQuestion.length}",
                                  style: TextStyle(
                                    color: NeutralColors.pureWhite,
                                    fontSize: 14 / 640 * screenHeight,
                                    fontFamily: "IBMPlexSans",
                                    fontWeight: FontWeight.w500,

                                  ),
                                  textAlign: TextAlign.left,
                                ),
                              ),
                              Center(
                                child: Container(
                                    width: (11 / 360) * screenWidth,
                                    margin: EdgeInsets.only(
                                      right: (22 / 360) * screenWidth,
                                    ),
                                    child:Center(child:Icon(
                                      questionArrowCheck?Icons.keyboard_arrow_up:Icons.keyboard_arrow_down,
                                      color: NeutralColors.pureWhite,
                                      size: 22.0/360*screenWidth,
                                    ))
                                ),
                              )
                            ],
                          ),
                          onTap: () {
                            setFiftyFiftyOptionsForGKZone();
                            setMostPopularAnswersforGkZone();
                            setBookMarkedquestionsForPrepare();
                            setState(() {
                            //  getBookMarkedquestionsForPrepare();
                              prepareOverLayEntryCheck
                                  ? prepareOverLayEntryCheck = false
                                  : prepareOverLayEntryCheck = true;
                              prepareOverLayEntryCheck
                                  ? show(context)
                                  : hide();

                            });
                          },
                        ),
                      ),
                      Container(
                        width: 95 / 360 * screenWidth,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          children: <Widget>[
                            InkWell(
                              onTap: () {
                              setState(() {
                                if(GKZoneFiftyFiftyOptionsList.contains((preparePositionForQuestion).toString())){
                                  GKZoneFiftyFiftyOptionsList.remove(preparePositionForQuestion.toString());
                                  print("**********if"+GKZoneFiftyFiftyOptionsList.toString());

                                  fiftyFiftyOptionCheck = false;
                                } else{
                                  GKZoneFiftyFiftyOptionsList.add(preparePositionForQuestion.toString());
                                  print("**********else"+GKZoneFiftyFiftyOptionsList.toString());
                                  fiftyFiftyOptionCheck = true;
                                }
                                  isFiftyFiftyOptionEnabled = !isFiftyFiftyOptionEnabled;
                                  prepareOverLayEntryCheck
                                      ? prepareOverLayEntryCheck = false
                                      : prepareOverLayEntryCheck = true;
                                  hide();

                                });
                              },
                              child: Container(
                                child: Padding(
                                  padding: const EdgeInsets.all(5.0),
                                  child: Container(
                                    child:
                                    SvgPicture.asset(
                                      isFiftyFiftyOptionEnabled ?  GKZoneAssets.fiftyFilledSvg : GKZoneAssets.fiftyNotFilledSvg,
                                      fit: BoxFit.fitHeight,
                                      height: (19.3 / 640) * screenHeight,
                                      width: (14.4 / 360) * screenWidth,
                                    ),
                                  ),
                                ),
                              ),
                            ),
                            //  Spacer(),
                            InkWell(
                              onTap: () {
                                setState(() {
                                  if(GKZoneMostPopularAnswersQuestionsList.contains((preparePositionForQuestion).toString())){
                                    GKZoneMostPopularAnswersQuestionsList.remove(preparePositionForQuestion.toString());
                                    print("**********if"+GKZoneMostPopularAnswersQuestionsList.toString());

                                    mostPopularOptionCheck = false;
                                  } else{
                                    GKZoneMostPopularAnswersQuestionsList.add(preparePositionForQuestion.toString());
                                    print("**********else"+GKZoneMostPopularAnswersQuestionsList.toString());
                                    mostPopularOptionCheck = true;
                                  }
                                  isMostPopularOptionEnabled = !isMostPopularOptionEnabled;
                                  prepareOverLayEntryCheck
                                      ? prepareOverLayEntryCheck = false
                                      : prepareOverLayEntryCheck = true;
                                  hide();

                                });
//                                for(int i =0;i< listForQuestionptions.length;i++){
//                                  if(listForQuestionptions[i].question == listForQuestionptions[preparePositionForQuestion].question){
//                                      print("question"+listForQuestionptions[i].question);
//                                    for(int j =0;j <listForQuestionptions[i].option.length;j++){
//                                   //   print(listForQuestionptions[i].option.length);
//                                      if(listForQuestionptions[i].option[j]['optionID'] == listForQuestionptions[preparePositionForQuestion].mostPopularAnswer){
//                                        print("coming");
//                                        setState(() {
//                                          isMostPopularOptionEnabled= true;
//                                        });
////                                        print(listForQuestionptions[i].option[j]['optionID']);
////                                        print(listForQuestionptions[preparePositionForQuestion].mostPopularAnswer);
//                                      }else{
//                                        setState(() {
//                                          print("Print Else Part");
//                                        });
//                                      }
//                                    }
//                                  }else{
//                                    print(listForQuestionptions[i].question);
//                                  }
//                                }
                              },
                              child: Padding(
                                padding: const EdgeInsets.all(5.0),
                                child: Container(
                                    height: (18 / 640) * screenHeight,
                                    width: (16 / 360) * screenWidth,
                                    child: SvgPicture.asset(
                                      isMostPopularOptionEnabled ? GKZoneAssets.popularFilledSvg : GKZoneAssets.popularNotFilledSvg,
                                      fit: BoxFit.fitHeight,
                                      //    ? Colors.grey
                                      //    : null,
                                    )),
                              ),
                            ),
                            // Spacer(),
                            InkWell(
                              onTap: () {
                               setState(() {
                                 if(prepareBookMarkedQuestionsList.contains(preparePositionForQuestion.toString())){
                                   prepareBookMarkedQuestionsList.remove(preparePositionForQuestion.toString());
                                   prepareBookMarkCheck = false;
                                 } else{
                                   prepareBookMarkedQuestionsList.add(preparePositionForQuestion.toString());
                                   prepareBookMarkCheck = true;
                                 }
                                 isBookMarkOptionEnabled = !isBookMarkOptionEnabled;
                                     prepareOverLayEntryCheck
                                      ? prepareOverLayEntryCheck = false
                                      : prepareOverLayEntryCheck = true;
                                  hide();

                                });
                              },
                              child: Padding(
                                padding: const EdgeInsets.all(5.0),
                                child: Container(
                                  height: (15 / 640) * screenHeight,
                                  width: (11.7 / 360) * screenWidth,
                                  // margin: EdgeInsets.only(right: 25.6),
                                  child: SvgPicture.asset(
                                    prepareBookMarkCheck
                                        ? getPrepareSvgImages.bookmarkActive
                                        : getPrepareSvgImages.bookmarkInactive,
                                    fit: BoxFit.contain,
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                Expanded(
                  child: PageView.builder(
                    controller: pageViewController,
                    itemBuilder: (context, position)
                    {
                      return(
                          Container(
                            color: Colors.white,
                            margin: EdgeInsets.only(left:(20/Constant.defaultScreenWidth)*screenWidth,right: ((20/Constant.defaultScreenWidth)*screenWidth),
                                top:(30/Constant.defaultScreenHeight)*screenHeight),
                            child: Column(
                              children: <Widget>[
                                Container(
                                  child: Text(
                                    //listOfQuestion[position],
                                    listForQuestionptions[position].question,
                                    style: TextStyle(
                                      fontSize: (16/Constant.defaultScreenWidth)*screenWidth,
                                      color: NeutralColors.dark_navy_blue,
                                      fontWeight: FontWeight.w500,
                                      //  height: 1.5,
                                      fontFamily: "IBMPlexSans",
                                    ),
                                  ),
                                ),
//                                Container(
//                                 // color:Colors.red,
//                                 // height:35/Constant.defaultScreenHeight*screenHeight,
//                                  margin:EdgeInsets.only(top:(10/Constant.defaultScreenHeight)*screenHeight,
//                                  bottom: (30/Constant.defaultScreenHeight)*screenHeight),
//                                  child: GridView.count(
//                                    crossAxisCount: 6,
//                                  //  childAspectRatio: (screenHeight / 350),
//                                    childAspectRatio : 1,
//                                    controller:
//                                    new ScrollController(keepScrollOffset: false),
//                                    shrinkWrap: true,
//                                    scrollDirection: Axis.vertical,
//                                    padding: const EdgeInsets.all(0.0),
//                                    mainAxisSpacing: 0.0,
//                                    crossAxisSpacing: 10.0,
//                                    children: _getOptionTiles(position),
//                                  ),
//                                ),
                                Container(
                                  // margin: EdgeInsets.only(top:(30/Constant.defaultScreenHeight)*screenHeight),
                                  child: Row(
                                    crossAxisAlignment: CrossAxisAlignment.center,
                                    children: <Widget>[
                                      GestureDetector(
                                        onTap:(){
                                          setState(() {
                                           // isClicked = !isClicked;
                                          });
                                        },
                                        child: Stack(
                                          children: <Widget>[
                                            Positioned(
                                              child: Container
                                                (
                                                margin: EdgeInsets.only(right:(15/Constant.defaultScreenWidth)*screenWidth,top:(30/Constant.defaultScreenHeight)*screenHeight),
                                                height: (35/Constant.defaultScreenHeight)*screenHeight,
                                                width: (35/Constant.defaultScreenWidth) *screenWidth,
                                                child: Container(
                                                  decoration: BoxDecoration(
                                                    border: Border.all(
                                                      color: isFiftyFiftyOptionEnabled && fiftyFityOption[position].contains(optionIndex)?  NeutralColors.ice_blue : NeutralColors.blue_grey,
                                                      // color: Colors.cyan,
                                                      width: (2 / Constant.defaultScreenWidth) * screenWidth,
                                                    ),
                                                    borderRadius: BorderRadius.all(Radius.circular(3)),
                                                  ),
                                                  child: Align(
                                                    alignment: Alignment.center,
                                                    child: Text(
                                                      '$optionIndex',
                                                      style: TextStyle(
                                                        fontSize: (14/Constant.defaultScreenWidth)*screenWidth,
                                                        color: isFiftyFiftyOptionEnabled && fiftyFityOption[position].contains(optionIndex)?  NeutralColors.dark_navy_blue.withOpacity(0.2) : NeutralColors.dark_navy_blue,
                                                        // color: Colors.cyan,
                                                        fontWeight: FontWeight.w400,
                                                        //  height: 1.5,
                                                        fontFamily: "IBMPlexSans",
                                                      ),
                                                      textAlign: TextAlign.center,
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            ),
                                            (isMostPopularOptionEnabled && (optionIndex == mostPopularAnswer[position]))? Container(                                          margin: EdgeInsets.only(left: (25/Constant.defaultScreenWidth)*screenWidth,
                                                  top:(15/Constant.defaultScreenHeight)*screenHeight),
                                              child: SvgPicture.asset(
                                                GKZoneAssets.RedPopularFilledSvg ,
                                                fit: BoxFit.none,
                                                //  alignment: Alignment.topRight,
                                                //  alignment: Alignment.topLeft,
                                              ),
                                            ) : Container()
                                          ],
                                        ),
                                      ),
                                      GestureDetector(
                                        child: Stack(
                                          children: <Widget>[
                                            Positioned(
                                              child: Container
                                                (
                                                margin: EdgeInsets.only(right:(15/Constant.defaultScreenWidth)*screenWidth,top:(30/Constant.defaultScreenHeight)*screenHeight),
                                                height: (35/Constant.defaultScreenHeight)*screenHeight,
                                                width: (35/Constant.defaultScreenWidth) *screenWidth,
                                                child: Container(
                                                  decoration: BoxDecoration(
                                                    border: Border.all(
                                                      color: isFiftyFiftyOptionEnabled && fiftyFityOption[position].contains(2)?  NeutralColors.ice_blue : NeutralColors.blue_grey,
                                                      // color: Colors.cyan,
                                                      width: (2 / Constant.defaultScreenWidth) * screenWidth,
                                                    ),
                                                    borderRadius: BorderRadius.all(Radius.circular(3)),
                                                  ),
                                                  child: Align(
                                                    alignment: Alignment.center,
                                                    child: Text(
                                                      '${optionIndex+1}',
                                                      style: TextStyle(
                                                        fontSize: (14/Constant.defaultScreenWidth)*screenWidth,
                                                        color: isFiftyFiftyOptionEnabled && fiftyFityOption[position].contains(2)?  NeutralColors.dark_navy_blue.withOpacity(0.2) : NeutralColors.dark_navy_blue,
                                                        fontWeight: FontWeight.w400,
                                                        //  height: 1.5,
                                                        fontFamily: "IBMPlexSans",
                                                      ),
                                                      textAlign: TextAlign.center,
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            ),
                                            (isMostPopularOptionEnabled && (2 == mostPopularAnswer[position]))? Container(                                          margin: EdgeInsets.only(left: (25/Constant.defaultScreenWidth)*screenWidth,
                                                  top:(15/Constant.defaultScreenHeight)*screenHeight),
                                              child: SvgPicture.asset(
                                                GKZoneAssets.RedPopularFilledSvg ,
                                                fit: BoxFit.none,
                                                //  alignment: Alignment.topRight,
                                                //  alignment: Alignment.topLeft,
                                              ),
                                            ) : Container()
                                          ],
                                        ),
                                      ),
                                      GestureDetector(
                                        child: Stack(
                                          children: <Widget>[
                                            Positioned(
                                              child: Container
                                                (
                                                margin: EdgeInsets.only(right:(15/Constant.defaultScreenWidth)*screenWidth,top:(30/Constant.defaultScreenHeight)*screenHeight),
                                                height: (35/Constant.defaultScreenHeight)*screenHeight,
                                                width: (35/Constant.defaultScreenWidth) *screenWidth,
                                                child: Container(
                                                  decoration: BoxDecoration(
                                                    border: Border.all(
                                                      color: isFiftyFiftyOptionEnabled && fiftyFityOption[position].contains(3)?  NeutralColors.ice_blue : NeutralColors.blue_grey,
                                                      // color: Colors.cyan,
                                                      width: (2 / Constant.defaultScreenWidth) * screenWidth,
                                                    ),
                                                    borderRadius: BorderRadius.all(Radius.circular(3)),
                                                  ),
                                                  child: Align(
                                                    alignment: Alignment.center,
                                                    child: Text(
                                                      '${optionIndex+2}',
                                                      style: TextStyle(
                                                        fontSize: (14/Constant.defaultScreenWidth)*screenWidth,
                                                        color: isFiftyFiftyOptionEnabled && fiftyFityOption[position].contains(3)?  NeutralColors.dark_navy_blue.withOpacity(0.2) : NeutralColors.dark_navy_blue,
                                                        fontWeight: FontWeight.w400,
                                                        //  height: 1.5,
                                                        fontFamily: "IBMPlexSans",
                                                      ),
                                                      textAlign: TextAlign.center,
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            ),
                                            (isMostPopularOptionEnabled && (3 == mostPopularAnswer[position]))? Container(                                          margin: EdgeInsets.only(left: (25/Constant.defaultScreenWidth)*screenWidth,
                                                  top:(15/Constant.defaultScreenHeight)*screenHeight),
                                              child: SvgPicture.asset(
                                                GKZoneAssets.RedPopularFilledSvg ,
                                                fit: BoxFit.none,
                                                //  alignment: Alignment.topRight,
                                                //  alignment: Alignment.topLeft,
                                              ),
                                            ) : Container()
                                          ],
                                        ),
                                      ),
                                      GestureDetector(
                                        child: Stack(
                                          children: <Widget>[
                                            Positioned(
                                              child: Container
                                                (
                                                margin: EdgeInsets.only(right:(15/Constant.defaultScreenWidth)*screenWidth,top:(30/Constant.defaultScreenHeight)*screenHeight),
                                                height: (35/Constant.defaultScreenHeight)*screenHeight,
                                                width: (35/Constant.defaultScreenWidth) *screenWidth,
                                                child: Container(
                                                  decoration: BoxDecoration(
                                                    border: Border.all(
                                                      color: isFiftyFiftyOptionEnabled && fiftyFityOption[position].contains(4)?  NeutralColors.ice_blue : NeutralColors.blue_grey,
                                                      // color: Colors.cyan,
                                                      width: (2 / Constant.defaultScreenWidth) * screenWidth,
                                                    ),
                                                    borderRadius: BorderRadius.all(Radius.circular(3)),
                                                  ),
                                                  child: Align(
                                                    alignment: Alignment.center,
                                                    child: Text(
                                                      '${optionIndex+3}',
                                                      style: TextStyle(
                                                        fontSize: (14/Constant.defaultScreenWidth)*screenWidth,
                                                        color: isFiftyFiftyOptionEnabled && fiftyFityOption[position].contains(4)?  NeutralColors.dark_navy_blue.withOpacity(0.2) : NeutralColors.dark_navy_blue,
                                                        fontWeight: FontWeight.w400,
                                                        //  height: 1.5,
                                                        fontFamily: "IBMPlexSans",
                                                      ),
                                                      textAlign: TextAlign.center,
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            ),
                                            (isMostPopularOptionEnabled && (4 == mostPopularAnswer[position]))? Container(
                                              margin: EdgeInsets.only(left: (25/Constant.defaultScreenWidth)*screenWidth,
                                                  top:(15/Constant.defaultScreenHeight)*screenHeight),
                                              child: SvgPicture.asset(
                                                GKZoneAssets.RedPopularFilledSvg ,
                                                fit: BoxFit.fitHeight,
                                                //  alignment: Alignment.topRight,
                                                //  alignment: Alignment.topLeft,
                                              ),
                                            ) : Container()
                                          ],
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                Container(
                                  height: 1/720*screenHeight,
                                  decoration:BoxDecoration(
                                      boxShadow: [new BoxShadow(
                                        color: NeutralColors.off_white.withOpacity(0.90),
                                        blurRadius: 0.0,
                                      ),]
                                  ),
                                    margin: EdgeInsets.only(top:(38/Constant.defaultScreenHeight)*screenHeight),
                                    ),
                                Container(


                                  margin: EdgeInsets.only(top:(20/Constant.defaultScreenHeight)*screenHeight),
                                  child: Text(
//                                    "Prose is to Poetry, said Valéry, as walking is to dancing. That poets regularly produce prose, while prose writers rarely write poetry, is not, as Brodsky argues, evidence of poetry’s superiority.",
                                    listForQuestionptions[position].anotherParagraph,
                                    style: TextStyle(
                                      fontSize: (16/Constant.defaultScreenWidth)*screenWidth,
                                      color: NeutralColors.dark_navy_blue,
                                      fontWeight: FontWeight.w400,
                                      //  height: 1.5,
                                      fontFamily: "IBMPlexSans",
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          )
                      );
                    },
                    itemCount: listOfQuestion.length,
                    onPageChanged: (page){
                      setState(() {
                        preparePositionForQuestion=page+1;
                        print("======================");
                        print(listForQuestionptions[page].mostPopularAnswer);
                        getBookMarkedquestionsForPrepare();
                        getMostPopularOptionsForGKZone();
                        getFiftyFiftyOptionsForGKZone();
                        if(gkZoneFiftyFiftyAnswersAfterSharedPref!=null){
                          if(gkZoneFiftyFiftyAnswersAfterSharedPref.contains((page).toString())){
                            fiftyFiftyOptionCheck =true;
                          }else{
                            fiftyFiftyOptionCheck = false;
                          }
                        }else{

                        }
                        if(gkZoneMostPopularAnswersAfterSharedPref!=null){
                          if(gkZoneMostPopularAnswersAfterSharedPref.contains((page+1).toString())){
                            mostPopularOptionCheck =true;
                          }else{
                            mostPopularOptionCheck = false;
                          }
                        }else{

                        }
                        if(prepareBookMarkedQuestionsAfterSharedPref!=null){
                          if(prepareBookMarkedQuestionsAfterSharedPref.contains((page+1).toString())){
                            prepareBookMarkCheck = true;
                          }else{
                            prepareBookMarkCheck = false;
                          }
                        }else{
                        }
                      });
                    },
                  ),
                ),
                GestureDetector(
                  onTap: (){
                    setState(() {
                      prepareOverLayEntryCheck
                          ? prepareOverLayEntryCheck = false
                          : prepareOverLayEntryCheck = true;
                      hide();
                    });
                  },
                  child: Container(
                    height:60/720*screenHeight ,
                    //margin: EdgeInsets.only(top: 40/720 * screenHeight,bottom:23/720* screenHeight),
                    child: Stack(
                      alignment: Alignment.bottomCenter,
                      children: [
                      //  _status(),
                        Positioned(
                            bottom: 0.01 * screenHeight,
                            child:  Container(
                              height:40/720*screenHeight ,
                              width:320/360*screenWidth ,
                              margin: EdgeInsets.only(left: (20/360)*screenWidth,right: (20/360)*screenWidth,bottom: (15/720)*screenHeight,top: (33/720)*screenHeight),
                              // color: Colors.black,
                              child: Row(
                                children: <Widget>[
                                  Container(
                                    decoration: BoxDecoration(
                                        color: SemanticColors.iceBlue
                                    ),
                                    width:105/360*screenWidth ,
                                    child:
                                    FlatButton( color: SemanticColors.iceBlue,
                                      textColor: Colors.white,
                                      disabledColor: SemanticColors.iceBlue ,
                                      disabledTextColor: NeutralColors.black,
                                      onPressed: (){
                                        setState(() {
                                        //  (prepareSumbit_button_status==null)?
                                          pageViewController.previousPage(duration: _kDuration, curve: _kCurve);
                                        //  (prepareSumbit_button_status==null)?
                                        //  iscardchecked==false:null;
                                          prepareOverLayEntryCheck
                                              ? prepareOverLayEntryCheck = false
                                              : prepareOverLayEntryCheck = true;
                                          hide();

                                        });
                                      },
                                      child: Text(
                                        Preparequestions.prev,
                                        style: TextStyle(
                                          color: NeutralColors.blue_grey,
                                          fontSize: 14,
                                          fontFamily: "IBMPlexSans",
                                          fontWeight: FontWeight.w500,
                                        ),
                                        textAlign: TextAlign.center,
                                      ),),

                                  ),
                                  Container(
                                    width:110/360*screenWidth  ,
                                    decoration: BoxDecoration(
                                        gradient:
                                        LinearGradient(
                                          begin: Alignment(-0.056, 0.431),
                                          end: Alignment(1.076, 0.579),
                                          stops: [
                                            0,
                                            1,
                                          ],
                                          colors: [
                                            SemanticColors.light_purpely,
                                            NeutralColors.box_color,
                                          ],
                                        ),
                                    ),
                                    child:FlatButton(
                                        onPressed: (){
//                                        setState(() {
//                                          if(listForQuestionptions[preparePositionForQuestion-1].answer == listForQuestionptions[preparePositionForQuestion-1].option[_selectedIndexForOption]['optionID']){
//                                            answer_status = "CORRECT";
//                                            if(preparePositionForQuestion == listForQuestionptions.length){
//                                              AppRoutes.push(context,PrepareCalculationSummary());
//                                            }
//                                          }else{
//                                            answer_status = "WRONG";
//                                            if(preparePositionForQuestion == listForQuestionptions.length){
//                                              if(isOptionsSelected == true){
//                                                AppRoutes.push(context,PrepareCalculationSummary());
//                                              }
//                                            }
//                                          }
//                                          prepareOverLayEntryCheck
//                                              ? prepareOverLayEntryCheck = false
//                                              : prepareOverLayEntryCheck = true;
//                                          hide();
//
//                                        });
                                        },
                                        child: Text(
                                          Preparequestions.submit,
                                          style: TextStyle(
                                           // color:(prepareSumbit_button_status==null && isOptionsSelected==true)?NeutralColors.pureWhite:AccentColors.blueGrey,
                                            fontSize: 14,
                                            fontFamily: "IBMPlexSans",
                                            fontWeight: FontWeight.w500,
                                            color:Colors.white,
                                          ),
                                          textAlign: TextAlign.center,
                                        )),),
                                  Container(
                                    decoration: BoxDecoration(
                                        color: GradientColors.iceBlue
                                    ),
                                    width: 105/360*screenWidth ,
                                    child:FlatButton(
                                      onPressed: (){
                                        setState(() {
                                          isMostPopularOptionEnabled = false;
                                          isFiftyFiftyOptionEnabled = false;
                                          pageViewController.nextPage(duration: _kDuration, curve: _kCurve);
                                          prepareOverLayEntryCheck
                                              ? prepareOverLayEntryCheck = false
                                              : prepareOverLayEntryCheck = true;
                                          hide();
                                        });
                                      },
                                      color:  GradientColors.iceBlue,
                                      textColor: Colors.white,
                                      disabledColor:  GradientColors.iceBlue,
                                      child: Text(
                                        Preparequestions.next,
                                        style: TextStyle(
                                          color:AccentColors.blueGrey,
                                          fontSize: 14,
                                          fontFamily: "IBMPlexSans",
                                          fontWeight: FontWeight.w500,
                                        ),
                                        textAlign: TextAlign.center,
                                      ),),),
                                ],
                              ),
                            )),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),

        ),
      ),
    );
  }
}

import 'dart:math';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:imsindia/components/custom_dropdown_gk_zone.dart';
import 'package:imsindia/resources/strings/gk_zone.dart';
import 'package:imsindia/routers/routes.dart';
import 'package:imsindia/utils/colors.dart';
import 'package:imsindia/utils/screen_util.dart';
import 'package:imsindia/utils/svg_images/calendar_svg_images.dart';
import 'package:imsindia/utils/svg_images/gk_zone_svg_images.dart';
//import 'package:imsindia/views/Arun/Samples/DropDown.dart';
import 'package:imsindia/views/GK_Zone/gk_zone_quiz_screen.dart';
import 'package:imsindia/views/GK_Zone/gk_zone_week5_sept.dart';
import 'package:imsindia/views/prepare_pages/prepare_questions_review_screen.dart';
import 'package:intl/intl.dart';
import 'package:percent_indicator/circular_percent_indicator.dart';
import 'package:queries/collections.dart';

import 'package:imsindia/api/bloc/services.dart';
import 'package:imsindia/utils/Url.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:imsindia/utils/global.dart' as global;

import 'gk_zone_lists_pdf.dart';

import 'gk_zone_question_review_screen.dart';
import 'gk_zone_question_screen.dart';
bool value = false;
GlobalKey _keyRed = GlobalKey();
GlobalKey _keyRed1 = GlobalKey();

List dropdowntitles = [
  'July ’19',
  'August ’19',
  'September ’19',
  'October ’19',
  'November ’19',
  'December ’19',
  'January ’20',
];

final titles = [
  'Week 1',
  'Week 2',
  'Week 3',
  'Week 3',
  'Week 3',
];
final body =[
  'Quiz available on 25th Spet',
  'Quiz available for 5 days!',
  'Quiz Complete!',
  'Quiz available on 25th Spet',
  'Quiz available on 25th Spet',
];
final percentiles = [0.3, 0.5, 1.0, 0.4, 0.8];
String selectedMonth;
List<String> getMonths = [];
List<String> getMonthsInputFormat = [];
List<String> dropDownMonthsInputFormat = [];
List<String> dropDownMonths = [];

final icons = [
  GKZoneAssets.scienceSvgIcon,
  GKZoneAssets.geographySvgIcon,
  GKZoneAssets.historySvgIcon,
];

class GKZoneCurrentAffairsTab extends StatefulWidget {
  var id;
  GKZoneCurrentAffairsTab(this.id);
  @override
  _GKZoneCurrentAffairsTabState createState() => _GKZoneCurrentAffairsTabState();
}
class _GKZoneCurrentAffairsTabState extends State<GKZoneCurrentAffairsTab>{
//  SuggestionsBoxController sugg;
  final TextEditingController _sample = new TextEditingController();



  //_controller = PanelController();
  double _panelHeightClosed = 0.0;
  var userId;
  var authorization;
  var courseID;
  var subCategoryTitle;
  var subCategoryId;
  var testId;
  var showQuizCheck;
  var takeQuizCheck;
  var showReadButtonCheck;
  var quizStatus;
  var read;
  var total;
  var isLoading = true;
  var totaldata;
  var selectedMon;

  void getCurrentAffairsList(int selectedId) async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var userId = prefs.getString("userId");
    var authorization = prefs.getString("loginAccessToken");
    var courseID = prefs.getString("courseId");
    Map postdata = {
      "userId": userId,
      "categoryId":selectedId,
      "Authorization": authorization,
      "courseId":courseID,
      "selectedDate":selectedMon,
    };
    print("Post Data"+postdata.toString());
    ApiService().postAPIStatusCodeFromResponse(URL.GKZone_CurrentAffairs, postdata, global.headers).then((result) {
      print("result"+result.toString());
      setState(() {
        if (result[0]['success']==true) {
          //isLoading = true;
          subCategoryTitle = [];
          subCategoryId = [];
          testId = [];
          showQuizCheck = [];
          takeQuizCheck = [];
          quizStatus = [];
          showReadButtonCheck =[];
          read = [];
          total = [];
          totaldata = result[0]['data'];
          for (var index = 0; index < result[0]['data'].length; index++) {
              subCategoryTitle.add(result[0]['data'][index]['pdfTitle']);
              subCategoryId.add(result[0]['data'][index]['catId']);
              testId.add(result[0]['data'][index]['testId']);
              showQuizCheck.add(result[0]['data'][index]['showQuiz']);
              takeQuizCheck.add(result[0]['data'][index]['showQuiz']);
              quizStatus.add(result[0]['data'][index]['testStatus']);
              read.add(result[0]['data'][index]['read']);
              total.add(result[0]['data'][index]['total']);
              showReadButtonCheck.add(result[0]['data'][index]['showReadButton']);
          }
          isLoading = false;
          } else {

        }
      });
    });
  }

  /// set access token to prod test launch/reveiw screen...............
  void setAccessTokenForProdTestLaunch(value) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString("accessTokenGkzone", value);
  }

  void setTestStatusToCallTestLaunchOrReviseApi(value) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString("testStatusgkzone", value);
    print(value + "valueeeeeeeeeee");
    print(prefs.setString("testStatusgkzone", value));
  }

  @override
  void initState() {

    getCurrentAffairsList(widget.id);

    var today = new DateTime.now();
    _currentMonthofCal = DateFormat("MMMM").format(DateTime.now());
    currentYear = DateFormat("yyyy").format(DateTime.now());
    var listnov  = _currentMonthofCal.substring(0,3)+" "+currentYear;
    var listnovInputFormat = currentYear+"-"+DateFormat("MM").format(DateTime.now())+"-"+"01";
    print("**************initState" +listnov +_currentMonthofCal +currentYear);

    String yearfromnow;
    for(int month=1;month<11;month++){
      var nextmonthFromNow = today.subtract(new Duration(days: 31));
      today = nextmonthFromNow;
      print("==========="+DateFormat("MMMM").format(today));
       yearfromnow = DateFormat("yyyy").format(today);
      var lasttwodigit_year = yearfromnow;
      getMonths.add(listnov);
      getMonths.add(DateFormat("MMMM").format(today).substring(0,3)+" "+yearfromnow);
      var resultList = new Collection(getMonths).distinct();
      dropDownMonths=resultList.toList();
      getMonthsInputFormat.add(listnovInputFormat);
      getMonthsInputFormat.add(yearfromnow+"-"+DateFormat("MM").format(today)+"-"+"01");
      var resultListt = new Collection(getMonthsInputFormat).distinct();
      dropDownMonthsInputFormat=resultListt.toList();
      print(getMonths);
      print(getMonthsInputFormat);
      print(dropDownMonthsInputFormat);
    }


    DateTime now = DateTime.now();
    DateFormat dateFormat = DateFormat("EEE d MMMM yyyy");
    String formattedDate = dateFormat.format(DateTime.now());
    _currentMonth = dropDownMonths[0];
    selectedMon = dropDownMonthsInputFormat[0];
    //_currentMonth = "Select Month";
  //  _currentMonth = DateFormat("MMMM").format(DateTime.now()).substring(0,3) +" "+currentYear;
   // print('*************************************'+_currentMonth);
    var newDate = DateFormat("EEE d MM yyyy").format(DateTime.now());
    print(newDate);
    currentYear = DateFormat("yyyy").format(DateTime.now());
    lastTwodigitofyear = currentYear;
    super.initState();

  }



  String _currentMonth = 'fsrfsrf' , _currentMonthofCal = '';
  String currentYear = '';
  String lastTwodigitofyear = '';
  @override
  Widget build(BuildContext context) {
    var _mediaQueryData = MediaQuery.of(context);
    double screenWidth = _mediaQueryData.size.width;
    double screenHeight = _mediaQueryData.size.height;
    return GestureDetector(
      onTap: (){

      },
      child: Scaffold(
        backgroundColor: Colors.transparent,
        body: Container(
          //color: Colors.white,
          child: Column(
            children: <Widget>[
              Container(
                  margin:EdgeInsets.only(left:(21 / Constant.defaultScreenWidth) * screenWidth,
                      top: (29 / Constant.defaultScreenHeight) * screenHeight,
                      bottom: (8 / Constant.defaultScreenHeight) * screenHeight),
              child:  Row(
                  children: <Widget>[
                    Container(
                      margin:EdgeInsets.only(right:(15 / Constant.defaultScreenWidth) * screenWidth ),
                      child: Text(
                        GKZoneStrings.select_Month,
                        style: TextStyle(
                          fontWeight: FontWeight.w500,
                          fontSize:  (14 / Constant.defaultScreenWidth) * screenWidth,
                          fontFamily: "IBMPlexSans",
                          color: NeutralColors.dark_navy_blue,                      ),
                      ),
                    ),
                    Container(
                      width:160/360*screenWidth ,
                      height: 40/720*screenHeight,
                      decoration: BoxDecoration(
                        borderRadius: new BorderRadius.circular(5.0),
                        border: Border.all(color: NeutralColors.gunmetal.withOpacity(0.15)),
                      ),
                      child: DropDownFormField(
                     //  selected_month: selectedMonth,
                        getImmediateSuggestions: true,
                        // autoFlipDirection: true,
                        textFieldConfiguration: TextFieldConfiguration(
                          controller: _sample,
                          label: selectedMonth!=null ? selectedMonth :_currentMonth,

                        ),
                       //  suggestionsBoxController:sugg,
                        suggestionsBoxDecoration: SuggestionsBoxDecoration(
                          dropDownHeight: 130/720*screenHeight,
                            borderRadius: new BorderRadius.circular(5.0),
                            color: Colors.white),
                        suggestionsCallback: (pattern) {
                          return SampleService.getSuggestions(pattern);
                        },
                        itemBuilder: (context, suggestion) {
                          print("suggestinmon"+selectedMonth.toString());
                          return Container(
                            color: selectedMonth != null &&
                                selectedMonth == suggestion
                                ? NeutralColors.ice_blue
                                : NeutralColors.pureWhite,
                            child: Padding(
                              padding:  EdgeInsets.only(top: (10 / Constant.defaultScreenHeight) * screenHeight,
                                bottom: (11 / Constant.defaultScreenHeight) * screenHeight,
                                  left:(15 / Constant.defaultScreenWidth) * screenWidth ),
                              child: Text(
                                suggestion,
                                style: TextStyle(
                                  fontFamily: "IBMPlexSans",
                                  fontWeight: FontWeight.w400,
                                    fontSize:
                                    (14 / Constant.defaultScreenWidth) *
                                        screenWidth,
                                  color: selectedMonth != null &&
                                      selectedMonth == suggestion
                                      ? NeutralColors.purpleish_blue
                                      : NeutralColors.bluey_grey,
                                  textBaseline: TextBaseline.alphabetic,
                                  letterSpacing: 0.0,
                                  inherit: false,
                                ),
                              ),
                            ),
                          );
                        },
                        hideOnLoading: true,
                        debounceDuration: Duration(milliseconds: 100),
                        transitionBuilder: (context, suggestionsBox, controller) {
                          return suggestionsBox;
                        },
                        onSuggestionSelected: (suggestion) {
                          print('Sangeethaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa'+suggestion
                          );
                          _sample.text = suggestion;
                          print("=======>>>>"+_sample.text);
                          setState(() {
                            selectedMonth = suggestion;
                           // _currentMonth =suggestion;
                           for(int i = 0;i<dropDownMonths.length;i++){
                                if(dropDownMonths[i]==suggestion){
                                  selectedMon = dropDownMonthsInputFormat[i];
                                }
                              }
                              isLoading = true;
                          });
                          print(selectedMon);
                          subCategoryTitle = [];
                          getCurrentAffairsList(widget.id);
                        },
                        onSaved: (value) => {print("something")},
                      ),

                    ),
                  ],
                )
              ),
               subCategoryTitle.length == 0
                  ? new Container(
                  margin: EdgeInsets.only(
                      top: (200.0 / 720) * screenHeight),
                  child:
                  Center(child: isLoading ? CircularProgressIndicator() : Text("No Data",))):
              Expanded(
                child: ListView.builder(
                  // physics: new Sc,

                  padding: EdgeInsets.only(
                      bottom: (10 / Constant.defaultScreenHeight) * screenHeight),
                  itemCount: subCategoryTitle.length,
                  itemBuilder: (context, index) {
                    return Container(
                      // height: (140/Constant.defaultScreenHeight)*screenHeight,
                      margin: EdgeInsets.only(
                        left: (20 / Constant.defaultScreenWidth) * screenWidth,
                        right: (20 / Constant.defaultScreenWidth) * screenWidth,
                        top: (10 / Constant.defaultScreenHeight) * screenHeight,
                                             ),
                      decoration: BoxDecoration(
                        border: Border.all(
                          color: NeutralColors.gunmetal.withOpacity(0.10),
                          // color: Colors.cyan,
                          width: (1 / Constant.defaultScreenWidth) * screenWidth,
                        ),
                        borderRadius: BorderRadius.all(Radius.circular(5)),
                      ),
                      child: Column(
                        children: <Widget>[
                          Container(
                            margin: EdgeInsets.only(
                                top: (20 / Constant.defaultScreenHeight) *
                                    screenHeight,
                                left: (23 / Constant.defaultScreenWidth) *
                                    screenWidth),
                            child: Row(
                              children: <Widget>[
                                Container(
                                  child: read[index]/total[index] != 1.0
                                      ? CircularPercentIndicator(
                                          radius: 35.0,
                                          lineWidth: 4.0,
                                          percent: read[index]/total[index],
                                          // center: percentiles[index]==1.0 ? GKZoneAssets.tickSvgIcon : Text(""),
                                          // progressColor: NeutralColors.kelly_green,
                                          backgroundColor:
                                              const Color(0xffeeeeee),
                                          progressColor:
                                              NeutralColors.kelly_green,
                                        )
                                      : GKZoneAssets.tickSvgIcon,
                                ),
                                Container(
                                  margin: EdgeInsets.only(
                                      left: (18/ Constant.defaultScreenWidth) *
                                          screenWidth),
                                  child:Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,

                                    children: <Widget>[
                                      Container(
//                                        margin: EdgeInsets.only(
//                                            right: (60/ Constant.defaultScreenWidth) *
//                                                screenWidth),
                                        child: Text(
                                          subCategoryTitle[index],
                                          style: TextStyle(
                                              fontFamily: "IBMPlexSans",
                                              color: const Color(0xff1f252b),
                                              fontWeight: FontWeight.w500,
                                              fontSize:
                                              (16 / Constant.defaultScreenWidth) *
                                                  screenWidth),
                                        ),
                                      ),
                                      // Container(

                                      //  margin: EdgeInsets.only(
                                      //      top: (5 / Constant.defaultScreenHeight) *
                                      //           screenHeight),
                                      //   child: Text(
                                      //     body[index],
                                      //     style: TextStyle(
                                      //         fontFamily: "IBMPlexSans",
                                      //         color: NeutralColors.blue_grey,
                                      //         fontWeight: FontWeight.w400,
                                      //         fontSize:
                                      //         (14 / Constant.defaultScreenWidth) *
                                      //             screenWidth),
                                      //   ),
                                      // ),
                                      ],
                                     ),

                                ),

                              ],
                            ),
                          ),

                          Container(
                            margin: EdgeInsets.only(
                                top: (22 / Constant.defaultScreenHeight) *
                                    screenHeight,
                                bottom: (20 / Constant.defaultScreenHeight) *
                                    screenHeight),
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[

                                (showReadButtonCheck[index] == "Yes") ?
                                GestureDetector(
                                  onTap:(){
                                    //AppRoutes.push(context, Week5Steps());
                                    AppRoutes.push(context, GKZoneListOfPdf(subCategoryId[index])).then((value)
                                              {
                                                print("qwertyuu");
                                                print(value);
                                                subCategoryTitle = [];
                                                getCurrentAffairsList(widget.id);
                                              });
                                  },
                                  child: Container(
                                    height: (40 / Constant.defaultScreenHeight) *
                                        screenHeight,
                                    width: (130 / Constant.defaultScreenWidth) *
                                        screenWidth,
                                    decoration: BoxDecoration(
                                      gradient: LinearGradient(
                                        begin: Alignment(-0.029, 0.472),
                                        end: Alignment(1.049, 0.538),
                                        stops: [
                                          0,
                                          1,
                                        ],
                                        colors: [
                                          SemanticColors.light_purpely,
                                          SemanticColors.dark_purpely
                                        ],
                                      ),
                                      borderRadius:
                                          BorderRadius.all(Radius.circular(2)),
                                    ),
                                    child: Align(
                                      alignment: Alignment.center,
                                      child: Text(
                                        GKZoneStrings.read_label.toUpperCase(),
                                        style: TextStyle(
                                          fontSize:
                                              (14 / Constant.defaultScreenWidth) *
                                                  screenWidth,
                                          fontWeight: FontWeight.w500,
                                          fontFamily: "IBMPlexSans",
                                          color: Colors.white,
                                        ),
                                        textAlign: TextAlign.center,
                                      ),
                                    ),
                                  ),
                                )
                                : Container (),

                                // showQuizCheck[index] == "No" ?
                                // Container() : showQuizCheck[index] == "No" ?
                                // Container() :
                                (testId[index] == "" || testId[index] == null) ?
                                Container () :
                                GestureDetector(
                                  onTap:(){
                                    //AppRoutes.push(context, GKZoneQuizScreen());
                                    setTestStatusToCallTestLaunchOrReviseApi(quizStatus[index]);

                                    global.getToken.then((t) {
                                      Map postdata = {
                                        "testId": testId[index]
                                      };
                                      print("POSTMAN");
                                      print(postdata);

                                      ApiService().postAPI(URL.TEST_LAUNCH_FOR_YES_PREPARE,postdata,t).then((result) {
                                        setState(() {
                                          if (result[0].toString().toLowerCase() == 'successfully resume'.toLowerCase() || result[0].toString().toLowerCase() == 'successfully created'.toLowerCase()) {
                                            var totalData = result[1]['data'];
                                            global.tokenForTestLaunchApi = totalData['token'];
                                            print("global");
                                            print(global.tokenForTestLaunchApi);
                                            setAccessTokenForProdTestLaunch(totalData['token']);
                                            print("start invoked");
                                            print(totalData['token']);
                                            if(quizStatus[index] == "Completed")
                                            {
                                              print(quizStatus[index]);
                                              print("inside review screen");

                                              Navigator.of(context).push(new MaterialPageRoute(builder: (_) => GkZoneQuestionReviewScreenUsingHtml())).then((value) {
                                                print("valueeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee");
                                                print(value);
                                                if (value == true) {
                                                  setState(() {
                                                    print("coming here");
                                                    print("loadForReviewAndResumeButoonloadForReviewAndResumeButoon");
                                                  });
                                                }
                                              });                                            }else{
                                              print("value of i for question screen");
                                              Navigator.of(context).push(new MaterialPageRoute(builder: (_)=>GkZoneQuestionScreenUsingHtml()),).then((value)
                                              {
                                                print("qwertyuu");
                                                print(value);
                                                subCategoryTitle = [];
                                                getCurrentAffairsList(widget.id);
                                              });
                                            }
                                            print("outsideifelse");
                                          } else {}
                                        });
                                      });
                                  });
                                  },
                                  child: Container(
                                    height: (40 / Constant.defaultScreenHeight) *
                                        screenHeight,
                                    width: (130 / Constant.defaultScreenWidth) *
                                        screenWidth,
                                    margin: EdgeInsets.only(
                                      left: (15 / Constant.defaultScreenWidth) *
                                          screenWidth,
                                    ),
                                    decoration: BoxDecoration(
                                      gradient: LinearGradient(
                                        begin: Alignment(-0.029, 0.472),
                                        end: Alignment(1.049, 0.538),
                                        stops: [
                                          0,
                                          1,
                                        ],
                                        colors: [
                                          SemanticColors.light_purpely,
                                          SemanticColors.dark_purpely
                                        ],
                                      ),
                                      borderRadius:
                                          BorderRadius.all(Radius.circular(2)),
                                    ),
                                    child: Align(
                                      alignment: Alignment.center,
                                      child: Text(
                                        quizStatus[index] == "Start" ? "START QUIZ" : quizStatus[index] == "In-Progress" ? "RESUME" : quizStatus[index] == "Completed" ? "REVIEW" : "",
                                        style: TextStyle(
                                          fontSize:
                                              (14 / Constant.defaultScreenWidth) *
                                                  screenWidth,
                                          fontWeight: FontWeight.w500,
                                          color: Colors.white,
                                          fontFamily: "IBMPlexSans",
                                        ),
                                        textAlign: TextAlign.center,
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          )
                        ],
                      ),
                    );
                  },
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class SampleService {
  static final List<String> degrees = [
    '',
    'Anusha',
    'Vetri',
    'Kavya',
  ];

  static List<String> getSuggestions(String query) {
    List<String> matches = List();
    matches.addAll(dropDownMonths);

    //  matches.retainWhere((s) => s.toLowerCase().contains(query.toLowerCase()));
    return matches;
  }
}
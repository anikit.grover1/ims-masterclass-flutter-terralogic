import 'package:flutter/material.dart';

class GKZoneQuestionsOptions{
  int questionID;
  String question;
//  String answer;
//  String paragraph;
//  var options;
  PageController controller;
  int mostPopularAnswer;
  var fiftyFiftyOptions;
  int pos;
  List<Map<String, dynamic>> option;
  String anotherParagraph;

  GKZoneQuestionsOptions({
    this.questionID,
    this.question,
//    this.answer,
//    this.paragraph,
//    this.options,
    this.controller,
    this.mostPopularAnswer,
    this.fiftyFiftyOptions,
    this.pos,
    this.option,
    this.anotherParagraph,
});

  GKZoneQuestionsOptions.gop(this.pos);

}
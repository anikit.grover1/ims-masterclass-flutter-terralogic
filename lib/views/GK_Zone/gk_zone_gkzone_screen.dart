import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_svg/svg.dart';
import 'package:imsindia/utils/colors.dart';
import 'package:imsindia/utils/emaximiser_svg_icons.dart';
import 'package:imsindia/utils/svg_images/gk_zone_svg_images.dart';
// ignore: camel_case_types
class Data{
  String correctAnswer;
  String text;
  Data({
    this.correctAnswer,
    this.text,
  });
}

class GKZoneWeeklyStats extends StatefulWidget{
  @override
  State<StatefulWidget> createState() => GKZoneWeeklyStatsState();

}
class GKZoneWeeklyStatsState extends State <GKZoneWeeklyStats> {
  @override
  Widget build(BuildContext context) {
    var _mediaQueryData = MediaQuery.of(context);
    double screenWidth = _mediaQueryData.size.width;
    double screenHeight = _mediaQueryData.size.height;

    final attempt1 = Data(
      correctAnswer: "12",
      text: "Answered correctly in 1st attempt",
    );
    final attempt2 = Data(
      correctAnswer: "25",
      text: "Answered correctly in 2nd attempt",
    );

    final List<Data> result = [
      attempt1,
      attempt2,
    ];
    return Scaffold(
      appBar: new AppBar(
        brightness: Brightness.light,
        elevation: 0.0,
        centerTitle: false,
        titleSpacing: 0.0,
        iconTheme: IconThemeData(
          color: Colors.black, //change your color here
        ),
        backgroundColor: Color.fromARGB(255, 255, 255, 255),
        leading: IconButton(
          icon: getSvgIcon.backSvgIcon,
          onPressed: () => Navigator.pop(context, false),
        ),
        title: new Text(
          "GK Zone",
          style: TextStyle(
            color: Color.fromARGB(255, 0, 0, 0),
            fontSize: screenHeight * 16 / 720,
            fontFamily: "IBMPlexSans",
            fontWeight: FontWeight.w700,
          ),
          textAlign: TextAlign.left,
        ),),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Container(
              color: Colors.white,
              child: Container(
                margin: EdgeInsets.only(top: 40 / 720 * screenHeight,
                    left: 20 / 360 * screenWidth,
                    right: 20 / 360 * screenWidth,
                    bottom: 0 / 720 * screenHeight),
                decoration: BoxDecoration(
                    border: Border.all(
                      color: NeutralColors.gunmetal.withOpacity(0.10),
                      width: (1 / 360) * screenWidth,
                    ),
                    borderRadius: BorderRadius.circular(5)
                ),
                child: Container(
                  child: Column(
                    children: <Widget>[
                      Container(
                        margin: EdgeInsets.only(top: 30 / 720 * screenHeight),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Container(
                              // color: Colors.green,
                              margin: EdgeInsets.only(left: 35 / 360 * screenWidth),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Container(
                                    child: Text(
                                      "52",
                                      style: TextStyle(
                                          fontFamily: "IBMPlexSans",
                                          color: NeutralColors.dark_navy_blue,
                                          fontWeight: FontWeight.w700,
                                          fontSize:
                                          (43 / 360) * screenWidth),
                                    ),
                                  ),
                                  Container(
                                    margin: EdgeInsets.only(
                                        top: 14 / 720 * screenHeight),
                                    child: Text(
                                      "ALL INDIA RANK",
                                      style: TextStyle(
                                          fontFamily: "IBMPlexSans",
                                          color: NeutralColors.gunmetal,
                                          fontWeight: FontWeight.w500,
                                          fontSize:
                                          (12 / 360) * screenWidth),
                                    ),
                                    //width: 93/360*screenWidth,
                                  ),
                                ],
                              ),
                            ),
                            Container(
                              //color: Colors.blue,
                              margin: EdgeInsets.only(right: 40 / 360 *
                                  screenWidth),
                              child: SvgPicture.asset(
                                GKZoneAssets.performance_rankSvg,
                                fit: BoxFit.scaleDown,
                              ),
                            ),
                          ],
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(top: 30 / 720 * screenHeight,
                          left: 15 / 360 * screenWidth,
                          right: 15 / 360 * screenWidth,
                          bottom: 10 / 360 * screenWidth,),
                        //color: Colors.lightGreen,
                        decoration: BoxDecoration(
                          gradient: LinearGradient(
                            begin: Alignment(0.029, 0.472),
                            end: Alignment(1.049, 0.538),
                            stops: [0, 1],
                            colors: [
                              Color.fromARGB(
                                  255, 168, 174, 35).withOpacity(0.10),
                              Color.fromARGB(
                                  255, 201, 110, 216).withOpacity(0.10),
                            ],
                          ),
                          borderRadius: BorderRadius.all(
                              Radius.circular(5)),
                        ),
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Container(
                                  margin: EdgeInsets.only(
                                      top: 21 / 720 * screenHeight,
                                      left: 20 / 360 * screenWidth),
                                  child: Text(
                                    "74",
                                    style: TextStyle(
                                      color: NeutralColors.dark_navy_blue,
                                      fontSize: 20 / 720 * screenHeight,
                                      fontFamily: "IBMPlexSans",
                                      fontWeight: FontWeight.w700,
                                    ),
                                    textAlign:
                                    TextAlign.left,
                                  ),
                                ),
                                Container(
                                  margin: EdgeInsets.only(
                                      top: 6 / 720 * screenHeight,
                                      left: 20 / 360 * screenWidth,
                                      bottom: 20 / 720 * screenHeight),
                                  child: Text(
                                    "TOTAL SCORE",
                                    style: TextStyle(
                                      color: NeutralColors.gunmetal,
                                      fontSize: 12 / 720 * screenHeight,
                                      fontFamily: "IBMPlexSans",
                                      fontWeight: FontWeight.w500,
                                    ),
                                    textAlign:
                                    TextAlign.left,
                                  ),
                                ),
                              ],
                            ),
                            Container(
                              height: 50 / 720 * screenHeight,
                              width: 2 / 360 * screenWidth,
                              margin: EdgeInsets.only(top: 19 / 720 * screenHeight,
                                  bottom: 20 / 720 * screenHeight,
                                  left: 0 / 360 * screenWidth,
                                  right: 00 / 360 * screenWidth),
                              decoration: BoxDecoration(
                                color: NeutralColors.purpley.withOpacity(.15),
                                borderRadius: BorderRadius.circular(10),
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.only(right: 20 / 360 *
                                  screenWidth),
                              child: Column(crossAxisAlignment: CrossAxisAlignment
                                  .start,
                                children: <Widget>[
                                  Container(
                                    margin: EdgeInsets.only(
                                      top: 21 / 720 * screenHeight,
                                      left: 0 / 360 * screenWidth,),
                                    child: Text(
                                      "89",
                                      style: TextStyle(
                                        color: NeutralColors.dark_navy_blue,
                                        fontSize: 20 / 720 * screenHeight,
                                        fontFamily: "IBMPlexSans",
                                        fontWeight: FontWeight.w700,
                                      ),
                                      textAlign:
                                      TextAlign.left,
                                    ),
                                  ),
                                  Container(
                                    margin: EdgeInsets.only(
                                        top: 6 / 720 * screenHeight,
                                        left: 0 / 360 * screenWidth,
                                        bottom: 20 / 720 * screenHeight),
                                    child: Text(
                                      "HIGHEST SCORE",
                                      style: TextStyle(
                                        color: NeutralColors.gunmetal,
                                        fontSize: 12 / 720 * screenHeight,
                                        fontFamily: "IBMPlexSans",
                                        fontWeight: FontWeight.w500,
                                      ),
                                      textAlign:
                                      TextAlign.left,
                                    ),
                                  ),
                                ],
                              ),
                            )
                          ],
                        ),
                      ),
                      ListView.separated(
                        itemCount: result.length,
                        shrinkWrap: true,
                        physics: NeverScrollableScrollPhysics(),
                        itemBuilder: (context, index) {
                          return  Container(
                            child:Column(
                              children: <Widget>[
                                new Container(
                                  margin: EdgeInsets.only(left: 15/360*screenWidth,top: 19.5/720*screenHeight,bottom: result.indexOf(result[index]) == (result.length - 1) ? 35/720*screenHeight : 19.5/720*screenHeight),
                                  child: Row(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: <Widget>[
                                      Container(
                                        margin: EdgeInsets.only(left: 20/360*screenWidth,top: 0/720*screenHeight),
                                        child: Text(
                                          result[index].correctAnswer.toString(),
                                          style: TextStyle(
                                              fontFamily: "IBMPlexSans",
                                              color: NeutralColors.dark_navy_blue,
                                              fontWeight: FontWeight.w500,
                                              fontSize:(20 / 360) *screenWidth),
                                        ),
                                      ),
                                      Container(
                                        margin: EdgeInsets.only(left: 12/360*screenWidth,top: 5/720*screenHeight),
                                        child: Text(
                                          result[index].text.toString(),
                                          style: TextStyle(
                                              fontFamily: "IBMPlexSans",
                                              color: NeutralColors.gunmetal,
                                              fontWeight: FontWeight.w400,
                                              fontSize: (14 / 360) * screenWidth),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          );
                        },
                        separatorBuilder: (context, index) {
                          return Container(
                              margin: EdgeInsets.symmetric(horizontal:(20 / 360) * screenWidth ),
                              child: Divider()
                          );
                        },
                      ),
                    ],
                  ),
                ),

              ),
            ),
            Container(
              height: 40,
              color:Colors.white,
            )
          ],
        ),
      ),

    );
  }
}


Widget getTextWidgets(List<Data> strings,final screenHeight,final screenWidth)
{
  List<Widget> list = new List<Widget>();
  for(var i=0; i < strings.length; i++){
    list.add(
      Container(
        child:Column(
          children: <Widget>[
            new Container(
              margin: EdgeInsets.only(left: 15/360*screenWidth,top: 30/720*screenHeight,bottom: 19.5/720*screenHeight),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Container(
                    margin: EdgeInsets.only(left: 20/360*screenWidth,top: 0/720*screenHeight),
                    child: Text(
                      strings[i].correctAnswer.toString(),
                      //item.Correctanswer,
                      style: TextStyle(
                          fontFamily: "IBMPlexSans",
                          color: NeutralColors.dark_navy_blue,
                          fontWeight: FontWeight.w500,
                          fontSize:(20 / 360) *screenWidth),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(left: 12/360*screenWidth,top: 5/720*screenHeight),
                    child: Text(
                      strings[i].text.toString(),
                      //item.text,
                      style: TextStyle(
                          fontFamily: "IBMPlexSans",
                          color: NeutralColors.gunmetal,
                          fontWeight: FontWeight.w400,
                          fontSize: (14 / 360) * screenWidth),
                    ),
                  ),
                ],
              ),

            ),
            Container(
              color: NeutralColors.gunmetal.withOpacity(.10),
              height: 1/720*screenHeight,
              margin: EdgeInsets.only(top:19.5/720*screenHeight,left: 15/360*screenWidth,right: 15/360*screenWidth),
              //child: Divider(),
            ),

          ],
        ),
      ),
    );


  }
  return new Column(children: list);
}
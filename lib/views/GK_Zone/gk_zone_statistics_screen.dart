import 'package:flutter/material.dart';
import 'package:imsindia/resources/strings/gk_zone.dart';

import 'package:imsindia/components/stop_watch_timer.dart';
import 'package:imsindia/components/custom_scrollbar_component.dart';
import 'package:imsindia/resources/strings/prepare.dart';
import 'package:imsindia/utils/colors.dart';
import 'package:imsindia/views/GK_Zone/gk_zone_question_review_screen.dart';
import 'package:imsindia/views/GK_Zone/gk_zone_question_screen.dart';


var screenWidthTotal;
var screenHeightTotal;

var _safeAreaHorizontal;

var _safeAreaVertical;

var screenHeight;

var screenWidth;

class GkZoneStatisticScreenHtml extends StatefulWidget {
  final Stopwatch stopwatch;
  bool bargraphActiveStatus;
  final int backtoquesnum_bargraph;
  String timeTakenForQuestion;
  String statusForQuestion;
  GkZoneQuestionScreenUsingHtmlState GkZoneQuestionsScreen;
  GkZoneQuestionReviewScreenUsingHtmlState GkZoneQuestionsReviewScreen;

  GkZoneStatisticScreenHtml({this.stopwatch,this.bargraphActiveStatus,this.backtoquesnum_bargraph,this.GkZoneQuestionsScreen,this.GkZoneQuestionsReviewScreen,this.timeTakenForQuestion, this. statusForQuestion});

  @override
  _GkZoneStatisticScreenHtmlState createState() => _GkZoneStatisticScreenHtmlState();
}

class _GkZoneStatisticScreenHtmlState extends State<GkZoneStatisticScreenHtml> {
  final Dependencies dependencies = new Dependencies();
  final ScrollController controller = ScrollController();

  @override
  bool pressed = false;
  bool pressAttention = true;
  Widget build(BuildContext context) {
    var _mediaQueryData = MediaQuery.of(context);
    screenWidthTotal = _mediaQueryData.size.width;
    screenHeightTotal = _mediaQueryData.size.height;
    _safeAreaHorizontal =
        _mediaQueryData.padding.left + _mediaQueryData.padding.right;
    _safeAreaVertical =
        _mediaQueryData.padding.top + _mediaQueryData.padding.bottom;

    screenHeight = screenHeightTotal - _safeAreaVertical;
    screenWidth = screenWidthTotal - _safeAreaHorizontal;

    return Container(
      height: (476/720)*screenHeight,
      margin: EdgeInsets.only(left: (20/360)*screenWidth),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Align(
            alignment: Alignment.topLeft,
            child:
            InkWell(child:Container(
              width: (150/360)*screenWidth,
              height: (23/720)*screenHeight,
              // margin: EdgeInsets.only(top:(39/720)*screenHeight),
              child: Row(
                // mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                      child:Icon(
                        Icons.arrow_back_ios,
                        color: PrimaryColors.azure_Dark,
                        size: 15/720*screenHeight,
                      )
                  ),
                  Container(
                    margin: EdgeInsets.only(left: (8/360)*screenWidth),
                    child:Text(
                      GkzoneStatisticsScreenStrings.Text_BackToQuestion,
                      style: TextStyle(
                        color: Color.fromARGB(255, 0, 171, 251),
                        fontSize: (14/360)*screenWidth,
                        fontFamily: "IBMPlexSans",
                      ),
                      textAlign: TextAlign.left,
                    ),



                  ),
                ],
              ),
            ),
              onTap: (){
                setState(() {
                  dependencies.stopwatch = widget.stopwatch;
                  if(widget.GkZoneQuestionsScreen != null && widget.GkZoneQuestionsReviewScreen == null){
                    widget.GkZoneQuestionsScreen.setState(() {
                      widget.GkZoneQuestionsScreen.gkzoneHeader_status_question=3;
                      widget.GkZoneQuestionsScreen.pageViewController=PageController(initialPage: widget.backtoquesnum_bargraph-1);
                      widget.GkZoneQuestionsScreen.widget.stopwatch=widget.stopwatch;
                      widget.GkZoneQuestionsScreen.panelSlide();
                      widget.GkZoneQuestionsScreen.widget.gkzoneStatisticsCheck=false;

                    });
                  }
                  if(widget.GkZoneQuestionsReviewScreen != null && widget.GkZoneQuestionsScreen == null){
                    widget.GkZoneQuestionsReviewScreen.setState(() {
                      widget.GkZoneQuestionsReviewScreen.gkzoneHeader_status_question=3;
                      widget.GkZoneQuestionsReviewScreen.gkzoneSumbit_button_status=null;
                      widget.GkZoneQuestionsReviewScreen.pageViewController=PageController(initialPage: widget.backtoquesnum_bargraph-1);
                      widget.GkZoneQuestionsReviewScreen.widget.stopwatch=widget.stopwatch;
                      widget.GkZoneQuestionsReviewScreen.panelSlide();
                      widget.GkZoneQuestionsReviewScreen.widget.gkzoneStatisticsCheck=false;
                    });
                  }
                });
              },
            ),
          ),
          Align(
            alignment: Alignment.topLeft,

            child:Container(
              height:(22/720)*screenHeight ,
              //width: (81/360)*screenWidth,
              margin: EdgeInsets.only(top:(21/720)*screenHeight),
              child:Text(
                GkzoneStatisticsScreenStrings.Text_QuestionStats,
                style: TextStyle(
                  color: Color.fromARGB(255, 0, 3, 44),
                  fontSize: (16/360)*screenWidth,
                  fontFamily: "IBMPlexSans",
                  fontWeight: FontWeight.w500,
                ),
                textAlign: TextAlign.left,
              ) ,
            ),),
          Align(
            alignment: Alignment.topLeft,
            child:Container(
              width: (107/360)*screenWidth,
              height: (20/720)*screenHeight,
              margin: EdgeInsets.only(top:(10/720)*screenHeight),
              decoration: BoxDecoration(
                // color:  (widget.statusForQuestion=="true")? PrimaryColors.kelly_green:(widget.statusForQuestion=="false")? PrimaryColors.dark_coral: Colors.yellow,
                border: Border.all(
                  color:  (widget.statusForQuestion=="true")? PrimaryColors.kelly_green:(widget.statusForQuestion=="false")? PrimaryColors.dark_coral: Colors.yellow,

                  width: 1,
                ),
                borderRadius: BorderRadius.all(Radius.circular(2)),
              ),
              child:  Center(child:Text(
                (widget.statusForQuestion=="true")? Preparequestions.correctStatus:(widget.statusForQuestion=="false")? Preparequestions.wrongStatusForReview: Preparequestions.skippedStatus,
                style: TextStyle(
                  color:(widget.statusForQuestion=="true")? PrimaryColors.kelly_green:(widget.statusForQuestion=="false")? PrimaryColors.dark_coral: Colors.yellow,
                  fontSize: (12/720)*screenHeight,
                  fontFamily: "IBMPlexSans",
                  fontWeight: FontWeight.w500,
                ),
                textAlign: TextAlign.center,
              ),),
            ),),
          DraggableScrollbar(
            controller: controller,
            heightScrollThumb: 78.0/720*screenHeight,
            weightScrollThumb: 3/360*screenWidth,
            colorScrollThumb: Color(0xffcecece),
            marginScrollThumb: EdgeInsets.only(right: 6/360*screenWidth,top:100/720*screenHeight),

            child:Container(
              width: 320/360*screenWidth,
              decoration: BoxDecoration(
                color: Color.fromARGB(255, 242, 246, 248),
                borderRadius: BorderRadius.all(Radius.circular(5)),
              ),
              margin: EdgeInsets.only(top: 32/720*screenHeight),
              child: Column(
                children: [
                  Container(
                    height: 43/720*screenHeight,
                    margin: EdgeInsets.only(left: 20/360*screenWidth, top: 20/720*screenHeight, right: 99/360*screenWidth),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: [
                        Align(
                          alignment: Alignment.topLeft,
                          child: Container(
                            width: 88/360*screenWidth,
                            height: 43/720*screenHeight,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  GkzoneStatisticsScreenStrings.Text_Timetaken,
                                  style: TextStyle(
                                    color: Color.fromARGB(255, 153, 154, 171),
                                    fontSize: 12/360*screenWidth,
                                    fontFamily: "IBMPlexSans",
                                    fontWeight: FontWeight.w500,
                                  ),
                                  textAlign: TextAlign.left,
                                ),
                                Container(
                                  margin: EdgeInsets.only(top: 1/720*screenHeight),
                                  child: Text(
                                    widget.timeTakenForQuestion,
                                    style: TextStyle(
                                      color: Color.fromARGB(255, 0, 3, 44),
                                      fontSize: 14/720*screenHeight,
                                      fontFamily: "IBMPlexSans",
                                      fontWeight: FontWeight.w500,
                                    ),
                                    textAlign: TextAlign.left,
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                        Spacer(),
//                      Align(
//                        alignment: Alignment.topLeft,
//                        child: Container(
//                          width: 90/360*screenWidth,
//                          height: 44/720*screenHeight,
//                          child: Column(
//                            crossAxisAlignment: CrossAxisAlignment.start,
//                            children: [
//                              Text(
//                                GkzoneStatisticsScreenStrings.Text_AvgTimetaken,
//                                style: TextStyle(
//                                  color: Color.fromARGB(255, 153, 154, 171),
//                                  fontSize: 12/360*screenWidth,
//                                  fontFamily: "IBMPlexSans",
//                                  fontWeight: FontWeight.w500,
//                                ),
//                                textAlign: TextAlign.left,
//                              ),
//                              Container(
//                                margin: EdgeInsets.only(top: 1/720*screenHeight),
//                                child: Text(
//                                  GkzoneStatisticsScreenStrings.Text_ValueAvgTimetaken,
//                                  style: TextStyle(
//                                    color: Color.fromARGB(255, 0, 3, 44),
//                                    fontSize: 14/720*screenHeight,
//                                    fontFamily: "IBMPlexSans",
//                                    fontWeight: FontWeight.w500,
//                                  ),
//                                  textAlign: TextAlign.left,
//                                ),
//                              ),
//                            ],
//                          ),
//                        ),
//                      ),
                      ],
                    ),
                  ),
//                Container(
//                  height: 43/720*screenHeight,
//                  margin: EdgeInsets.only(left: 20/360*screenWidth, top: 30/720*screenHeight, right: 99/360*screenWidth),
//
//                  child: Row(
//                    crossAxisAlignment: CrossAxisAlignment.stretch,
//                    children: [
//                      Align(
//                        alignment: Alignment.topLeft,
//                        child: Container(
//                          width: 88/360*screenWidth,
//                          height: 43/720*screenHeight,
//                          child: Column(
//                            crossAxisAlignment: CrossAxisAlignment.start,
//                            children: [
//                              Text(
//                                CalculationScreenStrings.Text_Accuracy,
//                                style: TextStyle(
//                                  color: Color.fromARGB(255, 153, 154, 171),
//                                  fontSize: 12/360*screenWidth,
//                                  fontFamily: "IBMPlexSans",
//                                  fontWeight: FontWeight.w500,
//                                ),
//                                textAlign: TextAlign.left,
//                              ),
//                              Container(
//                                margin: EdgeInsets.only(top: 1/720*screenHeight),
//                                child: Text(
//                                  GkzoneStatisticsScreenStrings.Text_ValueAccurcy,
//                                  style: TextStyle(
//                                    color: Color.fromARGB(255, 0, 3, 44),
//                                    fontSize: 14/720*screenHeight,
//                                    fontFamily: "IBMPlexSans",
//                                    fontWeight: FontWeight.w500,
//                                  ),
//                                  textAlign: TextAlign.left,
//                                ),
//                              ),
//                            ],
//                          ),
//                        ),
//                      ),
//                      Spacer(),
//                      Align(
//                        alignment: Alignment.topLeft,
//                        child: Container(
//                          width: 88/360*screenWidth,
//                          height: 43/720*screenHeight,
//                          child: Column(
//                            crossAxisAlignment: CrossAxisAlignment.start,
//                            children: [
//                              Text(
//                                GkzoneStatisticsScreenStrings.Text_AttemptedBy,
//                                style: TextStyle(
//                                  color: Color.fromARGB(255, 153, 154, 171),
//                                  fontSize: 12/360*screenWidth,
//                                  fontFamily: "IBMPlexSans",
//                                  fontWeight: FontWeight.w500,
//                                ),
//                                textAlign: TextAlign.left,
//                              ),
//                              Container(
//                                margin: EdgeInsets.only(top: 1/720*screenHeight),
//                                child: Text(
//                                  GkzoneStatisticsScreenStrings.Text_ValueAttemptedBy,
//                                  style: TextStyle(
//                                    color: Color.fromARGB(255, 0, 3, 44),
//                                    fontSize: 14/720*screenHeight,
//                                    fontFamily: "IBMPlexSans",
//                                    fontWeight: FontWeight.w500,
//                                  ),
//                                  textAlign: TextAlign.left,
//                                ),
//                              ),
//                            ],
//                          ),
//                        ),
//                      ),
//                    ],
//                  ),
//                ),
//                Container(
//                  height: 43/720*screenHeight,
//                  margin: EdgeInsets.only(left: 20/360*screenWidth, top: 30/720*screenHeight, right: 99/360*screenWidth,bottom: 14/720*screenHeight),
//                  child: Row(
//                    crossAxisAlignment: CrossAxisAlignment.stretch,
//                    children: [
//                      Align(
//                        alignment: Alignment.topLeft,
//                        child: Container(
//                          width: 88/360*screenWidth,
//                          height: 43/720*screenHeight,
//                          child: Column(
//                            crossAxisAlignment: CrossAxisAlignment.start,
//                            children: [
//                              Text(
//                                GkzoneStatisticsScreenStrings.Text_PValue,
//                                style: TextStyle(
//                                  color: Color.fromARGB(255, 153, 154, 171),
//                                  fontSize: 12/360*screenWidth,
//                                  fontFamily: "IBMPlexSans",
//                                  fontWeight: FontWeight.w500,
//                                ),
//                                textAlign: TextAlign.left,
//                              ),
//                              Container(
//                                margin: EdgeInsets.only(top: 1/720*screenHeight),
//                                child: Text(
//                                  GkzoneStatisticsScreenStrings.Text_ValuePValue,
//                                  style: TextStyle(
//                                    color: Color.fromARGB(255, 0, 3, 44),
//                                    fontSize: 14/720*screenHeight,
//                                    fontFamily: "IBMPlexSans",
//                                    fontWeight: FontWeight.w500,
//                                  ),
//                                  textAlign: TextAlign.left,
//                                ),
//                              ),
//                            ],
//                          ),
//                        ),
//                      ),
//                      Spacer(),
//                      Align(
//                        alignment: Alignment.topLeft,
//                        child: Container(
//                          width: 88/360*screenWidth,
//                          height: 43/720*screenHeight,
//                          child: Column(
//                            crossAxisAlignment: CrossAxisAlignment.start,
//                            children: [
//                              Text(
//                                GkzoneStatisticsScreenStrings.Text_ABCApproach,
//                                style: TextStyle(
//                                  color: Color.fromARGB(255, 153, 154, 171),
//                                  fontSize: 12/360*screenWidth,
//                                  fontFamily: "IBMPlexSans",
//                                  fontWeight: FontWeight.w500,
//                                ),
//                                textAlign: TextAlign.left,
//                              ),
//                              Container(
//                                margin: EdgeInsets.only(top: 1/720*screenHeight),
//                                child: Text(
//                                  GkzoneStatisticsScreenStrings.Text_ValueABCApproach,
//                                  style: TextStyle(
//                                    color: Color.fromARGB(255, 0, 3, 44),
//                                    fontSize: 14/720*screenHeight,
//                                    fontFamily: "IBMPlexSans",
//                                    fontWeight: FontWeight.w500,
//                                  ),
//                                  textAlign: TextAlign.left,
//                                ),
//                              ),
//                            ],
//                          ),
//                        ),
//                      ),
//                    ],
//                  ),
//                ),
                ],
              ),
            ),),

        ],

      ),


    );
  }
}
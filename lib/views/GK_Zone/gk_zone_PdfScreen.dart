
//import 'package:advance_pdf_viewer_fork/advance_pdf_viewer_fork.dart';
import 'package:flutter/material.dart';
//import 'package:advance_pdf_viewer/advance_pdf_viewer.dart';
import 'package:flutter_svg/svg.dart';
import 'package:imsindia/utils/colors.dart';
import 'package:imsindia/utils/svg_images/prepare_svg_images.dart';
import 'package:pdf_viewer_jk/pdf_viewer_jk.dart';


class GKZonePdfScreen extends StatefulWidget {
  var pdflink;

  @override
  GKZonePdfScreenState createState() => GKZonePdfScreenState();

  GKZonePdfScreen(this.pdflink);
}

class GKZonePdfScreenState extends State<GKZonePdfScreen> {

  bool _isLoading = true;
  PDFDocument document;

  loadDocument(String url) async {
    document = await PDFDocument.fromURL(url);
    if (mounted) {
      setState(() => _isLoading = false);
    }
  }


  void initState(){
    super.initState();
    print(widget.pdflink.toString());

    loadDocument(widget.pdflink.toString());

  }

  @override
  Widget build(BuildContext context) {
    final screenHeight = MediaQuery.of(context).size.height;//720dp
    final screenWidth = MediaQuery.of(context).size.width;//360dp
    Future<bool> _onWillPop() {
      setState(() {
        Navigator.pop(context,true);
      });
    }
    return  WillPopScope(
      onWillPop: _onWillPop,
      child: Scaffold(
        body: Column(
          children: [
            Container(
              margin: EdgeInsets.only(
                top: (42 / 720) * screenHeight,
              ),
              child: Row(
                //crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  InkWell(
                    onTap: () {
                      Navigator.pop(context,true);
                    },
                    child: Container(
                      width: (58 / 360) * screenWidth,
                      height: (30.0 / 720) * screenHeight,
                      margin: EdgeInsets.only(left: 0.0),
                      child: SvgPicture.asset(
                        getPrepareSvgImages.backIcon,
                        height: (5 / 720) * screenHeight,
                        width: (14 / 360) * screenWidth,
                        fit: BoxFit.none,
                      ),
                    ),
                  ),
                  Container(
                    height: (20 / 720) * screenHeight,
                    // margin: EdgeInsets.only(left: (20 / 360) * screenWidth),
                    child: Text(
                      "GK Zone",
                      style: TextStyle(
                        color: NeutralColors.black,
                        fontSize: (16 / 720) * screenHeight,
                        fontFamily: "IBMPlexSans",
                        fontWeight: FontWeight.w500,
                      ),
                      textAlign: TextAlign.left,
                    ),
                  ),
                ],
              ),
            ),
            Expanded(
              child: Center(
                child: _isLoading
                    ? Center(child: CircularProgressIndicator())
                    : PDFViewer(
                  document: document,
                  zoomSteps: 1,
                  showPicker: false,
                  //uncomment below line to preload all pages
                  lazyLoad: true,
                  // uncomment below line to scroll vertically
                  // scrollDirection: Axis.vertical,
                ),
              ),
            ),

          ],
        ),

      ),
    );
  }
}

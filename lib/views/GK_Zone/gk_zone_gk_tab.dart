import 'package:flutter/material.dart';
import 'package:imsindia/api/bloc/services.dart';
import 'package:imsindia/resources/strings/gk_zone.dart';
import 'package:imsindia/routers/routes.dart';
import 'package:imsindia/utils/Url.dart';
import 'package:imsindia/utils/colors.dart';
import 'package:imsindia/utils/screen_util.dart';
import 'package:imsindia/utils/svg_images/gk_zone_svg_images.dart';
import 'package:imsindia/views/GK_Zone/gk_zone_quiz_screen.dart';
import 'package:imsindia/views/GK_Zone/gk_zone_week5_sept.dart';
import 'package:percent_indicator/circular_percent_indicator.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'gk_zone_lists_pdf.dart';
import 'package:imsindia/utils/global.dart' as global;

import 'gk_zone_question_review_screen.dart';
import 'gk_zone_question_screen.dart';


final titles = ['Science & Technology', 'Geography', 'World History', ];

final icons = [GKZoneAssets.scienceSvgIcon, GKZoneAssets.geographySvgIcon,
  GKZoneAssets.historySvgIcon,];

bool loadForReviewAndResumeButoon = false;
 var authToken;


var generalKnowledgeTitle = [];
class GKZoneGKTab extends StatefulWidget {
  
  @override
  _GKZoneGKTabState createState() => _GKZoneGKTabState();
}

class _GKZoneGKTabState extends State<GKZoneGKTab> {



  var userId;
  var authorization;
  var courseID;
  var generalKnowledgeSubCategory;
  var examWiseSubcategory;
  
  var generalKnowledgeId;
  var examWiseTitle;
  var examWiseId;
  var generalKnowledgeRead;
  var generalKnowledgeTotal;
  var generalKnowledgeTestStatus;
  var generalKnowledgeTestID;
  var generalKnowledgeShowReadButton;

  void getUserId() async {

  }
  void getCategoryListt() async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    userId = prefs.getString("userId");
    authorization = prefs.getString("loginAccessToken");
    courseID = prefs.getString("courseId");
    Map postdata = {
      "userId": userId,
      "Authorization": authorization,
      "courseId":courseID
    };
    print("Post Data"+postdata.toString());
    ApiService().postAPIStatusCodeFromResponse(URL.GKZone_CategoryList, postdata, global.headers).then((result) {
      //print("Result"+result.toString());
      setState(() {
        if (result[0]['success']==true) {

          generalKnowledgeTitle = [];
          generalKnowledgeId = [];
          generalKnowledgeRead = [];
          generalKnowledgeTotal = [];
          generalKnowledgeTestID=[];
          generalKnowledgeTestStatus=[];
          generalKnowledgeShowReadButton = [];
          //print("Result value"+result[0]['data'].toString());
          for (var index = 0; index < result[0]['data'].length; index++){
            if(result[0]['data'][index]['categoryTitle'] != "SNAP"){

              //subcategory.add(result[0]['data'][index]['subCategory']);
              if(result[0]['data'][index]['categoryTitle'] == "General Knowledge"){
                generalKnowledgeSubCategory = result[0]['data'][index]['subCategory'];
              }
            }
          }

          for (var index = 0; index < generalKnowledgeSubCategory.length; index++){
            generalKnowledgeTitle.add(generalKnowledgeSubCategory[index]['title']);
            generalKnowledgeId.add(generalKnowledgeSubCategory[index]['id']);
            generalKnowledgeRead.add(generalKnowledgeSubCategory[index]['read']);
            generalKnowledgeTotal.add(generalKnowledgeSubCategory[index]['total']);
            generalKnowledgeTestID.add(generalKnowledgeSubCategory[index]['testId']);
            generalKnowledgeTestStatus.add(generalKnowledgeSubCategory[index]['testStatus']);
            generalKnowledgeShowReadButton.add(generalKnowledgeSubCategory[index]['showReadButton']);
          }
         
          print("exam wise"+generalKnowledgeTestStatus.toString());
          print("GK read"+generalKnowledgeRead.toString());
          print("GK total"+generalKnowledgeTotal.toString());

        } else {

        }
      });
    });
  }


  /// set access token to prod test launch/reveiw screen...............
  void setAccessTokenForProdTestLaunch(value) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString("accessTokenGkzone", value);
  }

  void setTestStatusToCallTestLaunchOrReviseApi(value) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString("testStatusgkzone", value);
    print(value + "valueeeeeeeeeee");
    print(prefs.setString("testStatusgkzone", value));
  }

@override
  void initState() {
    // TODO: implement initState
    super.initState();
    getCategoryListt();
  }

  @override
  Widget build(BuildContext context) {
    var _mediaQueryData = MediaQuery.of(context);
    double screenWidth = _mediaQueryData.size.width;
    double screenHeight = _mediaQueryData.size.height;
    return  Scaffold(
      backgroundColor: Colors.transparent,
      body: Container(
        //color: Colors.white,
        child: Column(
          children: <Widget>[
            Container(
              margin: EdgeInsets.only(bottom: (15/Constant.defaultScreenHeight)*screenHeight),
              child: Container(),
            ),
            generalKnowledgeTitle.length == 0
                  ? new Container(
                  margin: EdgeInsets.only(
                      top: (200.0 / 720) * screenHeight),
                  child:
                  Center(child: CircularProgressIndicator())):
            Expanded(
              child: ListView.builder(
                itemCount: generalKnowledgeTitle.length,
                itemBuilder: (context, index) {
                  return Container(
                    // height: (140/Constant.defaultScreenHeight)*screenHeight,
                    margin: EdgeInsets.only(left: (20/Constant.defaultScreenWidth)*screenWidth,
                      right: (20/Constant.defaultScreenWidth)*screenWidth,
                      top:(10/Constant.defaultScreenHeight)*screenHeight,
                      bottom:(10/Constant.defaultScreenHeight)*screenHeight,
                    ),
                    decoration: BoxDecoration(
                      border: Border.all(
                        color: NeutralColors.gunmetal.withOpacity(0.10),
                        width: (1/Constant.defaultScreenWidth)*screenWidth,
                      ),
                      borderRadius: BorderRadius.all(Radius.circular(5)),
                    ),
                    child: Column(
                      children: <Widget>[
                        Container(
                          margin: EdgeInsets.only(top:(20/Constant.defaultScreenHeight)*screenHeight,
                              left:(23/Constant.defaultScreenWidth)*screenWidth),
                          child: Row(
                            children: <Widget>[
                              Container(
                                child: generalKnowledgeRead[index]/generalKnowledgeTotal[index] != 1.0
                                    ? CircularPercentIndicator(
                                  radius: 35.0,
                                  lineWidth: 4.0,
                                  percent: generalKnowledgeRead[index]/generalKnowledgeTotal[index],
                                  // center: percentiles[index]==1.0 ? GKZoneAssets.tickSvgIcon : Text(""),
                                  // progressColor: NeutralColors.kelly_green,
                                  backgroundColor:
                                  const Color(0xffeeeeee),
                                  progressColor:
                                  NeutralColors.kelly_green,
                                )
                                    : GKZoneAssets.tickSvgIcon,
                              ),
                              Container(
                                //color: Colors.red,
                                width: (230/Constant.defaultScreenWidth)*screenWidth,
                                padding: EdgeInsets.only(left: (18/Constant.defaultScreenWidth)*screenWidth,right: (18/Constant.defaultScreenWidth)*screenWidth),
                                child: Text(
                                  generalKnowledgeTitle[index],
                                  style: TextStyle(
                                      fontFamily: "IBMPlexSans",
                                      color: const Color(0xff1f252b),
                                      fontWeight: FontWeight.w500,
                                      fontSize: (16/Constant.defaultScreenWidth)*screenWidth
                                  ),
                                  overflow: TextOverflow.ellipsis,
                                  maxLines: 2,
                                ),
                              )
                            ],
                          ),
                        ),
                      
                      Container(
                          margin: EdgeInsets.only(top:(20/Constant.defaultScreenHeight)*screenHeight,
                              bottom: (20/Constant.defaultScreenHeight)*screenHeight),
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[

                              generalKnowledgeShowReadButton[index] == "Yes" ?
                              GestureDetector(
                                onTap:(){
                                  AppRoutes.push(context, GKZoneListOfPdf(generalKnowledgeId[index])).then((value)
                                              {
                                                print("qwertyuu");
                                                print(value);
                                                generalKnowledgeTitle = [];
                                                getCategoryListt();
                                              });
                                },
                                child: Container(
                                  height: (40/Constant.defaultScreenHeight)*screenHeight,
                                  width: (130/Constant.defaultScreenWidth)*screenWidth,
                                  decoration: BoxDecoration(
                                    gradient: LinearGradient(
                                      begin: Alignment(-0.029, 0.472),
                                      end: Alignment(1.049, 0.538),
                                      stops: [
                                        0,
                                        1,
                                      ],
                                      colors: [SemanticColors.light_purpely, SemanticColors.dark_purpely],
                                    ),
                                    borderRadius: BorderRadius.all(Radius.circular(2)),
                                  ),
                                  child: Align(
                                    alignment: Alignment.center,
                                    child: Text(
                                      GKZoneStrings.read_label.toUpperCase(),
                                      style: TextStyle(
                                        fontSize: (14/Constant.defaultScreenWidth)*screenWidth,
                                        fontWeight: FontWeight.w500,
                                        fontFamily: "IBMPlexSans",
                                        color: Colors.white,
                                      ),
                                      textAlign: TextAlign.center,
                                    ),
                                  ),
                                ),
                              ) : Container(),

                              generalKnowledgeTestID[index]=="" ? Container () :
                              GestureDetector(
                                onTap:()
                                {
                                  setTestStatusToCallTestLaunchOrReviseApi(generalKnowledgeTestStatus[index]);
                                  global.getToken.then((t) {
                                      Map postdata = {
                                        "testId": generalKnowledgeTestID[index]
                                      };
                                      print("POSTMAN");
                                      print(postdata);

                                      ApiService()
                                          .postAPI(
                                          URL.TEST_LAUNCH_FOR_YES_PREPARE,
                                          postdata,
                                          t)
                                          .then((result) {
                                        setState(() {
                                          if (result[0].toString().toLowerCase() == 'successfully resume'.toLowerCase() || result[0].toString().toLowerCase() == 'successfully created'.toLowerCase()) {
                                            var totalData = result[1]['data'];
                                            global.tokenForTestLaunchApi = totalData['token'];
                                            print("global");
                                            print(global.tokenForTestLaunchApi);
                                            setAccessTokenForProdTestLaunch(totalData['token']);
                                            print("start invoked");
                                            print(totalData['token']);
                                            if(generalKnowledgeTestStatus[index] == "Completed")
                                            {
                                              print(generalKnowledgeTestStatus[index]);
                                              AppRoutes.push(context, GkZoneQuestionReviewScreenUsingHtml());
                                            }else{
                                              print("value of i for question screen");
                                              Navigator.of(context).push(new MaterialPageRoute(builder: (_)=>GkZoneQuestionScreenUsingHtml())).then((value)
                                              {
                                                print("qwertyuu");
                                                print(value);
                                                generalKnowledgeTitle = [];
                                                getCategoryListt();
                                              });
                                            }
                                            print("outsideifelse");
                                          } else {}
                                        });
                                      });
                                  });                                
                                  },
                                child: Container(
                                  height: (40/Constant.defaultScreenHeight)*screenHeight,
                                  width: (130/Constant.defaultScreenWidth)*screenWidth,
                                  margin: EdgeInsets.only(left: (15/Constant.defaultScreenWidth)*screenWidth,
                                  ),
                                  decoration: BoxDecoration(
                                    gradient: LinearGradient(
                                      begin: Alignment(-0.029, 0.472),
                                      end: Alignment(1.049, 0.538),
                                      stops: [
                                        0,
                                        1,
                                      ],
                                      colors: [SemanticColors.light_purpely, SemanticColors.dark_purpely],
                                    ),
                                    borderRadius: BorderRadius.all(Radius.circular(2)),
                                  ),
                                  child: Align(
                                    alignment: Alignment.center,
                                    child: Text(
                                      loadForReviewAndResumeButoon == true ?
                                      GKZoneStrings.waitString :
                                      (generalKnowledgeTestStatus[index] == 'Start') ? GKZoneStrings.sloveString :
                                      (generalKnowledgeTestStatus[index] == 'In-Progress') ? GKZoneStrings.ResumeString :
                                      ((generalKnowledgeTestStatus[index] == 'Completed')) ? GKZoneStrings.ReviewString : "abc",                                      style: TextStyle(
                                        fontSize: (14/Constant.defaultScreenWidth)*screenWidth,
                                        fontWeight: FontWeight.w500,
                                        color: Colors.white,
                                        fontFamily: "IBMPlexSans",
                                      ),
                                      textAlign: TextAlign.center,
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        )
                      ],
                    ),
                  );
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}

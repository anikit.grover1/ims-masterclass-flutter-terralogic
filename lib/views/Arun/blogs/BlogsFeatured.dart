import 'package:flutter/material.dart';
import 'package:imsindia/utils/colors.dart';

import '../ims_utility.dart';

const actualHeight = 720;
const actualWidth = 360;
var screenWidthTotal;
var screenHeightTotal;
var _safeAreaHorizontal;
var _safeAreaVertical;
var screenHeight;
var screenWidth;
var monVal = true;

class Featured extends StatefulWidget {
  int i;
  List listofposts;
  List listoflink;
  List listofimage;
  List listoftime;
  Featured(this.i, this.listofposts, this.listoflink,this.listofimage,this.listoftime);

  @override
  _FeaturedState createState() => _FeaturedState();
}



class _FeaturedState extends State<Featured> {

  @override
  void initState(){
    super.initState();
    print(widget.listofposts);
    print(widget.listoflink);

  }

  @override
  Widget build(BuildContext context) {
    CalculateScreen(context);

    return Container(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          
          Expanded(
            //height: screenHeight * 345 / actualHeight,
            child: ListView.builder(
//              separatorBuilder: (BuildContext context, int index) => Center(child: new Divider()),
//              shrinkWrap: true,
//              physics: ScrollPhysics(),
      itemCount: widget.listofposts.length,
      itemBuilder: (context, index) {
        final item = widget.listofposts[index];
        return index == 0 ?
        GestureDetector(
          onTap: (){
            setState(() {
        try {
                  launchURL('${widget.listoflink[index]}');
                } catch (e) {
                  print(e.toString());
                }
                });
          },
          child: Container(
              alignment: Alignment.topLeft,
              child: Container(
                 color: Color(0xFF0a0072).withOpacity(0.75),
                width: 360 * screenWidth /360,
                child: Padding(
                    padding: EdgeInsets.only(
                    top: screenHeight * 10 / actualHeight,
                    left: screenWidth * 20 / actualWidth,
                    right: screenWidth * 97.0 / actualWidth),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Padding(
                        padding: EdgeInsets.only(
                            top: screenHeight * 20 / actualHeight),
                        child: Text(widget.listofposts[0],
                            maxLines: 3,
                            overflow: TextOverflow.ellipsis,
                            style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.w700,
                                fontFamily: "IBMPlexSans",
                                fontStyle: FontStyle.normal,
                                fontSize: screenHeight * 18.0 / actualHeight)),
                      ),
                      // Padding(
                      //   padding: EdgeInsets.only(
                      //       top: screenHeight * 20 / actualHeight),
                      //   child: Text(
                      //       "While most people enjoy casino gambling, sports betting, lottery and bingo playing for the fun and excitement it provides.",
                      //       style: TextStyle(
                      //           color: Colors.white,
                      //           fontWeight: FontWeight.w400,
                      //           fontFamily: "IBMPlexSans",
                      //           fontStyle: FontStyle.normal,
                      //           fontSize: screenHeight * 14.0 / actualHeight)),
                      // ),
                      Padding(
                        padding: EdgeInsets.only(
                            top: screenHeight * 20 / actualHeight),
                        child: Text(
                            widget.listoftime[0],
                            style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.w400,
                                fontFamily: "IBMPlexSans",
                                fontStyle: FontStyle.normal,
                                fontSize: screenHeight * 12.0 / actualHeight)),
                      ),
                    ],
                  ),
                ),
              ),
              height: 250 * screenHeight / actualHeight,
              width: double.infinity,
              // foregroundDecoration: BoxDecoration(
              //     color: NeutralColors.purpleish_blue.withOpacity(0.5),
              // ),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(1),
                 
                  
                  
                  image: DecorationImage(
                      image: new NetworkImage(
                          widget.listofimage[0],),
                      fit: BoxFit.fill)
                      ),
            ),
        )
        : 
        Container(
            child: ListTile(
//            title: Text(item),

              title: Container(
//                screenHeight * 30 / actualHeight,
       // height: screenHeight * 118 / actualHeight,
        width: double.infinity,
        margin: EdgeInsets.only(
          top: 24.5/ actualWidth*screenWidth,
          left: screenWidth * 10.0 / actualWidth,
          right: screenWidth * 10.0 / actualWidth,
        ),
        child: Column(
          children: <Widget>[

            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Expanded(
                  flex: 6,
                  child: GestureDetector(
                    onTap: () {
                      //                                  <-- onTap
                      setState(() {
                        try {
                          launchURL('${widget.listoflink[index]}');
                        } catch (e) {
                          print(e.toString());
                        }
                      });
                    },

                    child: Padding(
                      padding: EdgeInsets.only(
                          right: screenWidth * 15.0 / actualWidth),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            item,
                            maxLines: 2,
                            overflow: TextOverflow.ellipsis,
                            textAlign: TextAlign.left,
                            style: TextStyle(
                                fontFamily: "IBMPlexSans",
                                fontStyle: FontStyle.normal,
                                fontWeight: FontWeight.w500,
                                color: ImsColors.dark_navy_blue,
                                fontSize:
                                    screenHeight * 14.0 / actualHeight),
                          ),
                          Container(
                            margin: EdgeInsets.only(
                                top: screenHeight * 17 / actualHeight),
                            child: Text(
                              widget.listoftime[index],
                              textAlign: TextAlign.left,
                              maxLines: 1,
                              style: TextStyle(
                                  fontFamily: "IBMPlexSans",
                                  fontStyle: FontStyle.normal,
                                  fontWeight: FontWeight.w500,
                                  color: ImsColors.bluey_grey,
                                  fontSize: screenHeight *
                                      12.0 /
                                      actualHeight),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
                widget.listofimage[index] != null ? Expanded(
                  flex: 3,
                  child: ClipRRect(
                    borderRadius: new BorderRadius.circular(4.0),
                    child: Container(
                      width: double.infinity,
                      height: screenHeight * 80 / actualHeight,
                      child: FittedBox(
                        fit: BoxFit.fill,
                        child: widget.listofimage[index].toString() == "" ?
                                  Image.asset('assets/images/imageplaceholder.jpg')
                                :FadeInImage.assetNetwork(
                                  placeholder: 'assets/images/imageplaceholder.jpg',
                                  image:  widget.listofimage[index].toString(),
                                  imageErrorBuilder: (context, error, stackTrace) {
                                    return Container(
                                      child: Image.asset("assets/images/imageplaceholder.jpg"),
                                    );
                                  },
                                )
                      ),
                    ),
                  ),
                ): Container(),
              ],
            ),
//                    Container(
//                      height: 1 / actualHeight * screenHeight,
//                      width: double.infinity,
//                      color: ImsColors.iceBlue,
//                    )

            Container(
              child: Visibility(
                visible: index > 0,
                child: Padding(
                  padding:EdgeInsets.only(top:30),
                  child: Center(
                    child: index == widget.listofposts.length - 1 ? Container() : Container(

                      height: screenHeight * 2/ actualHeight,

                      width: double.infinity,
                      color: ImsColors.ice_blue,
                    ),
                  ),
                ),
              ),
            ),


          ],
        )),

            ),
          );

      },
//              separatorBuilder: (context, index) {
//                return Center(
//                  child: Padding(
//                      padding: EdgeInsets.only(top:24.5,bottom:14.5,left:screenWidth * 21.0 / actualWidth ,
//                      right: screenWidth * 19.0 / actualWidth),
//
//                      child: Divider(),
//                  ),
//                );
//              },


    ),
          ),
        ],
      ),
    );
  }
}


class Item {
  var title = 'How to analyse Mock CATs: Mocks liya ab kya karna hai?';
  var subtitle = '01-Aug-2018';
  var subtitle2 = '50 min read';
  var imgUrl =
      'https://thumbs.dreamstime.com/b/inspirational-quote-there-blessings-everyday-find-them-create-treasure-beautiful-white-single-sinnia-flower-blossom-blurry-151653366.jpg';
}

void CalculateScreen(BuildContext context) {
  var _mediaQueryData = MediaQuery.of(context);
  screenWidthTotal = _mediaQueryData.size.width;
  screenHeightTotal = _mediaQueryData.size.height;
  _safeAreaHorizontal =
      _mediaQueryData.padding.left + _mediaQueryData.padding.right;
  _safeAreaVertical =
      _mediaQueryData.padding.top + _mediaQueryData.padding.bottom;

  screenHeight = screenHeightTotal - _safeAreaVertical;
  screenWidth = screenWidthTotal - _safeAreaHorizontal;
}

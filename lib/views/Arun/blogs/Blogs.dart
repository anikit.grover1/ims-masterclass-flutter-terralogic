import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:imsindia/components/bottom_navigation.dart';
import 'package:imsindia/components/navigation_bar.dart';
import 'package:imsindia/utils/Url.dart';
import 'package:imsindia/utils/colors.dart';
import 'package:imsindia/views/Arun/BlogContent.dart';
import 'package:imsindia/views/Arun/SqliteHelper.dart';
import 'package:imsindia/views/splash_screen/splash_screen.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../ims_utility.dart';
import 'BlogsFeatured.dart';
import 'package:http/http.dart' as http;

const actualHeight = 768;
const actualWidth = 360;
var screenWidthTotal;
var screenHeightTotal;
var _safeAreaHorizontal;
var _safeAreaVertical;
var screenHeight;
var screenWidth;
var monVal = true;


class Blogs extends StatefulWidget {
  @override
  BlogsState createState() => BlogsState();
}

class BlogsState extends State<Blogs> with TickerProviderStateMixin{

  var listoftabs = [];
  var listoftabids = [];
  var selectedcoursesforblog  ;
  var datacourse = [];
  var datatab = [];
  var dataposts = [];
  var listoftitles = [];
  var listoflinks = [];
  var listofposts = [];
  var listofDataLinks = [];
  List<Widget> fetchData = [] ;
  var listofalltabs = [];
  var objj;
  var listofalltabids = [];
  var course;
  var username;
  var userpin;
  var checkUsername = '';
  var checkUserpin = '';
  var checkcourse = '';
  TabController _tabController;
  List<Tab> tabs = List();
  var ttabs;
  var uniqueTabs ;
  List<Tab> databaseTablist = List();
  var listoflinksss= [];
  var listoftitlesss = [];
  var listofimages = [];
  var listoftimess = [];
  var databasearticleDataTitle;
  var databasearticleDataLink;
  var databasearticleDataImage;
  var databasearticleDataTime;
  var databaseImagelist = [];
  var databasePostlist = [];
  var databaseDataLinks = [];
  var databaseTimelist = [];
  var refreshKeyy = GlobalKey<RefreshIndicatorState>();

 // String course;

  void getBlogs() async {
    var getcoursesforblog = await http.get(Uri.parse(URL.BLOGS_API_COURSES_LIST + "100"));
    
    datacourse = jsonDecode(getcoursesforblog.body);
    
    print(datacourse.length);
    
    setState(() {
      if(datacourse != null){
        print("entered blog courses");
        print("course" + course);
        

        for (var index = 0; index < datacourse.length;index ++){
          print("course from API"+datacourse.toList()[index]["acf"]["course_id"]);
            if(datacourse.toList()[index]["acf"]["course_id"] == course){
            selectedcoursesforblog = datacourse.toList()[index]["id"];
            }
        }
    }
    });
    getTabs();
    print(selectedcoursesforblog);
    print("selectedcoursesforblog");
  }

  var tabvalue ;
  var tabList = [];
  var articleTitle ;
  var articleId ; 

  void getTabs() async {
    var gettabsforblog = await http.get(Uri.parse(URL.BLOGS_API_TAB_LIST + "100"));
    datatab = jsonDecode(gettabsforblog.body);
    print(datatab.length);
      setState(()  {
      if(datatab != null){
        print("entered tabs");
        for (var index = 0; index < datatab.length;index ++){
         // print(datatab.toList()[index]["acf"]["course"].toString());
         if(datatab.toList()[index]["name"]== "Uncategorized"){
           print("not required");
           tabList.add("");
         }
         else{
          tabList.add(datatab[index]);
          print("required");
           tabvalue = tabList[index]["acf"]["course"];
           print(tabvalue);
           if(tabList[index]["acf"]["course"] == false){
             print("false string");
           }
           else{
           for(var i = 0; i < tabList[index]["acf"]["course"].length ; i++){
            if(tabList[index]["acf"]["course"][i].toString() == selectedcoursesforblog.toString()){
              listofalltabids.add(datatab.toList()[index]["id"].toString());
              listofalltabs.add(datatab.toList()[index]["name"]);
            if(datatab.toList()[index]["id"].toString() == "95")
            {
              print("Tab not required for article of the week");
              articleTitle = datatab.toList()[index]["name"];
              articleId = datatab.toList()[index]["id"].toString();
            }
            else{
              listoftabids.add(datatab.toList()[index]["id"].toString());
              listoftabs.add(datatab.toList()[index]["name"]);
              tabs.add( new Tab(
              child: Text(
              datatab.toList()[index]["name"],
              ),
            ));
            }
            }
          }
         }
        }
      }
    }
    });
    print(tabs);
    print(tabs.length);
    print(tabList);
    print(tabList.length);
    getPosts();
    print(listoftabids);
    print(listoftabs);
   //  _tabController =  new TabController(vsync: this, length: tabs.length);
  }
  
  var articleDataTitle ;
  var articleDataLink ;
  var listofimageapi = [];
  var totalimageapi = [];
  var articleDataImageAPI;
  var listoftimes;

  var articleDataTitleList = []; 
  var articleDataLinkList = []; 
  var articleDataTimeList = []; 
  var articleDataImageAPIList = [];

  void getPosts() async {
    var getpostsforblog = await http.get(Uri.parse(URL.BLOGS_API_POST_LIST + "1000"));
    dataposts = jsonDecode(getpostsforblog.body);
    print(dataposts.length);
      setState(()  {
      if(dataposts != null){
        print("entered posts");
            for (var j = 0; j < listoftabids.length;j++){
           listoftitles = [];
           listoflinks = [];
           listofimageapi = [];
           listoftimes = [];
            for (var index = 0; index < dataposts.length;index ++){
            
            for (var i = 0 ; i < dataposts.toList()[index]["categories"].length ; i++){
             // print(listoftabids.contains(dataposts.toList()[index]["categories"][i].toString()));
                //  print("Category Id = "+dataposts.toList()[index]["categories"][i].toString() + " Cat Index i " + i.toString());
                //  print("Sub Cat ID = "+listoftabids[j].toString() + " J Values = "+ j.toString());
                 
                    
                  if(listoftabids[j].toString() == dataposts.toList()[index]["categories"][i].toString()){

                   // print("ListOfTitles = "+listoftabids[j].toString());
                   // print("title rendered = "+dataposts.toList()[index]["title"]["rendered"].toString());
                  for(var k = 0 ; k < dataposts.toList()[index]["ims_courses"].length ; k ++){
                   
                    if(selectedcoursesforblog.toString() == dataposts.toList()[index]["ims_courses"][k].toString()){
                    listoftitles.add(dataposts.toList()[index]["title"]["rendered"].toString());
                    listoflinks.add(dataposts.toList()[index]["acf"]["external_link"].toString());
                    if(dataposts.toList()[index]["_links"]["wp:featuredmedia"] != null)
                    {
                    listofimageapi.add(dataposts.toList()[index]["_links"]["wp:featuredmedia"][0]["href"].toString());
                    }else{
                      listofimageapi.add("");
                    }
                    listoftimes.add(dataposts.toList()[index]["date"]);
                    }
                  }
                    //imageApicall(dataposts.toList()[index]["_links"]["wp:featuredmedia"][0]["href"].toString());
              }

               if(articleId.toString() == dataposts.toList()[index]["categories"][i].toString()){
                 
                 for(var k = 0 ; k < dataposts.toList()[index]["ims_courses"].length ; k ++){
                    if(selectedcoursesforblog.toString() == dataposts.toList()[index]["ims_courses"][k].toString()){
                       articleDataTitleList.add(dataposts.toList()[index]["title"]["rendered"].toString());
                      
                      articleDataLinkList.add(dataposts.toList()[index]["acf"]["external_link"].toString());
                      
                      articleDataTimeList.add(dataposts.toList()[index]["date"].toString());
                      if(dataposts.toList()[index]["_links"]["wp:featuredmedia"] != null)
                      {
                      articleDataImageAPIList.add(dataposts.toList()[index]["_links"]["wp:featuredmedia"][0]["href"].toString());
                      }else{
                        articleDataImageAPIList.add("");
                      }
                    }else{
                      articleDataImageAPI = "";
                    }
                 }
               }
              print(listoftitles);
              print(listofimageapi);
              //break;
            }
            
          }
         listofposts.add(listoftitles);
         listofDataLinks.add(listoflinks);
         totalimageapi.add(listofimageapi);
         listoftotaltimes.add(listoftimes);
        }
    }
    });

    if(articleDataTitleList.length != 0 && articleDataTitleList.length != 0 && articleDataTimeList.length != 0 && articleDataImageAPIList.length != 0 )
    {
      articleDataTitle = articleDataTitleList[0];
      articleDataLink = articleDataLinkList[0];
      articleDataTime = articleDataTimeList[0];
      articleDataImageAPI = articleDataImageAPIList[0];
    }
    print(articleDataTitle.toString());
    print(listoftitles.length);
    print(listofposts.length);
    print(listoftitles);
    print(listofposts);
    //getData();
    getImageList();
    //saveDataDB();
  }
  
  var imageList = [];
  var totalImageList = [];
  var articleDataImage;

  getImageList() async{
     //for(var k = 0 ; k < totalimageapi.length; k++){
       if(articleDataImageAPI != ""){
      articleDataImage = await imageApicall(articleDataImageAPI);
    }
    for(var i = 0 ; i < totalimageapi.length; i++){
      print(totalimageapi);
      imageList = [];
      for(var j = 0; j < totalimageapi[i].length;j++){
        for(var k = 0 ; k < totalimageapi[i].length; k++){
          if(totalimageapi[i][j] != ""){
           imageList.add(await imageApicall(totalimageapi[i][j]));
          }else{
            imageList.add("");
          }
        break;
        }
      }
    totalImageList.add(imageList);
  }
    print("totalList"+totalImageList.toString());
    print("articleDataImage"+articleDataImage.toString());
    getDateList();
  }

  var timeList = [];
  var totalTimeList = [];
  var articleDataTime;
  var listoftotaltimes = [];
  var articleTime;

  getDateList() async{
    if(articleDataTime != null){
      articleTime = await dateConvert(articleDataTime);
    }
    for(var i = 0 ; i < listoftotaltimes.length; i++){
      print(listoftotaltimes);
      timeList = [];
      for(var j = 0; j < listoftotaltimes[i].length;j++){
        for(var k = 0 ; k < listoftotaltimes[i].length; k++){
        timeList.add(await dateConvert(listoftotaltimes[i][j]));
        break;
        }
      }
    totalTimeList.add(timeList);
  }
  saveDataDB();
  }

  Future dateConvert(String dateString) async
  {
    var monthList = ["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"];
    var mon;
    var day = DateTime.parse(dateString).day;
    var month = DateTime.parse(dateString).month;
    var year = DateTime.parse(dateString).year;
    // print("time convert day "+ day.toString());
    // print("time convert month "+ month.toString());
    // print("time convert year "+ year.toString());
    for(var i=0; i < monthList.length ;i++){
      if(i + 1 == month){
        setState(() {
           mon = monthList[i];
           print(mon);
        });
       
      }
    }
    if(day <= 9){
      //print("0$day-$mon-$year");
      return "0$day-$mon-$year";
    }
    else{
      //print("$day-$mon-$year");
       return "$day-$mon-$year";
    }
  }

  Future imageApicall(String api) async
  {
    var response = await http.get(Uri.parse(api));
    var decodeRes = jsonDecode(response.body);
    //print("decodedededed"+decodeRes["guid"]["rendered"].toString());
    if(decodeRes["guid"] != null){
      return decodeRes["guid"]["rendered"].toString();
    }
    else{
      return "";
    }
    
  }

  void getCourseId() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    course = prefs.getString("courseId");
    //print('course::::::'+prefs.getString("courseId"));
    getBlogs();


  }


//  void getCourseId() async {
//    SharedPreferences prefs = await SharedPreferences.getInstance();
//    course = prefs.getString("courseId");
//    username = prefs.getString(ImsStrings.sCurrentUserName);
//    userpin = prefs.getString(ImsStrings.sCurrentUserId);
//    checkUsername = prefs.getString("PrevioustUserName");
//    checkUserpin = prefs.getString("PreviousUserId");
//    checkcourse = prefs.getString("PreviouscourseId");
//    print('username::::::'+prefs.getString(ImsStrings.sCurrentUserName));
//    print(course + " equals " +checkcourse);
//    //print('checkUsername::::::'+prefs.getString("PreviousUserName"));
//    if(username != checkUsername && userpin != checkUserpin){
//      setUserIDforCheck();
//      getBlogs();
//      print("if part");
//    }
//    else if(course != checkcourse){
//      setUserIDforCheck();
//      getBlogs();
//      print("else if part");
//    }
//
//    else{
//      GetLocalSearchContentBlog();
//      print("else part");
//    }
//
//  }

//  void setUserIDforCheck() async {
//    SharedPreferences prefs = await SharedPreferences.getInstance();
//    prefs.setString("PreviousUserName", username);
//    prefs.setString("PreviousUserId", userpin);
//    prefs.setString("PreviouscourseId", course);
//  }


  @override
  void initState() {
    super.initState();
    objj = SqliteHelper('imsApp.db');
    objj.create();
    getCourseId();
   // getBlogs();

    GetLocalSearchContentBlog();
    
     print("initstate");
     //print(tabs.length);
  }
  
  @override
  void dispose(){
    super.dispose();
    _tabController.dispose();
  }

  
  void getData() async {
    for (var i = 0;i < databaseTablist.length;i++){
      fetchData.add(
        //Container(child: Text(i.toString()),));
       //Container(child: Text(i.toString()),);
      Featured(i,databasePostlist[i],databaseDataLinks[i],databaseImagelist[i],databaseTimelist[i]));
    }
  }

  void saveDataDB() async {
    await objj.deleteAllBlogs();
  //  print("should be 13 "+ listofalltitles.length.toString());
    print("should be 4 " + listofalltabs.length.toString());
    for (var j = 0; j < listofposts.length;j++){
      for (var k = 0; k < listofposts[j].length ; k++){
        InserDataBlog(listofposts[j][k],listofDataLinks[j][k],listoftabids[j], listoftabs[j],totalImageList[j][k],totalTimeList[j][k]);
      }
    }
    if(articleDataTitle != null && articleDataLink != null){
      InserDataBlog(articleDataTitle, articleDataLink, articleId, articleTitle,articleDataImage,articleTime);
    }
  }


  void InserDataBlog(String strContent, String strContent1, String strContent2,
      String strContent3,String strContent4,String strContent5) async {
    var bb = new BlogContent(
      content: strContent,
      content1: strContent1,
      content2: strContent2,
      content3: strContent3,
      content4: strContent4,
      content5: strContent5,
    );
    bb.toString();
    await objj.insertBlog(bb);
  }

  void GetLocalSearchContentBlog() async {
    itemsBlogs = await obj.getDataBlog();
    for (var i = 0; i < itemsBlogs.length; i++) {
      print(itemsBlogs.length);
      print('GetBlogSearchContent :: ${itemsBlogs[i]}');
      ttabs = itemsBlogs.map((o) => o.content3).toSet();
      print(ttabs.toList().toString());
      if(ttabs.toList().contains("Article of the week")){
        ttabs.remove("Article of the week");
        uniqueTabs = ttabs.toList();
      }
      else{
        uniqueTabs = ttabs.toList();
      }


    }
    if(uniqueTabs != null){
    for (var i = 0; i < uniqueTabs.length; i++) {
      databaseTablist.add( new Tab(
        child: Text(
          uniqueTabs[i],
        ),),);
    }
    }
    setState(() {
      _tabController =  new TabController(vsync: this, length: databaseTablist.length);
    });
    if(ttabs != null){
    for (var j = 0; j < ttabs.length;j++){
      listoftitlesss = [];
      listoflinksss = [];
      listofimages = [];
      listoftimess = [];
      for (var index = 0; index < itemsBlogs.length;index ++){

        for (var i = 0 ; i < itemsBlogs[index].content3.length ; i++){

          if(ttabs.toList()[j].toString() == itemsBlogs[index].content3.toString()){


            listoftitlesss.add(itemsBlogs[index].content.toString());
            listoflinksss.add(itemsBlogs[index].content1.toString());
            listofimages.add(itemsBlogs[index].content4.toString());
            listoftimess.add(itemsBlogs[index].content5.toString());
          }
          if("Article of the week" == itemsBlogs[index].content3.toString()){
            databasearticleDataTitle =  itemsBlogs[index].content.toString();
            databasearticleDataLink = itemsBlogs[index].content1.toString();
            databasearticleDataImage = itemsBlogs[index].content4.toString();
            databasearticleDataTime = itemsBlogs[index].content5.toString();
          }
          print(listoftitlesss);
          break;
        }
      }
      databasePostlist.add(listoftitlesss);
      databaseDataLinks.add(listoflinksss);
      databaseImagelist.add(listofimages);
      databaseTimelist.add(listoftimess);
    }
    }
    print(databasePostlist);
    getData();
  }
  // on dragging page refresh function will be activated
  Future<Null> refreshList() async {
    refreshKeyy.currentState?.show(atTop: false);
    await Future.delayed(Duration(seconds: 2));
    setState(() {
      print('Clicked on refresh');
      GetLocalSearchContentBlog();
    });

    return null;
  }




  @override
  Widget build(BuildContext context) {

    CalculateScreen(context);
    return Scaffold(
        appBar: AppBar(
          brightness: Brightness.light,
          elevation: 0.0,
          centerTitle: false,
          titleSpacing: 0.0,
          iconTheme: IconThemeData(
            color: Colors.black, //change your color here
          ),
          backgroundColor: Color.fromARGB(255, 255, 255, 255),

          title: Text(
            "Blogs",
            style: TextStyle(
                color: Colors.black,
                fontStyle: FontStyle.normal,
                fontWeight: FontWeight.w500,
                fontFamily: "IBMPlexSans",
                fontSize: 16.0 / 720 * screenHeight),
          ),
        ),
        drawer: NavigationDrawer(),
        bottomNavigationBar: BottomNavigation(),
        body: _tabController == null ? Center(child: CircularProgressIndicator()) : RefreshIndicator(
          displacement: 200,
                    color: SemanticColors.blueGrey,
                    backgroundColor: SemanticColors.iceBlue,
                    key: refreshKeyy,
                    onRefresh: refreshList,
                  child: Column(
                    children: <Widget>[
                      _tabController == null ? Center(child: CircularProgressIndicator()) : Container(
                      color: Colors.white,
                      height: 36 / 720 * screenHeight,
                      //margin: EdgeInsets.only(left: 20/ 360 * screenWidth),
                      child: Padding(
                        padding: EdgeInsets.only(left: 20/ 360 * screenWidth),
                        child: TabBar(

                        labelStyle: TextStyle(fontSize: 14 / 720 * screenHeight),
                        controller: _tabController,
                        isScrollable: true,
                        indicatorColor: ImsColors.purpleish_blue,
                        labelColor: ImsColors.purpleish_blue,
                        unselectedLabelColor: ImsColors.bluey_grey,
                        indicatorSize: TabBarIndicatorSize.tab,
                        tabs:   databaseTablist ),
                      ),
                    ),
                      
                      Expanded(
                          child: fetchData.length == databaseTablist.length && fetchData.length != 0 && databaseTablist.length != 0 ? TabBarView(
                          controller: _tabController,
                          children:  fetchData,
                        ) :  Container(
                          width: screenWidth,
                             color: Colors.white,
                            child: Text("No blogs to display",
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                fontSize: 12 / 720 * screenHeight,
                                fontFamily: "IBMPlexSans",
                                fontWeight: FontWeight.w500,
                              ),),
                          ),
                      ),



                    ],
                  ),
        ),
      
    );
  }
}


  void CalculateScreen(BuildContext context) {
  var _mediaQueryData = MediaQuery.of(context);
  screenWidthTotal = _mediaQueryData.size.width;
  screenHeightTotal = _mediaQueryData.size.height;
  _safeAreaHorizontal =
      _mediaQueryData.padding.left + _mediaQueryData.padding.right;
  _safeAreaVertical =
      _mediaQueryData.padding.top + _mediaQueryData.padding.bottom;

  screenHeight = screenHeightTotal - _safeAreaVertical;
  screenWidth = screenWidthTotal - _safeAreaHorizontal;
}

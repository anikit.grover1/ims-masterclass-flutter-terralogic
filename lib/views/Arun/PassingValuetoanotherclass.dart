import 'package:flutter/material.dart';

final keyA = new GlobalKey<MyStatefulWidget1State>();
final keyB = new GlobalKey<MyStatefulWidget2State>();

class MyApp1 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      home: new Scaffold(
        body: Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget>[
            new MyStatefulWidget1(key: keyA),
            new MyStatefulWidget2(key: keyB),
          ],
        ),
      ),
    );
  }
}

class MyStatefulWidget1 extends StatefulWidget {
  MyStatefulWidget1({Key key}) : super(key: key);
  State createState() => new MyStatefulWidget1State();
}

class MyStatefulWidget1State extends State<MyStatefulWidget1> {
  String _createdObject = "Hello world!";
  String _createdObject2 = "68768689!";
  String _createdObject3 = "&&**&(*&**&!";
  String get createdObject => _createdObject;
  String get createdObject2 => _createdObject2;
  String get createdObject3 => _createdObject3;

  @override
  Widget build(BuildContext context) {
    return new Center(
        child: Column(
      children: <Widget>[
        new Text(_createdObject),
        TextField(
          onChanged: (value) {
            setState(() {
              _createdObject2 = value;
            });
          },
        )
      ],
    ));
  }
}

class MyStatefulWidget2 extends StatefulWidget {
  MyStatefulWidget2({Key key}) : super(key: key);
  State createState() => new MyStatefulWidget2State();
}

class MyStatefulWidget2State extends State<MyStatefulWidget2> {
  String _text = 'PRESS ME';

  @override
  Widget build(BuildContext context) {
    return new Center(
      child: new RaisedButton(
        child: new Text(_text),
        onPressed: () {
          setState(() {
            _text = keyA.currentState.createdObject2;
          });
        },
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:imsindia/components/navigation_bar.dart';
import 'package:imsindia/utils/home_svg_icons.dart';
import 'package:imsindia/views/Arun/aboutims/aboutims.dart';

import '../ims_utility.dart';
import 'HelpTopics.dart';
import 'TopQueries.dart';

TextEditingController controller = TextEditingController();
const actualHeight = 925;
const actualWidth = 360;

var screenWidthTotal;
var screenHeightTotal;
var _safeAreaHorizontal;
var _safeAreaVertical;
var screenHeight;
var screenWidth;
var showSearch = false;

class FAQ extends StatefulWidget {
  @override
  _FAQState createState() => _FAQState();
}

class _FAQState extends State<FAQ> {
  @override
  initState() {
    super.initState();
    // Add listeners to this class
    controller.text = "";
  }

  Future<bool> _willpopScope() async {
    if (showSearch) {
      setState(() {
        showSearch = false;
      });
    } else
      return true;
  }

  @override
  Widget build(BuildContext context) {
    CalculateScreen(context);
//    showSearch = false;
    return WillPopScope(
      onWillPop: _willpopScope,
      child: Scaffold(
        backgroundColor: Colors.white,
      
        
        appBar: showSearch == false
            ? AppBar(
                brightness: Brightness.light,
                elevation: 0.0,
                centerTitle: false,
                titleSpacing: 0.0,
                backgroundColor: Colors.white,
                iconTheme: IconThemeData(
                  color: Colors.black, //change your color here
                ),
                title: Row(
                  children: <Widget>[
                    Text(
                      ImsStrings.sFAQ,
                      style: const TextStyle(
                          color: Colors.black,
                          fontWeight: FontWeight.w500,
                          fontFamily: "IBMPlexSans",
                          fontStyle: FontStyle.normal,
                          fontSize: 16.0),
                    ),
                  ],
                ),
                actions: <Widget>[
                  Container(
                    margin:
                        EdgeInsets.only(right: 20 / screenWidth * actualWidth),
                    height: (20 / actualHeight) * screenHeight,
                    width: (20 / actualWidth) * screenWidth,
                    child: GestureDetector(
                      child: Image.asset(
                        'assets/images/search.png',
                        fit: BoxFit.contain,
                      ),
                      onTap: () {
                        setState(() {
                          showSearch = true;
                        });
                      },
                    ),
                  ),
                ],
//                  leading: IconButton(
//                    icon: Icon(
//                      Icons.arrow_back,
//                      color: Colors.black,
//                    ),
//                    onPressed: () => Navigator.pop(context, false),
//                  ),
              )
            : AppBar(
                brightness: Brightness.light,
                centerTitle: false,
                titleSpacing: 0.0,
                automaticallyImplyLeading: false,
                backgroundColor: Colors.white,
                title: Row(
                  children: <Widget>[
                    Container(
                      height: (40 / actualHeight) * screenHeight,
                      width: (250.0 / actualWidth) * screenWidth,
                      //margin: EdgeInsets.only(right: (6 / 360) * screenWidth),
                      child: Center(
                        child: TextField(
                          onSubmitted: (value) {
                            debugPrint('Arun value in text $value');
                          },
                          textAlign: TextAlign.start,
                          autofocus: true,
                          controller: controller,
                          textInputAction: TextInputAction.send,
                          autocorrect: false,
                          decoration: InputDecoration(
                            focusedBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.all(
                                Radius.circular(30),
                              ),
                              borderSide: BorderSide(
                                  color: Color(0xfff2f4f4), width: 1.5),
                            ),
                            contentPadding: const EdgeInsets.all(11.0),
                            hintText: "Search FAQ’s",
                            alignLabelWithHint: true,
                            border: OutlineInputBorder(
                              borderSide: BorderSide(
                                  color: Color(0xfff2f4f4), width: 1.5),
                              borderRadius: BorderRadius.all(
                                Radius.circular(30),
                              ),
                            ),
                            hintStyle: TextStyle(
                                fontFamily: "IBMPlexSans",
                                fontWeight: FontWeight.w400,
                                fontStyle: FontStyle.normal,
                                color: Color(0xFF7e919a),
                                fontSize: 14.0),
                          ),
                          style: TextStyle(
                              fontFamily: "IBMPlexSans",
                              fontWeight: FontWeight.w400,
                              fontStyle: FontStyle.normal,
                              color: ImsColors.charcoal_grey,
                              fontSize: 14),
                        ),
                      ),
                    ),
                  ],
                ),
                leading: Container(
                  height: (14 / actualHeight) * screenHeight,
                  width: (14 / actualWidth) * screenWidth,
                  margin: EdgeInsets.only(
                      left: (16.0 / actualWidth) * screenWidth,
                      right: (9.0 / actualWidth) * screenWidth),
                  child: GestureDetector(
                    child: SvgPicture.asset(
                      getSvgIcon.search,
                      fit: BoxFit.contain,
                      //color: Colors.red,
                    ),
                    onTap: () {
                      debugPrint(
                          'Arun value from search text == ${controller.text}');
                      showSearch = false;
                    },
                  ),
                ),
                actions: <Widget>[
                  GestureDetector(
                    onTap: () {
                      // Navigator.pop(context, false);
                      setState(() {
                        controller.text = "";
                        showSearch = false;
                      });
                    },
                    child: Container(
                      height: (22 / actualHeight) * screenHeight,
                      width: (22 / actualWidth) * screenWidth,
                      margin: EdgeInsets.only(right: (19 / 360) * screenWidth),
                      child: Image.asset(
                        'assets/images/close.png',
                        fit: BoxFit.contain,
                      ),
                    ),
                  ),
                ],
              ),
//        drawer: NavigationDrawer(),
        body: FaQStatFull(),
      ),
    );
  }
}

class FaQStatFull extends StatefulWidget {
  @override
  _FaQStatFullState createState() => _FaQStatFullState();
}

class _FaQStatFullState extends State<FaQStatFull> {
  @override
  Widget build(BuildContext context) {
    CalculateScreen(context);
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Container(
              width: double.infinity,
              height: screenHeight * 42 / actualHeight,
              child: Padding(
                padding: EdgeInsets.only(top: 10 / actualHeight * screenHeight),
                child: Row(
                  children: <Widget>[
                    Expanded(
                      flex: 2,
                      child: Padding(
                        padding: EdgeInsets.only(
                          left: 20 / actualWidth * screenWidth,
                        ),
                        child: Text(
                          'Top Queries',
                          style: TextStyle(
                              fontFamily: "IBMPlexSans",
                              fontStyle: FontStyle.normal,
                              fontWeight: FontWeight.w700,
                              color: ImsColors.dark_navy_blue,
                              fontSize: screenHeight * 18 / actualHeight),
                        ),
                      ),
                    ),
                    Expanded(
                      flex: 1,
                      child: Padding(
                        padding: EdgeInsets.only(
                          right: 20 / actualWidth * screenWidth,
                        ),
                        child: Text(
                          'Help Topics',
                          style: TextStyle(
                              fontFamily: "IBMPlexSans",
                              fontStyle: FontStyle.normal,
                              fontWeight: FontWeight.w400,
                              color: ImsColors.azure,
                              fontSize: screenHeight * 14 / actualHeight),
                        ),
                      ),
                    ),
                  ],
                ),
              )),
          SizedBox(
            height: screenHeight * 315 / actualHeight,
            child: BodyLayoutTopQueries(),
          ),
          Container(
              width: double.infinity,
              height: screenHeight * 75 / actualHeight,
              child: Row(
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.only(
                        left: 20 / actualWidth * screenWidth,
                        top: 25 / actualHeight * screenHeight),
                    child: GestureDetector(
                      onTap: () {},
                      child: Text(
                        'Help Topics',
                        style: TextStyle(
                            fontFamily: "IBMPlexSans",
                            fontStyle: FontStyle.normal,
                            fontWeight: FontWeight.w500,
                            color: ImsColors.dark_navy_blue,
                            fontSize: screenHeight *
                                16 /
                                actualHeight), // / actualHeight * screenHeight),
                      ),
                    ),
                  ),
                ],
              )),
          SizedBox(
            height: screenHeight * 315 / actualHeight,
            child: BodyLayoutHelpTopics(),
          ),
//          Container(
//              alignment: Alignment.center,
//              width: double.infinity,
//              height: screenHeight * 50 / actualHeight,
//              child: Container(
//                child: Row(
//                  children: <Widget>[
//                    Padding(
//
//                        padding: EdgeInsets.only(
//
//                            top: 25 / actualHeight * screenHeight),
//                        child: Row(
//                          children: <Widget>[
//                            Text(
//                              'Can’t find an answer to your query? ',
//                              style: TextStyle(
//                                  fontFamily: "IBMPlexSans",
//                                  fontStyle: FontStyle.normal,
//                                  fontWeight: FontWeight.w400,
//                                  color: ImsColors.gunmetal,
//                                  fontSize: screenHeight * 12 / actualHeight),
//
//                              /// actualHeight * screenHeight),
//                            ),
//                            GestureDetector(
//                              onTap: () {
//                                Navigator.push(
//                                    context,
//                                    MaterialPageRoute(
//                                        builder: (context) => AboutIms()));
//                              },
//                              child: Text(
//                                '       Contact us here',
//                                style: TextStyle(
//                                    fontFamily: "IBMPlexSans",
//                                    fontStyle: FontStyle.normal,
//                                    fontWeight: FontWeight.w400,
//                                    color: ImsColors.azure,
//                                    fontSize: screenHeight * 12 / actualHeight),
//                              ),
//                            ),
//                          ],
//                        )),
//                  ],
//                ),
//              ))
          Container(
              alignment: Alignment.center,
              width: double.infinity,
              height: screenHeight * 50 / actualHeight,
child: Padding(
                        padding: EdgeInsets.only(
                            top: 25 / actualHeight * screenHeight),
              child: GestureDetector(
                child: RichText(
                  text: new TextSpan(
                    // Note: Styles for TextSpans must be explicitly defined.
                    // Child text spans will inherit styles from parent
                    style: new TextStyle(
                      fontSize: 14.0,
                      color: Colors.black,
                    ),
                    children: <TextSpan>[
                      new TextSpan(text: 'Can’t find an answer to your query?',style: TextStyle(
                                    fontFamily: "IBMPlexSans",
                                    fontStyle: FontStyle.normal,
                                    fontWeight: FontWeight.w400,
                                    color: ImsColors.gunmetal,
                                    fontSize: screenHeight * 12 / actualHeight), ),
                      new TextSpan(text: '       Contact us here', style: new TextStyle(
                                      fontFamily: "IBMPlexSans",
                                      fontStyle: FontStyle.normal,
                                      fontWeight: FontWeight.w400,
                                      color: ImsColors.azure,
                                      fontSize: screenHeight * 12 / actualHeight),),
                    ],
                  ),
                ),
                onTap: (){
                  Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => AboutIms()));
                },
              )),
          ),
        ],
      ),
    );
  }
}

void CalculateScreen(BuildContext context) {
  var _mediaQueryData = MediaQuery.of(context);
  screenWidthTotal = _mediaQueryData.size.width;
  screenHeightTotal = _mediaQueryData.size.height;
  _safeAreaHorizontal =
      _mediaQueryData.padding.left + _mediaQueryData.padding.right;
  _safeAreaVertical =
      _mediaQueryData.padding.top + _mediaQueryData.padding.bottom;

  screenHeight = screenHeightTotal - _safeAreaVertical;
  screenWidth = screenWidthTotal - _safeAreaHorizontal;
}

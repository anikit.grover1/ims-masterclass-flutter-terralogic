import 'package:flutter/material.dart';
import 'package:imsindia/components/navigation_bar.dart';
import 'package:imsindia/utils/emaximiser_svg_icons.dart';
import 'package:imsindia/views/Arun/aboutims/aboutims.dart';

import '../../ims_utility.dart';
import 'DetailTopQueries.dart';

TextEditingController controller = TextEditingController();
const actualHeight = 925;
const actualWidth = 360;
var screenWidthTotal;
var screenHeightTotal;
var _safeAreaHorizontal;
var _safeAreaVertical;
var screenHeight;
var screenWidth;
var showSearch = false;

class FAQDeatail extends StatefulWidget {
  String currentTopic;

  FAQDeatail({Key key, @required this.currentTopic}) : super(key: key);
  @override
  _FAQDeatailState createState() => _FAQDeatailState();
}

class _FAQDeatailState extends State<FAQDeatail> {
  @override
  initState() {
    super.initState();
    // Add listeners to this class
    controller.text = "";
  }

  @override
  Widget build(BuildContext context) {
//    showSearch = false;
    CalculateScreen(context);
    return Scaffold(
      backgroundColor: Colors.white,
      
      appBar: showSearch == false
          ? AppBar(
              brightness: Brightness.light,
              elevation: 0.0,
              centerTitle: false,
              titleSpacing: 0.0,
              backgroundColor: Colors.white,
              iconTheme: IconThemeData(
                color: Colors.black, //change your color here
              ),
              title: Row(
                children: <Widget>[
                  Text(
                    ImsStrings.sFAQ,
                    style: const TextStyle(
                        color: Colors.black,
                        fontWeight: FontWeight.w500,
                        fontFamily: "IBMPlexSans",
                        fontStyle: FontStyle.normal,
                        fontSize: 16.0),
                  ),
                ],
              ),
              actions: <Widget>[
                Container(
                  margin:
                      EdgeInsets.only(right: 20 / screenWidth * actualWidth),
                  height: (20 / actualHeight) * screenHeight,
                  width: (20 / actualWidth) * screenWidth,
                  child: GestureDetector(
                    child: Image.asset(
                      'assets/images/search.png',
                      fit: BoxFit.contain,
                    ),
                    onTap: () {
                      setState(() {
                        showSearch = true;
                      });
                    },
                  ),
                ),
              ],
            )
          : AppBar(
              brightness: Brightness.light,
              centerTitle: false,
              titleSpacing: 0.0,
              automaticallyImplyLeading: false,
              backgroundColor: Colors.white,
              title: Row(
                children: <Widget>[
                  Container(
                    height: (40 / actualHeight) * screenHeight,
                    width: (250.0 / actualWidth) * screenWidth,
                    //margin: EdgeInsets.only(right: (6 / 360) * screenWidth),
                    child: Center(
                      child: TextField(
                        onSubmitted: (value) {
                          debugPrint('Arun value in text $value');
                        },
                        textAlign: TextAlign.start,
                        autofocus: true,
                        controller: controller,
                        textInputAction: TextInputAction.send,
                        autocorrect: false,
                        decoration: InputDecoration(
                          focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.all(
                              Radius.circular(30),
                            ),
                            borderSide: BorderSide(
                                color: Color(0xfff2f4f4), width: 1.5),
                          ),
                          contentPadding: const EdgeInsets.all(11.0),
                          hintText: "Search FAQ’s",
                          alignLabelWithHint: true,
                          border: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: Color(0xfff2f4f4), width: 1.5),
                            borderRadius: BorderRadius.all(
                              Radius.circular(30),
                            ),
                          ),
                          hintStyle: TextStyle(
                              fontFamily: "IBMPlexSans",
                              fontWeight: FontWeight.w400,
                              fontStyle: FontStyle.normal,
                              color: Color(0xFF7e919a),
                              fontSize: 14.0),
                        ),
                        style: TextStyle(
                            fontFamily: "IBMPlexSans",
                            fontWeight: FontWeight.w400,
                            fontStyle: FontStyle.normal,
                            color: ImsColors.charcoal_grey,
                            fontSize: 14),
                      ),
                    ),
                  ),
                ],
              ),
              leading: Container(
                height: (14 / actualHeight) * screenHeight,
                width: (14 / actualWidth) * screenWidth,
                margin: EdgeInsets.only(
                    left: (16.0 / actualWidth) * screenWidth,
                    right: (9.0 / actualWidth) * screenWidth),
                child: GestureDetector(
                  child: Image.asset(
                    'assets/images/search.png',
                    fit: BoxFit.contain,
                  ),
                  onTap: () {
                    debugPrint(
                        'Arun value from search text == ${controller.text}');
                    showSearch = false;
                  },
                ),
              ),
              actions: <Widget>[
                GestureDetector(
                  onTap: () {
                    // Navigator.pop(context, false);
                    setState(() {
                      controller.text = "";
                      showSearch = false;
                    });
                  },
                  child: Container(
                    height: (22 / actualHeight) * screenHeight,
                    width: (22 / actualWidth) * screenWidth,
                    margin: EdgeInsets.only(right: (19 / 360) * screenWidth),
                    child: Image.asset(
                      'assets/images/close.png',
                      fit: BoxFit.contain,
                    ),
                  ),
                ),
              ],
            ),
//      drawer: NavigationDrawer(),
      body: FaQStatFull(
        currentTopic: widget.currentTopic,
      ),
    );
  }
}

class FaQStatFull extends StatefulWidget {
  String currentTopic;

  FaQStatFull({Key key, @required this.currentTopic}) : super(key: key);
  @override
  _FaQStatFullState createState() => _FaQStatFullState();
}

class _FaQStatFullState extends State<FaQStatFull> {
  @override
  Widget build(BuildContext context) {
    CalculateScreen(context);
    debugPrint('Arun ${widget.currentTopic}');
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Container(
              width: double.infinity,
              height: screenHeight * 78 / actualHeight,
              child: Padding(
                padding: EdgeInsets.only(
                    top: 8 / actualHeight * screenHeight,
                    bottom: 20 / actualHeight * screenHeight),
                child: Row(
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.only(top: 5),
                      child: IconButton(
                        icon: getSvgIcon.backSvgIcon,
                        onPressed: () => Navigator.pop(context, false),
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.only(
                        left: 10 / actualWidth * screenWidth,
                        top: 10 / actualHeight * screenWidth,
                      ),
                      width: 250.0,
                      child: Text(
                        widget.currentTopic,
                        maxLines: 1,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(
                            fontFamily: "IBMPlexSans",
                            fontStyle: FontStyle.normal,
                            fontWeight: FontWeight.w500,
                            color: ImsColors.dark_navy_blue,
                            fontSize: screenHeight * 14 / actualHeight),
                      ),
                    ),
//
                  ],
                ),
              )),
          SizedBox(
            height: screenHeight * 620 / actualHeight,
            child: DetailBodyLayoutSearchTopQueries(),
          ),
          Visibility(
            visible: !showSearch,
            child:  Container(
              alignment: Alignment.bottomCenter,
              width: double.infinity,
              height: screenHeight * 70 / actualHeight,
              child: Padding(
                  padding: EdgeInsets.only(
                      top: 25 / actualHeight * screenHeight),
                  child: GestureDetector(
                    child: RichText(
                      text: new TextSpan(
                        // Note: Styles for TextSpans must be explicitly defined.
                        // Child text spans will inherit styles from parent
                        style: new TextStyle(
                          fontSize: 14.0,
                          color: Colors.black,
                        ),
                        children: <TextSpan>[
                          new TextSpan(text: 'Can’t find an answer to your query?',style: TextStyle(
                              fontFamily: "IBMPlexSans",
                              fontStyle: FontStyle.normal,
                              fontWeight: FontWeight.w400,
                              color: ImsColors.gunmetal,
                              fontSize: screenHeight * 12 / actualHeight), ),
                          new TextSpan(text: '       Contact us here', style: new TextStyle(
                              fontFamily: "IBMPlexSans",
                              fontStyle: FontStyle.normal,
                              fontWeight: FontWeight.w400,
                              color: ImsColors.azure,
                              fontSize: screenHeight * 12 / actualHeight),),
                        ],
                      ),
                    ),
                    onTap: (){
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => AboutIms()));
                    },
                  )),
            ),
          ),
        ],
      ),
    );
  }
}

void CalculateScreen(BuildContext context) {
  var _mediaQueryData = MediaQuery.of(context);
  screenWidthTotal = _mediaQueryData.size.width;
  screenHeightTotal = _mediaQueryData.size.height;
  _safeAreaHorizontal =
      _mediaQueryData.padding.left + _mediaQueryData.padding.right;
  _safeAreaVertical =
      _mediaQueryData.padding.top + _mediaQueryData.padding.bottom;

  screenHeight = screenHeightTotal - _safeAreaVertical;
  screenWidth = screenWidthTotal - _safeAreaHorizontal;
}

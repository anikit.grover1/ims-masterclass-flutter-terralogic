//import 'package:flutter/material.dart';
//import 'package:flutter_geeta_learning/learning/ims/Final/ims_utility.dart';
//
//var screenWidthTotal;
//var screenHeightTotal;
//
//var _safeAreaHorizontal;
//
//var _safeAreaVertical;
//
//var screenHeight;
//
//var screenWidth;
//
//class Item {
//  var title =
//      'Arun Detail hai ?wala How to analyse Mock CATs: Mocks liya ab kya karna hai? Are sure you can do this?';
//  var subtitle = ' Search hai 01-Aug-2018';
//  var subtitle2 = '50 min Arun';
//  var imgUrl =
//      'https://thumbs.dreamstime.com/b/inspirational-quote-there-blessings-everyday-find-them-create-treasure-beautiful-white-single-sinnia-flower-blossom-blurry-151653366.jpg';
//}
//
//List<Item> itemsAll = [Item(), Item(), Item(), Item(), Item(), Item()];
//
//class DetailBodyLayoutSearchTopQueries extends StatefulWidget {
//  @override
//  DetailBodyLayoutSearchTopQueriesState createState() {
//    return new DetailBodyLayoutSearchTopQueriesState();
//  }
//}
//
//class DetailBodyLayoutSearchTopQueriesState
//    extends State<DetailBodyLayoutSearchTopQueries> {
//  @override
//  Widget build(BuildContext context) {
//    return _myListView();
//  }
//
//  Widget _myListView() {
//    var textSizeH = 0.0;
//    bool bShow = true;
//    try {
//      textSizeH = 14.0 / 930 * screenHeight;
//    } catch (e) {
//      textSizeH = 12.0;
//    }
//    return ListView.builder(
//      itemCount: itemsAll.length,
//      itemBuilder: (context, index) {
//        final item = itemsAll[index];
//        return Card(
//          child: ListTile(
////            title: Text(item),
//            title: Container(
////                margin: EdgeInsets.symmetric(
////                    vertical: 10.0 / 749 * screenHeight,
////                    horizontal: 5.0 / 360 * screenWidth),
//              child: Column(
//                crossAxisAlignment: CrossAxisAlignment.start,
//                children: <Widget>[
//                  Padding(
//                    padding: const EdgeInsets.only(top: 8.0),
//                    child: FlatButton(
//                      onPressed: () {
//                        if (bShow == true) {
//                          bShow = false;
//                        } else {
//                          bShow = true;
//                        }
//                      },
//                      child: Text(
//                        item.title,
//                        maxLines: 2,
//                        textAlign: TextAlign.left,
//                        style: TextStyle(
//                            fontSize: textSizeH,
//                            fontWeight: FontWeight.bold,
//                            color: ImsColors.dark_navy_blue),
//                      ),
//                    ),
//                  ),
//                  Visibility(
//                    visible: bShow,
//                    child: Padding(
//                      padding: const EdgeInsets.only(top: 8.0),
//                      child: Text(
//                        item.title,
//                        maxLines: 4,
//                        textAlign: TextAlign.left,
//                        style: TextStyle(
//                            fontSize: textSizeH,
//                            fontWeight: FontWeight.normal,
//                            color: ImsColors.bluey_grey),
//                      ),
//                    ),
//                  ),
//                ],
//              ),
//            ),
//            onTap: () {
//              //                                  <-- onTap
//              setState(() {});
//            },
//            onLongPress: () {
//              //                            <-- onLongPress
//              setState(() {});
//            },
//          ),
//        );
//      },
//    );
//  }
//}
import 'package:flutter/material.dart';

import '../../ims_utility.dart';

const actualHeight = 925;
const actualWidth = 360;
var screenWidthTotal;
var screenHeightTotal;
var _safeAreaHorizontal;
var _safeAreaVertical;
var screenHeight;
var screenWidth;
var bShowDetails = true;

class Item {
  Item(String sTitle, String sSubTitle, bool bSShow) {
    this.title = sTitle;
    this.subtitle = sSubTitle;
    this.bShowDetails = bSShow;
  }
  var title =
      'How to analyse Mock CATs: Mocks liya ab kya karna hai? Are sure you can do this?';
  var subtitle =
      'Slot booking for every proctored simCAT is activated 2 days before the test. You will get an SMS once the slot booking is activated.';
  var subtitle2 = '50 min Arun';
  var bShowDetails = false;
  var imgUrl =
      'https://thumbs.dreamstime.com/b/inspirational-quote-there-blessings-everyday-find-them-create-treasure-beautiful-white-single-sinnia-flower-blossom-blurry-151653366.jpg';
}

List<Item> itemsAll = [
  Item(
      'How to analyse Mock CATs: Mocks liya ab kya karna hai? Are sure you can do this?',
      'How to analyse Mock CATs: Mocks liya ab kya karna hai? Are sure you can do this?',
      true),
  Item(
      'How to analyse Mock CATs: Mocks liya ab kya karna hai? Are sure you can do this?',
      'How to analyse Mock CATs: Mocks liya ab kya karna hai? Are sure you can do this?',
      false),
  Item(
      'How to analyse Mock CATs: Mocks liya ab kya karna hai? Are sure you can do this?',
      'How to analyse Mock CATs: Mocks liya ab kya karna hai? Are sure you can do this?',
      true),
  Item(
      'How to analyse Mock CATs: Mocks liya ab kya karna hai? Are sure you can do this?',
      'How to analyse Mock CATs: Mocks liya ab kya karna hai? Are sure you can do this?',
      true),
  Item(
      'How to analyse Mock CATs: Mocks liya ab kya karna hai? Are sure you can do this?',
      'How to analyse Mock CATs: Mocks liya ab kya karna hai? Are sure you can do this?',
      true),
  Item(
      'How to analyse Mock CATs: Mocks liya ab kya karna hai? Are sure you can do this?',
      'How to analyse Mock CATs: Mocks liya ab kya karna hai? Are sure you can do this?',
      true),
];

class DetailBodyLayoutSearchTopQueries extends StatefulWidget {
  @override
  DetailBodyLayoutSearchTopQueriesState createState() {
    return new DetailBodyLayoutSearchTopQueriesState();
  }
}

class DetailBodyLayoutSearchTopQueriesState
    extends State<DetailBodyLayoutSearchTopQueries> {
  @override
  Widget build(BuildContext context) {
    CalculateScreen(context);
    return _myListView();
  }

  Widget _myListView() {
    var textSizeH = 0.0;
    try {
      textSizeH = 14.0 / actualHeight * screenHeight;
    } catch (e) {
      textSizeH = 12.0;
    }
    return ListView.builder(
      itemCount: itemsAll.length,
      itemBuilder: (context, index) {
        final item = itemsAll[index];
        return Container(
          child: Padding(
            padding: EdgeInsets.symmetric(
                horizontal: 20 / actualWidth * screenWidth),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.only(
                    top: 15 / actualHeight * screenHeight,
                    bottom: 10 / actualHeight * screenHeight,
                  ),
                  child: GestureDetector(
                    child: Text(
                      item.title,
                      maxLines: 2,
                      textAlign: TextAlign.left,
                      style: TextStyle(
                          fontSize: 14.0,
                          fontFamily: "IBMPlexSans",
                          color: ImsColors.dark_navy_blue),
                    ),
                    onTap: () {
                      setState(() {
                        if (itemsAll[index].bShowDetails) {
                          itemsAll[index].bShowDetails = false;
                          for (Item it in itemsAll) {
                            it.bShowDetails = false;
                          }
                        } else {
                          for (Item it in itemsAll) {
                            it.bShowDetails = false;
                          }
                          itemsAll[index].bShowDetails = true;
                        }
                      });
                    },
                  ),
                ),
                Visibility(
                  visible: itemsAll[index].bShowDetails,
                  child: Padding(
                    padding: EdgeInsets.only(
                        bottom: 15 / actualHeight * screenHeight),
                    child: Text(
                      item.subtitle,
                      maxLines: 4,
                      textAlign: TextAlign.left,
                      style: TextStyle(
                          fontFamily: "IBMPlexSans",
                          fontStyle: FontStyle.normal,
                          fontWeight: FontWeight.w400,
                          color: ImsColors.bluey_grey,
                          fontSize: screenHeight * 14 / actualHeight),
                    ),
                  ),
                ),
                Container(
                  width: double.infinity,
                  height: 1.0,
                  color: ImsColors.iceBlue,
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}

void CalculateScreen(BuildContext context) {
  var _mediaQueryData = MediaQuery.of(context);
  screenWidthTotal = _mediaQueryData.size.width;
  screenHeightTotal = _mediaQueryData.size.height;
  _safeAreaHorizontal =
      _mediaQueryData.padding.left + _mediaQueryData.padding.right;
  _safeAreaVertical =
      _mediaQueryData.padding.top + _mediaQueryData.padding.bottom;

  screenHeight = screenHeightTotal - _safeAreaVertical;
  screenWidth = screenWidthTotal - _safeAreaHorizontal;
}

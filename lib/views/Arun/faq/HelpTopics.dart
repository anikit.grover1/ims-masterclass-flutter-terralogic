import 'package:flutter/material.dart';

import '../ims_utility.dart';
import 'faqdetail/FAQDetail.dart';

const actualHeight = 925;
const actualWidth = 360;
var screenWidthTotal;
var screenHeightTotal;
var _safeAreaHorizontal;
var _safeAreaVertical;
var screenHeight;
var screenWidth;
var monVal = true;

class Item {
  var title =
      'How to analyse Mock CATs: Mocks liya ab kya karna hai? Are sure you can do this?';
  var subtitle = '01-Aug-2018';
  var subtitle2 = '50 min Arun';
  var imgUrl =
      'https://thumbs.dreamstime.com/b/inspirational-quote-there-blessings-everyday-find-them-create-treasure-beautiful-white-single-sinnia-flower-blossom-blurry-151653366.jpg';
}

List<Item> itemsAll = [Item(), Item(), Item(), Item(), Item(), Item()];

class BodyLayoutHelpTopics extends StatefulWidget {
  @override
  BodyLayoutHelpTopicsState createState() {
    return new BodyLayoutHelpTopicsState();
  }
}

class BodyLayoutHelpTopicsState extends State<BodyLayoutHelpTopics> {
  @override
  Widget build(BuildContext context) {
    CalculateScreen(context);
    return _myListView();
  }

  Widget _myListView() {
    var textSizeH = 0.0;
    try {
      textSizeH = 14.0 / actualHeight * screenHeight;
    } catch (e) {
      textSizeH = 12.0;
    }
    return ListView.builder(
      itemCount: itemsAll.length,
      itemBuilder: (context, index) {
        final item = itemsAll[index];
        return Container(
          child: Padding(
            padding: EdgeInsets.symmetric(
                horizontal: 20 / actualWidth * screenWidth),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.only(
                    top: 15 / actualHeight * screenHeight,
                    bottom: 15 / actualHeight * screenHeight,
                  ),
                  child: GestureDetector(
                    child: Text(
                      item.title,
                      maxLines: 2,
                      textAlign: TextAlign.left,
                      style: TextStyle(
                          fontFamily: "IBMPlexSans",
                          fontStyle: FontStyle.normal,
                          fontWeight: FontWeight.w400,
                          color: ImsColors.azure,
                          fontSize: screenHeight * 14 / actualHeight),
                    ),
                    onTap: () {
                      Navigator.push(
                        context,
                        new MaterialPageRoute(
                          builder: (context) {
                            return FAQDeatail(currentTopic: item.title);
                          },
                        ),
                      );
                    },
                  ),
                ),
                Container(
                  width: double.infinity,
                  height: 1.0,
                  color: ImsColors.iceBlue,
                )
              ],
            ),
          ),
        );
      },
    );
  }
}

void CalculateScreen(BuildContext context) {
  var _mediaQueryData = MediaQuery.of(context);
  screenWidthTotal = _mediaQueryData.size.width;
  screenHeightTotal = _mediaQueryData.size.height;
  _safeAreaHorizontal =
      _mediaQueryData.padding.left + _mediaQueryData.padding.right;
  _safeAreaVertical =
      _mediaQueryData.padding.top + _mediaQueryData.padding.bottom;

  screenHeight = screenHeightTotal - _safeAreaVertical;
  screenWidth = screenWidthTotal - _safeAreaHorizontal;
}

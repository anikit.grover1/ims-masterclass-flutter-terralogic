import 'package:flutter/material.dart';
import 'package:imsindia/api/bloc/services.dart';
import 'package:imsindia/utils/Url.dart';
import 'package:imsindia/utils/emaximiser_svg_icons.dart';
import 'package:imsindia/utils/global.dart' as globals;
import 'package:shared_preferences/shared_preferences.dart';

import '../ims_utility.dart';

class ChangePassword extends StatefulWidget {
  @override
  _ChangePasswordState createState() => _ChangePasswordState();
}

TextEditingController controllerOldPwd = TextEditingController();
TextEditingController controllerNewPwd = TextEditingController();
TextEditingController controllerReNewPwd = TextEditingController();
const actualHeight = 730;
const actualWidth = 360;
var screenWidthTotal;
var screenHeightTotal;
var _safeAreaHorizontal;
var _safeAreaVertical;
var screenHeight;
var screenWidth;

class _ChangePasswordState extends State<ChangePassword> {
  final GlobalKey<ScaffoldState> _scaffoldstate =
      new GlobalKey<ScaffoldState>();
  final _formKey = GlobalKey<FormState>();
  final FocusNode _firstInputFocusNode = new FocusNode();
  final FocusNode _secondInputFocusNode = new FocusNode();
  final FocusNode _thirdInputFocusNode = new FocusNode();
  bool loaderChecking=false;
  var bodyProgress;
//  double temp = (80 / actualHeight) * SizeConfig.screenHeight;
//  final String assetName = 'images/facebook.svg';
//  final Widget svgIcon = new SvgPicture.asset(assetName,
//      color: Colors.red, semanticsLabel: 'A red up arrow');
  @override
  Widget build(BuildContext context) {
    CalculateScreen(context);
    bodyProgress = loaderChecking?new Container(
      child: new Stack(
        children: <Widget>[

          new Container(
            alignment: AlignmentDirectional.center,
            decoration: new BoxDecoration(
              color: Colors.black12,
            ),
            child: new Container(


              alignment: AlignmentDirectional.center,
              child:
              new CircularProgressIndicator(),
              width: 170.00,
              height: 170.00,


            ),

          ),
        ],
      ),
    ):new Container();

    SizeConfig().init(context);
    return Container(
      child: Scaffold(
        key: _scaffoldstate,
        backgroundColor: Colors.white,
        
        appBar: AppBar(
          backgroundColor: Colors.white,
          brightness: Brightness.light,
          elevation: 0.0,
          centerTitle: false,
          titleSpacing: 0.0,
//          backgroundColor: Colors.white,
          title: Text(
            ImsStrings.sChangePassword,
            style: const TextStyle(
                color: Colors.black,
                fontWeight: FontWeight.w500,
                fontFamily: "IBMPlexSans",
                fontStyle: FontStyle.normal,
                fontSize: 16.0),
          ),
          leading: IconButton(
            icon: getSvgIcon.backSvgIcon,
            onPressed: () => Navigator.pop(context, false),
          ),
        ),
        body: Stack(
          children: <Widget>[
            Form(
              key: _formKey,
              child: Padding(
                padding: EdgeInsets.only(
                    top: screenHeight * 55 / actualHeight,
                    left: 45 / actualWidth * screenWidth,
                    right: 45 / actualWidth * screenWidth),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    TextFormField(
                      validator: (value) {
                        if (value.isEmpty) {
                          return 'Please enter old password';
                        } else if (value.length < 4) {
                          return 'Password should be 5 to 12 characters';
                        }
                        return null;
                      },
                      maxLength: 12,
                      focusNode: _firstInputFocusNode,
                      textInputAction: TextInputAction.next,
                      onEditingComplete: () {
                        FocusScope.of(context).requestFocus(_secondInputFocusNode);
                        setState(() {});
                      },
                      obscureText: true,
                      textAlign: TextAlign.start,
                      autofocus: true,
                      controller: controllerOldPwd,
                      autocorrect: false,
                      decoration: InputDecoration(
                        border: new UnderlineInputBorder(
                            borderSide:
                                new BorderSide(color: ImsColors.bluey_grey)),

                        enabledBorder: UnderlineInputBorder(
                          borderSide: BorderSide(color: ImsColors.purpley),
                        ),

                        //hintText: ImsStrings.sEnterOldPassword,
                        alignLabelWithHint: true,
                        labelText: ImsStrings.sOldPassword,
                        //hintText: ImsStrings.sEnterOldPassword,
                        // hintText: 'ImsStrings.sEnterOldPassword',
                        labelStyle: TextStyle(
                            fontSize: 12 / actualHeight * screenHeight,
                            color: ImsColors.bluey_grey),
                        hintStyle: TextStyle(
                            fontWeight: FontWeight.normal,
                            color: ImsColors.bluey_grey,
                            fontSize: 12),
                      ),
                      style: TextStyle(
                          color: ImsColors.charcoal_grey,
                          fontWeight: FontWeight.normal,
                          fontSize: 12),
                    ),
                    Padding(
                      padding:
                          EdgeInsets.only(top: 25 / actualHeight * screenHeight),
                      child: TextFormField(
                        onFieldSubmitted: (value) {
                          debugPrint('Arun value in text $value');
                        },
                        validator: (value) {
                          if (value.isEmpty) {
                            return 'Please enter new password';
                          } else if (value.length < 4) {
                            return 'Password should be 5 to 12 characters';
                          }
                          return null;
                        },
                        maxLength: 12,
                        focusNode: _secondInputFocusNode,
                        textInputAction: TextInputAction.next,
                        onEditingComplete: () {
                          labelText:
                          ImsStrings.sNewPassword;
                          FocusScope.of(context).requestFocus(_thirdInputFocusNode);
                        },
                        obscureText: true,
                        textAlign: TextAlign.start,
                        autofocus: true,
                        controller: controllerNewPwd,
                        autocorrect: false,
                        decoration: InputDecoration(
                          border: new UnderlineInputBorder(
                              borderSide:
                                  new BorderSide(color: ImsColors.bluey_grey)),

                          enabledBorder: UnderlineInputBorder(
                            borderSide: BorderSide(color: ImsColors.purpley),
                          ),

                          //hintText: ImsStrings.sEnterNewPassword,
                          alignLabelWithHint: true,
                          labelText: ImsStrings.sNewPassword,
                          hintText: ImsStrings.sEnterNewPassword,
                          labelStyle: TextStyle(
                              fontSize: 12 / actualHeight * screenHeight,
                              color: ImsColors.bluey_grey),
//              border: OutlineInputBorder(
//                borderRadius: BorderRadius.all(
//                  Radius.circular(50),
//                ),
//              ),
                          hintStyle: TextStyle(
                              fontWeight: FontWeight.normal,
                              color: ImsColors.bluey_grey,
                              fontSize: 12),
                        ),
                        style: TextStyle(
                            color: ImsColors.charcoal_grey,
                            fontWeight: FontWeight.normal,
                            fontSize: 12),
                      ),
                    ),
                    Padding(
                      padding:
                          EdgeInsets.only(top: 25 / actualHeight * screenHeight),
                      child: TextFormField(
                        onFieldSubmitted: (value) {
                          if (controllerNewPwd.text != controllerReNewPwd.text) {}
                        },
                        validator: (value) {
                          if (value.isEmpty) {
                            return 'Please re-enter new password';
                          } else if (value.length < 4) {
                            return 'Password should be 5 to 12 characters';
                          } else {
                            if (controllerNewPwd.text != controllerReNewPwd.text) {
                              return 'Password is not same';
                            }
                          }
                          return null;
                        },
                        maxLength: 12,
                        obscureText: true,
                        textAlign: TextAlign.start,
                        autofocus: true,
                        controller: controllerReNewPwd,
                        focusNode: _thirdInputFocusNode,
                        textInputAction: TextInputAction.done,
                        autocorrect: false,
                        decoration: InputDecoration(
                          border: new UnderlineInputBorder(
                              borderSide:
                                  new BorderSide(color: ImsColors.bluey_grey)),

                          enabledBorder: UnderlineInputBorder(
                            borderSide: BorderSide(color: ImsColors.purpley),
                          ),

                          //hintText: ImsStrings.sReEnterNewPassword,
                          alignLabelWithHint: true,
                          labelText: ImsStrings.sNewPassword,
                          hintText: ImsStrings.sReEnterNewPassword,
                          labelStyle: TextStyle(
                              fontSize: 12 / actualHeight * screenHeight,
                              color: ImsColors.bluey_grey),
                          hintStyle: TextStyle(
                              fontWeight: FontWeight.normal,
                              color: ImsColors.bluey_grey,
                              fontSize: 12),
                        ),
                        style: TextStyle(
                            color: ImsColors.charcoal_grey,
                            fontWeight: FontWeight.normal,
                            fontSize: 12),
                      ),
                    ),
                    Padding(
                        padding:
                            EdgeInsets.only(top: 45 / actualHeight * screenHeight),
                        child: GestureDetector(
//                      onTap: (){
//                        print("inside button");
//                        showContent();
//                      },
                          onTap: enableSubmit()
                              ? () {
                                  getUserName();
                                  getCurrentUseremail();
                                  debugPrint('Arun' +
                                      controllerOldPwd.text +
                                      controllerNewPwd.text +
                                      controllerReNewPwd.text);
                                  FocusScope.of(context).requestFocus(new FocusNode());

                                  if (_formKey.currentState.validate()) {
                                    setState(() {
                                      loaderChecking=true;
                                    });
                                    validateOldPassword();
                                  }
                                }
                              : (){
                            FocusScope.of(context).requestFocus(new FocusNode());
                          },
                          child: Container(
                            decoration: enableSubmit()
                                ? BoxDecoration(
                                    borderRadius: BorderRadius.all(
                                      Radius.circular(2),

                                      // Box decoration takes a gradient
                                    ),
                                    gradient: LinearGradient(
                                        begin: Alignment(0, 0),
                                        end: Alignment(
                                            1.0199999809265137, 1.0099999904632568),
                                        colors: [
                                          const Color(0xff3380cc),
                                          ImsColors.purpley
                                        ]),
                                  )
                                : BoxDecoration(
                                    borderRadius: BorderRadius.all(
                                      Radius.circular(2),

                                      // Box decoration takes a gradient
                                    ),
                                    gradient: LinearGradient(
                                        begin: Alignment(0, 0),
                                        end: Alignment(
                                            1.0199999809265137, 1.0099999904632568),
                                        colors: [
                                          const Color(0xfff7f5f9),
                                          ImsColors.disablecolor
                                        ]),
                                  ),
                            height: 40 / actualHeight * screenHeight,
                            width: double.infinity,
                            alignment: Alignment.center,
                            child: Text(
                              ImsStrings.sSaveChanges,
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 14 / actualHeight * screenHeight),
                            ),
                          ),
                        ))
                  ],
                ),
              ),
            ),
            new Align(child: bodyProgress,alignment: FractionalOffset.center,),
          ],
        ),
      ),
    );
  }

  @override
  initState() {
    super.initState();
    // Add listeners to this class
    initializeChangePWDScreen();
    getUserName();
    getCurrentUseremail();
  }

  bool isOldPasswordEmpty() {
    if (controllerOldPwd.text.isEmpty) {
      return true;
    }
    return false;
  }

  bool isNewPasswordEmpty() {
    if (controllerNewPwd.text.isEmpty) {
      return true;
    }
    return false;
  }

  void showErrorMessage(String value) {
    _scaffoldstate.currentState.showSnackBar(new SnackBar(
      content: new Text(value),
      duration: new Duration(seconds: 3),
    ));
  }

  bool isReNewPasswordEmpty() {
    if (controllerReNewPwd.text.isEmpty) {
      return true;
    }
    return false;
  }


  void validateOldPassword() {
    setState(() {
      loaderChecking=true;
    });
    Map postdata = {
      "txtpin": userName,
//      "txtpin": 'IMS0000394697',
      "txtpassword": controllerOldPwd.text,
      "loginmode": "1",
    };
    debugPrint('Arun : ${controllerOldPwd.text}');
    debugPrint('Arun : ${postdata}');
    debugPrint('Arun : ${URL.ValidateOldpasswordLink}');
    debugPrint('Arun : ${globals.headers}');

    ApiService()
        .postAPISettings(URL.ValidateOldpasswordLink, postdata, globals.headers)
        .then((result) {
      if (result[0] == 'Login Successfully') {
        setState(() {
          loaderChecking=false;
        });
        debugPrint('Arun : Login is successful 1');
        //showErrorMessage(result[0]);

        resetpassword();
        debugPrint('Arun : Login is successful 2');
      } else {
        setState(() {
          loaderChecking=false;
        });
        showErrorMessage(result[0]);
        debugPrint('Arun : Login is Failure 1');
        initializeChangePWDScreen();
      }
//    setState(() {
//
//    });
    });
  }

  void resetpassword() {
    setState(() {
      loaderChecking=true;
    });
    Map postdata = {
//      "txtemail": "gourav.salanke@imsindia.com",
      "txtemail": sCurrentUseremail,
      "txtpassword": controllerReNewPwd.text,
    };
    debugPrint('Arun : ${controllerReNewPwd.text}');
    debugPrint('Arun : ${postdata}');
    debugPrint('Arun : ${URL.ResetPasswordLinkURLinSettings}');
    debugPrint('Arun : ${globals.headers}');

    ApiService()
        .postAPISettings(
            URL.ResetPasswordLinkURLinSettings, postdata, globals.headers)
        .then((result) {
      if (result[0] == 'Password Reset Successfully') {
        setState(() {
          loaderChecking=false;
        });
        showErrorMessage(result[0]);
      } else {
        setState(() {
          loaderChecking=false;
        });
        showErrorMessage(result[0]);
      }
      initializeChangePWDScreen();
//    setState(() {
//
//    });
    });
  }
}



void CalculateScreen(BuildContext context) {
  var _mediaQueryData = MediaQuery.of(context);
  screenWidthTotal = _mediaQueryData.size.width;
  screenHeightTotal = _mediaQueryData.size.height;
  _safeAreaHorizontal =
      _mediaQueryData.padding.left + _mediaQueryData.padding.right;
  _safeAreaVertical =
      _mediaQueryData.padding.top + _mediaQueryData.padding.bottom;

  screenHeight = screenHeightTotal - _safeAreaVertical;
  screenWidth = screenWidthTotal - _safeAreaHorizontal;
}

bool enableSubmit() {
  debugPrint('Arun old ${controllerOldPwd.text}');
  debugPrint('Arun new ${controllerNewPwd.text}');
  debugPrint('Arun renew ${controllerReNewPwd.text}');
  if (controllerOldPwd.text != null &&
      controllerNewPwd.text != null &&
      controllerReNewPwd.text != null) {
    return true;
  }
  return false;
}

void initializeChangePWDScreen() {
  controllerOldPwd.text = '';
  controllerNewPwd.text = '';
  controllerReNewPwd.text = '';
}

var userName = '';
void getUserName() async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  userName = prefs.getString(ImsStrings.sCurrentUserId);
}

var sCurrentUseremail = '';
void getCurrentUseremail() async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  sCurrentUseremail = prefs.getString(ImsStrings.sCurrentUserEmail);
}

import 'package:flutter/material.dart';
import 'package:imsindia/components/bottom_navigation.dart';
import 'package:imsindia/components/navigation_bar.dart';
import 'package:imsindia/views/Arun/IMSSharedPref.dart';

import '../ims_utility.dart';
import 'ChangePassword.dart';

class Settings extends StatefulWidget {
  bool monVal;
  Settings(this.monVal);

  @override
  _SettingsState createState() => _SettingsState();
}

const actualHeight = 730;
const actualWidth = 360;
var screenWidthTotal;
var screenHeightTotal;
var _safeAreaHorizontal;
var _safeAreaVertical;
var screenHeight;
var screenWidth;
//bool monVal ;

class _SettingsState extends State<Settings> {
//  double temp = (80 / actualHeight) * SizeConfig.screenHeight;
//  final String assetName = 'images/facebook.svg';
//  final Widget svgIcon = new SvgPicture.asset(assetName,
//      color: Colors.red, semanticsLabel: 'A red up arrow');
    @override
    void initState(){
      
      //  IMSSharedPref().get_limitCellularDataUsage(
      //                              monVal);
      //                              print(monVal);
      //                              print("object");
      super.initState();
    }
  @override
  Widget build(BuildContext context) {

    
    CalculateScreen(context);

    SizeConfig().init(context);
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: ImsStrings.sSettings,
      home: Scaffold(
        backgroundColor: Colors.white,
        
        appBar: AppBar(
          backgroundColor: Colors.white,
          brightness: Brightness.light,
          elevation: 0.0,
          centerTitle: false,
          titleSpacing: 0.0,
          iconTheme: IconThemeData(
            color: Colors.black, //change your color here
          ),
//          backgroundColor: Colors.white,
          title: Text(
            ImsStrings.sSettings,
            style: const TextStyle(
                color: Colors.black,
                fontWeight: FontWeight.w500,
                fontFamily: "IBMPlexSans",
                fontStyle: FontStyle.normal,
                fontSize: 16.0),
          ),
//          leading: IconButton(
//            icon: Icon(
//              Icons.arrow_back,
//              color: Colors.black,
//            ),
//            onPressed: () => Navigator.pop(context, false),
//          ),
        ),
        drawer: NavigationDrawer(),
//        bottomNavigationBar: BottomNavigation(),
        body: Container(
          child: Padding(
            padding: EdgeInsets.only(
                top: screenHeight * 30 / actualHeight,
                left: 20 / actualWidth * screenWidth,
                right: 20 / actualWidth * screenWidth),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                // Commented the Data Usage Toggle
                
                 Text(
                   ImsStrings.sDataUsage,
                   style: TextStyle(
                       color: ImsColors.dark_navy_blue,
                       fontFamily: "IBMPlexSans",
                       fontWeight: FontWeight.w500,
                       fontSize: screenHeight * 14 / actualHeight),
                 ),
                 Padding(
                     padding:
                         EdgeInsets.only(top: screenHeight * 2 / actualHeight),
                     child: Row(
                       children: <Widget>[
                         Expanded(
                           child: Text(
                             ImsStrings.sLimitcellulardatausage,
                             style: TextStyle(
                                 color: ImsColors.dark_navy_blue,
                                 fontFamily: "IBMPlexSans",
                                 fontWeight: FontWeight.w400,
                                 fontStyle: FontStyle.normal,
                                 fontSize: screenHeight * 14 / actualHeight),
                           ),
                           flex: 6,
                         ),
                         Expanded(
                           flex: 1,
                           child: Checkbox(
                             value: widget.monVal,
                             onChanged: (bool value) {
                               print('Arun set bool == ${value}');
                               IMSSharedPref().set_limitCellularDataUsage(value);
                               bool blimitCellularDataUsage = true;
                               IMSSharedPref().get_limitCellularDataUsage(
                                   ).then((blimitCellularDataUsage){
                                     print('Arun bool == ${blimitCellularDataUsage}');
                                   });
                               
                               setState(() {
                                 widget.monVal = value;
                               });
                             },
                           ),
                         ),
                       ],
                     )),
                 Padding(
                   padding:
                       EdgeInsets.only(top: screenHeight * 1 / actualHeight),
                   child: Text(
                     ImsStrings.sOnlystreamvideosonWifi,
                     style: TextStyle(
                         color: ImsColors.bluey_grey,
                         fontWeight: FontWeight.w400,
                         fontFamily: "IBMPlexSans",
                         fontStyle: FontStyle.normal,
                         fontSize: screenHeight * 12 / actualHeight),
                   ),
                 ),
                 Padding(
                   padding:
                       EdgeInsets.only(top: screenHeight * 20 / actualHeight),
                   child: Container(
                       width: double.infinity,
                       height: 1.0,
                       color: ImsColors.iceBlue),
                 ),
                Padding(
                  padding:
                      EdgeInsets.only(top: screenHeight * 20.5 / actualHeight),
                  child: Text(
                    ImsStrings.sPrivacy,
                    style: TextStyle(
                        color: ImsColors.dark_navy_blue,
                        fontFamily: "IBMPlexSans",
                        fontWeight: FontWeight.w500,
                        fontSize: screenHeight * 14 / actualHeight),
                  ),
                ),
//                Padding( //Hidden to be implemented in future release -- Arun
//                  padding:
//                      EdgeInsets.only(top: screenHeight * 15 / actualHeight),
//                  child: GestureDetector(
//                    onTap: () {
//                      _showDialogSearchData(context);
//                    },
//                    child: Text(
//                      ImsStrings.sClearSearchHistory,
//                      style: TextStyle(
//                          color: ImsColors.dark_navy_blue,
//                          fontFamily: "IBMPlexSans",
//                          fontWeight: FontWeight.w400,
//                          fontStyle: FontStyle.normal,
//                          fontSize: screenHeight * 14 / actualHeight),
//                    ),
//                  ),
//                ),
                Padding(
                  padding:
                      EdgeInsets.only(top: screenHeight * 15 / actualHeight),
                  child: GestureDetector(
                    onTap: () {
                      _showDialogAppData(context);
                    },
                    child: Text(
                      ImsStrings.sClearAppData,
                      style: TextStyle(
                          color: ImsColors.dark_navy_blue,
                          fontWeight: FontWeight.w400,
                          fontFamily: "IBMPlexSans",
                          fontStyle: FontStyle.normal,
                          fontSize: screenHeight * 14 / actualHeight),
                    ),
                  ),
                ),
                Padding(
                  padding:
                      EdgeInsets.only(top: screenHeight * 20 / actualHeight),
                  child: Container(
                      width: double.infinity,
                      height: 1.0,
                      color: ImsColors.iceBlue),
                ),
                Padding(
                  padding:
                      EdgeInsets.only(top: screenHeight * 20 / actualHeight),
                  child: Text(
                    ImsStrings.sPrivacy,
                    style: TextStyle(
                        color: ImsColors.dark_navy_blue,
                        fontFamily: "IBMPlexSans",
                        fontWeight: FontWeight.w500,
                        fontSize: screenHeight * 14 / actualHeight),
                  ),
                ),
                Padding(
                  padding:
                      EdgeInsets.only(top: screenHeight * 15 / actualHeight),
                  child: GestureDetector(
                    onTap: () {
                      try {
                        launchURL('${ImsStrings.URL_PRIVACYPOLICY}');
                      } catch (e) {
                        print(e.toString());
                      }
                    },
                    child: Text(
                      ImsStrings.sPrivacyPolicy,
                      style: TextStyle(
                          color: ImsColors.dark_navy_blue,
                          fontFamily: "IBMPlexSans",
                          fontWeight: FontWeight.w400,
                          fontStyle: FontStyle.normal,
                          fontSize: screenHeight * 14 / actualHeight),
                    ),
                  ),
                ),
                Padding(
                  padding:
                      EdgeInsets.only(top: screenHeight * 15 / actualHeight),
                  child: GestureDetector(
                    onTap: () {
                      try {
                        launchURL('${ImsStrings.URL_PRIVACYPOLICY}');
                      } catch (e) {
                        print(e.toString());
                      }
                    },
                    child: Text(
                      ImsStrings.sTermsofUse,
                      style: TextStyle(
                          color: ImsColors.dark_navy_blue,
                          fontFamily: "IBMPlexSans",
                          fontWeight: FontWeight.w400,
                          fontStyle: FontStyle.normal,
                          fontSize: screenHeight * 14 / actualHeight),
                    ),
                  ),
                ),
                Padding(
                  padding:
                      EdgeInsets.only(top: screenHeight * 20 / actualHeight),
                  child: Container(
                      width: double.infinity,
                      height: 1.0,
                      color: ImsColors.iceBlue),
                ),
                Padding(
                  padding:
                      EdgeInsets.only(top: screenHeight * 20 / actualHeight),
                  child: GestureDetector(
                    onTap: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => ChangePassword()),
                      );
                    },
                    child: Text(
                      ImsStrings.sChangePassword,
                      style: TextStyle(
                          color: ImsColors.dark_navy_blue,
                          fontFamily: "IBMPlexSans",
                          fontWeight: FontWeight.w500,
                          fontSize: screenHeight * 14 / actualHeight),
                    ),
                  ),
                ),
                Padding(
                  padding:
                      EdgeInsets.only(top: screenHeight * 20 / actualHeight),
                  child: Container(
                      width: double.infinity,
                      height: 1.0,
                      color: ImsColors.iceBlue),
                ),
//                Padding( //Hiden to be implemented in future release --Arun
//                  padding:
//                      EdgeInsets.only(top: screenHeight * 20 / actualHeight),
//                  child: Text(
//                    ImsStrings.sLinkAccounts,
//                    style: TextStyle(
//                        color: ImsColors.dark_navy_blue,
//                        fontFamily: "IBMPlexSans",
//                        fontWeight: FontWeight.w500,
//                        fontSize: screenHeight * 14 / actualHeight),
//                  ),
//                ),
//                Padding(
//                    padding:
//                        EdgeInsets.only(top: screenHeight * 15 / actualHeight),
//                    child: Row(
//                      children: <Widget>[
//                        Expanded(
//                            flex: 1,
//                            child: Row(
//                              children: <Widget>[
//                                Image.asset(
//                                  'assets/images/google_plus_2.png',
//                                  height: 20 / actualHeight * screenHeight,
//                                  width: 20 / actualWidth * screenWidth,
//                                ),
//                                Padding(
//                                  padding: EdgeInsets.only(
//                                      left: 10 / actualWidth * screenWidth),
//                                  child: Text(
//                                    ImsStrings.sGoogle,
//                                    style: TextStyle(
//                                        color: ImsColors.dark_navy_blue,
//                                        fontWeight: FontWeight.w400,
//                                        fontFamily: "IBMPlexSans",
//                                        fontStyle: FontStyle.normal,
//                                        fontSize:
//                                            screenHeight * 14 / actualHeight),
//                                  ),
//                                ),
//                              ],
//                            )),
//                        Expanded(
//                            flex: 1,
//                            child: Row(
//                              children: <Widget>[
//                                Text(
//                                  'Arun Chaudhary ',
//                                  style: TextStyle(
//                                      color: ImsColors.azure,
//                                      fontWeight: FontWeight.w400,
//                                      fontFamily: "IBMPlexSans",
//                                      fontStyle: FontStyle.normal,
//                                      fontSize:
//                                          screenHeight * 14 / actualHeight),
//                                ),
//                                Padding(
//                                  padding: EdgeInsets.only(
//                                      left: 10 / actualWidth * screenWidth),
//                                  child: Image.asset(
//                                    'assets/images/path.png',
//                                    height: 20 / actualHeight * screenHeight,
//                                    width: 20 / actualWidth * screenWidth,
//                                  ),
//                                ),
//                              ],
//                            ))
//                      ],
//                    )),
//                Padding(
//                    padding:
//                        EdgeInsets.only(top: screenHeight * 15 / actualHeight),
//                    child: Row(
//                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                      children: <Widget>[
//                        Expanded(
//                            flex: 1,
//                            child: Row(
//                              children: <Widget>[
//                                Image.asset(
//                                  'assets/images/facebook.png',
//                                  height: 20 / actualHeight * screenHeight,
//                                  width: 20 / actualWidth * screenWidth,
//                                ),
//                                Padding(
//                                  padding: EdgeInsets.only(
//                                      left: 10 / actualWidth * screenWidth),
//                                  child: Text(
//                                    ImsStrings.sFacebook,
//                                    style: TextStyle(
//                                        color: ImsColors.dark_navy_blue,
//                                        fontWeight: FontWeight.w400,
//                                        fontFamily: "IBMPlexSans",
//                                        fontStyle: FontStyle.normal,
//                                        fontSize:
//                                            screenHeight * 14 / actualHeight),
//                                  ),
//                                ),
//                              ],
//                            )),
//                        Expanded(
//                          flex: 1,
//                          child: Row(
//                            children: <Widget>[
//                              Text(
//                                'Vivek Ji ',
//                                style: TextStyle(
//                                    color: ImsColors.azure,
//                                    fontWeight: FontWeight.w400,
//                                    fontFamily: "IBMPlexSans",
//                                    fontStyle: FontStyle.normal,
//                                    fontSize: screenHeight * 14 / actualHeight),
//                              ),
//                              Padding(
//                                padding: EdgeInsets.only(
//                                    left: 10 / actualWidth * screenWidth),
//                                child: Image.asset(
//                                  'assets/images/path.png',
//                                  height: 20 / actualHeight * screenHeight,
//                                  width: 20 / actualWidth * screenWidth,
//                                ),
//                              ),
//                            ],
//                          ),
//                        )
//                      ],
//                    )),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

void CalculateScreen(BuildContext context) {
  var _mediaQueryData = MediaQuery.of(context);
  screenWidthTotal = _mediaQueryData.size.width;
  screenHeightTotal = _mediaQueryData.size.height;
  _safeAreaHorizontal =
      _mediaQueryData.padding.left + _mediaQueryData.padding.right;
  _safeAreaVertical =
      _mediaQueryData.padding.top + _mediaQueryData.padding.bottom;

  screenHeight = screenHeightTotal - _safeAreaVertical;
  screenWidth = screenWidthTotal - _safeAreaHorizontal;
}

void _showDialogSearchData(BuildContext context) {
  // flutter defined function
  showDialog(
    context: context,
    builder: (BuildContext context) {
      // return object of type Dialog
      return AlertDialog(
        title: new Text("Alert!!"),
        content: new Text("Are you sure, You want to clear search history"),
        actions: <Widget>[
          // usually buttons at the bottom of the dialog
          new FlatButton(
            child: new Text("Yes"),
            onPressed: () {
              ClearLocalSearchData();
              Navigator.of(context).pop();
            },
          ),
          new FlatButton(
            child: new Text("No"),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
        ],
      );
    },
  );
}

void _showDialogAppData(BuildContext context) {
  // flutter defined function
  showDialog(
    context: context,
    builder: (BuildContext context) {
      // return object of type Dialog
      return AlertDialog(
        title: new Text("Alert!!"),
        content: new Text("Are you sure, You want to clear app data"),
        actions: <Widget>[
          // usually buttons at the bottom of the dialog
          new FlatButton(
            child: new Text("Yes"),
            onPressed: () {
              ClearAppData(context);
            },
          ),
          new FlatButton(
            child: new Text("No"),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
        ],
      );
    },
  );
}
/*

Sorry for the late reply.
I could not find settings screens in our Google drive folder.

Please find the description of each setting below:

    Limit Data Usage -
        By default, it should be unchecked
        If it's checked and the student launches a video, it should give the following prompt to the student
        "Your device isn't connected to a Wi-Fi network. Click continue to watch over a cellular network."
        The prompt should have 2 options - Continue & Cancel.

        [Arun ] refer IMSSharedPref class
        limitCellularDataUsage will store value of it as true / false please tacke action as mentioned {@All}

    Clear search history [Arun My action item here]
    Whenever a student searches something the search keywords are supposed to be stored so that the system prompts a list of previously searched keywords which will be open in a drop-down menu under the search box

    Clear App Data
    The idea behind this setting was that the OS-level setting of clearing data cleared everything and logs out the student. However, the app will be storing a lot of data on local storage like tests, reading materials etc. So using this student can clear the app data.
    Privacy policy and Terms of use will be links. Currently keep a single link "Privacy Policy and Terms" and point it to  https://www.imsindia.com/privacy-policy.html
    I will see if we need to have an app-specific policy
     Change password screens are present in the same document.
    It will be a 2 step process at back-end. Call Login API to verify the old password and then call Reset password API.
    Do you want me to create a word document and add the details?
    Link accountsrt5tks;'mky;dfp;56ik
    'sdiu;er788u 5['00gp5lckdjirt
    The idea was to let students link their Google and Facebook accounts so that they do not need to remember the password.
    I am not sure how it will work for the mobile app. I think the flow should be similar to the login by Google and Facebook.


Regards,
Gourav
 */

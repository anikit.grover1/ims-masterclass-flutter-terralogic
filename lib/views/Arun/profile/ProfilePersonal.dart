import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import '../ims_utility.dart';

const actualHeight = 768;
const actualWidth = 360;
var screenWidthTotal;
var screenHeightTotal;
var _safeAreaHorizontal;
var _safeAreaVertical;
var screenHeight;
var screenWidth;
var monVal = true;
var bShowNormalMode = true;

//Text('${itemsAll[0].subtitle}'),
TextEditingController controllerDisplayName = TextEditingController();
TextEditingController controllerImsPin = TextEditingController();
TextEditingController controllerMobileNo = TextEditingController();
TextEditingController controllerGender = TextEditingController();
TextEditingController controllerDateOfBirth = TextEditingController();
TextEditingController controllerEmailId = TextEditingController();
TextEditingController controllerMailingAddress = TextEditingController();
TextEditingController controllerPin = TextEditingController();
TextEditingController controllerCity = TextEditingController();
TextEditingController controllerState = TextEditingController();
TextEditingController controllerPermanentAddress = TextEditingController();
TextEditingController controllerPermanentPin = TextEditingController();
TextEditingController controllerPermanentCity = TextEditingController();
TextEditingController controllerPermanentState = TextEditingController();

void SetUpEditModeEnabled() {}
void SetUpEditModeDisabled() {}

class ProfilePersonal extends StatefulWidget {
  @override
  _ProfilePersonalState createState() => _ProfilePersonalState();
}

class _ProfilePersonalState extends State<ProfilePersonal> {
  @override
  Widget build(BuildContext context) {
    CalculateScreen(context);

    return SingleChildScrollView(
      child: Container(
          child: Column(
        children: <Widget>[
          Padding(
            padding: EdgeInsets.only(
              left: 20 / actualWidth * screenWidth,
              right: 20 / actualWidth * screenWidth,
              top: 20 / actualHeight * screenHeight,
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Visibility(
                  visible: bShowNormalMode,
                  child: GestureDetector(
                    onTap: () {
                      if (monVal)
                        monVal = false;
                      else
                        monVal = true;
                      setState(() {
                        bShowNormalMode = false;
                        controllerDisplayName.text = '${itemsAll[0].subtitle}';
                        controllerImsPin.text = '${itemsAll[1].subtitle}';
                        controllerMobileNo.text = '${itemsAll[2].subtitle}';
                        controllerGender.text = '${itemsAll[3].subtitle}';
                        controllerDateOfBirth.text = '${itemsAll[4].subtitle}';
                        controllerEmailId.text = '${itemsAll[5].subtitle}';
                        controllerMailingAddress.text =
                            '${itemsAll[6].subtitle}';
                        controllerPin.text = '${itemsAll[7].subtitle}';
                        controllerCity.text = '${itemsAll[8].subtitle}';
                        controllerState.text = '${itemsAll[9].subtitle}';
                        controllerPermanentAddress.text =
                            '${itemsAll[10].subtitle}';
                        controllerPermanentPin.text =
                            '${itemsAll[11].subtitle}';
                        controllerPermanentCity.text =
                            '${itemsAll[12].subtitle}';
                        controllerPermanentState.text =
                            '${itemsAll[13].subtitle}';
                      });
                    },
                    child: Container(
                      alignment: Alignment.center,
                      width: double.infinity,
                      height: screenHeight * 30 / actualHeight,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(
                          6.0,
                        ),
                        border: Border.all(
                          color: ImsColors.gunmetal,
                          width: 0.4,
                        ),
                      ),
                      child: Text(
                        ImsStrings.sEdit,
                        style: TextStyle(
                          fontSize: 14,
                           fontFamily: "IBMPlexSans",
                fontStyle: FontStyle.normal,
                          color: ImsColors.azure,
                          fontWeight: FontWeight.w500,
                        ),
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  height: screenHeight * 550 / actualHeight,
                  width: double.infinity,
                  child: BodyLayout(),
                ),
              ],
            ),
          ),
          Visibility(
            visible: !bShowNormalMode,
            child: GestureDetector(
              onTap: () {
                if (monVal)
                  monVal = false;
                else
                  monVal = true;
                setState(() {
                  bShowNormalMode = true;
                });
              },
              child: Container(
                decoration: BoxDecoration(
                  gradient: LinearGradient(
                    begin: Alignment.topLeft,
                    // end: Alignment(0.8, 0.0), // 10% of the width, so there are ten blinds.
                    colors: [
                      const Color(0xFF3380cc),
                      const Color(0xFF886dd7)
                    ], // whitish to gray
                    tileMode: TileMode
                        .repeated, // repeats the gradient over the canvas
                  ),
                ),
                alignment: Alignment.center,
                width: double.infinity,
                height: screenHeight * 40 / actualHeight,
                child: Text(
                  ImsStrings.sSave,
                  style: TextStyle(
                    fontSize: 14,
                     fontFamily: "IBMPlexSans",
                fontStyle: FontStyle.normal,
                    color: Colors.white,
                    fontWeight: FontWeight.w500,
                  ),
                ),
              ),
            ),
          ),
        ],
      )),
    );
  }
}

class Item {
  var title = 'Total Experience (in months)';
  var subtitle = '23';
  Item(String titl, String subtitl) {
    this.title = titl;
    this.subtitle = subtitl;
  }
//  var subtitle2 = '50 min Arun';
//  var imgUrl =
//      'https://thumbs.dreamstime.com/b/inspirational-quote-there-blessings-everyday-find-them-create-treasure-beautiful-white-single-sinnia-flower-blossom-blurry-151653366.jpg';
}

void CalculateScreen(BuildContext context) {
  var _mediaQueryData = MediaQuery.of(context);
  screenWidthTotal = _mediaQueryData.size.width;
  screenHeightTotal = _mediaQueryData.size.height;
  _safeAreaHorizontal =
      _mediaQueryData.padding.left + _mediaQueryData.padding.right;
  _safeAreaVertical =
      _mediaQueryData.padding.top + _mediaQueryData.padding.bottom;

  screenHeight = screenHeightTotal - _safeAreaVertical;
  screenWidth = screenWidthTotal - _safeAreaHorizontal;
}

class BodyLayout extends StatefulWidget {
  @override
  BodyLayoutState createState() {
    return new BodyLayoutState();
  }
}

List<Item> itemsAll = [
  Item('Display Name', 'Arun Kumar Chaudhary'),
  Item('IMS PIN', 'IMS0000421041'),
  Item('Mobile No.', '+9199990756785'),
  Item('Gender', 'Male'),
  Item('Date of Birth', '14/04/2019'),
  Item('Email ID', 'royalfren@gmail.com'),
  Item('Mailing Address',
      '702, Moreshwar Complex, Opp. IDBI Bank, Plot no. 35, \nSector 21, Navi Mumbai'),
  Item('Pincode', '400093'),
  Item('City', 'Mumbai'),
  Item('State', 'Maharashtra'),
  Item('Permanent Address',
      '702, Moreshwar Complex, Opp. IDBI Bank, Plot no. 35, Sector 21, Navi Mumbai'),
  Item('Pincode', '400093'),
  Item('City', 'Mumbai'),
  Item('State', 'Maharashtra'),
];

class BodyLayoutState extends State<BodyLayout> {
  List<String> titles = ['Sun', 'Moon', 'Star', 'Arun', 'sunny'];
  final FocusNode _firstInputFocusNode = new FocusNode();
  final FocusNode _secondInputFocusNode = new FocusNode();
  final FocusNode _thirdInputFocusNode = new FocusNode();
  final FocusNode _fourthInputFocusNode = new FocusNode();
  final FocusNode _fifthInputFocusNode = new FocusNode();
  final FocusNode _sixthInputFocusNode = new FocusNode();
  final FocusNode _seventhInputFocusNode = new FocusNode();
  final FocusNode _eighthInputFocusNode = new FocusNode();
  final FocusNode _ninthInputFocusNode = new FocusNode();
  final FocusNode _tenthInputFocusNode = new FocusNode();
  final FocusNode _eleevnthInputFocusNode = new FocusNode();
  final FocusNode _twelveInputFocusNode = new FocusNode();
  final FocusNode _thirteenthInputFocusNode = new FocusNode();
  final FocusNode _fourteenthInputFocusNode = new FocusNode();
  var switchOn = false;
  void _onSwitchChanged(bool value) {
    setState(() {
      switchOn = value;
    });
  }

  @override
  Widget build(BuildContext context) {
    return _myListView();
  }

  Widget _myListView() {
    return ListView(
      children: <Widget>[
        Container(
//
            width: double.infinity,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.only(
                      top: 15 / actualHeight * screenHeight,
                      bottom: bShowNormalMode
                          ? 15 / actualHeight * screenHeight
                          : 1 / actualHeight * screenHeight),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        itemsAll[0].title,
                        maxLines: 1,
                        textAlign: TextAlign.left,
                        style: TextStyle(
                            fontSize: 12.0 / actualHeight * screenHeight,
                            fontFamily: "IBMPlexSans",
                            fontStyle: FontStyle.normal,
                            fontWeight: FontWeight.normal,
                            color: ImsColors.bluey_grey),
                      ),
                      Container(
                        margin: EdgeInsets.only(
                            top: 3 / actualHeight * screenHeight),
                        child: Container(
                          // height: 18 / actualHeight * screenHeight,
                          child: bShowNormalMode
                              ? Text(
                                  '${itemsAll[0].subtitle}',
                                  textAlign: TextAlign.left,
                                  maxLines: 1,
                                  style: TextStyle(
                                      fontSize:
                                          14.0 / actualHeight * screenHeight,
                                      fontFamily: "IBMPlexSans",
                                      fontStyle: FontStyle.normal,
                                      fontWeight: FontWeight.normal,
                                      color: ImsColors.dark_navy_blue),
                                )
                              : TextField(
                                  focusNode: _firstInputFocusNode,
                                  textInputAction: TextInputAction.next,
                                  onEditingComplete: () {
                                    FocusScope.of(context)
                                        .requestFocus(_secondInputFocusNode);
                                  },
                                  controller: controllerDisplayName,
                                  textAlign: TextAlign.left,
                                  decoration:
                                      InputDecoration(border: InputBorder.none),
                                  maxLines: 1,
                                  style: TextStyle(
                                      fontSize:
                                          14.0 / actualHeight * screenHeight,
                                       fontFamily: "IBMPlexSans",
                fontStyle: FontStyle.normal,
                                      fontWeight: FontWeight.normal,
                                      color: ImsColors.dark_navy_blue),
                                ),
                        ),
                      ),
                    ],
                  ),
                ),
                Container(
                  height: 1 / actualHeight * screenHeight,
                  width: double.infinity,
                  color: ImsColors.iceBlue,
                )
              ],
            )),
        Container(
          width: double.infinity,
          child: Row(
            children: <Widget>[
              Expanded(
                child: Container(
                  margin: EdgeInsets.only(right: 10),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      Padding(
                        padding: EdgeInsets.only(
                            top: 15 / actualHeight * screenHeight,
                            bottom: bShowNormalMode
                                ? 15 / actualHeight * screenHeight
                                : 1 / actualHeight * screenHeight),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              itemsAll[1].title,
                              maxLines: 1,
                              textAlign: TextAlign.left,
                              style: TextStyle(
                                  fontSize: 12.0 / actualHeight * screenHeight,
                                   fontFamily: "IBMPlexSans",
                fontStyle: FontStyle.normal,
                                  fontWeight: FontWeight.normal,
                                  color: ImsColors.bluey_grey),
                            ),
                            Container(
                              margin: EdgeInsets.only(
                                  top: 3 / actualHeight * screenHeight),
                              child: Container(
                                // height: 18 / actualHeight * screenHeight,
                                child: bShowNormalMode
                                    ? Text(
                                        '${itemsAll[1].subtitle}',
                                        textAlign: TextAlign.left,
                                        maxLines: 1,
                                        style: TextStyle(
                                            fontSize: 14.0 /
                                                actualHeight *
                                                screenHeight,
                                             fontFamily: "IBMPlexSans",
                fontStyle: FontStyle.normal,
                                            fontWeight: FontWeight.normal,
                                            color: ImsColors.dark_navy_blue),
                                      )
                                    : TextField(
                                        focusNode: _secondInputFocusNode,
                                        textInputAction: TextInputAction.next,
                                        onEditingComplete: () {
                                          FocusScope.of(context).requestFocus(
                                              _thirdInputFocusNode);
                                        },
                                        controller: controllerImsPin,
                                        textAlign: TextAlign.left,
                                        decoration: InputDecoration(
                                            border: InputBorder.none),
                                        maxLines: 1,
                                        style: TextStyle(
                                            fontSize: 14.0 /
                                                actualHeight *
                                                screenHeight,
                                             fontFamily: "IBMPlexSans",
                fontStyle: FontStyle.normal,
                                            fontWeight: FontWeight.normal,
                                            color: ImsColors.dark_navy_blue),
                                      ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      Container(
                        height: 1 / actualHeight * screenHeight,
                        width: double.infinity,
                        color: ImsColors.iceBlue,
                      )
                    ],
                  ),
                ),
              ),
              Expanded(
                  child: Container(
                margin: EdgeInsets.only(left: 10),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.only(
                          top: 15 / actualHeight * screenHeight,
                          bottom: bShowNormalMode
                              ? 15 / actualHeight * screenHeight
                              : 1 / actualHeight * screenHeight),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            itemsAll[2].title,
                            maxLines: 1,
                            textAlign: TextAlign.left,
                            style: TextStyle(
                                fontSize: 12.0 / actualHeight * screenHeight,
                                 fontFamily: "IBMPlexSans",
                fontStyle: FontStyle.normal,
                                fontWeight: FontWeight.normal,
                                color: ImsColors.bluey_grey),
                          ),
                          Container(
                            margin: EdgeInsets.only(
                                top: 3 / actualHeight * screenHeight),
                            child: Container(
                              // height: 18 / actualHeight * screenHeight,
                              child: bShowNormalMode
                                  ? Text(
                                      '${itemsAll[2].subtitle}',
                                      textAlign: TextAlign.left,
                                      maxLines: 1,
                                      style: TextStyle(
                                          fontSize: 14.0 /
                                              actualHeight *
                                              screenHeight,
                                           fontFamily: "IBMPlexSans",
                fontStyle: FontStyle.normal,
                                          fontWeight: FontWeight.normal,
                                          color: ImsColors.dark_navy_blue),
                                    )
                                  : TextField(
                                      focusNode: _thirdInputFocusNode,
                                      textInputAction: TextInputAction.next,
                                      onEditingComplete: () {
                                        FocusScope.of(context).requestFocus(
                                            _fourthInputFocusNode);
                                      },
                                      inputFormatters: [
                                        WhitelistingTextInputFormatter
                                            .digitsOnly,
                                        LengthLimitingTextInputFormatter(12),
                                      ],
                                      controller: controllerMobileNo,
                                      textAlign: TextAlign.left,
                                      decoration: InputDecoration(
                                          border: InputBorder.none),
                                      maxLines: 1,
                                      style: TextStyle(
                                          fontSize: 14.0 /
                                              actualHeight *
                                              screenHeight,
                                           fontFamily: "IBMPlexSans",
                fontStyle: FontStyle.normal,
                                          fontWeight: FontWeight.normal,
                                          color: ImsColors.dark_navy_blue),
                                    ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      height: 1 / actualHeight * screenHeight,
                      width: double.infinity,
                      color: ImsColors.iceBlue,
                    )
                  ],
                ),
              )),
            ],
          ),
        ),
        Container(
          width: double.infinity,
          child: Row(
            children: <Widget>[
              Expanded(
                child: Container(
                  margin: EdgeInsets.only(right: 10),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      Padding(
                        padding: EdgeInsets.only(
                            top: 15 / actualHeight * screenHeight,
                            bottom: bShowNormalMode
                                ? 15 / actualHeight * screenHeight
                                : 1 / actualHeight * screenHeight),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              itemsAll[3].title,
                              maxLines: 1,
                              textAlign: TextAlign.left,
                              style: TextStyle(
                                  fontSize: 12.0 / actualHeight * screenHeight,
                                   fontFamily: "IBMPlexSans",
                fontStyle: FontStyle.normal,
                                  fontWeight: FontWeight.normal,
                                  color: ImsColors.bluey_grey),
                            ),
                            Container(
                              margin: EdgeInsets.only(
                                  top: 3 / actualHeight * screenHeight),
                              child: Container(
                                // height: 18 / actualHeight * screenHeight,
                                child: bShowNormalMode
                                    ? Text(
                                        '${itemsAll[3].subtitle}',
                                        textAlign: TextAlign.left,
                                        maxLines: 1,
                                        style: TextStyle(
                                            fontSize: 14.0 /
                                                actualHeight *
                                                screenHeight,
                                             fontFamily: "IBMPlexSans",
                fontStyle: FontStyle.normal,
                                            fontWeight: FontWeight.normal,
                                            color: ImsColors.dark_navy_blue),
                                      )
                                    : TextField(
                                        focusNode: _fourthInputFocusNode,
                                        textInputAction: TextInputAction.next,
                                        onEditingComplete: () {
                                          FocusScope.of(context).requestFocus(
                                              _fifthInputFocusNode);
                                        },
                                        controller: controllerGender,
                                        textAlign: TextAlign.left,
                                        decoration: InputDecoration(
                                            border: InputBorder.none),
                                        maxLines: 1,
                                        style: TextStyle(
                                            fontSize: 14.0 /
                                                actualHeight *
                                                screenHeight,
                                             fontFamily: "IBMPlexSans",
                fontStyle: FontStyle.normal,
                                            fontWeight: FontWeight.normal,
                                            color: ImsColors.dark_navy_blue),
                                      ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      Container(
                        height: 1 / actualHeight * screenHeight,
                        width: double.infinity,
                        color: ImsColors.iceBlue,
                      )
                    ],
                  ),
                ),
              ),
              Expanded(
                  child: Container(
                margin: EdgeInsets.only(left: 10),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.only(
                          top: 15 / actualHeight * screenHeight,
                          bottom: bShowNormalMode
                              ? 15 / actualHeight * screenHeight
                              : 1 / actualHeight * screenHeight),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            itemsAll[4].title,
                            maxLines: 1,
                            textAlign: TextAlign.left,
                            style: TextStyle(
                                fontSize: 12.0 / actualHeight * screenHeight,
                                 fontFamily: "IBMPlexSans",
                fontStyle: FontStyle.normal,
                                fontWeight: FontWeight.normal,
                                color: ImsColors.bluey_grey),
                          ),
                          Container(
                            margin: EdgeInsets.only(
                                top: 3 / actualHeight * screenHeight),
                            child: Container(
                              // height: 18 / actualHeight * screenHeight,
                              child: bShowNormalMode
                                  ? Text(
                                      '${itemsAll[4].subtitle}',
                                      textAlign: TextAlign.left,
                                      maxLines: 1,
                                      style: TextStyle(
                                          fontSize: 14.0 /
                                              actualHeight *
                                              screenHeight,
                                           fontFamily: "IBMPlexSans",
                fontStyle: FontStyle.normal,
                                          fontWeight: FontWeight.normal,
                                          color: ImsColors.dark_navy_blue),
                                    )
                                  : TextField(
                                      focusNode: _fifthInputFocusNode,
                                      textInputAction: TextInputAction.next,
                                      onEditingComplete: () {
                                        FocusScope.of(context)
                                            .requestFocus(_sixthInputFocusNode);
                                      },
                                      controller: controllerDateOfBirth,
                                      textAlign: TextAlign.left,
                                      decoration: InputDecoration(
                                          border: InputBorder.none),
                                      maxLines: 1,
                                      style: TextStyle(
                                          fontSize: 14.0 /
                                              actualHeight *
                                              screenHeight,
                                           fontFamily: "IBMPlexSans",
                fontStyle: FontStyle.normal,
                                          fontWeight: FontWeight.normal,
                                          color: ImsColors.dark_navy_blue),
                                    ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      height: 1 / actualHeight * screenHeight,
                      width: double.infinity,
                      color: ImsColors.iceBlue,
                    )
                  ],
                ),
              )),
            ],
          ),
        ),
        Container(
//
            width: double.infinity,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.only(
                      top: 15 / actualHeight * screenHeight,
                      bottom: bShowNormalMode
                          ? 15 / actualHeight * screenHeight
                          : 1 / actualHeight * screenHeight),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        itemsAll[5].title,
                        maxLines: 1,
                        textAlign: TextAlign.left,
                        style: TextStyle(
                            fontSize: 12.0 / actualHeight * screenHeight,
                             fontFamily: "IBMPlexSans",
                fontStyle: FontStyle.normal,
                            fontWeight: FontWeight.normal,
                            color: ImsColors.bluey_grey),
                      ),
                      Container(
                        margin: EdgeInsets.only(
                            top: 3 / actualHeight * screenHeight),
                        child: Container(
                          // height: 18 / actualHeight * screenHeight,
                          child: bShowNormalMode
                              ? Text(
                                  '${itemsAll[5].subtitle}',
                                  textAlign: TextAlign.left,
                                  maxLines: 1,
                                  style: TextStyle(
                                      fontSize:
                                          14.0 / actualHeight * screenHeight,
                                       fontFamily: "IBMPlexSans",
                fontStyle: FontStyle.normal,
                                      fontWeight: FontWeight.normal,
                                      color: ImsColors.dark_navy_blue),
                                )
                              : TextField(
                                  focusNode: _sixthInputFocusNode,
                                  textInputAction: TextInputAction.next,
                                  onEditingComplete: () {
                                    FocusScope.of(context)
                                        .requestFocus(_seventhInputFocusNode);
                                  },
                                  controller: controllerEmailId,
                                  textAlign: TextAlign.left,
                                  decoration:
                                      InputDecoration(border: InputBorder.none),
                                  maxLines: 1,
                                  style: TextStyle(
                                      fontSize:
                                          14.0 / actualHeight * screenHeight,
                                       fontFamily: "IBMPlexSans",
                fontStyle: FontStyle.normal,
                                      fontWeight: FontWeight.normal,
                                      color: ImsColors.dark_navy_blue),
                                ),
                        ),
                      ),
                    ],
                  ),
                ),
                Container(
                  height: 1 / actualHeight * screenHeight,
                  width: double.infinity,
                  color: ImsColors.iceBlue,
                )
              ],
            )),
        Container(
            width: double.infinity,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.only(
                      top: 15 / actualHeight * screenHeight,
                      bottom: bShowNormalMode
                          ? 15 / actualHeight * screenHeight
                          : 1 / actualHeight * screenHeight),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        itemsAll[6].title,
                        maxLines: 1,
                        textAlign: TextAlign.left,
                        style: TextStyle(
                            fontSize: 12.0 / actualHeight * screenHeight,
                             fontFamily: "IBMPlexSans",
                fontStyle: FontStyle.normal,
                            fontWeight: FontWeight.normal,
                            color: ImsColors.bluey_grey),
                      ),
                      Container(
                        margin: EdgeInsets.only(
                            top: 3 / actualHeight * screenHeight),
                        child: Container(
                          // height: 18 / actualHeight * screenHeight,
                          child: bShowNormalMode
                              ? Text(
                                  '${itemsAll[6].subtitle}',
                                  textAlign: TextAlign.left,
                                  maxLines: 2,
                                  style: TextStyle(
                                      fontSize:
                                          14.0 / actualHeight * screenHeight,
                                       fontFamily: "IBMPlexSans",
                fontStyle: FontStyle.normal,
                                      fontWeight: FontWeight.normal,
                                      color: ImsColors.dark_navy_blue),
                                )
                              : TextField(
                                  focusNode: _seventhInputFocusNode,
                                  textInputAction: TextInputAction.next,
                                  onEditingComplete: () {
                                    FocusScope.of(context)
                                        .requestFocus(_eighthInputFocusNode);
                                  },
                                  controller: controllerMailingAddress,
                                  textAlign: TextAlign.left,
                                  decoration:
                                      InputDecoration(border: InputBorder.none),
                                  maxLines: 2,
                                  style: TextStyle(
                                      fontSize:
                                          14.0 / actualHeight * screenHeight,
                                       fontFamily: "IBMPlexSans",
                fontStyle: FontStyle.normal,
                                      fontWeight: FontWeight.normal,
                                      color: ImsColors.dark_navy_blue),
                                ),
                        ),
                      ),
                    ],
                  ),
                ),
                Visibility(
                  visible: !bShowNormalMode,
                  child: Container(
                    height: 1 / actualHeight * screenHeight,
                    width: double.infinity,
                    color: ImsColors.iceBlue,
                  ),
                )
              ],
            )),
        Container(
          width: double.infinity,
          child: Row(
            children: <Widget>[
              Expanded(
                child: Container(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      Padding(
                        padding: EdgeInsets.only(
                            top: 15 / actualHeight * screenHeight,
                            bottom: bShowNormalMode
                                ? 15 / actualHeight * screenHeight
                                : 1 / actualHeight * screenHeight),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              itemsAll[7].title,
                              maxLines: 1,
                              textAlign: TextAlign.left,
                              style: TextStyle(
                                  fontSize: 12.0 / actualHeight * screenHeight,
                                   fontFamily: "IBMPlexSans",
                fontStyle: FontStyle.normal,
                                  fontWeight: FontWeight.normal,
                                  color: ImsColors.bluey_grey),
                            ),
                            Container(
                              margin: EdgeInsets.only(
                                  top: 3 / actualHeight * screenHeight),
                              child: Container(
                                // height: 18 / actualHeight * screenHeight,
                                child: bShowNormalMode
                                    ? Text(
                                        '${itemsAll[7].subtitle}',
                                        textAlign: TextAlign.left,
                                        maxLines: 1,
                                        style: TextStyle(
                                            fontSize: 14.0 /
                                                actualHeight *
                                                screenHeight,
                                             fontFamily: "IBMPlexSans",
                fontStyle: FontStyle.normal,
                                            fontWeight: FontWeight.normal,
                                            color: ImsColors.dark_navy_blue),
                                      )
                                    : TextField(
                                        focusNode: _eighthInputFocusNode,
                                        textInputAction: TextInputAction.next,
                                        onEditingComplete: () {
                                          FocusScope.of(context).requestFocus(
                                              _ninthInputFocusNode);
                                        },
                                        inputFormatters: [
                                          WhitelistingTextInputFormatter
                                              .digitsOnly,
                                          LengthLimitingTextInputFormatter(6),
                                        ],
                                        controller: controllerPin,
                                        textAlign: TextAlign.left,
                                        decoration: InputDecoration(
                                            border: InputBorder.none),
                                        maxLines: 1,
                                        style: TextStyle(
                                            fontSize: 14.0 /
                                                actualHeight *
                                                screenHeight,
                                             fontFamily: "IBMPlexSans",
                fontStyle: FontStyle.normal,
                                            fontWeight: FontWeight.normal,
                                            color: ImsColors.dark_navy_blue),
                                      ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      Visibility(
                        visible: !bShowNormalMode,
                        child: Container(
                          height: 1 / actualHeight * screenHeight,
                          width: double.infinity,
                          color: ImsColors.iceBlue,
                        ),
                      )
                    ],
                  ),
                ),
              ),
              Expanded(
                  child: Container(
                margin: EdgeInsets.only(left: 10),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.only(
                          top: 15 / actualHeight * screenHeight,
                          bottom: bShowNormalMode
                              ? 15 / actualHeight * screenHeight
                              : 1 / actualHeight * screenHeight),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            itemsAll[8].title,
                            maxLines: 1,
                            textAlign: TextAlign.left,
                            style: TextStyle(
                                fontSize: 12.0 / actualHeight * screenHeight,
                                 fontFamily: "IBMPlexSans",
                fontStyle: FontStyle.normal,
                                fontWeight: FontWeight.normal,
                                color: ImsColors.bluey_grey),
                          ),
                          Container(
                            margin: EdgeInsets.only(
                                top: 3 / actualHeight * screenHeight),
                            child: Container(
                              // height: 18 / actualHeight * screenHeight,
                              child: bShowNormalMode
                                  ? Text(
                                      '${itemsAll[8].subtitle}',
                                      textAlign: TextAlign.left,
                                      maxLines: 1,
                                      style: TextStyle(
                                          fontSize: 14.0 /
                                              actualHeight *
                                              screenHeight,
                                           fontFamily: "IBMPlexSans",
                fontStyle: FontStyle.normal,
                                          fontWeight: FontWeight.normal,
                                          color: ImsColors.dark_navy_blue),
                                    )
                                  : TextField(
                                      focusNode: _ninthInputFocusNode,
                                      textInputAction: TextInputAction.next,
                                      onEditingComplete: () {
                                        FocusScope.of(context)
                                            .requestFocus(_tenthInputFocusNode);
                                      },
                                      controller: controllerCity,
                                      textAlign: TextAlign.left,
                                      decoration: InputDecoration(
                                          border: InputBorder.none),
                                      maxLines: 1,
                                      style: TextStyle(
                                          fontSize: 14.0 /
                                              actualHeight *
                                              screenHeight,
                                           fontFamily: "IBMPlexSans",
                fontStyle: FontStyle.normal,
                                          fontWeight: FontWeight.normal,
                                          color: ImsColors.dark_navy_blue),
                                    ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Visibility(
                      visible: !bShowNormalMode,
                      child: Container(
                        height: 1 / actualHeight * screenHeight,
                        width: double.infinity,
                        color: ImsColors.iceBlue,
                      ),
                    )
                  ],
                ),
              )),
              Expanded(
                  child: Container(
                margin: EdgeInsets.only(left: 10),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.only(
                          top: 15 / actualHeight * screenHeight,
                          bottom: bShowNormalMode
                              ? 15 / actualHeight * screenHeight
                              : 1 / actualHeight * screenHeight),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            itemsAll[9].title,
                            maxLines: 1,
                            textAlign: TextAlign.left,
                            style: TextStyle(
                                fontSize: 12.0 / actualHeight * screenHeight,
                                 fontFamily: "IBMPlexSans",
                fontStyle: FontStyle.normal,
                                fontWeight: FontWeight.normal,
                                color: ImsColors.bluey_grey),
                          ),
                          Container(
                            margin: EdgeInsets.only(
                                top: 3 / actualHeight * screenHeight),
                            child: Container(
                              // height: 18 / actualHeight * screenHeight,
                              child: bShowNormalMode
                                  ? Text(
                                      '${itemsAll[9].subtitle}',
                                      textAlign: TextAlign.left,
                                      maxLines: 1,
                                      style: TextStyle(
                                          fontSize: 14.0 /
                                              actualHeight *
                                              screenHeight,
                                           fontFamily: "IBMPlexSans",
                fontStyle: FontStyle.normal,
                                          fontWeight: FontWeight.normal,
                                          color: ImsColors.dark_navy_blue),
                                    )
                                  : TextField(
                                      focusNode: _tenthInputFocusNode,
                                      textInputAction: TextInputAction.next,
                                      onEditingComplete: () {
                                        FocusScope.of(context).requestFocus(
                                            _eleevnthInputFocusNode);
                                      },
                                      controller: controllerState,
                                      textAlign: TextAlign.left,
                                      decoration: InputDecoration(
                                          border: InputBorder.none),
                                      maxLines: 1,
                                      style: TextStyle(
                                          fontSize: 14.0 /
                                              actualHeight *
                                              screenHeight,
                                           fontFamily: "IBMPlexSans",
                fontStyle: FontStyle.normal,
                                          fontWeight: FontWeight.normal,
                                          color: ImsColors.dark_navy_blue),
                                    ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Visibility(
                      visible: !bShowNormalMode,
                      child: Container(
                        height: 1 / actualHeight * screenHeight,
                        width: double.infinity,
                        color: ImsColors.iceBlue,
                      ),
                    )
                  ],
                ),
              )),
            ],
          ),
        ),
        Visibility(
          visible: bShowNormalMode,
          child: Container(
            height: 1 / actualHeight * screenHeight,
            width: double.infinity,
            color: ImsColors.iceBlue,
          ),
        ),
        Container(
            width: double.infinity,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.only(
                      top: 15 / actualHeight * screenHeight,
                      bottom: bShowNormalMode
                          ? 15 / actualHeight * screenHeight
                          : 1 / actualHeight * screenHeight),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Row(
                        children: <Widget>[
                          Text(
                            itemsAll[10].title,
                            maxLines: 1,
                            textAlign: TextAlign.left,
                            style: TextStyle(
                                fontSize: 12.0 / actualHeight * screenHeight,
                                 fontFamily: "IBMPlexSans",
                fontStyle: FontStyle.normal,
                                fontWeight: FontWeight.normal,
                                color: ImsColors.bluey_grey),
                          ),
                          Visibility(
                            visible: !bShowNormalMode,
                            child: Row(
                              children: <Widget>[
                                Padding(
                                  padding: EdgeInsets.only(left: 10.0),
                                  child: Text(
                                    'Same as Mailing Address',
                                    maxLines: 1,
                                    textAlign: TextAlign.left,
                                    style: TextStyle(
                                        fontSize:
                                            12.0 / actualHeight * screenHeight,
                                         fontFamily: "IBMPlexSans",
                fontStyle: FontStyle.normal,
                                        fontWeight: FontWeight.normal,
                                        color: ImsColors.gunmetal),
                                  ),
                                ),
                                Switch(
                                  onChanged: _onSwitchChanged,
                                  value: switchOn,
                                ),
                              ],
                            ),
                          )
                        ],
                      ),
                      Visibility(
                        visible: !switchOn,
                        child: Container(
                          margin: EdgeInsets.only(
                              top: 3 / actualHeight * screenHeight),
                          child: Container(
                            // height: 18 / actualHeight * screenHeight,
                            child: bShowNormalMode
                                ? Text(
                                    '${itemsAll[10].subtitle}',
                                    textAlign: TextAlign.left,
                                    maxLines: 2,
                                    style: TextStyle(
                                        fontSize:
                                            14.0 / actualHeight * screenHeight,
                                         fontFamily: "IBMPlexSans",
                fontStyle: FontStyle.normal,
                                        fontWeight: FontWeight.normal,
                                        color: ImsColors.dark_navy_blue),
                                  )
                                : TextField(
                                    focusNode: _eleevnthInputFocusNode,
                                    textInputAction: TextInputAction.next,
                                    onEditingComplete: () {
                                      FocusScope.of(context)
                                          .requestFocus(_twelveInputFocusNode);
                                    },
                                    controller: controllerPermanentAddress,
                                    textAlign: TextAlign.left,
                                    decoration: InputDecoration(
                                        border: InputBorder.none),
                                    maxLines: 2,
                                    style: TextStyle(
                                        fontSize:
                                            14.0 / actualHeight * screenHeight,
                                         fontFamily: "IBMPlexSans",
                fontStyle: FontStyle.normal,
                                        fontWeight: FontWeight.normal,
                                        color: ImsColors.dark_navy_blue),
                                  ),
                          ),
                        ),
                      ),
//                      Container(
//                        child: Switch(
//                          onChanged: _onSwitchChanged,
//                          value: switchOn,
//                        ),
//                      )
                    ],
                  ),
                ),
                Visibility(
                  visible: !switchOn && !bShowNormalMode,
                  child: Container(
                    height: 1 / actualHeight * screenHeight,
                    width: double.infinity,
                    color: ImsColors.iceBlue,
                  ),
                )
              ],
            )),
        Visibility(
          visible: !switchOn,
          child: Container(
            width: double.infinity,
            child: Row(
              children: <Widget>[
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      Padding(
                        padding: EdgeInsets.only(
                            top: 15 / actualHeight * screenHeight,
                            bottom: bShowNormalMode
                                ? 15 / actualHeight * screenHeight
                                : 1 / actualHeight * screenHeight),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              itemsAll[11].title,
                              maxLines: 1,
                              textAlign: TextAlign.left,
                              style: TextStyle(
                                  fontSize: 12.0 / actualHeight * screenHeight,
                                   fontFamily: "IBMPlexSans",
                fontStyle: FontStyle.normal,
                                  fontWeight: FontWeight.normal,
                                  color: ImsColors.bluey_grey),
                            ),
                            Container(
                              margin: EdgeInsets.only(
                                  top: 3 / actualHeight * screenHeight),
                              child: Container(
                                // height: 18 / actualHeight * screenHeight,
                                child: bShowNormalMode
                                    ? Text(
                                        '${itemsAll[11].subtitle}',
                                        textAlign: TextAlign.left,
                                        maxLines: 1,
                                        style: TextStyle(
                                            fontSize: 14.0 /
                                                actualHeight *
                                                screenHeight,
                                             fontFamily: "IBMPlexSans",
                fontStyle: FontStyle.normal,
                                            fontWeight: FontWeight.normal,
                                            color: ImsColors.dark_navy_blue),
                                      )
                                    : TextField(
                                        focusNode: _twelveInputFocusNode,
                                        textInputAction: TextInputAction.next,
                                        onEditingComplete: () {
                                          FocusScope.of(context).requestFocus(
                                              _thirteenthInputFocusNode);
                                        },
                                        inputFormatters: [
                                          WhitelistingTextInputFormatter
                                              .digitsOnly,
                                          LengthLimitingTextInputFormatter(6),
                                        ],
                                        controller: controllerPermanentPin,
                                        textAlign: TextAlign.left,
                                        decoration: InputDecoration(
                                            border: InputBorder.none),
                                        maxLines: 1,
                                        style: TextStyle(
                                            fontSize: 14.0 /
                                                actualHeight *
                                                screenHeight,
                                             fontFamily: "IBMPlexSans",
                fontStyle: FontStyle.normal,
                                            fontWeight: FontWeight.normal,
                                            color: ImsColors.dark_navy_blue),
                                      ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      Visibility(
                        visible: !bShowNormalMode,
                        child: Container(
                          height: 1 / actualHeight * screenHeight,
                          width: double.infinity,
                          color: ImsColors.iceBlue,
                        ),
                      )
                    ],
                  ),
                ),
                Expanded(
                    child: Container(
                  margin: EdgeInsets.only(left: 10),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      Padding(
                        padding: EdgeInsets.only(
                            top: 15 / actualHeight * screenHeight,
                            bottom: bShowNormalMode
                                ? 15 / actualHeight * screenHeight
                                : 1 / actualHeight * screenHeight),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              itemsAll[12].title,
                              maxLines: 1,
                              textAlign: TextAlign.left,
                              style: TextStyle(
                                  fontSize: 12.0 / actualHeight * screenHeight,
                                   fontFamily: "IBMPlexSans",
                fontStyle: FontStyle.normal,
                                  fontWeight: FontWeight.normal,
                                  color: ImsColors.bluey_grey),
                            ),
                            Container(
                              margin: EdgeInsets.only(
                                  top: 3 / actualHeight * screenHeight),
                              child: Container(
                                // height: 18 / actualHeight * screenHeight,
                                child: bShowNormalMode
                                    ? Text(
                                        '${itemsAll[12].subtitle}',
                                        textAlign: TextAlign.left,
                                        maxLines: 1,
                                        style: TextStyle(
                                            fontSize: 14.0 /
                                                actualHeight *
                                                screenHeight,
                                             fontFamily: "IBMPlexSans",
                fontStyle: FontStyle.normal,
                                            fontWeight: FontWeight.normal,
                                            color: ImsColors.dark_navy_blue),
                                      )
                                    : TextField(
                                        focusNode: _thirteenthInputFocusNode,
                                        textInputAction: TextInputAction.next,
                                        onEditingComplete: () {
                                          FocusScope.of(context).requestFocus(
                                              _fourteenthInputFocusNode);
                                        },
                                        controller: controllerPermanentCity,
                                        textAlign: TextAlign.left,
                                        decoration: InputDecoration(
                                            border: InputBorder.none),
                                        maxLines: 1,
                                        style: TextStyle(
                                            fontSize: 14.0 /
                                                actualHeight *
                                                screenHeight,
                                             fontFamily: "IBMPlexSans",
                fontStyle: FontStyle.normal,
                                            fontWeight: FontWeight.normal,
                                            color: ImsColors.dark_navy_blue),
                                      ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      Visibility(
                        visible: !bShowNormalMode,
                        child: Container(
                          height: 1 / actualHeight * screenHeight,
                          width: double.infinity,
                          color: ImsColors.iceBlue,
                        ),
                      )
                    ],
                  ),
                )),
                Expanded(
                    child: Container(
                  margin: EdgeInsets.only(left: 10),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      Padding(
                        padding: EdgeInsets.only(
                            top: 15 / actualHeight * screenHeight,
                            bottom: bShowNormalMode
                                ? 15 / actualHeight * screenHeight
                                : 1 / actualHeight * screenHeight),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              itemsAll[13].title,
                              maxLines: 1,
                              textAlign: TextAlign.left,
                              style: TextStyle(
                                  fontSize: 12.0 / actualHeight * screenHeight,
                                   fontFamily: "IBMPlexSans",
                fontStyle: FontStyle.normal,
                                  fontWeight: FontWeight.normal,
                                  color: ImsColors.bluey_grey),
                            ),
                            Container(
                              margin: EdgeInsets.only(
                                  top: 3 / actualHeight * screenHeight),
                              child: Container(
                                // height: 18 / actualHeight * screenHeight,
                                child: bShowNormalMode
                                    ? Text(
                                        '${itemsAll[13].subtitle}',
                                        textAlign: TextAlign.left,
                                        maxLines: 1,
                                        style: TextStyle(
                                            fontSize: 14.0 /
                                                actualHeight *
                                                screenHeight,
                                             fontFamily: "IBMPlexSans",
                fontStyle: FontStyle.normal,
                                            fontWeight: FontWeight.normal,
                                            color: ImsColors.dark_navy_blue),
                                      )
                                    : TextField(
                                        focusNode: _fourteenthInputFocusNode,
                                        textInputAction: TextInputAction.done,
                                        controller: controllerPermanentState,
                                        textAlign: TextAlign.left,
                                        decoration: InputDecoration(
                                            border: InputBorder.none),
                                        maxLines: 1,
                                        style: TextStyle(
                                            fontSize: 14.0 /
                                                actualHeight *
                                                screenHeight,
                                             fontFamily: "IBMPlexSans",
                fontStyle: FontStyle.normal,
                                            fontWeight: FontWeight.normal,
                                            color: ImsColors.dark_navy_blue),
                                      ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      Visibility(
                        visible: !bShowNormalMode,
                        child: Container(
                          height: 1 / actualHeight * screenHeight,
                          width: double.infinity,
                          color: ImsColors.iceBlue,
                        ),
                      )
                    ],
                  ),
                )),
              ],
            ),
          ),
        ),
      ],
    );
  }
}

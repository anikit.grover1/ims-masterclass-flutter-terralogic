import 'package:flutter/material.dart';

import '../ims_utility.dart';
import 'ProfileEducation.dart';
import 'ProfilePersonal.dart';
import 'ProfileWork.dart';
import 'package:imsindia/utils/emaximiser_svg_icons.dart';

const actualHeight = 720;
const actualWidth = 360;
var screenWidthTotal;
var screenHeightTotal;
var _safeAreaHorizontal;
var _safeAreaVertical;
var screenHeight;
var screenWidth;
var monVal = true;

class ProfileDetail extends StatefulWidget {
  @override
  _ProfileDetailState createState() => _ProfileDetailState();
}

class _ProfileDetailState extends State<ProfileDetail> {
  @override
  Widget build(BuildContext context) {
    CalculateScreen(context);
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: DefaultTabController(
        length: 3,
        child: Scaffold(
          // 
          //  
          appBar: AppBar(
            brightness: Brightness.light,
            elevation: 0.0,
            centerTitle: false,
            titleSpacing: 0.0,
            backgroundColor: Colors.white,
            title: Text(
              ImsStrings.sDetails,
              style: const TextStyle(
                  color: Colors.black,
                  fontWeight: FontWeight.w500,
                  fontFamily: "IBMPlexSans",
                  fontStyle: FontStyle.normal,
                  fontSize: 16.0),
            ),
            leading: IconButton(
              icon: getSvgIcon.backSvgIcon,
              onPressed: () => Navigator.pop(context, false),
            ),
            bottom: TabBar(
              labelStyle: TextStyle(fontSize: 14),
              isScrollable: false,
              indicatorColor: ImsColors.purpleish_blue,
              labelColor: ImsColors.purpleish_blue,
              unselectedLabelColor: ImsColors.bluey_grey,
              indicatorSize: TabBarIndicatorSize.tab,
              tabs: [
                Tab(
                  text: ImsStrings.sPersonal,
                ),
                Tab(
                  text: ImsStrings.sEducational,
                ),
                Tab(
                  text: ImsStrings.sWork,
                ),
              ],
            ),
          ),
          body: TabBarView(
            children: [
              ProfilePersonal(),
              ProfileEducation(),
              ProfileWork(),
            ],
          ),
        ),
      ),
    );
  }
}

void CalculateScreen(BuildContext context) {
  var _mediaQueryData = MediaQuery.of(context);
  screenWidthTotal = _mediaQueryData.size.width;
  screenHeightTotal = _mediaQueryData.size.height;
  _safeAreaHorizontal =
      _mediaQueryData.padding.left + _mediaQueryData.padding.right;
  _safeAreaVertical =
      _mediaQueryData.padding.top + _mediaQueryData.padding.bottom;

  screenHeight = screenHeightTotal - _safeAreaVertical;
  screenWidth = screenWidthTotal - _safeAreaHorizontal;
}

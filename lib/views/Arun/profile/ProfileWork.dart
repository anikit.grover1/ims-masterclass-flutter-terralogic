import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import '../ims_utility.dart';

const actualHeight = 768;
const actualWidth = 360;
var screenWidthTotal;
var screenHeightTotal;
var _safeAreaHorizontal;
var _safeAreaVertical;
var screenHeight;
var screenWidth;
var monVal = true;
var bShowNormalMode = true;
TextEditingController controllerExperience = TextEditingController();
TextEditingController controllerOrganisation = TextEditingController();
TextEditingController controllerSector = TextEditingController();

class ProfileWork extends StatefulWidget {
  @override
  _ProfileWorkState createState() => _ProfileWorkState();
}

class _ProfileWorkState extends State<ProfileWork> {
  @override
  Widget build(BuildContext context) {
    CalculateScreen(context);

    return SingleChildScrollView(
      child: Container(
          child: Column(
        children: <Widget>[
          Padding(
            padding: EdgeInsets.only(
              left: 20 / actualWidth * screenWidth,
              right: 20 / actualWidth * screenWidth,
              top: 20 / actualHeight * screenHeight,
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Visibility(
                  visible: bShowNormalMode,
                  child: GestureDetector(
                    onTap: () {
                      if (monVal)
                        monVal = false;
                      else
                        monVal = true;
                      setState(() {
                        bShowNormalMode = false;
                        controllerExperience.text = '${itemsAll[0].subtitle}';
                        controllerOrganisation.text = '${itemsAll[1].subtitle}';
                        controllerSector.text = '${itemsAll[2].subtitle}';
                      });
                    },
                    child: Container(
                      alignment: Alignment.center,
                      width: double.infinity,
                      height: screenHeight * 30 / actualHeight,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(
                          6.0,
                        ),
                        border: Border.all(
                          color: ImsColors.gunmetal,
                          width: 0.4,
                        ),
                      ),
                      child: Text(
                        ImsStrings.sEdit,
                        style: TextStyle(
                          fontSize: 14,
                          fontFamily: "IBMPlexSans",
                          fontStyle: FontStyle.normal,
                          color: ImsColors.azure,
                          fontWeight: FontWeight.w500,
                        ),
                      ),
                    ),
                  ),
                ),
                SizedBox(
//                  height: double.infinity,
                  height: screenHeight * 550 / actualHeight,
                  width: double.infinity,
                  child: BodyLayout(),
                ),
              ],
            ),
          ),
          Visibility(
            visible: !bShowNormalMode,
            child: GestureDetector(
              onTap: () {
                if (monVal)
                  monVal = false;
                else
                  monVal = true;
                setState(() {
                  bShowNormalMode = true;
                });
              },
              child: Container(
                margin: EdgeInsets.only(bottom: 150),
                decoration: BoxDecoration(
                  gradient: LinearGradient(
                    begin: Alignment.topLeft,
                    // end: Alignment(0.8, 0.0), // 10% of the width, so there are ten blinds.
                    colors: [
                      const Color(0xFF3380cc),
                      const Color(0xFF886dd7)
                    ], // whitish to gray
                    tileMode: TileMode
                        .repeated, // repeats the gradient over the canvas
                  ),
                ),
                alignment: Alignment.center,
                width: double.infinity,
                height: screenHeight * 40 / actualHeight,
                child: Text(
                  ImsStrings.sSave,
                  style: TextStyle(
                    fontSize: 14,
                    fontFamily: "IBMPlexSans",
                    fontStyle: FontStyle.normal,
                    color: Colors.white,
                    fontWeight: FontWeight.w500,
                  ),
                ),
              ),
            ),
          ),
        ],
      )),
    );
  }
}

class BodyLayout extends StatefulWidget {
  @override
  BodyLayoutState createState() {
    return new BodyLayoutState();
  }
}

List<Item> itemsAll = [
  Item('Total Experience (in months)', '23'),
  Item('Latest organisation you work with', 'Lollypop Design Studio'),
  Item('Sector', 'Design'),
];

class BodyLayoutState extends State<BodyLayout> {
  List<String> titles = ['Sun', 'Moon', 'Star', 'Arun', 'sunny'];
  final FocusNode _firstInputFocusNode = new FocusNode();
  final FocusNode _secondInputFocusNode = new FocusNode();
  final FocusNode _thirdInputFocusNode = new FocusNode();
  @override
  Widget build(BuildContext context) {
    return _myListView();
  }

  Widget _myListView() {
    return ListView(
      children: <Widget>[
        Container(
//            height: 75 / actualHeight * screenHeight,
            width: double.infinity,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.only(
                      top: 15 / actualHeight * screenHeight,
                      bottom: bShowNormalMode
                          ? 15 / actualHeight * screenHeight
                          : 1 / actualHeight * screenHeight),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        itemsAll[0].title,
                        maxLines: 1,
                        textAlign: TextAlign.left,
                        style: TextStyle(
                            fontSize: 12.0 / actualHeight * screenHeight,
                            fontFamily: "IBMPlexSans",
                            fontStyle: FontStyle.normal,
                            fontWeight: FontWeight.normal,
                            color: ImsColors.bluey_grey),
                      ),
                      Container(
                        margin: EdgeInsets.only(
                            top: 3 / actualHeight * screenHeight),
                        child: Container(
                          // height: 18 / actualHeight * screenHeight,
                          child: bShowNormalMode
                              ? Text(
                                  '${itemsAll[0].subtitle}',
                                  textAlign: TextAlign.left,
                                  maxLines: 1,
                                  style: TextStyle(
                                      fontSize:
                                          14.0 / actualHeight * screenHeight,
                                      fontFamily: "IBMPlexSans",
                                      fontStyle: FontStyle.normal,
                                      fontWeight: FontWeight.normal,
                                      color: ImsColors.dark_navy_blue),
                                )
                              : TextField(
                                  focusNode: _firstInputFocusNode,
                                  textInputAction: TextInputAction.next,
                                  onEditingComplete: () {
                                    FocusScope.of(context)
                                        .requestFocus(_secondInputFocusNode);
                                  },
                                  inputFormatters: [
                                    WhitelistingTextInputFormatter.digitsOnly,
                                    LengthLimitingTextInputFormatter(3),
                                  ],
                                  controller: controllerExperience,
                                  textAlign: TextAlign.left,
                                  decoration:
                                      InputDecoration(border: InputBorder.none),
                                  maxLines: 1,
                                  style: TextStyle(
                                      fontSize:
                                          14.0 / actualHeight * screenHeight,
                                      fontFamily: "IBMPlexSans",
                                      fontStyle: FontStyle.normal,
                                      fontWeight: FontWeight.normal,
                                      color: ImsColors.dark_navy_blue),
                                ),
                        ),
                      ),
                    ],
                  ),
                ),
                Container(
                  height: 1 / actualHeight * screenHeight,
                  width: double.infinity,
                  color: ImsColors.iceBlue,
                )
              ],
            )),
        Container(
//            height: 75 / actualHeight * screenHeight,
            width: double.infinity,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.only(
                      top: 15 / actualHeight * screenHeight,
                      bottom: bShowNormalMode
                          ? 15 / actualHeight * screenHeight
                          : 1 / actualHeight * screenHeight),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        itemsAll[1].title,
                        maxLines: 1,
                        textAlign: TextAlign.left,
                        style: TextStyle(
                            fontSize: 12.0 / actualHeight * screenHeight,
                            fontFamily: "IBMPlexSans",
                            fontStyle: FontStyle.normal,
                            fontWeight: FontWeight.normal,
                            color: ImsColors.bluey_grey),
                      ),
                      Container(
                        margin: EdgeInsets.only(
                            top: 3 / actualHeight * screenHeight),
                        child: Container(
                          // height: 18 / actualHeight * screenHeight,
                          child: bShowNormalMode
                              ? Text(
                                  '${itemsAll[1].subtitle}',
                                  textAlign: TextAlign.left,
                                  maxLines: 1,
                                  style: TextStyle(
                                      fontSize:
                                          14.0 / actualHeight * screenHeight,
                                      fontFamily: "IBMPlexSans",
                                      fontStyle: FontStyle.normal,
                                      fontWeight: FontWeight.normal,
                                      color: ImsColors.dark_navy_blue),
                                )
                              : TextField(
                                  focusNode: _secondInputFocusNode,
                                  textInputAction: TextInputAction.next,
                                  onEditingComplete: () {
                                    FocusScope.of(context)
                                        .requestFocus(_thirdInputFocusNode);
                                  },
                                  controller: controllerOrganisation,
                                  textAlign: TextAlign.left,
                                  decoration:
                                      InputDecoration(border: InputBorder.none),
                                  maxLines: 1,
                                  style: TextStyle(
                                      fontSize:
                                          14.0 / actualHeight * screenHeight,
                                      fontFamily: "IBMPlexSans",
                                      fontStyle: FontStyle.normal,
                                      fontWeight: FontWeight.normal,
                                      color: ImsColors.dark_navy_blue),
                                ),
                        ),
                      ),
                    ],
                  ),
                ),
                Container(
                  height: 1 / actualHeight * screenHeight,
                  width: double.infinity,
                  color: ImsColors.iceBlue,
                )
              ],
            )),
        Container(
//            height: 75 / actualHeight * screenHeight,
            width: double.infinity,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.only(
                      top: 15 / actualHeight * screenHeight,
                      bottom: bShowNormalMode
                          ? 15 / actualHeight * screenHeight
                          : 1 / actualHeight * screenHeight),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        itemsAll[2].title,
                        maxLines: 1,
                        textAlign: TextAlign.left,
                        style: TextStyle(
                            fontSize: 12.0 / actualHeight * screenHeight,
                            fontFamily: "IBMPlexSans",
                            fontStyle: FontStyle.normal,
                            fontWeight: FontWeight.normal,
                            color: ImsColors.bluey_grey),
                      ),
                      Container(
                        margin: EdgeInsets.only(
                            top: 3 / actualHeight * screenHeight),
                        child: Container(
                          // height: 18 / actualHeight * screenHeight,
                          child: bShowNormalMode
                              ? Text(
                                  '${itemsAll[2].subtitle}',
                                  textAlign: TextAlign.left,
                                  maxLines: 1,
                                  style: TextStyle(
                                      fontSize:
                                          14.0 / actualHeight * screenHeight,
                                      fontFamily: "IBMPlexSans",
                                      fontStyle: FontStyle.normal,
                                      fontWeight: FontWeight.normal,
                                      color: ImsColors.dark_navy_blue),
                                )
                              : TextField(
                                  focusNode: _thirdInputFocusNode,
                                  textInputAction: TextInputAction.done,
                                  controller: controllerSector,
                                  textAlign: TextAlign.left,
                                  decoration:
                                      InputDecoration(border: InputBorder.none),
                                  maxLines: 1,
                                  style: TextStyle(
                                      fontSize:
                                          14.0 / actualHeight * screenHeight,
                                      fontFamily: "IBMPlexSans",
                                      fontStyle: FontStyle.normal,
                                      fontWeight: FontWeight.normal,
                                      color: ImsColors.dark_navy_blue),
                                ),
                        ),
                      ),
                    ],
                  ),
                ),
                Container(
                  height: 1 / actualHeight * screenHeight,
                  width: double.infinity,
                  color: ImsColors.iceBlue,
                )
              ],
            )),
      ],
    );
  }
}

class Item {
  var title = 'Total Experience (in months)';
  var subtitle = '23';
  Item(String titl, String subtitl) {
    this.title = titl;
    this.subtitle = subtitl;
  }
//  var subtitle2 = '50 min Arun';
//  var imgUrl =
//      'https://thumbs.dreamstime.com/b/inspirational-quote-there-blessings-everyday-find-them-create-treasure-beautiful-white-single-sinnia-flower-blossom-blurry-151653366.jpg';
}

void CalculateScreen(BuildContext context) {
  var _mediaQueryData = MediaQuery.of(context);
  screenWidthTotal = _mediaQueryData.size.width;
  screenHeightTotal = _mediaQueryData.size.height;
  _safeAreaHorizontal =
      _mediaQueryData.padding.left + _mediaQueryData.padding.right;
  _safeAreaVertical =
      _mediaQueryData.padding.top + _mediaQueryData.padding.bottom;

  screenHeight = screenHeightTotal - _safeAreaVertical;
  screenWidth = screenWidthTotal - _safeAreaHorizontal;
}

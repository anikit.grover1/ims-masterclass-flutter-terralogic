//import 'package:flutter/material.dart';
//import 'package:imsindia/views/Arun/profile/ProfileDetail.dart';
//
//final actualHeight = 720;
//final actualWidth = 360;
//var screenWidthTotal;
//var screenHeightTotal;
//var _safeAreaHorizontal;
//var _safeAreaVertical;
//var screenHeight;
//var screenWidth;
//var monVal = true;
//
//class Profile extends StatelessWidget {
//  void onBtnSecondaryPressed(BuildContext context) => Navigator.push(
//      context, MaterialPageRoute(builder: (context) => ProfileDetail()));
//
//  void onGroup3Pressed(BuildContext context) {}
//
//  @override
//  Widget build(BuildContext context) {
//    CalculateScreen(context);
//    return Scaffold(
//      drawer: NavigationToolbar(),
//      body: Container(
//        constraints: BoxConstraints.expand(),
//        decoration: BoxDecoration(
//          color: Color.fromARGB(255, 255, 255, 255),
//        ),
//        child: Column(
//          crossAxisAlignment: CrossAxisAlignment.stretch,
//          children: [
//            Container(
//              height: 286,
//              child: Stack(
//                alignment: Alignment.center,
//                children: [
//                  Positioned(
//                    left: 0,
//                    top: 0,
//                    right: 0,
//                    child: Container(
//                      height: 286,
//                      child: Image.asset(
//                        "assets/images/profilemask-2.png",
//                        fit: BoxFit.cover,
//                      ),
//                    ),
//                  ),
//                  Positioned(
//                    left: 0,
//                    top: 42,
//                    right: 0,
//                    child: Container(
//                      height: 244,
//                      child: Column(
//                        crossAxisAlignment: CrossAxisAlignment.stretch,
//                        children: [
//                          Align(
//                            alignment: Alignment.topLeft,
//                            child: Container(
//                              width: 87,
//                              height: 20,
//                              margin: EdgeInsets.only(left: 16),
//                              child: Row(
//                                children: [
//                                  Expanded(
//                                    flex: 1,
//                                    child: Container(
//                                      height: 14,
//                                      margin: EdgeInsets.only(right: 20),
//                                      child: Image.asset(
//                                        "assets/images/hamburger.png",
//                                        fit: BoxFit.none,
//                                      ),
//                                    ),
//                                  ),
//                                  Container(
//                                    margin: EdgeInsets.only(right: 1),
//                                    child: Text(
//                                      "Profile",
//                                      style: TextStyle(
//                                        color:
//                                        Color.fromARGB(255, 255, 255, 255),
//                                        fontSize: 16,
//                                        fontFamily: "IBM Plex Sans",
//                                        fontWeight: FontWeight.w500,
//                                      ),
//                                      textAlign: TextAlign.left,
//                                    ),
//                                  ),
//                                ],
//                              ),
//                            ),
//                          ),
//                          Align(
//                            alignment: Alignment.topLeft,
//                            child: Container(
//                              width: 256,
//                              height: 95,
//                              margin: EdgeInsets.only(left: 20, top: 28),
//                              child: Row(
//                                crossAxisAlignment: CrossAxisAlignment.stretch,
//                                children: [
//                                  Align(
//                                    alignment: Alignment.topLeft,
//                                    child: Container(
//                                      width: 84,
//                                      height: 84,
//                                      child: Image.asset(
//                                        "assets/images/group-10.png",
//                                        fit: BoxFit.fill,
//                                      ),
//                                    ),
//                                  ),
//                                  Align(
//                                    alignment: Alignment.topLeft,
//                                    child: Container(
//                                      width: 147,
//                                      height: 93,
//                                      margin: EdgeInsets.only(left: 25, top: 2),
//                                      child: Column(
//                                        crossAxisAlignment:
//                                        CrossAxisAlignment.stretch,
//                                        children: [
//                                          Align(
//                                            alignment: Alignment.topLeft,
//                                            child: Text(
//                                              "Arun Chaudhary",
//                                              maxLines: 1,
//                                              style: TextStyle(
//                                                color: Color.fromARGB(
//                                                    255, 255, 255, 255),
//                                                fontSize: 18,
//                                                fontFamily: "IBM Plex Sans",
//                                                fontWeight: FontWeight.w700,
//                                              ),
//                                              textAlign: TextAlign.left,
//                                            ),
//                                          ),
//                                          Align(
//                                            alignment: Alignment.topLeft,
//                                            child: Container(
//                                              margin: EdgeInsets.only(top: 3),
//                                              child: Text(
//                                                "Central Delhi: Delhi",
//                                                maxLines: 1,
//                                                style: TextStyle(
//                                                  color: Color.fromARGB(
//                                                      255, 255, 255, 255),
//                                                  fontSize: 14,
//                                                  fontFamily: "IBM Plex Sans",
//                                                ),
//                                                textAlign: TextAlign.left,
//                                              ),
//                                            ),
//                                          ),
//                                          Align(
//                                            alignment: Alignment.topLeft,
//                                            child: Container(
//                                              width: 100,
//                                              height: 40,
//                                              margin: EdgeInsets.only(top: 8),
//                                              child: FlatButton(
//                                                onPressed: () => this
//                                                    .onBtnSecondaryPressed(
//                                                    context),
//                                                color: Color.fromARGB(
//                                                    255, 0, 171, 251),
//                                                shape: RoundedRectangleBorder(
//                                                  borderRadius:
//                                                  BorderRadius.all(
//                                                      Radius.circular(2)),
//                                                ),
//                                                textColor: Color.fromARGB(
//                                                    255, 255, 255, 255),
//                                                padding: EdgeInsets.all(0),
//                                                child: Text(
//                                                  "Your Details",
//                                                  style: TextStyle(
//                                                    fontSize: 14,
//                                                    fontFamily: "IBM Plex Sans",
//                                                    fontWeight: FontWeight.w500,
//                                                  ),
//                                                  textAlign: TextAlign.center,
//                                                ),
//                                              ),
//                                            ),
//                                          ),
//                                        ],
//                                      ),
//                                    ),
//                                  ),
//                                ],
//                              ),
//                            ),
//                          ),
//                          Container(
//                            height: 80,
//                            margin: EdgeInsets.only(top: 21),
//                            child: Opacity(
//                              opacity: 0.2,
//                              child: Image.asset(
//                                "assets/images/mask-copy-2.png",
//                                fit: BoxFit.cover,
//                              ),
//                            ),
//                          ),
//                        ],
//                      ),
//                    ),
//                  ),
//                  Positioned(
//                    left: 75,
//                    top: 148,
//                    child: Container(
//                      width: 25,
//                      height: 25,
//                      child: Image.asset(
//                        "assets/images/national-overall-leaderboard-topper-copy.png",
//                        fit: BoxFit.none,
//                      ),
//                    ),
//                  ),
//                  Positioned(
//                    top: 206,
//                    child: Container(
//                      width: 260,
//                      height: 80,
//                      child: Stack(
//                        alignment: Alignment.center,
//                        children: [
//                          Positioned(
//                            left: 0,
//                            top: 18,
//                            right: 0,
//                            child: Container(
//                              height: 44,
//                              child: Row(
//                                crossAxisAlignment: CrossAxisAlignment.stretch,
//                                children: [
//                                  Align(
//                                    alignment: Alignment.topLeft,
//                                    child: Container(
//                                      // width: 85,
//                                      height: 44,
//                                      child: Column(
//                                        crossAxisAlignment:
//                                        CrossAxisAlignment.start,
//                                        children: [
//                                          Container(
//                                            margin: EdgeInsets.only(left: 20),
//                                            child: Text(
//                                              "15",
//                                              maxLines: 1,
//                                              style: TextStyle(
//                                                color: Color.fromARGB(
//                                                    255, 255, 255, 255),
//                                                fontSize: 20,
//                                                fontFamily: "IBM Plex Sans",
//                                                fontWeight: FontWeight.w700,
//                                              ),
//                                              textAlign: TextAlign.center,
//                                            ),
//                                          ),
//                                          Container(
//                                            margin: EdgeInsets.only(top: 5),
//                                            child: Text(
//                                              "LEADERBOARD",
//                                              maxLines: 1,
//                                              style: TextStyle(
//                                                color: Color.fromARGB(
//                                                    255, 255, 255, 255),
//                                                fontSize: 12,
//                                                fontFamily: "IBM Plex Sans",
//                                                fontWeight: FontWeight.w500,
//                                              ),
//                                              textAlign: TextAlign.center,
//                                            ),
//                                          ),
//                                        ],
//                                      ),
//                                    ),
//                                  ),
//                                  Spacer(),
//                                  Align(
//                                    alignment: Alignment.topLeft,
//                                    child: Container(
//                                      // width: 82,
//                                      height: 44,
//                                      child: Column(
//                                        crossAxisAlignment:
//                                        CrossAxisAlignment.start,
//                                        children: [
//                                          Container(
//                                            margin: EdgeInsets.only(left: 17),
//                                            child: Text(
//                                              "2100",
//                                              maxLines: 1,
//                                              style: TextStyle(
//                                                color: Color.fromARGB(
//                                                    255, 255, 255, 255),
//                                                fontSize: 20,
//                                                fontFamily: "IBM Plex Sans",
//                                                fontWeight: FontWeight.w700,
//                                              ),
//                                              textAlign: TextAlign.center,
//                                            ),
//                                          ),
//                                          Container(
//                                            margin: EdgeInsets.only(top: 5),
//                                            child: Text(
//                                              "RUNS SCORED",
//                                              maxLines: 1,
//                                              style: TextStyle(
//                                                color: Color.fromARGB(
//                                                    255, 255, 255, 255),
//                                                fontSize: 12,
//                                                fontFamily: "IBM Plex Sans",
//                                                fontWeight: FontWeight.w500,
//                                              ),
//                                              textAlign: TextAlign.center,
//                                            ),
//                                          ),
//                                        ],
//                                      ),
//                                    ),
//                                  ),
//                                ],
//                              ),
//                            ),
//                          ),
//                          Positioned(
//                            child: Opacity(
//                              opacity: 0.2,
//                              child: Container(
//                                width: 1,
//                                height: 48,
//                                decoration: BoxDecoration(
//                                  color: Color.fromARGB(255, 242, 244, 244),
//                                ),
//                                child: Container(),
//                              ),
//                            ),
//                          ),
//                        ],
//                      ),
//                    ),
//                  ),
//                  Positioned(
//                    left: 0,
//                    top: 3,
//                    right: 0,
//                    child: Container(
//                      height: 59,
//                      child: Column(
//                        crossAxisAlignment: CrossAxisAlignment.stretch,
//                        children: [
//                          Container(
//                            height: 24,
//                            decoration: BoxDecoration(
//                              color: Color.fromARGB(43, 0, 0, 0),
//                            ),
//                            child: Container(),
//                          ),
//                        ],
//                      ),
//                    ),
//                  ),
//                ],
//              ),
//            ),
//            Align(
//              alignment: Alignment.center,
//              child: Container(
//                width: 320,
//                height: 80,
//                margin: EdgeInsets.only(top: 15),
//                decoration: BoxDecoration(
//                  border: Border.all(
//                    color: Color.fromARGB(255, 242, 244, 244),
//                    width: 1,
//                  ),
//                  borderRadius: BorderRadius.all(Radius.circular(5)),
//                ),
//                child: Stack(
//                  alignment: Alignment.topCenter,
//                  children: [
//                    Positioned(
//                      left: 10,
//                      top: 5,
//                      right: 10,
//                      child: Container(
//                        height: 64,
//                        child: Row(
//                          crossAxisAlignment: CrossAxisAlignment.stretch,
//                          children: [
//                            Align(
//                              alignment: Alignment.topCenter,
//                              child: Container(
//                                width: 73,
//                                // height: 54,
//                                child: Column(
//                                  crossAxisAlignment: CrossAxisAlignment.center,
//                                  children: [
//                                    Container(
//                                      margin:
//                                      EdgeInsets.only(left: 5, right: 5),
//                                      child: Text(
//                                        "88",
//                                        style: TextStyle(
//                                          color: Color.fromARGB(255, 0, 3, 44),
//                                          fontSize: 20,
//                                          fontFamily: "IBM Plex Sans",
//                                          fontWeight: FontWeight.w500,
//                                        ),
//                                        textAlign: TextAlign.left,
//                                      ),
//                                    ),
//                                    Align(
//                                      alignment: Alignment.topCenter,
//                                      child: Container(
//                                        margin: EdgeInsets.only(top: 4),
//                                        child: Text(
//                                          "BEST TAKE \nHOME SCORE",
//                                          maxLines: 2,
//                                          style: TextStyle(
//                                            color:
//                                            Color.fromARGB(255, 87, 93, 96),
//                                            fontSize: 10,
//                                            fontFamily: "IBM Plex Sans",
//                                            fontWeight: FontWeight.w500,
//                                          ),
//                                          textAlign: TextAlign.center,
//                                        ),
//                                      ),
//                                    ),
//                                  ],
//                                ),
//                              ),
//                            ),
//                            Align(
//                              alignment: Alignment.centerLeft,
//                              child: Container(
//                                width: 1,
//                                height: 47,
//                                margin: EdgeInsets.only(left: 21),
//                                decoration: BoxDecoration(
//                                  color: Color.fromARGB(255, 242, 244, 244),
//                                ),
//                                child: Container(),
//                              ),
//                            ),
//                            Spacer(),
//                            Align(
//                              alignment: Alignment.centerLeft,
//                              child: Container(
//                                width: 1,
//                                height: 47,
//                                margin: EdgeInsets.only(right: 20),
//                                decoration: BoxDecoration(
//                                  color: Color.fromARGB(255, 242, 244, 244),
//                                ),
//                                child: Container(),
//                              ),
//                            ),
//                            Align(
//                              alignment: Alignment.topCenter,
//                              child: Container(
//                                width: 73,
//                                height: 41,
//                                child: Column(
//                                  crossAxisAlignment:
//                                  CrossAxisAlignment.stretch,
//                                  children: [
//                                    Container(
//                                      margin:
//                                      EdgeInsets.only(left: 5, right: 5),
//                                      child: Text(
//                                        "88",
//                                        style: TextStyle(
//                                          color: Color.fromARGB(255, 0, 3, 44),
//                                          fontSize: 20,
//                                          fontFamily: "IBM Plex Sans",
//                                          fontWeight: FontWeight.w500,
//                                        ),
//                                        textAlign: TextAlign.center,
//                                      ),
//                                    ),
//                                    Container(
//                                      margin: EdgeInsets.only(top: 4),
//                                      child: Text(
//                                        "STRIKE RATE",
//                                        style: TextStyle(
//                                          color:
//                                          Color.fromARGB(255, 87, 93, 96),
//                                          fontSize: 10,
//                                          fontFamily: "IBM Plex Sans",
//                                          fontWeight: FontWeight.w500,
//                                        ),
//                                        textAlign: TextAlign.center,
//                                      ),
//                                    ),
//                                  ],
//                                ),
//                              ),
//                            ),
//                          ],
//                        ),
//                      ),
//                    ),
//                    Positioned(
//                      top: 5,
//                      child: Container(
//                        width: 88,
//                        height: 64,
//                        child: Column(
//                          children: [
//                            Text(
//                              "88",
//                              style: TextStyle(
//                                color: Color.fromARGB(255, 0, 3, 44),
//                                fontSize: 20,
//                                fontFamily: "IBM Plex Sans",
//                                fontWeight: FontWeight.w500,
//                              ),
//                              textAlign: TextAlign.left,
//                            ),
//                            Container(
//                              margin: EdgeInsets.only(top: 4),
//                              child: Text(
//                                "BEST PROCTORED \nSCORE",
//                                style: TextStyle(
//                                  color: Color.fromARGB(255, 87, 93, 96),
//                                  fontSize: 10,
//                                  fontFamily: "IBM Plex Sans",
//                                  fontWeight: FontWeight.w500,
//                                ),
//                                textAlign: TextAlign.center,
//                              ),
//                            ),
//                          ],
//                        ),
//                      ),
//                    ),
//                  ],
//                ),
//              ),
//            ),
//            Container(
//              height: 204,
//              margin: EdgeInsets.only(left: 16, top: 10, right: 24),
//              child: Column(
//                crossAxisAlignment: CrossAxisAlignment.stretch,
//                children: [
//                  Container(
//                    height: 45,
//                    child: Stack(
//                      alignment: Alignment.center,
//                      children: [
//                        Positioned(
//                          left: 20,
//                          top: 14,
//                          right: 20,
//                          child: Container(
//                            height: 18,
//                            child: Row(
//                              crossAxisAlignment: CrossAxisAlignment.stretch,
//                              children: [
//                                Align(
//                                  alignment: Alignment.topLeft,
//                                  child: Text(
//                                    "50’s",
//                                    style: TextStyle(
//                                      color: Color.fromARGB(255, 0, 3, 44),
//                                      fontSize: 14,
//                                      fontFamily: "IBM Plex Sans",
//                                    ),
//                                    textAlign: TextAlign.left,
//                                  ),
//                                ),
//                                Spacer(),
//                                Align(
//                                  alignment: Alignment.topLeft,
//                                  child: Text(
//                                    "25",
//                                    style: TextStyle(
//                                      color: Color.fromARGB(255, 0, 3, 44),
//                                      fontSize: 14,
//                                      fontFamily: "IBM Plex Sans",
//                                      fontWeight: FontWeight.w500,
//                                    ),
//                                    textAlign: TextAlign.left,
//                                  ),
//                                ),
//                              ],
//                            ),
//                          ),
//                        ),
//                        Positioned(
//                          left: 0,
//                          top: 0,
//                          right: 0,
//                          child: Opacity(
//                            opacity: 0.05,
//                            child: Container(
//                              height: 45,
//                              decoration: BoxDecoration(
//                                gradient: LinearGradient(
//                                  begin: Alignment(-0.01, 0.505),
//                                  end: Alignment(1.03, 0.485),
//                                  stops: [
//                                    0,
//                                    1,
//                                  ],
//                                  colors: [
//                                    Color.fromARGB(255, 168, 174, 35),
//                                    Color.fromARGB(255, 201, 110, 216),
//                                  ],
//                                ),
//                                borderRadius:
//                                BorderRadius.all(Radius.circular(5)),
//                              ),
//                              child: Container(),
//                            ),
//                          ),
//                        ),
//                      ],
//                    ),
//                  ),
//                  Container(
//                    height: 45,
//                    margin: EdgeInsets.only(top: 8),
//                    child: Stack(
//                      alignment: Alignment.center,
//                      children: [
//                        Positioned(
//                          left: 20,
//                          top: 14,
//                          right: 20,
//                          child: Container(
//                            height: 18,
//                            child: Row(
//                              crossAxisAlignment: CrossAxisAlignment.stretch,
//                              children: [
//                                Align(
//                                  alignment: Alignment.topLeft,
//                                  child: Text(
//                                    "100’s",
//                                    style: TextStyle(
//                                      color: Color.fromARGB(255, 0, 3, 44),
//                                      fontSize: 14,
//                                      fontFamily: "IBM Plex Sans",
//                                    ),
//                                    textAlign: TextAlign.left,
//                                  ),
//                                ),
//                                Spacer(),
//                                Align(
//                                  alignment: Alignment.topLeft,
//                                  child: Text(
//                                    "25",
//                                    style: TextStyle(
//                                      color: Color.fromARGB(255, 0, 3, 44),
//                                      fontSize: 14,
//                                      fontFamily: "IBM Plex Sans",
//                                      fontWeight: FontWeight.w500,
//                                    ),
//                                    textAlign: TextAlign.left,
//                                  ),
//                                ),
//                              ],
//                            ),
//                          ),
//                        ),
//                        Positioned(
//                          left: 0,
//                          top: 0,
//                          right: 0,
//                          child: Opacity(
//                            opacity: 0.05,
//                            child: Container(
//                              height: 45,
//                              decoration: BoxDecoration(
//                                gradient: LinearGradient(
//                                  begin: Alignment(-0.01, 0.505),
//                                  end: Alignment(1.03, 0.485),
//                                  stops: [
//                                    0,
//                                    1,
//                                  ],
//                                  colors: [
//                                    Color.fromARGB(255, 168, 174, 35),
//                                    Color.fromARGB(255, 201, 110, 216),
//                                  ],
//                                ),
//                                borderRadius:
//                                BorderRadius.all(Radius.circular(5)),
//                              ),
//                              child: Container(),
//                            ),
//                          ),
//                        ),
//                      ],
//                    ),
//                  ),
//                  Container(
//                    height: 45,
//                    margin: EdgeInsets.only(top: 8),
//                    child: Stack(
//                      alignment: Alignment.center,
//                      children: [
//                        Positioned(
//                          left: 20,
//                          top: 14,
//                          right: 20,
//                          child: Container(
//                            height: 18,
//                            child: Row(
//                              crossAxisAlignment: CrossAxisAlignment.stretch,
//                              children: [
//                                Align(
//                                  alignment: Alignment.topLeft,
//                                  child: Text(
//                                    "Blazing Streaks",
//                                    style: TextStyle(
//                                      color: Color.fromARGB(255, 0, 3, 44),
//                                      fontSize: 14,
//                                      fontFamily: "IBM Plex Sans",
//                                    ),
//                                    textAlign: TextAlign.left,
//                                  ),
//                                ),
//                                Spacer(),
//                                Align(
//                                  alignment: Alignment.topLeft,
//                                  child: Text(
//                                    "25",
//                                    style: TextStyle(
//                                      color: Color.fromARGB(255, 0, 3, 44),
//                                      fontSize: 14,
//                                      fontFamily: "IBM Plex Sans",
//                                      fontWeight: FontWeight.w500,
//                                    ),
//                                    textAlign: TextAlign.left,
//                                  ),
//                                ),
//                              ],
//                            ),
//                          ),
//                        ),
//                        Positioned(
//                          left: 0,
//                          top: 0,
//                          right: 0,
//                          child: Opacity(
//                            opacity: 0.05,
//                            child: Container(
//                              height: 45,
//                              decoration: BoxDecoration(
//                                gradient: LinearGradient(
//                                  begin: Alignment(-0.01, 0.505),
//                                  end: Alignment(1.03, 0.485),
//                                  stops: [
//                                    0,
//                                    1,
//                                  ],
//                                  colors: [
//                                    Color.fromARGB(255, 168, 174, 35),
//                                    Color.fromARGB(255, 201, 110, 216),
//                                  ],
//                                ),
//                                borderRadius:
//                                BorderRadius.all(Radius.circular(5)),
//                              ),
//                              child: Container(),
//                            ),
//                          ),
//                        ),
//                      ],
//                    ),
//                  ),
//                  Spacer(),
//                  Container(
//                    height: 45,
//                    child: Stack(
//                      alignment: Alignment.center,
//                      children: [
//                        Positioned(
//                          left: 20,
//                          right: 20,
//                          bottom: 13,
//                          child: Container(
//                            height: 18,
//                            child: Row(
//                              crossAxisAlignment: CrossAxisAlignment.stretch,
//                              children: [
//                                Align(
//                                  alignment: Alignment.bottomLeft,
//                                  child: Text(
//                                    "Hot Streak",
//                                    style: TextStyle(
//                                      color: Color.fromARGB(255, 0, 3, 44),
//                                      fontSize: 14,
//                                      fontFamily: "IBM Plex Sans",
//                                    ),
//                                    textAlign: TextAlign.left,
//                                  ),
//                                ),
//                                Spacer(),
//                                Align(
//                                  alignment: Alignment.bottomLeft,
//                                  child: Text(
//                                    "25",
//                                    style: TextStyle(
//                                      color: Color.fromARGB(255, 0, 3, 44),
//                                      fontSize: 14,
//                                      fontFamily: "IBM Plex Sans",
//                                      fontWeight: FontWeight.w500,
//                                    ),
//                                    textAlign: TextAlign.left,
//                                  ),
//                                ),
//                              ],
//                            ),
//                          ),
//                        ),
//                        Positioned(
//                          left: 0,
//                          right: 0,
//                          bottom: 0,
//                          child: Opacity(
//                            opacity: 0.05,
//                            child: Container(
//                              height: 45,
//                              decoration: BoxDecoration(
//                                gradient: LinearGradient(
//                                  begin: Alignment(-0.01, 0.505),
//                                  end: Alignment(1.03, 0.485),
//                                  stops: [
//                                    0,
//                                    1,
//                                  ],
//                                  colors: [
//                                    Color.fromARGB(255, 168, 174, 35),
//                                    Color.fromARGB(255, 201, 110, 216),
//                                  ],
//                                ),
//                                borderRadius:
//                                BorderRadius.all(Radius.circular(5)),
//                              ),
//                              child: Container(),
//                            ),
//                          ),
//                        ),
//                      ],
//                    ),
//                  ),
//                ],
//              ),
//            ),
//            Container(
//              height: 79,
//              margin: EdgeInsets.only(left: 17, top: 40, right: 23),
//              child: Row(
//                children: [
//                  Container(
//                    width: 190,
//                    height: 78,
//                    child: Column(
//                      crossAxisAlignment: CrossAxisAlignment.start,
//                      children: [
//                        Text(
//                          "Next Milestone in",
//                          style: TextStyle(
//                            color: Color.fromARGB(255, 31, 37, 43),
//                            fontSize: 18,
//                            fontFamily: "IBM Plex Sans",
//                            fontWeight: FontWeight.w700,
//                          ),
//                          textAlign: TextAlign.left,
//                        ),
//                        Container(
//                          width: 87,
//                          height: 27,
//                          margin: EdgeInsets.only(top: 15),
//                          child: Row(
//                            crossAxisAlignment: CrossAxisAlignment.stretch,
//                            children: [
//                              Align(
//                                alignment: Alignment.topLeft,
//                                child: Text(
//                                  "5000",
//                                  style: TextStyle(
//                                    color: Color.fromARGB(255, 86, 72, 235),
//                                    fontSize: 18,
//                                    fontFamily: "IBM Plex Sans",
//                                    fontWeight: FontWeight.w700,
//                                  ),
//                                  textAlign: TextAlign.left,
//                                ),
//                              ),
//                              Align(
//                                alignment: Alignment.topLeft,
//                                child: Container(
//                                  margin: EdgeInsets.only(left: 5, top: 3),
//                                  child: Text(
//                                    "runs",
//                                    style: TextStyle(
//                                      color: Color.fromARGB(255, 0, 3, 44),
//                                      fontSize: 14,
//                                      fontFamily: "IBM Plex Sans",
//                                    ),
//                                    textAlign: TextAlign.left,
//                                  ),
//                                ),
//                              ),
//                            ],
//                          ),
//                        ),
//                        Container(
//                          width: 181,
//                          height: 4,
//                          margin: EdgeInsets.only(top: 7),
//                          child: Stack(
//                            alignment: Alignment.center,
//                            children: [
//                              Positioned(
//                                left: 1,
//                                top: 0,
//                                child: Opacity(
//                                  opacity: 0.2,
//                                  child: Container(
//                                    width: 180,
//                                    height: 4,
//                                    decoration: BoxDecoration(
//                                      color: Color.fromARGB(255, 255, 152, 42),
//                                      borderRadius:
//                                      BorderRadius.all(Radius.circular(2)),
//                                    ),
//                                    child: Container(),
//                                  ),
//                                ),
//                              ),
//                              Positioned(
//                                left: 0,
//                                top: 0,
//                                child: Container(
//                                  width: 32,
//                                  height: 4,
//                                  decoration: BoxDecoration(
//                                    color: Color.fromARGB(255, 255, 152, 42),
//                                    borderRadius:
//                                    BorderRadius.all(Radius.circular(2)),
//                                  ),
//                                  child: Container(),
//                                ),
//                              ),
//                            ],
//                          ),
//                        ),
//                      ],
//                    ),
//                  ),
//                  Spacer(),
//                  Align(
//                    alignment: Alignment.topLeft,
//                    child: Container(
//                      width: 78,
//                      height: 75,
//                      margin: EdgeInsets.only(top: 4),
//                      child: Image.asset('assets/images/3_k_m.png'),
//                    ),
//                  ),
//                ],
//              ),
//            ),
//            Container(
//              height: 210,
//              margin: EdgeInsets.only(left: 17, top: 40, right: 23),
//              child: Column(
//                crossAxisAlignment: CrossAxisAlignment.start,
//                children: [
//                  Text(
//                    "Your badges",
//                    style: TextStyle(
//                      color: Color.fromARGB(255, 31, 37, 43),
//                      fontSize: 18,
//                      fontFamily: "IBM Plex Sans",
//                      fontWeight: FontWeight.w700,
//                    ),
//                    textAlign: TextAlign.left,
//                  ),
//                  Container(
//                    width: 300,
//                    height: 153,
//                    margin: EdgeInsets.only(left: 10, top: 32),
//                    child: Image.asset(
//                      "assets/images/group-9-copy.png",
//                      fit: BoxFit.cover,
//                    ),
//                  ),
//                ],
//              ),
//            ),
//            Container(
//              height: 123,
//              margin: EdgeInsets.only(left: 16, top: 40, right: 24),
//              child: Column(
//                crossAxisAlignment: CrossAxisAlignment.stretch,
//                children: [
//                  Align(
//                    alignment: Alignment.topLeft,
//                    child: Text(
//                      "Playoff Seasons",
//                      style: TextStyle(
//                        color: Color.fromARGB(255, 31, 37, 43),
//                        fontSize: 18,
//                        fontFamily: "IBM Plex Sans",
//                        fontWeight: FontWeight.w700,
//                      ),
//                      textAlign: TextAlign.left,
//                    ),
//                  ),
//                  Container(
//                    height: 23,
//                    margin: EdgeInsets.only(top: 20, right: 1),
//                    child: Row(
//                      crossAxisAlignment: CrossAxisAlignment.stretch,
//                      children: [
//                        Align(
//                          alignment: Alignment.topLeft,
//                          child: Text(
//                            "Old IMS Institutes",
//                            style: TextStyle(
//                              color: Color.fromARGB(255, 126, 145, 154),
//                              fontSize: 14,
//                              fontFamily: "IBM Plex Sans",
//                            ),
//                            textAlign: TextAlign.left,
//                          ),
//                        ),
//                        Spacer(),
//                        Align(
//                          alignment: Alignment.topLeft,
//                          child: Text(
//                            "3 calls",
//                            style: TextStyle(
//                              color: Color.fromARGB(255, 0, 3, 44),
//                              fontSize: 14,
//                              fontFamily: "IBM Plex Sans",
//                            ),
//                            textAlign: TextAlign.left,
//                          ),
//                        ),
//                      ],
//                    ),
//                  ),
//                  Container(
//                    height: 1,
//                    margin: EdgeInsets.only(top: 11),
//                    decoration: BoxDecoration(
//                      color: Color.fromARGB(255, 242, 244, 244),
//                    ),
//                    child: Container(),
//                  ),
//                  Container(
//                    height: 23,
//                    margin: EdgeInsets.only(top: 8, right: 1),
//                    child: Row(
//                      crossAxisAlignment: CrossAxisAlignment.stretch,
//                      children: [
//                        Align(
//                          alignment: Alignment.topLeft,
//                          child: Text(
//                            "Top 30 Institutes",
//                            style: TextStyle(
//                              color: Color.fromARGB(255, 126, 145, 154),
//                              fontSize: 14,
//                              fontFamily: "IBM Plex Sans",
//                            ),
//                            textAlign: TextAlign.left,
//                          ),
//                        ),
//                        Spacer(),
//                        Align(
//                          alignment: Alignment.topLeft,
//                          child: Container(
//                            margin: EdgeInsets.only(top: 5),
//                            child: Text(
//                              "12 calls",
//                              style: TextStyle(
//                                color: Color.fromARGB(255, 0, 3, 44),
//                                fontSize: 14,
//                                fontFamily: "IBM Plex Sans",
//                              ),
//                              textAlign: TextAlign.left,
//                            ),
//                          ),
//                        ),
//                      ],
//                    ),
//                  ),
//                  Spacer(),
//                  Container(
//                    height: 1,
//                    decoration: BoxDecoration(
//                      color: Color.fromARGB(255, 242, 244, 244),
//                    ),
//                    child: Container(),
//                  ),
//                ],
//              ),
//            ),
//            Spacer(),
//            Container(
//              height: 40,
//              margin: EdgeInsets.symmetric(horizontal: 45),
//              child: FlatButton(
//                onPressed: () => this.onGroup3Pressed(context),
//                color: Colors.transparent,
//                shape: RoundedRectangleBorder(
//                  borderRadius: BorderRadius.all(Radius.circular(2)),
//                ),
//                textColor: Color.fromARGB(255, 255, 255, 255),
//                padding: EdgeInsets.all(0),
//                child: Text(
//                  "UPDATE GDPI",
//                  style: TextStyle(
//                    fontSize: 14,
//                    fontFamily: "IBM Plex Sans",
//                    fontWeight: FontWeight.w500,
//                  ),
//                  textAlign: TextAlign.center,
//                ),
//              ),
//            ),
//          ],
//        ),
//      ),
//    );
//  }
//}
//
//void CalculateScreen(BuildContext context) {
//  var _mediaQueryData = MediaQuery.of(context);
//  screenWidthTotal = _mediaQueryData.size.width;
//  screenHeightTotal = _mediaQueryData.size.height;
//  _safeAreaHorizontal =
//      _mediaQueryData.padding.left + _mediaQueryData.padding.right;
//  _safeAreaVertical =
//      _mediaQueryData.padding.top + _mediaQueryData.padding.bottom;
//
//  screenHeight = screenHeightTotal - _safeAreaVertical;
//  screenWidth = screenWidthTotal - _safeAreaHorizontal;
//}

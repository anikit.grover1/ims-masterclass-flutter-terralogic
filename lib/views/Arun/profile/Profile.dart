import 'package:flutter/material.dart';
import 'package:imsindia/components/navigation_bar.dart';
import 'package:imsindia/views/Arun/profile/ProfileDetail.dart';
import 'package:imsindia/views/GDPI/GDPI_home_scr.dart';

final actualHeight = 720;
final actualWidth = 360;
var screenWidthTotal;
var screenHeightTotal;
var _safeAreaHorizontal;
var _safeAreaVertical;
var screenHeight;
var screenWidth;
var monVal = true;
final defaultScreenHeight = 720;
final defaultScreenWidth = 360;
final List<String> _dates = [
  "06-02-2019",
  "07-02-2019",
  "08-02-2019",
  "09-02-2019",
  "09-02-2019",
  "09-02-2019",
];

class Profile extends StatefulWidget {
  @override
  _Profile2State createState() => _Profile2State();
}

class _Profile2State extends State<Profile> {
//  void onBtnSecondaryPressed(BuildContext context) => Navigator.push(
//      context, MaterialPageRoute(builder: (context) => ProfileDetail()));

  void onGroup3Pressed(BuildContext context) {}

  final _scaffoldKey = GlobalKey<ScaffoldState>();
  @override
  Widget build(BuildContext context) {
    CalculateScreen(context);

    return Scaffold(
      key: _scaffoldKey,
      //  appBar: AppBar(),
///      drawer: NavigationDrawer(),
      body: Container(
          constraints: BoxConstraints.expand(),
          decoration: BoxDecoration(
            color: Color.fromARGB(255, 255, 255, 255),
          ),
          child: SingleChildScrollView(
            child: Container(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Container(
                    height: 286,
                    child: Stack(
                      alignment: Alignment.center,
                      children: [
                        Positioned(
                          left: 0,
                          top: 0,
                          right: 0,
                          child: Container(
                            height: 286,
                            child: GestureDetector(
                              onTap: () {
                                NavigationToolbar();
                                debugPrint('Print Arun');
                              },
                              child: Image.asset(
                                "assets/images/profilemask-2.png",
                                fit: BoxFit.cover,
                              ),
                            ),
                          ),
                        ),
                        Positioned(
                          left: 0,
                          top: 42,
                          right: 0,
                          child: Container(
                            height: 244,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.stretch,
                              children: [
                                InkWell(
                                  child: Align(
                                    alignment: Alignment.topLeft,
                                    child: Container(
                                      width: 87,
                                      height: 20,
                                      margin: EdgeInsets.only(left: 16),
                                      child: Row(
                                        children: [
                                          Expanded(
                                            flex: 1,
                                            child: Container(
                                              height: 14,
                                              margin:
                                                  EdgeInsets.only(right: 20),
                                              child: Image.asset(
                                                "assets/images/hamburger.png",
                                                fit: BoxFit.none,
                                                color: Colors.white,
                                              ),
                                            ),
                                          ),
                                          Container(
                                            margin: EdgeInsets.only(right: 1),
                                            child: Text(
                                              "Profile",
                                              style: TextStyle(
                                                color: Color.fromARGB(
                                                    255, 255, 255, 255),
                                                fontSize: 16,
                                                fontFamily: "IBMPlexSans",
                                                fontStyle: FontStyle.normal,
                                                fontWeight: FontWeight.w500,
                                              ),
                                              textAlign: TextAlign.left,
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                  onTap: () {
                                    _scaffoldKey.currentState.openDrawer();
                                  },
                                ),
                                Align(
                                  alignment: Alignment.topLeft,
                                  child: Container(
                                    width: 256,
                                    height: 95,
                                    margin: EdgeInsets.only(left: 20, top: 28),
                                    child: Row(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.stretch,
                                      children: [
                                        Align(
                                          alignment: Alignment.topLeft,
                                          child: Container(
                                            width: 84,
                                            height: 84,
                                            child: Image.asset(
                                              "assets/images/group-10.png",
                                              fit: BoxFit.fill,
                                            ),
                                          ),
                                        ),
                                        Align(
                                          alignment: Alignment.topLeft,
                                          child: Container(
                                            width: 147,
                                            height: 93,
                                            margin: EdgeInsets.only(
                                                left: 25, top: 2),
                                            child: Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.stretch,
                                              children: [
                                                Align(
                                                  alignment: Alignment.topLeft,
                                                  child: Text(
                                                    "Arun Chaudhary",
                                                    maxLines: 1,
                                                    style: TextStyle(
                                                      color: Color.fromARGB(
                                                          255, 255, 255, 255),
                                                      fontSize: 18,
                                                      fontFamily: "IBMPlexSans",
                                                      fontStyle:
                                                          FontStyle.normal,
                                                      fontWeight:
                                                          FontWeight.w700,
                                                    ),
                                                    textAlign: TextAlign.left,
                                                  ),
                                                ),
                                                Align(
                                                  alignment: Alignment.topLeft,
                                                  child: Container(
                                                    margin:
                                                        EdgeInsets.only(top: 3),
                                                    child: Text(
                                                      "Central Delhi: Delhi",
                                                      maxLines: 1,
                                                      style: TextStyle(
                                                        color: Color.fromARGB(
                                                            255, 255, 255, 255),
                                                        fontSize: 14,
                                                        fontFamily:
                                                            "IBMPlexSans",
                                                        fontStyle:
                                                            FontStyle.normal,
                                                      ),
                                                      textAlign: TextAlign.left,
                                                    ),
                                                  ),
                                                ),
                                                Align(
                                                  alignment: Alignment.topLeft,
                                                  child: Container(
                                                    width: 100,
                                                    height: 40,
                                                    margin:
                                                        EdgeInsets.only(top: 8),
                                                    child: FlatButton(
                                                      onPressed: () =>
                                                          Navigator.push(
                                                              context,
                                                              MaterialPageRoute(
                                                                builder:
                                                                    (context) =>
                                                                        ProfileDetail(),
                                                              )),
                                                      color: Color.fromARGB(
                                                          255, 0, 171, 251),
                                                      shape:
                                                          RoundedRectangleBorder(
                                                        borderRadius:
                                                            BorderRadius.all(
                                                                Radius.circular(
                                                                    2)),
                                                      ),
                                                      textColor: Color.fromARGB(
                                                          255, 255, 255, 255),
                                                      padding:
                                                          EdgeInsets.all(0),
                                                      child: Text(
                                                        "Your Details",
                                                        style: TextStyle(
                                                          fontSize: 14,
                                                          fontFamily:
                                                              "IBMPlexSans",
                                                          fontStyle:
                                                              FontStyle.normal,
                                                          fontWeight:
                                                              FontWeight.w500,
                                                        ),
                                                        textAlign:
                                                            TextAlign.center,
                                                      ),
                                                    ),
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                                Container(
                                  height: 80,
                                  margin: EdgeInsets.only(top: 21),
                                  child: Opacity(
                                    opacity: 0.2,
                                    child: Image.asset(
                                      "assets/images/mask-copy-2.png",
                                      fit: BoxFit.cover,
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                        Positioned(
                          left: 75,
                          top: 148,
                          child: Container(
                            width: 25,
                            height: 25,
                            child: Image.asset(
                              "assets/images/national-overall-leaderboard-topper-copy.png",
                              fit: BoxFit.none,
                            ),
                          ),
                        ),
                        Positioned(
                          top: 206,
                          child: Container(
                            width: 260,
                            height: 80,
                            child: Stack(
                              alignment: Alignment.center,
                              children: [
                                Positioned(
                                  left: 0,
                                  top: 18,
                                  right: 0,
                                  child: Container(
                                    height: 44,
                                    child: Row(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.stretch,
                                      children: [
                                        Align(
                                          alignment: Alignment.topLeft,
                                          child: Container(
                                            // width: 85,
                                            height: 44,
                                            child: Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: [
                                                Container(
                                                  margin:
                                                      EdgeInsets.only(left: 20),
                                                  child: Text(
                                                    "15",
                                                    maxLines: 1,
                                                    style: TextStyle(
                                                      color: Color.fromARGB(
                                                          255, 255, 255, 255),
                                                      fontSize: 18,
                                                      fontFamily: "IBMPlexSans",
                                                      fontStyle:
                                                          FontStyle.normal,
                                                      fontWeight:
                                                          FontWeight.w700,
                                                    ),
                                                    textAlign: TextAlign.center,
                                                  ),
                                                ),
                                                Container(
                                                  margin:
                                                      EdgeInsets.only(top: 3),
                                                  child: Text(
                                                    "LEADERBOARD",
                                                    maxLines: 1,
                                                    style: TextStyle(
                                                      color: Color.fromARGB(
                                                          255, 255, 255, 255),
                                                      fontSize: 12,
                                                      fontFamily: "IBMPlexSans",
                                                      fontStyle:
                                                          FontStyle.normal,
                                                      fontWeight:
                                                          FontWeight.w500,
                                                    ),
                                                    textAlign: TextAlign.center,
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                        ),
                                        Spacer(),
                                        Align(
                                          alignment: Alignment.topLeft,
                                          child: Container(
                                            // width: 82,
                                            height: 44,
                                            child: Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: [
                                                Container(
                                                  margin:
                                                      EdgeInsets.only(left: 17),
                                                  child: Text(
                                                    "2100",
                                                    maxLines: 1,
                                                    style: TextStyle(
                                                      color: Color.fromARGB(
                                                          255, 255, 255, 255),
                                                      fontSize: 18,
                                                      fontFamily: "IBMPlexSans",
                                                      fontStyle:
                                                          FontStyle.normal,
                                                      fontWeight:
                                                          FontWeight.w700,
                                                    ),
                                                    textAlign: TextAlign.center,
                                                  ),
                                                ),
                                                Container(
                                                  margin:
                                                      EdgeInsets.only(top: 3),
                                                  child: Text(
                                                    "RUNS SCORED",
                                                    maxLines: 1,
                                                    style: TextStyle(
                                                      color: Color.fromARGB(
                                                          255, 255, 255, 255),
                                                      fontSize: 12,
                                                      fontFamily: "IBMPlexSans",
                                                      fontStyle:
                                                          FontStyle.normal,
                                                      fontWeight:
                                                          FontWeight.w500,
                                                    ),
                                                    textAlign: TextAlign.center,
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                                Positioned(
                                  child: Opacity(
                                    opacity: 0.2,
                                    child: Container(
                                      width: 1,
                                      height: 48,
                                      decoration: BoxDecoration(
                                        color:
                                            Color.fromARGB(255, 242, 244, 244),
                                      ),
                                      child: Container(),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                        Positioned(
                          left: 0,
                          top: 3,
                          right: 0,
                          child: Container(
                            height: 59,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.stretch,
                              children: [
                                Container(
                                  height: 24,
                                  decoration: BoxDecoration(
                                    color: Color.fromARGB(43, 0, 0, 0),
                                  ),
                                  child: Container(),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Align(
                    alignment: Alignment.center,
                    child: Container(
                      width: 320,
                      height: 80,
                      margin: EdgeInsets.only(top: 15),
                      decoration: BoxDecoration(
                        border: Border.all(
                          color: Color.fromARGB(255, 242, 244, 244),
                          width: 1,
                        ),
                        borderRadius: BorderRadius.all(Radius.circular(5)),
                      ),
                      child: Stack(
                        alignment: Alignment.topCenter,
                        children: [
                          Positioned(
                            left: 10,
                            top: 5,
                            right: 10,
                            child: Container(
                              height: 64,
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.stretch,
                                children: [
                                  Align(
                                    alignment: Alignment.topCenter,
                                    child: Container(
                                      width: 73,
                                      // height: 54,
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.center,
                                        children: [
                                          Container(
                                            margin: EdgeInsets.only(
                                                left: 5, right: 5),
                                            child: Text(
                                              "88",
                                              style: TextStyle(
                                                color: Color.fromARGB(
                                                    255, 0, 3, 44),
                                                fontSize: 20,
                                                fontFamily: "IBMPlexSans",
                                                fontStyle: FontStyle.normal,
                                                fontWeight: FontWeight.w500,
                                              ),
                                              textAlign: TextAlign.left,
                                            ),
                                          ),
                                          Align(
                                            alignment: Alignment.topCenter,
                                            child: Container(
                                              margin: EdgeInsets.only(top: 4),
                                              child: Text(
                                                "BEST TAKE \nHOME SCORE",
                                                maxLines: 2,
                                                style: TextStyle(
                                                  color: Color.fromARGB(
                                                      255, 87, 93, 96),
                                                  fontSize: 10,
                                                  fontFamily: "IBMPlexSans",
                                                  fontStyle: FontStyle.normal,
                                                  fontWeight: FontWeight.w500,
                                                ),
                                                textAlign: TextAlign.center,
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                  Align(
                                    alignment: Alignment.centerLeft,
                                    child: Container(
                                      width: 1,
                                      height: 47,
                                      margin: EdgeInsets.only(left: 21),
                                      decoration: BoxDecoration(
                                        color:
                                            Color.fromARGB(255, 242, 244, 244),
                                      ),
                                      child: Container(),
                                    ),
                                  ),
                                  Spacer(),
                                  Align(
                                    alignment: Alignment.centerLeft,
                                    child: Container(
                                      width: 1,
                                      height: 47,
                                      margin: EdgeInsets.only(right: 20),
                                      decoration: BoxDecoration(
                                        color:
                                            Color.fromARGB(255, 242, 244, 244),
                                      ),
                                      child: Container(),
                                    ),
                                  ),
                                  Align(
                                    alignment: Alignment.topCenter,
                                    child: Container(
                                      width: 73,
//                                      height: 41,
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.stretch,
                                        children: [
                                          Container(
                                            margin: EdgeInsets.only(
                                                left: 5, right: 5),
                                            child: Text(
                                              "88",
                                              style: TextStyle(
                                                color: Color.fromARGB(
                                                    255, 0, 3, 44),
                                                fontSize: 20,
                                                fontFamily: "IBMPlexSans",
                                                fontStyle: FontStyle.normal,
                                                fontWeight: FontWeight.w500,
                                              ),
                                              textAlign: TextAlign.center,
                                            ),
                                          ),
                                          Container(
                                            margin: EdgeInsets.only(top: 4),
                                            child: Text(
                                              "STRIKE RATE \n",
                                              style: TextStyle(
                                                color: Color.fromARGB(
                                                    255, 87, 93, 96),
                                                fontSize: 10,
                                                fontFamily: "IBMPlexSans",
                                                fontStyle: FontStyle.normal,
                                                fontWeight: FontWeight.w500,
                                              ),
                                              textAlign: TextAlign.center,
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                          Positioned(
                            top: 5,
                            child: Container(
                              width: 88,
                              height: 64,
                              child: Column(
                                children: [
                                  Text(
                                    "88",
                                    style: TextStyle(
                                      color: Color.fromARGB(255, 0, 3, 44),
                                      fontSize: 20,
                                      fontFamily: "IBMPlexSans",
                                      fontStyle: FontStyle.normal,
                                      fontWeight: FontWeight.w500,
                                    ),
                                    textAlign: TextAlign.left,
                                  ),
                                  Container(
                                    margin: EdgeInsets.only(top: 4),
                                    child: Text(
                                      "BEST PROCTORED \nSCORE",
                                      style: TextStyle(
                                        color: Color.fromARGB(255, 87, 93, 96),
                                        fontSize: 10,
                                        fontFamily: "IBMPlexSans",
                                        fontStyle: FontStyle.normal,
                                        fontWeight: FontWeight.w500,
                                      ),
                                      textAlign: TextAlign.center,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  Container(
                    height: 204,
                    margin: EdgeInsets.only(left: 16, top: 10, right: 24),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: [
                        Container(
                          height: 45,
                          child: Stack(
                            alignment: Alignment.center,
                            children: [
                              Positioned(
                                left: 20,
                                top: 14,
                                right: 20,
                                child: Container(
                                  height: 18,
                                  child: Row(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.stretch,
                                    children: [
                                      Align(
                                        alignment: Alignment.topLeft,
                                        child: Text(
                                          "50’s",
                                          style: TextStyle(
                                            color:
                                                Color.fromARGB(255, 0, 3, 44),
                                            fontSize: 14,
                                            fontFamily: "IBMPlexSans",
                                            fontStyle: FontStyle.normal,
                                          ),
                                          textAlign: TextAlign.left,
                                        ),
                                      ),
                                      Spacer(),
                                      Align(
                                        alignment: Alignment.topLeft,
                                        child: Text(
                                          "25",
                                          style: TextStyle(
                                            color:
                                                Color.fromARGB(255, 0, 3, 44),
                                            fontSize: 14,
                                            fontFamily: "IBMPlexSans",
                                            fontStyle: FontStyle.normal,
                                            fontWeight: FontWeight.w500,
                                          ),
                                          textAlign: TextAlign.left,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                              Positioned(
                                left: 0,
                                top: 0,
                                right: 0,
                                child: Opacity(
                                  opacity: 0.05,
                                  child: Container(
                                    height: 45,
                                    decoration: BoxDecoration(
                                      gradient: LinearGradient(
                                        begin: Alignment(-0.01, 0.505),
                                        end: Alignment(1.03, 0.485),
                                        stops: [
                                          0,
                                          1,
                                        ],
                                        colors: [
                                          Color.fromARGB(255, 168, 174, 35),
                                          Color.fromARGB(255, 201, 110, 216),
                                        ],
                                      ),
                                      borderRadius:
                                          BorderRadius.all(Radius.circular(5)),
                                    ),
                                    child: Container(),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                        Container(
                          height: 45,
                          margin: EdgeInsets.only(top: 8),
                          child: Stack(
                            alignment: Alignment.center,
                            children: [
                              Positioned(
                                left: 20,
                                top: 14,
                                right: 20,
                                child: Container(
                                  height: 18,
                                  child: Row(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.stretch,
                                    children: [
                                      Align(
                                        alignment: Alignment.topLeft,
                                        child: Text(
                                          "100’s",
                                          style: TextStyle(
                                            color:
                                                Color.fromARGB(255, 0, 3, 44),
                                            fontSize: 14,
                                            fontFamily: "IBMPlexSans",
                                            fontStyle: FontStyle.normal,
                                          ),
                                          textAlign: TextAlign.left,
                                        ),
                                      ),
                                      Spacer(),
                                      Align(
                                        alignment: Alignment.topLeft,
                                        child: Text(
                                          "25",
                                          style: TextStyle(
                                            color:
                                                Color.fromARGB(255, 0, 3, 44),
                                            fontSize: 14,
                                            fontFamily: "IBMPlexSans",
                                            fontStyle: FontStyle.normal,
                                            fontWeight: FontWeight.w500,
                                          ),
                                          textAlign: TextAlign.left,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                              Positioned(
                                left: 0,
                                top: 0,
                                right: 0,
                                child: Opacity(
                                  opacity: 0.05,
                                  child: Container(
                                    height: 45,
                                    decoration: BoxDecoration(
                                      gradient: LinearGradient(
                                        begin: Alignment(-0.01, 0.505),
                                        end: Alignment(1.03, 0.485),
                                        stops: [
                                          0,
                                          1,
                                        ],
                                        colors: [
                                          Color.fromARGB(255, 168, 174, 35),
                                          Color.fromARGB(255, 201, 110, 216),
                                        ],
                                      ),
                                      borderRadius:
                                          BorderRadius.all(Radius.circular(5)),
                                    ),
                                    child: Container(),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                        Container(
                          height: 45,
                          margin: EdgeInsets.only(top: 8),
                          child: Stack(
                            alignment: Alignment.center,
                            children: [
                              Positioned(
                                left: 20,
                                top: 14,
                                right: 20,
                                child: Container(
                                  height: 18,
                                  child: Row(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.stretch,
                                    children: [
                                      Align(
                                        alignment: Alignment.topLeft,
                                        child: Text(
                                          "Blazing Streaks",
                                          style: TextStyle(
                                            color:
                                                Color.fromARGB(255, 0, 3, 44),
                                            fontSize: 14,
                                            fontFamily: "IBMPlexSans",
                                            fontStyle: FontStyle.normal,
                                          ),
                                          textAlign: TextAlign.left,
                                        ),
                                      ),
                                      Spacer(),
                                      Align(
                                        alignment: Alignment.topLeft,
                                        child: Text(
                                          "25",
                                          style: TextStyle(
                                            color:
                                                Color.fromARGB(255, 0, 3, 44),
                                            fontSize: 14,
                                            fontFamily: "IBMPlexSans",
                                            fontStyle: FontStyle.normal,
                                            fontWeight: FontWeight.w500,
                                          ),
                                          textAlign: TextAlign.left,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                              Positioned(
                                left: 0,
                                top: 0,
                                right: 0,
                                child: Opacity(
                                  opacity: 0.05,
                                  child: Container(
                                    height: 45,
                                    decoration: BoxDecoration(
                                      gradient: LinearGradient(
                                        begin: Alignment(-0.01, 0.505),
                                        end: Alignment(1.03, 0.485),
                                        stops: [
                                          0,
                                          1,
                                        ],
                                        colors: [
                                          Color.fromARGB(255, 168, 174, 35),
                                          Color.fromARGB(255, 201, 110, 216),
                                        ],
                                      ),
                                      borderRadius:
                                          BorderRadius.all(Radius.circular(5)),
                                    ),
                                    child: Container(),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                        Spacer(),
                        Container(
                          height: 45,
                          child: Stack(
                            alignment: Alignment.center,
                            children: [
                              Positioned(
                                left: 20,
                                right: 20,
                                bottom: 13,
                                child: Container(
                                  height: 18,
                                  child: Row(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.stretch,
                                    children: [
                                      Align(
                                        alignment: Alignment.bottomLeft,
                                        child: Text(
                                          "Hot Streak",
                                          style: TextStyle(
                                            color:
                                                Color.fromARGB(255, 0, 3, 44),
                                            fontSize: 14,
                                            fontFamily: "IBMPlexSans",
                                            fontStyle: FontStyle.normal,
                                          ),
                                          textAlign: TextAlign.left,
                                        ),
                                      ),
                                      Spacer(),
                                      Align(
                                        alignment: Alignment.bottomLeft,
                                        child: Text(
                                          "25",
                                          style: TextStyle(
                                            color:
                                                Color.fromARGB(255, 0, 3, 44),
                                            fontSize: 14,
                                            fontFamily: "IBMPlexSans",
                                            fontStyle: FontStyle.normal,
                                            fontWeight: FontWeight.w500,
                                          ),
                                          textAlign: TextAlign.left,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                              Positioned(
                                left: 0,
                                right: 0,
                                bottom: 0,
                                child: Opacity(
                                  opacity: 0.05,
                                  child: Container(
                                    height: 45,
                                    decoration: BoxDecoration(
                                      gradient: LinearGradient(
                                        begin: Alignment(-0.01, 0.505),
                                        end: Alignment(1.03, 0.485),
                                        stops: [
                                          0,
                                          1,
                                        ],
                                        colors: [
                                          Color.fromARGB(255, 168, 174, 35),
                                          Color.fromARGB(255, 201, 110, 216),
                                        ],
                                      ),
                                      borderRadius:
                                          BorderRadius.all(Radius.circular(5)),
                                    ),
                                    child: Container(),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    height: 79,
                    margin: EdgeInsets.only(left: 17, top: 40, right: 23),
                    child: Row(
                      children: [
                        Container(
                          width: 190,
                          height: 78,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                "Next Milestone in",
                                style: TextStyle(
                                  color: Color.fromARGB(255, 31, 37, 43),
                                  fontSize: 18,
                                  fontFamily: "IBMPlexSans",
                                  fontStyle: FontStyle.normal,
                                  fontWeight: FontWeight.w700,
                                ),
                                textAlign: TextAlign.left,
                              ),
                              Container(
                                width: 87,
                                height: 27,
                                margin: EdgeInsets.only(top: 15),
                                child: Row(
                                  crossAxisAlignment:
                                      CrossAxisAlignment.stretch,
                                  children: [
                                    Align(
                                      alignment: Alignment.topLeft,
                                      child: Text(
                                        "5000",
                                        style: TextStyle(
                                          color:
                                              Color.fromARGB(255, 86, 72, 235),
                                          fontSize: 18,
                                          fontFamily: "IBMPlexSans",
                                          fontStyle: FontStyle.normal,
                                          fontWeight: FontWeight.w700,
                                        ),
                                        textAlign: TextAlign.left,
                                      ),
                                    ),
                                    Align(
                                      alignment: Alignment.topLeft,
                                      child: Container(
                                        margin:
                                            EdgeInsets.only(left: 5, top: 3),
                                        child: Text(
                                          "runs",
                                          style: TextStyle(
                                            color:
                                                Color.fromARGB(255, 0, 3, 44),
                                            fontSize: 14,
                                            fontFamily: "IBMPlexSans",
                                            fontStyle: FontStyle.normal,
                                          ),
                                          textAlign: TextAlign.left,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              Container(
                                width: 181,
                                height: 4,
                                margin: EdgeInsets.only(top: 7),
                                child: Stack(
                                  alignment: Alignment.center,
                                  children: [
                                    Positioned(
                                      left: 1,
                                      top: 0,
                                      child: Opacity(
                                        opacity: 0.2,
                                        child: Container(
                                          width: 180,
                                          height: 4,
                                          decoration: BoxDecoration(
                                            color: Color.fromARGB(
                                                255, 255, 152, 42),
                                            borderRadius: BorderRadius.all(
                                                Radius.circular(2)),
                                          ),
                                          child: Container(),
                                        ),
                                      ),
                                    ),
                                    Positioned(
                                      left: 0,
                                      top: 0,
                                      child: Container(
                                        width: 32,
                                        height: 4,
                                        decoration: BoxDecoration(
                                          color:
                                              Color.fromARGB(255, 255, 152, 42),
                                          borderRadius: BorderRadius.all(
                                              Radius.circular(2)),
                                        ),
                                        child: Container(),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                        Spacer(),
                        Align(
                          alignment: Alignment.topLeft,
                          child: Container(
                            width: 78,
                            height: 75,
                            margin: EdgeInsets.only(top: 4),
                            child: Image.asset('assets/images/3_k_m.png'),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    height: 250,
                    margin: EdgeInsets.only(left: 17.0, top: 20, right: 30.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          "Your badges",
                          style: TextStyle(
                            color: Color.fromARGB(255, 31, 37, 43),
                            fontSize: 18,
                            fontFamily: "IBMPlexSans",
                            fontStyle: FontStyle.normal,
                            fontWeight: FontWeight.w700,
                          ),
                          textAlign: TextAlign.left,
                        ),
                        Center(
                          child: Container(
                            width: 300,
                            height: 200,
                            // margin: EdgeInsets.all( 30),
                            child: GridView.builder(
                              physics: NeverScrollableScrollPhysics(),
                              gridDelegate:
                                  SliverGridDelegateWithFixedCrossAxisCount(
                                crossAxisCount: 3,
                                crossAxisSpacing: 0.0,
                                mainAxisSpacing: 0.0,
                                childAspectRatio: 1,
                              ),
                              //physics: NeverScrollableScrollPhysics(),
                              padding: const EdgeInsets.all(0.0),
                              addRepaintBoundaries: true,
                              itemCount: _dates.length,
                              itemBuilder: (build, int indexDate) {
                                final item = _dates[indexDate];
                                return Container(
                                    height: 62,
                                    width: 68,
                                    margin:
                                        EdgeInsets.fromLTRB(10, 20, 10, 0.0),
                                    child: Image.network(
                                      "https://previews.123rf.com/images/martialred/martialred1507/martialred150700040/42273469-no-msg-msg-free-restaurant-food-badge-or-sticker-flat-icon.jpg",
//                                child: Image.network("data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAAAwFBMVEX/pQD/////owD/pgD/qAD/oQD/0YX/uTX/79D/tEH/qA//2pn/yXD/+ev/7M3//vv/tDP/u1z/26b/57//9OX/rSH/8df/4rD/v0v/uU7/48H/xFj/zn3/+vD/sSb/153/1Y3/rgz/yIf/v2X/ynX/9eD/uj//w3T/yGT/znP/68j/3aD/xX3/7tX/tTr/y2v/0Yv/tUj/3a3/sh7/uEf/u1P/5K//1on/sDP/wF//2KL/4bf/x4L/wm//qyr/1JJf+DQpAAAO7klEQVR4nO2deZuaOhTG8QCOe6y4jBVc7ojjaHWso45e2/r9v9UFGSCBsGQMDHj7Pu0frRHyM/s5yYlQSFWaMj5r6b5SSPNlSBnKsvymoDRfmiZh/QIgCALIm3aKb02RcFW7AhqIcFql91q+hHpwG0PKzuK7Mu7OwTVV07nmiSuh/ntfLNE/QqOhC2ggdi4BP0a3uH/q8swUT0K0EUH8s19S8o5qFRzQQBTnlGTaYf9HBGHDs7vlSXiegNnIZGmw8GRRf5NJQLO/mXhqI1rMJdlsqiBtOeaKI2F3Bnbm5cl4pTktDSmSLPgFktsYkaZf3mT7Z4Amx56IHyEqYsVkFORpu7JKsj6WvAVo19SPYUNbbU8SXsow5VdP+RGeSQ6QYahuu6jQrYl0QBPRHDa62x9NIGsxiD1u+eJGWG/6OIyC7G9GeyEI0Gy0/dGmL/kaqQBDbkMGN8IpjcPoNwJqqJNCAqB+85VXxngRKhEkrALpwClnnAj1Ml9AA3HNqZ7yIURTznymanz6Uz6EvQ7vIjQndi0uqywuhKs+f0Bzet7gkTkehPUk6qipWp1D7jgQohHnftQWSBcO9ZQDoZJAI/xAnJzTJmw8GhoMBvP5/PXlRX0fL5e9w1tSgCbisrdcjt/Vl5dX453Gm80MLJIjRBdjdWOsHEglB3hdpnheZrx/w1R3mQi1QZI0cQUzpoGSiVCfZIJQYlo9MhH2sgAoCHIrMcJX2lI9fcmDpAi1BHtNFsGEpSGyEJayAWgUIst4wUL4no1KahC+JEQ4y0oZwgPDiMhAqFcyQygxWMUZCA9fDeYKlokQvmalCA3CeRKE9V2GCMvxjTjxCZU/GSLsxLcYxycsil/NhUks8ifUTtkpQqMQT7ENHLEJG9wtorcIjrGnNbEJl5kZDU1BZcSXUCu19l8N5VF/2Yg3/45BWFfGT+VKlvoZU2KlfCoqMVpjFKF+eN39Een+oa+VkSexU35dRo2MYYRaqTirCFmks2XkDSoPaimsvgYQIk3vvb6BnGE6W6bx723e07WA9QadsDEaTCBRMyFfGXmdDEZ0Nwed8DFZK2gSMopyRi1FOmFm1rosggeWMswl4ewv4f+KMBP+CVbBIwthVuyGLPpLeAeEdHcGnXCeR8IAh81fwhyJiTAjjkI2yXQzMZ3wJZeE9A2bdEI1l4R0nxudsHj3hJnxhbJIptvB6YTVXBKOGQgPuSSk75umEy5zSUh3m9II0SIj20rYBG8LmqGGQlhvJbadMllBZ0mxgfsJu9NM+WBYBNLUv+XNS6i1+oGHeLIvEPotr/3bQ6jXclpDbUGnpocR9tYhh5TyIRDWh0DC+ktCW9LTFYivOpUQnZu5M+XTBfKwp/kJ9eJdFKAlkNSuhxCddzlwpMUXwMw+gmsR6pdM7ODmKZA2ukuoP+V4DAwSCKeuQ1hP5GTWVwt+6m4tVbk+2RLPR35OU6wdLgJmMmKU3KQWlChWXJmfB7Je/zves31JY/FBR8H7Uno1FcfFcE2P9vfEyvHn/qmmFi/L5aHX6x2Wy+q4qNa+9Y8dgQIplp9q06mqqoHPdvfRHU+1mpnU0dRSzdLpdKIVEOwQTkg1W4BUiFD9yY6C8NZSaLshkKYvWsVvHS+j8QOjiL3aip0lcaPR0yJbGs3faRs1PgjrtM40mlB3COk+9I+c6Gd1SDLCMPJ86NkhHEXtW0c0QtAJQqrXlxehIW0xJaZMMIzcW3gjoeMRtgnPlGrKkdBceOIBaqCZdBk6dimbUKNMargSkmdNEycEyZ57OzNvipmbM2FBcc0jLIRS5FE1CqE8tT90CFf+voY3YWHs/IoMhPApQnCe7xBqe56ESKNtpENOU0yaEGaO1c0hRK1PENaphJry72izufy7Vboes1DLLsQYhEp8Qv/uGNltu+4afzX0/RA4IeqW/FL6NMLuzpqZSp2dqhA5cfozklCjPLpUpRKa2ViYUkydz72e8efw4J1QTNzHu4T1qbevIQi153XZp3XFTkoQOtwATaIjRDU72wRh4+h/dNmeEJKE7Z3xVkPHq4adTsf8W/HkXMaiMWCWqK23ryEI699k8MtJSiU0P+gQcbvOVMLvtEc7TZYknIQkdYV9BSPseg82eQjD1kOBhMYEGD9Jp3/8il7CsEd7CUPS2l9ZY0/HCNEmCULi9yy0h6kQ/sKaBm4vHUlJEMpFrENNh1C8YN/Aa6l3RORE+IQ5hPS3VMqwjzlosL605n82f8JU2qEAJzdCqjvib3zTNk6EeJzH7wF9KW9Csegf8SnhyPj0pSI2XKBNOoR40D6bUKfY9LkQynvMTYLsyUfShAaiPUjZVv23qBUwdcSPJoQhHiZIp89Lv0PFrwDCt5CkRObt0BIWIaJu1SMJnyodn6IIAY7EtHlOJ9Srfqn0OY229CddUn1K8kcI2CshGlGt+uTMW+n5tHEe7SG0iluG4TMx81YY1odd58eIXlvQa65oxdG6Eiq+ZYWfkKbVhEqo15rr9ax/2rQUYvXUcF8TTVhiIKT7BaFztglXASftIwkbdEKkr7rdru4NDI3Pe9MgFKDcsAjrtYDu6bOEAYl3+KMjCRe3E1oDv1BAl6D+lysh2RKiCRUOhAJskEGoBLqJ+BGi1cBj844k7Dm9kvhvhDURBYfxlc8FQfNaAJIg7E48oxETYZS9VAv278KbJiyC5xMca2nrSGYimnDJhVCQz0LIZlme7fBcjl1LkWaqGofQStoNcSjKRUG5gTBgPKRKWQf3NKtf/3zo1w9Dz8/Pv3+7vwhBqG3spP98JH3+fQoGFOSeEBKijLS1bZ582js/Hmkv7R2UUtv7w7fwLY9+S5QvPKKTliC8WqK8aYMBjdmpUDgH7oT69Mx7L1Y6f467MTHko984IeFdC11b+AjD0nolb43RQivGJGRZPVnzUomclWK9TUqEoGrmnKYdFHnm5vUhAHHbxSltQui3rXlpY03/1u0rYPKI/BJLnwahFcTmurbY0jebcFjjAx7PSXNfkwYhdLbIJkQb6ojCg/CE/T926C8NQtEy1FprfI16LJYH4RG36ffSJIRHbI1Pd+N/mrDkLgShUsU+QCkSgvRhprVtbV3K3O7ThNgMDZ6xD9x4G0yEl08QgmC/wLGXtvx72DmUoQC4MdGdTnsJw1xmXsI47jUQnCNCrs1b5WbzJggJa2LbKcMOQdg5HoeWLBueaSKUbD8RSTg70uQ1KMKU4uX2D/w8CI1WhH2C7KtaoPMdf7blsb7qasRrbbetlipRCJHl3vao58k77Cl+C9OQwsn3RASvg2dsdors47cwwQnpsv0MYnR0fUQe64UyVkNw/2HVU9afJVzgcyTY4UeRvidDqJGExDdwwo0321wIiej/dszzZAlhQyfk5iElCPGtLcZTTnIahPjeAXwvhtcox4cQTrjzqZUGoYB79FxC7UfofhoGQoW43ArfvWPQW9lOmFCe0vbTrHyXbvEhFGT8RpyPOWvChPgWZHfE33Kb03gJB9h4of2SLcLo+KM3EArQ8o/42sk3OSQtUZ8mJG9r2IoJECIf4aN/byLNzS1i7sj3Yyjhm5F0PB4Xi6o6HXjH1b117MD4fPw+sJailX0tSrYlT5wZ//g4h/BxUmHs6mJmbuzdfgniykd4oc3vK/GPcVQiEzGcBolSyMmWD7khJML2eedZ7g0RNmGI+yKfks8ewlxG9wqTE9vMtmLc0RFZSyC2CcJchvoIl1wlCP0b9XMv2OGEpQzdCsBLtqHEIszUrQDcpLqElNMkdyDo1x1C/ZT7cBh+EWe5C917CqhgCSR1RYyH22YWTl9zE8CwRey+NLQKvj47fwLhtWEvuTArxtJ/iX1OBZMqJbaJ2Rrnd1GMIAxwlx4RYwgtj7lvjQDNKmET8MSJKp1yG+jLElROnt1Wvlhf1XWOixFgXfWGbPPFa0OL/IZyAanmD7tHibnXvuS0psLk0vbjUOMmnnM5w4GJEjNuYiGv8UurVJb/a/zSfJYhSwzafMYRfmcgvP9Y0Pcfz/v+Y7Lff1z9+78b4S9hJsVEmEtPFNM9M/kkvP+7gpgIc7l6YiG8/3vX/hJmUkyE93+H5SwPl8iSApCbDPeQLooPUp4uWzVPAj54YsOFExaQtlo+TsLPZ2ZE5pFS6bHaqDPdB2yprrw2RWo4tKzIzJw4nIdezx1GaJZl6XJadzJ7L3flOBiHXlkdTWhC6gd1f5SiX5mypONePeiRW09jEJqQbaXa/2oij3YXJQZeXEJTo0w5M0CiG7hvIVwEHBb+GsGRPjTcQlgPOvH9JYJvYd3n5wiztTNMVGO1QTbCc4buSYJOLzrDzIT1coYI13p0hpkJC/MMEZ5iV1IWwmWGCGOPFUyE3cyMiCCWorP7CUIUHFEqZQF9JXgzYXZ8bgFeptsJS5khjD2hYSTMyskhmMSe0DASZsVhE+CC4UHYy0YZBlway4NwlYlqSp7Y5EtIvXUodWF3V3AnRBcsCIkbLy1JGjyCm/3iIsNoyEhYWDw0Z6YeHx8Hg8H89UV9r44TrLogvVff1ZfXufE2453XdzcfWMYKVsIC7fKsXmKzOZAoi6Sou75uJKQJXRLarAnihpEmIcJC2xeRnxPhibIhllk8CMmANPwA19Fn9mOICyFKwtQIlSqHOsqJsKAlUE/hFGGujyk+hIUud2sqNBlWuWHiRFg4cD7CACLT5DNEvAgDtjMCRJAbn1MTyHMujbDAkVD3x+YHWVj/uIReFg3ly6+14J/4waQb/cp44kZYaHlKC+TJU6uhFRrBR3BBOC0KqNGqTTyM/OooT0IND88PMsxGDWsN0PZH87MBp9aQXm+MZoBDwiufftQUP0IsDBPIUg3zqyNKTEYzFSzdJPXGVHIYY9zDGl8cCa1gYebFHbOlpwhK3qjzJobkmbJoB7MgzUeILV7dTIEvoVY0yqpTfqf4FOoDbysVHymTTn28M+9HVvnVUb6Ehe7pZ416usoMeE64rqBTpFMgZfpzz2mst8SVsNANdgmhLTbtgWMruJh0bgPFVXwJQ9VwYlNA338QMjGlSFhoF6+9JcjT+N6/25UmYUHbTmRZlrY8O5JIpUpojghqi8kWeLv+A3UtZLyx/Wk6AAAAAElFTkSuQmCC",
                                      fit: BoxFit.fill,
                                    ));
                              },
                              //padding: EdgeInsets.all(0.0),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    height: 123,
                    margin: EdgeInsets.only(left: 16, top: 40, right: 24),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: [
                        Align(
                          alignment: Alignment.topLeft,
                          child: Text(
                            "Playoff Seasons",
                            style: TextStyle(
                              color: Color.fromARGB(255, 31, 37, 43),
                              fontSize: 18,
                              fontFamily: "IBMPlexSans",
                              fontStyle: FontStyle.normal,
                              fontWeight: FontWeight.w700,
                            ),
                            textAlign: TextAlign.left,
                          ),
                        ),
                        Container(
                          height: 23,
                          margin: EdgeInsets.only(top: 20, right: 1),
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.stretch,
                            children: [
                              Align(
                                alignment: Alignment.topLeft,
                                child: Text(
                                  "Old IMS Institutes",
                                  style: TextStyle(
                                    color: Color.fromARGB(255, 126, 145, 154),
                                    fontSize: 14,
                                    fontFamily: "IBMPlexSans",
                                    fontStyle: FontStyle.normal,
                                  ),
                                  textAlign: TextAlign.left,
                                ),
                              ),
                              Spacer(),
                              Align(
                                alignment: Alignment.topLeft,
                                child: Text(
                                  "3 calls",
                                  style: TextStyle(
                                    color: Color.fromARGB(255, 0, 3, 44),
                                    fontSize: 14,
                                    fontFamily: "IBMPlexSans",
                                    fontStyle: FontStyle.normal,
                                  ),
                                  textAlign: TextAlign.left,
                                ),
                              ),
                            ],
                          ),
                        ),
                        Container(
                          height: 1,
                          margin: EdgeInsets.only(top: 11),
                          decoration: BoxDecoration(
                            color: Color.fromARGB(255, 242, 244, 244),
                          ),
                          child: Container(),
                        ),
                        Container(
                          height: 23,
                          margin: EdgeInsets.only(top: 8, right: 1),
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.stretch,
                            children: [
                              Align(
                                alignment: Alignment.topLeft,
                                child: Text(
                                  "Top 30 Institutes",
                                  style: TextStyle(
                                    color: Color.fromARGB(255, 126, 145, 154),
                                    fontSize: 14,
                                    fontFamily: "IBMPlexSans",
                                    fontStyle: FontStyle.normal,
                                  ),
                                  textAlign: TextAlign.left,
                                ),
                              ),
                              Spacer(),
                              Align(
                                alignment: Alignment.topLeft,
                                child: Container(
                                  margin: EdgeInsets.only(top: 5),
                                  child: Text(
                                    "12 calls",
                                    style: TextStyle(
                                      color: Color.fromARGB(255, 0, 3, 44),
                                      fontSize: 14,
                                      fontFamily: "IBMPlexSans",
                                      fontStyle: FontStyle.normal,
                                    ),
                                    textAlign: TextAlign.left,
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                        Spacer(),
                        Container(
                          height: 1,
                          decoration: BoxDecoration(
                            color: Color.fromARGB(255, 242, 244, 244),
                          ),
                          child: Container(),
                        ),
                      ],
                    ),
                  ),
//                Spacer(),
//                Container(
//                  height: 40,
//                  margin: EdgeInsets.symmetric(horizontal: 45),
//                  child: FlatButton(
//                    onPressed: () => this.onGroup3Pressed(context),
//                    color: Colors.transparent,
//                    shape: RoundedRectangleBorder(
//                      borderRadius: BorderRadius.all(Radius.circular(2)),
//                    ),
//                    textColor: Color.fromARGB(255, 255, 255, 255),
//                    padding: EdgeInsets.all(0),
//                    child: Text(
//                      "UPDATE GDPI",
//                      style: TextStyle(
//                        fontSize: 14,
//                        fontFamily: "IBMPlexSans",
//                fontStyle: FontStyle.normal,
//                        fontWeight: FontWeight.w500,
//                      ),
//                      textAlign: TextAlign.center,
//                    ),
//                  ),
//                ),
                  GestureDetector(
                    onTap: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => GDPIHomescreen(),
                          ));
                    },
                    child: Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(
                          6.0,
                        ),
                        gradient: LinearGradient(
                          begin: Alignment.topLeft,
                          // end: Alignment(0.8, 0.0), // 10% of the width, so there are ten blinds.
                          colors: [
                            const Color(0xFF3380cc),
                            const Color(0xFF886dd7)
                          ], // whitish to gray
                          tileMode: TileMode
                              .repeated, // repeats the gradient over the canvas
                        ),
                      ),
                      margin: EdgeInsets.only(
                          top: 25, left: 45, right: 45, bottom: 42),
                      width: double.infinity,
                      height: 40.0,
                      child: Center(
                        child: Text(
                          'UPDATE GDPI',
                          style: TextStyle(
                            fontSize: 14,
                            color: Colors.white,
                            fontFamily: "IBMPlexSans",
                            fontStyle: FontStyle.normal,
                            fontWeight: FontWeight.w800,
                          ),
                        ),
                      ),
                    ),
                  )
                ],
              ),
            ),
          )),
    );
  }
}

void CalculateScreen(BuildContext context) {
  var _mediaQueryData = MediaQuery.of(context);
  screenWidthTotal = _mediaQueryData.size.width;
  screenHeightTotal = _mediaQueryData.size.height;
  _safeAreaHorizontal =
      _mediaQueryData.padding.left + _mediaQueryData.padding.right;
  _safeAreaVertical =
      _mediaQueryData.padding.top + _mediaQueryData.padding.bottom;

  screenHeight = screenHeightTotal - _safeAreaVertical;
  screenWidth = screenWidthTotal - _safeAreaHorizontal;
}

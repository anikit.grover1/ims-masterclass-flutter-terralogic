import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:imsindia/utils/emaximiser_svg_icons.dart';

import '../ims_utility.dart';

class AboutIms extends StatefulWidget {
  @override
  _AboutImsState createState() => _AboutImsState();
}

const actualHeight = 622;
const actualWidth = 360;
var screenWidthTotal;
var screenHeightTotal;

var _safeAreaHorizontal;

var _safeAreaVertical;

var screenHeight;

var screenWidth;

class _AboutImsState extends State<AboutIms> {
//  double temp = (80 / actualHeight) * SizeConfig.screenHeight;
//  final String assetName = 'images/facebook.svg';
//  final Widget svgIcon = new SvgPicture.asset(assetName,
//      color: Colors.red, semanticsLabel: 'A red up arrow');
  @override
  Widget build(BuildContext context) {
    CalculateScreen(context);
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.dark.copyWith(
      statusBarColor: Colors.black, //or set color with: Color(0xFF0000FF)
    ));
    return MaterialApp(
      color: Colors.white,
      debugShowCheckedModeBanner: false,
      title: 'Contact us',
      home: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          brightness: Brightness.light,
          elevation: 0.0,
          centerTitle: false,
          titleSpacing: 0.0,
          backgroundColor: Colors.white,
          title: Text(
            ImsStrings.sContactUs,
            style: const TextStyle(
                color: Colors.black,
                fontWeight: FontWeight.w500,
                fontFamily: "IBMPlexSans",
                fontStyle: FontStyle.normal,
                fontSize: 16.0),
          ),
          leading: IconButton(
            icon: getSvgIcon.backSvgIcon,
            onPressed: () => Navigator.pop(context, false),
          ),
        ),
        body: Container(
          color: Colors.white,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Padding(
                padding: EdgeInsets.only(top: screenHeight * 80 / actualHeight),
                child: Center(
                  child: Text(
                    ImsStrings.sPhone,
                    style: TextStyle(
                        fontFamily: "IBMPlexSans",
                        fontStyle: FontStyle.normal,
                        fontWeight: FontWeight.w400,
                        color: ImsColors.dark_navy_blue,
                        fontSize: screenHeight * 14 / actualHeight),
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(top: screenHeight * 10 / actualHeight),
                child: Center(
                  child: GestureDetector(
                    onTap: () {
                      launchCaller(ImsStrings.sImsPhoneNo);
                    },
                    child: Text(
                      ImsStrings.sImsPhoneNo,
                      style: TextStyle(
                          fontFamily: "IBMPlexSans",
                          fontStyle: FontStyle.normal,
                          fontWeight: FontWeight.w500,
                          color: ImsColors.azure,
                          fontSize: screenHeight * 14 / actualHeight),
//                      style: TextStyle(
//                          color: ImsColors.azure,
//                          fontFamily: "IBMPlexSans",
//                          fontSize: screenHeight * 14 / actualHeight),
                    ),
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(top: screenHeight * 19 / actualHeight),
                child: Center(
                  child: Text(
                    'Email',
                    style: TextStyle(
                        fontFamily: "IBMPlexSans",
                        fontStyle: FontStyle.normal,
                        fontWeight: FontWeight.w400,
                        color: ImsColors.dark_navy_blue,
                        fontSize: screenHeight * 14 / actualHeight),
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(top: screenHeight * 10 / actualHeight),
                child: Center(
                  child: GestureDetector(
                    onTap: () {
//                      launchMail('myims@imsindia.com', 'Query', 'Hello Ims');
                      EmailFunc('support@imsindia.com', 'Query', 'Hello Ims');
                    },
                    child: Text(
                      'support@imsindia.com',
                      style: TextStyle(
                          fontFamily: "IBMPlexSans",
                          fontStyle: FontStyle.normal,
                          fontWeight: FontWeight.w500,
                          color: ImsColors.azure,
                          fontSize: screenHeight * 14 / actualHeight),
                    ),
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(top: screenHeight * 19 / actualHeight),
                child: Center(
                  child: Text(
                    'Address',
                    style: TextStyle(
                        fontFamily: "IBMPlexSans",
                        fontStyle: FontStyle.normal,
                        fontWeight: FontWeight.w400,
                        color: ImsColors.dark_navy_blue,
                        fontSize: screenHeight * 14 / actualHeight),
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(
                    top: screenHeight * 10 / actualHeight,
                    left: screenWidth * 26 / actualWidth,
                    right: screenWidth * 26 / actualWidth),
                child: Center(
                  child: Text(
                    '6th Floor, NCL Building, E- Block, Bandra Kurla Complex, BKC Road, Bandra East, Mumbai, Maharashtra 400051.',
                    style: TextStyle(
                        fontFamily: "IBMPlexSans",
                        fontStyle: FontStyle.normal,
                        fontWeight: FontWeight.w500,
                        color: ImsColors.dark_navy_blue,
                        fontSize: screenHeight * 14 / actualHeight),
                    textAlign: TextAlign.center,
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(top: screenHeight * 17 / actualHeight),
                child: Center(
                  child: Text(
                    'Follow us on',
                    style: TextStyle(
                        fontFamily: "IBMPlexSans",
                        fontStyle: FontStyle.normal,
                        fontWeight: FontWeight.w400,
                        color: ImsColors.dark_navy_blue,
                        fontSize: screenHeight * 14 / actualHeight),
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(
                    top: screenHeight * 15 / actualHeight,
                    left: 125 / actualWidth * screenWidth),
                child: Center(
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      GestureDetector(
                        child: Image.asset(
                          'assets/images/twitter.png',
                          height: 20 / actualHeight * screenHeight,
                          width: 20 / actualWidth * screenWidth,
                        ),
                        onTap: () {
                          try {
                            launchURL('${ImsStrings.URL_TWITTER}');
                          } catch (e) {
                            print(e.toString());
                          }
                        },
                      ),
                      Container(
                        margin: EdgeInsets.only(
                            left: screenWidth * 25 / actualWidth,
                            right: screenWidth * 25 / actualWidth),
                        child: GestureDetector(
                          child: Image.asset(
                            'assets/images/instagram.png',
                            height: 20 / actualHeight * screenHeight,
                            width: 20 / actualWidth * screenWidth,
                          ),
                          onTap: () {
                            try {
                              launchURL('${ImsStrings.URL_INSTAGRAM}');
                            } catch (e) {
                              print(e.toString());
                            }
                          },
                        ),
                      ),
                      GestureDetector(
                        child: Image.asset(
                          'assets/images/facebook.png',
//                          assets/images/google_plus_2.png
                          height: 20 / actualHeight * screenHeight,
                          width: 20 / actualWidth * screenWidth,
                        ),
                        onTap: () {
                          try {
                            launchURL('${ImsStrings.URL_FACEBOOK}');
                          } catch (e) {
                            print(e.toString());
                          }
                        },
                      )
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  final twentyMillis = const Duration(milliseconds: 20);
  @override
  void initState() {
    super.initState();

    // new Timer(twentyMillis, printmsgininterval);
    // new Timer(twentyMillis, printmsginintervalAsync);
  }

  void printmsgininterval() {
    debugPrint('Arun :: Hello in printmsgininterval');
    new Timer(twentyMillis, printmsgininterval);
  }

  void printmsginintervalAsync() async {
    debugPrint('Arun :: Hello in printmsgininterval Async');
    new Timer(twentyMillis, printmsginintervalAsync);
  }
}

void CalculateScreen(BuildContext context) {
  var _mediaQueryData = MediaQuery.of(context);
  screenWidthTotal = _mediaQueryData.size.width;
  screenHeightTotal = _mediaQueryData.size.height;
  _safeAreaHorizontal =
      _mediaQueryData.padding.left + _mediaQueryData.padding.right;
  _safeAreaVertical =
      _mediaQueryData.padding.top + _mediaQueryData.padding.bottom;

  screenHeight = screenHeightTotal - _safeAreaVertical;
  screenWidth = screenWidthTotal - _safeAreaHorizontal;
}

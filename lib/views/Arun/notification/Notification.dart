import 'package:bubble_tab_indicator/bubble_tab_indicator.dart';
import 'package:flutter/material.dart';

import '../ims_utility.dart';
import 'BSchool.dart';
import 'IMS.dart';
import 'Personal.dart';
import 'TopPriority.dart';
import 'package:imsindia/utils/emaximiser_svg_icons.dart';

const actualHeight = 740;
const actualWidth = 360;
var screenWidthTotal;
var screenHeightTotal;
var _safeAreaHorizontal;
var _safeAreaVertical;
var screenHeight;
var screenWidth;

class Notifications extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    CalculateScreen(context);
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: DefaultTabController(
        length: 4,
        child: Scaffold(
          backgroundColor: Colors.white,
          appBar: AppBar(
            brightness: Brightness.light,
            elevation: 0.0,
            centerTitle: false,
            titleSpacing: 0.0,
            backgroundColor: Colors.white,
            title: Text(
              "Notifications",
              style: const TextStyle(
                  color: Colors.black,
                  fontWeight: FontWeight.w500,
                  fontFamily: "IBMPlexSans",
                  fontStyle: FontStyle.normal,
                  fontSize: 16.0),
            ),
            leading: IconButton(
              icon: getSvgIcon.backSvgIcon,
              onPressed: () => Navigator.pop(context, false),
            ),
            bottom: TabBar(
              labelStyle: TextStyle(fontSize: 14, color: Colors.white,
                fontWeight: FontWeight.w400,
                fontFamily: "IBMPlexSans",
                fontStyle: FontStyle.normal,),
              indicatorSize: TabBarIndicatorSize.tab,
              isScrollable: true,
              unselectedLabelColor: ImsColors.bluey_grey,
              unselectedLabelStyle:
                  TextStyle(fontSize: 14, color: Colors.white,
                    fontWeight: FontWeight.w400,
                    fontFamily: "IBMPlexSans",
                    fontStyle: FontStyle.normal,),

              indicator: BubbleTabIndicator(
                indicatorHeight: 30.0 / actualHeight * screenHeight,
                indicatorColor: ImsColors.purpleish_blue,
                tabBarIndicatorSize: TabBarIndicatorSize.tab,
              ),

//            TabBar(
//              labelStyle:
//                  TextStyle(fontSize: 14, color: ImsColors.purpleish_blue),
//              indicatorColor: ImsColors.purpleish_blue,
//              labelColor: ImsColors.bluey_grey,
//              unselectedLabelColor: ImsColors.bluey_grey,
              tabs: [
                Tab(
                  text: 'Top Priority',
                ),
                Tab(
                  text: 'IMS',
                ),
                Tab(
                  text: 'B-School',
                ),
                Tab(
                  text: 'Personal',
                ),
              ],
            ),
          ),
          body: TabBarView(
            children: [
              TopPriority(),
              IMS(),
              BSchool(),
              Personal(),
            ],
          ),
        ),
      ),
    );
  }
}

void CalculateScreen(BuildContext context) {
  var _mediaQueryData = MediaQuery.of(context);
  screenWidthTotal = _mediaQueryData.size.width;
  screenHeightTotal = _mediaQueryData.size.height;
  _safeAreaHorizontal =
      _mediaQueryData.padding.left + _mediaQueryData.padding.right;
  _safeAreaVertical =
      _mediaQueryData.padding.top + _mediaQueryData.padding.bottom;

  screenHeight = screenHeightTotal - _safeAreaVertical;
  screenWidth = screenWidthTotal - _safeAreaHorizontal;
}

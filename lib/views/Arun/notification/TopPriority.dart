import 'package:flutter/material.dart';

import '../ims_utility.dart';

const actualHeight = 740;
const actualWidth = 360;
var screenWidthTotal;
var screenHeightTotal;
var _safeAreaHorizontal;
var _safeAreaVertical;
var screenHeight;
var screenWidth;

class TopPriority extends StatefulWidget {
  @override
  _TopPriorityState createState() => _TopPriorityState();
}

class _TopPriorityState extends State<TopPriority> {
  @override
  Widget build(BuildContext context) {
    CalculateScreen(context);

    return Container(
      child: BodyLayout(),
    );
  }
}

class BodyLayout extends StatefulWidget {
  @override
  BodyLayoutState createState() {
    return new BodyLayoutState();
  }
}

class BodyLayoutState extends State<BodyLayout> {
  List<String> titles = ['Sun', 'Moon', 'Star', 'Arun', 'sunny'];
  List<Item> itemsAll = [
    Item(true),
    Item(false),
    Item(true),
    Item(false),
    Item(false),
    Item(true)
  ];

  @override
  Widget build(BuildContext context) {
    return _myListView();
  }

  Widget _myListView() {
    return ListView.builder(
      itemCount: titles.length,
      itemBuilder: (context, index) {
        final item = itemsAll[index];
        return Container(
          child: Container(
              margin: EdgeInsets.only(
                  top: 10.0 / actualHeight * screenHeight,
                  left: 20.0 / actualWidth * screenWidth,
                  right: 20.0 / actualWidth * screenWidth),
              child: Column(
                children: <Widget>[
                  Container(
                    child: Row(
                      children: <Widget>[
                        Expanded(
                          flex: 1,
                          child: Padding(
                            padding: EdgeInsets.only(
                                right: 25.0 / actualHeight * screenHeight),
                            child: SizedBox(
                              //the container has no width or height, but the sizedbox widget forces it to its own width/height
                              child: Container(
                                height: 60.0,
                                width: 3.0 / actualWidth * screenWidth,
                                decoration: new BoxDecoration(
                                    color: item.bInbdicatorVal
                                        ? ImsColors.grapefruit
                                        : ImsColors.iceBlue,
                                    borderRadius: new BorderRadius.only(
                                        topLeft: const Radius.circular(40.0),
                                        bottomLeft: const Radius.circular(40.0),
                                        bottomRight:
                                            const Radius.circular(40.0),
                                        topRight: const Radius.circular(40.0))),
                              ),
                            ),
                          ),
                        ),
                        Expanded(
                          flex: 10,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text(
                                item.title,
                                maxLines: 2,
                                textAlign: TextAlign.left,
                                style: TextStyle(
                                    fontSize: 14.0,
                                    fontWeight: FontWeight.w400,
                                    fontFamily: "IBMPlexSans",
                                    fontStyle: FontStyle.normal,

                                    color: ImsColors.dark_navy_blue),
                              ),
                              Container(
                                margin: EdgeInsets.only(
                                    top: 9 / actualHeight * screenHeight),
                                child: Text(
                                  '${item.subtitle}',
                                  textAlign: TextAlign.left,
                                  maxLines: 1,
                                  style: TextStyle(
                                      color: ImsColors.bluey_grey,
                                      fontWeight: FontWeight.w400,
                                      fontFamily: "IBMPlexSans",
                                      fontStyle: FontStyle.normal,
                                      fontSize: 12.0),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(
                        top: 14.5 / actualHeight * screenHeight),
                    child: Container(
                      height: 1 / actualHeight * screenHeight,
                      width: double.infinity,
                      color: ImsColors.iceBlue,
                    ),
                  )
                ],
              )),
        );
      },
    );
  }
}

class Item {
  var title = 'How to analyse Mock CATs: Mocks liya ab kya karna hai?';
  var subtitle = '01-Aug-2018';
  var subtitle2 = '50 min Arun';
  var bInbdicatorVal;
  var imgUrl =
      'https://thumbs.dreamstime.com/b/inspirational-quote-there-blessings-everyday-find-them-create-treasure-beautiful-white-single-sinnia-flower-blossom-blurry-151653366.jpg';
  Item(bool bValk) {
    bInbdicatorVal = bValk;
  }
}

void CalculateScreen(BuildContext context) {
  var _mediaQueryData = MediaQuery.of(context);
  screenWidthTotal = _mediaQueryData.size.width;
  screenHeightTotal = _mediaQueryData.size.height;
  _safeAreaHorizontal =
      _mediaQueryData.padding.left + _mediaQueryData.padding.right;
  _safeAreaVertical =
      _mediaQueryData.padding.top + _mediaQueryData.padding.bottom;

  screenHeight = screenHeightTotal - _safeAreaVertical;
  screenWidth = screenWidthTotal - _safeAreaHorizontal;
}

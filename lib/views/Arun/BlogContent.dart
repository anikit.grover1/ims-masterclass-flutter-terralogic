class BlogContent {
//  final int id;
//  final String name;
//  final int age;
//
//  Dog({this.id, this.name, this.age});
  //final int id;
  var id;
  final String content;
  final String content1;
  final String content2;
  final String content3;
  final String content4;
  final String content5;

  BlogContent({this.id,this.content, this.content1, this.content2, this.content3,this.content4,this.content5});

  Map<String, dynamic> toMap() {
    return {
      'id' : id,
      'content': content,
      'content1': content1,
      'content2': content2,
      'content3': content3,
      'content4': content4,
      'content5': content5
    };
  }

  // Implement toString to make it easier to see information about
  // each dog when using the print statement.
  @override
  String toString() {
    return 'Arun BlogContent data {id : $id, content: $content, content1: $content1,content2: $content2,content3: $content3,content4: $content4,content5: $content5,}';
  }
}

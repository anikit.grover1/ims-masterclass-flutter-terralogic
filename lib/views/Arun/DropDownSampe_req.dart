import 'package:flutter/material.dart';

class Arun extends StatefulWidget {
  @override
  _ArunState createState() => _ArunState();
}

bool bShowList = false;
bool bShowList2 = false;
String strVal = 'Mr. Shivank Verma';
String strVal2 = 'Mr. Arun Verma';

class _ArunState extends State<Arun> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        body: Padding(
          padding: EdgeInsets.only(top: 50.0),
          child: Column(
            children: <Widget>[
              Container(
                child: Column(
                  children: <Widget>[
                    Align(
                      alignment: Alignment.topLeft,
                      child: Container(
                        margin: EdgeInsets.only(left: 20, top: 20),
                        child: Opacity(
                          opacity: 0.5,
                          child: Text(
                            "Choose Your Mentor",
                            style: TextStyle(
                              color: Color.fromARGB(255, 126, 145, 154),
                              fontSize: 12,
                              fontFamily: "IBM Plex Sans",
                            ),
                            textAlign: TextAlign.left,
                          ),
                        ),
                      ),
                    ),
                    GestureDetector(
                      onTap: () {
                        setState(() {
                          if (bShowList)
                            bShowList = false;
                          else
                            bShowList = true;
                        });
                      },
                      child: Container(
                        height: 18,
                        margin: EdgeInsets.only(left: 20, top: 10, right: 20),
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          children: [
                            Align(
                              alignment: Alignment.topLeft,
                              child: Text(
                                "$strVal",
                                style: TextStyle(
                                  color: Color.fromARGB(255, 45, 52, 56),
                                  fontSize: 14,
                                  fontFamily: "IBM Plex Sans",
                                ),
                                textAlign: TextAlign.left,
                              ),
                            ),
                            Spacer(),
                            Align(
                              alignment: Alignment.topLeft,
                              child: Container(
                                width: 11,
                                height: 7,
                                margin: EdgeInsets.only(top: 5),
                                child: Opacity(
                                  opacity: 0.5,
                                  child: Image.asset(
                                    "assets/images/mentor_shape-copy-3.png",
                                    fit: BoxFit.none,
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    Container(
                      height: 1,
                      margin: EdgeInsets.only(left: 20, top: 5, right: 20),
                      decoration: BoxDecoration(
                        color: Color.fromARGB(255, 136, 109, 215),
                      ),
                      child: Container(),
                    ),
                  ],
                ),
              ),
//              GestureDetector(
//                onTap: () {
//                  setState(() {
//                    if (bShowList)
//                      bShowList = false;
//                    else
//                      bShowList = true;
//                  });
//                },
//                child: Container(
//                  margin: EdgeInsets.symmetric(horizontal: 20.0),
//                  width: double.infinity,
//                  height: 50.0,
//                  color: Colors.amber,
//                  child: Padding(
//                    padding: EdgeInsets.all(8.0),
//                    child: Text(
//                      '$strVal',
//                      style: TextStyle(
//                        fontSize: 14.0,
//                      ),
//                    ),
//                  ),
//                ),
//              ),
              Visibility(
                visible: bShowList,
                child: Container(
                  margin: EdgeInsets.symmetric(horizontal: 20.0),
                  width: double.infinity,
                  height: 250.0,
                  color: Colors.white,
                  child: ListView.builder(
                    itemCount: titles.length,
                    itemBuilder: (context, index) {
                      return Container(
                        child: ListTile(
                          // leading: Icon(icons[index]),
                          title: Text(titles[index]),
                          onTap: () {
                            //                                  <-- onTap
                            setState(() {
//                            titles.insert(index, 'Planet');
                              bShowList = false;
                              strVal = titles[index];
                            });
                          },
                        ),
                      );
                    },
                  ),
                ),
              ),
              Container(
                child: Column(
                  children: <Widget>[
                    Align(
                      alignment: Alignment.topLeft,
                      child: Container(
                        margin: EdgeInsets.only(left: 20, top: 20),
                        child: Opacity(
                          opacity: 0.5,
                          child: Text(
                            "Choose Your Mentor",
                            style: TextStyle(
                              color: Color.fromARGB(255, 126, 145, 154),
                              fontSize: 12,
                              fontFamily: "IBM Plex Sans",
                            ),
                            textAlign: TextAlign.left,
                          ),
                        ),
                      ),
                    ),
                    GestureDetector(
                      onTap: () {
                        setState(() {
                          if (bShowList2)
                            bShowList2 = false;
                          else
                            bShowList2 = true;
                        });
                      },
                      child: Container(
                        height: 18,
                        margin: EdgeInsets.only(left: 20, top: 10, right: 20),
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          children: [
                            Align(
                              alignment: Alignment.topLeft,
                              child: Text(
                                "$strVal2",
                                style: TextStyle(
                                  color: Color.fromARGB(255, 45, 52, 56),
                                  fontSize: 14,
                                  fontFamily: "IBM Plex Sans",
                                ),
                                textAlign: TextAlign.left,
                              ),
                            ),
                            Spacer(),
                            Align(
                              alignment: Alignment.topLeft,
                              child: Container(
                                width: 11,
                                height: 7,
                                margin: EdgeInsets.only(top: 5),
                                child: Opacity(
                                  opacity: 0.5,
                                  child: Image.asset(
                                    "assets/images/mentor_shape-copy-3.png",
                                    fit: BoxFit.none,
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    Container(
                      height: 1,
                      margin: EdgeInsets.only(left: 20, top: 5, right: 20),
                      decoration: BoxDecoration(
                        color: Color.fromARGB(255, 136, 109, 215),
                      ),
                      child: Container(),
                    ),
                  ],
                ),
              ),
//              GestureDetector(
//                onTap: () {
//                  setState(() {
//                    if (bShowList)
//                      bShowList = false;
//                    else
//                      bShowList = true;
//                  });
//                },
//                child: Container(
//                  margin: EdgeInsets.symmetric(horizontal: 20.0),
//                  width: double.infinity,
//                  height: 50.0,
//                  color: Colors.amber,
//                  child: Padding(
//                    padding: EdgeInsets.all(8.0),
//                    child: Text(
//                      '$strVal',
//                      style: TextStyle(
//                        fontSize: 14.0,
//                      ),
//                    ),
//                  ),
//                ),
//              ),
              Visibility(
                visible: bShowList2,
                child: Container(
                  margin: EdgeInsets.symmetric(horizontal: 20.0),
                  width: double.infinity,
                  height: 250.0,
                  color: Colors.white,
                  child: ListView.builder(
                    itemCount: titles.length,
                    itemBuilder: (context, index) {
                      return Container(
                        child: ListTile(
                          // leading: Icon(icons[index]),
                          title: Text(titles[index]),
                          onTap: () {
                            //                                  <-- onTap
                            setState(() {
//                            titles.insert(index, 'Planet');
                              bShowList2 = false;
                              strVal2 = titles[index];
                            });
                          },
                        ),
                      );
                    },
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

final titles = [
  'bike',
  'boat',
  'bus',
  'car',
  'railway',
  'run',
  'subway',
  'transit',
  'walk'
];

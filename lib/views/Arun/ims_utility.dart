import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mailer/flutter_mailer.dart';
//import 'package:imsindia/views/Arun/search/SqliteHelper_LocalSearchContent.dart';
import 'package:imsindia/views/Arun/SqliteHelper.dart';
import 'package:imsindia/views/splash_screen/splash_screen.dart';
import 'package:url_launcher/url_launcher.dart';

import 'IMSSharedPref.dart';

class ImsColors {
  //Semantic colors used in application
  static const Color dark_navy_blue = const Color(0xFF00022c);
  static const Color namecolor = const Color(0xFF1f252b);
  static const Color azure = const Color(0xFF00abfb);

  static const Color dusky_blue = const Color(0xFF475993);
  static const Color disablecolor = const Color(0xFFF7F5F9);
  static const Color blur = const Color(0xFF0d676767);
  static const Color grapefruit = const Color(0xFFff5757);
  static const Color bluey_grey = const Color(0xFF999aab);
  static const Color purpleish_blue = const Color(0xFF5647eb);
  static const Color ice_blue = const Color(0xFFf2f6f8);
  static const Color blueGrey = const Color(0xFF7e919a);
  static const Color iceBlue = const Color(0xFFf2f4f4);
  static const Color charcoal_grey = const Color(0xFF2d3438);
  static const Color purpley = const Color(0xFF886dd7);
  static const Color gunmetal = const Color(0xFF575d60);
}

class ImsStrings {
  static final String sEdit = 'Edit';
  static final String sSave = 'SAVE';
  static final String sPersonal = 'Personal';
  static final String sWork = 'Work';
  static final String sEducational = 'Educational';
  static final String sDetails = 'Details';

  static final String sFAQ = 'FAQ';
  static final String sSettings = 'Settings';
  static final String sDataUsage = 'Data Usage';
  static final String sLimitcellulardatausage = 'Limit cellular data usage';
  static final String sOnlystreamvideosonWifi = 'Only stream videos on Wifi';
  static final String sPrivacy = 'Privacy';
  static final String sClearSearchHistory = 'Clear Search History';
  static final String sClearAppData = 'Clear App Data';
  static final String sPrivacyPolicy = 'Privacy Policy';
  static final String sTermsofUse = 'Terms of Use';
  static final String sChangePassword = 'Change Password';
  static final String sLinkAccounts = 'Link Accounts';
  static final String sGoogle = 'Google';
  static final String sFacebook = 'Facebook';
  static final String sEnterOldPassword = 'Enter Old Password';
  static final String sReEnterNewPassword = 'Re-Enter New Password';
  static final String sOldPassword = 'Old Password';
  static final String sNewPassword = 'New Password';
  static final String sEnterNewPassword = 'Enter New Password';
  static final String sSaveChanges = 'SAVE CHANGES';
  static final String sContactUs = 'Contact Us';
  static final String sPhone = 'Phone';
  static final String sImsPhoneNo = '1800-1234-467';

  //DB Table Names
  static final String TBL_LOCALSEARCHCONTENT = 'localsearchcontent';
  static final String TBL_BlogContent = 'blogcontent';

  //URLs
  static final String URL_PRIVACYPOLICY =
      'https://www.imsindia.com/privacy-policy.html';
  static final String URL_FACEBOOK = 'https://www.facebook.com/imsindia';
  static final String URL_TWITTER = 'https://twitter.com/ims_india';
  static final String URL_INSTAGRAM = 'https://www.instagram.com/ims.india/';
  static final String URL_FORGOTPASSWORD = 'https://myaccount.imsindia.com/login/default.aspx?action=forgot-password';

  //Shared preferences
  static final String sCurrentPassword = 'CurrentPassword';
  static final String sCurrentUserId = 'sCurrentUserId';
  static final String sCurrentUserName = 'sCurrentUserName';
  static final String sCurrentUserEmail = 'sCurrentUserEmail';
  static final String sCurrentUserLoginId = 'sCurrentUserLoginId';
}

void launchURL(String sUrl) async {
//  var url = 'https://flutter.io';
  var url = sUrl;
  if (await canLaunch(url)) {
    await launch(url);
  } else {
    throw 'Could not launch $url';
  }
}

void launchCaller(String telNo) async {
  var url = "tel:$telNo";
  if (await canLaunch(url)) {
    await launch(url);
  } else {
    throw 'Could not launch $url';
  }
}

void launchMail(String toMailId, String subject, String body) async {
  var url = 'mailto:$toMailId?subject=$subject&body=$body';
  if (await canLaunch(url)) {
    await launch(url);
  } else {
    throw 'Could not launch $url';
  }
}

void EmailFunc(String toMailId, String subject, String body) async {
  MailOptions mailOptions = MailOptions(
    body: body,
    subject: subject,
    recipients: [toMailId],
    isHTML: true,
//    bccRecipients: ['other@example.com'],
//    ccRecipients: ['third@example.com'],
    //attachments: [ 'path/to/image.png', ],
  );

  await FlutterMailer.send(mailOptions);
}

void ClearLocalSearchData() {
  SqliteHelper obj =
      SqliteHelper('imsApp.db');
  obj.deleteAll();
}

void ClearAppData(BuildContext context) {
  // Similiar to Signout
  ClearLocalSearchData();
  IMSSharedPref().ClearSharedPreferences();
  Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => SplashScreen(),
      )).then((_) {
    Navigator.of(context).pop();
  });
}

class SizeConfig {
  static MediaQueryData _mediaQueryData;
  static double screenWidth;
  static double screenHeight;
  static double blockSizeHorizontal;
  static double blockSizeVertical;

  static double _safeAreaHorizontal;
  static double _safeAreaVertical;
  static double safeBlockHorizontal;
  static double safeBlockVertical;

  void init(BuildContext context) {
    _mediaQueryData = MediaQuery.of(context);
    screenWidth = _mediaQueryData.size.width;
    screenHeight = _mediaQueryData.size.height;
    blockSizeHorizontal = screenWidth / 100;
    blockSizeVertical = screenHeight / 100;

    _safeAreaHorizontal =
        _mediaQueryData.padding.left + _mediaQueryData.padding.right;
    _safeAreaVertical =
        _mediaQueryData.padding.top + _mediaQueryData.padding.bottom;
    safeBlockHorizontal = (screenWidth - _safeAreaHorizontal) / 100;
    safeBlockVertical = (screenHeight - _safeAreaVertical) / 100;
  }
}

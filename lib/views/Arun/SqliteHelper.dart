import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:imsindia/views/Arun/BlogContent.dart';
import 'package:imsindia/views/Arun/ims_utility.dart';
import 'package:imsindia/views/Arun/search/LocalSearchContect.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

class SqliteHelper {
  String dbName = '';
  Database database;

  SqliteHelper(String dbName) {
    this.dbName = dbName;
  }

  Future _create(Database db, int version) async {
    await db.execute("""
            CREATE TABLE ${ImsStrings.TBL_BlogContent}(id INTEGER PRIMARY KEY AUTOINCREMENT, content TEXT, content1 TEXT,content2 TEXT,content3 TEXT,content4 TEXT,content5 TEXT)""");

    await db.execute("""
            CREATE TABLE ${ImsStrings.TBL_LOCALSEARCHCONTENT}( content TEXT PRIMARY KEY)""");
  }
  //createDB() async {
  
    // database = openDatabase(
    //   // Set the path to the database. Note: Using the `join` function from the
    //   // `path` package is best practice to ensure the path is correctly
    //   // constructed for each platform.
    //   join(await getDatabasesPath(), dbName),
    //   // When the database is first created, create a table to store dogs.
    //   onCreate: 
    //   // (db, version) {
    //   //   return db.execute(
    //   //     "CREATE TABLE ${ImsStrings.TBL_BlogContent}( content TEXT PRIMARY KEY, content1 TEXT,content2 TEXT,content3 TEXT)",
    //   //   );
    //   // },
    //   // // Set the version. This executes the onCreate function and provides a
    //   // // path to perform database upgrades and downgrades.
    //   // version: 1,
    // );
    Future create() async {
        debugPrint(
      'Arun  table definition ' +
          "CREATE TABLE ${ImsStrings.TBL_BlogContent}(id INTEGER PRIMARY KEY AUTOINCREMENT, content TEXT, content1 TEXT,content2 TEXT,content3 TEXT,content4 TEXT,content5 TEXT)",
    );
    debugPrint(
      'Arun  table definition ' +
          "CREATE TABLE ${ImsStrings.TBL_LOCALSEARCHCONTENT}( content TEXT PRIMARY KEY)",
    );
    String dbPath = join(await getDatabasesPath(), dbName);

    database = await openDatabase(dbPath, version: 1,
        onCreate: this._create);
//  }

    debugPrint('Arun db created successfully');
  }

  Future<void> insertBlog(BlogContent data) async {
    // Get a reference to the database.
    Database db = await database;

    // Insert the BlogContent into the correct table. Also specify the
    // `conflictAlgorithm`. In this case, if the same BlogContent is inserted
    // multiple times, it replaces the previous data.
    if (db == null) {
      db = await openDatabase(
        // Set the path to the database. Note: Using the `join` function from the
        // `path` package is best practice to ensure the path is correctly
        // constructed for each platform.
        join(await getDatabasesPath(), 'imsApp.db'),
      );
    }
    debugPrint('Arun map of data : ${data.toMap()}');
    debugPrint('Arun String of data : ${data.toString()}');
    await db.insert(
      '${ImsStrings.TBL_BlogContent}',
      data.toMap(),
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
  }

  Future<List<BlogContent>> getDataBlog() async {
    // Get a reference to the database.
    Database db = await database;

    if (db == null) {
      db = await openDatabase(
        // Set the path to the database. Note: Using the `join` function from the
        // `path` package is best practice to ensure the path is correctly
        // constructed for each platform.
        join(await getDatabasesPath(), dbName),
      );
    }
    // Query the table for all The BlogContent.
    final List<Map<String, dynamic>> maps = await db.query('BlogContent');

    // Convert the List<Map<String, dynamic> into a List<Dog>.
    return List.generate(maps.length, (i) {
      return BlogContent(
          id: maps[i]['id'],
          content: maps[i]['content'],
          content1: maps[i]['content1'],
          content2: maps[i]['content2'],
          content3: maps[i]['content3'],
          content4: maps[i]['content4'],
          content5: maps[i]['content5']);

//      return Dog(
//        id: maps[i]['id'],
//        name: maps[i]['name'],
//        age: maps[i]['age'],
//      );
    });
  }

  Future<void> update(BlogContent data) async {
    // Get a reference to the database.
    Database db = await database;
    if (db == null) {
      db = await openDatabase(
        // Set the path to the database. Note: Using the `join` function from the
        // `path` package is best practice to ensure the path is correctly
        // constructed for each platform.
        join(await getDatabasesPath(), dbName),
      );
    }
    // Update the given BlogContent.
    await db.update(
      '${ImsStrings.TBL_BlogContent}',
      data.toMap(),

      where: "content = ?",
      // Pass the BlogContent's id as a whereArg to prevent SQL injection.
      whereArgs: [data.content],
    );
  }

  Future<void> delete() async {
    // Get a reference to the database.
    Database db = await database;

    if (db == null) {
      db = await openDatabase(
        // Set the path to the database. Note: Using the `join` function from the
        // `path` package is best practice to ensure the path is correctly
        // constructed for each platform.
        join(await getDatabasesPath(), dbName),
      );
    }
    // Remove the BlogContent from the database.
    await db.delete(
      '${ImsStrings.TBL_BlogContent}',
      // Use a `where` clause to delete a specific BlogContent.
      where: "content = ?",
      // Pass the BlogContent's id as a whereArg to prevent SQL injection.
      whereArgs: [],
    );

    await db.delete(
      '${ImsStrings.TBL_LOCALSEARCHCONTENT}',
      // Use a `where` clause to delete a specific localsearchcontent.
      where: "content = ?",
      // Pass the localsearchcontent's id as a whereArg to prevent SQL injection.
      whereArgs: [],
    );
  }

  Future<void> deleteAll() async {
    // Get a reference to the database.
    Database db = await database;

    if (db == null) {
      db = await openDatabase(
        // Set the path to the database. Note: Using the `join` function from the
        // `path` package is best practice to ensure the path is correctly
        // constructed for each platform.
        join(await getDatabasesPath(), dbName),
      );
    }
    // Remove the BlogContent from the database.
    await db.rawDelete(
      'delete from ${ImsStrings.TBL_BlogContent}',
      // Use a `where` clause to delete a specific BlogContent.
    );
    await db.rawDelete(
      'delete from ${ImsStrings.TBL_LOCALSEARCHCONTENT}',
      // Use a `where` clause to delete a specific localsearchcontent.
    );
  }

  Future<void> deleteAllBlogs() async {
    // Get a reference to the database.
    Database db = await database;

    if (db == null) {
      db = await openDatabase(
        // Set the path to the database. Note: Using the `join` function from the
        // `path` package is best practice to ensure the path is correctly
        // constructed for each platform.
        join(await getDatabasesPath(), dbName),
      );
    }
    // Remove the BlogContent from the database.
    await db.rawDelete(
      'delete from ${ImsStrings.TBL_BlogContent}',
      // Use a `where` clause to delete a specific BlogContent.
    );
  }

  Future<void> insertSearch(LocalSearchContent data) async {
    // Get a reference to the database.
    Database db = await database;

    // Insert the localsearchcontent into the correct table. Also specify the
    // `conflictAlgorithm`. In this case, if the same localsearchcontent is inserted
    // multiple times, it replaces the previous data.
    if (db == null) {
      db = await openDatabase(
        // Set the path to the database. Note: Using the `join` function from the
        // `path` package is best practice to ensure the path is correctly
        // constructed for each platform.
        join(await getDatabasesPath(), 'imsApp.db'),
      );
    }
    await db.insert(
      '${ImsStrings.TBL_LOCALSEARCHCONTENT}',
      data.toMap(),
      conflictAlgorithm: ConflictAlgorithm.replace,
    );


  }

  Future<List<LocalSearchContent>> getDataSearch() async {
    // Get a reference to the database.
    Database db = await database;

    if (db == null) {
      db = await openDatabase(
        // Set the path to the database. Note: Using the `join` function from the
        // `path` package is best practice to ensure the path is correctly
        // constructed for each platform.
        join(await getDatabasesPath(), dbName),
      );
    }
    // Query the table for all The localsearchcontent.
    final List<Map<String, dynamic>> maps =
        await db.query('localsearchcontent');

    // Convert the List<Map<String, dynamic> into a List<Dog>.
    return List.generate(maps.length, (i) {
      return LocalSearchContent(content: maps[i]['content']);
    });
  }

  
}

import 'package:shared_preferences/shared_preferences.dart';

class IMSSharedPref {
  static final limitCellularDataUsage = "limitCellularDataUsage";
  static final googleusername = "googleusername";
  static final facebookusername = "facebookusername";

  // [Arun classes like this should be singleton classes]
  //==> Praveen: From Now we will use singleton classes for this purpose
  static final IMSSharedPref _singleton = IMSSharedPref._internal();

  factory IMSSharedPref() {
    return _singleton;
  }

  IMSSharedPref._internal();
  Future<bool> get_limitCellularDataUsage() async {
     bool blimitCellularDataUsage;
    SharedPreferences prefs = await SharedPreferences.getInstance();
    blimitCellularDataUsage = prefs.getBool(limitCellularDataUsage);
    return blimitCellularDataUsage;
  }

  void set_limitCellularDataUsage(value) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setBool(limitCellularDataUsage, value);
  }

  void ClearSharedPreferences() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.clear();
  }
}

import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:imsindia/api/bloc/services.dart';
import 'package:imsindia/components/error_screen.dart';
import 'package:imsindia/resources/strings/myplan.dart';
import 'package:imsindia/utils/Url.dart';
import 'package:imsindia/utils/emaximiser_svg_icons.dart';
import 'package:imsindia/views/Arun/ims_utility.dart';
import 'package:imsindia/views/Arun/myPLAN/myPLAN_home_Screen.dart';
import 'package:intl/intl.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../../components/html_plugin/fluttr_html.dart';
import '../../../utils/colors.dart';
import 'package:imsindia/utils/global.dart' as global;

import 'myPLAN_bookslot_screen.dart';

var screenWidthTotal;
var screenHeightTotal;
var _safeAreaHorizontal;
var _safeAreaVertical;
var screenHeight;
var screenWidth;

class MyplanConfirmationScreen extends StatefulWidget {
  int bookingId ;
  MyplanConfirmationScreen(this.bookingId);
  @override
  MyplanConfirmationScreenState createState() => MyplanConfirmationScreenState();
}

class MyplanConfirmationScreenState extends State<MyplanConfirmationScreen> {
  bool buttonClickOnlyOnce = false;
  bool loadForCancelButton = false;
  var bookingDataBasedOnId;
  var cancelBookingApiMsg;
  Future _future;
  int bookingId=0;
  bool dateComparision=false;
  var imsPin;
  var imsCourseId;
  var booking;
  List dateFormatter(String date,String time) {
    final now = DateTime.now();
    print(time);
    DateTime timeFromApi= DateFormat.jm().parse(time);
    final dateTimeOfBooking = DateTime(int.parse(date.split("-")[0]), int.parse(date.split("-")[1]), int.parse(date.split("-")[2]), int.parse(DateFormat("HH:mm").format(timeFromApi).split(":")[0]), int.parse(DateFormat("HH:mm").format(timeFromApi).split(":")[1]));
    print(now);
    print(dateTimeOfBooking);
    if(dateTimeOfBooking.isBefore(now)){
      dateComparision=true;
    }else{
      dateComparision=false;
    }
  }

  void bookingsAvailable() async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    imsCourseId = prefs.getString("courseId");
    imsPin = prefs.getString(ImsStrings.sCurrentUserId);
    Map<String, dynamic> postData;
    postData = {
      "studentImsPin":imsPin,//"99a77771",
      "myImsCourseId":imsCourseId //"500"
    };
    ApiService().postAPITerr(URL.myPLAN_API_BOOKINGS_AVAILABLE, postData, global.headers).then((result)
    {
      if (result[0]['success'].toString().toLowerCase() == 'true'.toLowerCase()) {
        booking = result[0]['data'];
        loadForCancelButton = false;
        buttonClickOnlyOnce = false;
        Navigator.push(context, MaterialPageRoute(builder: (context) => myPLANHome(false,booking[0]['availableSlots'],booking[0]['slotsUsed'])));
      }
      else{
        print("showError Message");
        if(result[0].containsKey("message")){
          if(result[0]['message']=='available bookings are Empty'){
            loadForCancelButton = false;
            buttonClickOnlyOnce = false;
            Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) =>
                      myPLANHome(false,0,1),
                ));
          }else{
            loadForCancelButton = false;
            buttonClickOnlyOnce = false;
            print("Error Handling");
          }
        }else{
          loadForCancelButton = false;
          buttonClickOnlyOnce = false;
          print("Error Handling");
        }
      }
    });
  }
  Future getBookingsDetailsUsingId() async {
    final result = await ApiService().getAPITerr(URL.myPLAN_API_CONFIRMATION_BOOKING+bookingId.toString());
    if (result[0]['success']==true){
      setState(() {
        bookingDataBasedOnId=result[0]['data'];
        dateFormatter(bookingDataBasedOnId[0]['bookingDate'],bookingDataBasedOnId[0]['bookingTime']);
      });
      return bookingDataBasedOnId;
    }
    else{
      // totalDataForBookings = result[0]['message'];
      if(result[0].containsKey("message")){
        bookingDataBasedOnId=result[0]['message'];
      }else{
        bookingDataBasedOnId=result[0];
      }
      return bookingDataBasedOnId;
    }
  }
  Future cancelBooking() async {
    Map<String, dynamic> postData;
    postData = {
      "bookingId":bookingId.toString(),
    };
    final result = await ApiService().putAPITerr(URL.myPLAN_API_CANCEL_BOOKING,postData, global.headers);
    setState(() {
      if (result[0]['success'] == true) {
        setState(() {
          bookingsAvailable();
        });
      }
      else {
        loadForCancelButton = false;
        buttonClickOnlyOnce = false;
        // totalDataForBookings = result[0]['message'];
        if (result[0].containsKey("message")) {
          cancelBookingApiMsg = result[0]['message'];
        } else {
          cancelBookingApiMsg = result[0];
        }
      }
    });
  }
  @override
  void initState() {
    super.initState();
    // getconfirmationBooking();
    if(widget.bookingId==null){
      bookingId=0;
    }else{
      bookingId=widget.bookingId;
    }
    // dateFormatter("2020-10-01");
    _future = getBookingsDetailsUsingId();
  }

  @override
  Widget build(BuildContext context) {

    var _mediaQueryData = MediaQuery.of(context);
    screenWidthTotal = _mediaQueryData.size.width;
    screenHeightTotal = _mediaQueryData.size.height;
    _safeAreaHorizontal =
        _mediaQueryData.padding.left + _mediaQueryData.padding.right;
    _safeAreaVertical =
        _mediaQueryData.padding.top + _mediaQueryData.padding.bottom;

    screenHeight = screenHeightTotal - _safeAreaVertical;
    screenWidth = screenWidthTotal - _safeAreaHorizontal;
    onWillPop() {
      Navigator.of(context).pop(true);
    }
    return WillPopScope(
      onWillPop: onWillPop,
      child: Scaffold(
        appBar: AppBar(
          brightness: Brightness.light,
          elevation: 0.0,
          centerTitle: false,
          titleSpacing: 0.0,
          backgroundColor: Colors.white,
          title: Text(
            MyplanConfirmationScreenString.Meeting,
            style: const TextStyle(
                color: Colors.black,
                fontWeight: FontWeight.w500,
                fontFamily: "IBMPlexSans",
                fontStyle: FontStyle.normal,
                fontSize: 16.0),
          ),
          leading: IconButton(
            icon: getSvgIcon.backSvgIcon,
            onPressed: (){
              //bookingsAvailable();
              Navigator.pop(context);
            },
          ),
        ),
        body: SingleChildScrollView(
          child: FutureBuilder(
            builder: (context, snapshot) {
              if (snapshot.hasError) {
                return Container(); // widget to be shown on any error
              }

              return snapshot.hasData
                  ?
              bookingDataBasedOnId.length>0?
              Container(

                //constraints: BoxConstraints.expand(),

                decoration: BoxDecoration(
                  color:Color.fromARGB(255, 255, 255, 255),
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    Container(
                      // constraints: BoxConstraints.expand(),
                      decoration: BoxDecoration(
                        color: Color.fromARGB(255, 255, 255, 255),
                      ),
                      child: SingleChildScrollView(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          children: [
                            Container(
                              margin: EdgeInsets.only(left: 20/360*screenWidth, top: 30/720*screenHeight, right: 20/360*screenWidth),
                              child: Container(
                                decoration: BoxDecoration(
                                  color: Color.fromARGB(51, 255, 255, 255),
                                  border: Border.all(
                                    color: Color.fromARGB(51, 153, 154, 171),
                                    width: 1/360*screenWidth,
                                  ),
                                  borderRadius: BorderRadius.all(Radius.circular(5)),
                                ),
                                padding: EdgeInsets.only(bottom: 20/720*screenHeight),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Container(
                                      margin: EdgeInsets.only(left: 15/360*screenWidth, top: 15/720*screenHeight),
                                      child: Text(
                                        MyplanConfirmationScreenString.MentorName,
                                        style: TextStyle(
                                          color: Color.fromARGB(255, 153, 154, 171),
                                          // fontSize: 12,
                                          fontFamily: "IBM Plex Sans",
                                          fontWeight: FontWeight.w500,
                                        ),
                                        textAlign: TextAlign.left,
                                      ),
                                    ),
                                    Container(
                                      margin: EdgeInsets.only(left: 15/360*screenWidth, top: 1/720*screenHeight),
                                      child: Text(
                                        bookingDataBasedOnId[0]['mentorName'].toString(),
                                        style: TextStyle(
                                          color: Color.fromARGB(255, 0, 3, 44),
                                          // fontSize: 14,
                                          fontFamily: "IBM Plex Sans",
                                          fontWeight: FontWeight.w500,
                                        ),
                                        textAlign: TextAlign.left,
                                      ),
                                    ),
                                    Container(
                                      // width: 88,
                                      // height: 41,
                                      margin: EdgeInsets.only(left: 15/360*screenWidth, top: 22/720*screenHeight),
                                      child: Column(
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        children: [
                                          Text(
                                            MyplanConfirmationScreenString.MentorLocation,
                                            style: TextStyle(
                                              color: Color.fromARGB(255, 153, 154, 171),
                                              //fontSize: 12,
                                              fontFamily: "IBM Plex Sans",
                                              fontWeight: FontWeight.w500,
                                            ),
                                            textAlign: TextAlign.left,
                                          ),
                                          Container(
                                            margin: EdgeInsets.only(top: 1/720*screenHeight),
                                            child: Text(
                                              bookingDataBasedOnId[0]['branchName'].toString(),
                                              style: TextStyle(
                                                color: Color.fromARGB(255, 0, 3, 44),
                                                //fontSize: 12,
                                                fontFamily: "IBM Plex Sans",
                                                fontWeight: FontWeight.w500,
                                              ),
                                              textAlign: TextAlign.left,
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                    Container(
                                      width: 286/360*screenWidth,
                                      height: 45/720*screenHeight,
                                      margin: EdgeInsets.only(left: 15/360*screenWidth, top: 22/720*screenHeight),
                                      child: Row(
                                        crossAxisAlignment: CrossAxisAlignment.stretch,
                                        children: [
                                          Align(
                                            alignment: Alignment.topLeft,
                                            child: Container(
                                              // width: 88,
                                              // height: 41,
                                              child: Column(
                                                crossAxisAlignment: CrossAxisAlignment.start,
                                                children: [
                                                  Text(
                                                    MyplanConfirmationScreenString.MentorDate,
                                                    style: TextStyle(
                                                      color:
                                                      Color.fromARGB(255, 153, 154, 171),
                                                      //  fontSize: 12,
                                                      fontFamily: "IBM Plex Sans",
                                                      fontWeight: FontWeight.w500,
                                                    ),
                                                    textAlign: TextAlign.left,
                                                  ),
                                                  Container(
                                                    margin: EdgeInsets.only(top: 2/720*screenHeight),
                                                    child: Text(
                                                      bookingDataBasedOnId[0]['bookingDate'].toString(),
                                                      style: TextStyle(
                                                        color: Color.fromARGB(255, 0, 3, 44),
                                                        //  fontSize: 14,
                                                        fontFamily: "IBM Plex Sans",
                                                        fontWeight: FontWeight.w500,
                                                      ),
                                                      textAlign: TextAlign.left,
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ),
                                          Align(
                                            alignment: Alignment.topLeft,
                                            child: Container(
                                              width: 136/360*screenWidth,
                                              height: 50/720*screenHeight,
                                              margin: EdgeInsets.only(left: 62/360*screenWidth),
                                              child: Column(
                                                crossAxisAlignment:
                                                CrossAxisAlignment.stretch,
                                                children: [
                                                  Align(
                                                    alignment: Alignment.topLeft,
                                                    child: Text(
                                                      MyplanConfirmationScreenString.MentorTime,
                                                      style: TextStyle(
                                                        color: Color.fromARGB(
                                                            255, 153, 154, 171),
                                                        //  fontSize: 12,
                                                        fontFamily: "IBM Plex Sans",
                                                        fontWeight: FontWeight.w500,
                                                      ),
                                                      textAlign: TextAlign.left,
                                                    ),
                                                  ),
                                                  Align(
                                                    alignment: Alignment.topLeft,
                                                    child: Container(
                                                      margin: EdgeInsets.only(top: 2/720*screenHeight),
                                                      child: Text(
                                                        bookingDataBasedOnId[0]['bookingTime'].toString(),
                                                        style: TextStyle(
                                                          color:
                                                          Color.fromARGB(255, 0, 3, 44),
                                                          //   fontSize: 14,
                                                          fontFamily: "IBM Plex Sans",
                                                          fontWeight: FontWeight.w500,
                                                        ),
                                                        textAlign: TextAlign.left,
                                                      ),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                    Container(
                                      margin: EdgeInsets.only(left: 15/360*screenWidth, top: 20/720*screenHeight),
                                      child: Text(
                                        MyplanConfirmationScreenString.mode,
                                        style: TextStyle(
                                          color: Color.fromARGB(255, 153, 154, 171),
                                          //  fontSize: 12,
                                          fontFamily: "IBM Plex Sans",
                                          fontWeight: FontWeight.w500,
                                        ),
                                        textAlign: TextAlign.left,
                                      ),
                                    ),
                                    Container(
                                      // width: 290,
                                      margin: EdgeInsets.only(left: 15/360*screenWidth, top: 1/720*screenHeight),
                                      child: Text(
                                        bookingDataBasedOnId[0]['mode'].toString()=="Virtual"?"Virtual":"In-Centre",
                                        style: TextStyle(
                                          color: Color.fromARGB(255, 0, 3, 44),
                                          //fontSize: 12/360*screenWidth,
                                          fontFamily: "IBM Plex Sans",
                                          fontWeight: FontWeight.w500,
                                        ),
                                        textAlign: TextAlign.left,
                                      ),
                                    ),
                                    Container(
                                      margin: EdgeInsets.only(left: 15/360*screenWidth, top: 20/720*screenHeight),
                                      child: Text(
                                        MyplanConfirmationScreenString.MentorReasonforvisit,
                                        style: TextStyle(
                                          color: Color.fromARGB(255, 153, 154, 171),
                                          //  fontSize: 12,
                                          fontFamily: "IBM Plex Sans",
                                          fontWeight: FontWeight.w500,
                                        ),
                                        textAlign: TextAlign.left,
                                      ),
                                    ),
                                    Container(
                                      // width: 290,
                                      margin: EdgeInsets.only(left: 15/360*screenWidth, top: 1/720*screenHeight),
                                      child: Text(
                                        bookingDataBasedOnId[0]['reasonForBooking'].toString(),
                                        style: TextStyle(
                                          color: Color.fromARGB(255, 0, 3, 44),
                                          //fontSize: 12/360*screenWidth,
                                          fontFamily: "IBM Plex Sans",
                                          fontWeight: FontWeight.w500,
                                        ),
                                        textAlign: TextAlign.left,
                                      ),
                                    ),
                                    ( bookingDataBasedOnId[0]['feedBack']!=null && bookingDataBasedOnId[0]['feedBack']!=""&&bookingDataBasedOnId[0]['bookingStatus']=="ATTENDED"&&dateComparision == true)?
                                    Container(
                                      margin: EdgeInsets.only(left: 15/360*screenWidth, top: 20/720*screenHeight),
                                      child: Text(
                                        MyplanConfirmationScreenString.Feedback,
                                        style: TextStyle(
                                          color: Color.fromARGB(255, 153, 154, 171),
                                          // fontSize: 12/360*screenWidth,
                                          fontFamily: "IBM Plex Sans",
                                          fontWeight: FontWeight.w500,
                                        ),
                                        textAlign: TextAlign.left,
                                      ),
                                    ):Container(),
                                    ( bookingDataBasedOnId[0]['feedBack']!=null && bookingDataBasedOnId[0]['feedBack']!=""&&(bookingDataBasedOnId[0]['bookingStatus']=="ATTENDED"&&dateComparision == true||bookingDataBasedOnId[0]['bookingStatus']=="ATTENDED"&&dateComparision == false))?
                                    Container(
                                      //  width: 290/360*screenWidth,
                                      margin: EdgeInsets.only(left: 15/360*screenWidth, top: 1/720*screenHeight),
                                      child: Html(
                                        useRichText: false,
                                        data:bookingDataBasedOnId[0]['feedBack'].toString(),
                                        defaultTextStyle:TextStyle(
                                            color: Color.fromARGB(255, 0, 3, 44),
                                            //   fontSize: 14,
                                            fontFamily: "IBM Plex Sans",
                                            fontWeight: FontWeight.w500),

                                      ),
                                    ):Container(),

                                  ],
                                ),
                              ),
                            ),

                            /// cancel button ................
                            (bookingDataBasedOnId[0]['bookingStatus']=="BOOKED"&&dateComparision == false)?
                            Container(
                              // height: 23/720*screenHeight,
                              alignment: Alignment.center,
                              margin: EdgeInsets.only(top: 38/720*screenHeight),
                              child: FlatButton(
                                onPressed:(){
                                  if (buttonClickOnlyOnce == false) {
                                    setState(() {
                                      buttonClickOnlyOnce = true;
                                      loadForCancelButton=true;
                                    });
                                    cancelBooking();
                                  }
                                },
                                color: Colors.transparent,
                                textColor: Color.fromARGB(255, 126, 145, 154),
                                padding: EdgeInsets.all(0),
                                child: Text(
                                  loadForCancelButton?MyplanConfirmationScreenString.cancelling:MyplanConfirmationScreenString.CancelBooking,
                                  style: TextStyle(
                                    //fontSize: 14,
                                    fontFamily: "IBM Plex Sans",
                                  ),
                                  textAlign: TextAlign.center,
                                ),
                              ),
                            ):(bookingDataBasedOnId[0]['bookingStatus']=="CANCELLED"||bookingDataBasedOnId[0]['bookingStatus']=="CANCELLED AGAINST POLICY")?
                            Container(
                              //  width: 290,
                              margin: EdgeInsets.only(left: 15/360*screenWidth, top: 20/360*screenHeight),
                              child: Text(
                                MyplanConfirmationScreenString.RequestCancelled,
                                style: TextStyle(
                                  color:Color(0xFFff5757),
                                  //  fontSize: 14/360*screenWidth,
                                  fontFamily: "IBM Plex Sans",
                                  fontWeight: FontWeight.w500,
                                ),
                                textAlign: TextAlign.center,
                              ),
                            ):
                            (bookingDataBasedOnId[0]['bookingStatus']=="CANCELLED BY MENTOR")?
                            Container(
                              //  width: 290,
                              margin: EdgeInsets.only(left: 15/360*screenWidth, top: 20/360*screenHeight),
                              child: Text(
                                MyplanConfirmationScreenString.MentorRequestCancelled,
                                style: TextStyle(
                                  color:Color(0xFFff5757),
                                  //  fontSize: 14/360*screenWidth,
                                  fontFamily: "IBM Plex Sans",
                                  fontWeight: FontWeight.w500,
                                ),
                                textAlign: TextAlign.center,
                              ),
                            ):(bookingDataBasedOnId[0]['bookingStatus']=="ABSENT")?
                            Container(
                              // width: 290/,
                              margin: EdgeInsets.only(left: 15/360*screenWidth, top: 20/720*screenHeight),
                              child: Text(
                                MyplanConfirmationScreenString.Absent,
                                style: TextStyle(
                                  color:Color(0xFFff5757),
                                  // fontSize: 14/360*screenWidth,
                                  fontFamily: "IBM Plex Sans",
                                  fontWeight: FontWeight.w500,
                                ),
                                textAlign: TextAlign.center,
                              ),
                            ):(bookingDataBasedOnId[0]['bookingStatus']=="BOOKED"&&dateComparision == true||bookingDataBasedOnId[0]['bookingStatus']=="ATTENDED"&&dateComparision == false||bookingDataBasedOnId[0]['bookingStatus']=="ATTENDED"&&dateComparision == true)?Container():Container(),
                            /// 24 hours msg ............
                            (bookingDataBasedOnId[0]['bookingStatus']=="BOOKED"&&dateComparision == false||bookingDataBasedOnId[0]['bookingStatus']=="CANCELLED AGAINST POLICY")?
                            Container(
                              // width: 290,
                              margin: EdgeInsets.only(left: 15/360*screenWidth, top: 20/720*screenHeight,bottom: 40/720*screenHeight),
                              child: Text(
                                MyplanConfirmationScreenString.MentorBookimgcancelled,
                                style: TextStyle(
                                  color:Color(0xFFff5757),
                                  //  fontSize: 14/360*screenWidth,
                                  fontFamily: "IBM Plex Sans",
                                  fontWeight: FontWeight.w500,
                                ),
                                textAlign: TextAlign.center,
                              ),
                            ):Container(),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              )
                  :Container(
                margin: EdgeInsets.only(
                  bottom: 40 / 720 * screenHeight,
                  top: 150/720 * screenHeight,
                ),
                child: Center(child: ErrorScreen()),
              )
                  : Container(child: Center(child: CircularProgressIndicator(),),); // widget to be shown while data is being loaded from api call method
            },
            future: _future,
          ),
        ),

      ),
    );
  }
}

//dateComparision = true means past date

import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:imsindia/utils/colors.dart';
import 'package:imsindia/utils/emaximiser_svg_icons.dart';
import '../ims_utility.dart';
import 'myPLAN_bookmentor_screen.dart';
import 'myPLAN_bookslot_screen.dart';
import 'myPLAN_home_Screen.dart';
import 'dart:async';
import 'package:imsindia/resources/strings/myplan.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:imsindia/api/bloc/services.dart';
import 'package:imsindia/utils/Url.dart';
import 'package:imsindia/utils/global.dart' as global;
import 'package:fluttertoast/fluttertoast.dart';
import 'package:flutter_mailer/flutter_mailer.dart';


Timer _timer;
int _start = 30;
TextEditingController _controller_otp = new TextEditingController();
int count = 0;
class OtpScreen extends StatefulWidget {
  var branchId;
  var mentorId;
  var slotDate;
  var slotTime;
  var slotType;
  var courseName;
  var reasonForBooking;
  var courseReasonTypeId;
  var otp;
  var mobilenumber;
  var slotID;
  OtpScreen(this.branchId,this.mentorId,this.slotDate,this.slotTime,this.slotID,this.slotType,
      this.courseName,this.reasonForBooking,this.courseReasonTypeId,this.otp,this.mobilenumber);

  @override
  OtpScreenState createState() => OtpScreenState();
}

class OtpScreenState extends State<OtpScreen> {
  bool buttonClickOnlyOnce = false;
  bool loadForVerifyButoon = false;
  var studentImsPin;
  var studentEmail;
  var studentName;
  var courseName_navigationdropdown;
  var otp_number;
  bool enteredOTP = false;
  bool otp_status = false;
  bool _resendOtp =false;
  String invalid_otp;
  String Success_MSG;
  String Already_EXIST;
  int count = 0;
  int resendCount = 1;
  bool showSupportmsg = false;
  bool showedSupportmsg = false;

  var imsPin;
  var imsCourseId;
  var booking;
  void startTimer() {
    setState(() {
      if(_start == 0){
        _start = 30;
      }
    });

    const oneSec = const Duration(seconds: 1);
    _timer = new Timer.periodic(
      oneSec,
          (Timer timer) {
            if (this.mounted) {
              setState(() {
               // _resendOtp = false;
                if (_start < 1) {
                  timer.cancel();
                } else {
                  _start = _start - 1;
                  if(_start == 0){
                   // _resendOtp = false;
                  }
                }
              },);
            }
            }
    );
  }
  void showMessage(String inputValue) {
    Fluttertoast.showToast(
      msg: inputValue,
      toastLength: Toast.LENGTH_SHORT,
      gravity: ToastGravity.BOTTOM,
      timeInSecForIosWeb: 1,
      backgroundColor: PrimaryColors.azure_Dark,
      textColor: Colors.white,
      fontSize: 14.0,
    );
  }
  @override
  void initState() {
    _start = 30;
    _controller_otp.text = '';
    invalid_otp = null;
    Success_MSG = null;
    startTimer();
    super.initState();

  }
  @override
  void dispose() {
    _timer.cancel();
    super.dispose();

  }

  void nextField(String value, FocusNode focusNode) {
    if (value.length == 6) {
      focusNode.requestFocus();
    }
  }



  @override
  Widget build(BuildContext context) {
    final screenHeight = MediaQuery.of(context).size.height;
    final screenWidth = MediaQuery.of(context).size.width;
    final defaultScreenHeight = 720;
    final defaultScreenWidth = 360;

    Future<bool> _onWillPop() {
      Navigator.popUntil(context, (route) {
        return count++ == 2;
      });
    }
    return WillPopScope(
      onWillPop: _onWillPop,
      child: Scaffold(
          backgroundColor: Colors.white,
          appBar: new AppBar(
            brightness: Brightness.light,
            elevation: 0.0,
            centerTitle: false,
            iconTheme: IconThemeData(
              color: Colors.black, //change your color here
            ),
            titleSpacing: 0.0,
            actions: <Widget>[],
            backgroundColor: Color.fromARGB(255, 255, 255, 255),
            leading: GestureDetector(
              onTap: () {
                Navigator.popUntil(context, (route) {
                  return count++ == 2;
                });
                },
              child: Container(
                // color: Colors.white,
                width: (28 / 360) * screenWidth,
                height: (24.0 / 678) * screenHeight,
                margin: EdgeInsets.only(left: 0.0),
                child: SvgPicture.asset(
                  getSvgIcon.backbutton,
                  height: (5 / 678) * screenHeight,
                  width: (14 / 360) * screenWidth,
                  fit: BoxFit.none,
                ),
              ),
            ),
          ),
          body: Stack(
            children: <Widget>[
              SingleChildScrollView(
                child: Column(
                  children: [
                    Container(
                      margin: EdgeInsets.only(left: 20/360 * screenWidth, top: 30/720 * screenHeight ,right: 20/360 * screenWidth),
                      child: Center(
                        child: Text(
                          otpScreenString.EnterOtp,
                          style: TextStyle(
                            color: Color.fromARGB(255, 31, 37, 43),
                            fontSize: 18 / 720 * screenHeight,
                            fontFamily: "IBMPlexSans",
                            fontWeight: FontWeight.w700,
                          ),
                          textAlign: TextAlign.center,
                        ),
                      ),
                    ),
                    Container(
                      height: 1,
                      margin: EdgeInsets.only( top: 20/720 * screenHeight),
                      decoration: BoxDecoration(
                        color: SemanticColors.light_ice_blue,
                      ),
                      child: Container(),
                    ),
                    Container(
                      margin: EdgeInsets.only(left: 20/360 * screenWidth, top: 30/720 * screenHeight,right: 20/360 * screenWidth),
                      child: Align(
                        alignment: Alignment.topLeft,
                        child: Text(
                            otpScreenString.Otp +'${widget.mobilenumber}',
                          style: TextStyle(
                            color: Color.fromARGB(255, 31, 37, 43),
                            fontSize: 16 / 720 * screenHeight,
                            fontFamily: "IBMPlexSans",
                            fontWeight: FontWeight.w400,
                          ),
                          textAlign: TextAlign.left,
                        ),
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(left: 20/360 * screenWidth, top: 10/720 * screenHeight),
                      child: Align(
                        alignment: Alignment.topRight,
                        child: Stack(
                          alignment: const Alignment(0, 0),
                          children: <Widget>[
                            _start == 0
                                  ? (showedSupportmsg == false
                                      ? (resendCount <= 3
                                         ? GestureDetector(
                                                              onTap: () {
                                                                // OTP code resend
                                                                setState(() {
                                                                  resendCount ++ ;
                                                                  _controller_otp.text = '';
                                                                  _resendOtp = true;
                                                                  startTimer();
                                                                  OTPGenerator();
                                                                });
                                                              },
                                                              child: Text(
                                otpScreenString.ResendOtp,
                                style: TextStyle(
                                  color: NeutralColors.grycolor,
                                  fontSize: 12/360*screenWidth,
                                  fontFamily: "IBM Plex Sans",
                                  fontWeight: FontWeight.w500,
                                ),
                              ),
                                                              )
                                         : GestureDetector(
                                        onTap: () {
                                         print("==============show error msg");
                                         print(_resendOtp);
                                         print(_start);
                                         print(invalid_otp);
                                         setState(() {
                                           showedSupportmsg = true;
                                           showSupportmsg = true;
                                         });

                                        },
                                        child: Text(
                                          otpScreenString.ResendOtp,
                                          style: TextStyle(
                                            color: NeutralColors.grycolor,
                                            fontSize: 12/360*screenWidth,
                                            fontFamily: "IBM Plex Sans",
                                            fontWeight: FontWeight.w500,
                                          ),
                                        ),
                                  )
                                        )
                                      : GestureDetector(
                                            onTap: () {
                                              // // OTP code resend
                                              // setState(() {
                                              //   _controller_otp.text = '';
                                              //   _resendOtp = true;
                                              //   startTimer();
                                              //   OTPGenerator();
                                              // });
                                            },
                                            child: Text(
                                otpScreenString.ResendOtp,
                                style: TextStyle(
                                  color: NeutralColors.grycolor,
                                  fontSize: 12/360*screenWidth,
                                  fontFamily: "IBM Plex Sans",
                                  fontWeight: FontWeight.w500,
                                ),
                              ),
                                         )
                                    )
                                : GestureDetector(
                              onTap: () {
                                // // OTP code resend
                                // setState(() {
                                //   _controller_otp.text = '';
                                //   _resendOtp = true;
                                //   startTimer();
                                //   OTPGenerator();
                                // });
                              },
                              child: Text(
                                otpScreenString.ResendOtp,
                                style: TextStyle(
                                  color: GradientColors.blueGrey,
                                  fontSize: 12/360*screenWidth,
                                  fontFamily: "IBM Plex Sans",
                                  fontWeight: FontWeight.w500,
                                ),
                              ),
                            ) ,
                            Container(
                              margin: EdgeInsets.only(left: 140/360 * screenWidth,right: 20/360 * screenWidth),
                              width: 30,
                              height: 30,
                              decoration: BoxDecoration(
                                border: Border.all(width: 1,color: NeutralColors.ice_blue),
                                shape: BoxShape.circle,
                                color: Colors.white,
                              ),
                              child: Center(
                                child: Text(
                                  "$_start",
                                  style: const TextStyle(
                                      color: NeutralColors.blue_grey,
                                      fontWeight: FontWeight.w800,
                                      fontFamily: "IBMPlexSans",
                                      fontStyle: FontStyle.normal,
                                      fontSize: 10
                                  ),
                                ),
                              ),
                            ),
                          ],

                        ),
                      ),
                    ),
                    /***END CONTAINER FOR RESEND BUTTON***/
                    Stack(
                      alignment: const Alignment(0, 0),
                      children: <Widget>[
                        Container(
                          margin: EdgeInsets.only(left: 20/360 * screenWidth, top: 10/720 * screenHeight ,right: 20/360 * screenWidth),
                          child: Theme(
                            data: new ThemeData(
                              primaryColor: NeutralColors.deep_sky_blue,
                              primaryColorDark: NeutralColors.deep_sky_blue,
                            ),
                            child: new TextField(
                              controller: _controller_otp,
                              //obscureText: true,
                              onSubmitted: (text){
                                 otp_number = text;
                                 print("======================clicked done button on keyboard");
                                print(_controller_otp.text);
                                if(_controller_otp.text == null){
                                  enteredOTP = true;
                                }
                                else{
                                  enteredOTP = false;
                                  showSupportmsg = false;
                                  if (buttonClickOnlyOnce == false) {
                                    setState(() {
                                      buttonClickOnlyOnce = true;
                                      loadForVerifyButoon=true;
                                    });
                                    BookSlot();
                                  }
                                }
                              },
                              decoration: new InputDecoration(
                                border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(5.0),
                                  borderSide: BorderSide(
                                    color: NeutralColors.deep_sky_blue,
                                    style: BorderStyle.solid,
                                  ),
                                ),
                                hintText: otpScreenString.EnterOtp,
                                hintStyle:TextStyle(
                                  fontSize: 12 / 720 * screenHeight,
                                ),
                              ),
                            ),
                          ),
                        ),
                        // Padding(
                        //   padding: const EdgeInsets.all(12.0),
                        //   child: Align(
                        //     alignment: Alignment.topRight,
                        //     child: Container(
                        //       margin: EdgeInsets.only(left: 170/360 * screenWidth, top: 10/720 * screenHeight ,right: 20/360 * screenWidth),
                        //       width: 30,
                        //       height: 30,
                        //       decoration: BoxDecoration(
                        //         border: Border.all(width: 1,color: NeutralColors.ice_blue),
                        //         shape: BoxShape.circle,
                        //         color: Colors.white,
                        //       ),
                        //       child: Center(
                        //         child: Text(
                        //           "$_start",
                        //           style: const TextStyle(
                        //               color: NeutralColors.blue_grey,
                        //               fontWeight: FontWeight.w500,
                        //               fontFamily: "IBMPlexSans",
                        //               fontStyle: FontStyle.normal,
                        //               fontSize: 8
                        //           ),
                        //         ),
                        //       ),
                        //     ),
                        //   ),
                        // ),
                      ],
                    ),
                    _start != 0 && _resendOtp  ? Container(
                      margin: EdgeInsets.only(left: 20/360 * screenWidth, top: 10/720 * screenHeight ),
                      child: Align(
                        alignment: Alignment.topLeft,
                        child: Text(
                          otpScreenString.ResendOtp_msg,
                          style: TextStyle(
                            color: NeutralColors.scroll,
                            fontSize: 14 / 720 * screenHeight,
                            fontFamily: "IBMPlexSans",
                            fontWeight: FontWeight.w400,
                          ),
                          textAlign: TextAlign.left,
                        ),
                      ),
                    ) : (showSupportmsg == true ? Container(
                      margin: EdgeInsets.only(left: 20/360 * screenWidth, top: 10/720 * screenHeight,right: 20/360 * screenWidth ),
                      child: RichText(text: TextSpan(
                        children: [
                          TextSpan(
                            text: otpScreenString.supportMsg,
                            style: new TextStyle(
                              color: NeutralColors.grapefruit,
                              fontSize: 12/actualWidth*screenWidth,
                              fontFamily: "IBM Plex Sans",
                            ),
                          ),
                          TextSpan(
                            text: otpScreenString.supportMailId,
                            style: new TextStyle(
                              fontSize: 12/actualWidth*screenWidth,
                              fontFamily: "IBM Plex Sans",
                              color: Color.fromARGB(255, 0, 171, 251),
                            ),
                            recognizer: new TapGestureRecognizer()
                               ..onTap = () {
                                 print("**********************************Click on support");
                                 EmailFunc('support@imsindia.com', 'Query', 'Hello Ims');
                               },
                          ),
                        ]
                      )),
                    ) :Container()),
                    invalid_otp == null  ? Container() : (showSupportmsg == true || _resendOtp == true ? Container() : Container(
                      margin: EdgeInsets.only(left: 20/360 * screenWidth, top: 10/720 * screenHeight ),
                      child: Align(
                        alignment: Alignment.topLeft,
                        child: Text(
                          otpScreenString.ValidEnterOtpTitle,
                          style: TextStyle(
                            color: NeutralColors.grapefruit,
                            fontSize: 12/360*screenWidth,
                            fontFamily: "IBM Plex Sans",
                            fontWeight: FontWeight.w500,
                          ),
                        ) ,
                      ),
                    )),
                     Success_MSG == null ? Container() : Container(
                       margin: EdgeInsets.only(left: 20/360 * screenWidth, top: 10/720 * screenHeight ),
                       child: Align(
                         alignment: Alignment.topLeft,
                         child: Text(
                           otpScreenString.Success_VerifyMSG,
                           style: TextStyle(
                             color: NeutralColors.scroll,
                             fontSize: 12/360*screenWidth,
                             fontFamily: "IBM Plex Sans",
                             fontWeight: FontWeight.w500,
                           ),
                         ) ,
                       ),
                     ),
                     Already_EXIST == null ? Container() : Container(
                       margin: EdgeInsets.only(left: 20/360 * screenWidth, top: 10/720 * screenHeight ),
                       child: Align(
                         alignment: Alignment.topLeft,
                         child: Text(
                           otpScreenString.ALREADY_EXIST,
                           style: TextStyle(
                             color: NeutralColors.scroll,
                             fontSize: 12/360*screenWidth,
                             fontFamily: "IBM Plex Sans",
                             fontWeight: FontWeight.w500,
                           ),
                         ) ,
                       ),
                     ),
                     Container(
                      height: 60/720 * screenHeight,
                      width: 90/360 * screenWidth,
                      margin: EdgeInsets.only(left: 45/360 * screenWidth, top: 20/720 * screenHeight, right: 45/360 * screenWidth),
                      child: Stack(
                        alignment: Alignment.center,
                        children: [
                          Positioned(
                            left: 0,
                            top: 0,
                            right: 0,
                            child: GestureDetector(
                              onTap: () {
                                //MyMentorEightWidget
                                print("======================clicked verify");
                                print(_controller_otp.text);
                                if(_controller_otp.text == null){
                                  enteredOTP = true;
                                }
                                else{
                                  enteredOTP = false;
                                  showSupportmsg = false;
                                  if (buttonClickOnlyOnce == false) {
                                    setState(() {
                                      buttonClickOnlyOnce = true;
                                      loadForVerifyButoon=true;
                                    });
                                    BookSlot();
                                  }
                                }
                              },
                              child: Container(
                                height: 40,
                                decoration: BoxDecoration(
                                  gradient: LinearGradient(
                                    begin: Alignment(-0.011, 0.494),
                                    end: Alignment(1.031, 0.516),
                                    stops: [
                                      0,
                                      1,
                                    ],
                                    colors: [
                                      Color.fromARGB(255, 51, 128, 204),
                                      Color.fromARGB(255, 137, 110, 216),
                                    ],
                                  ),
                                  borderRadius: BorderRadius.all(Radius.circular(2)),
                                ),
                                child: Column(
                                  children: [
                                    Container(
                                      margin: EdgeInsets.only(top: 11),
                                      child: Text(
                                        loadForVerifyButoon?otpScreenString.Verifying:otpScreenString.Verify,
                                        style: TextStyle(
                                          color: Color.fromARGB(255, 255, 255, 255),
                                          fontSize: 14,
                                          fontFamily: "IBM Plex Sans",
                                          fontWeight: FontWeight.w500,
                                        ),
                                        textAlign: TextAlign.center,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ],
          )),
    );
  }

  //API FOR OTP GENERATOR
  void OTPGenerator()async{
    Map<String, dynamic> postdata_otpgen;
    SharedPreferences prefs = await SharedPreferences.getInstance();
    studentImsPin = prefs.getString(ImsStrings.sCurrentUserId);
    studentEmail = prefs.getString(ImsStrings.sCurrentUserEmail);
    studentName = prefs.getString("_studentName");
    postdata_otpgen = {
      "studentImsPin":studentImsPin
    };
    ApiService().postAPITerr(URL.myPLAN_API_OTPGenerator, postdata_otpgen, global.headers).then((result) {
      setState(() {
        print("===========================print resendcount");
        print(resendCount);
        if(result[0]['success'].toString().toLowerCase() == 'true'.toLowerCase()){
          print(result[0]['success'].toString());
          showMessage(result[0]['message'].toString());
        }else{
        }
      });
    });
  }
  // get avaliable slots
  void bookingsAvailable() async{
    print("********************************inside booking avaliable");
    SharedPreferences prefs = await SharedPreferences.getInstance();
    imsCourseId = prefs.getString("courseId");
    imsPin = prefs.getString(ImsStrings.sCurrentUserId);
    Map<String, dynamic> postData;
    postData = {
      "studentImsPin":imsPin,//"99a77771",
      "myImsCourseId":imsCourseId //"500"
    };
    print("================================ZZZZZZZZZZZZZZZZZZZZZZZZZZZZZ");
    print(postData);
    ApiService().postAPITerr(URL.myPLAN_API_BOOKINGS_AVAILABLE, postData, global.headers).then((result)
    {
      print(URL.myPLAN_API_BOOKINGS_AVAILABLE);
      if (result[0]['success'].toString().toLowerCase() == 'true'.toLowerCase()) {
        booking = result[0]['data'];
        loadForVerifyButoon = false;
        buttonClickOnlyOnce = false;
        print("********************************before navigation");
        Navigator.pop(context);
        Navigator.push(context, MaterialPageRoute(builder: (context) => myPLANHome(false,booking[0]['availableSlots'],booking[0]['slotsUsed'])));
      }
      else{
        print("showError Message");
        if(result[0].containsKey("message")){
           if(result[0]['message']=='available bookings are Empty'){
             loadForVerifyButoon = false;
             buttonClickOnlyOnce = false;
            Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) =>
                      myPLANHome(false,0,1),
                ));
          }else{
             loadForVerifyButoon = false;
             buttonClickOnlyOnce = false;
            print("Error Handling");
          }
        }else{
          loadForVerifyButoon = false;
          buttonClickOnlyOnce = false;
          print("Error Handling");
        }

      }
    });
  }
  //API FOR VERIFY OTP
  void BookSlot()async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    studentImsPin = prefs.getString(ImsStrings.sCurrentUserId);
    studentEmail = prefs.getString(ImsStrings.sCurrentUserEmail);
    studentName = prefs.getString("_studentName");
    courseName_navigationdropdown = prefs.getString('courseName');
    Map<String, dynamic> postdata_bookslot;
    postdata_bookslot = {
      "branchId":widget.branchId,
      "mentorId":widget.mentorId,
      "slotDate":widget.slotDate,
      "slotTime":widget.slotTime,
      "slotId" : widget.slotID,
      "imsPin":studentImsPin,
      "studentName":studentName,
      "studentEmail":studentEmail,
      "slotType":widget.slotType,
      "courseName":courseName_navigationdropdown,
      "reasonForBooking":widget.reasonForBooking,
      "courseReasonTypeId":widget.courseReasonTypeId,
      "otp":_controller_otp.text
    };
    print("===============================================verify");
    print(postdata_bookslot);
    print(_controller_otp.text);
    print(buttonClickOnlyOnce);
    ApiService().postAPITerr(URL.myPLAN_BOOKSLOT, postdata_bookslot, global.headers).then((result) {
          print("result result");
          print(result);
          setState(() {
            if (result[0]['success'].toString().toLowerCase() == 'true'.toLowerCase()) {
              print("========================================coming if");
              print(result[0]['success'].toString());
              showMessage(result[0]['message'].toString());
              Success_MSG = result[0]['message'].toString();
              print("********************************coming before booking avaliable ");
              bookingsAvailable();
            }
            else {
              print("========================================coming elseif");
              if(result[0]['message'].toString() =="StudentBooking ::: $studentImsPin already exist"){
                print("coming here");
                Already_EXIST = "Sorry!! This slot has been taken up. Please book the session for another time-slot available.";
                bookingsAvailable();
              }
              print("=====================================goes to else");
              print(resendCount);
              print(showSupportmsg);
              print(_resendOtp);
              Success_MSG = null;
              loadForVerifyButoon = false;
              buttonClickOnlyOnce = false;
              _resendOtp = false;
              invalid_otp = result[0]['message'].toString();
              // showMessage(result[0]['message'].toString());
              if(result[0]['message'].toString() =="Please enter correct OTP to validate"){
                // Please Enter Valid OTP
              }

              print(result[0]['message'].toString());
            }
          });
    });
  }
  //Redirect to mail app
  void EmailFunc(String toMailId, String subject, String body) async {
    MailOptions mailOptions = MailOptions(
      body: body,
      subject: subject,
      recipients: [toMailId],
      isHTML: true,
    );

    await FlutterMailer.send(mailOptions);
  }
}

import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/material.dart';
import 'package:imsindia/utils/emaximiser_svg_icons.dart';

import '../ims_utility.dart';

class MeetingConfirmation_2_w extends StatelessWidget {
  void onCancelRequestPressed(BuildContext context) =>
      Navigator.pop(context, false);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        brightness: Brightness.light,
        elevation: 0.0,
        centerTitle: false,
        titleSpacing: 0.0,
        backgroundColor: Colors.white,
        title: Text(
          "Meeting1",
          style: const TextStyle(
              color: Colors.black,
              fontWeight: FontWeight.w500,
              fontFamily: "IBMPlexSans",
              fontStyle: FontStyle.normal,
              fontSize: 16.0),
        ),
        leading: IconButton(
          icon: getSvgIcon.backSvgIcon,
          onPressed: () => Navigator.pop(context, false),
        ),
      ),
      body: Container(
        constraints: BoxConstraints.expand(),
        decoration: BoxDecoration(
          color: Color.fromARGB(255, 255, 255, 255),
        ),
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              Container(
                margin: EdgeInsets.only(left: 20, top: 30, right: 20),
                child: DottedBorder(
                  dashPattern: [6, 3, 6, 3],
                  borderType: BorderType.RRect,
                  radius: Radius.circular(5),
                  padding: EdgeInsets.only(bottom: 20.0),
                  color: ImsColors.gunmetal,
                  child: ClipRRect(
                    borderRadius: BorderRadius.all(Radius.circular(5)),
                    child: Container(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(
                            margin: EdgeInsets.only(left: 15, top: 15),
                            child: Text(
                              "Mentor Name",
                              style: TextStyle(
                                color: Color.fromARGB(255, 153, 154, 171),
                                fontSize: 12,
                                fontFamily: "IBM Plex Sans",
                                fontWeight: FontWeight.w500,
                              ),
                              textAlign: TextAlign.left,
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.only(left: 15, top: 1),
                            child: Text(
                              "Mr. Shivank Verma",
                              style: TextStyle(
                                color: Color.fromARGB(255, 0, 3, 44),
                                fontSize: 14,
                                fontFamily: "IBM Plex Sans",
                                fontWeight: FontWeight.w500,
                              ),
                              textAlign: TextAlign.left,
                            ),
                          ),
                          Container(
                            width: 88,
                            height: 41,
                            margin: EdgeInsets.only(left: 15, top: 22),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  "Date",
                                  style: TextStyle(
                                    color: Color.fromARGB(255, 153, 154, 171),
                                    fontSize: 12,
                                    fontFamily: "IBM Plex Sans",
                                    fontWeight: FontWeight.w500,
                                  ),
                                  textAlign: TextAlign.left,
                                ),
                                Container(
                                  margin: EdgeInsets.only(top: 1),
                                  child: Text(
                                    "02-07-2019",
                                    style: TextStyle(
                                      color: Color.fromARGB(255, 0, 3, 44),
                                      fontSize: 14,
                                      fontFamily: "IBM Plex Sans",
                                      fontWeight: FontWeight.w500,
                                    ),
                                    textAlign: TextAlign.left,
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.only(left: 15, top: 20),
                            child: Text(
                              "Topic",
                              style: TextStyle(
                                color: Color.fromARGB(255, 153, 154, 171),
                                fontSize: 12,
                                fontFamily: "IBM Plex Sans",
                                fontWeight: FontWeight.w500,
                              ),
                              textAlign: TextAlign.left,
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.only(left: 15, top: 1),
                            child: Text(
                              "Verbal Ability & Reading Comprehension",
                              style: TextStyle(
                                color: Color.fromARGB(255, 0, 3, 44),
                                fontSize: 14,
                                fontFamily: "IBM Plex Sans",
                                fontWeight: FontWeight.w500,
                              ),
                              textAlign: TextAlign.left,
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.only(left: 15, top: 22),
                            child: Text(
                              "Your Request",
                              style: TextStyle(
                                color: Color.fromARGB(255, 153, 154, 171),
                                fontSize: 12,
                                fontFamily: "IBM Plex Sans",
                                fontWeight: FontWeight.w500,
                              ),
                              textAlign: TextAlign.left,
                            ),
                          ),
                          Container(
                            width: 290,
                            margin: EdgeInsets.only(left: 15, top: 1),
                            child: Text(
                              "My scores are decent. I want to get into the top B-schools and need to discuss how to go ahead.",
                              style: TextStyle(
                                color: Color.fromARGB(255, 0, 3, 44),
                                fontSize: 14,
                                fontFamily: "IBM Plex Sans",
                                fontWeight: FontWeight.w500,
                              ),
                              textAlign: TextAlign.left,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
              Container(
                height: 23,
                alignment: Alignment.center,
                margin: EdgeInsets.only(
                  top: 38,
                ),
                child: FlatButton(
                  onPressed: () => this.onCancelRequestPressed(context),
                  color: Colors.transparent,
                  textColor: Color.fromARGB(255, 126, 145, 154),
                  padding: EdgeInsets.all(0),
                  child: Text(
                    "Cancel Request",
                    style: TextStyle(
                      fontSize: 14,
                      fontFamily: "IBM Plex Sans",
                    ),
                    textAlign: TextAlign.center,
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:imsindia/utils/svg_images/error_screen_svg_images.dart';
import 'package:imsindia/views/Arun/ims_utility.dart';
import 'package:imsindia/utils/emaximiser_svg_icons.dart';


import '../../GK_Zone/gk_zone_current_affairs_tab.dart';
import 'myPLAN_home_Screen.dart';


var screenWidthTotal;
var screenHeightTotal;
var _safeAreaHorizontal;
var _safeAreaVertical;
var screenHeight;
var screenWidth;

class myPlanErrorScreen extends StatefulWidget {
  @override
  _myPlanErrorScreenState createState() => _myPlanErrorScreenState();
}

class _myPlanErrorScreenState extends State<myPlanErrorScreen> {
  @override
  Widget build(BuildContext context) {
    var _mediaQueryData = MediaQuery.of(context);
    screenWidthTotal = _mediaQueryData.size.width;
    screenHeightTotal = _mediaQueryData.size.height;
    _safeAreaHorizontal =
        _mediaQueryData.padding.left + _mediaQueryData.padding.right;
    _safeAreaVertical =
        _mediaQueryData.padding.top + _mediaQueryData.padding.bottom;

    screenHeight = screenHeightTotal - _safeAreaVertical;
    screenWidth = screenWidthTotal - _safeAreaHorizontal;
    return Scaffold(
      appBar: AppBar(
        brightness: Brightness.light,
        elevation: 0.0,
        centerTitle: false,
        titleSpacing: 0.0,
        backgroundColor: Colors.white,

        leading: IconButton(
          icon: getSvgIcon.backSvgIcon,
          onPressed: () =>
              Navigator.pop(context, true),
        ),
      ),
      backgroundColor: Colors.white,
      body: Center(
        child: Container(
          child:Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                margin: EdgeInsets.only(
                    left: 18 / 360 * screenWidth,
                    top: 12.5 / 720 * screenHeight),
                child: SvgPicture.asset(
                  ErrorScreenAssets.errorScreenImg,
                  fit: BoxFit.fill,
                ),
              ),
              Container(
                margin: EdgeInsets.only(
                    top: 20 / 720 * screenHeight),
                child: Text("There is no Slots Available",
                  style: TextStyle(color: ImsColors.dark_navy_blue,
                    fontFamily: "IBMPlexSans",
                    fontStyle: FontStyle.normal,
                    fontSize: 16.0,
                    fontWeight: FontWeight.w700,),
                ),

              ),
              // Container(
              //   margin: EdgeInsets.only(
              //     top: 20 / 720 * screenHeight,
              //   ),
              //   child: InkWell(
              //     onTap: (){
              //
              //     },
              //     child: Text("Try Again",
              //       style: TextStyle(color: ImsColors.azure,
              //         fontFamily: "IBMPlexSans",
              //         fontStyle: FontStyle.normal,
              //         fontSize: 14.0,
              //         fontWeight: FontWeight.w700,),
              //     ),
              //   ),
              //
              // ),
            ],
          ) ,
        ),
      ),
    );

  }
}

import 'package:flutter/material.dart';

import '2_w_book_a_mentor.dart';

class MyMentorSevenWidget extends StatelessWidget {
  void onMyMENTORPressed(BuildContext context) => Navigator.push(
      context, MaterialPageRoute(builder: (context) => BookAMentor_2_w()));

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        constraints: BoxConstraints.expand(),
        decoration: BoxDecoration(
          color: Color.fromARGB(255, 255, 255, 255),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Container(
              height: 57,
              margin: EdgeInsets.only(left: 8, top: 5, right: 8),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  Align(
                    alignment: Alignment.topLeft,
                    child: Container(
                      width: 18,
                      height: 14,
                      margin: EdgeInsets.only(left: 12, top: 41),
                      child: Image.asset(
                        "assets/images/hamburger.png",
                        fit: BoxFit.none,
                      ),
                    ),
                  ),
                  Container(
                    width: 87,
                    height: 20,
                    margin: EdgeInsets.only(left: 20),
                    child: FlatButton(
                      onPressed: () => this.onMyMENTORPressed(context),
                      color: Colors.transparent,
                      textColor: Color.fromARGB(255, 0, 0, 0),
                      padding: EdgeInsets.all(0),
                      child: Text(
                        "myMENTOR",
                        style: TextStyle(
                          fontSize: 16,
                          fontFamily: "IBM Plex Sans",
                          fontWeight: FontWeight.w500,
                        ),
                        textAlign: TextAlign.left,
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Container(
              height: 36,
              margin: EdgeInsets.only(top: 30),
              decoration: BoxDecoration(
                color: Color.fromARGB(255, 255, 255, 255),
                boxShadow: [
                  BoxShadow(
                    color: Color.fromARGB(14, 104, 104, 104),
                    offset: Offset(0, 8),
                    blurRadius: 16,
                  ),
                ],
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  Container(
                    height: 18,
                    margin: EdgeInsets.only(left: 55, top: 10, right: 53),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: [
                        Align(
                          alignment: Alignment.topLeft,
                          child: Text(
                            "Meet a Mentor",
                            style: TextStyle(
                              color: Color.fromARGB(255, 153, 154, 171),
                              fontSize: 14,
                              fontFamily: "IBM Plex Sans",
                            ),
                            textAlign: TextAlign.left,
                          ),
                        ),
                        Spacer(),
                        Align(
                          alignment: Alignment.topLeft,
                          child: Text(
                            "Write to Mentor",
                            style: TextStyle(
                              color: Color.fromARGB(255, 86, 72, 235),
                              fontSize: 14,
                              fontFamily: "IBM Plex Sans",
                              fontWeight: FontWeight.w500,
                            ),
                            textAlign: TextAlign.left,
                          ),
                        ),
                      ],
                    ),
                  ),
                  Align(
                    alignment: Alignment.topRight,
                    child: Container(
                      width: 131,
                      height: 2,
                      margin: EdgeInsets.only(top: 6, right: 37),
                      decoration: BoxDecoration(
                        color: Color.fromARGB(255, 86, 72, 235),
                      ),
                      child: Container(),
                    ),
                  ),
                ],
              ),
            ),
            Align(
              alignment: Alignment.topCenter,
              child: Container(
                margin: EdgeInsets.only(top: 53),
                child: Text(
                  "Send request to discuss\nyour progress with a mentor",
                  style: TextStyle(
                    color: Color.fromARGB(255, 0, 3, 44),
                    fontSize: 14,
                    fontFamily: "IBM Plex Sans",
                  ),
                  textAlign: TextAlign.center,
                ),
              ),
            ),
            Align(
              alignment: Alignment.topCenter,
              child: Container(
                width: 270,
                height: 40,
                margin: EdgeInsets.only(top: 22),
                decoration: BoxDecoration(
                  gradient: LinearGradient(
                    begin: Alignment(-0.011, 0.494),
                    end: Alignment(1.031, 0.516),
                    stops: [
                      0,
                      1,
                    ],
                    colors: [
                      Color.fromARGB(255, 51, 128, 204),
                      Color.fromARGB(255, 137, 110, 216),
                    ],
                  ),
                  borderRadius: BorderRadius.all(Radius.circular(2)),
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      margin: EdgeInsets.only(left: 84, top: 11),
                      child: Text(
                        "SEND REQUEST",
                        style: TextStyle(
                          color: Color.fromARGB(255, 255, 255, 255),
                          fontSize: 14,
                          fontFamily: "IBM Plex Sans",
                          fontWeight: FontWeight.w500,
                        ),
                        textAlign: TextAlign.center,
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Align(
              alignment: Alignment.topLeft,
              child: Container(
                margin: EdgeInsets.only(left: 20, top: 60),
                child: Text(
                  "Queries",
                  style: TextStyle(
                    color: Color.fromARGB(255, 0, 3, 44),
                    fontSize: 18,
                    fontFamily: "IBM Plex Sans",
                    fontWeight: FontWeight.w700,
                  ),
                  textAlign: TextAlign.left,
                ),
              ),
            ),
            Expanded(
              flex: 1,
              child: Container(
                margin: EdgeInsets.all(20),
                child: Stack(
                  alignment: Alignment.center,
                  children: [
                    Positioned(
                      left: 0,
                      top: 0,
                      right: 0,
                      child: Container(
                        height: 110,
                        child: Image.asset(
                          "assets/images/mentormask.png",
                          fit: BoxFit.cover,
                        ),
                      ),
                    ),
                    Positioned(
                      left: 22,
                      top: 5,
                      right: 5,
                      child: Container(
                        height: 88,
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          children: [
                            Align(
                              alignment: Alignment.topLeft,
                              child: Container(
                                margin: EdgeInsets.only(top: 25),
                                child: Text(
                                  "24",
                                  style: TextStyle(
                                    color: Color.fromARGB(255, 0, 3, 44),
                                    fontSize: 36,
                                    fontFamily: "IBM Plex Sans",
                                    fontWeight: FontWeight.w500,
                                  ),
                                  textAlign: TextAlign.left,
                                ),
                              ),
                            ),
                            Align(
                              alignment: Alignment.topLeft,
                              child: Container(
                                margin: EdgeInsets.only(left: 18, top: 15),
                                child: Opacity(
                                  opacity: 0.1,
                                  child: Container(
                                    width: 1,
                                    height: 69,
                                    decoration: BoxDecoration(
                                      color: Color.fromARGB(255, 87, 93, 96),
                                    ),
                                    child: Container(),
                                  ),
                                ),
                              ),
                            ),
                            Align(
                              alignment: Alignment.topLeft,
                              child: Container(
                                width: 180,
                                height: 80,
                                margin: EdgeInsets.only(left: 15, top: 8),
                                child: Column(
                                  crossAxisAlignment:
                                      CrossAxisAlignment.stretch,
                                  children: [
                                    Align(
                                      alignment: Alignment.topLeft,
                                      child: Text(
                                        "Mr. Shivank Verma",
                                        style: TextStyle(
                                          color:
                                              Color.fromARGB(255, 31, 37, 43),
                                          fontSize: 18,
                                          fontFamily: "IBM Plex Sans",
                                          fontWeight: FontWeight.w500,
                                        ),
                                        textAlign: TextAlign.left,
                                      ),
                                    ),
                                    Align(
                                      alignment: Alignment.topLeft,
                                      child: Container(
                                        width: 146,
                                        height: 15,
                                        margin: EdgeInsets.only(top: 14),
                                        child: Row(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.stretch,
                                          children: [
                                            Align(
                                              alignment: Alignment.topLeft,
                                              child: Text(
                                                "Location",
                                                style: TextStyle(
                                                  color: Color.fromARGB(
                                                      255, 153, 154, 171),
                                                  fontSize: 12,
                                                  fontFamily: "IBM Plex Sans",
                                                  fontWeight: FontWeight.w500,
                                                ),
                                                textAlign: TextAlign.left,
                                              ),
                                            ),
                                            Align(
                                              alignment: Alignment.topLeft,
                                              child: Container(
                                                margin:
                                                    EdgeInsets.only(left: 71),
                                                child: Text(
                                                  "Time",
                                                  style: TextStyle(
                                                    color: Color.fromARGB(
                                                        255, 153, 154, 171),
                                                    fontSize: 12,
                                                    fontFamily: "IBM Plex Sans",
                                                    fontWeight: FontWeight.w500,
                                                  ),
                                                  textAlign: TextAlign.left,
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                    Align(
                                      alignment: Alignment.topLeft,
                                      child: Container(
                                        width: 180,
                                        height: 23,
                                        margin: EdgeInsets.only(top: 1),
                                        child: Row(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.stretch,
                                          children: [
                                            Align(
                                              alignment: Alignment.topLeft,
                                              child: Text(
                                                "Vallabh Vidy...",
                                                style: TextStyle(
                                                  color: Color.fromARGB(
                                                      255, 0, 3, 44),
                                                  fontSize: 14,
                                                  fontFamily: "IBM Plex Sans",
                                                  fontWeight: FontWeight.w500,
                                                ),
                                                textAlign: TextAlign.left,
                                              ),
                                            ),
                                            Align(
                                              alignment: Alignment.topLeft,
                                              child: Container(
                                                margin:
                                                    EdgeInsets.only(left: 27),
                                                child: Text(
                                                  "10:00 AM",
                                                  style: TextStyle(
                                                    color: Color.fromARGB(
                                                        255, 0, 3, 44),
                                                    fontSize: 14,
                                                    fontFamily: "IBM Plex Sans",
                                                    fontWeight: FontWeight.w500,
                                                  ),
                                                  textAlign: TextAlign.left,
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            Spacer(),
                            Align(
                              alignment: Alignment.topLeft,
                              child: Container(
                                width: 30,
                                height: 20,
                                decoration: BoxDecoration(
                                  color: Color.fromARGB(26, 0, 3, 44),
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(10)),
                                ),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.end,
                                  children: [
                                    Container(
                                      margin: EdgeInsets.only(top: 1, right: 5),
                                      child: Text(
                                        "1/3",
                                        style: TextStyle(
                                          color:
                                              Color.fromARGB(255, 87, 93, 96),
                                          fontSize: 14,
                                          fontFamily: "IBM Plex Sans",
                                          fontWeight: FontWeight.w500,
                                        ),
                                        textAlign: TextAlign.left,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    Positioned(
                      left: 17,
                      top: 20,
                      bottom: 20,
                      child: Container(
                        width: 54,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          children: [
                            Align(
                              alignment: Alignment.topLeft,
                              child: Container(
                                margin: EdgeInsets.only(left: 17),
                                child: Text(
                                  "Tue",
                                  style: TextStyle(
                                    color: Color.fromARGB(255, 153, 154, 171),
                                    fontSize: 12,
                                    fontFamily: "IBM Plex Sans",
                                    fontWeight: FontWeight.w500,
                                  ),
                                  textAlign: TextAlign.left,
                                ),
                              ),
                            ),
                            Expanded(
                              flex: 1,
                              child: Align(
                                alignment: Alignment.topLeft,
                                child: Container(
                                  margin: EdgeInsets.only(top: 40),
                                  child: Text(
                                    "Nov 2018",
                                    style: TextStyle(
                                      color: Color.fromARGB(255, 153, 154, 171),
                                      fontSize: 12,
                                      fontFamily: "IBM Plex Sans",
                                      fontWeight: FontWeight.w500,
                                    ),
                                    textAlign: TextAlign.left,
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Expanded(
              flex: 1,
              child: Container(
                margin: EdgeInsets.all(20),
                child: Stack(
                  alignment: Alignment.center,
                  children: [
                    Positioned(
                      left: 0,
                      top: 0,
                      right: 0,
                      child: Container(
                        height: 110,
                        child: Opacity(
                          opacity: 0.2,
                          child: Image.asset(
                            "assets/images/mentormask-2.png",
                            fit: BoxFit.cover,
                          ),
                        ),
                      ),
                    ),
                    Positioned(
                      left: 22,
                      top: 5,
                      right: 5,
                      child: Container(
                        height: 88,
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          children: [
                            Align(
                              alignment: Alignment.topLeft,
                              child: Container(
                                margin: EdgeInsets.only(top: 25),
                                child: Text(
                                  "05",
                                  style: TextStyle(
                                    color: Color.fromARGB(255, 0, 3, 44),
                                    fontSize: 36,
                                    fontFamily: "IBM Plex Sans",
                                    fontWeight: FontWeight.w500,
                                  ),
                                  textAlign: TextAlign.left,
                                ),
                              ),
                            ),
                            Align(
                              alignment: Alignment.topLeft,
                              child: Container(
                                margin: EdgeInsets.only(left: 18, top: 15),
                                child: Opacity(
                                  opacity: 0.2,
                                  child: Container(
                                    width: 1,
                                    height: 69,
                                    decoration: BoxDecoration(
                                      color: Color.fromARGB(255, 87, 93, 96),
                                    ),
                                    child: Container(),
                                  ),
                                ),
                              ),
                            ),
                            Align(
                              alignment: Alignment.topLeft,
                              child: Container(
                                width: 180,
                                height: 80,
                                margin: EdgeInsets.only(left: 15, top: 8),
                                child: Column(
                                  crossAxisAlignment:
                                      CrossAxisAlignment.stretch,
                                  children: [
                                    Align(
                                      alignment: Alignment.topLeft,
                                      child: Text(
                                        "Mr. Krunal Desai",
                                        style: TextStyle(
                                          color: Color.fromARGB(255, 0, 3, 44),
                                          fontSize: 18,
                                          fontFamily: "IBM Plex Sans",
                                          fontWeight: FontWeight.w500,
                                        ),
                                        textAlign: TextAlign.left,
                                      ),
                                    ),
                                    Align(
                                      alignment: Alignment.topLeft,
                                      child: Container(
                                        width: 146,
                                        height: 15,
                                        margin: EdgeInsets.only(top: 14),
                                        child: Row(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.stretch,
                                          children: [
                                            Align(
                                              alignment: Alignment.topLeft,
                                              child: Text(
                                                "Location",
                                                style: TextStyle(
                                                  color: Color.fromARGB(
                                                      255, 153, 154, 171),
                                                  fontSize: 12,
                                                  fontFamily: "IBM Plex Sans",
                                                  fontWeight: FontWeight.w500,
                                                ),
                                                textAlign: TextAlign.left,
                                              ),
                                            ),
                                            Align(
                                              alignment: Alignment.topLeft,
                                              child: Container(
                                                margin:
                                                    EdgeInsets.only(left: 71),
                                                child: Text(
                                                  "Time",
                                                  style: TextStyle(
                                                    color: Color.fromARGB(
                                                        255, 153, 154, 171),
                                                    fontSize: 12,
                                                    fontFamily: "IBM Plex Sans",
                                                    fontWeight: FontWeight.w500,
                                                  ),
                                                  textAlign: TextAlign.left,
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                    Align(
                                      alignment: Alignment.topLeft,
                                      child: Container(
                                        width: 180,
                                        height: 23,
                                        margin: EdgeInsets.only(top: 1),
                                        child: Row(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.stretch,
                                          children: [
                                            Align(
                                              alignment: Alignment.topLeft,
                                              child: Text(
                                                "Thane",
                                                style: TextStyle(
                                                  color: Color.fromARGB(
                                                      255, 0, 3, 44),
                                                  fontSize: 14,
                                                  fontFamily: "IBM Plex Sans",
                                                  fontWeight: FontWeight.w500,
                                                ),
                                                textAlign: TextAlign.left,
                                              ),
                                            ),
                                            Align(
                                              alignment: Alignment.topLeft,
                                              child: Container(
                                                margin:
                                                    EdgeInsets.only(left: 78),
                                                child: Text(
                                                  "12:30 PM",
                                                  style: TextStyle(
                                                    color: Color.fromARGB(
                                                        255, 0, 3, 44),
                                                    fontSize: 14,
                                                    fontFamily: "IBM Plex Sans",
                                                    fontWeight: FontWeight.w500,
                                                  ),
                                                  textAlign: TextAlign.left,
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            Spacer(),
                            Align(
                              alignment: Alignment.topLeft,
                              child: Container(
                                width: 30,
                                height: 20,
                                decoration: BoxDecoration(
                                  color: Color.fromARGB(26, 0, 3, 44),
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(10)),
                                ),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.end,
                                  children: [
                                    Container(
                                      margin: EdgeInsets.only(top: 1, right: 5),
                                      child: Text(
                                        "2/3",
                                        style: TextStyle(
                                          color:
                                              Color.fromARGB(255, 87, 93, 96),
                                          fontSize: 14,
                                          fontFamily: "IBM Plex Sans",
                                          fontWeight: FontWeight.w500,
                                        ),
                                        textAlign: TextAlign.left,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    Positioned(
                      left: 17,
                      top: 20,
                      bottom: 20,
                      child: Container(
                        width: 52,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          children: [
                            Align(
                              alignment: Alignment.topLeft,
                              child: Container(
                                margin: EdgeInsets.only(left: 17),
                                child: Text(
                                  "Fri",
                                  style: TextStyle(
                                    color: Color.fromARGB(255, 153, 154, 171),
                                    fontSize: 12,
                                    fontFamily: "IBM Plex Sans",
                                    fontWeight: FontWeight.w500,
                                  ),
                                  textAlign: TextAlign.left,
                                ),
                              ),
                            ),
                            Expanded(
                              flex: 1,
                              child: Align(
                                alignment: Alignment.topLeft,
                                child: Container(
                                  margin: EdgeInsets.only(top: 40),
                                  child: Text(
                                    "Feb 2018",
                                    style: TextStyle(
                                      color: Color.fromARGB(255, 153, 154, 171),
                                      fontSize: 12,
                                      fontFamily: "IBM Plex Sans",
                                      fontWeight: FontWeight.w500,
                                    ),
                                    textAlign: TextAlign.left,
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Expanded(
              flex: 1,
              child: Container(
                margin: EdgeInsets.only(left: 20, top: 20, right: 20),
                child: Stack(
                  alignment: Alignment.center,
                  children: [
                    Positioned(
                      left: 0,
                      top: 0,
                      right: 0,
                      child: Container(
                        height: 110,
                        child: Opacity(
                          opacity: 0.2,
                          child: Image.asset(
                            "assets/images/mentormask-2.png",
                            fit: BoxFit.cover,
                          ),
                        ),
                      ),
                    ),
                    Positioned(
                      left: 22,
                      top: 5,
                      right: 5,
                      child: Container(
                        height: 88,
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          children: [
                            Align(
                              alignment: Alignment.topLeft,
                              child: Container(
                                margin: EdgeInsets.only(top: 25),
                                child: Text(
                                  "17",
                                  style: TextStyle(
                                    color: Color.fromARGB(255, 0, 3, 44),
                                    fontSize: 36,
                                    fontFamily: "IBM Plex Sans",
                                    fontWeight: FontWeight.w500,
                                  ),
                                  textAlign: TextAlign.left,
                                ),
                              ),
                            ),
                            Align(
                              alignment: Alignment.topLeft,
                              child: Container(
                                margin: EdgeInsets.only(left: 18, top: 15),
                                child: Opacity(
                                  opacity: 0.2,
                                  child: Container(
                                    width: 1,
                                    height: 69,
                                    decoration: BoxDecoration(
                                      color: Color.fromARGB(255, 87, 93, 96),
                                    ),
                                    child: Container(),
                                  ),
                                ),
                              ),
                            ),
                            Align(
                              alignment: Alignment.topLeft,
                              child: Container(
                                width: 180,
                                height: 80,
                                margin: EdgeInsets.only(left: 15, top: 8),
                                child: Column(
                                  crossAxisAlignment:
                                      CrossAxisAlignment.stretch,
                                  children: [
                                    Align(
                                      alignment: Alignment.topLeft,
                                      child: Text(
                                        "Mr. Tejas More",
                                        style: TextStyle(
                                          color: Color.fromARGB(255, 0, 3, 44),
                                          fontSize: 18,
                                          fontFamily: "IBM Plex Sans",
                                          fontWeight: FontWeight.w500,
                                        ),
                                        textAlign: TextAlign.left,
                                      ),
                                    ),
                                    Align(
                                      alignment: Alignment.topLeft,
                                      child: Container(
                                        width: 146,
                                        height: 15,
                                        margin: EdgeInsets.only(top: 14),
                                        child: Row(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.stretch,
                                          children: [
                                            Align(
                                              alignment: Alignment.topLeft,
                                              child: Text(
                                                "Location",
                                                style: TextStyle(
                                                  color: Color.fromARGB(
                                                      255, 153, 154, 171),
                                                  fontSize: 12,
                                                  fontFamily: "IBM Plex Sans",
                                                  fontWeight: FontWeight.w500,
                                                ),
                                                textAlign: TextAlign.left,
                                              ),
                                            ),
                                            Align(
                                              alignment: Alignment.topLeft,
                                              child: Container(
                                                margin:
                                                    EdgeInsets.only(left: 71),
                                                child: Text(
                                                  "Time",
                                                  style: TextStyle(
                                                    color: Color.fromARGB(
                                                        255, 153, 154, 171),
                                                    fontSize: 12,
                                                    fontFamily: "IBM Plex Sans",
                                                    fontWeight: FontWeight.w500,
                                                  ),
                                                  textAlign: TextAlign.left,
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                    Align(
                                      alignment: Alignment.topLeft,
                                      child: Container(
                                        width: 180,
                                        height: 23,
                                        margin: EdgeInsets.only(top: 1),
                                        child: Row(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.stretch,
                                          children: [
                                            Align(
                                              alignment: Alignment.topLeft,
                                              child: Text(
                                                "Kalyan",
                                                style: TextStyle(
                                                  color: Color.fromARGB(
                                                      255, 0, 3, 44),
                                                  fontSize: 14,
                                                  fontFamily: "IBM Plex Sans",
                                                  fontWeight: FontWeight.w500,
                                                ),
                                                textAlign: TextAlign.left,
                                              ),
                                            ),
                                            Align(
                                              alignment: Alignment.topLeft,
                                              child: Container(
                                                margin:
                                                    EdgeInsets.only(left: 74),
                                                child: Text(
                                                  "09:00 AM",
                                                  style: TextStyle(
                                                    color: Color.fromARGB(
                                                        255, 0, 3, 44),
                                                    fontSize: 14,
                                                    fontFamily: "IBM Plex Sans",
                                                    fontWeight: FontWeight.w500,
                                                  ),
                                                  textAlign: TextAlign.left,
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            Spacer(),
                            Align(
                              alignment: Alignment.topLeft,
                              child: Container(
                                width: 30,
                                height: 20,
                                decoration: BoxDecoration(
                                  color: Color.fromARGB(26, 0, 3, 44),
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(10)),
                                ),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.end,
                                  children: [
                                    Container(
                                      margin: EdgeInsets.only(top: 1, right: 5),
                                      child: Text(
                                        "3/3",
                                        style: TextStyle(
                                          color:
                                              Color.fromARGB(255, 87, 93, 96),
                                          fontSize: 14,
                                          fontFamily: "IBM Plex Sans",
                                          fontWeight: FontWeight.w500,
                                        ),
                                        textAlign: TextAlign.left,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    Positioned(
                      left: 17,
                      top: 20,
                      bottom: 20,
                      child: Container(
                        width: 52,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          children: [
                            Align(
                              alignment: Alignment.topLeft,
                              child: Container(
                                margin: EdgeInsets.only(left: 17),
                                child: Text(
                                  "Mon",
                                  style: TextStyle(
                                    color: Color.fromARGB(255, 153, 154, 171),
                                    fontSize: 12,
                                    fontFamily: "IBM Plex Sans",
                                    fontWeight: FontWeight.w500,
                                  ),
                                  textAlign: TextAlign.left,
                                ),
                              ),
                            ),
                            Expanded(
                              flex: 1,
                              child: Align(
                                alignment: Alignment.topLeft,
                                child: Container(
                                  margin: EdgeInsets.only(top: 40),
                                  child: Text(
                                    "Jan 2018",
                                    style: TextStyle(
                                      color: Color.fromARGB(255, 153, 154, 171),
                                      fontSize: 12,
                                      fontFamily: "IBM Plex Sans",
                                      fontWeight: FontWeight.w500,
                                    ),
                                    textAlign: TextAlign.left,
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

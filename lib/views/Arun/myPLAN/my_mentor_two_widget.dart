//import 'package:flutter/material.dart';
//
//import 'my_mentor_three_widget.dart';
//
//class MyMentorTwoWidget extends StatelessWidget {
//  void onMyMENTORPressed(BuildContext context) => Navigator.push(
//      context, MaterialPageRoute(builder: (context) => MyMentorThreeWidget()));
//
//  @override
//  Widget build(BuildContext context) {
//    return Scaffold(
//      body: Container(
//        constraints: BoxConstraints.expand(),
//        decoration: BoxDecoration(
//          color: Color.fromARGB(255, 255, 255, 255),
//        ),
//        child: Column(
//          crossAxisAlignment: CrossAxisAlignment.stretch,
//          children: [
//            Container(
//              height: 57,
//              margin: EdgeInsets.only(left: 8, top: 5, right: 8),
//              child: Row(
//                crossAxisAlignment: CrossAxisAlignment.end,
//                children: [
//                  Align(
//                    alignment: Alignment.topLeft,
//                    child: Container(
//                      width: 18,
//                      height: 14,
//                      margin: EdgeInsets.only(left: 12, top: 41),
//                      child: Image.asset(
//                        "assets/images/hamburger.png",
//                        fit: BoxFit.none,
//                      ),
//                    ),
//                  ),
//                  Container(
//                    width: 87,
//                    height: 20,
//                    margin: EdgeInsets.only(left: 20),
//                    child: FlatButton(
//                      onPressed: () => this.onMyMENTORPressed(context),
//                      color: Colors.transparent,
//                      textColor: Color.fromARGB(255, 0, 0, 0),
//                      padding: EdgeInsets.all(0),
//                      child: Text(
//                        "myMENTOR",
//                        style: TextStyle(
//                          fontSize: 16,
//                          fontFamily: "IBM Plex Sans",
//                          fontWeight: FontWeight.w500,
//                        ),
//                        textAlign: TextAlign.left,
//                      ),
//                    ),
//                  ),
//                ],
//              ),
//            ),
//            Container(
//              height: 36,
//              margin: EdgeInsets.only(top: 30),
//              decoration: BoxDecoration(
//                color: Color.fromARGB(255, 255, 255, 255),
//                boxShadow: [
//                  BoxShadow(
//                    color: Color.fromARGB(14, 104, 104, 104),
//                    offset: Offset(0, 8),
//                    blurRadius: 16,
//                  ),
//                ],
//              ),
//              child: Column(
//                crossAxisAlignment: CrossAxisAlignment.stretch,
//                children: [
//                  Container(
//                    height: 18,
//                    margin: EdgeInsets.only(left: 55, top: 10, right: 55),
//                    child: Row(
//                      crossAxisAlignment: CrossAxisAlignment.stretch,
//                      children: [
//                        Align(
//                          alignment: Alignment.topLeft,
//                          child: Text(
//                            "Meet a Mentor",
//                            style: TextStyle(
//                              color: Color.fromARGB(255, 86, 72, 235),
//                              fontSize: 14,
//                              fontFamily: "IBM Plex Sans",
//                              fontWeight: FontWeight.w500,
//                            ),
//                            textAlign: TextAlign.left,
//                          ),
//                        ),
//                        Spacer(),
//                        Align(
//                          alignment: Alignment.topLeft,
//                          child: Text(
//                            "Write to Mentor",
//                            style: TextStyle(
//                              color: Color.fromARGB(255, 153, 154, 171),
//                              fontSize: 14,
//                              fontFamily: "IBM Plex Sans",
//                            ),
//                            textAlign: TextAlign.left,
//                          ),
//                        ),
//                      ],
//                    ),
//                  ),
//                  Align(
//                    alignment: Alignment.topLeft,
//                    child: Container(
//                      width: 123,
//                      height: 2,
//                      margin: EdgeInsets.only(left: 40, top: 6),
//                      decoration: BoxDecoration(
//                        color: Color.fromARGB(255, 86, 72, 235),
//                      ),
//                      child: Container(),
//                    ),
//                  ),
//                ],
//              ),
//            ),
//            Align(
//              alignment: Alignment.topCenter,
//              child: Container(
//                width: 262,
//                height: 211,
//                margin: EdgeInsets.only(top: 43),
//                child: Stack(
//                  alignment: Alignment.center,
//                  children: [
//                    Positioned(
//                      top: 0,
//                      child: Container(
//                        width: 262,
//                        child: Text(
//                          "Want a solid plan of action to improve your scores? \n\n",
//                          style: TextStyle(
//                            color: Color.fromARGB(255, 31, 37, 43),
//                            fontSize: 18,
//                            fontFamily: "IBM Plex Sans",
//                            fontWeight: FontWeight.w700,
//                          ),
//                          textAlign: TextAlign.center,
//                        ),
//                      ),
//                    ),
//                    Positioned(
//                      top: 71,
//                      child: Container(
//                        width: 133,
//                        height: 140,
//                        child: Image.asset(
//                          "assets/images/mentorpic.png",
//                          fit: BoxFit.none,
//                        ),
//                      ),
//                    ),
//                  ],
//                ),
//              ),
//            ),
//            Align(
//              alignment: Alignment.topCenter,
//              child: Container(
//                width: 225,
//                margin: EdgeInsets.only(top: 18),
//                child: Text(
//                  "Book a slot to discuss your progress with a mentor",
//                  style: TextStyle(
//                    color: Color.fromARGB(255, 87, 93, 96),
//                    fontSize: 14,
//                    fontFamily: "IBM Plex Sans",
//                  ),
//                  textAlign: TextAlign.center,
//                ),
//              ),
//            ),
//            Container(
//              height: 57,
//              margin: EdgeInsets.only(left: 45, top: 42, right: 45),
//              child: Stack(
//                alignment: Alignment.center,
//                children: [
//                  Positioned(
//                    top: 38,
//                    child: Text(
//                      "BOOK SLOT",
//                      style: TextStyle(
//                        color: Color.fromARGB(255, 255, 255, 255),
//                        fontSize: 14,
//                        fontFamily: "IBM Plex Sans",
//                      ),
//                      textAlign: TextAlign.left,
//                    ),
//                  ),
//                  Positioned(
//                    left: 0,
//                    top: 0,
//                    right: 0,
//                    child: Container(
//                      height: 40,
//                      decoration: BoxDecoration(
//                        gradient: LinearGradient(
//                          begin: Alignment(-0.011, 0.494),
//                          end: Alignment(1.031, 0.516),
//                          stops: [
//                            0,
//                            1,
//                          ],
//                          colors: [
//                            Color.fromARGB(255, 51, 128, 204),
//                            Color.fromARGB(255, 137, 110, 216),
//                          ],
//                        ),
//                        borderRadius: BorderRadius.all(Radius.circular(2)),
//                      ),
//                      child: Column(
//                        children: [
//                          Container(
//                            margin: EdgeInsets.only(top: 11),
//                            child: Text(
//                              "BOOK SLOT",
//                              style: TextStyle(
//                                color: Color.fromARGB(255, 255, 255, 255),
//                                fontSize: 14,
//                                fontFamily: "IBM Plex Sans",
//                                fontWeight: FontWeight.w500,
//                              ),
//                              textAlign: TextAlign.center,
//                            ),
//                          ),
//                        ],
//                      ),
//                    ),
//                  ),
//                ],
//              ),
//            ),
//            Align(
//              alignment: Alignment.topCenter,
//              child: Container(
//                margin: EdgeInsets.only(top: 31),
//                child: Text(
//                  "No slots available?\nYou can Write to a Mentor instead",
//                  style: TextStyle(
//                    color: Color.fromARGB(255, 87, 93, 96),
//                    fontSize: 12,
//                    fontFamily: "IBM Plex Sans",
//                  ),
//                  textAlign: TextAlign.center,
//                ),
//              ),
//            ),
//          ],
//        ),
//      ),
//    );
//  }
//}

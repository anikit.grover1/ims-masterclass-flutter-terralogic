import 'package:flutter/material.dart';
import 'package:imsindia/views/Arun/ims_utility.dart';

import '2_w_book_a_mentor.dart';

const actualHeight = 768;
const actualWidth = 360;
var screenWidthTotal;
var screenHeightTotal;
var _safeAreaHorizontal;
var _safeAreaVertical;
var screenHeight;
var screenWidth;
var monVal = true;

class WriteToAMentorWidget_2_w extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    CalculateScreen(context);
    return Scaffold(
      backgroundColor: ImsColors.blur,
      body: Container(
        constraints: BoxConstraints.expand(),
        decoration: BoxDecoration(),
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              Align(
                alignment: Alignment.topCenter,
                child: Container(
                  width: 262,
                  height: 211,
                  margin: EdgeInsets.only(top: 43),
                  child: Stack(
                    alignment: Alignment.center,
                    children: [
                      Positioned(
                        top: 0,
                        child: Container(
                          width: 262,
                          child: Text(
                            "Want a solid plan of action to improve your scores? \n\n",
                            style: TextStyle(
                              color: Color.fromARGB(255, 31, 37, 43),
                              fontSize: 18,
                              height: 1.5,
                              fontFamily: "IBM Plex Sans",
                              fontWeight: FontWeight.w700,
                            ),
                            textAlign: TextAlign.center,
                          ),
                        ),
                      ),
                      Positioned(
                        top: 71,
                        child: Container(
                          width: 168,
                          height: 140,
                          child: Image.asset(
                            "assets/images/mentorwrite-to-a-mentor.png",
                            fit: BoxFit.none,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              Align(
                alignment: Alignment.topCenter,
                child: Container(
                  width: 225,
                  margin: EdgeInsets.only(top: 18),
                  child: Text(
                    "Book a slot to discuss your progress with a mentor",
                    style: TextStyle(
                      color: Color.fromARGB(255, 87, 93, 96),
                      fontSize: 14,
                      height: 1.5,
                      fontFamily: "IBM Plex Sans",
                    ),
                    textAlign: TextAlign.center,
                  ),
                ),
              ),
              Align(
                alignment: Alignment.topCenter,
                child: GestureDetector(
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => BookAMentor_2_w()));
                  },
                  child: Container(
                    width: 270,
                    height: 57,
                    margin: EdgeInsets.only(top: 42),
                    child: Stack(
                      alignment: Alignment.center,
                      children: [
//                      Positioned(
//                        top: 38,
//                        child: Text(
//                          "BOOK SLOT",
//                          style: TextStyle(
//                            color: Color.fromARGB(255, 255, 255, 255),
//                            fontSize: 14,
//                            fontFamily: "IBM Plex Sans",
//                          ),
//                          textAlign: TextAlign.left,
//                        ),
//                      ),
                        Positioned(
                          top: 0,
                          child: Container(
                            width: 270,
                            height: 40,
                            decoration: BoxDecoration(
                              gradient: LinearGradient(
                                begin: Alignment(-0.011, 0.494),
                                end: Alignment(1.031, 0.516),
                                stops: [
                                  0,
                                  1,
                                ],
                                colors: [
                                  Color.fromARGB(255, 51, 128, 204),
                                  Color.fromARGB(255, 137, 110, 216),
                                ],
                              ),
                              borderRadius:
                                  BorderRadius.all(Radius.circular(2)),
                            ),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                GestureDetector(
                                  onTap: () {
                                    Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) =>
                                                BookAMentor_2_w()));
                                  },
                                  child: Container(
                                    margin: EdgeInsets.only(left: 84, top: 11),
                                    child: Text(
                                      "SEND REQUEST",
                                      style: TextStyle(
                                        color:
                                            Color.fromARGB(255, 255, 255, 255),
                                        fontSize: 14,
                                        fontFamily: "IBM Plex Sans",
                                        fontWeight: FontWeight.w500,
                                      ),
                                      textAlign: TextAlign.center,
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              Container(
                margin: EdgeInsets.only(
                    top: 60,
                    bottom: 20.0,
                    left: 20 / screenWidth * actualWidth),
                child: Text(
                  'Queries',
                  style: TextStyle(
                    color: ImsColors.dark_navy_blue,
                    fontFamily: "IBMPlexSans",
                    fontStyle: FontStyle.normal,
                    fontSize: 18.0,
                    fontWeight: FontWeight.w700,
                  ),
                ),
              ),
              SizedBox(
                height: 345,
                child: BodyLayout(),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class BodyLayout extends StatefulWidget {
  @override
  BodyLayoutState createState() {
    return new BodyLayoutState();
  }
}

class BodyLayoutState extends State<BodyLayout> {
//  List<String> titles = ['Sun', 'Moon', 'Star', 'Arun', 'sunny'];
  List<Item> itemsAll = [Item(), Item(), Item(), Item(), Item(), Item()];

  @override
  Widget build(BuildContext context) {
    CalculateScreen(context);
    return _myListView();
  }

  Widget _myListView() {
    return ListView.builder(
      itemCount: itemsAll.length,
      itemBuilder: (context, index) {
        final item = itemsAll[index];
        return Container(
          child: ListTile(
//            title: Text(item),
            title: Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(
                    7.0,
                  ),
                  border: Border.all(
                    width: 0.4,
                    color: ImsColors.bluey_grey,
                  ),
                ),
                margin: EdgeInsets.only(
                  bottom: 20.0 / actualHeight * screenHeight,
                ),
                child: Column(
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.only(
                          top: 5 / actualHeight * screenHeight,
                          bottom: 15 / actualHeight * screenHeight),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Expanded(
                              flex: 4,
                              child: Padding(
                                padding: EdgeInsets.only(top: 10.0),
                                child: Container(
                                  padding:
                                      EdgeInsets.only(left: 12.5, right: 12.5),
                                  child: Column(
                                    children: <Widget>[
                                      Padding(
                                        padding: EdgeInsets.only(top: 6.0),
                                        child: Text(
                                          'Tue',
                                          style: TextStyle(
                                            color: ImsColors.bluey_grey,
                                            fontSize: 12,
                                            fontFamily: "IBM Plex Sans-Medium",
                                            fontWeight: FontWeight.w500,
                                          ),
                                        ),
                                      ),
                                      Container(
                                        margin: EdgeInsets.only(
                                            top: 6.0, bottom: 7.0),
                                        child: Text(
                                          '24',
                                          style: TextStyle(
                                              color: ImsColors.dark_navy_blue,
                                              fontWeight: FontWeight.w500,
                                              fontFamily: "IBMPlexSans",
                                              fontStyle: FontStyle.normal,
                                              fontSize: 36.0),
                                        ),
                                      ),
                                      Text(
                                        'Nov 2018',
                                        style: TextStyle(
                                          color: ImsColors.bluey_grey,
                                          fontSize: 12,
                                          fontFamily: "IBM Plex Sans-Medium",
                                          fontWeight: FontWeight.w500,
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                              )),
                          Expanded(
                            flex: 10,
                            child: Padding(
                              padding: EdgeInsets.only(top: 10),
                              child: Row(
                                children: <Widget>[
                                  Container(
                                    margin: EdgeInsets.only(right: 20),
                                    width: 1,
                                    height: 80,
                                    color: ImsColors.bluey_grey,
                                  ),
                                  Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: <Widget>[
                                      Padding(
                                        padding: EdgeInsets.only(bottom: 12),
                                        child: Container(
                                          width: 160,
                                          child: Text(
                                            item.title,
                                            maxLines: 1,
                                            textAlign: TextAlign.left,
                                            overflow: TextOverflow.ellipsis,
                                            style: TextStyle(
                                                color: ImsColors.namecolor,
                                                fontWeight: FontWeight.w500,
                                                fontFamily: "IBMPlexSans",
                                                fontStyle: FontStyle.normal,
                                                fontSize: 18.0),
                                          ),
                                        ),
                                      ),
                                      Row(
                                        children: <Widget>[
                                          Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: <Widget>[
                                              Container(
                                                child: Text(
                                                  'Location',
                                                  textAlign: TextAlign.left,
                                                  maxLines: 1,
                                                  style: TextStyle(
                                                      color:
                                                          ImsColors.bluey_grey,
                                                      fontSize: 12.0),
                                                ),
                                              ),
                                              Container(
                                                margin: EdgeInsets.only(top: 3),
                                                child: Text(
                                                  '${item.location} ',
                                                  textAlign: TextAlign.left,
                                                  maxLines: 1,
                                                  style: TextStyle(
                                                    color: ImsColors
                                                        .dark_navy_blue,
                                                    fontSize: 14.0,
                                                    fontWeight: FontWeight.w500,
                                                    fontFamily: "IBMPlexSans",
                                                    fontStyle: FontStyle.normal,
                                                  ),
                                                ),
                                              ),
                                            ],
                                          ),
                                          Container(
                                            margin: EdgeInsets.only(left: 45),
                                            child: Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: <Widget>[
                                                Container(
                                                  child: Text(
                                                    'Time',
                                                    textAlign: TextAlign.left,
                                                    maxLines: 1,
                                                    style: TextStyle(
                                                        color: ImsColors
                                                            .bluey_grey,
                                                        fontSize: 12.0),
                                                  ),
                                                ),
                                                Container(
                                                  margin:
                                                      EdgeInsets.only(top: 3),
                                                  child: Text(
                                                    '${item.time}',
                                                    textAlign: TextAlign.left,
                                                    maxLines: 1,
                                                    style: TextStyle(
                                                      color: ImsColors
                                                          .dark_navy_blue,
                                                      fontSize: 14.0,
                                                      fontWeight:
                                                          FontWeight.w500,
                                                      fontFamily: "IBMPlexSans",
                                                      fontStyle:
                                                          FontStyle.normal,
                                                    ),
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                        ],
                                      )
                                    ],
                                  ),
                                ],
                              ),
                            ),
                          ),
                          Expanded(
                            flex: 2,
                            child: Container(
                              alignment: Alignment.center,
                              margin: EdgeInsets.only(right: 5),
                              height: 25,
                              width: 25,
//                              margin: EdgeInsets.only(left: 10, top: 2),
                              decoration: BoxDecoration(
                                color: Color.fromARGB(26, 65, 125, 206),
                                borderRadius:
                                    BorderRadius.all(Radius.circular(10)),
                              ),
                              child: Container(
                                margin: EdgeInsets.only(left: 1.5, top: 3),
                                child: Text(
                                  '${index + 1} / ${itemsAll.length}',
                                  style: TextStyle(
                                    color: Color.fromARGB(255, 122, 113, 213),
                                    fontSize: 14,
                                    fontFamily: "IBM Plex Sans",
                                    fontWeight: FontWeight.w500,
                                  ),
                                  textAlign: TextAlign.left,
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      height: 1 / actualHeight * screenHeight,
                      width: double.infinity,
                      color: ImsColors.iceBlue,
                    )
                  ],
                )),
            onTap: () {
              //                                  <-- onTap
              setState(() {
//                titles.insert(index, 'Planet');
              });
            },
            onLongPress: () {
              //                            <-- onLongPress
              setState(() {
//                titles.removeAt(index);
              });
            },
          ),
        );
      },
    );
  }
}

class Item {
  var title = 'Arun Kumar Chaudhary';
  var location = 'Thane';
  var time = '9.00 AM';
  var imgUrl =
      'https://thumbs.dreamstime.com/b/inspirational-quote-there-blessings-everyday-find-them-create-treasure-beautiful-white-single-sinnia-flower-blossom-blurry-151653366.jpg';
}

void CalculateScreen(BuildContext context) {
  var _mediaQueryData = MediaQuery.of(context);
  screenWidthTotal = _mediaQueryData.size.width;
  screenHeightTotal = _mediaQueryData.size.height;
  _safeAreaHorizontal =
      _mediaQueryData.padding.left + _mediaQueryData.padding.right;
  _safeAreaVertical =
      _mediaQueryData.padding.top + _mediaQueryData.padding.bottom;

  screenHeight = screenHeightTotal - _safeAreaVertical;
  screenWidth = screenWidthTotal - _safeAreaHorizontal;
}

import 'package:flutter/material.dart';
import 'package:imsindia/components/navigation_bar.dart';
import 'package:imsindia/views/Arun/myPLAN/2_w_write_to_mentor.dart';
import 'package:imsindia/resources/strings/myplan.dart';
import 'package:imsindia/views/Arun/myPLAN/no_access_screen.dart';
import 'package:imsindia/views/home_pages/home_widget.dart';
import '../ims_utility.dart';
import 'myPLAN_bookslot_screen.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:imsindia/api/bloc/services.dart';
import 'package:imsindia/utils/Url.dart';
import 'package:imsindia/utils/global.dart' as global;

class myPLANHome extends StatefulWidget {
  bool flag;
  int avaliableSlots;
  int slotsUsed;
  myPLANHome(this.flag,this.avaliableSlots,this.slotsUsed);

    @override
  _myPLANHomeState createState() => _myPLANHomeState();
}

class _myPLANHomeState extends State<myPLANHome> with TickerProviderStateMixin{
 
  TabController tabController;
  int avaliableSlots;
  int totalSlots;
  int slotsUsed;
  var courseName;
  bool  branchChecking=false;
  List venueData = [];
  var studentImsPin;
  var myImsCourseId_venue;
  Future _future;

  getCourseName() async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      courseName =prefs.getString('courseName');
      print(courseName);
      print("course name");
    });

  }
  Future  getVenueDetails()async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    studentImsPin = prefs.getString(ImsStrings.sCurrentUserId);
    myImsCourseId_venue = prefs.getString("courseId");

    Map<String, dynamic> postdata_venue;
    postdata_venue = {
      "myImsCourseId":myImsCourseId_venue,
      "studentImsPin": studentImsPin //"99a77771" //studentImsPin //"99a77771"
    };
    print(postdata_venue);
    print("post datapost datapost datapost datapost data");
    var _headers = {"Content-Type": "application/json"};
    final result = await ApiService().postAPITerr(URL.myPLAN_API_VENUE_DETAILS, postdata_venue, global.headers);
    if (result[0]['success']==true){
      setState(() {
        branchChecking=true;
      });
      return branchChecking;
    }
    else{
      branchChecking=false;
      return branchChecking;
    }



  }
  @override
  void initState() {
    super.initState();
   tabController = new TabController(vsync: this, length: 1);
   if(widget.flag == true){
     tabController.animateTo(1);
   }
   else{
     tabController.animateTo(0);
   }
    if(widget.avaliableSlots != null){
      avaliableSlots= widget.avaliableSlots;
    }
    else{
      avaliableSlots= 0;
    }
    if(widget.slotsUsed != null){
      slotsUsed= widget.slotsUsed;
    }
    else{
      slotsUsed= 0;
    }
    getCourseName();
    _future =  getVenueDetails();

    //totalSlots
  }

  @override
  Widget build(BuildContext context) {
    Future<bool> _onWillPop() {
      print("no not at all coming here");
      Navigator.pop(context);
      Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (context) =>
          HomeWidget()), (Route<dynamic> route) => false);
    }
    return FutureBuilder(

        builder: (context, snapshot) {
          if (snapshot.hasError) {
            return Container(); // widget to be shown on any error
          }
          return snapshot.hasData ?
          branchChecking==false?

          WillPopScope(
            onWillPop:_onWillPop ,
            child: Scaffold(
                appBar: AppBar(
                  brightness: Brightness.light,
                  elevation: 0.0,
                  centerTitle: false,
                  titleSpacing: 0.0,
                  iconTheme: IconThemeData(
                    color: Colors.black, //change your color here
                  ),
                  title: GestureDetector(
                    child: Text(
                      // courseName.toString().toLowerCase()=="GDPI".toUpperCase()?tabHomeScreenString.piSlotBooking:tabHomeScreenString.myMENTOR,
                      courseName.toString()=="GDPI"||courseName.toString()=="PGDBA"?tabHomeScreenString.piSlotBooking:tabHomeScreenString.myMENTOR,
                      style: const TextStyle(
                          color: Colors.black,
                          fontWeight: FontWeight.w600,
                          fontFamily: "IBMPlexSans-Medium",
                          fontSize: 16.0),
                    ),
                  ),
                  backgroundColor: Color.fromARGB(255, 255, 255, 255),
                 ),
                body: Container(child: Center(child: NoAccessScreen()))),
          ):

          DefaultTabController(
            length: 2,
            child: Scaffold(
              backgroundColor: Colors.white,
              appBar: AppBar(
                brightness: Brightness.light,
                elevation: 0.0,
                centerTitle: false,
                titleSpacing: 0.0,
                iconTheme: IconThemeData(
                  color: Colors.black, //change your color here
                ),
                backgroundColor: Color.fromARGB(255, 255, 255, 255),

                title: GestureDetector(
                  child: Text(
                    // courseName.toString().toLowerCase()=="GDPI".toUpperCase()?tabHomeScreenString.piSlotBooking:tabHomeScreenString.myMENTOR,
                    courseName.toString()=="GDPI"||courseName.toString()=="PGDBA"?tabHomeScreenString.piSlotBooking:tabHomeScreenString.myMENTOR,
                    style: const TextStyle(
                        color: Colors.black,
                        fontWeight: FontWeight.w600,
                        fontFamily: "IBMPlexSans-Medium",
                        fontSize: 16.0),
                  ),
                ),
//            leading: IconButton(
//              icon: Icon(
//                Icons.arrow_back,
//                color: Colors.black,
//              ),
//              onPressed: () => Navigator.pop(context, false),
//            ),
                bottom: TabBar(
                  controller: tabController,
                  labelStyle: TextStyle(fontSize: 14),
                  isScrollable: true,
                  indicatorSize: TabBarIndicatorSize.tab,
                  indicatorColor: ImsColors.purpleish_blue,
                  labelColor: ImsColors.purpleish_blue,
                  unselectedLabelColor: ImsColors.bluey_grey,
                  tabs: [
                    Tab(
                      text:   tabHomeScreenString.meetamentor,
                    ),
//                Tab(
//                  text: 'Write to Mentor',
//                ),
                  ],
                ),
              ),
//              drawer: NavigationDrawer(),
              body: TabBarView(
                controller: tabController,
                children: [
                  MyplanBookslotScreen(avaliableSlots,slotsUsed),
                  // WriteToAMentorWidget_2_w(),
                ],
              ),
            ),

          )
              : Scaffold(body: Container(child: Center(child: CircularProgressIndicator(),),));
        },
      future: _future,

    );






  }
}

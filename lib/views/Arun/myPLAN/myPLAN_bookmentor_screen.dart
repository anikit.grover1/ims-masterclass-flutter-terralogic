import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
//import 'package:flutter_typeahead/flutter_typeahead.dart';
import 'package:imsindia/components/custom_DropDown.dart';
import 'package:imsindia/api/bloc/services.dart';
//import 'package:imsindia/components/Dropdown_Leaderboard.dart';
import 'package:imsindia/resources/strings/login.dart';
import 'package:imsindia/resources/strings/myplan.dart';
import 'package:imsindia/utils/Url.dart';
import 'package:imsindia/utils/colors.dart';
import 'package:imsindia/utils/emaximiser_svg_icons.dart';
import 'package:imsindia/utils/global.dart' as global;
import 'package:imsindia/views/Arun/myPLAN/2_w_book_a_mentor.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../ims_utility.dart';
import 'myPLAN_confirmation_booking_screen.dart';
import 'myPLAN_otpScreen.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:link/link.dart';


final TextEditingController _Venues = new TextEditingController();
final TextEditingController _Mentors = new TextEditingController();
final TextEditingController _Reasons = new TextEditingController();
final TextEditingController _Mode = new TextEditingController();

var authToken;
var screenHeight;
var screenWidth;
var headers;
var mentor_name;
var _selectedbranchID;
var _selectedmentorID;
var _selectedCourseID;
var validationForMode;
var mode;
var _selectedCourseName;
List<String> mentorName = [];
List<String> modeName = ["Virtual","In-Class"];
List<String> mentorID = [];
List<String> branchName = [];
List<String> branchId = [];
List mentorData = [];
List venueData = [];
List dataforavailableDate = [];
List dataforavailableSlot = [];
List dataforcourseReasonTypes = [];
List<String> slotIDBasedonDate = [];
List<String> slotStatusBasedonDate = [];
List<String> slotIDBasedonTime = [];
List<String> slotStatusBasedonTime = [];
List<String> slotTime = [];
List<String> slotID = [];
List<String> slotDate = [];
List<String> courseReasonTypeId = [];
List<String> courseReasonType = [];

class Bookamentor_Screen extends StatefulWidget {
  var slot_total;
  var slots_used;
  Bookamentor_Screen(this.slot_total, this.slots_used);

  @override
  Bookamentor_Screen_State createState() => Bookamentor_Screen_State();
}

bool _enabledVenue = false;
bool _enabledMode = false;
bool _enabledMentor = false;
bool _enabledReason = false;
bool _enabledDate = false;
bool _enabledSlot = false;

class Bookamentor_Screen_State extends State<Bookamentor_Screen> {
  int countcliked = 0;
  bool bookSlot_STATUSCLICK = false;
  bool noDate =false;
  bool noTime =false;
  var studentImsPin;
  var myImsCourseId_venue;
  var myImsCourseId;
  var myImsCourseId_mentor;
  var totalSlots;
  var usedSlots;
  var availableSlots;
  final TextEditingController _mentorDetails = new TextEditingController();
  final TextEditingController _messagecontroller = new TextEditingController();
  String labelValue = "";
  String noDateavailable = "";
  var studentEmail;
  var studentName;
  final FocusNode _firstInputFocusNode = new FocusNode();
  String checkInternetStatusFlag;
  String text_reason;

//  void onMentorSlotBookingPressed(BuildContext context) => Navigator.push(
//      context,
//      MaterialPageRoute(builder: (context) => MeetingConfirmation_2_m()));

  String _venue = 'St. Jude’s High School1';

  final List<String> _dates = [
    "06-02-2019",
    "07-02-2019",
    "08-02-2019",
    "09-02-2019",
  ];

  final List<String> _slots = [
    "10:00 AM",
    "11:00 AM",
    "12:00 PM",
    "02:00 PM",
    "03:00 PM",
    "04:00 PM",
    "05:00 PM",
    "06:00 PM",
    "07:00 PM",
  ];
  int _selectedIndexDate;
  int _selectedIndexSlot;

  _onSelectedDate(int indexDate) {
    setState(() {
      print("=============================Selected Date");
      print(indexDate);
      _selectedIndexDate = indexDate;
      _enabledDate = true;
    });
  }

  _onSelectedSlot(int indexSlot) {
    setState(() {
      _selectedIndexSlot = indexSlot;
      _enabledSlot = true;
    });
  }
  Future checkInternet() async {
    var connectivityResult = await (Connectivity().checkConnectivity());
    checkInternetStatusFlag = connectivityResult.toString();
    return checkInternetStatusFlag;
  }
  void showMessage(String inputValue) {
    Fluttertoast.showToast(
      msg: inputValue,
      toastLength: Toast.LENGTH_SHORT,
      gravity: ToastGravity.BOTTOM,
      timeInSecForIosWeb: 1,
      backgroundColor: PrimaryColors.azure_Dark,
      textColor: Colors.white,
      fontSize: 14.0,
    );
  }
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
     _selectedbranchID="";
     _selectedmentorID="";
     _selectedCourseID="";
     validationForMode="";
    print("=================================================slot length" + slotTime.length.toString());
    print(widget.slots_used);
    print(widget.slot_total);
    bookSlot_STATUSCLICK = false;
    _messagecontroller.text = '';
    if(widget.slots_used != null){
      usedSlots = widget.slots_used;
      usedSlots = usedSlots+1;
    }else {
      usedSlots = 0;
    }


    if(widget.slot_total != null){
      totalSlots = widget.slot_total;
    }else{
      totalSlots = 0;
    }
    noDateavailable = "";
    if(noDate == false){
      noTime = false;
    }
    _Venues.text = '';
    _Mentors.text = '';
    _Reasons.text = '';
    _enabledVenue = false;
    bool _enabledMode = false;

    _enabledMentor = false;
    _enabledDate = false;
    _enabledSlot = false;
    mentorName= [];
    slotTime = [];
    slotID = [];
    slotDate = [];
    getStudentImsPin_pref();
    getmyImsCourseId_pref();
    headers = {"Content-Type": "application/json"};
    global.getToken.then((t){
      authToken=t;
    });
    sampleData=[];
    getVenueDetails();
    addRadioModel();

  }

  @override
  Widget build(BuildContext context) {
    final screenHeight = MediaQuery.of(context).size.height;
    final screenWidth = MediaQuery.of(context).size.width;
    final defaultScreenHeight = 720;
    final defaultScreenWidth = 360;
    var size = MediaQuery.of(context).size;
    final double itemHeight = (size.height) / 2;
    final double itemWidth = size.width / 2;
    Future<bool> _onWillPop() {
      Navigator.pop(context);
    }
    return WillPopScope(
      onWillPop: _onWillPop,
      child: Scaffold(
        appBar: AppBar(
          brightness: Brightness.light,
          elevation: 0.0,
          centerTitle: false,
          titleSpacing: 0.0,
          backgroundColor: Colors.white,
          title: Text(
            MyplanBookMentorScreenString.BookaMentor,
            style: const TextStyle(
                color: Colors.black,
                fontWeight: FontWeight.w500,
                fontFamily: "IBMPlexSans",
                fontStyle: FontStyle.normal,
                fontSize: 16.0
            ),
          ),
          leading: IconButton(
            icon: getSvgIcon.backSvgIcon,
            onPressed: () => Navigator.pop(context, false),
          ),
        ),
        body: SingleChildScrollView(
          child: Container(
            color: Colors.white,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                //Start point of Mentor Slot Booking container
                Container(
                  height: 36/720 * screenHeight,
                  margin: EdgeInsets.only(left:20/360 * screenWidth, top: 25/720 * screenHeight),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      Align(
                        alignment: Alignment.topLeft,
                        child: Container(
                          // width: 171,
                          // height: 23,
                          child: FlatButton(
                            onPressed: () {},
                            color: Colors.transparent,
                            textColor: Color.fromARGB(255, 0, 3, 44),
                            padding: EdgeInsets.all(0),
                            child: Text(
                              MyplanBookMentorScreenString.MentorSlotBooking,
                              style: TextStyle(
                                fontSize: 18/360*screenWidth,
                                fontFamily: "IBM Plex Sans",
                                fontWeight: FontWeight.w700,
                              ),
                              textAlign: TextAlign.left,
                            ),
                          ),
                        ),
                      ),
                      Expanded(
                        flex: 2,
                        child: Align(
                          alignment: Alignment.topLeft,
                          child: Container(
                            height: 32/720 * screenHeight,
                            //width: 100/360 * screenWidth,
                            margin: EdgeInsets.only(left: 10/360 * screenWidth, top: 2/720 * screenHeight),
                            decoration: BoxDecoration(
                              color: Color.fromARGB(26, 65, 125, 206),
                              borderRadius: BorderRadius.all(Radius.circular(10)),
                            ),
                            child: Container(
                              margin: EdgeInsets.only(left: 10/360 * screenWidth,top: 8/720 * screenHeight,right: 10/360 * screenWidth),
                              child: Text(
                                "${usedSlots}" "/" "${totalSlots}",
                                style: TextStyle(
                                  color: Color.fromARGB(255, 122, 113, 213),
                                  fontSize: 12/720 * screenHeight,
                                  fontFamily: "IBM Plex Sans",
                                  fontWeight: FontWeight.w500,
                                ),
                                textAlign: TextAlign.left,
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                //End point of Mentor Slot Booking container

                /**Start point of Select venue container**/
                Container(
                  margin: EdgeInsets.only(left: 20/360 * screenWidth, top: 20/720 * screenHeight,right: 22/360 * screenWidth),
                  child: DropDownFormField(
                    getImmediateSuggestions: true,
                    // autoFlipDirection: true,
                    textFieldConfiguration: TextFieldConfiguration(
                        controller: _Venues,
                        label: MyplanBookMentorScreenString.SelectVenue),
                    suggestionsBoxDecoration: SuggestionsBoxDecoration(
                        dropDownHeight: 150,
                        borderRadius: new BorderRadius.circular(5.0),
                        color: Colors.white),
                    suggestionsCallback: (pattern) {
                      return branchName;
                    },
                    itemBuilder: (context, suggestion) {
                      return ListTile(
                        dense: true,
                        contentPadding: EdgeInsets.only(left: 10),
                        title: Text(
                          suggestion,
                          style: TextStyle(
                            fontFamily: "IBMPlexSans",
                            fontWeight: FontWeight.w400,
                            fontSize: (13 / 720) * screenHeight,
                            color: NeutralColors.bluey_grey,
                            textBaseline: TextBaseline.alphabetic,
                            letterSpacing: 0.0,
                            inherit: false,
                          ),
                        ),
                      );
                    },
                    hideOnLoading: true,
                    debounceDuration: Duration(milliseconds: 100),
                    transitionBuilder: (context, suggestionsBox, controller) {
                      return suggestionsBox;
                    },
                    onSuggestionSelected: (suggestion) {
                      setState(() {
                        print("============================suggestion"+suggestion);
                        print(branchName.length);
                        for(int i=0;i<branchName.length;i++){
                          if(branchName[i] == suggestion){
                            _selectedbranchID = branchId[i];
                            print("============================"+_selectedbranchID);
                            getMentorDetails(_selectedbranchID);
                          }
                        }
                        _selectedmentorID = "";
                        _enabledVenue = true;
                      });
                      _Venues.text = suggestion;
                    },
                    onSaved: (value) => {print("something")},
                  ),
                ),
                /**End point of Select venue container**/

                /**Start point of Select Mentor container**/
                _enabledVenue == true
                    ? Container(
                  margin: EdgeInsets.only(left: 20/360 * screenWidth, top: 20/720 * screenHeight,right: 22/360 * screenWidth),
                  child: DropDownFormField(
                    getImmediateSuggestions: true,
                    // autoFlipDirection: true,
                    textFieldConfiguration: TextFieldConfiguration(
                        controller: _Mentors,
                        label: MyplanBookMentorScreenString.ChooseYourMentor),
                    suggestionsBoxDecoration: SuggestionsBoxDecoration(
                        dropDownHeight: 150,
                        borderRadius: new BorderRadius.circular(5.0),
                        color: Colors.white),
                    suggestionsCallback: (pattern) {
                      return mentorName;
                    },
                    itemBuilder: (context, suggestion) {
                      return ListTile(
                        dense: true,
                        contentPadding: EdgeInsets.only(left: 10),
                        title: Text(
                          suggestion,
                          style: TextStyle(
                            fontFamily: "IBMPlexSans",
                            fontWeight: FontWeight.w400,
                            fontSize: (13 / 720) * screenHeight,
                            color: NeutralColors.bluey_grey,
                            textBaseline: TextBaseline.alphabetic,
                            letterSpacing: 0.0,
                            inherit: false,
                          ),
                        ),
                      );
                    },
                    hideOnLoading: true,
                    debounceDuration: Duration(milliseconds: 100),
                    transitionBuilder: (context, suggestionsBox, controller) {
                      return suggestionsBox;
                    },
                    onSuggestionSelected: (suggestion) {
                      setState(() {
                        print("============================suggestionMentor"+suggestion);
                        print(mentorName.length);
                        for(int i=0;i<mentorName.length;i++){
                          if(mentorName[i] == suggestion){
                            _selectedmentorID = mentorID[i];
                            validationForMode="";
                            print("============================"+_selectedmentorID);
                          }
                        }
                        _enabledMode = true;
                      });
                      _Mentors.text = suggestion;
                      //getAvailableDates();
                    },
                    onSaved: (value) => {print("something")},
                  ),
                )
                    : Container(
                  margin: EdgeInsets.only(left: 20/360 * screenWidth, top: 20/720 * screenHeight,right: 22/360 * screenWidth),
                  child: DropDownFormField(
                    getImmediateSuggestions: true,
                    // autoFlipDirection: true,
                    textFieldConfiguration: TextFieldConfiguration(
                        controller: _Mentors,
                        label: MyplanBookMentorScreenString.ChooseYourMentor),
                    suggestionsBoxDecoration: SuggestionsBoxDecoration(
                        dropDownHeight: 150,
                        borderRadius: new BorderRadius.circular(5.0),
                        color: Colors.white),
                    suggestionsCallback: (pattern) {
                      return null;
                    },
                    itemBuilder: (context, suggestion) {
                      return ListTile(
                        dense: true,
                        contentPadding: EdgeInsets.only(left: 10),
                        title: Text(
                          suggestion,
                          style: TextStyle(
                            fontFamily: "IBMPlexSans",
                            fontWeight: FontWeight.w400,
                            fontSize: (13 / 720) * screenHeight,
                            color: NeutralColors.bluey_grey,
                            textBaseline: TextBaseline.alphabetic,
                            letterSpacing: 0.0,
                            inherit: false,
                          ),
                        ),
                      );
                    },
                    hideOnLoading: true,
                    debounceDuration: Duration(milliseconds: 100),
                    transitionBuilder: (context, suggestionsBox, controller) {
                      return null;
                    },
                    onSuggestionSelected: (suggestion) {

                    },
                    onSaved: (value) => {print("something")},
                  ),
                ) ,
                /**End point of Select Mentor container**/
                /** start point of validation **/
                (validationForMode!=null&&validationForMode!="")?Container(
                  margin: EdgeInsets.only(left: 20/360 * screenWidth, top: 8/720 * screenHeight ,right: 20/360 * screenWidth),
                  child: Text(
                    validationForMode,
                    style: TextStyle(
                      color: NeutralColors.grapefruit,
                      fontSize: (12 / 360) * screenWidth,
                      fontFamily: "IBM Plex Sans",
                    ),
                    textAlign: TextAlign.left,
                  ),
                ):Text(""),

                /** start point of mode**/
                Container(
                  height: 65/720*screenHeight,
                  margin: EdgeInsets.only( top: 20/720 * screenHeight),
                  child: new ListView.builder(
                    scrollDirection: Axis.horizontal,
                    itemCount: sampleData.length,
                    itemBuilder: (BuildContext context, int index) {
                      return new InkWell(
                        //highlightColor: Colors.red,
                        splashColor: Colors.blueAccent,
                        onTap: () {
                          setState(() {
                            sampleData.forEach((element) => element.isSelected = false);
                            sampleData[index].isSelected = true;
                             if(_selectedmentorID!=null&&_selectedmentorID!=""){
                               if(sampleData[index].buttonText=='Virtual'){
                                 print("coming here");
                                 mode="Virtual";
                               }else if(sampleData[index].buttonText=='In-Centre'){
                                 print("coming here1");
                                 mode="In-Center";
                               }else{
                                 print("coming here2");
                                 mode=sampleData[index].buttonText;
                               }
                               print("modeeeeeeeee");
                               print(mode);
                               _selectedIndexDate=null;
                               _selectedIndexSlot=null;
                               noDate=false;
                               noTime=false;
                               _enabledMentor = true;
                               slotIDBasedonTime = [];
                               slotStatusBasedonTime=[];
                               slotTime = [];
                               slotID = [];
                               getAvailableDates();
                             }else{
                             validationForMode=MyplanBookMentorScreenString.validationForMode;
                            }

                          });
                        },
                        child: new RadioItem(sampleData[index]),
                      );
                    },
                  ),
                ),
                /** end point of mode**/

                /** start point of note **/
                Container(
                  margin: EdgeInsets.only(left: 20/360 * screenWidth, top: 8/720 * screenHeight ,right: 20/360 * screenWidth),
                  child: new RichText(
                    textAlign: TextAlign.left,
                    text: new TextSpan(
                      // Note: Styles for TextSpans must be explicitly defined.
                      // Child text spans will inherit styles from parent
                      style: TextStyle(
                        color: SemanticColors.light_black,
                        fontSize: (14 / 360) * screenWidth,
                        fontFamily: "IBM Plex Sans",

                      ),

                      children: <TextSpan>[
                        new TextSpan(text:MyplanBookMentorScreenString.note, style: new TextStyle(fontWeight: FontWeight.bold)),
                        new TextSpan(text: MyplanBookMentorScreenString.noteText),
                      ],
                    ),
                  ),
                ),
                /** end point of note **/
                /**Start point of available dates**/
                Container(
                  margin: EdgeInsets.fromLTRB(20, 30.5, 0.0, 0.0),
                  child: Align(
                    alignment: Alignment.topLeft,
                    child: Text(
                      MyplanBookMentorScreenString.AvailableDates,
                      style: TextStyle(
                          color: SemanticColors.light_black,
                          fontSize: (14 / 360) * screenWidth,
                          fontFamily: "IBM Plex Sans",
                          fontWeight: FontWeight.bold
                      ),
                      textAlign: TextAlign.left,
                    ),
                  ),
                ),
                noDate == false
                    ? (noDateavailable == null || noDateavailable == "" ?
                Container(
                  // margin: EdgeInsets.fromLTRB(20, 20, 20, 0.0),
                  // child: Text(
                  //   noDate ? noDateavailable :"No data Available",
                  //   style: TextStyle(
                  //     color: NeutralColors.blue_grey,
                  //     fontSize: (12 / 360) * screenWidth,
                  //     fontFamily: "IBM Plex Sans",
                  //   ),
                  //   textAlign: TextAlign.left,
                  // ),
                ) : Container(
                  margin: EdgeInsets.fromLTRB(20, 20, 20, 0.0),
                  child: Text(
                    noDateavailable,
                    style: TextStyle(
                      color: NeutralColors.mango,
                      fontSize: (12 / 360) * screenWidth,
                      fontFamily: "IBM Plex Sans",
                    ),
                    textAlign: TextAlign.left,
                  ),
                ))
                    : Container(
//              margin: EdgeInsets.fromLTRB(20, 20, 20, 0.0),
                  //height: 140.0,
                  child: GridView.builder(
                    shrinkWrap: true,
                    physics: NeverScrollableScrollPhysics(),
                    gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 3,
                      crossAxisSpacing: 0.0,
                      mainAxisSpacing: 0.0,
                      childAspectRatio: screenWidth / (screenHeight/4),
                    ),
                    //physics: NeverScrollableScrollPhysics(),
                    padding: const EdgeInsets.all(0.0),
                    addRepaintBoundaries: true,
                    itemCount: slotDate.length,
                    itemBuilder: (build, int indexDate) {
                      final item = slotDate[indexDate];
                      return Container(
                        height: 40/720 *screenHeight,
                        width: 100/360 *screenWidth,
                        margin: EdgeInsets.fromLTRB(10, 20, 10, 0.0),
                        child: FlatButton(
                          onPressed: () {
                            _onSelectedDate(indexDate);
                            if (indexDate != null) {
                              setState(() {
                                noTime = true;
                                _selectedIndexSlot = null;
                                _enabledDate = true;
                                getAvailableSlot();
                              });
                            }
                          },
                          color: _selectedIndexDate != null &&
                              _selectedIndexDate == indexDate
                              ? NeutralColors.purpleish_blue
                              : Colors.white,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.all(Radius.circular(20)),
                            side: BorderSide(
                              width: 1,
                              color: _selectedIndexDate != null &&
                                  _selectedIndexDate == indexDate
                                  ? Colors.transparent
                                  : SemanticColors.iceBlue,
                              style: BorderStyle.solid,
                            ),
                          ),
                          textColor: _selectedIndexDate != null &&
                              _selectedIndexDate == indexDate
                              ? Colors.white
                              : Color.fromARGB(255, 100, 100, 100),
                          padding: EdgeInsets.all(0.0),
                          child: Container(
                            child: Text(
                              item,
                              style: TextStyle(
                                fontSize: 14 / 720 * screenHeight,
                                fontFamily: "IBM Plex Sans",
                              ),
                              softWrap: false,
                              textAlign: TextAlign.left,
                            ),
                          ),
                        ),
                      );
                    },
                    //padding: EdgeInsets.all(0.0),
                  ),
                ),
                /**end point of available dates**/

                /**start point of available slot**/
                Container(
                  margin: EdgeInsets.fromLTRB(20, 30.5, 0.0, 0.0),
                  child: Align(
                    alignment: Alignment.topLeft,
                    child: Text(
                      MyplanBookMentorScreenString.AvailableSlots,
                      style: TextStyle(
                          color: SemanticColors.light_black,
                          fontSize: (14 / 360) * screenWidth,
                          fontFamily: "IBM Plex Sans",
                          fontWeight:  FontWeight.bold
                      ),
                      textAlign: TextAlign.left,
                    ),
                  ),
                ),
                noTime == false
                    ?Container(
                  // margin: EdgeInsets.fromLTRB(20, 20, 20, 0.0),
                  // child: Text(
                  //   "No data Available",
                  //   style: TextStyle(
                  //     color: NeutralColors.blue_grey,
                  //     fontSize: (12 / 360) * screenWidth,
                  //     fontFamily: "IBM Plex Sans",
                  //   ),
                  //   textAlign: TextAlign.left,
                  // ),
                )
                    :Container(
//              margin: EdgeInsets.fromLTRB(20, 20, 20, 0.0),
                  // height: 220.0,
                  child: GridView.builder(
                    shrinkWrap: true,
                    physics: NeverScrollableScrollPhysics(),
                    gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 3,
                      crossAxisSpacing: 0.0,
                      mainAxisSpacing: 0.0,
                      childAspectRatio: screenWidth / (screenHeight/4),
                    ),
                    //physics: NeverScrollableScrollPhysics(),
                    padding: const EdgeInsets.all(0.0),

                    itemCount: slotTime.length,
                    itemBuilder: (build, int indexSlot) {
                      final item = slotTime[indexSlot];
                      return Container(
                        height: 40/720*screenHeight,
                        width: 100/360*screenWidth,
                        margin: EdgeInsets.fromLTRB(10, 20, 10, 0.0),
                        child: FlatButton(
                          onPressed: () {
                            _onSelectedSlot(indexSlot);
                            if (indexSlot != null)
                              setState(() => _enabledSlot = true);
                            getCourseReasonTypes();
                          },
                          color: _selectedIndexSlot != null &&
                              _selectedIndexSlot == indexSlot
                              ? NeutralColors.purpleish_blue
                              : Colors.white,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.all(Radius.circular(20)),
                            side: BorderSide(
                              width: 1,
                              color: _selectedIndexSlot != null &&
                                  _selectedIndexSlot == indexSlot
                                  ? Colors.transparent
                                  : SemanticColors.iceBlue,
                              style: BorderStyle.solid,
                            ),
                          ),
                          textColor: _selectedIndexSlot != null &&
                              _selectedIndexSlot == indexSlot
                              ? Colors.white
                              : NeutralColors.index_color,
                          padding: EdgeInsets.all(0),
                          child: Text(
                            item,
                            style: TextStyle(
                              fontSize: 14 / 720 * screenHeight,
                              fontFamily: "IBM Plex Sans",
                            ),
                            textAlign: TextAlign.left,
                          ),
                        ),
                      );
                    },
                    //padding: EdgeInsets.all(0.0),
                  ),
                ),
                /**End point of available slot**/

                /**Start point Reason Container**/
                _enabledSlot == true
                    ? Container(
                  margin: EdgeInsets.only(left: 20/360 * screenWidth, top: 20/720 * screenHeight,right: 22/360 * screenWidth),
                  child: DropDownFormField(
                    getImmediateSuggestions: true,
                    // autoFlipDirection: true,
                    textFieldConfiguration: TextFieldConfiguration(
                        controller: _Reasons,
                        label: MyplanBookMentorScreenString.SelectYourReason),
                    suggestionsBoxDecoration: SuggestionsBoxDecoration(
                        dropDownHeight: 150,
                        borderRadius: new BorderRadius.circular(5.0),
                        color: Colors.white),
                    suggestionsCallback: (pattern) {
                      return courseReasonType;
                    },
                    itemBuilder: (context, suggestion) {
                      return ListTile(
                        dense: true,
                        contentPadding: EdgeInsets.only(left: 10),
                        title: Text(
                          suggestion,
                          style: TextStyle(
                            fontFamily: "IBMPlexSans",
                            fontWeight: FontWeight.w400,
                            fontSize: (13 / 720) * screenHeight,
                            color: NeutralColors.bluey_grey,
                            textBaseline: TextBaseline.alphabetic,
                            letterSpacing: 0.0,
                            inherit: false,
                          ),
                        ),
                      );
                    },
                    hideOnLoading: true,
                    debounceDuration: Duration(milliseconds: 100),
                    transitionBuilder: (context, suggestionsBox, controller) {
                      return suggestionsBox;
                    },
                    onSuggestionSelected: (suggestion) {
                      setState(() {
                        setState(() {
                          for(int i=0;i<courseReasonType.length;i++){
                            if(courseReasonType[i] == suggestion){
                              _selectedCourseName = suggestion;
                              _selectedCourseID = courseReasonTypeId[i];
                              print("============================"+_selectedCourseID);
                            }
                          }
                          _enabledReason = true;
                        });
                        _Reasons.text = suggestion;
                      });
                      _Reasons.text = suggestion;

                      print("text reason");
                      print(text_reason);
                    },
                    onSaved: (value) => {print("something")},
                  ),
                )
                    : Container(
                  margin: EdgeInsets.only(left: 20/360 * screenWidth, top: 20/720 * screenHeight,right: 22/360 * screenWidth),
                  child: DropDownFormField(
                    getImmediateSuggestions: true,
                    // autoFlipDirection: true,
                    textFieldConfiguration: TextFieldConfiguration(
                        controller: _Reasons,
                        label: MyplanBookMentorScreenString.SelectYourReason),
                    suggestionsBoxDecoration: SuggestionsBoxDecoration(
                        dropDownHeight: 150,
                        borderRadius: new BorderRadius.circular(5.0),
                        color: Colors.white),
                    suggestionsCallback: (pattern) {
                      return null;
                    },
                    itemBuilder: (context, suggestion) {
                      return ListTile(
                        dense: true,
                        contentPadding: EdgeInsets.only(left: 10),
                        title: Text(
                          suggestion,
                          style: TextStyle(
                            fontFamily: "IBMPlexSans",
                            fontWeight: FontWeight.w400,
                            fontSize: (13 / 720) * screenHeight,
                            color: NeutralColors.bluey_grey,
                            textBaseline: TextBaseline.alphabetic,
                            letterSpacing: 0.0,
                            inherit: false,
                          ),
                        ),
                      );
                    },
                    hideOnLoading: true,
                    debounceDuration: Duration(milliseconds: 100),
                    transitionBuilder: (context, suggestionsBox, controller) {
                      return null;
                    },
                    onSuggestionSelected: (suggestion) {

                    },
                    onSaved: (value) => {print("something")},
                  ),
                ),
                /**End point Reason Container**/
                Container(
                  height: 100,
                  margin: EdgeInsets.only(left: 20/360 * screenWidth, top: 30/720 * screenHeight ,right: 20/360 * screenWidth),
                  child: Stack(
                    alignment: Alignment.center,
                    children: [
                      Container(
                        child: Opacity(
                          opacity: 0.5,
                          child: Container(
                            height: 80/720 * screenHeight,
                            decoration: BoxDecoration(
                              color: Color.fromARGB(255, 255, 255, 255),
                              border: Border.all(
                                color: Color.fromARGB(255, 87, 93, 96)
                                    .withOpacity(0.20),
                                width: 1,
                              ),
                              borderRadius: BorderRadius.all(Radius.circular(5)),
                            ),
                            child: Container(
                              child: Padding(
                                padding: EdgeInsets.only(left: 8.0, right: 8.0),
                                child: TextField(
                                  controller: _messagecontroller,
                                  onSubmitted: (text){
                                    print("===============================enter text");
                                    print(text);
                                    text_reason = text;
                                    text_reason = _messagecontroller.text;
                                  },
                                  textInputAction: TextInputAction.done,
                                  decoration: InputDecoration(
                                      border: InputBorder.none,
                                      hintText: MyplanBookMentorScreenString.WhatWould,
                                      hintStyle:
                                      TextStyle(color: ImsColors.bluey_grey)),
                                  maxLines: 5,
                                  style: TextStyle(
                                    color: Colors.black,
                                    fontSize: 14,
                                    fontFamily: "IBM Plex Sans",
                                  ),
                                  textAlign: TextAlign.left,
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                Center(
                  child: Container(
                    margin: EdgeInsets.only(left: 20/360 * screenWidth, top: 8/720 * screenHeight ,right: 20/360 * screenWidth),
                    child: Text(
                      'Booking cancelled within 24 hours of session time will be treated as availed.',
                      style: TextStyle(
                        color: NeutralColors.grapefruit,
                        fontSize: (12 / 360) * screenWidth,
                        fontFamily: "IBM Plex Sans",
                      ),
                      textAlign: TextAlign.left,
                    ),
                  ),
                ),
                _enabledMentor &&
                    _enabledDate &&
                    _enabledSlot &&
                    _enabledVenue  &&
                    _enabledReason&&
                    (text_reason!=null&&text_reason!="")
                    ? GestureDetector(
                  onTap: () {
                    if(bookSlot_STATUSCLICK == false){
                      setState(() {
                        bookSlot_STATUSCLICK = true;
                      });
                      OTPGenerator();
                    }
                  },
                  child: Align(
                    alignment: Alignment.topCenter,
                    child: Container(
                      width: 270,
                      height: 40,
                      margin: EdgeInsets.only(top: 40/720 * screenHeight, bottom: 40.0/720 * screenHeight),
                      decoration: BoxDecoration(
                        gradient: LinearGradient(
                          begin: Alignment(-0.011, 0.494),
                          end: Alignment(1.031, 0.516),
                          stops: [
                            0,
                            1,
                          ],
                          colors: [
                            Color.fromARGB(255, 51, 128, 204),
                            Color.fromARGB(255, 137, 110, 216),
                          ],
                        ),
                        borderRadius: BorderRadius.all(Radius.circular(2)),
                      ),
                      child: Center(
                        child: Text(
                          MyplanBookMentorScreenString.BookSlot,
//
                          style: TextStyle(
                            color: Color.fromARGB(255, 255, 255, 255),
                            fontSize: 14,
                            fontFamily: "IBMPlexSans",
                            fontWeight: FontWeight.w500,
                          ),

                          textAlign: TextAlign.center,
                        ),
                      ),
                    ),
                  ),
                ) :
                GestureDetector(
                  // //TESTING+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                  // onTap: (){
                  //   OTPGenerator();
                  // },
                  child: Align(
                    alignment: Alignment.topCenter,
                    child: Container(
                      width: 270/360*screenWidth,
                      height: 40/720*screenHeight,
                      margin: EdgeInsets.only(top: 40/720 * screenHeight, bottom: 40.0/720 * screenHeight),
                      decoration: BoxDecoration(
                        gradient: LinearGradient(
                          begin: Alignment(-0.011, 0.494),
                          end: Alignment(1.031, 0.516),
                          stops: [
                            0,
                            1,
                          ],
                          colors: [
                            Color.fromARGB(255, 51, 128, 204),
                            Color.fromARGB(255, 137, 110, 216),
                          ],
                        ),
                        borderRadius: BorderRadius.all(Radius.circular(2)),
                      ),
                      child: Center(
                        child: Text(
                          MyplanBookMentorScreenString.BookSlot,
                          style:  TextStyle(
                            color: Color(0xFF7e919a),
                            fontSize: 14 / 360 * screenWidth,
                            fontFamily: "IBMPlexSans",
                            fontWeight: FontWeight.w500,
                          ),
                          textAlign: TextAlign.center,
                        ),
                      ),
                    ),
                  ),
                ),


                /** start point of ims centre **/
                Center(
                  child: Container(
                    margin: EdgeInsets.only(left: 20/360 * screenWidth, bottom: 25/720 * screenHeight ,right: 20/360 * screenWidth),
                    child: Row(
                      children: [
                        Container(
                            child:Text(MyplanBookMentorScreenString.connect,
                                style: TextStyle(
                                  color: SemanticColors.light_black,
                                  fontSize: (14 / 360) * screenWidth,
                                  fontFamily: "IBM Plex Sans",
                                  // add add underline in text
                                )
                            )
                        ),
                        Link(
                          child: Text(MyplanBookMentorScreenString.hyperLinkForIms, style: TextStyle(
                            decoration: TextDecoration.underline,
                            color: PrimaryColors.azure_Dark,
                            fontSize: (14 / 360) * screenWidth,
                            fontFamily: "IBM Plex Sans",
                            // add add underline in text
                          ),),
                          url: MyplanBookMentorScreenString.imsSupportUrl,
                          onError: _showErrorSnackBar,
                        ),
                      ],
                    ),
                  ),
                ),
                /** end point of ims centre **/


              ],
            ),
          ),
        ),
      ),
    );
  }

  void _showErrorSnackBar() {
    Scaffold.of(context).showSnackBar(
      SnackBar(
        content: Text('Oops... the URL couldn\'t be opened!'),
      ),
    );
  }
  void getStudentImsPin_pref() async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    studentImsPin = prefs.getString(ImsStrings.sCurrentUserId);
    print("=============================studentImsPin");
    print(studentImsPin);
  }

  void getmyImsCourseId_pref() async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    myImsCourseId = prefs.getString("courseId");
    print("=============================courseId");
    print(studentImsPin);
  }

  void getTotalSlots_pref() async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    if(prefs.getInt("totalSlots") != null){
      totalSlots = prefs.getInt("totalSlots");
    }else{
      totalSlots = 0;
    }
    print("=============================totalSlots");
    print(prefs.getInt("totalSlots"));
  }

  void getAvailableSlots_pref() async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    if(prefs.getInt("availableSlots") != null){
      availableSlots = prefs.getInt("availableSlots");
      availableSlots = availableSlots+1;
    }else{
      availableSlots = 0;
    }
    print("=============================availableSlots");
    print(availableSlots);
  }

  void getSlotUsed_pref()async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    if(prefs.getInt("slotsUsed") != null){
      usedSlots = prefs.getInt("slotsUsed");
      usedSlots = usedSlots + 1;
    }else{
      usedSlots = 0;
    }
    print("=============================usedSlots");
    print(usedSlots);
  }

//API TO LOAD VENUE DETAILS BASED ON studentImsPin
  void getVenueDetails()async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    studentImsPin = prefs.getString(ImsStrings.sCurrentUserId);
    myImsCourseId_venue = prefs.getString("courseId");
    branchName = [];
    branchId = [];
    Map<String, dynamic> postdata_venue;
    postdata_venue = {
      "myImsCourseId":myImsCourseId_venue,
      "studentImsPin": studentImsPin //"99a77771" //studentImsPin //"99a77771"
    };
    print(postdata_venue);
    print("post data");
    var _headers = {"Content-Type": "application/json"};
    ApiService().postAPITerr(URL.myPLAN_API_VENUE_DETAILS, postdata_venue, global.headers).then((result) {
      setState(() {
        if (result[0]['success'].toString().toLowerCase() == 'true'.toLowerCase()){
          venueData = result[0]['data'];
          for(int i = 0; i < venueData.length; i++){
            print("--------------------inside for venueloop");
            print(venueData[i]['branchId']);
            print(venueData[i]['branchName']);
            branchName.add(venueData[i]['branchName']);
            branchId.add(venueData[i]['branchId'].toString());
          }
        };
      });
    });
  }

//API TO LOAD MENTOR DEATILS
  void getMentorDetails(selectedbranchID)async{
    _Mentors.text = '';
    _Reasons.text = '';
    _messagecontroller.text = '';
    mentorName = [];
    mentorID = [];
    slotIDBasedonDate = [];
    slotDate = [];
    slotStatusBasedonDate = [];
    slotIDBasedonTime = [];
    slotStatusBasedonTime=[];
    slotTime = [];
    slotID = [];
    noDateavailable = "";
    noDate = false;
    noTime = false ;
    _selectedIndexDate = null;
    Map<String, dynamic> postdata_mentor;
    SharedPreferences prefs = await SharedPreferences.getInstance();
    myImsCourseId = prefs.getString("courseId");
    postdata_mentor = {
      "branchId":_selectedbranchID,
      "myImsCourseId":myImsCourseId
    };
    ApiService().postAPITerr(URL.myPLAN_API_MENTOR_DETAILS,postdata_mentor,global.headers).then((returnValue) {
      print("====================123");
      print(URL.myPLAN_API_MENTOR_DETAILS);
      print(returnValue[0]['data']);
      setState(() {
        if(returnValue[0]['success'].toString().toLowerCase() == 'true'.toLowerCase()){
          mentorData = returnValue[0]['data'];
          print(mentorData.length);
          for(int i = 0; i < mentorData.length; i++){
            print("--------------------inside for loop");
            mentorName.add(mentorData[i]['mentorName']);
            mentorID.add(mentorData[i]['mentorId'].toString());
          }
        }
        print(mentorName.length);
      });
    });
  }

//API TO LOAD DATES AVAILABLE BASED ON BRANCHID and VENUE ID
  void getAvailableDates()async{
    slotIDBasedonDate = [];
    slotDate = [];
    slotStatusBasedonDate = [];
    Map<String, dynamic> postdata_dates;
    postdata_dates = {
      "mentorId": _selectedmentorID , //"2",
      "branchId": _selectedbranchID,//"122"
      "mode":mode
    };
    print("-------------------------------print avialable of datesaaaaaaaaaaaaaaaaaaaa");
    print(URL.myPLAN_API_DATES_AVAILABLE);
    print(postdata_dates);
    ApiService().postAPITerr(URL.myPLAN_API_DATES_AVAILABLE, postdata_dates, global.headers).then((result) {
      print("===============================COmingelse");
      print(result[0]['success']);
      setState(() {
        if(result[0]['success'].toString().toLowerCase() == 'true'.toLowerCase()){
          noDate = true;
          print(result[0]['data']);
          dataforavailableDate = result[0]['data'];
          print("=================================length");
          print(dataforavailableDate.length);
          validationForMode="";
          for(int i=0;i<dataforavailableDate.length;i++){
            slotIDBasedonDate.add(dataforavailableDate[i]['slotId'].toString());
            if(slotDate.contains(dataforavailableDate[i]['slotDate'].toString())){
              print("==================================it contains");
            }else{
              print("==================================it notcontains");
              slotDate.add(dataforavailableDate[i]['slotDate'].toString());
            }
            slotStatusBasedonDate.add(dataforavailableDate[i]['slotStatus'].toString());
          }
          print(slotDate.length);
        }else{
          if(result[0].containsKey("message")){
            if(result[0]['message']=='No Record found'){
              noDateavailable =MyplanBookMentorScreenString.noSlots;
              noDate = false;
              noTime = false;
              _Reasons.text = '';
              _enabledDate = false;
              _enabledSlot = false;
            }else{

            }
          }
        }
      });
    });
  }


//API TO LOAD AVAILABLE SLOTS BASED ON mentionID,branchId,selectedDate
  void getAvailableSlot()async{
    slotIDBasedonTime = [];
    slotStatusBasedonTime=[];
    slotTime = [];
    slotID = [];
    Map<String, dynamic> postdata_slots;
    postdata_slots = {
      "mentorId":_selectedmentorID,
      "branchId":_selectedbranchID,
      "selectedDate":slotDate[_selectedIndexDate],
      "mode":mode
    };
    print("==========================slotpostdata");
    print(postdata_slots);
    ApiService().postAPITerr(URL.myPLAN_API_SLOTS_AVAILABLE, postdata_slots, global.headers).then((result) {
      setState(() {
        if(result[0]['success'].toString().toLowerCase() == 'true'.toLowerCase()){
          dataforavailableSlot = result[0]['data'];
          print("========================available SLOTTTTTTTTTTTTTTT");
          print(dataforavailableSlot);
          for(int i=0;i<dataforavailableSlot.length;i++){
            if(dataforavailableSlot[i]['slotStatus'] == 'AVAILABLE'){
              print("===============================AVAILABLE time are");
              slotIDBasedonTime.add(dataforavailableSlot[i]['slotId'].toString());
              slotStatusBasedonTime.add(dataforavailableSlot[i]['slotStatus'].toString());
              if(slotTime.contains(dataforavailableDate[i]['slotTime'].toString())){
                print("==================================it contains slot");
              }else{
                print("==================================it notcontains slot");
                slotTime.add(dataforavailableSlot[i]['slotTime'].toString());
              }
              slotID.add(dataforavailableSlot[i]['slotId'].toString());
            }else{}
          }
        }else{
          print("===============================COmingelse");
          print(result);
        }
        print(slotTime.length);
      });
    });
  }


//API TO LOAD COURSE REASON TYPE BASED ON myImsCourseId
  void getCourseReasonTypes()async{
    courseReasonType = [];
    courseReasonTypeId = [];
    Map<String, dynamic> postdata_reasonType;
    postdata_reasonType = {
      "myImsCourseId":myImsCourseId
    };
    ApiService().postAPITerr(URL.myPLAN_API_COURSE_REASON_TYPE, postdata_reasonType, global.headers).then((result) {
      setState(() {
        if(result[0]['success'].toString().toLowerCase() == 'true'.toLowerCase()){
          dataforcourseReasonTypes = result[0]['data'];
          for(int i=0;i<dataforcourseReasonTypes.length;i++){
            print("===================================11111111111111");
            print(dataforcourseReasonTypes[i]);
            courseReasonTypeId.add(dataforcourseReasonTypes[i]['courseReasonTypeId'].toString());
            courseReasonType.add(dataforcourseReasonTypes[i]['courseReasonType'].toString());
          }
        }else{
        }
      });
    });
  }

//API FOR SEND OTP
  void OTPGenerator()async{
    Map<String, dynamic> postdata_otpgen;
    SharedPreferences prefs = await SharedPreferences.getInstance();
    studentImsPin = prefs.getString(ImsStrings.sCurrentUserId);
    studentEmail = prefs.getString(ImsStrings.sCurrentUserEmail);
    studentName = prefs.getString("_studentName");
    postdata_otpgen = {
      "studentImsPin":studentImsPin
    };
    print("==========================resonty[ppppppppppppppppp");
    print(postdata_otpgen);
    print("==================ALL data for verification");
    print(text_reason);
    print(_messagecontroller.text);
    print("branchId");
    print(_selectedbranchID);
    print("mentorId");
    print(_selectedmentorID);
    print("slotDate" + slotDate[_selectedIndexDate].toString());
    print("slotTime" + slotTime[_selectedIndexSlot].toString());
    print("imsPin" + studentImsPin);
    print("studentName" + studentName.toString());
    print("studentEmail" +studentEmail);
    print("slotType");
    print("courseName");
    print("reasonForBooking");
    print("courseReasonTypeId");
    print("otp");
    print("slotID" + slotID[_selectedIndexSlot].toString());
    // Navigator.push(
    //     context,
    //     MaterialPageRoute(
    //         builder: (context) =>
    //             OtpScreen(_selectedbranchID,
    //                 _selectedmentorID,
    //                 slotDate[_selectedIndexDate],
    //                 slotTime[_selectedIndexSlot],
    //                 "1",
    //                 _selectedCourseName,
    //                 text_reason,
    //                 _selectedCourseID,
    //                 "abc",
    //                 "bbbbb")));
    ApiService().postAPITerr(URL.myPLAN_API_OTPGenerator, postdata_otpgen, global.headers).then((result) {
      setState(() {
        if(result[0]['success'].toString().toLowerCase() == 'true'.toLowerCase()){
          bookSlot_STATUSCLICK = false;
          print(result[0]['success'].toString());
          showMessage(result[0]['message'].toString());
          print("==================ALL data for verification");
          print("branchId");
          print(_selectedbranchID);
          print("mentorId");
          print(_selectedmentorID);
          print("slotDate" + slotDate[_selectedIndexDate].toString());
          print("slotTime" + slotTime[_selectedIndexSlot].toString());
          print("imsPin" + studentImsPin);
          print("studentName" + studentName);
          print("studentEmail" +studentEmail);
          print("slotType");
          print("courseName");
          print("reasonForBooking");
          print("courseReasonTypeId");
          print("otp");
          Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) =>
                      OtpScreen(_selectedbranchID,
                          _selectedmentorID,
                          slotDate[_selectedIndexDate],
                          slotTime[_selectedIndexSlot],
                          slotID[_selectedIndexSlot].toString(),
                          "1",
                          _selectedCourseName,
                          _messagecontroller.text,
                          _selectedCourseID,
                          result[0]['message'].toString(),
                          result[0]['mobile'][0].toString())));
        }else{
          bookSlot_STATUSCLICK = false;
        }
      });
    });
  }

  void removeDuplicateDate() {

  }

}
class VenueService {
  static Future<List<String>> getSuggestions(String query) async {
    print("=================================================ReasonType");
    print(branchName.length);
    branchName.retainWhere((s) => s.toLowerCase().contains(query.toLowerCase()));
    return branchName;
  }
}

class MentorService {
  static Future<List<String>> getSuggestions(String query) async {
    mentorName.retainWhere((s) => s.toLowerCase().contains(query.toLowerCase()));
    return mentorName;
  }
}

class ReasonType {
  static List<String> getSuggestions(String query) {
    print("=================================================ReasonType");
    print(courseReasonType.length);
    courseReasonType.retainWhere((s) => s.toLowerCase().contains(query.toLowerCase()));
    return courseReasonType;
  }
}

List<RadioModel> sampleData = new List<RadioModel>();
List<String> buttonNames=["Virtual","In-Centre"];

addRadioModel(){
  for(var i=0; i<buttonNames.length;i++){
    print("here");
    print(buttonNames.length);
    sampleData.add(new RadioModel(false, buttonNames[i]));
  }
}
class RadioItem extends StatelessWidget {
  final RadioModel _item;
  RadioItem(this._item);
  @override
  Widget build(BuildContext context) {
    final screenHeight = MediaQuery.of(context).size.height;
    final screenWidth = MediaQuery.of(context).size.width;
    return new Container(
      width: 152/360*screenWidth,
      margin: new EdgeInsets.only(top:15.0,bottom: 15.0,left: 20),
      child: new Container(
        child: new Center(
          child: new Text(_item.buttonText,
              style: new TextStyle(
                  color:
                  _item.isSelected ? PrimaryColors.azure_Dark : Colors.black,
                  //fontWeight: FontWeight.bold,
                  fontSize: 18.0)),
        ),
        decoration: new BoxDecoration(
          color: _item.isSelected
              ? Colors.transparent
              : SemanticColors.light_ice_blue,
          border: new Border.all(
              width: 1.0,
              color: _item.isSelected
                  ? PrimaryColors.azure_Dark
                  : SemanticColors.light_ice_blue),
          borderRadius: const BorderRadius.all(const Radius.circular(2.0)),
        ),
      ),
    );
  }
}

class RadioModel {
  bool isSelected;
  final String buttonText;

  RadioModel(this.isSelected, this.buttonText);
}
import 'package:flutter/material.dart';
//import 'package:flutter_typeahead/flutter_typeahead.dart';
import 'package:imsindia/utils/colors.dart';
import 'package:imsindia/utils/emaximiser_svg_icons.dart';
import 'package:imsindia/views/Arun/ims_utility.dart';
import 'package:imsindia/views/Arun/myPLAN/myPLAN_bookmentor_screen.dart';

import '2_w_meetingconfirmation.dart';

const actualHeight = 768;
const actualWidth = 360;
var screenWidthTotal;
var screenHeightTotal;
var _safeAreaHorizontal;
var _safeAreaVertical;
var screenHeight;
var screenWidth;
var monVal = true;
bool _enabledVenue = false;
bool _enabledMentor = false;

bool _enabledReason = false;
final TextEditingController _Venues = new TextEditingController();

final TextEditingController _Mentors = new TextEditingController();

class BookAMentor_2_w extends StatefulWidget {
  final FocusNode _firstInputFocusNode = new FocusNode();
  final TextEditingController __helpTopic = new TextEditingController();
  final TextEditingController _mentor = new TextEditingController();

  void onMentorSlotBookingPressed(BuildContext context) => Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => MeetingConfirmation_2_w()));

  @override
  _BookAMentor_2_wState createState() => _BookAMentor_2_wState();
}

class _BookAMentor_2_wState extends State<BookAMentor_2_w> {
  void onMentorSlotBookingPressed(BuildContext context) => Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => MeetingConfirmation_2_w()));
  String _venue = 'St. Jude’s High School1';
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _Venues.text = '';
    _Mentors.text = '';
    _enabledVenue = false;
    _enabledMentor = false;
  }

  @override
  Widget build(BuildContext context) {
    CalculateScreen(context);
    return Scaffold(
      appBar: AppBar(
        brightness: Brightness.light,
        elevation: 0.0,
        centerTitle: false,
        titleSpacing: 0.0,
        backgroundColor: Colors.white,
        title: Text(
          "Book a Mentor",
          style: const TextStyle(
              color: Colors.black,
              fontWeight: FontWeight.w500,
              fontFamily: "IBMPlexSans",
              fontStyle: FontStyle.normal,
              fontSize: 16.0),
        ),
        leading: IconButton(
          icon: getSvgIcon.backSvgIcon,
          onPressed: () => Navigator.pop(context, false),
        ),
      ),
      body: SingleChildScrollView(
//        constraints: BoxConstraints.expand(),
//        decoration: BoxDecoration(
//          color: Color.fromARGB(255, 255, 255, 255),
//        ),
        child: Container(
          color: Colors.white,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                height: 23,
                margin: EdgeInsets.only(left: 10, top: 30, right: 129),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    Align(
                      alignment: Alignment.topLeft,
                      child: Container(
                        width: 171,
                        height: 23,
                        child: FlatButton(
                          onPressed: () {},
                          color: Colors.transparent,
                          textColor: Color.fromARGB(255, 0, 3, 44),
                          padding: EdgeInsets.all(0),
                          child: Text(
                            "Mentor Slot Booking",
                            style: TextStyle(
                              fontSize: 18,
                              fontFamily: "IBM Plex Sans",
                              fontWeight: FontWeight.w700,
                            ),
                            textAlign: TextAlign.left,
                          ),
                        ),
                      ),
                    ),
                    Expanded(
                      flex: 1,
                      child: Align(
                        alignment: Alignment.topLeft,
                        child: Container(
                          height: 25,
                          width: 35,
                          margin: EdgeInsets.only(left: 10, top: 2),
                          decoration: BoxDecoration(
                            color: Color.fromARGB(26, 65, 125, 206),
                            borderRadius: BorderRadius.all(Radius.circular(10)),
                          ),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Container(
                                margin: EdgeInsets.only(left: 6, top: 2),
                                child: Text(
                                  "1/3",
                                  style: TextStyle(
                                    color: Color.fromARGB(255, 122, 113, 213),
                                    fontSize: 14,
                                    fontFamily: "IBM Plex Sans",
                                    fontWeight: FontWeight.w500,
                                  ),
                                  textAlign: TextAlign.left,
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Align(
                alignment: Alignment.topLeft,
                child: Container(
                  margin: EdgeInsets.only(left: 20, top: 20),
                  child: Opacity(
                    opacity: 0.5,
                    child: Text(
                      "Choose Your Mentor",
                      style: TextStyle(
                        color: Color.fromARGB(255, 126, 145, 154),
                        fontSize: 12,
                        fontFamily: "IBM Plex Sans",
                      ),
                      textAlign: TextAlign.left,
                    ),
                  ),
                ),
              ),
              Container(
                height: 18,
                margin: EdgeInsets.only(left: 20, top: 20, right: 20),
//                 child: TypeAheadFormField(
//                   getImmediateSuggestions: true,
//                   textFieldConfiguration: TextFieldConfiguration(
//                     cursorColor: Colors.white,
//                     cursorWidth: 0.0,
//                     decoration: InputDecoration(
//                       border: InputBorder.none,
// //                      focusedBorder: UnderlineInputBorder(
// //                          borderSide: BorderSide(
// //                              color:
// //                              SemanticColors.dark_purpely,
// //                              width: 1.5)),
//                       labelText: '',
//                       labelStyle: TextStyle(
//                           fontFamily: "IBMPlexSans",
//                           fontSize: 14 / 360 * screenWidth,
//                           fontWeight: FontWeight.w400,
//                           color: NeutralColors.blue_grey,
//                           inherit: false,
//                           height: 0),
//                       contentPadding: EdgeInsets.only(bottom: 10.0),
//                       suffixIcon: IconButton(
//                           //  disabledColor: Colors.red,
//                           padding:
//                               EdgeInsets.only(left: 30 / 360 * screenWidth),
//                           icon: const Icon(Icons.keyboard_arrow_down,
//                               color: Colors.grey),
//                           //     iconSize: 5,
//
//                           //   color:Colors.red,
//                           onPressed: () {}),
// //                      enabledBorder: UnderlineInputBorder(
// //                          borderSide: BorderSide(
// //                              color:
// //                              SemanticColors.dark_purpely,
// //                              width: 1.5)),
//                     ),
//                     controller: _Mentors,
//                   ),
//                   suggestionsCallback: (pattern) {
//                     if (pattern.toString().isEmpty) {
//                       _enabledMentor = false;
//                     }
//                     return MentorService.getSuggestions(pattern);
//                   },
//                   itemBuilder: (context, suggestion) {
//                     return Container(
//                       color: Colors.white,
//                       child: ListTile(
//                         title: Text(suggestion),
//                       ),
//                     );
//                   },
//                   transitionBuilder: (context, suggestionsBox, controller) {
//                     return suggestionsBox;
//                   },
//                   onSuggestionSelected: (suggestion) {
//                     setState(() {
//                       _enabledMentor = true;
//                     });
//                     _Mentors.text = suggestion;
//                     // this._typeAheadController.dispose();
//                   },
//                 ),
              ),
              Container(
                height: 1,
                margin: EdgeInsets.only(left: 20, top: 5, right: 20),
                decoration: BoxDecoration(
                  color: Color.fromARGB(255, 136, 109, 215),
                ),
                child: Container(),
              ),
              Align(
                alignment: Alignment.topLeft,
                child: Container(
                  margin: EdgeInsets.only(left: 20, top: 21),
                  child: Opacity(
                    opacity: 0.5,
                    child: Text(
                      "Pick category you need help with",
                      style: TextStyle(
                        color: Color.fromARGB(255, 126, 145, 154),
                        fontSize: 12,
                        fontFamily: "IBM Plex Sans",
                      ),
                      textAlign: TextAlign.left,
                    ),
                  ),
                ),
              ),
              Container(
                height: 18,
                margin: EdgeInsets.only(left: 20, top: 20, right: 20),
//                 child: TypeAheadFormField(
//                   getImmediateSuggestions: true,
//                   suggestionsBoxController: SuggestionsBoxController(),
//                   textFieldConfiguration: TextFieldConfiguration(
//                     cursorColor: Colors.white,
//                     cursorWidth: 0.0,
//                     decoration: InputDecoration(
//                       border: InputBorder.none,
// //                      focusedBorder: UnderlineInputBorder(
// //                          borderSide: BorderSide(
// //                              color:
// //                              SemanticColors.dark_purpely,
// //                              width: 1.5)),
//                       labelText: '',
//                       labelStyle: TextStyle(
//                           fontFamily: "IBMPlexSans",
//                           fontSize: 14 / 360 * screenWidth,
//                           fontWeight: FontWeight.w400,
//                           color: NeutralColors.blue_grey,
//                           inherit: false,
//                           height: 0),
//                       contentPadding: EdgeInsets.only(bottom: 10.0),
//                       suffixIcon: IconButton(
//                           //  disabledColor: Colors.red,
//                           padding:
//                               EdgeInsets.only(left: 30 / 360 * screenWidth),
//                           icon: const Icon(Icons.keyboard_arrow_down,
//                               color: Colors.grey),
//                           //     iconSize: 5,
//
//                           //   color:Colors.red,
//                           onPressed: () {}),
// //                      enabledBorder: UnderlineInputBorder(
// //                          borderSide: BorderSide(
// //                              color:
// //                              SemanticColors.dark_purpely,
// //                              width: 1.5)),
//                     ),
//                     controller: _Venues,
//                   ),
//                   suggestionsCallback: (pattern) {
//                     if (pattern.toString().isEmpty) {
//                       _enabledVenue = false;
//                     }
//                     return VenueService.getSuggestions(pattern);
//                   },
//                   itemBuilder: (context, suggestion) {
//                     return Container(
//                       color: Colors.white,
//                       child: ListTile(
//                         title: Text(suggestion),
//                       ),
//                     );
//                   },
//                   transitionBuilder: (context, suggestionsBox, controller) {
//                     return suggestionsBox;
//                   },
//                   onSuggestionSelected: (suggestion) {
//                     setState(() {
//                       _enabledVenue = true;
//                     });
//                     _Venues.text = suggestion;
//                     // this._typeAheadController.dispose();
//                   },
//                 ),
              ),
              Container(
                height: 1,
                margin: EdgeInsets.only(left: 20, top: 5, right: 20),
                decoration: BoxDecoration(
                  color: Color.fromARGB(255, 136, 109, 215),
                ),
                child: Container(),
              ),
              Container(
                height: 100,
                margin: EdgeInsets.only(left: 20, top: 31, right: 20),
                child: Stack(
                  alignment: Alignment.center,
                  children: [
                    Container(
                      child: Opacity(
                        opacity: 0.5,
                        child: Container(
                          height: 80,
                          decoration: BoxDecoration(
                            color: Color.fromARGB(255, 255, 255, 255),
                            border: Border.all(
                              color: Color.fromARGB(255, 87, 93, 96)
                                  .withOpacity(0.20),
                              width: 1,
                            ),
                            borderRadius: BorderRadius.all(Radius.circular(5)),
                          ),
                          child: Container(
                            child: Padding(
                              padding: EdgeInsets.only(left: 8.0, right: 8.0),
                              child: TextField(
                                onSubmitted: (v) {
                                  _enabledReason = true;
                                },
                                // focusNode: _firstInputFocusNode,
                                textInputAction: TextInputAction.done,
                                decoration: InputDecoration(
                                    border: InputBorder.none,
                                    hintText: 'What would you like help with?',
                                    hintStyle:
                                        TextStyle(color: ImsColors.bluey_grey)),
                                maxLines: 5,
                                style: TextStyle(
                                  color: Colors.black,
                                  fontSize: 14,
                                  fontFamily: "IBM Plex Sans",
                                ),
                                textAlign: TextAlign.left,
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Align(
                alignment: Alignment.topCenter,
                child: GestureDetector(
                  onTap: () {
                    if (_enabledMentor && _enabledVenue) {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => MeetingConfirmation_2_w()));
                    } else {
                      return null;
//                      Fluttertoast.showToast(
//                          msg: 'Please select required values',
//                          toastLength: Toast.LENGTH_SHORT,
//                          gravity: ToastGravity.CENTER,
//                          timeInSecForIos: 1,
//                          backgroundColor: Colors.red,
//                          textColor: Colors.white,
//                          fontSize: 16.0);
                    }
                  },
                  child: Container(
                    alignment: Alignment.center,
                    width: 270,
                    height: 40,
                    margin: EdgeInsets.only(top: 40),
                    decoration: BoxDecoration(
                      gradient: LinearGradient(
                        begin: Alignment(-0.011, 0.494),
                        end: Alignment(1.031, 0.516),
                        stops: [
                          0,
                          1,
                        ],
                        colors: [
                          Color.fromARGB(255, 51, 128, 204),
                          Color.fromARGB(255, 137, 110, 216),
                        ],
                      ),
                      borderRadius: BorderRadius.all(Radius.circular(2)),
                    ),
                    child: Text(
                      "SEND REQUEST",
                      style: _enabledVenue && _enabledMentor
                          ? TextStyle(
                              color: Color.fromARGB(255, 255, 255, 255),
                              fontSize: 14,
                              fontFamily: "IBMPlexSans",
                              fontWeight: FontWeight.w500,
                            )
                          : TextStyle(
                              color: Color(0xFF7e919a),
                              fontSize: 14 / 360 * screenWidth,
                              fontFamily: "IBMPlexSans",
                              fontWeight: FontWeight.w500,
                            ),
                      textAlign: TextAlign.center,
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class ListOfMentors {
  static final List<String> courses = [
    'Mr  k . k Gupta',
    'Mr R. S Arora',
    'Arun Kumar',
    'Vineet Kumar',
    'Sumeet Kumar',
  ];
  static List<String> getSuggestions(String query) {
    List<String> matches = List();
    matches.addAll(courses);
    //matches.retainWhere((s) => s.toLowerCase().contains(query.toLowerCase()));
    return matches;
  }
}

class CategoryService {
  static final List<String> categories = [
    'SCHOOL1',
    'COLLEGE 1',
    'School Vidya 1',
    'College Sandhya 2',
    'Kolkata'
  ];

  static List<String> getSuggestions(String query) {
    List<String> matches = List();
    matches.addAll(categories);

    matches.retainWhere((s) => s.toLowerCase().contains(query.toLowerCase()));
    return matches;
  }
}

class MentorService {
  static final List<String> mentors = [
    'Mr. Arun Kumar Chaudhary',
    'Ms. Sunidhi xyz',
    'Smt. Suchitra Sinha',
    'Mr RAJ KUMAR',
    'Kolkata'
  ];

  static List<String> getSuggestions(String query) {
    List<String> matches = List();
    matches.addAll(mentors);

    matches.retainWhere((s) => s.toLowerCase().contains(query.toLowerCase()));
    return matches;
  }
}

void CalculateScreen(BuildContext context) {
  var _mediaQueryData = MediaQuery.of(context);
  screenWidthTotal = _mediaQueryData.size.width;
  screenHeightTotal = _mediaQueryData.size.height;
  _safeAreaHorizontal =
      _mediaQueryData.padding.left + _mediaQueryData.padding.right;
  _safeAreaVertical =
      _mediaQueryData.padding.top + _mediaQueryData.padding.bottom;

  screenHeight = screenHeightTotal - _safeAreaVertical;
  screenWidth = screenWidthTotal - _safeAreaHorizontal;
}

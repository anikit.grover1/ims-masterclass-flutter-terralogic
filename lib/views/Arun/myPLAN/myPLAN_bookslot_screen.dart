import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:imsindia/api/bloc/services.dart';
import 'package:imsindia/components/error_screen.dart';
import 'package:imsindia/resources/strings/myplan.dart';
import 'package:imsindia/utils/Url.dart';
import 'package:imsindia/utils/colors.dart';
import 'package:imsindia/views/Arun/ims_utility.dart';
import 'package:imsindia/views/home_pages/home_widget.dart';
import 'package:intl/intl.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sliding_up_panel/sliding_up_panel.dart';
import 'myPLAN_bookmentor_screen.dart';
import 'myPLAN_confirmation_booking_screen.dart';
import 'package:imsindia/utils/global.dart' as global;

const actualHeight = 768;
const actualWidth = 360;
var screenWidthTotal;
var screenHeightTotal;
var _safeAreaHorizontal;
var _safeAreaVertical;
var screenHeight;
var screenWidth;
var monVal = true;

var totalSlots;
var totalDataForBookings;
List dateFormatter(String date) {
  DateFormat format = DateFormat("yyyy-MM-dd");
  var dateAfterConv =format.parse(date);
  dynamic dayData =
      '{ "1" : "Mon", "2" : "Tue", "3" : "Wed", "4" : "Thur", "5" : "Fri", "6" : "Sat", "7" : "Sun" }';

  dynamic monthData =
      '{ "1" : "Jan", "2" : "Feb", "3" : "Mar", "4" : "Apr", "5" : "May", "6" : "June", "7" : "Jul", "8" : "Aug", "9" : "Sep", "10" : "Oct", "11" : "Nov", "12" : "Dec" }';
  return [json.decode(dayData)['${dateAfterConv.weekday}'],
    dateAfterConv.day.toString(),
    json.decode(monthData)['${dateAfterConv.month}'] ,
    dateAfterConv.year.toString()];
}

class MyplanBookslotScreen extends StatefulWidget {
  final avaliableSlots;
  final slotsUsed;
  MyplanBookslotScreen(this.avaliableSlots,this.slotsUsed);
  @override
  _MyplanBookslotScreenState createState() => _MyplanBookslotScreenState();
}

class _MyplanBookslotScreenState extends State<MyplanBookslotScreen>
{
  bool clickedbookSlot= false;
  Future _future;
  var imsPin;
  var imsCourseId;
  bool dataCheckInBookingsSlot = false;
  var booking;
  var imspin;
  var imsPinForBookingsData;
  var imsCourseIdForBookingsData;
  int avaliableSlots;
  int slotsUsed;
  var slot_available;
  var slots_used;
  var slot_total;
  var bookingsavailabledata;

  List BookingsData=[];
  List _availableSlots=[];
  List _slotsUsed=[];
  List  _totalSlots=[];
  final myPlanControllerForPopUp = PanelController();
  double _myPlanPanelHeightClosed = 0.0;
  bool myPlanPopUpForBookSlotButton = false;



  Future getBookingsDetails() async {
    print("coming here");
  SharedPreferences prefs = await SharedPreferences.getInstance();
  imsPinForBookingsData = prefs.getString(ImsStrings.sCurrentUserLoginId);
  imsCourseIdForBookingsData = prefs.getString("courseId");
    print("valuesssssssssssssssssssssss");


  // var result = await ApiService().getAPITerr('https://getProjectList');
  Map<String, dynamic> postData;
  postData = {
    "studentImsPin":imsPinForBookingsData.toString(),
    "myImsCourseId":imsCourseIdForBookingsData,
  };


  final result = await ApiService().postAPITerr(URL.myPLAN_bookingsData, postData, global.headers);

  if (result[0]['success']==true){
    setState(() {
      totalDataForBookings=result[0]['data'];
    });
    return totalDataForBookings;
  }
  else{
    // totalDataForBookings = result[0]['message'];
    if(result[0].containsKey("message")){
      if(result[0]['message']=='No Record found'){
        totalDataForBookings = result[0]['message'];
      }else{
        totalDataForBookings=result[0]['message'];
      }
    }else{
      totalDataForBookings=result[0];
    }
    return totalDataForBookings;
  }
}

  void bookingsAvailable() async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    imsCourseId = prefs.getString("courseId");
    imsPin = prefs.getString(ImsStrings.sCurrentUserId);
    Map<String, dynamic> postData;
    postData = {
      "studentImsPin":imsPin,//"99a77771",
      "myImsCourseId":imsCourseId //"500"
    };
    print("================================ZZZZZZZZZZZZZZZZZZZZZZZZZZZZZ");
    print(postData);
    ApiService().postAPITerr(URL.myPLAN_API_BOOKINGS_AVAILABLE, postData, global.headers).then((result)
    {
      print(URL.myPLAN_API_BOOKINGS_AVAILABLE);
      if (result[0]['success'].toString().toLowerCase() == 'true'.toLowerCase()) {
        clickedbookSlot = false;
        booking = result[0]['data'];
        for(int i = 0; i < booking.length; i++)
        {
          _availableSlots.add(booking[i]['availableSlots']);
          print(booking[i]['availableSlots']);
          _slotsUsed.add(booking[i]['slotsUsed']);
          _totalSlots.add(booking[i]['slotsUsed']);
          avaliableSlots=booking[i]['availableSlots'];
          slot_total =booking[i]['totalSlots'];
          slots_used = booking[i]['slotsUsed'];
          print("================total" + slot_total.toString());
          print("=============used"+slots_used.toString());
          // settotalSlots(booking[i]['totalSlots']);
          // setavailableSlots(booking[i]['availableSlots']);
          // setslotsUsed(booking[i]['slotsUsed']);
        }
        Navigator.push(context, MaterialPageRoute(builder: (context) => Bookamentor_Screen(slot_total,slots_used)));
      }
      else{
        clickedbookSlot = false;
        print("showError Message");
      }
    });
  }



  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    if(widget.avaliableSlots!=null){
      avaliableSlots=widget.avaliableSlots;
    }else{
      avaliableSlots=0;
    }
    if(widget.slotsUsed!=null){
      slotsUsed=widget.slotsUsed;
    }else{
      slotsUsed=0;
    }
    _future = getBookingsDetails();

    print(avaliableSlots);
    print(slotsUsed);
    print("issues");


  }

  Widget build(BuildContext context) {
    CalculateScreen(context);
    Future<bool> _onWillPop() {
      print("yes of course ");
      Navigator.pop(context);
      Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (context) =>
          HomeWidget()), (Route<dynamic> route) => false);    }
    return
      WillPopScope(
        onWillPop: _onWillPop,
        child: Scaffold(
        backgroundColor: ImsColors.blur,
        body: SlidingUpPanel(
          onPanelClosed: () => setState(() {
            myPlanPopUpForBookSlotButton=false;
          }),
          onPanelSlide: (double pos) => setState(() {}),
          onPanelOpened: () => setState(() {
          }),
          maxHeight: screenHeight * 0.35,
          minHeight: _myPlanPanelHeightClosed,
          parallaxEnabled: false,
          parallaxOffset: 0.5,
          //defaultPanelState: PanelState.CLOSED,
          backdropEnabled: true,
          backdropTapClosesPanel: true,
          isDraggable: true,
          body: _body(context),
          panel: _popUpForBookSlotButton(context),
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(25.0),
              topRight: Radius.circular(25.0)),
          controller: myPlanControllerForPopUp,
        ),

    ),
      );

  }
  Widget _body(context) {
    return FutureBuilder(
      builder: (context, snapshot) {
        if (snapshot.hasError) {
          return Container(); // widget to be shown on any error
        }
        return snapshot.hasData
            ?  Container(
          constraints: BoxConstraints.expand(),
          decoration: BoxDecoration(),
          child: SingleChildScrollView(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                slotsUsed==0?
                Align(
                  alignment: Alignment.topCenter,
                  child: Container(
                    //   width: 261/actualHeight*screenWidth,
                    height: 211/actualHeight*screenHeight,
                    margin: EdgeInsets.only(top: 20/actualHeight*screenHeight),
                    child: Container(
                      //  width: 133/720,
                      //  height: 140,
                      child: Image.asset(
                        "assets/images/mentorpic.png",
                        fit: BoxFit.none,
                      ),
                    ),
                  ),
                ):Text(""),
                slotsUsed==0?
                Align(
                  alignment: Alignment.topCenter,
                  child: Container(
                    width: 330/actualWidth*screenWidth,
                    margin: EdgeInsets.only(top: 10/actualHeight*screenHeight),
                    child: Text(
                      MyplanHomeScreenString.Bookaslottodiscuss,
                      style: TextStyle(
                        color: Color.fromARGB(255, 87, 93, 96),
                        fontSize: 14/actualWidth*screenWidth,
                        height: 1.5,
                        fontFamily: "IBM Plex Sans",
                      ),
                      textAlign: TextAlign.center,
                    ),
                  ),
                ):Text(""),
                GestureDetector(
                  onTap: () {
                    if(avaliableSlots>=1) {
                      if(clickedbookSlot == false){
                        clickedbookSlot = true;
                        bookingsAvailable();
                      }
                      print("======================================checkkkkkkkkkkkkkkkkk");
                      print(avaliableSlots);
                      print(slots_used);
                      print(slot_total);
                    }else{
                      myPlanPopUpForBookSlotButton=true;
                      myPlanControllerForPopUp.open();
                    }
                  },
                  child: Container(
                    height: 57/actualHeight*screenHeight,
                    margin: EdgeInsets.only(left: 45/actualWidth*screenWidth, top: 42/actualHeight*screenHeight, right: 45/actualWidth*screenWidth),
                    child: Stack(
                      alignment: Alignment.center,
                      children: [

                        Positioned(
                          left: 0,
                          top: 0,
                          right: 0,
                          child: Container(
                            height: 40/720*screenHeight,
                            decoration: BoxDecoration(
                              gradient: LinearGradient(
                                begin: Alignment(-0.011, 0.494),
                                end: Alignment(1.031, 0.516),
                                stops: [
                                  0,
                                  1,
                                ],

                                colors:((avaliableSlots>=1)? [
                                  Color.fromARGB(255, 51, 128, 204),
                                  Color.fromARGB(255, 137, 110, 216),
                                ]:[Color.fromARGB(255, 51, 128, 204),
                                  Color.fromARGB(255, 137, 110, 216),
                                ]
                                ),),
                              borderRadius: BorderRadius.all(Radius.circular(2)),
                            ),
                            child: Column(
                              children: [
                                Container(
                                  margin: EdgeInsets.only(top: 11/actualHeight*screenHeight),
                                  child: Text(
                                    "BOOK SLOT",
                                    style: TextStyle(
                                      color:(avaliableSlots>=1)? Color.fromARGB(255, 255, 255, 255):AccentColors.blueGrey,
                                      fontSize: 14/actualWidth*screenWidth,
                                      fontFamily: "IBM Plex Sans",
                                      fontWeight: FontWeight.w500,
                                    ),
                                    textAlign: TextAlign.center,
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                BodyLayout(),
              ],
            ),
          ),
        )
            : Container(child: Center(child: CircularProgressIndicator(),),); // widget to be shown while data is being loaded from api call method
      },
      future: _future,
    );
  }
  /// popup for book slot button ......... ///
  Widget _popUpForBookSlotButton(context) {
    final screenHeight = MediaQuery.of(context).size.height;
    final screenWidth = MediaQuery.of(context).size.width;
    return Container(
      height: screenHeight * 265 / 640,
      margin: EdgeInsets.only(bottom: 100 / 640 * screenHeight),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.end,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Align(
            alignment: Alignment.topCenter,
            child: Container(
              margin: EdgeInsets.only(bottom: 10 / 720 * screenHeight,left: 10/360*screenWidth,right: 10/360*screenWidth,top:20/720*screenHeight),
              child: Text(
                MyplanHomeScreenString.popUpMsgForNoSlots,
                style: TextStyle(
                  color: NeutralColors.black,
                  fontSize: screenHeight * 16 / 678,
                  fontFamily: "IBMPlexSans",
                  fontWeight: FontWeight.w500,
                ),
                textAlign: TextAlign.center,
              ),
            ),
          ),
          Align(
            alignment: Alignment.center,
            child: Container(
              width: screenWidth * 130 / 360,
              height: screenHeight * 40 / 720,
              margin: EdgeInsets.only(top: 20 / 720 * screenHeight),
              child: FlatButton(
                onPressed: () {
                  myPlanControllerForPopUp.close();
                  //dependencies.stopwatch.start();
                },
                color: Color.fromARGB(255, 0, 171, 251),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(2)),
                ),
                textColor: NeutralColors.pureWhite,
                padding: EdgeInsets.all(0),
                child: Text(
                  MyplanHomeScreenString.okayButtonForPopUpForNoSlots,
                  style: TextStyle(
                    fontSize: screenHeight * 14 / 720,
                    fontFamily: "IBMPlexSans",
                    fontWeight: FontWeight.w500,
                  ),
                  textAlign: TextAlign.center,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
class BodyLayout extends StatefulWidget {
  @override
  BodyLayoutState createState() {
    return new BodyLayoutState();
  }
}

class BodyLayoutState extends State<BodyLayout> {
//  List<String> titles = ['Sun', 'Moon', 'Star', 'Arun', 'sunny'];

  @override
  Widget build(BuildContext context) {
    CalculateScreen(context);
    return  totalDataForBookings.toString() == "No Record found"?Container():
    totalDataForBookings.length>0?
    Column(
      children: [
        Container(
          margin: EdgeInsets.only(
            top: 40/actualHeight*screenHeight,
            bottom: 20.0/actualHeight*screenHeight,
          ),
          child: Text(
            MyplanHomeScreenString.BookedSlots,
            style: TextStyle(color: ImsColors.dark_navy_blue,
              fontFamily: "IBMPlexSans",
              fontStyle: FontStyle.normal,
              fontSize: 18.0/actualWidth*screenWidth,
              fontWeight: FontWeight.w700,),
          ),
        ),
        totalDataForBookings == null ?
        Container(
            height: 345/actualHeight*screenHeight,
            child: Center(child: CircularProgressIndicator())):
        Container(
          margin: EdgeInsets.only(bottom: 150/720*screenHeight),
          child: _myListView(),
        )
      ],
    ):
    Container(
      margin: EdgeInsets.only(
        bottom: 40 / actualHeight * screenHeight,
      ),
      child: Column(
        children: [
          Container(
            margin: EdgeInsets.only(
              top: 40/actualHeight*screenWidth,
              bottom: 20.0/actualHeight*screenHeight,
            ),
            child: Text(
              MyplanHomeScreenString.BookedSlots,
              style: TextStyle(color: ImsColors.dark_navy_blue,
                fontFamily: "IBMPlexSans",
                fontStyle: FontStyle.normal,
                fontSize: 18.0/actualWidth*screenWidth,
                fontWeight: FontWeight.w700,),
            ),
          ),
          ErrorScreen(),
        ],
      ),
    );
  }
  Widget _myListView() {
    return ListView.builder(
      itemCount: totalDataForBookings.length,
      shrinkWrap: true,
      physics: NeverScrollableScrollPhysics(),
      itemBuilder: (context, index) {
        final dataForBooking = totalDataForBookings[index];
        // print(dataForBooking['bookingDate']);
        return Container(
          child: ListTile(
//            title: Text(item),
            title: Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(7.0,),
                  border: Border.all(
                    width: 0.4,
                    color: ImsColors.bluey_grey,
                  ),
                ),
                margin: EdgeInsets.only(
                  bottom: 20.0 / actualHeight * screenHeight,

                ),
                child: Column(
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.only(
                          top: 5 / actualHeight * screenHeight,
                          bottom: 15 / actualHeight * screenHeight),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Expanded(
                              flex: 4,
                              child: Padding(
                                padding:  EdgeInsets.only(top:10.0/actualHeight*screenHeight),
                                child: Container(
                                  padding:
                                  EdgeInsets.only(left: 12.5/actualWidth*screenWidth, right: 12.5/actualWidth*screenWidth),
                                  child: Column(
                                    children: <Widget>[
                                      Padding(
                                        padding:  EdgeInsets.only(top:6.0/actualHeight*screenHeight),
                                        child: Text(
                                          dateFormatter(dataForBooking['bookingDate'])[0].toString(),
                                          style: TextStyle(
                                            color: ImsColors.bluey_grey,
                                            fontSize: 12/360*screenWidth,
                                            fontFamily: "IBM Plex Sans-Medium",
                                            fontWeight: FontWeight.w500,
                                          ),
                                        ),
                                      ),
                                      Container(
                                        margin: EdgeInsets.only(top: 6.0/actualHeight*screenHeight, bottom: 7.0/actualHeight*screenHeight),
                                        child: Text(
                                          dateFormatter(dataForBooking['bookingDate'])[1].toString(),
                                          style: TextStyle(
                                              color: ImsColors.dark_navy_blue,
                                              fontWeight: FontWeight.w500,
                                              fontFamily: "IBMPlexSans",
                                              fontStyle: FontStyle.normal,
                                              fontSize: 35.0/360*screenWidth
                                          ),
                                        ),
                                      ),
                                      Text(
                                        '${ dateFormatter(dataForBooking['bookingDate'])[2]}'+" "+ '${ dateFormatter(dataForBooking['bookingDate'])[3]}',
                                        style: TextStyle(
                                          color: ImsColors.bluey_grey,
                                          fontSize: 12/actualWidth*screenWidth,
                                          fontFamily: "IBM Plex Sans-Medium",
                                          fontWeight: FontWeight.w500,
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                              )),
                          Expanded(
                            flex: 10,
                            child: Padding(
                              padding: EdgeInsets.only(top: 20/actualHeight*screenHeight),
                              child: Row(
                                children: <Widget>[
                                  Container(
                                    margin: EdgeInsets.only(right: 20/actualWidth*screenWidth),
                                    width: 1/actualWidth*screenWidth,
                                    height: 80/actualHeight*screenHeight,
                                    color: ImsColors.bluey_grey,
                                  ),
                                  Column(
                                    crossAxisAlignment:
                                    CrossAxisAlignment.start,
                                    children: <Widget>[
                                      Padding(
                                        padding: EdgeInsets.only(bottom: 13/actualHeight*screenHeight),
                                        child: Container(
                                          width: 160/actualWidth*screenWidth,
                                          child: Text(
                                            dataForBooking['mentorName'],
                                            maxLines: 1,
                                            textAlign: TextAlign.left,
                                            overflow: TextOverflow.ellipsis,
                                            style: TextStyle(

                                              color: ImsColors.namecolor,

                                              fontWeight: FontWeight.w500,
                                              fontFamily: "IBMPlexSans",
                                              fontStyle: FontStyle.normal,
                                              fontSize: 16.0/actualWidth*screenWidth,
                                            ),
                                          ),
                                        ),
                                      ),
                                      Row(
                                        children: <Widget>[
                                          Column(
                                            crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                            children: <Widget>[
                                              Container(
                                                margin: EdgeInsets.only(top: 3/actualHeight*screenHeight),
                                                child: Text(
                                                  MyplanHomeScreenString.Location,
                                                  textAlign: TextAlign.left,
                                                  maxLines: 1,
                                                  style: TextStyle(
                                                      color:
                                                      ImsColors.bluey_grey,
                                                      fontSize: 11.0/actualWidth*screenWidth),
                                                ),
                                              ),
                                              Container(
                                                width: 100/actualWidth*screenWidth,
                                                margin: EdgeInsets.only(top: 3/actualHeight*screenHeight),
                                                child: Text(
                                                  dataForBooking['branchName'],
                                                  textAlign: TextAlign.left,
                                                  maxLines: 1,
                                                  overflow: TextOverflow.ellipsis,
                                                  style: TextStyle(
                                                    color: ImsColors
                                                        .dark_navy_blue,
                                                    fontSize: 12.0/actualWidth*screenWidth,
                                                    fontWeight: FontWeight.w500,
                                                    fontFamily: "IBMPlexSans",
                                                    fontStyle: FontStyle.normal,
                                                  ),
                                                ),
                                              ),
                                            ],
                                          ),
                                          Container(
                                            margin: EdgeInsets.only(left: 28/actualWidth*screenWidth),
                                            child: Column(
                                              crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                              children: <Widget>[
                                                Container(
                                                  child: Text(
                                                    MyplanHomeScreenString.Time,
                                                    textAlign: TextAlign.left,
                                                    maxLines: 1,
                                                    style: TextStyle(
                                                        color: ImsColors
                                                            .bluey_grey,
                                                        fontSize: 11.0/actualWidth*screenWidth),
                                                  ),
                                                ),
                                                Container(
                                                  margin:
                                                  EdgeInsets.only(top: 3/actualHeight*screenHeight),
                                                  child: Text(
                                                    dataForBooking['bookingTime'],
                                                    textAlign: TextAlign.left,
                                                    maxLines: 1,
                                                    style: TextStyle(
                                                      color: ImsColors
                                                          .dark_navy_blue,
                                                      fontSize: 12.0/actualWidth*screenWidth,
                                                      fontWeight: FontWeight.w500,
                                                      fontFamily: "IBMPlexSans",
                                                      fontStyle: FontStyle.normal,),
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                        ],
                                      )
                                    ],
                                  ),
                                ],
                              ),
                            ),
                          ),
//                           Expanded(
//                             flex: 2,
//                             child: Container(
//                               alignment: Alignment.center,
//                               margin: EdgeInsets.only(right: 5/actualWidth*screenWidth),
//                               height: 28/actualHeight*screenHeight,
//                               width: 100/actualWidth*screenWidth,
// //                              margin: EdgeInsets.only(left: 10, top: 2),
//                               decoration: BoxDecoration(
//                                 color: Color.fromARGB(26, 65, 125, 206),
//                                 borderRadius: BorderRadius.all(Radius.circular(10)),
//                               ),
//                               child: Container(
//                                 margin: EdgeInsets.only(top: 3/actualHeight*screenHeight),
//                                 child: Text(
//                                   '${index+1} / ${totalDataForBookings.length.toString()}',
//                                   style: TextStyle(
//                                     color: Color.fromARGB(255, 122, 113, 213),
//                                     fontSize: 11/actualWidth*screenWidth,
//                                     fontFamily: "IBM Plex Sans",
//                                     fontWeight: FontWeight.w500,
//                                   ),
//                                   textAlign: TextAlign.left,
//                                 ),
//                               ),
//                             ),
//                           ),
                        ],
                      ),
                    ),
                    Container(
                      height: 1 / actualHeight * screenHeight,
                      width: double.infinity,
                      color: ImsColors.iceBlue,
                    )
                  ],
                )),
            onTap: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) =>
                          MyplanConfirmationScreen(dataForBooking['bookingId'])));
              //                                  <-- onTap
              setState(() {
//                titles.insert(index, 'Planet');
              });
            },
            onLongPress: () {
              //                            <-- onLongPress
              setState(() {
//                titles.removeAt(index);
              });
            },
          ),
        );
      },
    );
  }

}



void CalculateScreen(BuildContext context) {
  var _mediaQueryData = MediaQuery.of(context);
  screenWidthTotal = _mediaQueryData.size.width;
  screenHeightTotal = _mediaQueryData.size.height;
  _safeAreaHorizontal =
      _mediaQueryData.padding.left + _mediaQueryData.padding.right;
  _safeAreaVertical =
      _mediaQueryData.padding.top + _mediaQueryData.padding.bottom;

  screenHeight = screenHeightTotal - _safeAreaVertical;
  screenWidth = screenWidthTotal - _safeAreaHorizontal;
}


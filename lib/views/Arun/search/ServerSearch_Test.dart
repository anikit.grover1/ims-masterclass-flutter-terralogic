import 'package:flutter/material.dart';

import '../ims_utility.dart';

const actualHeight = 740;
const actualWidth = 360;
var screenWidthTotal;
var screenHeightTotal;
var _safeAreaHorizontal;
var _safeAreaVertical;
var screenHeight;
var screenWidth;

class SS_Test extends StatefulWidget {
  @override
  _SS_TestState createState() => _SS_TestState();
}

class _SS_TestState extends State<SS_Test> {
  @override
  Widget build(BuildContext context) {
    CalculateScreen(context);

    return Container(
      child: BodyLayout(),
    );
  }
}

class BodyLayout extends StatefulWidget {
  @override
  BodyLayoutState createState() {
    return new BodyLayoutState();
  }
}

class BodyLayoutState extends State<BodyLayout> {
  List<Item> itemsAll = [
    Item(true),
    Item(false),
    Item(true),
    Item(false),
    Item(false),
    Item(true)
  ];

  @override
  Widget build(BuildContext context) {
    return _myListView();
  }

  Widget _myListView() {
    return ListView.builder(
      itemCount: itemsAll.length,
      itemBuilder: (context, index) {
        final item = itemsAll[index];
        return Container(
          child: Container(
              margin: EdgeInsets.only(
                  top: 10.0 / actualHeight * screenHeight,
                  left: 20.0 / actualWidth * screenWidth,
                  right: 20.0 / actualWidth * screenWidth),
              child: Column(
                children: <Widget>[
                  Container(

                    child: Row(
                      children: <Widget>[
                        Expanded(
                            flex: 2,
                            child: Container(
                              margin: EdgeInsets.only(right: 12.0),
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(
                                  5.0,
                                ),
                                border: Border.all(
                                  width: 0.4,
                                  color: ImsColors.blueGrey,
                                ),
                              ),
                              child: Column(
                                children: <Widget>[
                                  Container(
                                    margin: EdgeInsets.only(top: 5),
                                    child: Text(
                                      "27",
                                      style: TextStyle(
                                        fontFamily: "IBMPlexSans",
                                        fontStyle: FontStyle.normal,
                                        fontWeight: FontWeight.w700,
                                        color: ImsColors.dusky_blue,
                                        fontSize: 18,
                                      ),
                                      textAlign: TextAlign.left,
                                    ),
                                    alignment: Alignment.topCenter,
                                  ),
                                  Container(
                                    decoration: BoxDecoration(
                                        gradient: LinearGradient(
                                            begin: Alignment(0, 0),
                                            end: Alignment(1.0199999809265137,
                                                1.0099999904632568),
                                            colors: [
                                              const Color(0xff3380cc),
                                              ImsColors.purpley
                                            ])),
                                    margin: EdgeInsets.only(
                                        top: 9 / actualHeight * screenHeight,
                                        bottom: 0),
                                    child: Text(
                                      "Aug 2018",
                                      style: TextStyle(
                                        color: ImsColors.dark_navy_blue,
                                        fontSize: 12,
                                        fontFamily: "IBMPlexSans",
                                        fontStyle: FontStyle.normal,
                                      ),
                                      textAlign: TextAlign.left,
                                    ),
                                    alignment: Alignment.bottomCenter,
                                  ),
                                ],
                              ),
                            )),
                        Expanded(
                            flex: 4,
                            child: Container(
                              child: Column(
                                children: <Widget>[
                                  Container(
                                    child: Text(
                                      "simCAt 07",
                                      style: TextStyle(
                                        color: ImsColors.dark_navy_blue,
                                        fontSize: 14,
                                        fontFamily: "IBMPlexSans",
                                        fontStyle: FontStyle.normal,
                                      ),
                                      textAlign: TextAlign.left,
                                    ),
                                    alignment: Alignment.topLeft,
                                  ),
                                  Container(
                                    margin: EdgeInsets.only(top: 9),
                                    child: Text(
                                      "Slots left : 100",
                                      style: TextStyle(
                                        color: ImsColors.blueGrey,
                                        fontSize: 12,
                                        fontFamily: "IBMPlexSans",
                                        fontStyle: FontStyle.normal,
                                      ),
                                      textAlign: TextAlign.left,
                                    ),
                                    alignment: Alignment.topLeft,
                                  ),
                                ],
                              ),
                            )),
                        Expanded(
                            flex: 2,
                            child: Container(
                                decoration: BoxDecoration(
                                  color: ImsColors.azure,
                                    borderRadius: BorderRadius.circular(
                                      2.0,
                                    ),),
                                height: 40,
                                width: 90,

                                child: Text(
                                  'Book Slot',
                                  style: TextStyle(
                                    fontFamily: "IBMPlexSans",
                                    fontStyle: FontStyle.normal,
                                    fontWeight: FontWeight.w700,
                                    color: Colors.white,
                                    fontSize: 14,
                                  ),
                                  textAlign: TextAlign.left,
                                ),
                                alignment: Alignment.center)),
                      ],
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(
                        top: 14.5 / actualHeight * screenHeight),
                    child: Container(
                      height: 1 / actualHeight * screenHeight,
                      width: double.infinity,
                      color: ImsColors.iceBlue,
                    ),
                  )
                ],
              )),
        );
      },
    );
  }
}

class Item {
  var title = 'How to analyse Mock CATs: Mocks liya ab kya karna hai?';
  var subtitle = '01-Aug-2018';
  var subtitle2 = '50 min Arun';
  var bInbdicatorVal;
  var imgUrl =
      'https://thumbs.dreamstime.com/b/inspirational-quote-there-blessings-everyday-find-them-create-treasure-beautiful-white-single-sinnia-flower-blossom-blurry-151653366.jpg';
  Item(bool bValk) {
    bInbdicatorVal = bValk;
  }
}

void CalculateScreen(BuildContext context) {
  var _mediaQueryData = MediaQuery.of(context);
  screenWidthTotal = _mediaQueryData.size.width;
  screenHeightTotal = _mediaQueryData.size.height;
  _safeAreaHorizontal =
      _mediaQueryData.padding.left + _mediaQueryData.padding.right;
  _safeAreaVertical =
      _mediaQueryData.padding.top + _mediaQueryData.padding.bottom;

  screenHeight = screenHeightTotal - _safeAreaVertical;
  screenWidth = screenWidthTotal - _safeAreaHorizontal;
}

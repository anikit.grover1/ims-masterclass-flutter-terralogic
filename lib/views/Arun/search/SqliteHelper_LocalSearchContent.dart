import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:imsindia/views/Arun/ims_utility.dart';
import 'package:imsindia/views/Arun/search/LocalSearchContect.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

class SqliteHelper_LocalSearchContent {
  String dbName = '';
  Future<Database> database;

  SqliteHelper_LocalSearchContent(String dbName) {
    this.dbName = dbName;
  }
  createDB() async {
    database = openDatabase(
      // Set the path to the database. Note: Using the `join` function from the
      // `path` package is best practice to ensure the path is correctly
      // constructed for each platform.
      join(await getDatabasesPath(), dbName),
      // When the database is first created, create a table to store dogs.
      onCreate: (db, version) {
        return db.execute(
          "CREATE TABLE ${ImsStrings.TBL_LOCALSEARCHCONTENT}( content TEXT PRIMARY KEY)",
        );
      },
      // Set the version. This executes the onCreate function and provides a
      // path to perform database upgrades and downgrades.
      version: 1,
    );

    debugPrint('Arun db created successfully');
  }

  Future<void> insert(LocalSearchContent data) async {
    // Get a reference to the database.
    Database db = await database;

    // Insert the localsearchcontent into the correct table. Also specify the
    // `conflictAlgorithm`. In this case, if the same localsearchcontent is inserted
    // multiple times, it replaces the previous data.
    if (db == null) {
      db = await openDatabase(
        // Set the path to the database. Note: Using the `join` function from the
        // `path` package is best practice to ensure the path is correctly
        // constructed for each platform.
        join(await getDatabasesPath(), 'imsApp.db'),
      );
    }
    await db.insert(
      '${ImsStrings.TBL_LOCALSEARCHCONTENT}',
      data.toMap(),
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
  }

  Future<List<LocalSearchContent>> getData() async {
    // Get a reference to the database.
    Database db = await database;

    if (db == null) {
      db = await openDatabase(
        // Set the path to the database. Note: Using the `join` function from the
        // `path` package is best practice to ensure the path is correctly
        // constructed for each platform.
        join(await getDatabasesPath(), dbName),
      );
    }
    // Query the table for all The localsearchcontent.
    final List<Map<String, dynamic>> maps =
        await db.query('localsearchcontent');

    // Convert the List<Map<String, dynamic> into a List<Dog>.
    return List.generate(maps.length, (i) {
      return LocalSearchContent(content: maps[i]['content']);
    });
  }

  Future<void> update(LocalSearchContent data) async {
    // Get a reference to the database.
    Database db = await database;
    if (db == null) {
      db = await openDatabase(
        // Set the path to the database. Note: Using the `join` function from the
        // `path` package is best practice to ensure the path is correctly
        // constructed for each platform.
        join(await getDatabasesPath(), dbName),
      );
    }
    // Update the given localsearchcontent.
    await db.update(
      '${ImsStrings.TBL_LOCALSEARCHCONTENT}',
      data.toMap(),

      where: "content = ?",
      // Pass the localsearchcontent's id as a whereArg to prevent SQL injection.
      whereArgs: [data.content],
    );
  }

  Future<void> delete() async {
    // Get a reference to the database.
    Database db = await database;

    if (db == null) {
      db = await openDatabase(
        // Set the path to the database. Note: Using the `join` function from the
        // `path` package is best practice to ensure the path is correctly
        // constructed for each platform.
        join(await getDatabasesPath(), dbName),
      );
    }
    // Remove the localsearchcontent from the database.
    await db.delete(
      '${ImsStrings.TBL_LOCALSEARCHCONTENT}',
      // Use a `where` clause to delete a specific localsearchcontent.
      where: "content = ?",
      // Pass the localsearchcontent's id as a whereArg to prevent SQL injection.
      whereArgs: [],
    );
  }

  Future<void> deleteAll() async {
    // Get a reference to the database.
    Database db = await database;

    if (db == null) {
      db = await openDatabase(
        // Set the path to the database. Note: Using the `join` function from the
        // `path` package is best practice to ensure the path is correctly
        // constructed for each platform.
        join(await getDatabasesPath(), dbName),
      );
    }
    // Remove the localsearchcontent from the database.
    await db.rawDelete(
      'delete from ${ImsStrings.TBL_LOCALSEARCHCONTENT}',
      // Use a `where` clause to delete a specific localsearchcontent.
    );
  }
}

import 'package:bubble_tab_indicator/bubble_tab_indicator.dart';
import 'package:flutter/material.dart';
import 'package:imsindia/views/Arun/search/ServerSearch_Event.dart';
import 'package:imsindia/views/Arun/search/ServerSearch_Videos.dart';

import '../ims_utility.dart';
import 'ServerSearch_All.dart';
import 'ServerSearch_Discussion.dart';
import 'ServerSearch_Test.dart';

var screenWidthTotal;
var listLength;
var screenHeightTotal;
var _safeAreaHorizontal;
var _safeAreaVertical;
var screenHeight;
var screenWidth;
var showSearch = false;
const actualHeight = 750;
const actualWidth = 360;

TextEditingController controller = TextEditingController();

class ServerSearch extends StatefulWidget {
  String currentSearchTopic = '';
  ServerSearch({Key key, @required this.currentSearchTopic}) : super(key: key);

  @override
  _ServerSearchState createState() => _ServerSearchState();
}

class _ServerSearchState extends State<ServerSearch> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    controller.text = widget.currentSearchTopic;
  }

  @override
  void dispose() {
    super.dispose();
    widget.currentSearchTopic = '';
  }

  @override
  Widget build(BuildContext context) {
    CalculateScreen(context);
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: DefaultTabController(
        length: 5,
        child: Scaffold(
          
          appBar: AppBar(
            brightness: Brightness.light,
            elevation: 0.0,
            centerTitle: false,
            titleSpacing: 0.0,
            automaticallyImplyLeading: false,
            backgroundColor: Colors.white,
            title: Row(
              children: <Widget>[
                Container(
                  height: (40 / actualHeight) * screenHeight,
                  width: (253.0 / actualWidth) * screenWidth,
                  //margin: EdgeInsets.only(right: (6 / 360) * screenWidth),
                  child: Center(
                    child: TextField(
                      onSubmitted: (value) {
                        debugPrint('Arun value in text $value');
                      },
                      textAlign: TextAlign.start,
                      autofocus: true,
                      controller: controller,
                      textInputAction: TextInputAction.send,
                      autocorrect: false,
                      decoration: InputDecoration(
                        focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.all(
                            Radius.circular(30),
                          ),
                          borderSide:
                              BorderSide(color: Color(0xfff2f4f4), width: 1.5),
                        ),
                        contentPadding: const EdgeInsets.all(11.0),
                        alignLabelWithHint: true,
                        border: OutlineInputBorder(
                          borderSide:
                              BorderSide(color: Color(0xfff2f4f4), width: 1.5),
                          borderRadius: BorderRadius.all(
                            Radius.circular(30),
                          ),
                        ),
                        hintStyle: TextStyle(
                            fontFamily: "IBMPlexSans",
                            fontWeight: FontWeight.w400,
                            fontStyle: FontStyle.normal,
                            color: Color(0xFF7e919a),
                            fontSize: 14.0),
                      ),
                      style: TextStyle(
                          fontFamily: "IBMPlexSans",
                          fontWeight: FontWeight.w400,
                          fontStyle: FontStyle.normal,
                          color: ImsColors.charcoal_grey,
                          fontSize: 14),
                    ),
                  ),
                ),
              ],
            ),
            leading: Container(
              height: (14 / actualHeight) * screenHeight,
              width: (14 / actualWidth) * screenWidth,
              margin: EdgeInsets.only(
                  left: (16.0 / actualWidth) * screenWidth,
                  right: (9.0 / actualWidth) * screenWidth),
              child: GestureDetector(
                child: Image.asset(
                  'assets/images/search.png',
                  fit: BoxFit.contain,
                ),
                onTap: () {
                  debugPrint(
                      'Arun value from search text == ${controller.text}');
                  showSearch = false;
                },
              ),
            ),
            actions: <Widget>[
              GestureDetector(
                onTap: () {
                  Navigator.pop(context, false);
                },
                child: Container(
                  height: (22 / actualHeight) * screenHeight,
                  width: (22 / actualWidth) * screenWidth,
                  margin: EdgeInsets.only(right: (19 / 360) * screenWidth),
                  child: Image.asset(
                    'assets/images/close.png',
                    fit: BoxFit.contain,
                  ),
                ),
              ),
            ],
            bottom: TabBar(
              labelStyle: TextStyle(fontSize: 14, color: Colors.white),
              indicatorSize: TabBarIndicatorSize.tab,
              isScrollable: true,
              unselectedLabelColor: ImsColors.bluey_grey,
              unselectedLabelStyle:
                  TextStyle(fontSize: 14, color: Colors.white),

              indicator: BubbleTabIndicator(
                indicatorHeight: 30.0 / actualHeight * screenHeight,
                indicatorColor: ImsColors.purpleish_blue,
                tabBarIndicatorSize: TabBarIndicatorSize.tab,
              ),

//            TabBar(
//              labelStyle:
//                  TextStyle(fontSize: 14, color: ImsColors.purpleish_blue),
//              indicatorColor: ImsColors.purpleish_blue,
//              labelColor: ImsColors.bluey_grey,
//              unselectedLabelColor: ImsColors.bluey_grey,
              tabs: [
                Tab(
                  text: 'All',
                ),
                Tab(
                  text: 'Test',
                ),
                Tab(
                  text: 'Event',
                ),
                Tab(
                  text: 'Videos',
                ),
                Tab(
                  text: 'Dsicussion',
                ),
              ],
            ),
          ),
          body: TabBarView(
            children: [
              SS_All(),
              SS_Test(),
              SS_Event(),
              SS_Videos(),
              SS_Discussion(),
            ],
          ),
        ),
      ),
    );
  }
}

void CalculateScreen(BuildContext context) {
  var _mediaQueryData = MediaQuery.of(context);
  screenWidthTotal = _mediaQueryData.size.width;
  screenHeightTotal = _mediaQueryData.size.height;
  _safeAreaHorizontal =
      _mediaQueryData.padding.left + _mediaQueryData.padding.right;
  _safeAreaVertical =
      _mediaQueryData.padding.top + _mediaQueryData.padding.bottom;

  screenHeight = screenHeightTotal - _safeAreaVertical;
  screenWidth = screenWidthTotal - _safeAreaHorizontal;
  listLength = 550 * actualHeight / screenHeight;
}

class LocalSearchContent {
  final String content;

  LocalSearchContent({this.content});

  Map<String, dynamic> toMap() {
    return {
      'content': content,
    };
  }

  // Implement toString to make it easier to see information about
  // each dog when using the print statement.
  @override
  String toString() {
    return 'LocalSearchContent{ name: $content}';
  }
}

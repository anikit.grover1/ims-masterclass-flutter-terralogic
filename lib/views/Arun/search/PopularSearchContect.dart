class PopularSearchContect {
  final String content;

  PopularSearchContect({this.content});

  Map<String, dynamic> toMap() {
    return {
      'content': content,
    };
  }

  // Implement toString to make it easier to see information about
  // each dog when using the print statement.
  @override
  String toString() {
    return 'PopularSearchContect{ name: $content}';
  }
}

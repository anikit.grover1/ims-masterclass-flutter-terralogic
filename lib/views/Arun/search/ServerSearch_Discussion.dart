import 'package:flutter/material.dart';

import '../ims_utility.dart';

const actualHeight = 740;
const actualWidth = 360;
var screenWidthTotal;
var screenHeightTotal;
var _safeAreaHorizontal;
var _safeAreaVertical;
var screenHeight;
var screenWidth;

class SS_Discussion extends StatefulWidget {
  @override
  _SS_DiscussionState createState() => _SS_DiscussionState();
}

class _SS_DiscussionState extends State<SS_Discussion> {
  @override
  Widget build(BuildContext context) {
    CalculateScreen(context);

    return Container(
      child: BodyLayout(),
    );
  }
}

class BodyLayout extends StatefulWidget {
  @override
  BodyLayoutState createState() {
    return new BodyLayoutState();
  }
}

class BodyLayoutState extends State<BodyLayout> {

  List<Item> itemsAll = [
    Item(true),
    Item(false),
    Item(true),
    Item(false),
    Item(false),
    Item(true)
  ];

  @override
  Widget build(BuildContext context) {
    return _myListView();
  }

  Widget _myListView() {
    return ListView.builder(
      itemCount: itemsAll.length,
      itemBuilder: (context, index) {
        final item = itemsAll[index];
        return getContainer(index);
      },
    );
  }
}

Widget getContainer(int index) {
  if (index % 2 == 0) {
    return Container(
      child: Container(
          margin: EdgeInsets.only(
              top: 10.0 / actualHeight * screenHeight,
              left: 20.0 / actualWidth * screenWidth,
              right: 20.0 / actualWidth * screenWidth),
          child: Column(
            children: <Widget>[
              Container(
                padding: EdgeInsets.all(5),
                child: Row(
                  children: <Widget>[
                    Expanded(
                        flex: 5,
                        child: Container(
                          child: Column(
                            children: <Widget>[
                              Container(
                                child: Text(
                                  "simCAT 12 analysis",
                                  style: TextStyle(
                                    color: ImsColors.dark_navy_blue,
                                    fontSize: 14,
                                    fontFamily: "IBMPlexSans",
                                    fontStyle: FontStyle.normal,
                                  ),
                                  textAlign: TextAlign.left,
                                ),
                                alignment: Alignment.topLeft,
                              ),
                              Container(
                                margin: EdgeInsets.only(top: 6),
                                child: Text(
                                  "Prep Talk",
                                  style: TextStyle(
                                    color: ImsColors.dark_navy_blue,
                                    fontSize: 14,
                                    fontFamily: "IBMPlexSans",
                                    fontStyle: FontStyle.normal,
                                  ),
                                  textAlign: TextAlign.left,
                                ),
                                alignment: Alignment.topLeft,
                              ),
                              Container(
                                margin: EdgeInsets.only(top: 21),
                                child: Text(
                                  "25 min",
                                  style: TextStyle(
                                    color: ImsColors.blueGrey,
                                    fontSize: 12,
                                    fontFamily: "IBMPlexSans",
                                    fontStyle: FontStyle.normal,
                                  ),
                                  textAlign: TextAlign.left,
                                ),
                                alignment: Alignment.topLeft,
                              ),
                            ],
                          ),
                        )),
                    Expanded(
                      flex: 3,
                      child: Container(
                        height: 80,
                        width: 90,
                        child: FittedBox(
                          fit: BoxFit.fill,
                          child: Image.network(
                            'https://thumbs.dreamstime.com/b/inspirational-quote-there-blessings-everyday-find-them-create-treasure-beautiful-white-single-sinnia-flower-blossom-blurry-151653366.jpg',
                          ),
                        ),
                      ),
                    )
                  ],
                ),
              ),
              Padding(
                padding:
                    EdgeInsets.only(top: 14.5 / actualHeight * screenHeight),
                child: Container(
                  height: 1 / actualHeight * screenHeight,
                  width: double.infinity,
                  color: ImsColors.iceBlue,
                ),
              )
            ],
          )),
    );
  } else {
    return Container(
      child: Container(
          margin: EdgeInsets.only(
              top: 10.0 / actualHeight * screenHeight,
              left: 20.0 / actualWidth * screenWidth,
              right: 20.0 / actualWidth * screenWidth),
          child: Column(
            children: <Widget>[
              Container(
                padding: EdgeInsets.all(5),
                child: Row(
                  children: <Widget>[
                    Expanded(
                        flex: 5,
                        child: Container(
                          child: Column(
                            children: <Widget>[
                              Container(
                                child: Text(
                                  "simCAT 12 analysis and simCAT 12 analysis is simCAT 12 analysis",
                                  maxLines: 2,
                                  style: TextStyle(
                                    color: ImsColors.dark_navy_blue,
                                    fontSize: 14,
                                    fontFamily: "IBMPlexSans",
                                    fontStyle: FontStyle.normal,
                                  ),
                                  textAlign: TextAlign.left,
                                ),
                                alignment: Alignment.topLeft,
                              ),
                              Container(
                                margin: EdgeInsets.only(top: 27),
                                child: Text(
                                  "1 Month ago",
                                  style: TextStyle(
                                    color: ImsColors.blueGrey,
                                    fontSize: 12,
                                    fontFamily: "IBMPlexSans",
                                    fontStyle: FontStyle.normal,
                                  ),
                                  textAlign: TextAlign.left,
                                ),
                                alignment: Alignment.topLeft,
                              ),
                            ],
                          ),
                        )),
                    Expanded(
                      flex: 3,
                      child: Container(
                        height: 80,
                        width: 90,
                        child: FittedBox(
                          fit: BoxFit.fill,
                          child: Image.network(
                            'https://thumbs.dreamstime.com/b/inspirational-quote-there-blessings-everyday-find-them-create-treasure-beautiful-white-single-sinnia-flower-blossom-blurry-151653366.jpg',
                          ),
                        ),
                      ),
                    )
                  ],
                ),
              ),
              Padding(
                padding:
                    EdgeInsets.only(top: 14.5 / actualHeight * screenHeight),
                child: Container(
                  height: 1 / actualHeight * screenHeight,
                  width: double.infinity,
                  color: ImsColors.iceBlue,
                ),
              )
            ],
          )),
    );
  }
}

class Item {
  var type = 1;
  var title = 'How to analyse Mock CATs: Mocks liya ab kya karna hai?';
  var subtitle = '01-Aug-2018';
  var subtitle2 = '50 min Arun';
  var bInbdicatorVal;
  var imgUrl =
      'https://thumbs.dreamstime.com/b/inspirational-quote-there-blessings-everyday-find-them-create-treasure-beautiful-white-single-sinnia-flower-blossom-blurry-151653366.jpg';
  Item(bool bValk) {
    bInbdicatorVal = bValk;
  }
}

void CalculateScreen(BuildContext context) {
  var _mediaQueryData = MediaQuery.of(context);
  screenWidthTotal = _mediaQueryData.size.width;
  screenHeightTotal = _mediaQueryData.size.height;
  _safeAreaHorizontal =
      _mediaQueryData.padding.left + _mediaQueryData.padding.right;
  _safeAreaVertical =
      _mediaQueryData.padding.top + _mediaQueryData.padding.bottom;

  screenHeight = screenHeightTotal - _safeAreaVertical;
  screenWidth = screenWidthTotal - _safeAreaHorizontal;
}

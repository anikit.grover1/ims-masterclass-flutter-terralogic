import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:imsindia/views/Arun/ims_utility.dart';
import 'package:imsindia/views/Arun/search/ServerSearch.dart';
import 'package:imsindia/utils/home_svg_icons.dart';
import 'LocalSearchContect.dart';
import 'PopularSearchContect.dart';
import 'SqliteHelper_LocalSearchContent.dart';

var screenWidthTotal;
var screenHeightTotal;
var _safeAreaHorizontal;
var _safeAreaVertical;
var screenHeight;
var screenWidth;
var showPopularSearch = true;
const actualHeight = 750;
const actualWidth = 360;
TextEditingController controller = TextEditingController();
var currentSearchItem = '';
String filter;
var obj;
List<LocalSearchContent> itemsRecentSearch;

class Search extends StatefulWidget {
//  void onRectanglePressed(BuildContext context) => Navigator.push(context, MaterialPageRoute(builder: (context) => SearchTwoWidget()));

  @override
  _SearchState createState() => _SearchState();
}

class _SearchState extends State<Search> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    obj = SqliteHelper_LocalSearchContent('imsApp.db');
    obj.createDB();
    GetLocalSearchContent();
    showPopularSearch = true;
    controller.text = '';
    controller.addListener(
          () {
        setState(() {
          filter = controller.text;
        });
      },
    );
  }

  @override
  void dispose() {
    super.dispose();
    currentSearchItem = '';
  }

  @override
  Widget build(BuildContext context) {
    CalculateScreen(context);

    return Scaffold(
      
      appBar: AppBar(
        brightness: Brightness.light,
        elevation: 0.0,
        centerTitle: false,
        titleSpacing: 0.0,
        automaticallyImplyLeading: false,
        backgroundColor: Colors.white,
        title: Row(
          children: <Widget>[
            Container(
              height: (40 / actualHeight) * screenHeight,
              width: (255.0 / actualWidth) * screenWidth,
              //margin: EdgeInsets.only(right: (6 / 360) * screenWidth),
              child: Center(
                child: TextField(
                  onSubmitted: (value) {
                    debugPrint('Arun value in text $value');
                  },
                  onChanged: (val) {
                    setState(() {
                      if (val.length > 0)
                        showPopularSearch = false;
                      else
                        showPopularSearch = true;
                    });
                  },
                  textAlign: TextAlign.start,
                  autofocus: true,
                  controller: controller,
                  textInputAction: TextInputAction.send,
                  autocorrect: false,
                  decoration: InputDecoration(
                    focusedBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.all(
                        Radius.circular(30),
                      ),
                      borderSide:
                      BorderSide(color: Color(0xfff2f4f4), width: 1.5),
                    ),
                    contentPadding: const EdgeInsets.all(11.0),
                    hintText: "Search",
                    alignLabelWithHint: true,
                    border: OutlineInputBorder(
                      borderSide:
                      BorderSide(color: Color(0xfff2f4f4), width: 1.5),
                      borderRadius: BorderRadius.all(
                        Radius.circular(30),
                      ),
                    ),
                    hintStyle: TextStyle(
                        fontFamily: "IBMPlexSans",
                        fontWeight: FontWeight.w400,
                        fontStyle: FontStyle.normal,
                        color: Color(0xFF7e919a),
                        fontSize: 14.0),
                  ),

                  style: TextStyle(
                      fontFamily: "IBMPlexSans",
                      fontWeight: FontWeight.w400,
                      fontStyle: FontStyle.normal,
                      color: ImsColors.charcoal_grey,
                      fontSize: 14),
                ),
              ),
            ),
          ],
        ),
        leading: Container(
          height: (14 / actualHeight) * screenHeight,
          width: (14 / actualWidth) * screenWidth,
          margin: EdgeInsets.only(
              left: (16.0 / actualWidth) * screenWidth,
              right: (9.0 / actualWidth) * screenWidth),
          child: GestureDetector(
            child: SvgPicture.asset(
              getSvgIcon.search,
              fit: BoxFit.contain,
              //color: Colors.red,
            ),
            onTap: () {
              if (controller.text != null && controller.text.length > 2) {
                currentSearchItem = controller.text.toString();
                InserData(currentSearchItem);
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => ServerSearch(
                          currentSearchTopic: currentSearchItem,
                        )));
              } else {}
            },
          ),
        ),
        actions: <Widget>[
          GestureDetector(
            onTap: () {
              Navigator.pop(context, false);
            },
            child: Container(
              height: (22 / actualHeight) * screenHeight,
              width: (22 / actualWidth) * screenWidth,
              margin: EdgeInsets.only(right: (19 / 360) * screenWidth),
              child: Image.asset(
                'assets/images/close.png',
                fit: BoxFit.contain,
              ),
            ),
          ),
        ],
      ),
      body: showPopularSearch
          ? Container(
        constraints: BoxConstraints.expand(),
        decoration: BoxDecoration(
          color: Color.fromARGB(255, 255, 255, 255),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Align(
              alignment: Alignment.topLeft,
              child: Container(
                margin: EdgeInsets.only(left: 44, top: 30),
                child: Text(
                  "Popular Searches",
                  style: TextStyle(
                    color: Color.fromARGB(255, 0, 3, 44),
                    fontSize: 18,
                    fontFamily: "IBMPlexSans",
                    fontStyle: FontStyle.normal,
                    fontWeight: FontWeight.w700,
                  ),
                  textAlign: TextAlign.left,
                ),
              ),
            ),
            Container(
              width: double.infinity,
              height: 350,
              child: _myListView(),
            ),
          ],
        ),
      )
          : Container(
        constraints: BoxConstraints.expand(),
        decoration: BoxDecoration(
          color: Color.fromARGB(255, 255, 255, 255),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Container(
              width: double.infinity,
              height: 350,
              child: _myListViewResentSearch(),
            ),
          ],
        ),
      ),
    );
  }

  onRectanglePressed(BuildContext context) => Navigator.push(
      context, MaterialPageRoute(builder: (context) => ServerSearch()));
}

Widget _myListView() {
  var textSizeH = 0.0;
  try {
    textSizeH = 14.0 / actualHeight * screenHeight;
  } catch (e) {
    textSizeH = 12.0;
  }
  return ListView.builder(
    itemCount: itemsAll.length,
    itemBuilder: (context, index) {
      final item = itemsAll[index];
      return Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            GestureDetector(
              child: Align(
                alignment: Alignment.topLeft,
                child: Container(
                  margin: EdgeInsets.only(left: 44, top: 20),
                  child: Text(
                    itemsAll[index].content,
                    style: TextStyle(
                      color: Color.fromARGB(255, 0, 171, 251),
                      fontSize: 14,
                      fontFamily: "IBMPlexSans",
                      fontStyle: FontStyle.normal,
                    ),
                    textAlign: TextAlign.left,
                  ),
                ),
              ),
              onTap: () {
                currentSearchItem = itemsAll[index].content;

                controller.text = currentSearchItem;
                InserData(currentSearchItem);
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => ServerSearch(
                          currentSearchTopic: currentSearchItem,
                        )));
              },
            ),
          ],
        ),
      );
    },
  );
}

Widget _myListViewResentSearch() {
  var textSizeH = 0.0;
  try {
    textSizeH = 14.0 / actualHeight * screenHeight;
  } catch (e) {
    textSizeH = 12.0;
  }
  return ListView.builder(
    itemCount: itemsRecentSearch.length,
    itemBuilder: (context, index) {
      final item = itemsRecentSearch[index];
      return Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            GestureDetector(
              child: _getItem(itemsRecentSearch[index].content),
              onTap: () {
                currentSearchItem = itemsRecentSearch[index].content;
                controller.text = currentSearchItem;
                InserData(currentSearchItem);
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => ServerSearch(
                          currentSearchTopic: currentSearchItem,
                        )));
              },
            ),
          ],
        ),
      );
    },
  );
}

Widget _getItem(String str) {
//  Align(
//    alignment: Alignment.topLeft,
//    child: Container(
//      margin: EdgeInsets.only(left: 44, top: 20),
//      child: _getItem(itemsRecentSearch[index].title),
//    ),
//  )

  if (filter == null || filter == "") {
    return Align(
      alignment: Alignment.topLeft,
      child: Container(
        margin: EdgeInsets.only(left: 44, top: 20),
        child: Text(
          str,
          style: TextStyle(
            color: Colors.black,
            fontSize: 14,
            fontFamily: "IBMPlexSans",
            fontStyle: FontStyle.normal,
          ),
          textAlign: TextAlign.left,
        ),
      ),
    );
  } else {
    if (str.toLowerCase().contains(filter.toLowerCase())) {
      return Align(
        alignment: Alignment.topLeft,
        child: Container(
          margin: EdgeInsets.only(left: 44, top: 20),
          child: Text(
            str,
            style: TextStyle(
              color: Colors.black,
              fontSize: 14,
              fontFamily: "IBMPlexSans",
              fontStyle: FontStyle.normal,
            ),
            textAlign: TextAlign.left,
          ),
        ),
      );
    } else {
      return Container();
    }
  }
}

List<PopularSearchContect> itemsAll = [
  PopularSearchContect(content: 'geometry videos'),
  PopularSearchContect(content: 'algebra tests'),
  PopularSearchContect(content: 'mumbai leaderboard'),
  PopularSearchContect(content: 'orange cap holder'),
  PopularSearchContect(content: 'geometry videos'),
];

void InserData(String strContent) async {
  await obj.insert(new LocalSearchContent(content: strContent));
  GetLocalSearchContent();
}

void ClearSearchContent() {}
void GetLocalSearchContent() async {
  itemsRecentSearch = await obj.getData();
  for (int i = 0; i < itemsRecentSearch.length; i++) {
    print('Arun :: ${itemsRecentSearch[i]}');
  }
}

void CalculateScreen(BuildContext context) {
  var _mediaQueryData = MediaQuery.of(context);
  screenWidthTotal = _mediaQueryData.size.width;
  screenHeightTotal = _mediaQueryData.size.height;
  _safeAreaHorizontal =
      _mediaQueryData.padding.left + _mediaQueryData.padding.right;
  _safeAreaVertical =
      _mediaQueryData.padding.top + _mediaQueryData.padding.bottom;

  screenHeight = screenHeightTotal - _safeAreaVertical;
  screenWidth = screenWidthTotal - _safeAreaHorizontal;
}

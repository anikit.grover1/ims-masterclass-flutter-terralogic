import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:imsindia/components/primary_button_gradient.dart';
import 'package:imsindia/utils/svg_images/leaderboard_svg_images.dart';
import 'package:imsindia/views/book_a_test_screens/ticket_screen_backup.dart';
import 'package:imsindia/components/navigation_bar.dart';
import 'package:imsindia/components/custom_DropDown_EventBooking.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../../resources/strings/login.dart';
import '../../utils/colors.dart';

//import 'package:flutter_typeahead/flutter_typeahead.dart';


class StatesService {
  static final List<String> venues = [
    'St. Jude’s High School/1',
    'St. Jude’s High School/2',
    'St. Jude’s High School/3',
    'St. Jude’s High School/4',

  ];

  static List<String> getSuggestions() {
    List<String> matches = List();
    matches.addAll(venues);

    //matches.retainWhere((s) => s.toLowerCase().contains(query.toLowerCase()));
    return matches;

  }

//  static List<String> getSuggestions() {
//    CourseService().CourseData();
//    List<String> matches = List();
//    matches.addAll(courses);
//    //  matches.retainWhere((s) => s.toLowerCase().contains(query.toLowerCase()));
//    return courses;
//  }
}


class LoginAppLabels {

  static const String selectCourse= "Test Venue";



}
class SimcatBookingWidgetBackup extends StatefulWidget {
  var title;

  SimcatBookingWidgetBackup(this.title);

  @override
  State<StatefulWidget> createState() => _ThreeStepLoginState();
}

//final TextEditingController _courses = new TextEditingController();
//String courseId;
//String get course => _courses.text;

class _ThreeStepLoginState extends State<SimcatBookingWidgetBackup> {

  String data = "";
  String nameKey = "_key_name";
  TextEditingController controller = TextEditingController();



  final GlobalKey<ScaffoldState> _scaffoldstate =
  new GlobalKey<ScaffoldState>();

  void onbuttonPressed(BuildContext context) => Navigator.push(
      context, MaterialPageRoute(builder: (context) => SimcatTicket_screen_backup()));

  final TextEditingController _stateOfVenues = new TextEditingController();
  String get courses => _stateOfVenues.text;
  static const String selectCourses= "St. Jude’s High School1";

  List<String> _venues = <String>[
    'St. Jude’s High School1',
    'St. Jude’s High School2',
    'St. Jude’s High School3',
    'St. Jude’s High School4'
  ];
  String _venue = 'St. Jude’s High School1';

  final List<String> _dates = [
    "06-02-2019",
    "07-02-2019",
    "08-02-2019",
    "09-02-2019",
  ];

  final List<String> _slots = [
    "10:00 AM",
    "11:00 AM",
    "12:00 PM",
    "02:00 PM",
    "03:00 PM",
    "04:00 PM",
    "05:00 PM",
    "06:00 PM",
    "07:00 PM",
  ];
  int _selectedIndexDate;
  int _selectedIndexSlot;
  bool _enabledVenue = false;
  bool _enabledDate = false;
  bool _enabledSlot = false;
  String dropDownValue;

  _onSelectedDate(int indexDate) {
    setState(() => _selectedIndexDate = indexDate);
  }

  _onSelectedSlot(int indexSlot) {
    setState(() => _selectedIndexSlot = indexSlot);
  }

  void showtoast(){
    _scaffoldstate.currentState.showSnackBar(new SnackBar(
      content: new Text("Please select all the fields."),
      duration: new Duration(seconds: 3),
    ));
  }

  void setDate() async {
    print("*******Coming setData");
    SharedPreferences preferences = await SharedPreferences.getInstance();
    await preferences.setString("nameDate",_dates[_selectedIndexDate]);
    //await preferences.setString("nameTime", _slots[_selectedIndexSlot]);
    //await preferences.setString("dropDownVal", dropDownValue);

  }
  void setTime() async {
    print("*******Coming setData");
    SharedPreferences preferences = await SharedPreferences.getInstance();
    //await preferences.setString("nameDate",_dates[_selectedIndexDate]);
    await preferences.setString("nameTime", _slots[_selectedIndexSlot]);
    //await preferences.setString("dropDownVal", dropDownValue);

  }

  dateHeight(){
    double height;
    int mod = 0;
    mod = _dates.length % 3;
    height = _dates.length /3;
    if(mod > 0){
      height += 1;
    }
    return height;
  }

  slotsHeight(){
    double height;
    int mod = 0;
    mod = _slots.length % 3;
    height = _slots.length /3;
    if(mod > 0){
      height += 1;
    }
    return height;
  }


  @override
  Widget build(BuildContext context) {
    final screenHeight = MediaQuery.of(context).size.height;
    final screenWidth = MediaQuery.of(context).size.width;
    final defaultScreenHeight = 720;
    final defaultScreenWidth = 360;


    return Scaffold(
      
      key: _scaffoldstate,
      appBar: new AppBar(
        brightness: Brightness.light,
        elevation: 0.0,
        centerTitle: false,
        iconTheme: IconThemeData(
          color: Colors.black, //change your color here
        ),
        backgroundColor: Color.fromARGB(255, 255, 255, 255),
        titleSpacing: 0.0,
        title: Text(
          "Event Bookings",
          style: TextStyle(
            color: Color.fromARGB(255, 0, 0, 0),
            fontSize: screenWidth * 16 / 360,
            fontFamily: "IBMPlexSans",
            fontWeight: FontWeight.w500,
          ),
          textAlign: TextAlign.left,
        ),
        leading:  GestureDetector(
          onTap: (){
            Navigator.pop(context);
          },
          child: Container(
            color: Colors.transparent,
            width: 28 / 360 * screenWidth,
            height: 24 / 720 * screenHeight,
            child: SvgPicture.asset(
              LeaderboardAssets.back_icon,
              fit: BoxFit.scaleDown,
            ),
          ),
        ),
      ),
      // drawer: NavigationDrawer(),
      body: SingleChildScrollView(
        child: Container(
          //  constraints: BoxConstraints.expand(),
          decoration: BoxDecoration(
            color: Color.fromARGB(255, 255, 255, 255),
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              Container(
                margin: EdgeInsets.only(left: 20 / defaultScreenWidth * screenWidth, top: (30/ defaultScreenHeight) * screenHeight),
                child: Text(
                  widget.title.toString(),
                  style: TextStyle(
                    color: Color.fromARGB(255, 0, 3, 44),
                    fontSize: 18 / defaultScreenWidth * screenWidth,
                    fontFamily: "IBMPlexSans",
                    fontWeight: FontWeight.w700,
                  ),
                  textAlign: TextAlign.left,
                ),
              ),

              Padding(
                  padding: EdgeInsets.only(
                      left: screenWidth * 20 / 360,
                      top: screenHeight * 25 / 720,
                      right: screenWidth * 20/ 360),
                  child: DropDownFormField(
                    getImmediateSuggestions: true,

                    // autoFlipDirection: true,
                    textFieldConfiguration: TextFieldConfiguration(
                        controller: _stateOfVenues,
                        label: LoginAppLabels.selectCourse),
                    suggestionsBoxDecoration: SuggestionsBoxDecoration(
                        dropDownHeight: 150,
                        borderRadius: new BorderRadius.circular(5.0),
                        color: Colors.white),
                    suggestionsCallback: (pattern) {
                      return StatesService.getSuggestions();
                    },
                    itemBuilder: (context, suggestion) {
                      return ListTile(
                        dense: true,
                        contentPadding: EdgeInsets.only(left: 10),
                        title: Text(
                          suggestion,
                          style: TextStyle(
                            fontFamily: "IBMPlexSans",
                            fontWeight: FontWeight.w400,
                            fontSize: (13 / 720) * screenHeight,
                            color: NeutralColors.bluey_grey,
                            textBaseline: TextBaseline.alphabetic,
                            letterSpacing: 0.0,
                            inherit: false,
                          ),
                        ),
                      );
                    },
                    hideOnLoading: true,
                    debounceDuration: Duration(milliseconds: 100),
                    transitionBuilder: (context, suggestionsBox, controller) {
                      return suggestionsBox;
                    },
                    onSuggestionSelected: (suggestion) {
                      for (int i = 0; i < StatesService.venues.length; i++) {
                        if (StatesService.venues[i] == suggestion) {
                          //  courseId =  StatesService.venues[i];
                        }
                      }
                      setState(() {
                        _enabledVenue=true;
                        dropDownValue = suggestion;
                      });
                      print(suggestion);
                      // _courses.text = suggestion;
                      _stateOfVenues.text=suggestion;
                      //setState(() {});
                    },
                    onSaved: (value) => {print("something")},
                  )),



              Container(
                margin: EdgeInsets.fromLTRB(20 / 360 * screenWidth, 30.5 / 720 * screenHeight, 0.0, 0.0),
                child: Align(
                  alignment: Alignment.topLeft,
                  child: Text(
                    "Available Dates",
                    style: TextStyle(
                      color: Color.fromARGB(255, 0, 3, 44),
                      fontSize: (14 / 360) * screenWidth,
                      fontFamily: "IBMPlexSans",
                    ),
                    textAlign: TextAlign.left,
                  ),
                ),
              ),
              Container(
                margin: EdgeInsets.fromLTRB(15/360*screenWidth, 0, 20/360*screenWidth, 0.0),
                //height: 70.0 / 720 * screenHeight * dateHeight(),
                color: Colors.transparent,
                child: GridView.builder(
                  shrinkWrap: true,
                  physics: NeverScrollableScrollPhysics(),
                  gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 3,
                    crossAxisSpacing: 0.0,
                    mainAxisSpacing: 0.0,
                    childAspectRatio: defaultScreenHeight / defaultScreenWidth,
                  ),
                  //physics: NeverScrollableScrollPhysics(),
                  padding: const EdgeInsets.all(0.0),
                  addRepaintBoundaries: true,
                  itemCount: _dates.length,
                  itemBuilder: (build, int indexDate) {
                    final item = _dates[indexDate];
                    return Container(
                      // height: 40 / 720 * screenHeight,
                      // width: 120 / 360 * screenWidth,
                      color: Colors.transparent,
                      margin: EdgeInsets.fromLTRB(5/360*screenWidth, 20, 5/360*screenWidth, 0.0),
                      child: FlatButton(
                        onPressed: () {
                          _onSelectedDate(indexDate);
                          if (indexDate != null) {
                            setState(() => _enabledDate = true);
                          }
                        },
                        color: _selectedIndexDate != null &&
                            _selectedIndexDate == indexDate
                            ? NeutralColors.purpleish_blue
                            : Colors.white,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.all(Radius.circular(20 * (screenHeight/screenWidth * 2))),
                          side: BorderSide(
                            width: 1,
                            color: _selectedIndexDate != null &&
                                _selectedIndexDate == indexDate
                                ? Colors.transparent
                                : SemanticColors.iceBlue,
                            style: BorderStyle.solid,
                          ),
                        ),
                        textColor: _selectedIndexDate != null &&
                            _selectedIndexDate == indexDate
                            ? Colors.white
                            : Color.fromARGB(255, 100, 100, 100),
                        padding: EdgeInsets.all(0.0),
                        child: Container(
                          child: Text(
                            item,
                            style: TextStyle(
                              fontSize: 14 / 720 * screenHeight,
                              fontFamily: "IBMPlexSans",
                            ),
                            softWrap: false,
                            textAlign: TextAlign.left,
                          ),
                        ),
                      ),
                    );
                  },
                  //padding: EdgeInsets.all(0.0),
                ),
              ),
              Container(
                margin: EdgeInsets.fromLTRB(20 / 360 * screenWidth, 30.5 / 720 * screenHeight, 0.0, 0.0),
                child: Align(
                  alignment: Alignment.topLeft,
                  child: Text(
                    "Available Slots",
                    style: TextStyle(
                      color: Color.fromARGB(255, 0, 3, 44),
                      fontSize: (14 / 360) * screenWidth,
                      fontFamily: "IBMPlexSans",
                    ),
                    textAlign: TextAlign.left,
                  ),
                ),
              ),
              Container(
                margin: EdgeInsets.fromLTRB(15/360*screenWidth, 0, 20/360*screenWidth, 0.0),
                // height: 70.0 / 720 * screenHeight * slotsHeight(),
                color: Colors.transparent,
                child: GridView.builder(
                  shrinkWrap: true,
                  physics: NeverScrollableScrollPhysics(),
                  gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 3,
                    crossAxisSpacing: 0.0,
                    mainAxisSpacing: 0.0,
                    childAspectRatio: defaultScreenHeight / defaultScreenWidth,
                  ),
                  //physics: NeverScrollableScrollPhysics(),
                  padding: const EdgeInsets.all(0.0),

                  itemCount: _slots.length,
                  itemBuilder: (build, int indexSlot) {
                    final item = _slots[indexSlot];
                    return Container(
                      color: Colors.transparent,
                      margin: EdgeInsets.fromLTRB(5/360*screenWidth, 20, 5/360*screenWidth, 0.0),
                      child: FlatButton(
                        onPressed: () {
                          _onSelectedSlot(indexSlot);
                          if (indexSlot != null)
                            setState(() => _enabledSlot = true);
                        },
                        color: _selectedIndexSlot != null &&
                            _selectedIndexSlot == indexSlot
                            ? NeutralColors.purpleish_blue
                            : Colors.white,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.all(Radius.circular(20 * (screenHeight/screenWidth * 2))),
                          side: BorderSide(
                            width: 1,
                            color: _selectedIndexSlot != null &&
                                _selectedIndexSlot == indexSlot
                                ? Colors.transparent
                                : SemanticColors.iceBlue,
                            style: BorderStyle.solid,
                          ),
                        ),
                        textColor: _selectedIndexSlot != null &&
                            _selectedIndexSlot == indexSlot
                            ? Colors.white
                            : Color.fromARGB(255, 100, 100, 100),
                        padding: EdgeInsets.all(0),
                        child: Text(
                          item,
                          style: TextStyle(
                            fontSize: 14 / 720 * screenHeight,
                            fontFamily: "IBMPlexSans",
                          ),
                          textAlign: TextAlign.left,
                        ),
                      ),
                    );
                  },
                  //padding: EdgeInsets.all(0.0),
                ),
              ),

              GestureDetector(
                onTap: _enabledDate && _enabledSlot && _enabledVenue ?  () {
                  setDate();
                  setTime();
//                        MeetingConfirmation_2_m
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => SimcatTicket_screen_backup()
                      ));
                }:showtoast,
                child: Container(

                  margin: EdgeInsets.only(top: 40 / 720 * screenHeight, bottom: 90.0 / 720 * screenHeight,right: 45 / 360 * screenWidth, left: 45.0 / 360 * screenWidth),
                  decoration:_enabledDate && _enabledSlot && _enabledVenue? BoxDecoration(
                    gradient:_enabledDate && _enabledSlot && _enabledVenue? LinearGradient(
                      begin: Alignment(-1.031, 0.494),
                      end: Alignment(1.031, 0.516),
                      stops: [
                        0,
                        1,
                      ],
                      colors: [
                        Color.fromARGB(255, 51, 128, 204),
                        Color.fromARGB(255, 137, 110, 216),
                      ],
                    ):LinearGradient(
                      begin: Alignment(-0.056, 0.431),
                      end: Alignment(1.076, 0.579),
                      stops: [
                        0,
                        1,
                      ],
                      colors: [
                        Color(0xFFf2f4f4),
                        Color(0xFFf2f4f4),
                      ],
                    ),
                    borderRadius: BorderRadius.all(Radius.circular(2)),
                  ): BoxDecoration(
                    gradient   :LinearGradient(
                      begin: Alignment(-0.056, 0.431),
                      end: Alignment(1.076, 0.579),
                      stops: [
                        0,
                        1,
                      ],
                      colors: [
                        Color(0xFFf2f4f4),
                        Color(0xFFf2f4f4),
                      ],
                    ),
                    borderRadius: BorderRadius.all(Radius.circular(2)),
                  ),
                  child: Container(
                    margin: EdgeInsets.only(top: 11 / 720 * screenHeight, bottom: 11.0 / 720 * screenHeight,),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Text(
                          "BOOK SLOT",
                          style:_enabledDate && _enabledSlot && _enabledVenue? TextStyle(
                            color: Color.fromARGB(255, 255, 255, 255),
                            fontSize: 14 / 360 * screenWidth,
                            fontFamily: "IBMPlexSans",
                            fontWeight: FontWeight.w500,
                          ):TextStyle(
                            color: Color(0xFF7e919a),
                            fontSize: 14 / 360 * screenWidth,
                            fontFamily: "IBMPlexSans",
                            fontWeight: FontWeight.w500,
                          ),
                          textAlign: TextAlign.center,
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

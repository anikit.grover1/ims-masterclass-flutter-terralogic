import 'package:flutter/material.dart';
import 'package:imsindia/api/bloc/services.dart';
import 'package:imsindia/components/custom_DropDown.dart';
import 'package:imsindia/components/primary_button_gradient.dart';
import 'package:imsindia/resources/strings/book_a_test.dart';
import 'package:imsindia/utils/Url.dart';
import 'package:imsindia/utils/screen_util.dart';
import 'package:imsindia/views/book_a_test_screens/book_test_homepage.dart';
import 'package:imsindia/views/book_a_test_screens/ticket_screen.dart';
import 'package:imsindia/views/book_a_test_screens/ticket_screen_backup.dart';
import 'package:imsindia/views/event_booking_pages/ticket_screen_widget.dart';
import 'package:imsindia/components/navigation_bar.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../../utils/colors.dart';
import 'package:imsindia/utils/global.dart' as global;
import 'package:imsindia/utils/colors.dart';
//import '//package:keyboard_visibility/keyboard_visibility.dart';
import 'package:imsindia/utils/emaximiser_svg_icons.dart';
var venueListForSimCatBooking=[];
class StatesService {
  static  List venues = [];

  static List getSuggestions(String query) {
    List matches = List();
    venues=venueListForSimCatBooking;
    matches.addAll(venues);

    // matches.retainWhere((s) => s.toLowerCase().contains(query.toLowerCase()));
    return matches;
  }
}

class SimcatBookingWidget extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => SimcatBookingWidgetState();
}

class SimcatBookingWidgetState extends State<SimcatBookingWidget> {
  final GlobalKey<ScaffoldState> _scaffoldstate =
  new GlobalKey<ScaffoldState>();

  void onbuttonPressed(BuildContext context) => Navigator.push(
      context, MaterialPageRoute(builder: (context) => SimcatTicket_screen()));
  final TextEditingController _stateOfVenues = new TextEditingController();
  var titleForSimcat;
  var idForSimcat;
  FocusNode _focusNode = new FocusNode();
  int _selectedIndexDate;
  int _selectedIndexSlot;
  bool _enabledVenue = false;
  bool _enabledDate = false;
  bool _enabledSlot = false;
  var dateListForSimCatBooking=[];
  var slotListForSimCatBooking=[];
  var testIdForSimCatBooking=[];
  var venueIdForSimCatBooking=[];
  var testSlotIdForSimCatBooking=[];
  var testId;
  var venueId;
  var testSlotId;


  void showtoast(){
    _scaffoldstate.currentState.showSnackBar(new SnackBar(
      content: new Text("Please select all the fields."),
      duration: new Duration(seconds: 3),
    ));
  }

  _onSelectedDate(int indexDate) {
    setState(() => _selectedIndexDate = indexDate);
  }

  _onSelectedSlot(int indexSlot) {
    setState(() => _selectedIndexSlot = indexSlot);
  }
  GlobalKey<ScaffoldState> _key;
  @override
  void initState() {
    // TODO: implement initState
    _key = GlobalKey<ScaffoldState>();
    // KeyboardVisibilityNotification().addNewListener(
    //   onHide: () {
    //     _focusNode.unfocus();
    //   },
    // );
    super.initState();
    getIdAndTitleForSimcatBooking().then((_){
      simCatBookingData();
    });
  }
  ///* set student booking id for admin card *///
  void setStudentBookingIdForAdminCard(value) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString("studentBookingIdForAdimnCard", value);
  }

  ///* get id and title for sim cat booking *///
   getIdAndTitleForSimcatBooking() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    idForSimcat=prefs.getString("idForSimcatBooking");
    titleForSimcat=prefs.getString("titleForSimcatBooking");
    print(idForSimcat);
    print("id for simcat");
  }
///* Api call for sim cat booking *///
  void simCatBookingData() async{
    ApiService().getAPI(URL.BOOK_A_TEST_API_TO_SIMCAT_BOOKING + "f5f3639cbae2ca0e7c4c2aa007556e65", global.headersWithAuthorizationKey)
        .then((returnValue) {
      setState(() {
        if (returnValue[0] == 'Fetched Successfully') {
          var totalData = returnValue[1]['data'];
          for (var index = 0; index < totalData.length; index++) {
            if(venueListForSimCatBooking.contains(totalData[index]['venueName'])){
            }else{
              venueListForSimCatBooking.add(totalData[index]['venueName']);
            }
            if(dateListForSimCatBooking.contains(totalData[index]['slotBooking_ToDate'])){
            }else{
              dateListForSimCatBooking.add(totalData[index]['slotBooking_ToDate']);
            }if(slotListForSimCatBooking.contains(totalData[index]['timeFrom'])){
            }else{
              slotListForSimCatBooking.add(totalData[index]['timeFrom']);
            }
             testIdForSimCatBooking.add(totalData[index]['testId']);
             venueIdForSimCatBooking.add(totalData[index]['venueId']);
             testSlotIdForSimCatBooking.add(totalData[index]['id']);
          }

        }
      });
    });
  }

///* post API call for book a slot *///
  void bookATestApi() async{
    Map postdata = {
      "testId": testId,
      "venueId": venueId,
      "testSlotId": testSlotId
    };
    ApiService().postAPI(URL.BOOK_A_TEST_API_TO_SAVE_SIMCAT_DATA ,postdata, global.headersWithAuthorizationKey,)
        .then((returnValue) {
      setState(() {
//        if (returnValue[0] == 'Fetched Successfully') {
//          var totalData = returnValue[1]['data'];
//          for (var index = 0; index < totalData.length; index++) {
//
//        }
//        }
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    final screenHeight = MediaQuery.of(context).size.height;
    final screenWidth = MediaQuery.of(context).size.width;
    final defaultScreenHeight = 720;
    final defaultScreenWidth = 360;

    return Scaffold(
      key: _scaffoldstate,
      
      appBar: AppBar(
        automaticallyImplyLeading: true,
        backgroundColor: _focusNode.hasFocus
            ? Colors.black.withOpacity(0.25)
            : Colors.white,
        elevation: 0.0,
        centerTitle: false,
        titleSpacing: 0.0,
        brightness: Brightness.light,
        iconTheme: IconThemeData(
          color: Colors.black,
        ),
        leading: IconButton(
          icon: getSvgIcon.backSvgIcon,
          onPressed: () =>  Navigator.pop(context,BookHomeScreen()),
        ),
        title: Text(
          "Book a Test",
          style: TextStyle(
            color: Colors.black,
            fontWeight: FontWeight.w500,
            fontFamily: "IBMPlexSans",
            fontSize: (16/Constant.defaultScreenWidth)*screenWidth,
            fontStyle: FontStyle.normal,
          ),
        ),
      ),
      body: venueListForSimCatBooking.length==0?Center(child:CircularProgressIndicator()):Container(
         constraints: BoxConstraints.expand(),
        decoration: BoxDecoration(
          color: Color.fromARGB(255, 255, 255, 255),
        ),
        child: ListView(
          children: [
            Container(
              margin: EdgeInsets.only(left: 20 / defaultScreenWidth * screenWidth, top: (30/ defaultScreenHeight) * screenHeight),
              child: Text(
                titleForSimcat==null?"Book a Test":titleForSimcat,
                style: TextStyle(
                  color: Color.fromARGB(255, 0, 3, 44),
                  fontSize: 16.5/ defaultScreenWidth * screenWidth,
                  fontFamily: "IBMPlexSans",
                  fontWeight: FontWeight.w700,
                ),
                textAlign: TextAlign.left,
              ),
            ),
            Padding(
                padding: EdgeInsets.only(
                    left: screenWidth * 20 / 360,
                    top: screenHeight * 25 / 720,
                    right: screenWidth * 20/ 360),
                child: DropDownFormField(
                  getImmediateSuggestions: true,
                  textFieldConfiguration: TextFieldConfiguration(
                      controller: _stateOfVenues,
                      label: BookTestScreenStrings.testVenue),
                  suggestionsBoxDecoration: SuggestionsBoxDecoration(
                      dropDownHeight: 150,
                      borderRadius: new BorderRadius.circular(5.0),
                      color: Colors.white),
                  suggestionsCallback: (pattern) {
                    return StatesService.getSuggestions(pattern);
                  },
                  itemBuilder: (context, suggestion) {
                    return ListTile(
                      dense: true,
                      contentPadding: EdgeInsets.only(left: 10),
                      title: Text(
                        suggestion,
                        style: TextStyle(
                          fontFamily: "IBMPlexSans",
                          fontWeight: FontWeight.w400,
                          fontSize: (13 / 720) * screenHeight,
                          color: NeutralColors.bluey_grey,
                          textBaseline: TextBaseline.alphabetic,
                          letterSpacing: 0.0,
                          inherit: false,

                        ),
                      ),
                    );
                  },
                  hideOnLoading: true,
                  debounceDuration: Duration(milliseconds: 100),
                  transitionBuilder: (context, suggestionsBox, controller) {
                    return suggestionsBox;
                  },
                  onSuggestionSelected: (suggestion) {
                    for (int i = 0; i < StatesService.venues.length; i++) {
                      if (StatesService.venues[i] == suggestion) {
                          testId=testIdForSimCatBooking[i];
                      }

                    }
                    setState(() {
                      _enabledVenue=true;
                    });
                    // _courses.text = suggestion;
                    _stateOfVenues.text=suggestion;
                    //setState(() {});
                  },
                  onSaved: (value) => {},
                )),
            Container(
              margin: EdgeInsets.fromLTRB(20, 30.5, 0.0, 0.0),
              child: Align(
                alignment: Alignment.topLeft,
                child: Text(
                  "Available Dates",
                  style: TextStyle(
                    color: Color.fromARGB(255, 0, 3, 44),
                    fontSize: (14 / 360) * screenWidth,
                    fontFamily: "IBMPlexSans",
                  ),
                  textAlign: TextAlign.left,
                ),
              ),
            ),
            ///* dates list container *///
            Container(
              margin: EdgeInsets.fromLTRB(15/360*screenWidth, 0, 20/360*screenWidth, 0.0),
              //height: 140.0 / 720 * screenHeight ,
              color: Colors.transparent,
              child: GridView.builder(
                shrinkWrap: true,
                physics: NeverScrollableScrollPhysics(),
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: 3,
                  crossAxisSpacing: 0.0,
                  mainAxisSpacing: 0.0,
                  childAspectRatio: defaultScreenHeight / defaultScreenWidth,
                ),
                //physics: NeverScrollableScrollPhysics(),
                padding: const EdgeInsets.all(0.0),
                addRepaintBoundaries: true,
                itemCount: dateListForSimCatBooking.length,
                itemBuilder: (build, int indexDate) {
                  final item = dateListForSimCatBooking[indexDate];
                  return Container(
                    height: 40 / 720 * screenHeight,
                    width: 120 / 360 * screenWidth,
                    margin: EdgeInsets.fromLTRB(10, 20, 10, 0.0),
                    child: FlatButton(
                      onPressed: () {
                        _onSelectedDate(indexDate);
                        venueId=venueIdForSimCatBooking[indexDate];
                        if (indexDate != null) {
                          setState(() => _enabledDate = true);
                        }
                      },
                      color: _selectedIndexDate != null &&
                          _selectedIndexDate == indexDate
                          ? NeutralColors.purpleish_blue
                          : Colors.white,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.all(Radius.circular(20)),
                        side: BorderSide(
                          width: 1,
                          color: _selectedIndexDate != null &&
                              _selectedIndexDate == indexDate
                              ? Colors.transparent
                              : SemanticColors.iceBlue,
                          style: BorderStyle.solid,
                        ),
                      ),
                      textColor: _selectedIndexDate != null &&
                          _selectedIndexDate == indexDate
                          ? Colors.white
                          : Color.fromARGB(255, 100, 100, 100),
                      padding: EdgeInsets.all(0.0),
                      child: Container(
                        child: Text(
                          item.toString().substring(0,10),
                          style: TextStyle(
                            fontSize: 14 / 720 * screenHeight,
                            fontFamily: "IBMPlexSans",
                          ),
                          softWrap: false,
                          textAlign: TextAlign.left,
                        ),
                      ),
                    ),
                  );
                },
                //padding: EdgeInsets.all(0.0),
              ),
            ),
            Container(
              margin: EdgeInsets.fromLTRB(20, 30.5, 0.0, 0.0),
              child: Align(
                alignment: Alignment.topLeft,
                child: Text(
                  "Available Slots",
                  style: TextStyle(
                    color: Color.fromARGB(255, 0, 3, 44),
                    fontSize: (14 / 360) * screenWidth,
                    fontFamily: "IBMPlexSans",
                  ),
                  textAlign: TextAlign.left,
                ),
              ),
            ),
            ///* time list container *///
            Container(
              margin: EdgeInsets.fromLTRB(15/360*screenWidth, 0, 20/360*screenWidth, 0.0),
              //height: 210.0 / 720 * screenHeight,
              color: Colors.transparent,
              child: GridView.builder(
                shrinkWrap: true,
                physics: NeverScrollableScrollPhysics(),
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: 3,
                  crossAxisSpacing: 0.0,
                  mainAxisSpacing: 0.0,
                  childAspectRatio: defaultScreenHeight / defaultScreenWidth,
                ),
                //physics: NeverScrollableScrollPhysics(),
                padding: const EdgeInsets.all(0.0),

                itemCount: slotListForSimCatBooking.length,
                itemBuilder: (build, int indexSlot) {
                  final item = slotListForSimCatBooking[indexSlot];
                  return Container(
                    height: 40 / 720 * screenHeight,
                    width: 100 / 360 * screenWidth,
                    margin: EdgeInsets.fromLTRB(10, 20, 10, 0.0),
                    child: FlatButton(
                      onPressed: () {
                        _onSelectedSlot(indexSlot);
                        testSlotId=testSlotIdForSimCatBooking[indexSlot];
                        if (indexSlot != null)
                          setState(() => _enabledSlot = true);
                      },
                      color: _selectedIndexSlot != null &&
                          _selectedIndexSlot == indexSlot
                          ? NeutralColors.purpleish_blue
                          : Colors.white,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.all(Radius.circular(20)),
                        side: BorderSide(
                          width: 1,
                          color: _selectedIndexSlot != null &&
                              _selectedIndexSlot == indexSlot
                              ? Colors.transparent
                              : SemanticColors.iceBlue,
                          style: BorderStyle.solid,
                        ),
                      ),
                      textColor: _selectedIndexSlot != null &&
                          _selectedIndexSlot == indexSlot
                          ? Colors.white
                          : Color.fromARGB(255, 100, 100, 100),
                      padding: EdgeInsets.all(0),
                      child: Text(
                        item+" PM",
                        style: TextStyle(
                          fontSize: 14 / 720 * screenHeight,
                          fontFamily: "IBMPlexSans",
                        ),
                        textAlign: TextAlign.left,
                      ),
                    ),
                  );
                },
                //padding: EdgeInsets.all(0.0),
              ),
            ),
            Align(
              alignment: Alignment.topCenter,
              child: Container(
                width: 270,
                height: 40,
                margin: EdgeInsets.only(top: 40/defaultScreenHeight*screenHeight, bottom: 40.0/defaultScreenHeight*screenHeight),
                decoration:_enabledDate && _enabledSlot && _enabledVenue? BoxDecoration(
                  gradient:_enabledDate && _enabledSlot && _enabledVenue? LinearGradient(
                    begin: Alignment(-0.011, 0.494),
                    end: Alignment(1.031, 0.516),
                    stops: [
                      0,
                      1,
                    ],
                    colors: [
                      Color.fromARGB(255, 51, 128, 204),
                      Color.fromARGB(255, 137, 110, 216),
                    ],
                  ):LinearGradient(
                    begin: Alignment(-0.056, 0.431),
                    end: Alignment(1.076, 0.579),
                    stops: [
                      0,
                      1,
                    ],
                    colors: [
                      Color(0xFFf2f4f4),
                      Color(0xFFf2f4f4),
                    ],
                  ),
                  borderRadius: BorderRadius.all(Radius.circular(2)),
                ):BoxDecoration(
                  gradient   :LinearGradient(
                    begin: Alignment(-0.056, 0.431),
                    end: Alignment(1.076, 0.579),
                    stops: [
                      0,
                      1,
                    ],
                    colors: [
                      Color(0xFFf2f4f4),
                      Color(0xFFf2f4f4),
                    ],
                  ),
                ),
                child: GestureDetector(
                  onTap:_enabledDate && _enabledSlot && _enabledVenue? () {

                    bookATestApi();
                    //setStudentBookingIdForAdminCard("28ce33fc975c27498466ce372e687a61");
//                        MeetingConfirmation_2_m
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => SimcatTicket_screen_backup()
                        ));
                  }:showtoast,
                  child: Container(
                    margin: EdgeInsets.only(top: 11),
                    child: Text(
                      "BOOK SLOT",
                      style:  _enabledDate && _enabledSlot && _enabledVenue?TextStyle(
                        color: Color.fromARGB(255, 255, 255, 255),
                        fontSize: 14,
                        fontFamily: "IBMPlexSans",
                        fontWeight: FontWeight.w500,
                      ):TextStyle(
                        color: Color(0xFF7e919a),
                        fontSize: 14 / 360 * screenWidth,
                        fontFamily: "IBMPlexSans",
                        fontWeight: FontWeight.w500,
                      ),
                      textAlign: TextAlign.center,
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_email_sender/flutter_email_sender.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:flutter_svg/svg.dart';
import 'package:imsindia/components/bottom_navigation.dart';
import 'package:imsindia/components/navigation_bar.dart';
import 'package:imsindia/routers/routes.dart';
import 'package:imsindia/utils/screen_util.dart';
import 'package:sliding_up_panel/sliding_up_panel.dart';
import 'package:imsindia/utils/emaximiser_svg_icons.dart';
import 'package:imsindia/utils/book_a_slot.dart';

//import 'package:pdf/pdf.dart';
//import 'package:pdf/widgets.dart' as pdf;
//import 'package:printing/printing.dart';

import 'dart:typed_data';
import 'dart:ui' as ui;
import 'package:flutter/rendering.dart';
import 'dart:io';
import 'dart:convert';
import 'package:image/image.dart' as Images;
import 'package:path_provider/path_provider.dart';

//import '//package:keyboard_visibility/keyboard_visibility.dart';
import 'book_test_homepage.dart';



class EventData {
  String id;
  String name;
  String email;
  String date;
  String time;
  String test_venu;

  EventData({
    this.id,
    this.name,
    this.email,
    this.date,
    this.time,
    this.test_venu,
  });
}

class SimcatTicket_screen_backup extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return SimcatTicketScreenWidgetState();
  }
}

class SimcatTicketScreenWidgetState extends State<SimcatTicket_screen_backup> {

  final Eventcontent = EventData(
    id: "676916-WF6FC4",
    name: "Stephen Moses",
    email: "stephen@gmail.com",
    date:"02-07-2019",
    time:  "10:00 AM - 02:00 PM",
    test_venu: "Mother Tekla Auditorium, 143, General KS Thimayya Rd, Craig Park Layout, Ashok Nagar, Bengaluru, Karnataka 560025.",

  );

  GlobalKey _globalKey = new GlobalKey();
  static const androidMethodChannel = const MethodChannel('team.native.io/screenshot');


  bool inside = false;
  Uint8List imageInMemory;

  Future <Uint8List> _capturePng() async {
    try {
      print('inside');
      inside = true;
      RenderRepaintBoundary boundary =
      _globalKey.currentContext.findRenderObject();
      ui.Image image = await boundary.toImage(pixelRatio: 3.0);
      ByteData byteData =
      await image.toByteData(format: ui.ImageByteFormat.png);
      Uint8List pngBytes = byteData.buffer.asUint8List();
      //String bs64 = base64Encode(pngBytes);
      //print(pngBytes);
      //print(bs64);

      print('png done');
      setState(() {
        imageInMemory = pngBytes;
        inside = false;
//        Printing.layoutPdf(
//          onLayout: buildPdf,
//        );
      });
      return pngBytes;
    } catch (e) {
      print(e);
    }
  }


  void onYesPressed(BuildContext context) => Navigator.push(context,
      MaterialPageRoute(builder: (context) => BookHomeScreen()));

  final _controller = PanelController();
  double _panelHeightClosed = 0.0;
  final Email email = Email(

    body: 'Email body',
    subject: 'Email subject',
    recipients: ['example@example.com'],
    cc: ['cc@example.com'],
    bcc: ['bcc@example.com'],
    //attachmentPath: '/path/to/attachment.zip',

  );
  FocusNode _focusNode = new FocusNode();
  GlobalKey<ScaffoldState> _key;
  @override
  void initState() {
    // TODO: implement initState
    _key = GlobalKey<ScaffoldState>();
    // KeyboardVisibilityNotification().addNewListener(
    //   onHide: () {
    //     print(_focusNode.hasFocus);
    //     _focusNode.unfocus();
    //   },
    // );
    super.initState();
  }
  void dispose() {
    super.dispose();
  }


  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      systemNavigationBarColor: Colors.grey[200],
      systemNavigationBarIconBrightness: Brightness.dark,
      systemNavigationBarDividerColor: Colors.black,
    ));
    final screenHeight = MediaQuery.of(context).size.height;
    final screenWidth = MediaQuery.of(context).size.width;

    return Scaffold(
//      drawer:
//      NavigationDrawer(), //this will just add the Navigation Drawer Icon
//      appBar: new AppBar(
//        brightness: Brightness.light,
//        elevation: 0.0,
//        centerTitle: false,
//        titleSpacing: 0.0,
//        iconTheme: IconThemeData(
//          color: Colors.black, //change your color here
//        ),
//        backgroundColor: Color.fromARGB(255, 255, 255, 255),
//        title: Text(
//          "Book a Test",
//          style: TextStyle(
//            color: Color.fromARGB(255, 0, 0, 0),
//            fontSize: screenHeight * 16 / 720,
//            fontFamily: "IBMPlexSans",
//            fontWeight: FontWeight.w500,
//          ),
//          textAlign: TextAlign.left,
//        ),
//      ),
      appBar: AppBar(
        automaticallyImplyLeading: true,
        backgroundColor: _focusNode.hasFocus
            ? Colors.black.withOpacity(0.25)
            : Colors.white,
        elevation: 0.0,
        centerTitle: false,
        brightness: Brightness.light,
        titleSpacing: 0.0,
        iconTheme: IconThemeData(
          color: Colors.black,
        ),
        title: Text(
          "Book a Test",
          style: TextStyle(
            color: Colors.black,
            fontWeight: FontWeight.w500,
            fontFamily: "IBMPlexSans",
            fontSize: (16/Constant.defaultScreenWidth)*screenWidth,
            fontStyle: FontStyle.normal,
          ),
        ),
      ),
      drawer: NavigationDrawer(),
//      bottomNavigationBar: BottomNavigation(),
      body: Container(
        child: SlidingUpPanel(
          maxHeight: screenHeight / 2,
          minHeight: _panelHeightClosed,
          parallaxEnabled: false,
          parallaxOffset: .5,
          //defaultPanelState: PanelState.OPEN,
          isDraggable: true,
          body: _body(),
          panel: _panel(),
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(25.0), topRight: Radius.circular(25.0)),
          //onPanelSlide: (double pos) => setState((){
          //_fabHeight =  _initFabHeight/2;
          //}),
          controller: _controller,
        ),
      ),
    );
  }

  Widget _body() {
    final screenHeight = MediaQuery.of(context).size.height;
    final screenWidth = MediaQuery.of(context).size.width;
    final defaultScreenHeight = 720;
    final defaultScreenWidth = 360;
    return SingleChildScrollView(
      child: Container(
        height: screenHeight * 1020 / 700,
        //constraints: BoxConstraints.expand(),
        decoration: BoxDecoration(
          color: Color.fromARGB(255, 255, 255, 255),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Align(
              alignment: Alignment.topLeft,
            ),

            _ticket(),

            Container(
              width: screenWidth * 320 / 360,
              margin: EdgeInsets.only(left: 20 / defaultScreenWidth * screenWidth, top: 10 / defaultScreenHeight * screenHeight, right: 20 / defaultScreenWidth * screenWidth),
              decoration: BoxDecoration(
                color: Color.fromARGB(255, 242, 246, 248),
                borderRadius: BorderRadius.all(Radius.circular(5)),
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  Align(
                    alignment: Alignment.topLeft,
                    child: Container(
                      height: 22/defaultScreenHeight*screenHeight,
                      margin: EdgeInsets.only(left: 15 / defaultScreenWidth * screenWidth, top: 14 / defaultScreenHeight * screenHeight),
                      child: Text(
                        "Important Instructions",
                        style: TextStyle(
                          color: Color.fromARGB(255, 0, 3, 44),
                          fontSize: screenHeight * 13 / 720,
                          fontFamily: "IBMPlexSans",
                          fontWeight: FontWeight.w500,
                        ),
                        textAlign: TextAlign.left,
                      ),
                    ),
                  ),
                  Container(
                    height: 54/defaultScreenHeight*screenHeight,
                    margin: EdgeInsets.only(left: 15 / defaultScreenWidth * screenWidth, top: 5/ defaultScreenHeight * screenHeight, right: 15/ defaultScreenWidth * screenWidth),
                    child: Text(
                      "Kindly reach the venue 10 minutes before the slot timing. Students who arrive late will not be allowed to enter.",
                      style: TextStyle(
                        color: Color.fromARGB(255, 87, 93, 96),
                        fontSize: screenWidth * 12 / 360,
                        fontFamily: "IBMPlexSans",
                      ),
                      textAlign: TextAlign.left,
                    ),
                  ),
                  Container(
                    height: 36/defaultScreenHeight*screenHeight,
                    margin: EdgeInsets.only(left: 15/ defaultScreenWidth * screenWidth, top: 10/ defaultScreenHeight * screenHeight, right: 32/ defaultScreenWidth * screenWidth),
                    child: Text(
                      "Ensure that your mobile is switched off and silence is maintained.",
                      style: TextStyle(
                        color: Color.fromARGB(255, 87, 93, 96),
                        fontSize: screenWidth * 12 / 360,
                        fontFamily: "IBMPlexSans",
                      ),
                      textAlign: TextAlign.left,
                    ),
                  ),
                  Container(
                    height: 36/defaultScreenHeight*screenHeight,
                    margin: EdgeInsets.only(left: 15/ defaultScreenWidth * screenWidth, top: 10/ defaultScreenHeight * screenHeight, right: 15/ defaultScreenWidth * screenWidth),
                    child: Text(
                      "Please carry a prinout of the registration confirmation along with your IMS I-Card.",
                      style: TextStyle(
                        color: Color.fromARGB(255, 87, 93, 96),
                        fontSize: screenWidth * 12 / 360,
                        fontFamily: "IBMPlexSans",
                      ),
                      textAlign: TextAlign.left,
                    ),
                  ),
                  Container(
                    height: 36/defaultScreenHeight*screenHeight,
                    margin: EdgeInsets.only(left: 15 / defaultScreenWidth * screenWidth, top: 10 / defaultScreenHeight * screenHeight, right: 15  / defaultScreenWidth * screenWidth) ,
                    child: Text(
                      "Once a booking is confirmed, you can change it only after cancelling the existing booking.",
                      style: TextStyle(
                        color: Color.fromARGB(255, 87, 93, 96),
                        fontSize: screenWidth * 12 / 360,
                        fontFamily: "IBMPlexSans",
                      ),
                      textAlign: TextAlign.left,
                    ),
                  ),
                  Container(
                    height: screenHeight * 50 / 720,
                    margin: EdgeInsets.only(top: 15 / defaultScreenHeight * screenHeight ,bottom:26/defaultScreenHeight*screenHeight),
                    child: Stack(
                      alignment: Alignment.center,
                      children: [
                        Positioned(
                          left: 0,
                          top: 0,
                          right: 0,
                          child: Opacity(
                            opacity: 0.5,
                            child: Container(
                              height: screenHeight * 50 / 720,
                              decoration: BoxDecoration(
                                color: Color.fromARGB(255, 253, 212, 36),
                                borderRadius:
                                BorderRadius.all(Radius.circular(2)),
                              ),
                              child: Container(),
                            ),
                          ),
                        ),
                        Positioned(
                          left: 15 /defaultScreenWidth * screenWidth ,
                          top: 10 / defaultScreenHeight * screenHeight,
                          // bottom: 7 / defaultScreenHeight * screenHeight,
                          right: 15 /defaultScreenWidth * screenWidth,
                          child: Html(
                            useRichText: false,
                            data:"<a href='https://ims.brainvalley.in/campaign/?cId=65124'><Font size = '5' color = 'red'><b> Click here to take the SimCAT - 7</b></Font></a>",
                            defaultTextStyle: TextStyle(
                              color: Color.fromARGB(255, 0, 3, 44),
                              fontSize: screenWidth * 12 / 360,
                              fontFamily: "IBMPlexSans",
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            Container(
              height: screenHeight * 40 / 720,
              margin: EdgeInsets.only( top: 40 / defaultScreenHeight * screenHeight),
              child: Center(
                child: Container(
                  width: screenWidth * 130 / 360,
                  height: screenHeight * 40 / 720,

                  child: FlatButton(
                    onPressed: () {
                      _capturePng();
                    },
                    color: Color.fromARGB(255, 0, 171, 251),
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(Radius.circular(3)),
                    ),
                    textColor: Color.fromARGB(255, 255, 255, 255),
                    padding: EdgeInsets.all(0),
                    child: Text(
                      "Print Ticket",
                      style: TextStyle(
                        fontSize: screenHeight * 14 / 720,
                        fontFamily: "IBMPlexSans",
                        fontWeight: FontWeight.w500,
                      ),
                      textAlign: TextAlign.center,
                    ),
                  ),
                ),
              ),
            ),
            Align(
              alignment: Alignment.topCenter,
              child: Container(
                width: screenWidth * 125 / 360,
                height: screenHeight * 28 / 720,
                margin: EdgeInsets.only(
                    top: 30 / defaultScreenHeight * screenHeight, bottom: 24 / defaultScreenHeight * screenHeight),
                child: FlatButton(
                  onPressed: () {
                    _controller.open();
                  },
                  color: Colors.transparent,
                  textColor: Color.fromARGB(255, 126, 145, 154),
                  padding: EdgeInsets.all(0),
                  child: Text(
                    "Cancel Booking",
                    style: TextStyle(
                      fontSize: screenHeight * 14 / 720,
                      fontFamily: "IBMPlexSans",
                    ),
                    textAlign: TextAlign.center,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

//  List<int> buildPdf(PdfPageFormat format) {
//    final pdf.Document doc = pdf.Document();
//
//    final img = Images.decodeImage(imageInMemory);
//    final image = PdfImage(
//      doc.document,
//      image: img.data.buffer.asUint8List(),
//      width: img.width,
//      height: img.height,
//    );
//    print("enter pdf");
//    doc.addPage(
//      pdf.Page(
//        pageFormat: format,
//        build: (pdf.Context context) {
//          return pdf.ConstrainedBox(
//            constraints: const pdf.BoxConstraints.expand(),
//            child: pdf.FittedBox(
//              child: pdf.Container(
//                  child: pdf.Image(image)
//              ),
//            ),
//          );
//        },
//      ),
//    );
//
//    return doc.save();
//  }
  Future<Null> _screenShot() async{
    try{
      RenderRepaintBoundary boundary = _globalKey.currentContext.findRenderObject();
      ui.Image image = await boundary.toImage();
      final directory = (await getExternalStorageDirectory()).path;
      ByteData byteData = await image.toByteData(format: ui.ImageByteFormat.png);
      Uint8List pngBytes = byteData.buffer.asUint8List();
      File imgFile =new File('$directory/screenshot.png');
      imgFile.writeAsBytes(pngBytes);
      Uri fileURI= new Uri.file('$directory/screenshot.png');
      print('Screenshot Path:'+imgFile.path);
      await androidMethodChannel.invokeMethod('takeScreenshot',{'filePath':imgFile.path});
    }
    on PlatformException catch(e){
      print("Exception while taking screenshot:"+e.toString());
    }
  }

  Widget _ticket(){
    final screenHeight = MediaQuery.of(context).size.height;
    final screenWidth = MediaQuery.of(context).size.width;
    final defaultScreenHeight = 720;
    final defaultScreenWidth = 360;
    return RepaintBoundary(
      key: _globalKey,
      child: Container(
        height: screenHeight * 360 / 720,
        margin: EdgeInsets.only(left: 20/defaultScreenWidth*screenWidth, top: 30/defaultScreenHeight*screenHeight, right: 20/defaultScreenWidth*screenWidth),
        child: DottedBorder(
          color: Color.fromARGB(77, 87, 93, 96),
          radius: Radius.circular(5),
          strokeWidth: 1,
          borderType: BorderType.RRect,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              Container(
                height: screenHeight * 41 / 720,
                margin: EdgeInsets.only(left: 15/defaultScreenWidth*screenWidth, top: 17.5/defaultScreenHeight*screenHeight),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    Align(
                      alignment: Alignment.topLeft,
                      child: Text(
                        "Admit Card Token ID",
                        style: TextStyle(
                          color: Color.fromARGB(255, 153, 154, 171),
                          fontSize: screenHeight * 12 / 720,
                          fontFamily: "IBMPlexSans",
                          fontWeight: FontWeight.w500,
                        ),
                        textAlign: TextAlign.left,
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 4/defaultScreenHeight*screenHeight),
                      child: Text(
                        Eventcontent.id,
                        style: TextStyle(
                          color: Color.fromARGB(255, 0, 3, 44),
                          fontSize: screenHeight * 13 / 720,
                          fontFamily: "IBMPlexSans",
                          fontWeight: FontWeight.w500,
                        ),
                        textAlign: TextAlign.left,
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                height: screenHeight * 41 / 720,
                margin: EdgeInsets.only(left: 15/defaultScreenWidth*screenWidth, top: 20/defaultScreenHeight*screenHeight),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    Align(
                      alignment: Alignment.topLeft,
                      child: Text(
                        "Candidate Name",
                        style: TextStyle(
                          color: Color.fromARGB(255, 153, 154, 171),
                          fontSize: screenHeight * 12 / 720,
                          fontFamily: "IBMPlexSans",
                          fontWeight: FontWeight.w500,
                        ),
                        textAlign: TextAlign.left,
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 4/defaultScreenHeight*screenHeight),
                      child: Text(
                        Eventcontent.name,
                        style: TextStyle(
                          color: Color.fromARGB(255, 0, 3, 44),
                          fontSize: screenHeight * 13 / 720,
                          fontFamily: "IBMPlexSans",
                          fontWeight: FontWeight.w500,
                        ),
                        textAlign: TextAlign.left,
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                height: screenHeight * 41 / 720,
                margin: EdgeInsets.only(left: 15/defaultScreenWidth*screenWidth, top: 20/defaultScreenHeight*screenHeight),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    Align(
                      alignment: Alignment.topLeft,
                      child: Text(
                        "Candidate Email",
                        style: TextStyle(
                          color: Color.fromARGB(255, 153, 154, 171),
                          fontSize: screenHeight * 12 / 720,
                          fontFamily: "IBMPlexSans",
                          fontWeight: FontWeight.w500,
                        ),
                        textAlign: TextAlign.left,
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 4/defaultScreenHeight*screenHeight),
                      child: Text(
                        Eventcontent.email,
                        style: TextStyle(
                          color: Color.fromARGB(255, 0, 3, 44),
                          fontSize: screenHeight * 13 / 720,
                          fontFamily: "IBMPlexSans",
                          fontWeight: FontWeight.w500,
                        ),
                        textAlign: TextAlign.left,
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                height: screenHeight * 41 / 720,
                margin: EdgeInsets.only(left: 15/defaultScreenWidth*screenWidth, top: 20 / defaultScreenHeight * screenHeight),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    Align(
                      alignment: Alignment.topLeft,
                      child: Container(
                        width: screenWidth * 130 / 360,
                        height: screenHeight * 41 / 720,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          children: [
                            Align(
                              alignment: Alignment.topLeft,
                              child: Text(
                                "Date",
                                style: TextStyle(
                                  color:
                                  Color.fromARGB(255, 153, 154, 171),
                                  fontSize: screenHeight * 12 / 720,
                                  fontFamily: "IBMPlexSans",
                                  fontWeight: FontWeight.w500,
                                ),
                                textAlign: TextAlign.left,
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.only(top: 4/defaultScreenHeight*screenHeight),
                              child: Text(
                                Eventcontent.date,
                                style: TextStyle(
                                  color: Color.fromARGB(255, 0, 3, 44),
                                  fontSize: screenHeight * 13 / 720,
                                  fontFamily: "IBMPlexSans",
                                  fontWeight: FontWeight.w500,
                                ),
                                textAlign: TextAlign.left,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    Spacer(),
                    Align(
                      alignment: Alignment.topLeft,
                      child: Container(
                        width: screenWidth * 150 / 360,
                        height: screenHeight * 41 / 720,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          children: [
                            Align(
                              alignment: Alignment.topLeft,
                              child: Text(
                                "Time",
                                style: TextStyle(
                                  color:
                                  Color.fromARGB(255, 153, 154, 171),
                                  fontSize: screenHeight * 12 / 720,
                                  fontFamily: "IBMPlexSans",
                                  fontWeight: FontWeight.w500,
                                ),
                                textAlign: TextAlign.left,
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.only(top: 4/defaultScreenHeight*screenHeight),
                              child: Text(
                                Eventcontent.time,
                                style: TextStyle(
                                  color: Color.fromARGB(255, 0, 3, 44),
                                  fontSize: screenHeight * 13 / 720,
                                  fontFamily: "IBMPlexSans",
                                  fontWeight: FontWeight.w500,
                                ),
                                textAlign: TextAlign.left,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                height: screenHeight * 90 / 720,
                margin: EdgeInsets.only(left: 15/defaultScreenWidth*screenWidth, top: 20/defaultScreenHeight*screenHeight, right: 15/defaultScreenWidth*screenWidth),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    Align(
                      alignment: Alignment.topLeft,
                      child: Text(
                        "Test Venue",
                        style: TextStyle(
                          color: Color.fromARGB(255, 153, 154, 171),
                          fontSize: screenHeight * 12 / 720,
                          fontFamily: "IBMPlexSans",
                          fontWeight: FontWeight.w500,
                        ),
                        textAlign: TextAlign.left,
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 4/defaultScreenHeight*screenHeight),
                      child: Text(
                        Eventcontent.test_venu,
                        style: TextStyle(
                          color: Color.fromARGB(255, 0, 3, 44),
                          fontSize: screenHeight * 13 / 720,
                          fontFamily: "IBMPlexSans",
                          fontWeight: FontWeight.w500,
                        ),
                        textAlign: TextAlign.left,
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _panel() {
    final screenHeight = MediaQuery.of(context).size.height;
    final screenWidth = MediaQuery.of(context).size.width;
    final defaultScreenHeight = 720;
    final defaultScreenWidth = 360;
    return Container(
      height: screenHeight * 265 / 720,
      margin: EdgeInsets.only(bottom: 18 / defaultScreenHeight * screenHeight),
      //color: Colors.black,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.end,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Align(
            alignment: Alignment.topCenter,
            child: Container(
              width: screenWidth * 140 / 360,
              height: screenHeight * 130 / 720,
              margin: EdgeInsets.only(bottom: 18 / defaultScreenHeight * screenHeight),
              child: new SvgPicture.asset(
                getBookSlotSvg.cancelbooking,
                fit: BoxFit.contain,
                //color: Colors.red,
              ),
            ),
          ),

          Align(
            alignment: Alignment.topCenter,
            child: Container(
              margin: EdgeInsets.only(bottom: 32 / defaultScreenHeight * screenHeight),
              child: Text(
                "Are you sure you want\nto cancel booking?",
                style: TextStyle(
                  color: Color.fromARGB(255, 0, 0, 0),
                  fontSize: screenHeight * 16 / 720,
                  fontFamily: "IBMPlexSans",
                  fontWeight: FontWeight.w500,
                ),
                textAlign: TextAlign.center,
              ),
            ),
          ),
          Container(
            height: screenHeight * 40 / 720,
            margin: EdgeInsets.only(left: 43 / defaultScreenWidth * screenWidth, bottom: 40/ defaultScreenHeight * screenHeight, right: 42 / defaultScreenWidth * screenWidth),
            child: Row(
              children: [
                Container(
                  width: screenWidth * 130 / 360,
                  height: screenHeight * 40 / 720,
                  child: FlatButton(
                    onPressed: () {
                      _controller.close();
                    },
                    color: Color.fromARGB(255, 242, 244, 244),
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(Radius.circular(3)),
                    ),
                    textColor: Color.fromARGB(255, 126, 145, 154),
                    padding: EdgeInsets.all(0),
                    child: Text(
                      "No",
                      style: TextStyle(
                        fontSize: screenHeight * 14 / 720,
                        fontFamily: "IBMPlexSans",
                        fontWeight: FontWeight.w500,
                      ),
                      textAlign: TextAlign.center,
                    ),
                  ),
                ),
                Spacer(),
                Container(
                  width: screenWidth * 130 / 360,
                  height: screenHeight * 40 / 720,
                  child: FlatButton(
                    onPressed: (){
                      int count = 0;
                      Navigator.popUntil(context, (route) {
                        return count++ == 2;
                      }
                      );
                    },
//                      //  onYesPressed(context),
//                    //_controller.close(),
//                    AppRoutes.pop(context),
                    color: Color.fromARGB(255, 0, 171, 251),
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(Radius.circular(2)),
                    ),
                    textColor: Color.fromARGB(255, 255, 255, 255),
                    padding: EdgeInsets.all(0),
                    child: Text(
                      "Yes",
                      style: TextStyle(
                        fontSize: screenHeight * 14 / 720,
                        fontFamily: "IBMPlexSans",
                        fontWeight: FontWeight.w500,
                      ),
                      textAlign: TextAlign.center,
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:imsindia/components/custom_expansion_panel.dart';
import 'package:imsindia/components/navigation_bar.dart';
import 'package:imsindia/utils/colors.dart';
import 'package:imsindia/views/book_a_test_screens/simcat_booking.dart';
import 'package:imsindia/resources/strings/book_a_test.dart';
import 'package:imsindia/api/bloc/services.dart';
import 'package:imsindia/views/book_a_test_screens/ticket_screen.dart';
import 'package:imsindia/views/book_a_test_screens/ticket_screen_backup.dart';
import 'package:intl/intl.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../../routers/routes.dart';
import 'package:imsindia/utils/Url.dart';
import 'package:imsindia/utils/global.dart' as global;

var screenWidthTotal;
var screenHeightTotal;
var _safeAreaHorizontal;
var _safeAreaVertical;
var screenHeight;
var screenWidth;

List titles = [
  'SimCAT 1',
  'SimCAT 2',
  'SimCAT 3',
  'SimCAT 4',
  'SimCAT 5',
];
List insideList = [
  'Availability',
  'Test Date',
  'Mode',
];

List datelist = [
  '20 April 2018',
  ' 20 April 2018 - 7 May 2018',
  'Center Proctored',
];

class BookHomeScreen extends StatefulWidget {
  @override
  BookHomeScreenState createState() => BookHomeScreenState();
}

class BookHomeScreenState extends State<BookHomeScreen> {
  int _activeMeterIndex;
  var bookASlotTitles = [];
  var bookASlotTestFromDate =[];
  var bookASlotTestToDate=[];
  var bookASlotMode=[];
  var studentBookedSlotValue=[];
  var bookSlotIds=[];
  var studentBookingIdsForAdminCard=[];
  ///** book a test home page API(book slot cards api) **///
  void bookingSlotsData() async{
    ApiService().getAPI(URL.BOOK_A_TEST_API_TO_GET_SLOTS + 'a8db8976c04bb6015f01c5bc2689ed98', global.headersWithAuthorizationKey)
        .then((returnValue) {
      setState(() {
        if (returnValue[0] == 'Fetched Successfully') {
          var totalData = returnValue[1]['data'];
          for (var index = 0; index < totalData.length; index++) {
            bookASlotTitles.add(totalData[index]['component']['title']);
            bookASlotTestFromDate.add(utcToDate(totalData[index]['component']['slotBooking_FromDate']));
            bookASlotTestToDate.add(utcToDate(totalData[index]['component']['slotBooking_ToDate']));
            bookASlotMode.add(totalData[index]['component']['Mode']);
            studentBookedSlotValue.add(totalData[index]['studentBookedSlot']['IsUserSlotBooked']);
            bookSlotIds.add(totalData[index]['id']);
            studentBookingIdsForAdminCard.add(totalData[index]['studentBookedSlot']['studentbookingId']);
          }
        }
      });
    });
  }
  ///* set id and title for book test slot page *///
   void setIdAndTitleForSimcatBooking(value1,value2) async {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      prefs.setString("idForSimcatBooking", value1);
      prefs.setString("titleForSimcatBooking", value2);

   }
  ///* set student booking id for admin card *///
  void setStudentBookingIdForAdminCard(value) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString("studentBookingIdForAdimnCard", value);
  }

  ///* Converting UTC to Datetime Format *///
  String utcToDate(String test_date) {
    String _ignoreSubMicro(String s) {
      if (s.length > 27) return s.substring(0, 26) + s[s.length - 1];
      return s;
    }
//    const invalidNano = "2019-05-31T10:35:45.347333481Z";
//    //const vvgdfg = "25-08-2019T12:29:35.291Z";
    var date_test = test_date;
    String obj1 = date_test.split("T")[0];
    String obj2 = date_test.split("T")[1];
    DateFormat dateFormat = DateFormat("dd-MM-yyyy");
    String string = dateFormat.format(DateTime.now());
    DateTime dateTime = dateFormat.parse(obj1);
    String new_date = dateTime.toString();
    var newdata = new_date.split(" ")[0]+"T"+obj2;
    final date = DateTime.parse(_ignoreSubMicro(newdata));
    String _year = DateFormat("yyyy").format(date);
    String month = DateFormat("MMM").format(date);
    String _day = DateFormat("dd").format(date);
    String testdate = _day +" "+month+" "+_year;
    return testdate;
  }


  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    bookingSlotsData();
  }
  Widget build(BuildContext context) {
    var _mediaQueryData = MediaQuery.of(context);
    screenWidthTotal = _mediaQueryData.size.width;
    screenHeightTotal = _mediaQueryData.size.height;
    _safeAreaHorizontal =
        _mediaQueryData.padding.left + _mediaQueryData.padding.right;
    _safeAreaVertical =
        _mediaQueryData.padding.top + _mediaQueryData.padding.bottom;

    screenHeight = screenHeightTotal - _safeAreaVertical;
    screenWidth = screenWidthTotal - _safeAreaHorizontal;

    return Scaffold(
        appBar: AppBar(
          brightness: Brightness.light,
          centerTitle: false,
          titleSpacing: 0.0,
          backgroundColor: Colors.white,
          elevation: 0.0,
          iconTheme: IconThemeData(
            color: NeutralColors.black, //change your color here
          ),
          title: Container(
            child: Text(
              BookTestScreenStrings.bookaTest,
              style: TextStyle(
                color:NeutralColors.textblack,
                fontSize: (16 / 720) * screenHeight,
                fontFamily: "IBMPlexSans",
                fontWeight: FontWeight.w500,
              ),
              textAlign: TextAlign.left,
            ),
          ),
        ),
//        drawer: NavigationDrawer(),
        body: bookASlotTitles.length==0?Center(child:CircularProgressIndicator()):Container(
          color: NeutralColors.pureWhite,
          child: Column(
            children: <Widget>[
              new Expanded(
                child: new ListView.builder(
                  itemBuilder: (BuildContext context, int i) {
                    return Container(
                      decoration: BoxDecoration(
                        boxShadow: [BoxShadow(
                          offset: Offset(0, 5),
                          blurRadius: 10,
                          color: (_activeMeterIndex == i)?Color(0xffeaeaea): Colors.transparent,
                        ),],
                        borderRadius: new BorderRadius.only(topLeft:new Radius.circular(5) ,topRight:new Radius.circular(5),bottomLeft:new Radius.circular(5) ,bottomRight:new Radius.circular(5) ),
                        border: Border.all(color: Colors.transparent),
                      ),
                      margin: EdgeInsets.only(
                          top: (10.0 / 720) * screenHeight,
                          left: (20.0 / 360) * screenWidth,
                          right: (20.0 / 360) * screenWidth),
                      child: CustomExpansionPanelList(
                        expansionHeaderHeight: (45 / 720) * screenHeight,
                        iconColor: (_activeMeterIndex == i)?NeutralColors.pureWhite:NeutralColors.black,
                        backgroundColor1: (_activeMeterIndex == i)?NeutralColors.purpleish_blue:SemanticColors.light_purpely.withOpacity(0.1000000014901161),
                        backgroundColor2:(_activeMeterIndex == i)? NeutralColors.purpleish_blue:SemanticColors.dark_purpely.withOpacity(0.07),expansionCallback: (int index, bool status) {
                          setState(() {
                            _activeMeterIndex =
                                _activeMeterIndex == i ? null : i;
                          });
                        },
                        children: [
                          new ExpansionPanel(
                              canTapOnHeader: true,
                              isExpanded: _activeMeterIndex == i,
                              headerBuilder: (BuildContext context,
                                      bool isExpanded) =>
                                  new Container(
                                    alignment: Alignment.centerLeft,
                                    child: new Padding(
                                      padding: EdgeInsets.only(
                                          left: (15 / 320) * screenWidth),
                                      child: new Text(bookASlotTitles[i],
                                          style: new TextStyle(
                                              fontSize:
                                                  (13.4 / 720) * screenHeight,
                                              fontFamily:"IBMPlexSans",
                                              fontWeight: FontWeight.w500,
                                              color: (_activeMeterIndex == i)
                                                  ? Colors.white
                                                  : NeutralColors.charcoal_grey)),
                                    ),
                                  ),
                              body: Container(
                               // height: (295/720)*screenHeight,
                                color: NeutralColors.pureWhite,
                                child: Column(
                                  children: <Widget>[
                                    Container(
                                      margin: EdgeInsets.only(top: 12 / 720 * screenHeight),
                                      decoration: new BoxDecoration(border: new Border.all(
                                        color: NeutralColors.pureWhite),
                                    color: NeutralColors.pureWhite),
                                    child: new Column(
                                      mainAxisAlignment: MainAxisAlignment.start,
                                      children: <Widget>[
                                        Container(
                                          child: Column(
                                            children: <Widget>[
                                              Container(
                                                  width: (320 / 360) *
                                                      screenWidth,
                                                  margin: EdgeInsets.only(

                                                      left: (15 / 360) *
                                                          screenWidth),
                                                  child: new Text(
                                                    "Test Date",
                                                    style: TextStyle(
                                                      color:
                                                      NeutralColors.bluey_grey,
                                                      fontSize: (14 / 678) *
                                                          screenHeight,
                                                      fontFamily:
                                                      "IBMPlexSans",
                                                      fontWeight:
                                                      FontWeight.w500,
                                                    ),
                                                    textAlign: TextAlign.left,
                                                  )),
                                              Container(
                                                  width: (320 / 360) *
                                                      screenWidth,
                                                  margin: EdgeInsets.only(
                                                      top: (5 / 678) *
                                                          screenHeight,
                                                      left: (15 / 360) *
                                                          screenWidth,
                                                      right: (15 / 360) *
                                                          screenWidth),
                                                  child: new Row(
                                                    mainAxisAlignment:
                                                    MainAxisAlignment

                                                        .spaceBetween,
                                                    children: <Widget>[
                                                      Text(
                                                        bookASlotTestFromDate[i].toString()+" - "+bookASlotTestToDate[i].toString(),
                                                        style: TextStyle(
                                                          color:NeutralColors.dark_blue,
                                                          fontSize: (14 / 678) * screenHeight,
                                                          fontFamily: "IBMPlexSans",
                                                          fontWeight: FontWeight.w500,
                                                        ),
                                                        textAlign:
                                                        TextAlign.left,
                                                      ),
                                                    ],
                                                  )),
                                            ],
                                          ),
                                        ),
                                        Container(
                                          margin: EdgeInsets.only(top: (12/720)*screenHeight),
                                          child: Column(
                                            children: <Widget>[
                                              Container(
                                                  width: (320 / 360) *
                                                      screenWidth,
                                                  margin: EdgeInsets.only(
                                                      top: (12 / 678) *
                                                          screenHeight,
                                                      left: (15 / 360) *
                                                          screenWidth),
                                                  child: new Text(
                                                    "Mode",
                                                    style: TextStyle(
                                                      color:
                                                      NeutralColors.bluey_grey,
                                                      fontSize: (14 / 678) *
                                                          screenHeight,
                                                      fontFamily:
                                                      "IBMPlexSans",
                                                      fontWeight:
                                                      FontWeight.w500,
                                                    ),
                                                    textAlign: TextAlign.left,
                                                  )),
                                              Container(
                                                  width: (320 / 360) *
                                                      screenWidth,
                                                  margin: EdgeInsets.only(
                                                      top: (5 / 678) *
                                                          screenHeight,
                                                      left: (15 / 360) *
                                                          screenWidth,
                                                      right: (15 / 360) *
                                                          screenWidth),
                                                  child: new Row(
                                                    mainAxisAlignment:
                                                    MainAxisAlignment
                                                        .spaceBetween,
                                                    children: <Widget>[
                                                      Text(
                                                        bookASlotMode[i][0].toUpperCase()+bookASlotMode[i].substring(1)+" Proctored",
                                                        style: TextStyle(
                                                          color:NeutralColors.dark_blue,
                                                          fontSize: (14 / 678) * screenHeight,
                                                          fontFamily: "IBMPlexSans",
                                                          fontWeight: FontWeight.w500,
                                                        ),
                                                        textAlign:
                                                        TextAlign.left,
                                                      ),
                                                    ],
                                                  )),
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                    ),
                                    Container(
                                      width: (238 / 360) * screenWidth,
                                      height: (40 / 678) * screenHeight,
                                      margin: EdgeInsets.only(
                                          top: 20/720 *screenHeight,
                                          left: 40 / 360 * screenWidth,
                                          right: 40 / 360 * screenWidth,
                                          bottom: 15 / 720 * screenHeight),
                                      decoration: BoxDecoration(
                                        gradient: LinearGradient(
                                          begin: Alignment(0, 0),
                                          end: Alignment(
                                              1.0199999809265137,
                                              1.0099999904632568),
                                          colors: [
                                            NeutralColors.grycolor,
                                            SemanticColors.dark_purpely,
                                          ],
                                        ),
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(2)),
                                      ),
                                      child: FlatButton(
                                        onPressed: () {
                                          if(studentBookedSlotValue[i]!=true){
                                          setIdAndTitleForSimcatBooking(bookSlotIds[i],bookASlotTitles[i]);
                                          AppRoutes.push(context, SimcatBookingWidget());
                                          }
                                          else{
                                            setStudentBookingIdForAdminCard(studentBookingIdsForAdminCard[i]);
                                            Navigator.push(
                                                context, MaterialPageRoute(builder: (context) => SimcatTicket_screen_backup()));
                                          }
                                        },
                                        shape: RoundedRectangleBorder(
                                          borderRadius: BorderRadius.all(
                                              Radius.circular(2)),
                                        ),
                                        textColor: GradientColors.pureWhite,
                                        padding: EdgeInsets.all(0),
                                        child: Text(
                                          studentBookedSlotValue[i]==true?BookTestScreenStrings.viewAdminCard:BookTestScreenStrings.bookaSlot,
                                          style: TextStyle(
                                            fontSize: 14,
                                            fontFamily: "IBMPlexSans",
                                            fontWeight: FontWeight.w500,
                                          ),
                                          textAlign: TextAlign.center,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              )),
                        ],
                      ),
                    );
                  },
                  itemCount: bookASlotTitles.length,
                ),
              ),
            ],
          ),
        ));
  }
}

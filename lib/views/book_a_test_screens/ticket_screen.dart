import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_email_sender/flutter_email_sender.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:flutter_svg/svg.dart';
import 'package:imsindia/api/bloc/services.dart';
import 'package:imsindia/components/bottom_navigation.dart';
import 'package:imsindia/components/navigation_bar.dart';
import 'package:imsindia/resources/strings/book_a_test.dart';
import 'package:imsindia/utils/Url.dart';
import 'package:imsindia/utils/screen_util.dart';
import 'package:intl/intl.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sliding_up_panel/sliding_up_panel.dart';
import 'package:imsindia/utils/book_a_slot.dart';
//Commented for Printing issue

//import 'package:pdf/pdf.dart';
//import 'package:pdf/widgets.dart' as pdf;
//import 'package:printing/printing.dart';

import 'dart:typed_data';
import 'dart:ui' as ui;
import 'package:flutter/rendering.dart';
import 'dart:io';
import 'package:image/image.dart' as Images;
import 'package:path_provider/path_provider.dart';
//import '//package:keyboard_visibility/keyboard_visibility.dart';
import 'book_test_homepage.dart';
import 'package:imsindia/utils/global.dart' as global;



class EventData {
  String id;
  String name;
  String email;
  String date;
  String time;
  String test_venu;

  EventData({
    this.id,
    this.name,
    this.email,
    this.date,
    this.time,
    this.test_venu,


  });
}

class SimcatTicket_screen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return SimcatTicketScreenWidgetState();
  }
}

class SimcatTicketScreenWidgetState extends State<SimcatTicket_screen> {
  var sudentBookingIdForAdminCard;
  var sudentBookingtokenIdForAdminCard;
  var adimitCardToken;
  var candidateName;
  var Date;
  var timeFrom;
  var timeTo;
  var testVenueName;
  var testVenueCountry;
  var testVenueState;
  var testVenueCity;
  var testVenueStreet;
  var venueNotes;
  var testSlotIdForCancelAdmitApi;
  var sudentBookingIdForCancelAdminCard;
  var cancelMsgFromApi;
  ///* get student Booking Id /*//
  getStudentBookingId() async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    sudentBookingIdForAdminCard=prefs.getString("studentBookingIdForAdimnCard");
  }
  ///* get api details for admin card *///
  void apiForAdminCardData() async{
    ApiService().getAPI(URL.BOOK_A_TEST_API_TO_GET_ADMIN_CARD_DATA + sudentBookingIdForAdminCard, global.headersWithAuthorizationKey)
        .then((returnValue) {
      setState(() {
        if (returnValue[0] == 'Fetched Successfully') {
          var totalData = returnValue[1]['data'];
          sudentBookingtokenIdForAdminCard=totalData[0]['admitCardTokenId'];
          timeFrom=totalData[0]['timeFrom'];
          Date = DateFormat("dd-MM-yyyy").format(DateTime.parse(totalData[0]['examDate']));
          testVenueName=totalData[0]['venueName'];
          testVenueCountry=totalData[0]['venueAddress']['country'];
          testVenueState=totalData[0]['venueAddress']['state'];
          testVenueCity=totalData[0]['venueAddress']['city'];
          testVenueStreet=totalData[0]['venueAddress']['street'];
          venueNotes=totalData[0]['venueNotes'];
          testSlotIdForCancelAdmitApi=totalData[0]['testSlotId'];
          sudentBookingIdForCancelAdminCard=totalData[0]['studentbookingId'];
        }
      });
    });
  }
/// cancel button api ///
  void cancelAdmitCardApi() async{
    Map putdata = {
      "studentBookedId": sudentBookingIdForCancelAdminCard,
      "testSlotId": testSlotIdForCancelAdmitApi
    };
    ApiService().putAPI(URL.BOOK_A_TEST_API_TO_CANCEL_ADMIT_CARD,putdata ,global.headersWithAuthorizationKey)
        .then((returnValue) {
      setState(() {
        if (returnValue[0] == 'Cancelled Successfully') {
          var totalData = returnValue[1]['data'];
          cancelMsgFromApi=returnValue[1]['message'];
        }
      });
    });
  }
  final Eventcontent = EventData(
    id: "676916-WF6FC4",
    name: "Stephen Moses",
    email: "stephen@gmail.com",
    date:"02-07-2019",
    time:  "10:00 AM - 02:00 PM",
    test_venu: "Mother Tekla Auditorium, 143, General KS Thimayya Rd, Craig Park Layout, Ashok Nagar, Bengaluru, Karnataka 560025.",

  );

  GlobalKey _globalKey = new GlobalKey();
  static const androidMethodChannel = const MethodChannel('team.native.io/screenshot');


  bool inside = false;
  Uint8List imageInMemory;

  Future <Uint8List> _capturePng() async {
    try {
      inside = true;
      RenderRepaintBoundary boundary =
      _globalKey.currentContext.findRenderObject();
      ui.Image image = await boundary.toImage(pixelRatio: 3.0);
      ByteData byteData =
      await image.toByteData(format: ui.ImageByteFormat.png);
      Uint8List pngBytes = byteData.buffer.asUint8List();
      //String bs64 = base64Encode(pngBytes);

      setState(() {
        imageInMemory = pngBytes;
        inside = false;
//        Printing.layoutPdf(
//          onLayout: buildPdf,
//        );
      });
      return pngBytes;
    } catch (e) {
    }
  }


  void onYesPressed(BuildContext context) => Navigator.push(context,
      MaterialPageRoute(builder: (context) => BookHomeScreen()));

  final _controller = PanelController();
  double _panelHeightClosed = 0.0;
  final Email email = Email(

    body: 'Email body',
    subject: 'Email subject',
    recipients: ['example@example.com'],
    cc: ['cc@example.com'],
    bcc: ['bcc@example.com'],
    //attachmentPath: '/path/to/attachment.zip',

  );
  FocusNode _focusNode = new FocusNode();
  GlobalKey<ScaffoldState> _key;
  @override
  void initState() {
    // TODO: implement initState
    _key = GlobalKey<ScaffoldState>();
    // KeyboardVisibilityNotification().addNewListener(
    //   onHide: () {
    //     _focusNode.unfocus();
    //   },
    // );
    super.initState();
    getStudentBookingId().then((_){
      apiForAdminCardData();
    });
  }
  void dispose() {
    super.dispose();
  }


  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      systemNavigationBarColor: Colors.grey[200],
      systemNavigationBarIconBrightness: Brightness.dark,
      systemNavigationBarDividerColor: Colors.black,
    ));
    final screenHeight = MediaQuery.of(context).size.height;
    final screenWidth = MediaQuery.of(context).size.width;

    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: true,
        backgroundColor: _focusNode.hasFocus
            ? Colors.black.withOpacity(0.25)
            : Colors.white,
        elevation: 0.0,
        centerTitle: false,
        brightness: Brightness.light,
        titleSpacing: 0.0,
        iconTheme: IconThemeData(
          color: Colors.black,
        ),
        title: Text(
          "Book a Test",
          style: TextStyle(
            color: Colors.black,
            fontWeight: FontWeight.w500,
            fontFamily: "IBMPlexSans",
            fontSize: (16/Constant.defaultScreenWidth)*screenWidth,
            fontStyle: FontStyle.normal,
          ),
        ),
      ),
      drawer:NavigationDrawer(),
//      bottomNavigationBar: BottomNavigation(),
      body: sudentBookingIdForAdminCard==null?Center(child:Text("Nothing To Display")):Container(
        child: SlidingUpPanel(
          maxHeight: screenHeight / 2,
          minHeight: _panelHeightClosed,
          parallaxEnabled: false,
          parallaxOffset: .5,
          //defaultPanelState: PanelState.OPEN,
          isDraggable: true,
          body: _body(),
          panel: _panel(),
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(25.0), topRight: Radius.circular(25.0)),
          //onPanelSlide: (double pos) => setState((){
          //_fabHeight =  _initFabHeight/2;
          //}),
          controller: _controller,
        ),
      ),
    );
  }

  Widget _body() {
    final screenHeight = MediaQuery.of(context).size.height;
    final screenWidth = MediaQuery.of(context).size.width;
    final defaultScreenHeight = 720;
    final defaultScreenWidth = 360;
    return SingleChildScrollView(
      child: Container(
        decoration: BoxDecoration(
          color: Color.fromARGB(255, 255, 255, 255),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Align(
              alignment: Alignment.topLeft,
            ),
            _ticket(),
            Container(
              width: screenWidth * 320 / 360,
              margin: EdgeInsets.only(left: 20 / defaultScreenWidth * screenWidth, top: 10 / defaultScreenHeight * screenHeight, right: 20 / defaultScreenWidth * screenWidth),
              decoration: BoxDecoration(
                color: Color.fromARGB(255, 242, 246, 248),
                borderRadius: BorderRadius.all(Radius.circular(5)),
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  Align(
                    alignment: Alignment.topLeft,
                    child: Container(
                      height: 22/defaultScreenHeight*screenHeight,
                      margin: EdgeInsets.only(left: 15 / defaultScreenWidth * screenWidth, top: 14 / defaultScreenHeight * screenHeight),
                      child: Text(
                        "Important Instructions",
                        style: TextStyle(
                          color: Color.fromARGB(255, 0, 3, 44),
                          fontSize: screenHeight * 13 / 720,
                          fontFamily: "IBMPlexSans",
                          fontWeight: FontWeight.w500,
                        ),
                        textAlign: TextAlign.left,
                      ),
                    ),
                  ),
                  ///* important instructions list *//
                  getTextWidgets(BookTestScreenStrings.listOfInstructions),
                  /// venue notes ///
                  venueNotes==" "?Text(""):Container(
                    height: screenHeight * 50 / 720,
                    margin: EdgeInsets.only(top: 15 / defaultScreenHeight * screenHeight,bottom: 25/defaultScreenHeight*screenHeight ),
                    child: Stack(
                      alignment: Alignment.center,
                      children: [
                        Positioned(
                          left: 0,
                          top: 0,
                          right: 0,
                          child: Opacity(
                            opacity: 0.5,
                            child: Container(
                              height: screenHeight * 50 / 720,
                              decoration: BoxDecoration(
                                color: Color.fromARGB(255, 253, 212, 36),
                                borderRadius:
                                BorderRadius.all(Radius.circular(2)),
                              ),
                              child: Container(),
                            ),
                          ),
                        ),
                        Positioned(
                          left: 15 /defaultScreenWidth * screenWidth ,
                          top: 10 / defaultScreenHeight * screenHeight,
                          // bottom: 7 / defaultScreenHeight * screenHeight,
                          right: 15 /defaultScreenWidth * screenWidth,
                          child: Html(
                            data: "<a href='https://ims.brainvalley.in/campaign/?cId=65124'><Font size = '5' color = 'red'><b> Click here to take the SimCAT - 7</b></Font></a>",
                            defaultTextStyle: TextStyle(
                              color: Color.fromARGB(255, 0, 3, 44),
                              fontSize: screenWidth * 12 / 360,
                              fontFamily: "IBMPlexSans",
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            Container(
              height: screenHeight * 40 / 720,
              margin: EdgeInsets.only(left: 43 / defaultScreenWidth * screenWidth, top: 40 / defaultScreenHeight * screenHeight, right: 42 / defaultScreenWidth * screenWidth),
              child: Row(
                children: [
                  Container(
                    width: screenWidth * 130 / 360,
                    height: screenHeight * 40 / 720,

                    child: FlatButton(
                      onPressed: () {
                        _capturePng();
                      },
                      color: Color.fromARGB(255, 0, 171, 251),
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.all(Radius.circular(3)),
                      ),
                      textColor: Color.fromARGB(255, 255, 255, 255),
                      padding: EdgeInsets.all(0),
                      child: Text(
                        "Print Ticket",
                        style: TextStyle(
                          fontSize: screenHeight * 14 / 720,
                          fontFamily: "IBMPlexSans",
                          fontWeight: FontWeight.w500,
                        ),
                        textAlign: TextAlign.center,
                      ),
                    ),
                  ),
                  Spacer(),
                  Container(
                    width: screenWidth * 130 / 360,
                    height: screenHeight * 40 / 720,
                    //margin: EdgeInsets.only(top: 40/defaultScreenHeight*screenHeight),
                    child: FlatButton(
                      onPressed: () async {
                        // _capturePngEmail();
                        _screenShot();
                        await FlutterEmailSender.send(email);

                      },
                      color: Color.fromARGB(255, 0, 171, 251),
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.all(Radius.circular(2)),
                      ),
                      textColor: Color.fromARGB(255, 255, 255, 255),
                      padding: EdgeInsets.all(0),
                      child: Text(
                        "Email Ticket",
                        style: TextStyle(
                          fontSize: screenHeight * 14 / 720,
                          fontFamily: "IBMPlexSans",
                          fontWeight: FontWeight.w500,
                        ),
                        textAlign: TextAlign.center,
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Align(
              alignment: Alignment.topCenter,
              child: Container(
                width: screenWidth * 125 / 360,
                height: screenHeight * 28 / 720,
                margin: EdgeInsets.only(
                    top: 30 / defaultScreenHeight * screenHeight, bottom: 24 / defaultScreenHeight * screenHeight),
                child: FlatButton(
                  onPressed: () {
                    _controller.open();
                  },
                  color: Colors.transparent,
                  textColor: Color.fromARGB(255, 126, 145, 154),
                  padding: EdgeInsets.all(0),
                  child: Text(
                    "Cancel Booking",
                    style: TextStyle(
                      fontSize: screenHeight * 14 / 720,
                      fontFamily: "IBMPlexSans",
                    ),
                    textAlign: TextAlign.center,
                  ),
                ),
              ),
            ),
            Container(height: 200.0,)
          ],
        ),
      ),
    );
  }
// Commented for Printing plugin issue
//  List<int> buildPdf(PdfPageFormat format) {
//    final pdf.Document doc = pdf.Document();
//
//    final img = Images.decodeImage(imageInMemory);
//    final image = PdfImage(
//      doc.document,
//      image: img.data.buffer.asUint8List(),
//      width: img.width,
//      height: img.height,
//    );
//    doc.addPage(
//      pdf.Page(
//        pageFormat: format,
//        build: (pdf.Context context) {
//          return pdf.ConstrainedBox(
//            constraints: const pdf.BoxConstraints.expand(),
//            child: pdf.FittedBox(
//              child: pdf.Container(
//                  child: pdf.Image(image)
//              ),
//            ),
//          );
//        },
//      ),
//    );
//
//    return doc.save();
//  }


  Future<Null> _screenShot() async{

    try{
      RenderRepaintBoundary boundary = _globalKey.currentContext.findRenderObject();
      ui.Image image = await boundary.toImage();
      final directory = (await getExternalStorageDirectory()).path;
      ByteData byteData = await image.toByteData(format: ui.ImageByteFormat.png);
      Uint8List pngBytes = byteData.buffer.asUint8List();
      File imgFile =new File('$directory/screenshot.png');
      imgFile.writeAsBytes(pngBytes);
      Uri fileURI= new Uri.file('$directory/screenshot.png');
      await androidMethodChannel.invokeMethod('takeScreenshot',{'filePath':imgFile.path});
    }
    on PlatformException catch(e){
    }
  }

  Widget _ticket(){
    final screenHeight = MediaQuery.of(context).size.height;
    final screenWidth = MediaQuery.of(context).size.width;
    final defaultScreenHeight = 720;
    final defaultScreenWidth = 360;
    return RepaintBoundary(
      key: _globalKey,
      child: Container(
        margin: EdgeInsets.only(left: 20/defaultScreenWidth*screenWidth, top: 30/defaultScreenHeight*screenHeight, right: 20/defaultScreenWidth*screenWidth),
        child: DottedBorder(
          color: Color.fromARGB(77, 87, 93, 96),
          radius: Radius.circular(5),
          strokeWidth: 1,
          borderType: BorderType.RRect,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              Container(
                height: screenHeight * 41 / 720,
                margin: EdgeInsets.only(left: 15/defaultScreenWidth*screenWidth, top: 17.5/defaultScreenHeight*screenHeight),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    Align(
                      alignment: Alignment.topLeft,
                      child: Text(
                        "Admit Card Token ID",
                        style: TextStyle(
                          color: Color.fromARGB(255, 153, 154, 171),
                          fontSize: screenHeight * 12 / 720,
                          fontFamily: "IBMPlexSans",
                          fontWeight: FontWeight.w500,
                        ),
                        textAlign: TextAlign.left,
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 4/defaultScreenHeight*screenHeight),
                      child: Text(
                        sudentBookingtokenIdForAdminCard.toString(),
                        style: TextStyle(
                          color: Color.fromARGB(255, 0, 3, 44),
                          fontSize: screenHeight * 13 / 720,
                          fontFamily: "IBMPlexSans",
                          fontWeight: FontWeight.w500,
                        ),
                        textAlign: TextAlign.left,
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                height: screenHeight * 41 / 720,
                margin: EdgeInsets.only(left: 15/defaultScreenWidth*screenWidth, top: 20/defaultScreenHeight*screenHeight),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    Align(
                      alignment: Alignment.topLeft,
                      child: Text(
                        "Candidate Name",
                        style: TextStyle(
                          color: Color.fromARGB(255, 153, 154, 171),
                          fontSize: screenHeight * 12 / 720,
                          fontFamily: "IBMPlexSans",
                          fontWeight: FontWeight.w500,
                        ),
                        textAlign: TextAlign.left,
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 4/defaultScreenHeight*screenHeight),
                      child: Text(
                        Eventcontent.name,
                        style: TextStyle(
                          color: Color.fromARGB(255, 0, 3, 44),
                          fontSize: screenHeight * 13 / 720,
                          fontFamily: "IBMPlexSans",
                          fontWeight: FontWeight.w500,
                        ),
                        textAlign: TextAlign.left,
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                height: screenHeight * 41 / 720,
                margin: EdgeInsets.only(left: 15/defaultScreenWidth*screenWidth, top: 20/defaultScreenHeight*screenHeight),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    Align(
                      alignment: Alignment.topLeft,
                      child: Text(
                        "Candidate Email",
                        style: TextStyle(
                          color: Color.fromARGB(255, 153, 154, 171),
                          fontSize: screenHeight * 12 / 720,
                          fontFamily: "IBMPlexSans",
                          fontWeight: FontWeight.w500,
                        ),
                        textAlign: TextAlign.left,
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 4/defaultScreenHeight*screenHeight),
                      child: Text(
                        Eventcontent.email,
                        style: TextStyle(
                          color: Color.fromARGB(255, 0, 3, 44),
                          fontSize: screenHeight * 13 / 720,
                          fontFamily: "IBMPlexSans",
                          fontWeight: FontWeight.w500,
                        ),
                        textAlign: TextAlign.left,
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                height: screenHeight * 41 / 720,
                margin: EdgeInsets.only(left: 15/defaultScreenWidth*screenWidth, top: 20 / defaultScreenHeight * screenHeight),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    Align(
                      alignment: Alignment.topLeft,
                      child: Container(
                        width: screenWidth * 130 / 360,
                        height: screenHeight * 41 / 720,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          children: [
                            Align(
                              alignment: Alignment.topLeft,
                              child: Text(
                                "Date",
                                style: TextStyle(
                                  color:
                                  Color.fromARGB(255, 153, 154, 171),
                                  fontSize: screenHeight * 12 / 720,
                                  fontFamily: "IBMPlexSans",
                                  fontWeight: FontWeight.w500,
                                ),
                                textAlign: TextAlign.left,
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.only(top: 4/defaultScreenHeight*screenHeight),
                              child: Text(
                                Date.toString(),
                                style: TextStyle(
                                  color: Color.fromARGB(255, 0, 3, 44),
                                  fontSize: screenHeight * 13 / 720,
                                  fontFamily: "IBMPlexSans",
                                  fontWeight: FontWeight.w500,
                                ),
                                textAlign: TextAlign.left,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    Spacer(),
                    Align(
                      alignment: Alignment.topLeft,
                      child: Container(
                        width: screenWidth * 150 / 360,
                        height: screenHeight * 41 / 720,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          children: [
                            Align(
                              alignment: Alignment.topLeft,
                              child: Text(
                                "Time",
                                style: TextStyle(
                                  color:
                                  Color.fromARGB(255, 153, 154, 171),
                                  fontSize: screenHeight * 12 / 720,
                                  fontFamily: "IBMPlexSans",
                                  fontWeight: FontWeight.w500,
                                ),
                                textAlign: TextAlign.left,
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.only(top: 4/defaultScreenHeight*screenHeight),
                              child: Text(
                                timeFrom.toString()+"AM"+"-"+timeFrom.toString()+"PM",
                                style: TextStyle(
                                  color: Color.fromARGB(255, 0, 3, 44),
                                  fontSize: screenHeight * 13 / 720,
                                  fontFamily: "IBMPlexSans",
                                  fontWeight: FontWeight.w500,
                                ),
                                textAlign: TextAlign.left,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                margin: EdgeInsets.only(left: 15/defaultScreenWidth*screenWidth, top: 20/defaultScreenHeight*screenHeight, right: 15/defaultScreenWidth*screenWidth),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    Align(
                      alignment: Alignment.topLeft,
                      child: Text(
                        "Test Venue",
                        style: TextStyle(
                          color: Color.fromARGB(255, 153, 154, 171),
                          fontSize: screenHeight * 12 / 720,
                          fontFamily: "IBMPlexSans",
                          fontWeight: FontWeight.w500,
                        ),
                        textAlign: TextAlign.left,
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 4/defaultScreenHeight*screenHeight,bottom: 20/defaultScreenHeight*screenHeight),
                      child: Text(
                        testVenueName.toString()+","+testVenueCountry.toString()+","+testVenueState.toString()+","+testVenueCity.toString()+","+testVenueStreet.toString(),
                        style: TextStyle(
                          color: Color.fromARGB(255, 0, 3, 44),
                          fontSize: screenHeight * 13 / 720,
                          fontFamily: "IBMPlexSans",
                          fontWeight: FontWeight.w500,
                        ),
                        textAlign: TextAlign.left,
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _panel() {
    final screenHeight = MediaQuery.of(context).size.height;
    final screenWidth = MediaQuery.of(context).size.width;
    final defaultScreenHeight = 720;
    final defaultScreenWidth = 360;
    return Container(
      height: screenHeight * 265 / 720,
      margin: EdgeInsets.only(bottom: 18 / defaultScreenHeight * screenHeight),
      //color: Colors.black,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.end,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Align(
            alignment: Alignment.topCenter,
            child: Container(
              width: screenWidth * 140 / 360,
              height: screenHeight * 130 / 720,
              margin: EdgeInsets.only(bottom: 18 / defaultScreenHeight * screenHeight),
              child: new SvgPicture.asset(
                getBookSlotSvg.cancelbooking,
                fit: BoxFit.contain,
                //color: Colors.red,
              ),
            ),
          ),

          Align(
            alignment: Alignment.topCenter,
            child: Container(
              margin: EdgeInsets.only(bottom: 32 / defaultScreenHeight * screenHeight),
              child: Text(
                "Are you sure you want\nto cancel booking?",
                style: TextStyle(
                  color: Color.fromARGB(255, 0, 0, 0),
                  fontSize: screenHeight * 16 / 720,
                  fontFamily: "IBMPlexSans",
                  fontWeight: FontWeight.w500,
                ),
                textAlign: TextAlign.center,
              ),
            ),
          ),
          Container(
            height: screenHeight * 40 / 720,
            margin: EdgeInsets.only(left: 43 / defaultScreenWidth * screenWidth, bottom: 40/ defaultScreenHeight * screenHeight, right: 42 / defaultScreenWidth * screenWidth),
            child: Row(
              children: [
                Container(
                  width: screenWidth * 130 / 360,
                  height: screenHeight * 40 / 720,
                  child: FlatButton(
                    onPressed: () {
                      _controller.close();
                    },
                    color: Color.fromARGB(255, 242, 244, 244),
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(Radius.circular(3)),
                    ),
                    textColor: Color.fromARGB(255, 126, 145, 154),
                    padding: EdgeInsets.all(0),
                    child: Text(
                      "No",
                      style: TextStyle(
                        fontSize: screenHeight * 14 / 720,
                        fontFamily: "IBMPlexSans",
                        fontWeight: FontWeight.w500,
                      ),
                      textAlign: TextAlign.center,
                    ),
                  ),
                ),
                Spacer(),
                Container(
                  width: screenWidth * 130 / 360,
                  height: screenHeight * 40 / 720,
                  child: FlatButton(
                    onPressed: (){
                      int count = 0;
                      //cancelAdmitCardApi();
//                      if(cancelMsgFromApi=="Cancelled Successfully"){
//                        Navigator.popUntil(context, (route) {
//                          return count++ == 2;
//                        }
//                        );
//                      }
                      Navigator.popUntil(context, (route) {
                          return count++ == 2;
                        }
                        );
                    },
//                      //  onYesPressed(context),
//                    //_controller.close(),
//                    AppRoutes.pop(context),
                    color: Color.fromARGB(255, 0, 171, 251),
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(Radius.circular(2)),
                    ),
                    textColor: Color.fromARGB(255, 255, 255, 255),
                    padding: EdgeInsets.all(0),
                    child: Text(
                      "Yes",
                      style: TextStyle(
                        fontSize: screenHeight * 14 / 720,
                        fontFamily: "IBMPlexSans",
                        fontWeight: FontWeight.w500,
                      ),
                      textAlign: TextAlign.center,
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  /// important instructions ... ///
  Widget getTextWidgets(List<String> strings)
  {
    final screenHeight = MediaQuery.of(context).size.height;
    final screenWidth = MediaQuery.of(context).size.width;
    final defaultScreenHeight = 720;
    final defaultScreenWidth = 360;
    List<Widget> list = new List<Widget>();
    for(var i = 0; i < strings.length; i++){
      list.add(Container(
          margin: EdgeInsets.only(left: 15/ defaultScreenWidth * screenWidth, top: 10/ defaultScreenHeight * screenHeight, right: 15/ defaultScreenWidth * screenWidth),
          child: new Text(strings[i],
            style: TextStyle(
              color: Color.fromARGB(255, 87, 93, 96),
              fontSize: screenWidth * 12 / 360,
              fontFamily: "IBMPlexSans",
            ),
            textAlign: TextAlign.left,
          ),

      ),);
    }
    return new Column(children: list);
  }
}

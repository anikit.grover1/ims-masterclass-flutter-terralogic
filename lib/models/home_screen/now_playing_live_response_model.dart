// To parse this JSON data, do
//
//     final nowPlayingLiveResponseModel = nowPlayingLiveResponseModelFromJson(jsonString);

import 'dart:convert';

NowPlayingLiveResponseModel nowPlayingLiveResponseModelFromJson(String str) => NowPlayingLiveResponseModel.fromJson(json.decode(str));

String nowPlayingLiveResponseModelToJson(NowPlayingLiveResponseModel data) => json.encode(data.toJson());

class NowPlayingLiveResponseModel {
  NowPlayingLiveResponseModel({
    this.success,
    this.data,
    this.isComboAccess,
    this.message,
    this.errorCode,
  });

  bool success;
  List<Datum> data;
  bool isComboAccess;
  String message;
  dynamic errorCode;

  factory NowPlayingLiveResponseModel.fromJson(Map<String, dynamic> json) => NowPlayingLiveResponseModel(
    success: json["success"],
    data: List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
    isComboAccess: json["isComboAccess"],
    message: json["message"],
    errorCode: json["errorCode"],
  );

  Map<String, dynamic> toJson() => {
    "success": success,
    "data": List<dynamic>.from(data.map((x) => x.toJson())),
    "isComboAccess": isComboAccess,
    "message": message,
    "errorCode": errorCode,
  };
}

class Datum {
  Datum({
    this.orderNo,
    this.originalId,
    this.subject,
    this.componentType,
    this.component,
    this.lessonType,
    this.parentId,
    this.exams,
    this.companyCode,
    this.categories,
    this.courseId,
    this.timeStamp,
    this.id,
    this.isProductAccess,
    this.userRatingCount,
    this.testId,
    this.testStatus,
    this.isActive,
    this.videoDts,
  });

  int orderNo;
  String originalId;
  List<String> subject;
  ComponentType componentType;
  Component component;
  LessonType lessonType;
  ParentId parentId;
  List<String> exams;
  CompanyCode companyCode;
  List<String> categories;
  String courseId;
  DateTime timeStamp;
  String id;
  bool isProductAccess;
  int userRatingCount;
  String testId;
  TestStatus testStatus;
  int isActive;
  VideoDts videoDts;

  factory Datum.fromJson(Map<String, dynamic> json) => Datum(
    orderNo: json["orderNo"],
    originalId: json["OriginalId"] == null ? null : json["OriginalId"],
    subject: json["subject"] == null ? null : List<String>.from(json["subject"].map((x) => x == null ? null : x)),
    componentType: componentTypeValues.map[json["componentType"]],
    component: Component.fromJson(json["component"]),
    lessonType: lessonTypeValues.map[json["lessonType"]],
    parentId: parentIdValues.map[json["parentId"]],
    exams: List<String>.from(json["exams"].map((x) => x)),
    companyCode: companyCodeValues.map[json["companyCode"]],
    categories: List<String>.from(json["categories"].map((x) => x)),
    courseId: json["courseId"],
    timeStamp: DateTime.parse(json["timeStamp"]),
    id: json["id"],
    isProductAccess: json["isProductAccess"],
    userRatingCount: json["userRatingCount"] == null ? null : json["userRatingCount"],
    testId: json["testId"],
    testStatus: testStatusValues.map[json["testStatus"]],
    isActive: json["isActive"] == null ? null : json["isActive"],
    videoDts: json["videoDts"] == null ? null : VideoDts.fromJson(json["videoDts"]),
  );

  Map<String, dynamic> toJson() => {
    "orderNo": orderNo,
    "OriginalId": originalId == null ? null : originalId,
    "subject": subject == null ? null : List<dynamic>.from(subject.map((x) => x == null ? null : x)),
    "componentType": componentTypeValues.reverse[componentType],
    "component": component.toJson(),
    "lessonType": lessonTypeValues.reverse[lessonType],
    "parentId": parentIdValues.reverse[parentId],
    "exams": List<dynamic>.from(exams.map((x) => x)),
    "companyCode": companyCodeValues.reverse[companyCode],
    "categories": List<dynamic>.from(categories.map((x) => x)),
    "courseId": courseId,
    "timeStamp": timeStamp.toIso8601String(),
    "id": id,
    "isProductAccess": isProductAccess,
    "userRatingCount": userRatingCount == null ? null : userRatingCount,
    "testId": testId,
    "testStatus": testStatusValues.reverse[testStatus],
    "isActive": isActive == null ? null : isActive,
    "videoDts": videoDts == null ? null : videoDts.toJson(),
  };
}

enum CompanyCode { IMS }

final companyCodeValues = EnumValues({
  "ims": CompanyCode.IMS
});

class Component {
  Component({
    this.duration,
    this.thumbnail,
    this.videoDescription,
    this.vimeoUri,
    this.playerType,
    this.videoAuthor,
    this.referenceType,
    this.vimeoLink,
    this.label,
    this.title,
    this.content,
    this.objectType,
    this.viewLimit,
  });

  String duration;
  String thumbnail;
  String videoDescription;
  String vimeoUri;
  PlayerType playerType;
  String videoAuthor;
  ReferenceType referenceType;
  String vimeoLink;
  Label label;
  String title;
  String content;
  ObjectType objectType;
  int viewLimit;

  factory Component.fromJson(Map<String, dynamic> json) => Component(
    duration: json["duration"],
    thumbnail: json["thumbnail"] == null ? null : json["thumbnail"],
    videoDescription: json["videoDescription"] == null ? null : json["videoDescription"],
    vimeoUri: json["vimeoURI"] == null ? null : json["vimeoURI"],
    playerType: playerTypeValues.map[json["playerType"]],
    videoAuthor: json["videoAuthor"] == null ? null : json["videoAuthor"],
    referenceType: referenceTypeValues.map[json["referenceType"]],
    vimeoLink: json["vimeoLink"] == null ? null : json["vimeoLink"],
    label: json["label"] == null ? null : labelValues.map[json["label"]],
    title: json["title"],
    content: json["content"] == null ? null : json["content"],
    objectType: objectTypeValues.map[json["objectType"]],
    viewLimit: json["viewLimit"] == null ? null : json["viewLimit"],
  );

  Map<String, dynamic> toJson() => {
    "duration": duration,
    "thumbnail": thumbnail == null ? null : thumbnail,
    "videoDescription": videoDescription == null ? null : videoDescription,
    "vimeoURI": vimeoUri == null ? null : vimeoUri,
    "playerType": playerTypeValues.reverse[playerType],
    "videoAuthor": videoAuthor == null ? null : videoAuthor,
    "referenceType": referenceTypeValues.reverse[referenceType],
    "vimeoLink": vimeoLink == null ? null : vimeoLink,
    "label": label == null ? null : labelValues.reverse[label],
    "title": title,
    "content": content == null ? null : content,
    "objectType": objectTypeValues.reverse[objectType],
    "viewLimit": viewLimit == null ? null : viewLimit,
  };
}

enum Label { CAT }

final labelValues = EnumValues({
  "CAT": Label.CAT
});

enum ObjectType { LESSON }

final objectTypeValues = EnumValues({
  "Lesson": ObjectType.LESSON
});

enum PlayerType { VIMEO, YOUTUBE }

final playerTypeValues = EnumValues({
  "VIMEO": PlayerType.VIMEO,
  "YOUTUBE": PlayerType.YOUTUBE
});

enum ReferenceType { VIDEO }

final referenceTypeValues = EnumValues({
  "Video": ReferenceType.VIDEO
});

enum ComponentType { LEARNING_OBJECT }

final componentTypeValues = EnumValues({
  "learningObject": ComponentType.LEARNING_OBJECT
});

enum LessonType { FREE }

final lessonTypeValues = EnumValues({
  "free": LessonType.FREE
});

enum ParentId { D943_A1_EE90_B33_A6523301_A01_B77674690_D772_F449 }

final parentIdValues = EnumValues({
  "d943a1ee90b33a6523301a01b77674690d772f449": ParentId.D943_A1_EE90_B33_A6523301_A01_B77674690_D772_F449
});

enum TestStatus { START }

final testStatusValues = EnumValues({
  "Start": TestStatus.START
});

class VideoDts {
  VideoDts({
    this.isBookmarked,
    this.referParentId,
    this.objectType,
    this.status,
    this.parentId,
    this.videoRating,
    this.referenceType,
    this.companyCode,
    this.userId,
    this.courseId,
    this.timeStamp,
    this.id,
    this.duration,
    this.isChannel,
    this.referenceId,
  });

  bool isBookmarked;
  ParentId referParentId;
  ObjectType objectType;
  String status;
  String parentId;
  int videoRating;
  ReferenceType referenceType;
  CompanyCode companyCode;
  String userId;
  String courseId;
  DateTime timeStamp;
  String id;
  int duration;
  bool isChannel;
  String referenceId;

  factory VideoDts.fromJson(Map<String, dynamic> json) => VideoDts(
    isBookmarked: json["isBookmarked"],
    referParentId: parentIdValues.map[json["referParentId"]],
    objectType: objectTypeValues.map[json["objectType"]],
    status: json["status"],
    parentId: json["parentId"],
    videoRating: json["videoRating"],
    referenceType: referenceTypeValues.map[json["referenceType"]],
    companyCode: companyCodeValues.map[json["companyCode"]],
    userId: json["userId"],
    courseId: json["courseId"],
    timeStamp: DateTime.parse(json["timeStamp"]),
    id: json["id"],
    duration: json["duration"],
    isChannel: json["isChannel"],
    referenceId: json["referenceId"],
  );

  Map<String, dynamic> toJson() => {
    "isBookmarked": isBookmarked,
    "referParentId": parentIdValues.reverse[referParentId],
    "objectType": objectTypeValues.reverse[objectType],
    "status": status,
    "parentId": parentId,
    "videoRating": videoRating,
    "referenceType": referenceTypeValues.reverse[referenceType],
    "companyCode": companyCodeValues.reverse[companyCode],
    "userId": userId,
    "courseId": courseId,
    "timeStamp": timeStamp.toIso8601String(),
    "id": id,
    "duration": duration,
    "isChannel": isChannel,
    "referenceId": referenceId,
  };
}

class EnumValues<T> {
  Map<String, T> map;
  Map<T, String> reverseMap;

  EnumValues(this.map);

  Map<T, String> get reverse {
    if (reverseMap == null) {
      reverseMap = map.map((k, v) => new MapEntry(v, k));
    }
    return reverseMap;
  }
}

import 'package:flutter/material.dart';
import 'package:imsindia/utils/colors.dart';
import 'package:flutter/material.dart';
import 'package:imsindia/utils/validator.dart';
import 'package:imsindia/components/primary_Button.dart';



class TextStyles {
  var primaryButtonTextStyles= new PrimaryButton();

  static TextStyle get loginTitleStyle =>new TextStyle(
    color: const Color(0xff1f252b),
    fontWeight: FontWeight.w700,
    fontFamily: "IBMPlexSans",
    fontStyle: FontStyle.normal,
    fontSize:  21,
    inherit: false,
  );

  //textstyle for login subtitle
  static TextStyle get loginSubTitleStyle => const TextStyle(
      color: const Color(0xff7e919a),
      fontWeight: FontWeight.w400,
      fontFamily: "IBMPlexSans",
      fontStyle: FontStyle.normal,
      fontSize: 14.0
  );

  //textstyle for label
  static TextStyle get labelStyle => TextStyle(
            height: 2.5,
            fontFamily: "IBMPlexSans",
            fontSize:14,
            fontWeight: FontWeight.w400,
            color: NeutralColors.blue_grey,
            inherit: false,
          );
 static TextStyle get labelStyleDropDown => TextStyle(
        fontFamily: "IBMPlexSans",
        fontSize:14,
        fontWeight: FontWeight.w400,
        color: NeutralColors.blue_grey,
        inherit: false,
        fontStyle: FontStyle.normal
      );
  static TextStyle get prefixStyle => TextStyle(
            fontFamily: "IBMPlexSans",
            fontWeight: FontWeight.w400,
            fontSize: 14,
            color: NeutralColors.black,
            textBaseline: TextBaseline.alphabetic,
            letterSpacing: 0.0,
            inherit: false,
            
          );
static TextStyle get hintStyle => TextStyle(
            fontFamily: "IBMPlexSans",
            fontSize:14,
            fontWeight: FontWeight.w400,
            color: NeutralColors.blue_grey,
            inherit: false,
          );
  //textstyle for input form while editing
  static TextStyle get editTextStyle => TextStyle(
        fontFamily: "IBMPlexSans",
        fontWeight: FontWeight.w400,
        fontSize: 14,
        color: NeutralColors.charcoal_grey,
        textBaseline: TextBaseline.alphabetic,
        letterSpacing: 0.0,
        inherit: false,
      );
    static TextStyle get editTextStyleWithCharcoal => TextStyle(
        fontFamily: "IBMPlexSans",
        fontWeight: FontWeight.w400,
        fontSize: 14,
        color: NeutralColors.charcoal_grey,
        textBaseline: TextBaseline.alphabetic,
        letterSpacing: 0.0,
        inherit: false,
      );

  //textstyle for buttontext
  static TextStyle get buttonTextStyle => TextStyle(
        fontFamily: "IBMPlexSans",
        fontWeight: FontWeight.w500,
        fontSize: 14,
        color:NeutralColors.pureWhite,
        inherit: false,
      );
  static TextStyle get buttonTextStyleGradient => TextStyle(
    fontFamily: "IBMPlexSans",
    fontWeight: FontWeight.w500,
    fontSize: 14,
    color: NeutralColors.blue_grey,
    inherit: false,
  );
  //textstyle for error
  static TextStyle get errorTextStyle => TextStyle(
    fontFamily: "IBMPlexSans",
    fontWeight: FontWeight.w400,
    fontSize: 12,
    color: Colors.red,
    inherit: true,
    textBaseline: TextBaseline.alphabetic,
    decorationStyle: TextDecorationStyle.solid

  );
  //textstyle for google and facebook
  static TextStyle get google_FacebookTextStyle => TextStyle(
    fontFamily: "IBMPlexSans",
    fontSize: 14,
    fontWeight: FontWeight.w500,
    color: NeutralColors.pureWhite,
    inherit: true,
  );

  //textstyle for signup
  static TextStyle get signUpStyle => TextStyle(
        fontFamily: "IBMPlexSans",
        fontSize: 12,
        color: PrimaryColors.azure_Dark,
        inherit: false,
      );

  //textstyle for signup message
  static TextStyle get signUpMessageStyle => TextStyle(
        fontFamily: "IBMPlexSans",
        fontSize: 12,
        color: NeutralColors.gunmetal,
        inherit: false,
      );

  //textstyle for signup message
  static TextStyle get loginAlternativeOption => const TextStyle(
      color: NeutralColors.dark_navy_blue,
      fontWeight: FontWeight.normal,
      fontFamily: "IBMPlexSans",
      fontStyle: FontStyle.normal,
      fontSize: 14.0);


  static TextStyle get loginAlternativeMessage => const TextStyle(
  color: SemanticColors.blueGrey,
  fontWeight: FontWeight.w400,
  fontFamily: "IBMPlexSans",
  fontStyle: FontStyle.normal,
  fontSize: 14.0);

  static TextStyle get accountConfirmation => const TextStyle(
  color: NeutralColors.gunmetal,
  fontWeight: FontWeight.w400,
  fontFamily: "IBMPlexSans",
  fontStyle: FontStyle.normal,
  fontSize: 12.0);


  static TextStyle get forgotPassword => const TextStyle(
  color:  NeutralColors.charcoal_grey,
  fontWeight: FontWeight.w700,
  fontFamily: "IBMPlexSans",
  fontStyle:  FontStyle.normal,
  fontSize: 21.0
  );
  static TextStyle get forgotPasswordMessage => const TextStyle(
  color:  NeutralColors.blue_grey,
  fontWeight: FontWeight.w400,
  fontFamily: "IBMPlexSans",
  fontStyle:  FontStyle.normal,
  fontSize: 14.0
  );
  static TextStyle get congratulationsSubText => const TextStyle(
      color:  NeutralColors.blue_grey,
      fontWeight: FontWeight.w400,
      fontFamily: "IBMPlexSans",
      fontStyle:  FontStyle.normal,
      fontSize: 14.0
  );
  static TextStyle get continueImsPsw => const TextStyle(
    color:  PrimaryColors.azure_Dark,
    fontSize: 14,
    fontFamily: "IBMPlexSans",
    fontWeight: FontWeight.w400,
  );
  static TextStyle get createProfile => const TextStyle(
      color:  NeutralColors.charcoal_grey,
      fontWeight: FontWeight.w700,
      fontFamily: "IBMPlexSans",
      fontStyle:  FontStyle.normal,
      fontSize: 21
  );

}

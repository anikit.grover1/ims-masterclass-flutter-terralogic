
class getPrepareSvgImages{

  /// active and inactive state of icons ///
  static String solutionInactive = 'assets/images/solution_inactive.svg';
  static String solutionActive = 'assets/images/solution_active.svg';
  static String barGraphInactive = 'assets/images/bargraph_inactive.svg';
  static String barGraphActive = 'assets/images/bargraph_active.svg';
  static String bookmarkInactive = 'assets/images/bookmark_inactive.svg';
  static String bookmarkActive = 'assets/images/bookmark_active.svg';
  /// end active and inactive state of icons ///

  static String backIcon = 'assets/images/back.svg';
  static String dropdown = 'assets/images/secondary_dropdown.svg';
  static String exitImageForPopUp = 'assets/images/exit_1.svg';
  static String puseImageForPopUp = 'assets/images/pause.svg';
  static String playwhite = 'assets/images/playwhite.svg';
  static const String teacher_Icon = 'assets/images/teacher.svg';
}
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
class ChannelAssets {
// Assets Used for Login Page
  static const String filtericon = "assets/images/filter.svg";
  static const String downloadicon = "assets/images/download_blue.svg";
  static const String calendaricon = "assets/images/calendar_blue.svg";
  static const String filterSelected = "assets/images/filterselected.svg";
  static const String filledStar = "assets/images/path.svg";
  static const String emptyStar= "assets/images/path (1).svg";
  static const String Star= "assets/images/path (2).svg";
  static const String bookmark='assets/images/bookmark_inactive.svg';
  static const String filtericonAnalytics = "assets/images/filter.svg";
  static const String activeRadioButton = "assets/images/active_radio_button.svg";
  static const String inactiveRadioButton = "assets/images/inactive_radio_button.svg";
  static const String bookmarkblue = "assets/images/bookmark_blue.svg";
  static const String bookmarkselected = "assets/images/bookmark-selected.svg";
  static const String likeinactive = "assets/images/like_inactive.svg";
  static const String likeactive = "assets/images/like_active.svg";
  static const String comment = "assets/images/comment.svg";
  static const String attachment = "assets/images/attachment_gradient.svg";


  static Widget filterIcon = new SvgPicture.asset(
    filtericon,
    color: Colors.black,
    alignment:Alignment.centerLeft,
  );
  static Widget filterIconAnalytics = new SvgPicture.asset(
    filtericon,
    color: Colors.grey,
    alignment:Alignment.centerLeft,
  );
  static Widget downloadSvgIcon = new SvgPicture.asset(
    downloadicon,
  );
  static Widget calendarSvgIcon = new SvgPicture.asset(
    calendaricon,
  );
  static Widget filledStarSvgIcon = new SvgPicture.asset(
    filledStar,
  );
  static Widget emptyStarSvgIcon = new SvgPicture.asset(
    emptyStar,
  );
  static Widget starSvgIcon = new SvgPicture.asset(
    Star,
  );
  static Widget bookmarkSvgIcon = new SvgPicture.asset(
    bookmark,
  );
  static Widget bookmarkSelectedSvgIcon = new SvgPicture.asset(
      bookmarkselected,
  );
  static Widget activeRadioButtonIcon = new SvgPicture.asset(
    activeRadioButton,
  );
  static Widget inactiveRadioButtonIcon = new SvgPicture.asset(
    inactiveRadioButton,
  );

  static Widget filterSelectedSvgIcon = new SvgPicture.asset(
    filterSelected,
    height: 18,
    width: 16,
  );
  static Widget bookmarkblueSvgIcon = new SvgPicture.asset(
    bookmarkblue,
  );
  static Widget likeinactiveSvgIcon = new SvgPicture.asset(
    likeinactive,
  );
  static Widget likeactiveSvgIcon = new SvgPicture.asset(
    likeactive,
  );
  static Widget commentSvgIcon = new SvgPicture.asset(
    comment,
  );
  static Widget attachmentSvgIcon = new SvgPicture.asset(
    attachment,
  );
}
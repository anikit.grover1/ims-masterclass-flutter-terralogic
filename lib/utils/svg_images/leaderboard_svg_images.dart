class LeaderboardAssets {
// Assets Used for Login Page
  static const String olive_leaves_icon = 'assets/images/olive_branches_award_symbol.svg';  
  static const String back_icon = 'assets/images/back.svg';


  static const String Maestro_DI_icon = 'assets/images/Maestro_DI.svg';
  static const String Maestro_LR = 'assets/images/Maestro_LR.svg';
  static const String Prodigy_GK_icon = 'assets/images/Prodigy_GK';
  static const String Amateur_DI_icon = 'assets/images/Amateur_DI.svg';




}
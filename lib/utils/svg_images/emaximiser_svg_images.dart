
class getEmaximiserSvgImages{
  static String solutionInactive = 'assets/images/solution_inactive.svg';
  static String solutionActive = 'assets/images/solution_active.svg';
  static String barGraphInactive = 'assets/images/bargraph_inactive.svg';
  static String barGraphActive = 'assets/images/bargraph_active.svg';
  static String backIcon = 'assets/images/back.svg';
  static String bookmarkInactive = 'assets/images/bookmark_inactive.svg';
  static String bookmarkActive = 'assets/images/bookmark_active.svg';
  static String dropdown = 'assets/images/secondary_dropdown.svg';
  static String exitImageForPopUp = 'assets/images/exit_1.svg';
  static String puseImageForPopUp = 'assets/images/pause.svg';
}
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
class GKZoneAssets {
// Assets Used for Login Page
  static const String scienceSvg = "assets/images/gk_zone_image_one.svg";
  static const String geographySvg = "assets/images/gk_zone_image_two.svg";
  static const String historySvg = "assets/images/gk_zone_image_three.svg";
  static const String insideTickSvg = "assets/images/circular_tick.svg";
  static const String tickSvg = "assets/images/tick.svg";
  static const String notCompleted = "assets/images/Oval.svg";
  static const String selectedSvg = "assets/images/selected.svg";
  static const String fiftyFilledSvg = "assets/images/fifty_fifty_option_active.svg";
  static const String fiftyNotFilledSvg = "assets/images/fifty_fifty_option_inactive.svg";
  static const String popularFilledSvg = "assets/images/popular_answer_active.svg";
  static const String popularNotFilledSvg = "assets/images/popular_answer_inactive.svg";
  static const String RedPopularFilledSvg = "assets/images/popular_answer_red.svg";
  static const String performance_rankSvg = "assets/images/performance_rank.svg";

  static Widget scienceSvgIcon = new SvgPicture.asset(
    scienceSvg,

  );
  static Widget geographySvgIcon = new SvgPicture.asset(
    geographySvg,

  );
  static Widget historySvgIcon = new SvgPicture.asset(
    historySvg,
  );
  static Widget fiftyNotFilledSvgIcon = new SvgPicture.asset(
    fiftyNotFilledSvg,
  );
  static Widget fiftyFilledSvgIcon = new SvgPicture.asset(
    fiftyFilledSvg,
  );
  static Widget popularFilledSvgIcon = new SvgPicture.asset(
    popularFilledSvg,
  );
  static Widget popularNotFilledSvgIcon = new SvgPicture.asset(
    popularNotFilledSvg,
  );
  static Widget RedPopularFilledSvgIcon = new SvgPicture.asset(
    RedPopularFilledSvg,
  );
  static Widget tickSvgIcon = new SvgPicture.asset(
    tickSvg,
    height: 35,
    width: 35,
  );
  static Widget performance_rankSvgIcon = new SvgPicture.asset(
    performance_rankSvg,
  );
}
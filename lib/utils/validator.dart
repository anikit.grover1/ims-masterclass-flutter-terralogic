import 'package:imsindia/resources/strings/login.dart';

//validation for Text Input Fields
class FieldValidator {
 // static String get match => 'true';
  static bool buttonEnable = false;
  static bool phoneNo = false;
  static bool dropDownValue = false;
  static bool nameValue = false;
  static bool emailID = false;
  static bool forgotEmailID = false;
  static bool password = false;
  static bool imsPin = false;
  static bool loginImsPin = false;
  static bool imsPassword = false;
  static bool loginImsPassword = false;
  static bool address = false;
  static bool pinCode = false;
  static bool organisation = false;
  static bool experience = false;
  static bool college = false;
  static bool gradeValue = false;
  static bool loginUsingIms = false;

  //validation for Email ID
  static String validateEmail(String value) {
    if (value.isEmpty || value.length==0) {
      buttonEnable = false;
      emailID = false;
      forgotEmailID = false;
      return null;
    }


    Pattern pattern =
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';

    RegExp regex = new RegExp(pattern);

    if (!regex.hasMatch(value.trim())) {
      buttonEnable=false;
      emailID=false;
      forgotEmailID = false;
      print('emailID'+emailID.toString());



      return LoginAppLabels.invalidID;
    }
    if (regex.hasMatch(value.trim())) {
      buttonEnable=true;
      emailID=true;
      forgotEmailID = true;
      print('emailID'+emailID.toString());
      return null;
    }

  }

  static String validatePassword(String value){
    if (value.isEmpty) {
      buttonEnable=false;
      password = false;
      return null;
    }
    else{
      buttonEnable=true;
      password = true;
    }
//    Pattern pattern = r'^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[!@#\$&*~]).{8,}$';
//    RegExp regex = new RegExp(pattern);
//    if (!regex.hasMatch(value.trim())) {
//      buttonEnable=false;
//      password = false;
//      return CongratulationsLabels.invalidPassword;
//    }
//    if (regex.hasMatch(value.trim())) {
//      buttonEnable=true;
//      password = true;
//
//      return null;
//    }
  }
  static String validateMobileNo(String value){
    if (value.isEmpty) {
      phoneNo=false;
      return null;
    }
    Pattern pattern = r'^[6-9]\d{9}$';
    RegExp regex = new RegExp(pattern);
    if (!regex.hasMatch(value.trim())) {
      phoneNo=false;

      return ThreeStepLoginLables.invalidMobileno;
    }
    if (regex.hasMatch(value.trim())) {
      phoneNo=true;

      return null;
    }
  }
  static String validateDropDown(String value){
    if (value.isEmpty || value=='select the product') {
      dropDownValue=false;
      return null;
    }else{
      dropDownValue=true;
    }
  }

  static String validateName(String value) {
    Pattern pattern = r'(^[a-zA-Z ]*$)';
    RegExp regex = new RegExp(pattern);
    if (value.isEmpty || value.length==0) {
      buttonEnable = false;
      nameValue = false;
      print('1st');
      return null;
    }
    if(value.length>=4){
     if (!regex.hasMatch(value.trim())) {
      buttonEnable = false;
      nameValue = false;
      print('2nd');

      return CreateProfileLabels.invalidName;
    }
    else if (regex.hasMatch(value.trim())) {
      buttonEnable = true;
      nameValue = true;
      print('3rd');

      return null;
    }
    }else{
      buttonEnable = false;
      nameValue = false;
      print('4th');
      return CreateProfileLabels.nameLengh;
    }

  }

  static String validateIMSPin(String value){
    if(value.isEmpty && value.length==0){
        imsPin=false;
        loginImsPin=false;

        return null;
    }else if(value.length<3){
      imsPin=false;
      loginImsPin=false;

      return LoginUsingIMSPin.invalidIMSPin;
    }
    else{
      imsPin=true;
      loginImsPin=true;
      print('validateIMSPassword'+value);

      return null;
    }
  }
  static String validateIMSPassword(String value){
    if(value.isEmpty && value.length==0){
      imsPassword=false;

      return null;
    }else if(value.length<3){
      imsPassword=false;
      return LoginUsingIMSPin.invalidIMSPassword;
    }
    else{
      imsPassword=true;
      print('validateIMSPassword'+value);

      return null;
    }
  }

  static String validateLoginIMSPassword(String value){
    if(value.isEmpty && value.length==0){
      loginImsPassword=false;

      return null;
    }else if(value.length<6){
      loginImsPassword=false;
      return LoginUsingIMSPin.invalidIMSPassword;
    }
    else{
      loginImsPassword=true;
      print('validateIMSPassword'+value);

      return null;
    }
  }
  static String validateLoginUsingIMSPassword(String value){
    if (value.isEmpty || value.length==0) {
      buttonEnable = false;
      emailID = false;
      return null;
    }
 if(value.length<3){
      loginUsingIms=false;
      return LoginUsingIMSPin.invalidIMSPassword;
    }
    else{
      loginUsingIms=true;
      print('validateIMSPassword'+loginUsingIms.toString());

      return null;
    }
  }
  static String validateAddress(String value){
    if (value.isEmpty) {
      address=false;
      print('isEmpty');
      return null;
    }else if(value.length<5){
      address=false;
      return ThreeStepLoginLables.invalidAddresslength;

    }
    Pattern pattern = r'(^[a-zA-Z0-9\s,#().-]*$)';
    RegExp regex = new RegExp(pattern);
    if (!regex.hasMatch(value.trim())) {
      print('isEmptyfsgsrgfsgfdfgdfg');

      address=false;

      return ThreeStepLoginLables.invalidAddress;
    }
    if (regex.hasMatch(value.trim())) {
      print('isEmptygbbdghd');

      address=true;

      return null;
    }
  }
  static String validatePinCode(String value){
    if (value.isEmpty) {
      pinCode=false;
      return null;
    }
    else if(value.length<5){
      organisation=false;
      return LoginConfirmaddressLables.pincodeLength;
    }
    Pattern pattern = r'^[1-9][0-9]{5}$';
    RegExp regex = new RegExp(pattern);
    if (!regex.hasMatch(value.trim())) {
      pinCode=false;

      return ThreeStepLoginLables.invalidPincode;
    }
    if (regex.hasMatch(value.trim())) {
      pinCode=true;

      return null;
    }
  }
  static String validateGrade(String value){
     if(value.isEmpty && value.length==0){
      gradeValue=false;

      return null;
    }Pattern pattern = r'^[a-eA-E]$';
    RegExp regex = new RegExp(pattern);
    if (!regex.hasMatch(value.trim())) {
      gradeValue=false;
      return null;
    }
    if (regex.hasMatch(value.trim())) {
      gradeValue=true;

      return null;
    }
    else{
      gradeValue=true;
      print('validateIMSPassword'+value);

      return null;
    }
  }

  static String validateExperience(String value){
    if (value.isEmpty) {
      experience=false;
      return null;
    }
    Pattern pattern = r'^[1-9][0-9]*$';
    RegExp regex = new RegExp(pattern);
    if (!regex.hasMatch(value.trim())) {
      experience=false;
      return LoginWorkexperienceLables.invalidExperience;
    }
    if (regex.hasMatch(value.trim())) {
      experience=true;


      return null;
    }
  }

  static String validateOrganisation(String value){
    if (value.isEmpty) {
      organisation=false;
      return null;
    }
    else if(value.length<5){
      organisation=false;
      return LoginWorkexperienceLables.invalidOrganisationlength;
    }
    Pattern pattern = r'^[0-9]*$';
    RegExp regex = new RegExp(pattern);
    if (regex.hasMatch(value.trim())) {
      organisation=false;
      return LoginWorkexperienceLables.organisation;
    }
    if (!regex.hasMatch(value.trim())) {
      organisation=true;

      return null;
    }
  }

  static String validateCollege(String value){
    if (value.isEmpty) {
      college=false;
      return null;
    }
    else if(value.length<5){
      college=false;
      return CreateProfileLabels.invalidCollegelength;
    }
    Pattern pattern = r'^[0-9]*$';
    RegExp regex = new RegExp(pattern);
    if (regex.hasMatch(value.trim())) {
      college=false;
      return CreateProfileLabels.invalidCollege;
    }
    if (!regex.hasMatch(value.trim())) {
      college=true;

      return null;
    }
  }

}
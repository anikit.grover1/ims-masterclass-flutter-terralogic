import 'package:flutter/material.dart';

class PrimaryColors {
  //Primary colors used in application
  static const Color azure_Dark = const Color(0xFF00abfb);
  static const Color azure_Medium = const Color(0xFF7e919a);
  static const Color azure_light = const Color(0xFFf2f4f4);
  static const Color kelly_green = const Color(0xFF00b410);
  static const Color yellow = const Color(0xFFffcd00);
  static const Color dark_coral = const Color(0xFFdc4e41);

//00b410
}
class AccentColors {
  //Accent colors used in application
  static const Color purpleish_blue_Dark = const Color(0xFFFFFFFF);
  static const Color blueGrey = const Color(0xFF7e919a);
  static const Color iceBlue = const Color(0xFFf2f4f4);
}
class SemanticColors {
  //Semantic colors used in application
  static const Color light_purpely = const Color(0xFF3380cc);
  static const Color dark_purpely = const Color(0xFF886dd7);
  static const Color light_black = const Color(0xFF808080);

  static const Color dark_coral = const Color(0xFFdc4e41);
  static const Color dusky_blue = const Color(0xFF475993);
  static const Color pureWhite = const Color(0xFFFFFFFF);
  static const Color blueGrey = const Color(0xFF7e919a);
  static const Color iceBlue = const Color(0xFFf2f4f4);
  static const Color light_ice_blue = const Color(0xFFe2ecf0);
}
class NeutralColors {
  //Neutral colors used in application
  static const Color black = const Color(0xFF1f252b);
  static const Color dark_black = const Color(0xFF000000);

  static const Color blue_grey = const Color(0xFF7e919a);
  static const Color ice_blue = const Color(0xfff2f4f4);
  static const Color charcoal_grey = const Color(0xff2d3438);
  static const Color dark_navy_blue = const Color(0xFF00022c);
  static const Color gunmetal = const Color(0xFF575d60);
  static const Color pureWhite = const Color(0xFFFFFFFF);
  static const Color sun_yellow = const Color(0xFFFDD424);
  static const Color mango = const Color(0xFFff982a);
  static const Color off_white = const Color(0xFFfff8e7);
  static const Color purpleish_blue = const Color(0xFF5647EB);
  static const Color purply = const Color(0xFF7a71d5);
  static const Color bluey_grey = const Color(0xFF999aab);
  static const Color grapefruit = const Color(0xFFff5757);
  static const Color purpley = const Color(0xFF886dd7);
  static const Color dark_blue = const Color(0xFF00032c);
  static const Color index_color = const Color(0xFF646464);
  static const Color box_color = const Color(0xFF896ed8);
  static const Color insturction_color = const Color(0xFFf2f6f8);
  static const Color colors_scroll = const Color(0xffcecece);
  static const Color scroll = const Color(0xff00b410);
  static const Color greycolor = const Color(0xFF3380cc);
  static const Color grycolor = const Color(0xFF3380cc);
  static const Color textblack = const Color(0xFF000000);
  static const Color kelly_green = const Color(0xFF00b410);
  static const Color pale_grey = const Color(0xFFe5e7fa);
  static const Color white10 = const Color(0xFFf7f6f9);
  static const Color purpel_box = const Color(0XFF5647EB);
  static const Color preparePi_balck = const Color(0xFF1F252B);
    static const Color deep_sky_blue = const Color(0xFF0b76ff);

}

class GradientColors {
  //Gradient colors used in application
  static const Color pureWhite = const Color(0xFFFFFFFF);
  static const Color blueGrey = const Color(0xFF7e919a);
  static const Color iceBlue = const Color(0xFFf2f4f4);
  static const Color googleColor = const Color(0xFFdc4e41);
  static const Color facebookColor = const Color(0xFF475993);
}

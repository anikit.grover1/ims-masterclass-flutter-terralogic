import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
//import 'package:imsindia/utils/svg_icons.dart';



class getSvgIcon{
  static String analysis = 'assets/images/analyse.svg';
  static String chanel = 'assets/images/channel.svg';
  static String gdpi = 'assets/images/gdpi.svg';
  static String gkzone = 'assets/images/gkzone.svg';
  static String maximiser = 'assets/images/maximiser.svg';
  static String notification = 'assets/images/notification.svg';
  static String practice = 'assets/images/practice.svg';
  static String prepare = 'assets/images/prepare.svg';
  static String blogs='assets/images/blogs.svg';
  static String arrows='assets/images/shape-copy-3.svg'; 
  static String search='assets/images/search.svg';
  static String filter = 'assets/images/filter.svg';
  static String profilePlaceHolderImage = 'assets/images/profileImage_placeholder.svg';
  //svg files are missing
  // static String discussion='assets/images/blogs.svg';
  //static String leadindboard='assets/images/blogs.svg';
  //static String eventbooking='assets/images/blogs.svg';










  static Widget analyseSvgIcon = new SvgPicture.asset(
    analysis,
  );
  static Widget chanelSvgIcon = new SvgPicture.asset(
    chanel,
  );
  static Widget gdpiSvgIcon = new SvgPicture.asset(
    gdpi,
  );
  static Widget gkzoneSvgIcon = new SvgPicture.asset(
    gkzone,
  );
  static Widget maximiserSvgIcon = new SvgPicture.asset(
    maximiser,
  );
  static Widget notificationSvgIcon = new SvgPicture.asset(
    notification,
  );
  static Widget practiceSvgIcon = new SvgPicture.asset(
    practice,
  );
  static Widget prepareSvgIcon = new SvgPicture.asset(
    prepare,
  );
  static Widget blogsSvgIcon = new SvgPicture.asset(
    blogs,
  );
  static Widget filterSvgIcon = new SvgPicture.asset(
    filter,
  );
  static Widget profilePlaceHolderSvgIcon = new SvgPicture.asset(
    profilePlaceHolderImage,
  );
  
  // svg files are missing
// static Widget discussionSvgIcon = new SvgPicture.asset(
//    blogs,
//  );
// static Widget leaderboardSvgIcon = new SvgPicture.asset(
//    blogs,
//  );/ static Widget eventboookingSvgIcon = new SvgPicture.asset(
//    blogs,
//  );


}
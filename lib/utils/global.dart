library imsIndia.globals;

import 'package:flutter/material.dart';
import 'package:imsindia/api/bloc/services.dart';
import 'package:imsindia/utils/Url.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:imsindia/views/Arun/ims_utility.dart';

var loginAccessToken;
var sample;
int result = 10;
TextEditingController _sampleController = new TextEditingController();
var courseId;
const idForLearnFirstHirApi = '12898';
const idForEmaxFirstHirApi ='13025';
var idForLearnTestApi = '';
var titleForPrepareSubjectsScreen = 'PieChart';
var userName;
var resetLinkSentMail;
var tokenForTestLaunchApi;
String token;
String presentToken;
//   void getCourseId() async {
//   SharedPreferences prefs = await SharedPreferences.getInstance();
//   courseId=prefs.getString("courseId");
// }

  //returns true if it is debug mode and returns false in release mode
   bool get checkdebugmode{
     var debug = false;
     assert(debug = true); //assert works only in debug mode
    return debug;
  }
  
  //Function used to get userToken for required API
  Future get getToken async {
    print("inside token");  
   SharedPreferences prefs = await SharedPreferences.getInstance();
   token = prefs.getString("loginAccessToken");
   var loginCheckFlag = prefs.getBool("LoginStatus");
   var course = prefs.getString("courseId");
   var t = {
     "Content-Type": "application/json",
     "Authorization": token
    };

    //API : Calling the get menu API to check if the token is valid or not
    return ApiService().getAPI(URL.GET_MENU_ITEMS + course,t).then((returnValue)  async {
          print("inside inside");

          //If the statuscode is 401 and the message is "Unauthorized" , then run the login API in the background
          if(returnValue[0].toString().toLowerCase() == "Unauthorized".toLowerCase()){
            if(loginCheckFlag == true){
            return await getNewToken.then((value) async { 
               return {
                    "Content-Type": "application/json",
                    "Authorization": value.toString()
                    };});
          }

         //else use the token available from the login
          }else{
            return  {
                    "Content-Type": "application/json",
                    "Authorization": token
                    };
          }
        });
 }

 //API : To run the login API in the background using the login mode 3 and get the new token
 Future get getNewToken async {
    var headers = {"Content-Type": "application/json"};
    
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var userEmail = prefs.getString(ImsStrings.sCurrentUserEmail);
    var courseId = prefs.getString("courseId");
    var token;
    Map postdata = {
      "loginId": userEmail,
      "password": "password",
      "courseId": courseId,
      "loginMode": "3",
      "companyCode": "ims"
    };
    // ignore: missing_return
    return ApiService().postAPI(URL.Login, postdata, headers).then((result) {
      print("Login result value"+result.toString());
      
        if (result[0].toString().toLowerCase() == 'login successful'.toLowerCase()) {
          
          // setCourseId(courseId);
          // setCourseName(courseName);
          // setUserID(result[1]['data']['userData']['userId']);
          
          setToken(result[1]['data']['token']);
          return result[1]['data']['token'].toString();

          // setCurrentUserEmail(result[1]['data']['userData']['emailId']);
          // setCurrentUserLoginId(result[1]['data']['userData']['loginId']);
          // IMSSharedPref().set_limitCellularDataUsage(false);

          }
        });
 }

 void setToken(value) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString("loginAccessToken", value);
    print(value);
  }


 Future get getStatus async {
   print("inside status check");
   bool flag = false;
   SharedPreferences prefs = await SharedPreferences.getInstance();
   flag = prefs.getBool("flagCheck");
   print(flag);
   return flag;
 }


var headersWithAuthorizationKey = {
  "Content-Type": "application/json",
  "Authorization":"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjM0MjkzNCIsImlzU3R1ZGVudCI6MSwiY29tcGFueUNvZGUiOiJpbXMiLCJpYXQiOjE1NzEyMDg0OTl9.RtMVe7fmZZMUa_hTNyK8VgjNA2Ev1e9LEJU52v1yueA"
};


var headers = {"Content-Type": "application/json"};
 var headersWithOtherAuthorizationKey={
    "Accept": "application/json",
   "Authorization":"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjM0MjkzNCIsImlzU3R1ZGVudCI6MSwiY29tcGFueUNvZGUiOiJpbXMiLCJpYXQiOjE1Njk5MjIyMjl9.V0WKsMB8NWn3wqSwThWrcU2lPesRqSRl9Cwk6xMTpGM"
 };
var headersAnalyticsWithAuthorizationKey={
  "Accept": "application/json",
  "Authorization":"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6Ijk2MyIsImlzU3R1ZGVudCI6MSwiY29tcGFueUNvZGUiOiJpbXMiLCJpYXQiOjE1Nzc0Mjg1OTB9.Pdu-_FRwnSp4UIdijxpdeGRC0YQgie_JpKy8hXwIJf0"
};
class GlobalDataForSharedPreferences extends StatefulWidget {
  @override
  _GlobalDataForSharedPreferencesState createState() => _GlobalDataForSharedPreferencesState();
}

class _GlobalDataForSharedPreferencesState extends State<GlobalDataForSharedPreferences> {
  @override
  void initState() {
    
    // TODO: implement initState
    super.initState();
  }
  
  
  @override
  Widget build(BuildContext context) {
    return Container(
      
    );
  }
}
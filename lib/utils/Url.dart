class URL {
  static const String BASE_URL =
      'https://agfdgp5ji9.execute-api.ap-south-1.amazonaws.com/dev/';
  static const String BASE_URL_USING_PROD =
      'https://08brkq02c5.execute-api.ap-south-1.amazonaws.com/prod/';
  static const String GET_COURSE_NAMES = BASE_URL + 'courses/all-courses';
  static const String GET_USER_COURSE_NAMES = BASE_URL + 'courses/user-courses';
  static const String Login = BASE_URL + 'login';
  static const String Details_Using_Email_or_Pin =
      'https://lima.imsindia.com/MyIMS/default.aspx?action=getstudent';
  static const String ForgotPassword =
      'http://lima.imsindia.com/MyIMS/default.aspx?action=forgotpassword';
  static const String ResetPasswordLink = BASE_URL + 'reset-password';
  static const String ResetLinkEmailBase64 =
      "https://myims.imsindia.com/reset?id=";
  static const String ValidateOldpasswordLink =
      "https://lima.imsindia.com/MyIMS/default.aspx?action=login";
  static const String ResetPasswordLinkURLinSettings =
      "https://lima.imsindia.com/MyIMS/default.aspx?action=resetpassword";

  ///** prepare API Url's**///
  static String GET_LEARN_FIRST_HIRA_NAMES =
      BASE_URL + 'get-learning-module?courseId=';
  static String GET_LEARN_SUBJECTS_NAMES = BASE_URL + 'learn/tests?parentId=';
  static String GET_SUBJECT_QUES_DETAILS =
      BASE_URL + 'get-learningobject?objectId=';
  static String TEST_LAUNCH_FOR_YES_PREPARE = BASE_URL + 'test-launch';
  static String GET_QUESTIONS_DATA_FOR_PREPARE =
      BASE_URL_USING_PROD + 'test-launch';
  static String GET_QUESTIONS_DATA_FOR_PREPARE_REVISE_SECTION = BASE_URL_USING_PROD + 'revise';
  static String END_TEST_API_FOR_PREPARE = BASE_URL_USING_PROD + 'end-test';
  static String USERACTION_API_FOR_PREPARE = BASE_URL+'user-actions';
  ///** prepare API Url's end**///

  ///** practice API Url's**///
  static String GET_LEARN_FIRST_HIRA_NAMES_PRACTICE =
      BASE_URL + 'get-learning-module?courseId=';
  static String GET_LEARN_SUBJECTS_NAMES_PRACTICE = BASE_URL + 'student-tests?parentId=';

  ///** practice API Url's end**///

  ///** DB name for prepare track API
  static final String TBLForPrepareTrackApi = 'TableForPrepareTrackApiDetails';
  ///** DB name for prepare track API ends...



  ///** Emax API Url's**///
  static String SYNC_API = BASE_URL_USING_PROD+'sync-track';

  static String EMAX_END_TEST_API =
      BASE_URL_USING_PROD + 'end-test';
  static String EMAX_URL = BASE_URL + 'emaximiser?parentId=';
  static String EMAX_URL_DROP = BASE_URL + 'student-tests?parentId=';
  static String EMAX_PROD_REVISE = BASE_URL_USING_PROD + 'revise';
  static String EMAX_END_TEST= BASE_URL_USING_PROD + 'end-test';
  static String EMAX_USER_ACTION =BASE_URL+ "user-actions/emaximiser";

  ///** Emax DB Url's**///

  static final String TBL_EmaximiserContent = 'emaximisercontent';


  ///** Emax Url's end**///

  static String GET_MENU_ITEMS = BASE_URL + 'get-learningmodule-id?courseId=';
  static String GET_VIDEOS = BASE_URL + 'learn/videos?parentId=';
  static String WEATHER_CITY_HYD =
      'api.openweathermap.org/data/2.5/weather?q=hyderabad&appid=c13dd907a69f32f9fb608b02e07e36f6';
  static String WEATHER_CITY_LON =
      'api.openweathermap.org/data/2.5/weather?q=london&appid=c13dd907a69f32f9fb608b02e07e36f6';
  static String SAMPLE_EMPLOYEE_DETAILS = 'https://reqres.in/api/users?page=1';

  ///** Book a test API Url's **///
  static String BOOK_A_TEST_API_TO_GET_SLOTS =
      BASE_URL + 'get-bookingslots?parentId=';
  static String BOOK_A_TEST_API_TO_SIMCAT_BOOKING =
      BASE_URL + 'test-venue-booking?testId=';
  static String BOOK_A_TEST_API_TO_SAVE_SIMCAT_DATA =
      BASE_URL + 'test-venue-booking';
  static String BOOK_A_TEST_API_TO_GET_ADMIN_CARD_DATA =
      BASE_URL + 'test-venue-booking/admit-card?studentBookedId=';
  static String BOOK_A_TEST_API_TO_CANCEL_ADMIT_CARD =
      BASE_URL + 'test-venue-booking';

  ///** Analytics API Url's **///
  static String ANALYTICS_API_LEARN_MODULE_LIST = BASE_URL + 'performance/lists?courseId=';
  static String ANALYTICS_API_SCORECARD_DATA = BASE_URL + 'performance/score-card?courseId=';
  static String ANALYTICS_API_PERFORMANCE_ANALYTICS = BASE_URL + 'performance/performance-analytics?testId=';
  static String ANALYTICS_API_SCORECARDIMPROVEMENT = BASE_URL + 'performance/improvement?testId=';

  ///** Analytics wrapper API Url's **///
  static String ANALYTICS_API_PERFORMANCE_ANALYTICS_WRAPPER = 'https://22flcuciee.execute-api.ap-south-1.amazonaws.com/dev/wrapperAnalytics';

  ///** Blogs API Url's **///
  static String BLOGS_API_COURSES_LIST = 'https://myims.imsindia.com/wp-json/wp/v2/ims_courses?per_page=';
  static String BLOGS_API_TAB_LIST = 'https://myims.imsindia.com/wp-json/wp/v2/categories?per_page=';
  static String BLOGS_API_POST_LIST = 'https://myims.imsindia.com/wp-json/wp/v2/posts?per_page=';

  ///**myPLAN API Url's**///
  /// live .........
  //static String myPLAN_baseUrl = 'http://13.235.65.199:5051';
  /// staging ..............
  static String myPLAN_baseUrl = 'http://52.172.7.155:5051';

  static String myPLAN_API_VENUE_DETAILS = myPLAN_baseUrl+'/myPlan/student/mentorsData/branchesWeb';
  static String myPLAN_API_MENTOR_DETAILS = myPLAN_baseUrl+'/myPlan/student/mentorsData';


  static String myPLAN_API_ADD_STUDENT = myPLAN_baseUrl+'/myPlan/student/addStudent';
  static String myPLAN_API_BOOKINGS_AVAILABLE = myPLAN_baseUrl+'/myPlan/student/bookingsAvailable';
  static String myPLAN_API_CONFIRMATION_BOOKING = myPLAN_baseUrl+'/myPlan/student/bookingsData/';
  static String myPLAN_API_CANCEL_BOOKING = myPLAN_baseUrl+'/myPlan/student/cancelBooking';
  static String myPLAN_API_DATES_AVAILABLE = myPLAN_baseUrl+'/myPlan/student/datesAvailable';
  static String myPLAN_API_SLOTS_AVAILABLE = myPLAN_baseUrl+'/myPlan/student/slotsAvailable';
  static String myPLAN_API_COURSE_REASON_TYPE = myPLAN_baseUrl+'/myPlan/student/courseReasonTypes';
  static String myPLAN_API_OTPGenerator = myPLAN_baseUrl+'/myPlan/auth/OTPGenerator';
  static String myPLAN_bookingsData = myPLAN_baseUrl+'/myPlan/student/bookingsData';
  static String myPLAN_BOOKSLOT = myPLAN_baseUrl+'/myPlan/student/bookSlot';



      ///**GD-PI API Url's**///

  static String GET_FIRST_SECOND_TITLE_GDPI = BASE_URL + 'get-learning-module?courseId=';
  static String GDPI_RHS_DATA = BASE_URL + 'student-tests?parentId=';
  static String GDPI_GET_LEARNINGOBJECT = BASE_URL+'get-learningobject?objectId=';



     ///Gk-Zone API Urls***//

  static const String GKZone_Base_Url =
      'https://9v9hwmw0q2.execute-api.ap-south-1.amazonaws.com/dev/';

  static String GKZone_CategoryList = GKZone_Base_Url + 'categories-gkzone';
  static String GKZone_ReadPDF = GKZone_Base_Url + 'readpdf-gkzone';
  static String GKZone_SavePDFRecord = GKZone_Base_Url + 'savepdfrecords-gkzone';
  static String GKZone_Examwise = GKZone_Base_Url + 'examwise-gkzone';
  static String GKZone_CurrentAffairs = GKZone_Base_Url + 'currentaffairs-gkzone';

  static String GKZONE_READPDF= GKZone_Base_Url +'readpdf-gkzone';
  static String SAVE_PDF_RECORDS= GKZone_Base_Url +'savepdfrecords-gkzone';



}

URL url = URL();

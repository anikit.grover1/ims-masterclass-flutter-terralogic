import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class getSvgIcon{
  static String backbutton = 'assets/images/back.svg';
  static String lockicon = 'assets/images/lock.svg';
  static String selectedicon = 'assets/images/selected.svg';
  static String tickicon = 'assets/images/tick.svg';




  static Widget backSvgIcon = new SvgPicture.asset(
    backbutton,
    color: Colors.black,
  );
  static Widget lockSvgIcon = new SvgPicture.asset(
    lockicon,
    color: Colors.black,
  );
  static Widget selectedSvgIcon = new SvgPicture.asset(
    selectedicon,
    color: Colors.black,
  );
  static Widget tickSvgIcon = new SvgPicture.asset(
    tickicon,
    color: Colors.black,
  );

}




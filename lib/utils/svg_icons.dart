import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class getSvgIcon{
  static String backbutton = 'assets/images/back.svg';
  static String welcome = 'assets/images/welcome.svg';
  static String camera = 'assets/images/camera.svg';
  static String lock = 'assets/images/lock.svg';
  static String unlock = 'assets/images/unlock.svg';
  static String drop_down_button = 'assets/images/primary_dropdown.svg';
  static String combined_shape = 'assets/images/combined_shape.svg';
  static String avatar = 'assets/images/avatar.svg';
  static String analytics = 'assets/images/analytics.svg';
  static String google = 'assets/images/google.svg';
  static String facebook = 'assets/images/facebook.svg';
  static String like = 'assets/images/shape.svg';
  static String bookmark = 'assets/images/bookmark.svg';

  static Widget backSvgIcon = new SvgPicture.asset(
    backbutton,
    color: Colors.black,
  );
  static Widget welcomeSvgIcon = new SvgPicture.asset(
    welcome,
  );
  static Widget cameraSvgIcon = new SvgPicture.asset(
    camera,
  );
  static Widget lockSvgIcon = new SvgPicture.asset(
    lock,
    height: 13,
    width: 10,
  );
  static Widget unlockSvgIcon = new SvgPicture.asset(
    unlock,
    height: 13,
    width: 10,
  );
  static Widget drop_down_buttonSvgIcon = new SvgPicture.asset(
    drop_down_button,
  );
  static Widget combinedshapeSvgIcon = new SvgPicture.asset(
    combined_shape,
    fit: BoxFit.fill,
  );
  static Widget avatarSvgIcon = new SvgPicture.asset(
    avatar,
  );
  static Widget analyticsSvgIcon = new SvgPicture.asset(
    analytics,
    height: 132,
    width: 130,
  );
  static Widget googleSvgIcon = new SvgPicture.asset(
    google,
    height: 40,
    width: 130,
  );
  static Widget facebookSvgIcon = new SvgPicture.asset(
    facebook,
    height: 40,
    width: 130,
  );
  static Widget likeSvgIcon = new SvgPicture.asset(
    like,
  );
  static Widget bookmarkSvgIcon = new SvgPicture.asset(
    bookmark,
  );
}
import 'dart:async';

import 'package:imsindia/models/home_screen/now_playing_live_response_model.dart';
import 'package:imsindia/networking/handlers/api-response.dart';
import 'package:imsindia/networking/home_screen/now_playing_live_repository.dart';

class NowPlayingLiveBloc {

    NowPlayingLiveRepository _nowPlayingLiveRepository;
    StreamController _nowPlayingLiveController;

    StreamSink<ApiResponse<NowPlayingLiveResponseModel>> get _nowPlayingLiveSink => _nowPlayingLiveController.sink;
    Stream<ApiResponse<NowPlayingLiveResponseModel>> get nowPlayingLiveStream => _nowPlayingLiveController.stream;

    NowPlayingLiveBloc() {

        _nowPlayingLiveController = StreamController<ApiResponse<NowPlayingLiveResponseModel>>();
        _nowPlayingLiveRepository = NowPlayingLiveRepository();
        fetchNowPlayingLive();
    }

    fetchNowPlayingLive() async {

        _nowPlayingLiveSink.add(ApiResponse.loading(("Fetching Live Streaming..")));

        try {

            NowPlayingLiveResponseModel _model = await _nowPlayingLiveRepository.fetchNowPlayingLive();
            _nowPlayingLiveSink.add(ApiResponse.completed(_model));

        } catch (exception) {

            _nowPlayingLiveSink.add(ApiResponse.error(exception.toString()));
        }

    }

    dispose() {
        _nowPlayingLiveController.close();
    }
}


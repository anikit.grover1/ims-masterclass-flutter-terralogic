import 'package:f_logs/f_logs.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:imsindia/resources/strings/login.dart';
import 'package:imsindia/views/Analytics/analytics_home.dart';
import 'package:imsindia/views/Analytics/analytics_scorecard_performance_summary.dart';
import 'package:imsindia/views/home_pages/home_widget.dart';
import 'package:imsindia/views/leaderboard/leaderboard_screen.dart';
import 'package:imsindia/views/practice_pages/practice_home_screen.dart';
import 'package:imsindia/views/splash_screen/splash_screen.dart';
import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:firebase_core/firebase_core.dart';

import 'views/anikit_updates/ui/masterclass_screen/masterclass_screen.dart';

void main() async{
  //Crashlytics.instance.enableInDevMode = true;
  // Pass all uncaught errors from the framework to Crashlytics.
  //FlutterError.onError = Crashlytics.instance.recordFlutterError;
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
 // init();
  runApp(MyApp());
}

// init() {
//   /// Configuration example 1
// //  LogsConfig config = LogsConfig()
// //    ..isDebuggable = true
// //    ..isDevelopmentDebuggingEnabled = true
// //    ..customClosingDivider = "|"
// //    ..customOpeningDivider = "|"
// //    ..csvDelimiter = ", "
// //    ..isLogsEnabled = true
// //    ..encryptionEnabled = false
// //    ..encryptionKey = "123"
// //    ..formatType = FormatType.FORMAT_CURLY
// //    ..logLevelsEnabled = [LogLevel.INFO, LogLevel.ERROR]
// //    ..dataLogTypes = [
// //      DataLogType.DEVICE.toString(),
// //      DataLogType.NETWORK.toString(),
// //      "Zubair"
// //    ]
// //    ..timestampFormat = TimestampFormat.TIME_FORMAT_FULL_1;

//   /// Configuration example 2
// //  LogsConfig config = FLog.getDefaultConfigurations()
// //    ..isDevelopmentDebuggingEnabled = true
// //    ..timestampFormat = TimestampFormat.TIME_FORMAT_FULL_2;

//   /// Configuration example 3 Format Custom
//   LogsConfig config = FLog.getDefaultConfigurations()
//     ..isDebuggable = true
//     ..isDevelopmentDebuggingEnabled = true
//     ..isLogsEnabled = true
//     ..logLevelsEnabled
//     ..timestampFormat = TimestampFormat.TIME_FORMAT_FULL_3
//     ..formatType = FormatType.FORMAT_CUSTOM
//     ..fieldOrderFormatCustom = [
//       FieldName.TIMESTAMP,
//       FieldName.LOG_LEVEL,
//       FieldName.CLASSNAME,
//       FieldName.METHOD_NAME,
//       FieldName.TEXT,
//       FieldName.EXCEPTION,
//       FieldName.STACKTRACE
//     ]
//     ..customOpeningDivider = "|"
//     ..customClosingDivider = "|";

//   FLog.applyConfigurations(config);
// }

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
//    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
//      statusBarColor: Colors.transparent, //top bar color
//      statusBarIconBrightness: Brightness.dark, //top bar icons
//      systemNavigationBarColor: Colors.white, //bottom bar color
//      systemNavigationBarIconBrightness: Brightness.dark, //bottom bar icons
//    ));

    //To set the screen portrait up only in entire app (Android and iOS)
    // SystemChrome.setPreferredOrientations([
    //   DeviceOrientation.portraitUp,
    

    return MaterialApp(
      title: LoginAppStrings.appName,
      //This will redirect to LoginWelcome Screen
      home: MasterclassPlayerPage(),
       // SplashScreen(),
      //This is to Hide Debug Mode banner in application
      debugShowCheckedModeBanner: false,
      theme: ThemeData(primarySwatch: Colors.blue),
    );
  }
}
